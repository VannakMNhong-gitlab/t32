// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_PersonRoles
    public class Search_VwPersonRoles
    {
        public Guid PersonId { get; set; } // PersonId
        public string FirstName { get; set; } // FirstName
        public string LastName { get; set; } // LastName
        public string MiddleName { get; set; } // MiddleName
        public string FullName { get; set; } // FullName
        public int LoginId { get; set; } // LoginId
        public string ExternalId { get; set; } // ExternalId
        public string Username { get; set; } // Username
        public int? RoleId { get; set; } // RoleId
        public string RoleName { get; set; } // RoleName
        public bool? IsActive { get; set; } // IsActive
        public string Description { get; set; } // Description
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public string LastUpdatedByName { get; set; } // LastUpdatedByName
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // TrainingGrantStatus
    public class TrainingGrantStatu
    {
        public int Id { get; set; } // Id (Primary key)
        public string Name { get; set; } // Name
        public int SortOrder { get; set; } // SortOrder

        // Reverse navigation
        public virtual ICollection<TrainingGrant> TrainingGrants { get; set; } // TrainingGrant.FK_TrainingGrant_TrainingGrantStatus

        public TrainingGrantStatu()
        {
            TrainingGrants = new List<TrainingGrant>();
        }
    }

}

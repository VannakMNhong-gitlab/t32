// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Person
    public class Person
    {
        public Guid PersonId { get; set; } // PersonId (Primary key)
        public string FirstName { get; set; } // FirstName
        public string MiddleName { get; set; } // MiddleName
        public string LastName { get; set; } // LastName
        public int? IsUnderrepresentedMinority { get; set; } // IsUnderrepresentedMinority
        public int? IsIndividualWithDisabilities { get; set; } // IsIndividualWithDisabilities
        public int? IsFromDisadvantagedBkgd { get; set; } // IsFromDisadvantagedBkgd
        public DateTime DateCreated { get; set; } // DateCreated
        public int DisplayId { get; set; } // DisplayId
        public bool IsDeleted { get; set; } // IsDeleted
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public int? GenderId { get; set; } // GenderId
        public int? GenderAtBirthId { get; set; } // GenderAtBirthId
        public int? EthnicityId { get; set; } // EthnicityId

        // Reverse navigation
        public virtual ICollection<AcademicHistory> AcademicHistories { get; set; } // AcademicHistory.FK_AcademicHistory_Person
        public virtual ICollection<Applicant> Applicants { get; set; } // Applicant.FK_Applicant_Person
        public virtual ICollection<ContactAddress> ContactAddresses { get; set; } // ContactAddress.FK_ContactAddress_Person
        public virtual ICollection<ContactEmail> ContactEmails { get; set; } // ContactEmail.FK_ContactEmail_Person
        public virtual ICollection<ContactPhone> ContactPhones { get; set; } // ContactPhone.FK_ContactPhone_Person
        public virtual ICollection<Faculty> Faculties { get; set; } // Faculty.FK_Faculty_Person
        public virtual ICollection<FacultyAcademicData> FacultyAcademicDatas { get; set; } // FacultyAcademicData.FK_FacultyAcademicData_Person
        public virtual ICollection<FacultyAppointment> FacultyAppointments { get; set; } // FacultyAppointment.FK_FacultyAppointment_Person
        public virtual ICollection<FacultyResearchInterest> FacultyResearchInterests { get; set; } // FacultyResearchInterest.FK_FacultyResearchInterest_Person
        public virtual ICollection<GradeRecord> GradeRecords { get; set; } // GradeRecord.FK_GradeRecord_Person
        public virtual ICollection<Login> Logins { get; set; } // Logins.FK_Logins_Person
        public virtual ICollection<Mentee> Mentees { get; set; } // Mentee.FK_Mentee_Person
        public virtual ICollection<MenteeSupport> MenteeSupports { get; set; } // MenteeSupport.FK_MenteeSupport_Person
        public virtual ICollection<OsuTimeline> OsuTimelines { get; set; } // OSUTimeline.FK_OSUTimeline_Person
        public virtual ICollection<Program> Programs { get; set; } // Many to many mapping
        public virtual ICollection<Publication> Publications_AuthorId { get; set; } // Publication.FK_Publication_PersonAuthor
        public virtual ICollection<Publication> Publications_CoAuthorId { get; set; } // Publication.FK_Publication_PersonCoAuthor
        public virtual ICollection<TrainingPeriod> TrainingPeriods { get; set; } // TrainingPeriod.FK_TrainingPeriod_Person
        public virtual ICollection<WorkHistory> WorkHistories { get; set; } // WorkHistory.FK_WorkHistory_Person

        // Foreign keys
        public virtual Ethnicity Ethnicity { get; set; } // FK_Person_Ethnicity
        public virtual Gender Gender { get; set; } // FK_Person_Gender

        public Person()
        {
            PersonId = System.Guid.NewGuid();
            IsUnderrepresentedMinority = 0;
            IsIndividualWithDisabilities = 0;
            IsFromDisadvantagedBkgd = 0;
            DateCreated = System.DateTime.Now;
            IsDeleted = false;
            AcademicHistories = new List<AcademicHistory>();
            Applicants = new List<Applicant>();
            ContactAddresses = new List<ContactAddress>();
            ContactEmails = new List<ContactEmail>();
            ContactPhones = new List<ContactPhone>();
            Faculties = new List<Faculty>();
            FacultyAcademicDatas = new List<FacultyAcademicData>();
            FacultyAppointments = new List<FacultyAppointment>();
            FacultyResearchInterests = new List<FacultyResearchInterest>();
            GradeRecords = new List<GradeRecord>();
            Logins = new List<Login>();
            Mentees = new List<Mentee>();
            MenteeSupports = new List<MenteeSupport>();
            OsuTimelines = new List<OsuTimeline>();
            Publications_AuthorId = new List<Publication>();
            Publications_CoAuthorId = new List<Publication>();
            TrainingPeriods = new List<TrainingPeriod>();
            WorkHistories = new List<WorkHistory>();
            Programs = new List<Program>();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_SupportGeneralData
    internal class Search_VwSupportGeneralDataConfiguration : EntityTypeConfiguration<Search_VwSupportGeneralData>
    {
        public Search_VwSupportGeneralDataConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_SupportGeneralData");
            HasKey(x => new { x.SupportDisplayId, x.SupportTitle, x.SupportId, x.SupportStatusId });

            Property(x => x.SupportId).HasColumnName("SupportId").IsRequired();
            Property(x => x.SupportTitle).HasColumnName("SupportTitle").IsRequired();
            Property(x => x.SupportDisplayId).HasColumnName("SupportDisplayId").IsRequired();
            Property(x => x.SupportTypeId).HasColumnName("SupportTypeId").IsOptional();
            Property(x => x.SupportType).HasColumnName("SupportType").IsOptional().HasMaxLength(150);
            Property(x => x.SupportTypeAbbreviation).HasColumnName("SupportTypeAbbreviation").IsOptional().HasMaxLength(50);
            Property(x => x.SupportTypeFullName).HasColumnName("SupportTypeFullName").IsOptional().HasMaxLength(203);
            Property(x => x.SupportStatusId).HasColumnName("SupportStatusId").IsRequired();
            Property(x => x.SupportStatus).HasColumnName("SupportStatus").IsOptional().HasMaxLength(150);
            Property(x => x.SupportNumber).HasColumnName("SupportNumber").IsOptional().HasMaxLength(20);
            Property(x => x.SupportOrganizationId).HasColumnName("SupportOrganizationId").IsOptional();
            Property(x => x.SupportOrganization).HasColumnName("SupportOrganization").IsOptional().HasMaxLength(150);
            Property(x => x.SupportOrganizationAbbreviation).HasColumnName("SupportOrganizationAbbreviation").IsOptional().HasMaxLength(50);
            Property(x => x.SupportOrganizationFullName).HasColumnName("SupportOrganizationFullName").IsOptional().HasMaxLength(203);
            Property(x => x.DateStarted).HasColumnName("DateStarted").IsOptional();
            Property(x => x.DateEnded).HasColumnName("DateEnded").IsOptional();
            Property(x => x.DateCreated).HasColumnName("DateCreated").IsOptional();
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.CreatedByName).HasColumnName("CreatedByName").IsOptional().HasMaxLength(129);
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.LastUpdatedByName).HasColumnName("LastUpdatedByName").IsOptional().HasMaxLength(129);
        }
    }

}

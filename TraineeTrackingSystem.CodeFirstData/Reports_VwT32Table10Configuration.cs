// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_T32Table10
    internal class Reports_VwT32Table10Configuration : EntityTypeConfiguration<Reports_VwT32Table10>
    {
        public Reports_VwT32Table10Configuration(string schema = "Reports")
        {
            ToTable(schema + ".vw_T32Table10");
            HasKey(x => new { x.IsRenewal, x.Column4ItemTypeId, x.TrainingGrantId, x.Column4ItemType });

            Property(x => x.TrainingGrantId).HasColumnName("TrainingGrantId").IsRequired();
            Property(x => x.IsRenewal).HasColumnName("IsRenewal").IsRequired();
            Property(x => x.DoctoralLevelId).HasColumnName("DoctoralLevelId").IsOptional();
            Property(x => x.DoctoralLevel).HasColumnName("DoctoralLevel").IsOptional().HasMaxLength(150);
            Property(x => x.Column4ItemTypeId).HasColumnName("Column4ItemTypeId").IsRequired();
            Property(x => x.Column4ItemType).HasColumnName("Column4ItemType").IsRequired().HasMaxLength(10);
            Property(x => x.Column4ItemId).HasColumnName("Column4ItemId").IsOptional();
            Property(x => x.Column4ItemName).HasColumnName("Column4ItemName").IsOptional().HasMaxLength(250);
        }
    }

}

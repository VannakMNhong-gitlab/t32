// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_PersonDemographics
    internal class Search_VwPersonDemographicsConfiguration : EntityTypeConfiguration<Search_VwPersonDemographics>
    {
        public Search_VwPersonDemographicsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_PersonDemographics");
            HasKey(x => new { x.IsMentee, x.IsFaculty, x.DateCreated, x.IsApplicant, x.DisplayId, x.PersonId });

            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.DisplayId).HasColumnName("DisplayId").IsRequired();
            Property(x => x.FirstName).HasColumnName("FirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MiddleName).HasColumnName("MiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.LastName).HasColumnName("LastName").IsOptional().HasMaxLength(75);
            Property(x => x.FullName).HasColumnName("FullName").IsOptional().HasMaxLength(178);
            Property(x => x.IsUnderrepresentedMinority).HasColumnName("IsUnderrepresentedMinority").IsOptional();
            Property(x => x.IsIndividualWithDisabilities).HasColumnName("IsIndividualWithDisabilities").IsOptional();
            Property(x => x.IsFromDisadvantagedBkgd).HasColumnName("IsFromDisadvantagedBkgd").IsOptional();
            Property(x => x.GenderId).HasColumnName("GenderId").IsOptional();
            Property(x => x.Gender).HasColumnName("Gender").IsOptional().HasMaxLength(100);
            Property(x => x.GenderAtBirthId).HasColumnName("GenderAtBirthId").IsOptional();
            Property(x => x.GenderAtBirth).HasColumnName("GenderAtBirth").IsOptional().HasMaxLength(100);
            Property(x => x.EthnicityId).HasColumnName("EthnicityId").IsOptional();
            Property(x => x.Ethnicity).HasColumnName("Ethnicity").IsOptional().HasMaxLength(300);
            Property(x => x.DateCreated).HasColumnName("DateCreated").IsRequired();
            Property(x => x.EmployeeId).HasColumnName("EmployeeId").IsOptional().HasMaxLength(50);
            Property(x => x.StudentId).HasColumnName("StudentId").IsOptional().HasMaxLength(50);
            Property(x => x.ApplicantId).HasColumnName("ApplicantId").IsOptional();
            Property(x => x.IsFaculty).HasColumnName("IsFaculty").IsRequired();
            Property(x => x.IsMentee).HasColumnName("IsMentee").IsRequired();
            Property(x => x.IsApplicant).HasColumnName("IsApplicant").IsRequired();
            Property(x => x.PrimaryEmailAddress).HasColumnName("PrimaryEmailAddress").IsOptional().HasMaxLength(500);
            Property(x => x.EmailAddress).HasColumnName("EmailAddress").IsOptional().HasMaxLength(500);
            Property(x => x.MailingAddress).HasColumnName("MailingAddress").IsOptional().HasMaxLength(1668);
            Property(x => x.AddressLine1).HasColumnName("AddressLine1").IsOptional().HasMaxLength(500);
            Property(x => x.AddressLine2).HasColumnName("AddressLine2").IsOptional().HasMaxLength(500);
            Property(x => x.AddressLine3).HasColumnName("AddressLine3").IsOptional().HasMaxLength(500);
            Property(x => x.AddressCity).HasColumnName("AddressCity").IsOptional().HasMaxLength(100);
            Property(x => x.AddressStateId).HasColumnName("AddressStateId").IsOptional();
            Property(x => x.AddressStateProvince).HasColumnName("AddressStateProvince").IsOptional().HasMaxLength(50);
            Property(x => x.AddressPostalCode).HasColumnName("AddressPostalCode").IsOptional().HasMaxLength(10);
            Property(x => x.PhoneNumber).HasColumnName("PhoneNumber").IsOptional().HasMaxLength(25);
        }
    }

}

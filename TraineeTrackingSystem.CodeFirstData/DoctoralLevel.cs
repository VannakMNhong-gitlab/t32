// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // DoctoralLevel
    public class DoctoralLevel
    {
        public int Id { get; set; } // Id (Primary key)
        public string LevelName { get; set; } // LevelName

        // Reverse navigation
        public virtual ICollection<AcademicHistory> AcademicHistories { get; set; } // AcademicHistory.FK_AcademicHistory_DoctoralLevel
        public virtual ICollection<Applicant> Applicants { get; set; } // Applicant.FK_Applicant_DoctoralLevel
        public virtual ICollection<TraineeSummaryInfo> TraineeSummaryInfoes { get; set; } // TraineeSummaryInfo.FK_TraineeSummaryInfo_DoctoralLevel
        public virtual ICollection<TrainingGrant> TrainingGrants { get; set; } // TrainingGrant.FK_TrainingGrant_DoctoralLevel
        public virtual ICollection<TrainingGrantTrainee> TrainingGrantTrainees { get; set; } // TrainingGrantTrainee.FK_TrainingGrantTrainee_DoctoralLevel
        public virtual ICollection<TrainingPeriod> TrainingPeriods { get; set; } // TrainingPeriod.FK_TrainingPeriod_DoctoralLevel

        public DoctoralLevel()
        {
            AcademicHistories = new List<AcademicHistory>();
            Applicants = new List<Applicant>();
            TraineeSummaryInfoes = new List<TraineeSummaryInfo>();
            TrainingGrants = new List<TrainingGrant>();
            TrainingGrantTrainees = new List<TrainingGrantTrainee>();
            TrainingPeriods = new List<TrainingPeriod>();
        }
    }

}

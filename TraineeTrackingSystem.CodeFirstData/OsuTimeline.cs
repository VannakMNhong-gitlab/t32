// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // OSUTimeline
    public class OsuTimeline
    {
        public Guid Id { get; set; } // Id (Primary key)
        public Guid PersonId { get; set; } // PersonId
        public int? YearEntered { get; set; } // YearEntered
        public DateTime? DateApplied { get; set; } // DateApplied
        public bool? WasInterviewed { get; set; } // WasInterviewed
        public bool? WasOfferedPosition { get; set; } // WasOfferedPosition
        public bool? AcceptedOffer { get; set; } // AcceptedOffer
        public bool? WasEnrolled { get; set; } // WasEnrolled
        public bool? EnteredProgram { get; set; } // EnteredProgram
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded
        public bool? DeclinedOffer { get; set; } // DeclinedOffer
        public int? DeclinedAltInstitutionId { get; set; } // Declined_AltInstitutionId
        public bool? WasConsideredForAdmission { get; set; } // WasConsideredForAdmission
        public bool? WasAccepted { get; set; } // WasAccepted
        public bool IsDeleted { get; set; } // IsDeleted

        // Foreign keys
        public virtual Person Person { get; set; } // FK_OSUTimeline_Person

        public OsuTimeline()
        {
            Id = System.Guid.NewGuid();
            IsDeleted = false;
        }
    }

}

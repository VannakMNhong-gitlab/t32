// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_T32Table8A
    internal class Reports_VwT32Table8AConfiguration : EntityTypeConfiguration<Reports_VwT32Table8A>
    {
        public Reports_VwT32Table8AConfiguration(string schema = "Reports")
        {
            ToTable(schema + ".vw_T32Table8A");
            HasKey(x => new { x.IsRenewal, x.Column2ItemType, x.Column2ItemTypeId, x.TrainingGrantId });

            Property(x => x.TrainingGrantId).HasColumnName("TrainingGrantId").IsRequired();
            Property(x => x.IsRenewal).HasColumnName("IsRenewal").IsRequired();
            Property(x => x.DoctoralLevelId).HasColumnName("DoctoralLevelId").IsOptional();
            Property(x => x.DoctoralLevel).HasColumnName("DoctoralLevel").IsOptional().HasMaxLength(150);
            Property(x => x.Column2ItemTypeId).HasColumnName("Column2ItemTypeId").IsRequired();
            Property(x => x.Column2ItemType).HasColumnName("Column2ItemType").IsRequired().HasMaxLength(10);
            Property(x => x.Column2ItemId).HasColumnName("Column2ItemId").IsOptional();
            Property(x => x.Column2ItemName).HasColumnName("Column2ItemName").IsOptional().HasMaxLength(250);
        }
    }

}

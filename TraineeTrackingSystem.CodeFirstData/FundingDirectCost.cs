// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // FundingDirectCost
    public class FundingDirectCost
    {
        public int Id { get; set; } // Id (Primary key)
        public Guid FundingId { get; set; } // FundingId
        public int? CostYear { get; set; } // CostYear
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded
        public Decimal? CurrentYearDirectCosts { get; set; } // CurrentYearDirectCosts
        public Decimal? TotalDirectCosts { get; set; } // TotalDirectCosts
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public bool IsDeleted { get; set; } // IsDeleted
        public int? BudgetPeriodStatusId { get; set; } // BudgetPeriodStatusId

        // Foreign keys
        public virtual BudgetPeriodStatu BudgetPeriodStatu { get; set; } // FK_FundingDirectCost_BudgetPeriodStatus
        public virtual Funding Funding { get; set; } // FK_FundingDirectCost_Funding

        public FundingDirectCost()
        {
            IsDeleted = false;
        }
    }

}

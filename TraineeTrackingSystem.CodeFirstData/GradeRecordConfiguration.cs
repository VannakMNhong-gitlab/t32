// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // GradeRecord
    internal class GradeRecordConfiguration : EntityTypeConfiguration<GradeRecord>
    {
        public GradeRecordConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".GradeRecord");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.InstitutionId).HasColumnName("InstitutionId").IsOptional();
            Property(x => x.Gpa).HasColumnName("GPA").IsOptional().HasPrecision(10,2);
            Property(x => x.GpaScale).HasColumnName("GPAScale").IsOptional().HasPrecision(10,2);
            Property(x => x.YearTested).HasColumnName("YearTested").IsOptional();
            Property(x => x.TestScoreTypeId).HasColumnName("TestScoreTypeId").IsOptional();
            Property(x => x.GreScoreVerbal).HasColumnName("GREScoreVerbal").IsOptional().HasPrecision(10,2);
            Property(x => x.GreScoreQuantitative).HasColumnName("GREScoreQuantitative").IsOptional().HasPrecision(10,2);
            Property(x => x.GreScoreAnalytical).HasColumnName("GREScoreAnalytical").IsOptional().HasPrecision(10,2);
            Property(x => x.GreScoreSubject).HasColumnName("GREScoreSubject").IsOptional().HasPrecision(10,2);
            Property(x => x.GrePercentileVerbal).HasColumnName("GREPercentileVerbal").IsOptional().HasPrecision(10,2);
            Property(x => x.GrePercentileQuantitative).HasColumnName("GREPercentileQuantitative").IsOptional().HasPrecision(10,2);
            Property(x => x.GrePercentileAnalytical).HasColumnName("GREPercentileAnalytical").IsOptional().HasPrecision(10,2);
            Property(x => x.GrePercentileSubject).HasColumnName("GREPercentileSubject").IsOptional().HasPrecision(10,2);
            Property(x => x.McatScoreVerbalReasoning).HasColumnName("MCATScoreVerbalReasoning").IsOptional().HasPrecision(10,2);
            Property(x => x.McatScorePhysicalSciences).HasColumnName("MCATScorePhysicalSciences").IsOptional().HasPrecision(10,2);
            Property(x => x.McatScoreBiologicalSciences).HasColumnName("MCATScoreBiologicalSciences").IsOptional().HasPrecision(10,2);
            Property(x => x.McatScoreWriting).HasColumnName("MCATScoreWriting").IsOptional().HasMaxLength(5);
            Property(x => x.McatPercentile).HasColumnName("MCATPercentile").IsOptional().HasPrecision(10,2);
            Property(x => x.GpaComment).HasColumnName("GPAComment").IsOptional().HasMaxLength(150);
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();

            // Foreign keys
            HasRequired(a => a.Person).WithMany(b => b.GradeRecords).HasForeignKey(c => c.PersonId); // FK_GradeRecord_Person
            HasOptional(a => a.Institution).WithMany(b => b.GradeRecords).HasForeignKey(c => c.InstitutionId); // FK_GradeRecord_Institution
            HasOptional(a => a.TestScoreType).WithMany(b => b.GradeRecords).HasForeignKey(c => c.TestScoreTypeId); // FK_GradeRecord_TestScoreType
        }
    }

}

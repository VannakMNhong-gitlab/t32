// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_UsageOfStateProvinces
    public class Search_VwUsageOfStateProvinces
    {
        public int StateProvinceId { get; set; } // StateProvinceId
        public string Abbreviation { get; set; } // Abbreviation
        public string FullName { get; set; } // FullName
        public int? TotalContactAddress { get; set; } // TotalContactAddress
        public int? TotalInstitutions { get; set; } // TotalInstitutions
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Faculty
    internal class FacultyConfiguration : EntityTypeConfiguration<Faculty>
    {
        public FacultyConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".Faculty");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.EmployeeId).HasColumnName("EmployeeId").IsRequired().HasMaxLength(50);
            Property(x => x.FacultyRankId).HasColumnName("FacultyRankId").IsOptional();
            Property(x => x.PrimaryOrganizationId).HasColumnName("PrimaryOrganizationId").IsOptional();
            Property(x => x.SecondaryOrganizationId).HasColumnName("SecondaryOrganizationId").IsOptional();
            Property(x => x.OtherAffiliations).HasColumnName("OtherAffiliations").IsOptional().HasMaxLength(1000);
            Property(x => x.OtherTitles).HasColumnName("OtherTitles").IsOptional().HasMaxLength(250);
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.DateCreated).HasColumnName("DateCreated").IsOptional();
            Property(x => x.IsActive).HasColumnName("IsActive").IsRequired();
            Property(x => x.TiuId).HasColumnName("TIUId").IsOptional();
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.MentorId).HasColumnName("MentorId").IsOptional();
            Property(x => x.PositionDepartmentId).HasColumnName("PositionDepartmentId").IsOptional();

            // Foreign keys
            HasRequired(a => a.Person).WithMany(b => b.Faculties).HasForeignKey(c => c.PersonId); // FK_Faculty_Person
            HasOptional(a => a.FacultyRank).WithMany(b => b.Faculties).HasForeignKey(c => c.FacultyRankId); // FK_Faculty_FacultyRank
            HasOptional(a => a.Organization_PrimaryOrganizationId).WithMany(b => b.Faculties_PrimaryOrganizationId).HasForeignKey(c => c.PrimaryOrganizationId); // FK_Faculty_PrimaryOrganization
            HasOptional(a => a.Organization_SecondaryOrganizationId).WithMany(b => b.Faculties_SecondaryOrganizationId).HasForeignKey(c => c.SecondaryOrganizationId); // FK_Faculty_SecondaryOrganization
            HasOptional(a => a.Organization_TiuId).WithMany(b => b.Faculties_TiuId).HasForeignKey(c => c.TiuId); // FK_Faculty_TIU
            HasMany(t => t.Programs).WithMany(t => t.Faculties).Map(m => 
            {
                m.ToTable("FacultyProgram");
                m.MapLeftKey("FacultyId");
                m.MapRightKey("ProgramId");
            });
            HasMany(t => t.TrainingPeriods).WithMany(t => t.Faculties).Map(m => 
            {
                m.ToTable("TrainingPeriodFaculty");
                m.MapLeftKey("FacultyId");
                m.MapRightKey("TrainingPeriodId");
            });
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Institution
    public class Institution
    {
        public int Id { get; set; } // Id (Primary key)
        public string Name { get; set; } // Name
        public string InstitutionIdentifier { get; set; } // InstitutionIdentifier
        public string City { get; set; } // City
        public int? StateId { get; set; } // StateId
        public int? CountryId { get; set; } // CountryId
        public int InstitutionTypeId { get; set; } // InstitutionTypeId
        public bool IsDeleted { get; set; } // IsDeleted

        // Reverse navigation
        public virtual ICollection<AcademicHistory> AcademicHistories_InstitutionId { get; set; } // AcademicHistory.FK_AcademicHistory_Institution
        public virtual ICollection<AcademicHistory> AcademicHistories_ResidencyInstitutionId { get; set; } // AcademicHistory.FK_AcademicHistory_ResidencyInstitution
        public virtual ICollection<FacultyAcademicData> FacultyAcademicDatas { get; set; } // FacultyAcademicData.FK_FacultyAcademicData_Institution
        public virtual ICollection<FacultyAppointment> FacultyAppointments { get; set; } // FacultyAppointment.FK_FacultyAppointment_Institution
        public virtual ICollection<Funding> Fundings_PrimeSponsorId { get; set; } // Funding.FK_Funding_InstitutionPrimeSponsor
        public virtual ICollection<Funding> Fundings_SponsorId { get; set; } // Funding.FK_Funding_Sponsor
        public virtual ICollection<GradeRecord> GradeRecords { get; set; } // GradeRecord.FK_GradeRecord_Institution
        public virtual ICollection<MenteeSupport> MenteeSupports { get; set; } // MenteeSupport.FK_MenteeSupport_Institution
        public virtual ICollection<TrainingPeriod> TrainingPeriods { get; set; } // TrainingPeriod.FK_TrainingPeriod_Institution
        public virtual ICollection<WorkHistory> WorkHistories { get; set; } // WorkHistory.FK_WorkHistory_Institution

        // Foreign keys
        public virtual Country Country { get; set; } // FK_Institution_Country
        public virtual InstitutionType InstitutionType { get; set; } // FK_Institution_InstitutionType
        public virtual StateProvince StateProvince { get; set; } // FK_Institution_StateProvince

        public Institution()
        {
            IsDeleted = false;
            AcademicHistories_InstitutionId = new List<AcademicHistory>();
            AcademicHistories_ResidencyInstitutionId = new List<AcademicHistory>();
            FacultyAcademicDatas = new List<FacultyAcademicData>();
            FacultyAppointments = new List<FacultyAppointment>();
            Fundings_PrimeSponsorId = new List<Funding>();
            Fundings_SponsorId = new List<Funding>();
            GradeRecords = new List<GradeRecord>();
            MenteeSupports = new List<MenteeSupport>();
            TrainingPeriods = new List<TrainingPeriod>();
            WorkHistories = new List<WorkHistory>();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Institution
    internal class InstitutionConfiguration : EntityTypeConfiguration<Institution>
    {
        public InstitutionConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".Institution");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Name).HasColumnName("Name").IsRequired().HasMaxLength(250);
            Property(x => x.InstitutionIdentifier).HasColumnName("InstitutionIdentifier").IsOptional().HasMaxLength(150);
            Property(x => x.City).HasColumnName("City").IsOptional().HasMaxLength(150);
            Property(x => x.StateId).HasColumnName("StateId").IsOptional();
            Property(x => x.CountryId).HasColumnName("CountryId").IsOptional();
            Property(x => x.InstitutionTypeId).HasColumnName("InstitutionTypeId").IsRequired();
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();

            // Foreign keys
            HasOptional(a => a.StateProvince).WithMany(b => b.Institutions).HasForeignKey(c => c.StateId); // FK_Institution_StateProvince
            HasOptional(a => a.Country).WithMany(b => b.Institutions).HasForeignKey(c => c.CountryId); // FK_Institution_Country
            HasRequired(a => a.InstitutionType).WithMany(b => b.Institutions).HasForeignKey(c => c.InstitutionTypeId); // FK_Institution_InstitutionType
        }
    }

}

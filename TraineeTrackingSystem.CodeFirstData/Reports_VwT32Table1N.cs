// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_T32Table1N
    public class Reports_VwT32Table1N
    {
        public Guid TrainingGrantId { get; set; } // TrainingGrantId
        public bool IsRenewal { get; set; } // IsRenewal
        public int? DoctoralLevelId { get; set; } // DoctoralLevelId
        public string DoctoralLevel { get; set; } // DoctoralLevel
        public int Column1ItemTypeId { get; set; } // Column1ItemTypeId
        public string Column1ItemType { get; set; } // Column1ItemType
        public int? Column1ItemId { get; set; } // Column1ItemId
        public string Column1ItemName { get; set; } // Column1ItemName
        public int? TotalParticipatingFaculty { get; set; } // TotalParticipatingFaculty
        public int? TotalPredocMentees { get; set; } // TotalPredocMentees
        public int? TotalPredocMenteesOnAnyTg { get; set; } // TotalPredocMenteesOnAnyTG
        public int? TotalPredocMenteesOnTg { get; set; } // TotalPredocMenteesOnTG
        public int? TotalPredocTgeMenteesOnTg { get; set; } // TotalPredocTGEMenteesOnTG
        public int? TotalPredocUrmMenteesOnTg { get; set; } // TotalPredocURMMenteesOnTG
        public int? TotalPredocDisabilitiesMenteesOnTg { get; set; } // TotalPredocDisabilitiesMenteesOnTG
        public int? TotalPredocDisadvantagedMenteesOnTg { get; set; } // TotalPredocDisadvantagedMenteesOnTG
        public int? TotalPostdocMentees { get; set; } // TotalPostdocMentees
        public int? TotalPostdocMenteesOnAnyTg { get; set; } // TotalPostdocMenteesOnAnyTG
        public int? TotalPostdocMenteesOnTg { get; set; } // TotalPostdocMenteesOnTG
        public int? TotalPostdocTgeMenteesOnTg { get; set; } // TotalPostdocTGEMenteesOnTG
        public int? TotalPostdocUrmMenteesOnTg { get; set; } // TotalPostdocURMMenteesOnTG
        public int? TotalPostdocDisabilitiesMenteesOnTg { get; set; } // TotalPostdocDisabilitiesMenteesOnTG
        public int? TotalPostdocDisadvantagedMenteesOnTg { get; set; } // TotalPostdocDisadvantagedMenteesOnTG
    }

}

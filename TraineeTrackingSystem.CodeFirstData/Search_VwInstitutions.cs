// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_Institutions
    public class Search_VwInstitutions
    {
        public int Id { get; set; } // Id
        public string Name { get; set; } // Name
        public string InstitutionIdentifier { get; set; } // InstitutionIdentifier
        public string City { get; set; } // City
        public int? StateProvinceId { get; set; } // StateProvinceId
        public string StateProvinceAbbreviation { get; set; } // StateProvinceAbbreviation
        public string StateProvinceFullName { get; set; } // StateProvinceFullName
        public int? CountryId { get; set; } // CountryId
        public int InstitutionTypeId { get; set; } // InstitutionTypeId
        public string InstitutionType { get; set; } // InstitutionType
        public string Country { get; set; } // Country
        public int? TotalInstitutionUsage { get; set; } // TotalInstitutionUsage
    }

}

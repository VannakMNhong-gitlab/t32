// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingPeriodPrograms
    internal class Search_VwTrainingPeriodProgramsConfiguration : EntityTypeConfiguration<Search_VwTrainingPeriodPrograms>
    {
        public Search_VwTrainingPeriodProgramsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_TrainingPeriodPrograms");
            HasKey(x => new { x.ProgramId, x.ProgramDisplayId, x.TrainingPeriodId, x.ProgramTitle, x.PersonId, x.DoctoralLevelId });

            Property(x => x.TrainingPeriodId).HasColumnName("TrainingPeriodId").IsRequired();
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.InstitutionId).HasColumnName("InstitutionId").IsOptional();
            Property(x => x.InstitutionIdentifier).HasColumnName("InstitutionIdentifier").IsOptional().HasMaxLength(150);
            Property(x => x.InstitutionName).HasColumnName("InstitutionName").IsOptional().HasMaxLength(250);
            Property(x => x.InstitutionCity).HasColumnName("InstitutionCity").IsOptional().HasMaxLength(150);
            Property(x => x.InstitutionStateId).HasColumnName("InstitutionStateId").IsOptional();
            Property(x => x.InstitutionStateAbbreviation).HasColumnName("InstitutionStateAbbreviation").IsOptional().HasMaxLength(50);
            Property(x => x.InstitutionStateFullName).HasColumnName("InstitutionStateFullName").IsOptional().HasMaxLength(50);
            Property(x => x.InstitutionCountryId).HasColumnName("InstitutionCountryId").IsOptional();
            Property(x => x.InstitutionCountry).HasColumnName("InstitutionCountry").IsOptional().HasMaxLength(150);
            Property(x => x.DoctoralLevelId).HasColumnName("DoctoralLevelId").IsRequired();
            Property(x => x.DoctoralLevel).HasColumnName("DoctoralLevel").IsOptional().HasMaxLength(150);
            Property(x => x.YearStarted).HasColumnName("YearStarted").IsOptional();
            Property(x => x.YearEnded).HasColumnName("YearEnded").IsOptional();
            Property(x => x.AcademicDegreeId).HasColumnName("AcademicDegreeId").IsOptional();
            Property(x => x.AcademicDegree).HasColumnName("AcademicDegree").IsOptional().HasMaxLength(25);
            Property(x => x.DegreeSoughtId).HasColumnName("DegreeSoughtId").IsOptional();
            Property(x => x.DegreeSought).HasColumnName("DegreeSought").IsOptional().HasMaxLength(25);
            Property(x => x.ResearchProjectTitle).HasColumnName("ResearchProjectTitle").IsOptional().HasMaxLength(250);
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.LastUpdatedByName).HasColumnName("LastUpdatedByName").IsOptional().HasMaxLength(129);
            Property(x => x.ProgramId).HasColumnName("ProgramId").IsRequired();
            Property(x => x.ProgramDisplayId).HasColumnName("ProgramDisplayId").IsRequired();
            Property(x => x.ProgramTitle).HasColumnName("ProgramTitle").IsRequired().HasMaxLength(250);
        }
    }

}

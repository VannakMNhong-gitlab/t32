// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_ClinicalFacultyPathways
    internal class Search_VwClinicalFacultyPathwaysConfiguration : EntityTypeConfiguration<Search_VwClinicalFacultyPathways>
    {
        public Search_VwClinicalFacultyPathwaysConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_ClinicalFacultyPathways");
            HasKey(x => new { x.ClinicalFacultyPathwayId, x.ClinicalFacultyPathway });

            Property(x => x.ClinicalFacultyPathwayId).HasColumnName("ClinicalFacultyPathwayId").IsRequired();
            Property(x => x.ClinicalFacultyPathway).HasColumnName("ClinicalFacultyPathway").IsRequired().HasMaxLength(500);
            Property(x => x.TotalClinicalFacultyPathwayUsage).HasColumnName("TotalClinicalFacultyPathwayUsage").IsOptional();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_T32Table8A
    public class Reports_VwT32Table8A
    {
        public Guid TrainingGrantId { get; set; } // TrainingGrantId
        public bool IsRenewal { get; set; } // IsRenewal
        public int? DoctoralLevelId { get; set; } // DoctoralLevelId
        public string DoctoralLevel { get; set; } // DoctoralLevel
        public int Column2ItemTypeId { get; set; } // Column2ItemTypeId
        public string Column2ItemType { get; set; } // Column2ItemType
        public int? Column2ItemId { get; set; } // Column2ItemId
        public string Column2ItemName { get; set; } // Column2ItemName
    }

}

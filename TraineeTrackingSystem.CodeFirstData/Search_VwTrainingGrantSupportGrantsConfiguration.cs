// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingGrantSupportGrants
    internal class Search_VwTrainingGrantSupportGrantsConfiguration : EntityTypeConfiguration<Search_VwTrainingGrantSupportGrants>
    {
        public Search_VwTrainingGrantSupportGrantsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_TrainingGrantSupportGrants");
            HasKey(x => new { x.IsDeleted, x.FundingId, x.Id });

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.FundingId).HasColumnName("FundingId").IsRequired();
            Property(x => x.DoctoralLevelId).HasColumnName("DoctoralLevelId").IsOptional();
            Property(x => x.NumPredocPositionsRequested).HasColumnName("NumPredocPositionsRequested").IsOptional();
            Property(x => x.NumPostdocPositionsRequested).HasColumnName("NumPostdocPositionsRequested").IsOptional();
            Property(x => x.NumPredocPositionsAwarded).HasColumnName("NumPredocPositionsAwarded").IsOptional();
            Property(x => x.NumPostdocPositionsAwarded).HasColumnName("NumPostdocPositionsAwarded").IsOptional();
            Property(x => x.PredocSupportMonthsAwarded).HasColumnName("PredocSupportMonthsAwarded").IsOptional();
            Property(x => x.PostdocSupportMonthsAwarded).HasColumnName("PostdocSupportMonthsAwarded").IsOptional();
            Property(x => x.PreviousTrainingGrantId).HasColumnName("PreviousTrainingGrantId").IsOptional();
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
            Property(x => x.ApplicantPoolAcademicYear).HasColumnName("ApplicantPoolAcademicYear").IsOptional();
            Property(x => x.ProjectedStartDate).HasColumnName("ProjectedStartDate").IsOptional();
        }
    }

}

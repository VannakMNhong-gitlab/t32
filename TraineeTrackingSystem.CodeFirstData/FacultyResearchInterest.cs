// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // FacultyResearchInterest
    public class FacultyResearchInterest
    {
        public int Id { get; set; } // Id (Primary key)
        public Guid PersonId { get; set; } // PersonId
        public string ResearchInterest { get; set; } // ResearchInterest
        public bool IsDeleted { get; set; } // IsDeleted
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy

        // Reverse navigation
        public virtual ICollection<TrainingGrant> TrainingGrants { get; set; } // Many to many mapping

        // Foreign keys
        public virtual Person Person { get; set; } // FK_FacultyResearchInterest_Person

        public FacultyResearchInterest()
        {
            IsDeleted = false;
            TrainingGrants = new List<TrainingGrant>();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_MenteePublications
    public class Search_VwMenteePublications
    {
        public Guid MenteeId { get; set; } // MenteeId
        public Guid MenteePersonId { get; set; } // MenteePersonId
        public string FirstName { get; set; } // FirstName
        public string MiddleName { get; set; } // MiddleName
        public string LastName { get; set; } // LastName
        public string FullName { get; set; } // FullName
        public int DisplayId { get; set; } // DisplayId
        public string StudentId { get; set; } // StudentId
        public bool IsTrainingGrantEligible { get; set; } // IsTrainingGrantEligible
        public DateTime? MenteeDateLastUpdated { get; set; } // MenteeDateLastUpdated
        public string MenteeLastUpdatedBy { get; set; } // MenteeLastUpdatedBy
        public string MenteeLastUpdatedByName { get; set; } // MenteeLastUpdatedByName
        public string Department { get; set; } // Department
        public Guid? PubGuid { get; set; } // Pub_GUID
        public string PubTitle { get; set; } // PubTitle
        public int? PubYearPublished { get; set; } // PubYearPublished
        public string PubAbstract { get; set; } // PubAbstract
        public string PubPmcid { get; set; } // PubPMCID
        public string PubPmid { get; set; } // PubPMID
        public string PubManuscriptId { get; set; } // PubManuscriptId
        public string PubJournal { get; set; } // PubJournal
        public string PubVolume { get; set; } // PubVolume
        public string PubIssue { get; set; } // PubIssue
        public string PubPagination { get; set; } // PubPagination
        public string PubCitation { get; set; } // PubCitation
        public bool? PubIsPublicAccess { get; set; } // PubIsPublicAccess
        public int? PubIsMenteeFirstAuthor { get; set; } // PubIsMenteeFirstAuthor
        public int? PubIsAdvisorCoAuthor { get; set; } // PubIsAdvisorCoAuthor
        public string PubAuthorList { get; set; } // PubAuthorList
        public DateTime? PubDatePublicationLastUpdated { get; set; } // PubDatePublicationLastUpdated
        public DateTime? PubDateLastUpdated { get; set; } // PubDateLastUpdated
        public string PubLastUpdatedBy { get; set; } // PubLastUpdatedBy
        public string PubLastUpdatedByName { get; set; } // PubLastUpdatedByName
    }

}

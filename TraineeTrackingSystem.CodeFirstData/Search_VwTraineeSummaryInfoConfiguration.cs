// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TraineeSummaryInfo
    internal class Search_VwTraineeSummaryInfoConfiguration : EntityTypeConfiguration<Search_VwTraineeSummaryInfo>
    {
        public Search_VwTraineeSummaryInfoConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_TraineeSummaryInfo");
            HasKey(x => new { x.TrainingGrantId, x.DoctoralLevelId, x.TraineeSummaryInfoId, x.TrainingGrantTitle, x.DoctoralLevel });

            Property(x => x.TraineeSummaryInfoId).HasColumnName("TraineeSummaryInfoId").IsRequired();
            Property(x => x.TrainingGrantId).HasColumnName("TrainingGrantId").IsRequired();
            Property(x => x.TrainingGrantTitle).HasColumnName("TrainingGrantTitle").IsRequired();
            Property(x => x.DoctoralLevelId).HasColumnName("DoctoralLevelId").IsRequired();
            Property(x => x.DoctoralLevel).HasColumnName("DoctoralLevel").IsRequired().HasMaxLength(150);
            Property(x => x.DateSummaryStarted).HasColumnName("DateSummaryStarted").IsOptional();
            Property(x => x.DateSummaryEnded).HasColumnName("DateSummaryEnded").IsOptional();
            Property(x => x.PositionsAwarded).HasColumnName("PositionsAwarded").IsOptional();
            Property(x => x.SupportMonthsAwarded).HasColumnName("SupportMonthsAwarded").IsOptional();
            Property(x => x.TraineesAppointed).HasColumnName("TraineesAppointed").IsOptional();
            Property(x => x.SupportMonthsUsed).HasColumnName("SupportMonthsUsed").IsOptional();
            Property(x => x.UrmTraineesAppointed).HasColumnName("URMTraineesAppointed").IsOptional();
            Property(x => x.DisabilitiesTraineesAppointed).HasColumnName("DisabilitiesTraineesAppointed").IsOptional();
            Property(x => x.DisadvantagedTraineesAppointed).HasColumnName("DisadvantagedTraineesAppointed").IsOptional();
            Property(x => x.UrmSupportMonthsUsed).HasColumnName("URMSupportMonthsUsed").IsOptional();
            Property(x => x.DisabilitiesSupportMonthsUsed).HasColumnName("DisabilitiesSupportMonthsUsed").IsOptional();
            Property(x => x.DisadvantagedSupportMonthsUsed).HasColumnName("DisadvantagedSupportMonthsUsed").IsOptional();
            Property(x => x.NumMdAppointed).HasColumnName("NumMDAppointed").IsOptional();
            Property(x => x.NumMdPhDAppointed).HasColumnName("NumMDPhDAppointed").IsOptional();
            Property(x => x.NumPhDAppointed).HasColumnName("NumPhDAppointed").IsOptional();
            Property(x => x.NumOtherDegreeAppointed).HasColumnName("NumOtherDegreeAppointed").IsOptional();
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.LastUpdatedByName).HasColumnName("LastUpdatedByName").IsOptional().HasMaxLength(129);
        }
    }

}

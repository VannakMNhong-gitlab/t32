// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // PersonRoles
    internal class PersonRoleConfiguration : EntityTypeConfiguration<PersonRole>
    {
        public PersonRoleConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".PersonRoles");
            HasKey(x => new { x.LoginId, x.RoleId });

            Property(x => x.LoginId).HasColumnName("LoginId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.RoleId).HasColumnName("RoleId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.IsActive).HasColumnName("IsActive").IsRequired();
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
            Property(x => x.DateAssigned).HasColumnName("DateAssigned").IsRequired();
            Property(x => x.AssignedBy).HasColumnName("AssignedBy").IsRequired().HasMaxLength(50);
            Property(x => x.DateRemoved).HasColumnName("DateRemoved").IsOptional();
            Property(x => x.RemovedBy).HasColumnName("RemovedBy").IsOptional().HasMaxLength(50);
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);

            // Foreign keys
            HasRequired(a => a.Login).WithMany(b => b.PersonRoles).HasForeignKey(c => c.LoginId); // FK_PersonRoles_Logins
            HasRequired(a => a.Role).WithMany(b => b.PersonRoles).HasForeignKey(c => c.RoleId); // FK_PersonRoles_Roles
        }
    }

}

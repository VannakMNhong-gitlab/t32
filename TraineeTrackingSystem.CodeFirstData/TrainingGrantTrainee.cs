// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // TrainingGrantTrainee
    public class TrainingGrantTrainee
    {
        public Guid Id { get; set; } // Id (Primary key)
        public Guid TrainingGrantId { get; set; } // TrainingGrantId
        public Guid MenteeId { get; set; } // MenteeId
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded
        public int? DoctoralLevelId { get; set; } // DoctoralLevelId
        public int? PredocTraineeStatusId { get; set; } // PredocTraineeStatusId
        public string PredocReasonForLeaving { get; set; } // PredocReasonForLeaving
        public int? PostdocTraineeStatusId { get; set; } // PostdocTraineeStatusId
        public string PostdocReasonForLeaving { get; set; } // PostdocReasonForLeaving
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public bool IsDeleted { get; set; } // IsDeleted
        public Guid? TrainingPeriodId { get; set; } // TrainingPeriodId

        // Foreign keys
        public virtual DoctoralLevel DoctoralLevel { get; set; } // FK_TrainingGrantTrainee_DoctoralLevel
        public virtual Mentee Mentee { get; set; } // FK_TrainingGrantTrainee_Mentee
        public virtual TraineeStatu TraineeStatu_PostdocTraineeStatusId { get; set; } // FK_TrainingGrantTrainee_TraineeStatusPostdoc
        public virtual TraineeStatu TraineeStatu_PredocTraineeStatusId { get; set; } // FK_TrainingGrantTrainee_TraineeStatusPredoc
        public virtual TrainingGrant TrainingGrant { get; set; } // FK_TrainingGrantTrainee_TrainingGrant
        public virtual TrainingPeriod TrainingPeriod { get; set; } // FK_TrainingGrantTrainee_TrainingPeriod

        public TrainingGrantTrainee()
        {
            Id = System.Guid.NewGuid();
            IsDeleted = false;
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_T32Table1R
    internal class Reports_VwT32Table1RConfiguration : EntityTypeConfiguration<Reports_VwT32Table1R>
    {
        public Reports_VwT32Table1RConfiguration(string schema = "Reports")
        {
            ToTable(schema + ".vw_T32Table1R");
            HasKey(x => new { x.Column1ItemType, x.TrainingGrantId, x.Column1ItemTypeId, x.IsRenewal });

            Property(x => x.TrainingGrantId).HasColumnName("TrainingGrantId").IsRequired();
            Property(x => x.IsRenewal).HasColumnName("IsRenewal").IsRequired();
            Property(x => x.DoctoralLevelId).HasColumnName("DoctoralLevelId").IsOptional();
            Property(x => x.DoctoralLevel).HasColumnName("DoctoralLevel").IsOptional().HasMaxLength(150);
            Property(x => x.Column1ItemTypeId).HasColumnName("Column1ItemTypeId").IsRequired();
            Property(x => x.Column1ItemType).HasColumnName("Column1ItemType").IsRequired().HasMaxLength(10);
            Property(x => x.Column1ItemId).HasColumnName("Column1ItemId").IsOptional();
            Property(x => x.Column1ItemName).HasColumnName("Column1ItemName").IsOptional().HasMaxLength(250);
            Property(x => x.TotalParticipatingFaculty).HasColumnName("TotalParticipatingFaculty").IsOptional();
            Property(x => x.TotalPredocMentees).HasColumnName("TotalPredocMentees").IsOptional();
            Property(x => x.TotalPredocMenteesOnAnyTg).HasColumnName("TotalPredocMenteesOnAnyTG").IsOptional();
            Property(x => x.TotalPredocMenteesOnTg).HasColumnName("TotalPredocMenteesOnTG").IsOptional();
            Property(x => x.TotalPredocTgeMenteesOnTg).HasColumnName("TotalPredocTGEMenteesOnTG").IsOptional();
            Property(x => x.TotalPredocUrmMenteesOnTg).HasColumnName("TotalPredocURMMenteesOnTG").IsOptional();
            Property(x => x.TotalPredocDisabilitiesMenteesOnTg).HasColumnName("TotalPredocDisabilitiesMenteesOnTG").IsOptional();
            Property(x => x.TotalPredocDisadvantagedMenteesOnTg).HasColumnName("TotalPredocDisadvantagedMenteesOnTG").IsOptional();
            Property(x => x.TotalPostdocMentees).HasColumnName("TotalPostdocMentees").IsOptional();
            Property(x => x.TotalPostdocMenteesOnAnyTg).HasColumnName("TotalPostdocMenteesOnAnyTG").IsOptional();
            Property(x => x.TotalPostdocMenteesOnTg).HasColumnName("TotalPostdocMenteesOnTG").IsOptional();
            Property(x => x.TotalPostdocTgeMenteesOnTg).HasColumnName("TotalPostdocTGEMenteesOnTG").IsOptional();
            Property(x => x.TotalPostdocUrmMenteesOnTg).HasColumnName("TotalPostdocURMMenteesOnTG").IsOptional();
            Property(x => x.TotalPostdocDisabilitiesMenteesOnTg).HasColumnName("TotalPostdocDisabilitiesMenteesOnTG").IsOptional();
            Property(x => x.TotalPostdocDisadvantagedMenteesOnTg).HasColumnName("TotalPostdocDisadvantagedMenteesOnTG").IsOptional();
            Property(x => x.TotalPredocTrainees).HasColumnName("TotalPredocTrainees").IsOptional();
            Property(x => x.TotalPredocTgeTrainees).HasColumnName("TotalPredocTGETrainees").IsOptional();
            Property(x => x.TotalPredocUrmTrainees).HasColumnName("TotalPredocURMTrainees").IsOptional();
            Property(x => x.TotalPredocDisabilitiesTrainees).HasColumnName("TotalPredocDisabilitiesTrainees").IsOptional();
            Property(x => x.TotalPredocDisadvantagedTrainees).HasColumnName("TotalPredocDisadvantagedTrainees").IsOptional();
            Property(x => x.TotalPostdocTrainees).HasColumnName("TotalPostdocTrainees").IsOptional();
            Property(x => x.TotalPostdocTgeTrainees).HasColumnName("TotalPostdocTGETrainees").IsOptional();
            Property(x => x.TotalPostdocUrmTrainees).HasColumnName("TotalPostdocURMTrainees").IsOptional();
            Property(x => x.TotalPostdocDisabilitiesTrainees).HasColumnName("TotalPostdocDisabilitiesTrainees").IsOptional();
            Property(x => x.TotalPostdocDisadvantagedTrainees).HasColumnName("TotalPostdocDisadvantagedTrainees").IsOptional();
        }
    }

}

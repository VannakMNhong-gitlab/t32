// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // TraineeSummaryInfo
    internal class TraineeSummaryInfoConfiguration : EntityTypeConfiguration<TraineeSummaryInfo>
    {
        public TraineeSummaryInfoConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".TraineeSummaryInfo");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.TrainingGrantId).HasColumnName("TrainingGrantId").IsRequired();
            Property(x => x.DoctoralLevelId).HasColumnName("DoctoralLevelId").IsRequired();
            Property(x => x.DateStarted).HasColumnName("DateStarted").IsOptional();
            Property(x => x.DateEnded).HasColumnName("DateEnded").IsOptional();
            Property(x => x.PositionsAwarded).HasColumnName("PositionsAwarded").IsOptional();
            Property(x => x.SupportMonthsAwarded).HasColumnName("SupportMonthsAwarded").IsOptional();
            Property(x => x.TraineesAppointed).HasColumnName("TraineesAppointed").IsOptional();
            Property(x => x.SupportMonthsUsed).HasColumnName("SupportMonthsUsed").IsOptional();
            Property(x => x.UrmTraineesAppointed).HasColumnName("URMTraineesAppointed").IsOptional();
            Property(x => x.DisabilitiesTraineesAppointed).HasColumnName("DisabilitiesTraineesAppointed").IsOptional();
            Property(x => x.DisadvantagedTraineesAppointed).HasColumnName("DisadvantagedTraineesAppointed").IsOptional();
            Property(x => x.UrmSupportMonthsUsed).HasColumnName("URMSupportMonthsUsed").IsOptional();
            Property(x => x.DisabilitiesSupportMonthsUsed).HasColumnName("DisabilitiesSupportMonthsUsed").IsOptional();
            Property(x => x.DisadvantagedSupportMonthsUsed).HasColumnName("DisadvantagedSupportMonthsUsed").IsOptional();
            Property(x => x.NumMdAppointed).HasColumnName("NumMDAppointed").IsOptional();
            Property(x => x.NumMdPhDAppointed).HasColumnName("NumMDPhDAppointed").IsOptional();
            Property(x => x.NumPhDAppointed).HasColumnName("NumPhDAppointed").IsOptional();
            Property(x => x.NumOtherDegreeAppointed).HasColumnName("NumOtherDegreeAppointed").IsOptional();
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();

            // Foreign keys
            HasRequired(a => a.TrainingGrant).WithMany(b => b.TraineeSummaryInfoes).HasForeignKey(c => c.TrainingGrantId); // FK_TraineeSummaryInfo_TrainingGrant
            HasRequired(a => a.DoctoralLevel).WithMany(b => b.TraineeSummaryInfoes).HasForeignKey(c => c.DoctoralLevelId); // FK_TraineeSummaryInfo_DoctoralLevel
        }
    }

}

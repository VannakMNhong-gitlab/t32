// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_PersonDemographics
    public class Search_VwPersonDemographics
    {
        public Guid PersonId { get; set; } // PersonId
        public int DisplayId { get; set; } // DisplayId
        public string FirstName { get; set; } // FirstName
        public string MiddleName { get; set; } // MiddleName
        public string LastName { get; set; } // LastName
        public string FullName { get; set; } // FullName
        public int? IsUnderrepresentedMinority { get; set; } // IsUnderrepresentedMinority
        public int? IsIndividualWithDisabilities { get; set; } // IsIndividualWithDisabilities
        public int? IsFromDisadvantagedBkgd { get; set; } // IsFromDisadvantagedBkgd
        public int? GenderId { get; set; } // GenderId
        public string Gender { get; set; } // Gender
        public int? GenderAtBirthId { get; set; } // GenderAtBirthId
        public string GenderAtBirth { get; set; } // GenderAtBirth
        public int? EthnicityId { get; set; } // EthnicityId
        public string Ethnicity { get; set; } // Ethnicity
        public DateTime DateCreated { get; set; } // DateCreated
        public string EmployeeId { get; set; } // EmployeeId
        public string StudentId { get; set; } // StudentId
        public int? ApplicantId { get; set; } // ApplicantId
        public int IsFaculty { get; set; } // IsFaculty
        public int IsMentee { get; set; } // IsMentee
        public int IsApplicant { get; set; } // IsApplicant
        public string PrimaryEmailAddress { get; set; } // PrimaryEmailAddress
        public string EmailAddress { get; set; } // EmailAddress
        public string MailingAddress { get; set; } // MailingAddress
        public string AddressLine1 { get; set; } // AddressLine1
        public string AddressLine2 { get; set; } // AddressLine2
        public string AddressLine3 { get; set; } // AddressLine3
        public string AddressCity { get; set; } // AddressCity
        public int? AddressStateId { get; set; } // AddressStateId
        public string AddressStateProvince { get; set; } // AddressStateProvince
        public string AddressPostalCode { get; set; } // AddressPostalCode
        public string PhoneNumber { get; set; } // PhoneNumber
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_UsageOfCountries
    public class Search_VwUsageOfCountries
    {
        public int CountryId { get; set; } // CountryId
        public string CountryName { get; set; } // CountryName
        public int? TotalContactAddress { get; set; } // TotalContactAddress
        public int? TotalInstitutions { get; set; } // TotalInstitutions
        public int? TotalStateProvince { get; set; } // TotalStateProvince
    }

}

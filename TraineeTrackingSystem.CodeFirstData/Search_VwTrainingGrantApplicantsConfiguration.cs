// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingGrantApplicants
    internal class Search_VwTrainingGrantApplicantsConfiguration : EntityTypeConfiguration<Search_VwTrainingGrantApplicants>
    {
        public Search_VwTrainingGrantApplicantsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_TrainingGrantApplicants");
            HasKey(x => new { x.TrainingGrantId, x.ApplicantId, x.FundingId, x.ApplicantGuid, x.ApplicantPersonId });

            Property(x => x.TrainingGrantId).HasColumnName("TrainingGrantId").IsRequired();
            Property(x => x.FundingId).HasColumnName("FundingId").IsRequired();
            Property(x => x.Title).HasColumnName("Title").IsOptional();
            Property(x => x.ApplicantGuid).HasColumnName("ApplicantGUID").IsRequired();
            Property(x => x.ApplicantPersonId).HasColumnName("ApplicantPersonId").IsRequired();
            Property(x => x.ApplicantId).HasColumnName("ApplicantId").IsRequired();
            Property(x => x.ApplicantTypeId).HasColumnName("ApplicantTypeId").IsOptional();
            Property(x => x.ApplicantType).HasColumnName("ApplicantType").IsOptional().HasMaxLength(150);
            Property(x => x.ApplicantDeptId).HasColumnName("ApplicantDeptId").IsOptional();
            Property(x => x.ApplicantDept).HasColumnName("ApplicantDept").IsOptional().HasMaxLength(250);
            Property(x => x.YearEntered).HasColumnName("YearEntered").IsOptional();
            Property(x => x.ApplicantPrograms).HasColumnName("ApplicantPrograms").IsOptional();
            Property(x => x.ApplicantPoolAcademicYear).HasColumnName("ApplicantPoolAcademicYear").IsOptional();
        }
    }

}

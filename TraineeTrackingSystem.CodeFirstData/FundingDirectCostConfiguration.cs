// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // FundingDirectCost
    internal class FundingDirectCostConfiguration : EntityTypeConfiguration<FundingDirectCost>
    {
        public FundingDirectCostConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".FundingDirectCost");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.FundingId).HasColumnName("FundingId").IsRequired();
            Property(x => x.CostYear).HasColumnName("CostYear").IsOptional();
            Property(x => x.DateStarted).HasColumnName("DateStarted").IsOptional();
            Property(x => x.DateEnded).HasColumnName("DateEnded").IsOptional();
            Property(x => x.CurrentYearDirectCosts).HasColumnName("CurrentYearDirectCosts").IsOptional().HasPrecision(10,2);
            Property(x => x.TotalDirectCosts).HasColumnName("TotalDirectCosts").IsOptional().HasPrecision(10,2);
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
            Property(x => x.BudgetPeriodStatusId).HasColumnName("BudgetPeriodStatusId").IsOptional();

            // Foreign keys
            HasRequired(a => a.Funding).WithMany(b => b.FundingDirectCosts).HasForeignKey(c => c.FundingId); // FK_FundingDirectCost_Funding
            HasOptional(a => a.BudgetPeriodStatu).WithMany(b => b.FundingDirectCosts).HasForeignKey(c => c.BudgetPeriodStatusId); // FK_FundingDirectCost_BudgetPeriodStatus
        }
    }

}

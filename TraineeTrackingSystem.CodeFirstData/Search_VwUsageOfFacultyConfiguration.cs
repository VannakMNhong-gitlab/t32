// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_UsageOfFaculty
    internal class Search_VwUsageOfFacultyConfiguration : EntityTypeConfiguration<Search_VwUsageOfFaculty>
    {
        public Search_VwUsageOfFacultyConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_UsageOfFaculty");
            HasKey(x => new { x.FacultyId, x.PersonId });

            Property(x => x.FacultyId).HasColumnName("FacultyId").IsRequired();
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.LastName).HasColumnName("LastName").IsOptional().HasMaxLength(75);
            Property(x => x.FirstName).HasColumnName("FirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MiddleName).HasColumnName("MiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.TotalFacultyAcademics).HasColumnName("TotalFacultyAcademics").IsOptional();
            Property(x => x.TotalFacultyPrograms).HasColumnName("TotalFacultyPrograms").IsOptional();
            Property(x => x.TotalFacultyResearchInterests).HasColumnName("TotalFacultyResearchInterests").IsOptional();
            Property(x => x.TotalFundings).HasColumnName("TotalFundings").IsOptional();
            Property(x => x.TotalTrainingGrants).HasColumnName("TotalTrainingGrants").IsOptional();
            Property(x => x.TotalTrainingPeriods).HasColumnName("TotalTrainingPeriods").IsOptional();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FacultyDemographics
    internal class Search_VwFacultyDemographicsConfiguration : EntityTypeConfiguration<Search_VwFacultyDemographics>
    {
        public Search_VwFacultyDemographicsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_FacultyDemographics");
            HasKey(x => new { x.DisplayId, x.Id, x.PersonId, x.EmployeeId, x.IsActive });

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.FirstName).HasColumnName("FirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MiddleName).HasColumnName("MiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.LastName).HasColumnName("LastName").IsOptional().HasMaxLength(75);
            Property(x => x.FullName).HasColumnName("FullName").IsOptional().HasMaxLength(178);
            Property(x => x.DisplayId).HasColumnName("DisplayId").IsRequired();
            Property(x => x.EmployeeId).HasColumnName("EmployeeId").IsRequired().HasMaxLength(50);
            Property(x => x.Rank).HasColumnName("Rank").IsOptional().HasMaxLength(150);
            Property(x => x.PositionDepartmentId).HasColumnName("PositionDepartmentId").IsOptional();
            Property(x => x.PositionDepartment).HasColumnName("PositionDepartment").IsOptional().HasMaxLength(250);
            Property(x => x.PrimaryOrganizationId).HasColumnName("PrimaryOrganizationId").IsOptional();
            Property(x => x.PrimaryOrganization).HasColumnName("PrimaryOrganization").IsOptional().HasMaxLength(250);
            Property(x => x.PositionOrganizationId).HasColumnName("PositionOrganizationId").IsOptional();
            Property(x => x.PositionOrganization).HasColumnName("PositionOrganization").IsOptional().HasMaxLength(250);
            Property(x => x.SecondaryOrganization).HasColumnName("SecondaryOrganization").IsOptional().HasMaxLength(250);
            Property(x => x.MentorId).HasColumnName("MentorId").IsOptional();
            Property(x => x.MentorFirstName).HasColumnName("MentorFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MentorMiddleName).HasColumnName("MentorMiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.MentorLastName).HasColumnName("MentorLastName").IsOptional().HasMaxLength(75);
            Property(x => x.MentorFullName).HasColumnName("MentorFullName").IsOptional().HasMaxLength(178);
            Property(x => x.OtherAffiliations).HasColumnName("OtherAffiliations").IsOptional().HasMaxLength(1000);
            Property(x => x.OtherTitles).HasColumnName("OtherTitles").IsOptional().HasMaxLength(250);
            Property(x => x.IsActive).HasColumnName("IsActive").IsRequired();
            Property(x => x.GenderId).HasColumnName("GenderId").IsOptional();
            Property(x => x.Gender).HasColumnName("Gender").IsOptional().HasMaxLength(100);
            Property(x => x.GenderAtBirthId).HasColumnName("GenderAtBirthId").IsOptional();
            Property(x => x.GenderAtBirth).HasColumnName("GenderAtBirth").IsOptional().HasMaxLength(100);
            Property(x => x.EthnicityId).HasColumnName("EthnicityId").IsOptional();
            Property(x => x.Ethnicity).HasColumnName("Ethnicity").IsOptional().HasMaxLength(300);
            Property(x => x.FacultyDateLastUpdated).HasColumnName("FacultyDateLastUpdated").IsOptional();
            Property(x => x.FacultyLastUpdatedBy).HasColumnName("FacultyLastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.FacultyLastUpdatedByName).HasColumnName("FacultyLastUpdatedByName").IsOptional().HasMaxLength(129);
        }
    }

}

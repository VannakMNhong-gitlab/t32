// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FacultyFundingWithDirectCosts
    internal class Search_VwFacultyFundingWithDirectCostsConfiguration : EntityTypeConfiguration<Search_VwFacultyFundingWithDirectCosts>
    {
        public Search_VwFacultyFundingWithDirectCostsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_FacultyFundingWithDirectCosts");
            HasKey(x => new { x.FacultyId, x.FacultyPersonId, x.FundingId, x.StatusId });

            Property(x => x.FacultyId).HasColumnName("FacultyId").IsRequired();
            Property(x => x.FacultyPersonId).HasColumnName("FacultyPersonId").IsRequired();
            Property(x => x.FacultyLastName).HasColumnName("FacultyLastName").IsOptional().HasMaxLength(75);
            Property(x => x.FacultyFirstName).HasColumnName("FacultyFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.FacultyFullName).HasColumnName("FacultyFullName").IsOptional().HasMaxLength(178);
            Property(x => x.FacultyRoleId).HasColumnName("FacultyRoleId").IsOptional();
            Property(x => x.FacultyRole).HasColumnName("FacultyRole").IsOptional().HasMaxLength(150);
            Property(x => x.FundingId).HasColumnName("FundingId").IsRequired();
            Property(x => x.TrainingGrantId).HasColumnName("TrainingGrantId").IsOptional();
            Property(x => x.Title).HasColumnName("Title").IsOptional();
            Property(x => x.StatusId).HasColumnName("StatusId").IsRequired();
            Property(x => x.StatusName).HasColumnName("StatusName").IsOptional().HasMaxLength(150);
            Property(x => x.DateProjectedStart).HasColumnName("DateProjectedStart").IsOptional();
            Property(x => x.DateProjectedEnd).HasColumnName("DateProjectedEnd").IsOptional();
            Property(x => x.SponsorId).HasColumnName("SponsorId").IsOptional();
            Property(x => x.SponsorName).HasColumnName("SponsorName").IsOptional().HasMaxLength(250);
            Property(x => x.SponsorAwardNumber).HasColumnName("SponsorAwardNumber").IsOptional().HasMaxLength(50);
            Property(x => x.DateStarted).HasColumnName("DateStarted").IsOptional();
            Property(x => x.DateEnded).HasColumnName("DateEnded").IsOptional();
            Property(x => x.GrtNumber).HasColumnName("GRTNumber").IsOptional().HasMaxLength(20);
            Property(x => x.PrimeAward).HasColumnName("PrimeAward").IsOptional().HasMaxLength(250);
            Property(x => x.CostYear).HasColumnName("CostYear").IsOptional();
            Property(x => x.DateCostsStarted).HasColumnName("DateCostsStarted").IsOptional();
            Property(x => x.DateCostsEnded).HasColumnName("DateCostsEnded").IsOptional();
            Property(x => x.BudgetPeriodStatusId).HasColumnName("BudgetPeriodStatusId").IsOptional();
            Property(x => x.BudgetPeriodStatus).HasColumnName("BudgetPeriodStatus").IsOptional().HasMaxLength(150);
            Property(x => x.CurrentYearDirectCosts).HasColumnName("CurrentYearDirectCosts").IsOptional().HasPrecision(10,2);
            Property(x => x.TotalDirectCosts).HasColumnName("TotalDirectCosts").IsOptional().HasPrecision(10,2);
            Property(x => x.CostsDateLastUpdated).HasColumnName("CostsDateLastUpdated").IsOptional();
            Property(x => x.CostsLastUpdatedBy).HasColumnName("CostsLastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.CostsLastUpdatedByName).HasColumnName("CostsLastUpdatedByName").IsOptional().HasMaxLength(129);
        }
    }

}

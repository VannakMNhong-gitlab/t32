// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    public interface ITTSEntities : IDisposable
    {
        IDbSet<AcademicDegree> AcademicDegrees { get; set; } // AcademicDegree
        IDbSet<AcademicHistory> AcademicHistories { get; set; } // AcademicHistory
        IDbSet<Applicant> Applicants { get; set; } // Applicant
        IDbSet<BudgetPeriodStatu> BudgetPeriodStatus { get; set; } // BudgetPeriodStatus
        IDbSet<ClinicalFacultyPathway> ClinicalFacultyPathways { get; set; } // ClinicalFacultyPathway
        IDbSet<ContactAddress> ContactAddresses { get; set; } // ContactAddress
        IDbSet<ContactEmail> ContactEmails { get; set; } // ContactEmail
        IDbSet<ContactEntityType> ContactEntityTypes { get; set; } // ContactEntityType
        IDbSet<ContactPhone> ContactPhones { get; set; } // ContactPhone
        IDbSet<Country> Countries { get; set; } // Country
        IDbSet<DoctoralLevel> DoctoralLevels { get; set; } // DoctoralLevel
        IDbSet<Ethnicity> Ethnicities { get; set; } // Ethnicity
        IDbSet<Faculty> Faculties { get; set; } // Faculty
        IDbSet<FacultyAcademicData> FacultyAcademicDatas { get; set; } // FacultyAcademicData
        IDbSet<FacultyAppointment> FacultyAppointments { get; set; } // FacultyAppointment
        IDbSet<FacultyRank> FacultyRanks { get; set; } // FacultyRank
        IDbSet<FacultyResearchInterest> FacultyResearchInterests { get; set; } // FacultyResearchInterest
        IDbSet<FacultyTrack> FacultyTracks { get; set; } // FacultyTrack
        IDbSet<Funding> Fundings { get; set; } // Funding
        IDbSet<FundingDirectCost> FundingDirectCosts { get; set; } // FundingDirectCost
        IDbSet<FundingFaculty> FundingFaculties { get; set; } // FundingFaculty
        IDbSet<FundingProgramProject> FundingProgramProjects { get; set; } // FundingProgramProjects
        IDbSet<FundingRole> FundingRoles { get; set; } // FundingRole
        IDbSet<FundingStatu> FundingStatus { get; set; } // FundingStatus
        IDbSet<FundingType> FundingTypes { get; set; } // FundingType
        IDbSet<Gender> Genders { get; set; } // Gender
        IDbSet<GradeRecord> GradeRecords { get; set; } // GradeRecord
        IDbSet<Institution> Institutions { get; set; } // Institution
        IDbSet<InstitutionAssociation> InstitutionAssociations { get; set; } // InstitutionAssociation
        IDbSet<InstitutionType> InstitutionTypes { get; set; } // InstitutionType
        IDbSet<LeaveOfAbsence> LeaveOfAbsences { get; set; } // LeaveOfAbsence
        IDbSet<Login> Logins { get; set; } // Logins
        IDbSet<MedicalRank> MedicalRanks { get; set; } // MedicalRank
        IDbSet<Mentee> Mentees { get; set; } // Mentee
        IDbSet<MenteeSupport> MenteeSupports { get; set; } // MenteeSupport
        IDbSet<Organization> Organizations { get; set; } // Organization
        IDbSet<OrganizationType> OrganizationTypes { get; set; } // OrganizationType
        IDbSet<OsuTimeline> OsuTimelines { get; set; } // OSUTimeline
        IDbSet<Person> People { get; set; } // Person
        IDbSet<PersonRole> PersonRoles { get; set; } // PersonRoles
        IDbSet<Program> Programs { get; set; } // Program
        IDbSet<Publication> Publications { get; set; } // Publication
        IDbSet<Report> Reports { get; set; } // Reports
        IDbSet<Reports_VwT32Table10> Reports_VwT32Table10 { get; set; } // vw_T32Table10
        IDbSet<Reports_VwT32Table1N> Reports_VwT32Table1N { get; set; } // vw_T32Table1N
        IDbSet<Reports_VwT32Table1R> Reports_VwT32Table1R { get; set; } // vw_T32Table1R
        IDbSet<Reports_VwT32Table3> Reports_VwT32Table3 { get; set; } // vw_T32Table3
        IDbSet<Reports_VwT32Table4> Reports_VwT32Table4 { get; set; } // vw_T32Table4
        IDbSet<Reports_VwT32Table5A> Reports_VwT32Table5A { get; set; } // vw_T32Table5A
        IDbSet<Reports_VwT32Table5B> Reports_VwT32Table5B { get; set; } // vw_T32Table5B
        IDbSet<Reports_VwT32Table7A> Reports_VwT32Table7A { get; set; } // vw_T32Table7A
        IDbSet<Reports_VwT32Table7B> Reports_VwT32Table7B { get; set; } // vw_T32Table7B
        IDbSet<Reports_VwT32Table8A> Reports_VwT32Table8A { get; set; } // vw_T32Table8A
        IDbSet<Reports_VwT32Table8B> Reports_VwT32Table8B { get; set; } // vw_T32Table8B
        IDbSet<Reports_VwT32Table9A> Reports_VwT32Table9A { get; set; } // vw_T32Table9A
        IDbSet<Reports_VwT32Table9B> Reports_VwT32Table9B { get; set; } // vw_T32Table9B
        IDbSet<Reports_VwTrainingGrantTotals> Reports_VwTrainingGrantTotals { get; set; } // vw_TrainingGrantTotals
        IDbSet<ReportType> ReportTypes { get; set; } // ReportTypes
        IDbSet<Role> Roles { get; set; } // Roles
        IDbSet<Search_VwAcademicDegrees> Search_VwAcademicDegrees { get; set; } // vw_AcademicDegrees
        IDbSet<Search_VwApplicantAcademicHistory> Search_VwApplicantAcademicHistory { get; set; } // vw_ApplicantAcademicHistory
        IDbSet<Search_VwApplicantData> Search_VwApplicantData { get; set; } // vw_ApplicantData
        IDbSet<Search_VwApplicantDemographics> Search_VwApplicantDemographics { get; set; } // vw_ApplicantDemographics
        IDbSet<Search_VwApplicantPrograms> Search_VwApplicantPrograms { get; set; } // vw_ApplicantPrograms
        IDbSet<Search_VwClinicalFacultyPathways> Search_VwClinicalFacultyPathways { get; set; } // vw_ClinicalFacultyPathways
        IDbSet<Search_VwCountries> Search_VwCountries { get; set; } // vw_Countries
        IDbSet<Search_VwEthnicities> Search_VwEthnicities { get; set; } // vw_Ethnicities
        IDbSet<Search_VwFacultyAcademicData> Search_VwFacultyAcademicData { get; set; } // vw_FacultyAcademicData
        IDbSet<Search_VwFacultyAcademicHistory> Search_VwFacultyAcademicHistory { get; set; } // vw_FacultyAcademicHistory
        IDbSet<Search_VwFacultyAppointments> Search_VwFacultyAppointments { get; set; } // vw_FacultyAppointments
        IDbSet<Search_VwFacultyData> Search_VwFacultyData { get; set; } // vw_FacultyData
        IDbSet<Search_VwFacultyDemographics> Search_VwFacultyDemographics { get; set; } // vw_FacultyDemographics
        IDbSet<Search_VwFacultyFundingWithDirectCosts> Search_VwFacultyFundingWithDirectCosts { get; set; } // vw_FacultyFundingWithDirectCosts
        IDbSet<Search_VwFacultyGrantsAndFunding> Search_VwFacultyGrantsAndFunding { get; set; } // vw_FacultyGrantsAndFunding
        IDbSet<Search_VwFacultyMentees> Search_VwFacultyMentees { get; set; } // vw_FacultyMentees
        IDbSet<Search_VwFacultyPrograms> Search_VwFacultyPrograms { get; set; } // vw_FacultyPrograms
        IDbSet<Search_VwFacultyRanks> Search_VwFacultyRanks { get; set; } // vw_FacultyRanks
        IDbSet<Search_VwFacultyResearchInterests> Search_VwFacultyResearchInterests { get; set; } // vw_FacultyResearchInterests
        IDbSet<Search_VwFacultyTracks> Search_VwFacultyTracks { get; set; } // vw_FacultyTracks
        IDbSet<Search_VwFundingCostInformation> Search_VwFundingCostInformation { get; set; } // vw_FundingCostInformation
        IDbSet<Search_VwFundingDemographics> Search_VwFundingDemographics { get; set; } // vw_FundingDemographics
        IDbSet<Search_VwFundingFaculty> Search_VwFundingFaculty { get; set; } // vw_FundingFaculty
        IDbSet<Search_VwFundingProgramProjectData> Search_VwFundingProgramProjectData { get; set; } // vw_FundingProgramProjectData
        IDbSet<Search_VwFundingProgramProjects> Search_VwFundingProgramProjects { get; set; } // vw_FundingProgramProjects
        IDbSet<Search_VwFundingRoles> Search_VwFundingRoles { get; set; } // vw_FundingRoles
        IDbSet<Search_VwFundingSubAwardData> Search_VwFundingSubAwardData { get; set; } // vw_FundingSubAwardData
        IDbSet<Search_VwGenders> Search_VwGenders { get; set; } // vw_Genders
        IDbSet<Search_VwGeneratedIDs> Search_VwGeneratedIDs { get; set; } // vw_GeneratedIDs
        IDbSet<Search_VwInstitutions> Search_VwInstitutions { get; set; } // vw_Institutions
        IDbSet<Search_VwLogins> Search_VwLogins { get; set; } // vw_Logins
        IDbSet<Search_VwMenteeAcademicData> Search_VwMenteeAcademicData { get; set; } // vw_MenteeAcademicData
        IDbSet<Search_VwMenteeData> Search_VwMenteeData { get; set; } // vw_MenteeData
        IDbSet<Search_VwMenteeDemographics> Search_VwMenteeDemographics { get; set; } // vw_MenteeDemographics
        IDbSet<Search_VwMenteeMentors> Search_VwMenteeMentors { get; set; } // vw_MenteeMentors
        IDbSet<Search_VwMenteePublications> Search_VwMenteePublications { get; set; } // vw_MenteePublications
        IDbSet<Search_VwMenteeTrainingPeriods> Search_VwMenteeTrainingPeriods { get; set; } // vw_MenteeTrainingPeriods
        IDbSet<Search_VwMenteeWorkHistory> Search_VwMenteeWorkHistory { get; set; } // vw_MenteeWorkHistory
        IDbSet<Search_VwOrganizations> Search_VwOrganizations { get; set; } // vw_Organizations
        IDbSet<Search_VwOrganizationTypes> Search_VwOrganizationTypes { get; set; } // vw_OrganizationTypes
        IDbSet<Search_VwPerson> Search_VwPerson { get; set; } // vw_Person
        IDbSet<Search_VwPersonDemographics> Search_VwPersonDemographics { get; set; } // vw_PersonDemographics
        IDbSet<Search_VwPersonOrphans> Search_VwPersonOrphans { get; set; } // vw_PersonOrphans
        IDbSet<Search_VwPersonRoles> Search_VwPersonRoles { get; set; } // vw_PersonRoles
        IDbSet<Search_VwPrograms> Search_VwPrograms { get; set; } // vw_Programs
        IDbSet<Search_VwReports> Search_VwReports { get; set; } // vw_Reports
        IDbSet<Search_VwStateProvinces> Search_VwStateProvinces { get; set; } // vw_StateProvinces
        IDbSet<Search_VwSupportGeneralData> Search_VwSupportGeneralData { get; set; } // vw_SupportGeneralData
        IDbSet<Search_VwSupportOrganizations> Search_VwSupportOrganizations { get; set; } // vw_SupportOrganizations
        IDbSet<Search_VwSupportTypes> Search_VwSupportTypes { get; set; } // vw_SupportTypes
        IDbSet<Search_VwTraineeCurrentSummaryInfo> Search_VwTraineeCurrentSummaryInfo { get; set; } // vw_TraineeCurrentSummaryInfo
        IDbSet<Search_VwTraineeSummaryInfo> Search_VwTraineeSummaryInfo { get; set; } // vw_TraineeSummaryInfo
        IDbSet<Search_VwTrainingGrantApplicants> Search_VwTrainingGrantApplicants { get; set; } // vw_TrainingGrantApplicants
        IDbSet<Search_VwTrainingGrantCostInformation> Search_VwTrainingGrantCostInformation { get; set; } // vw_TrainingGrantCostInformation
        IDbSet<Search_VwTrainingGrantDepartments> Search_VwTrainingGrantDepartments { get; set; } // vw_TrainingGrantDepartments
        IDbSet<Search_VwTrainingGrantDistinctTrainees> Search_VwTrainingGrantDistinctTrainees { get; set; } // vw_TrainingGrantDistinctTrainees
        IDbSet<Search_VwTrainingGrantFaculty> Search_VwTrainingGrantFaculty { get; set; } // vw_TrainingGrantFaculty
        IDbSet<Search_VwTrainingGrantFacultyWithPrograms> Search_VwTrainingGrantFacultyWithPrograms { get; set; } // vw_TrainingGrantFacultyWithPrograms
        IDbSet<Search_VwTrainingGrantFacultyWithResearchInterests> Search_VwTrainingGrantFacultyWithResearchInterests { get; set; } // vw_TrainingGrantFacultyWithResearchInterests
        IDbSet<Search_VwTrainingGrantGeneralData> Search_VwTrainingGrantGeneralData { get; set; } // vw_TrainingGrantGeneralData
        IDbSet<Search_VwTrainingGrantMentees> Search_VwTrainingGrantMentees { get; set; } // vw_TrainingGrantMentees
        IDbSet<Search_VwTrainingGrantMenteesWithPrograms> Search_VwTrainingGrantMenteesWithPrograms { get; set; } // vw_TrainingGrantMenteesWithPrograms
        IDbSet<Search_VwTrainingGrantPrograms> Search_VwTrainingGrantPrograms { get; set; } // vw_TrainingGrantPrograms
        IDbSet<Search_VwTrainingGrantSupportGrants> Search_VwTrainingGrantSupportGrants { get; set; } // vw_TrainingGrantSupportGrants
        IDbSet<Search_VwTrainingGrantTrainees> Search_VwTrainingGrantTrainees { get; set; } // vw_TrainingGrantTrainees
        IDbSet<Search_VwTrainingPeriodMentors> Search_VwTrainingPeriodMentors { get; set; } // vw_TrainingPeriodMentors
        IDbSet<Search_VwTrainingPeriodPrograms> Search_VwTrainingPeriodPrograms { get; set; } // vw_TrainingPeriodPrograms
        IDbSet<Search_VwUsageOfAcademicDegrees> Search_VwUsageOfAcademicDegrees { get; set; } // vw_UsageOfAcademicDegrees
        IDbSet<Search_VwUsageOfClinicalFacultyPathways> Search_VwUsageOfClinicalFacultyPathways { get; set; } // vw_UsageOfClinicalFacultyPathways
        IDbSet<Search_VwUsageOfCountries> Search_VwUsageOfCountries { get; set; } // vw_UsageOfCountries
        IDbSet<Search_VwUsageOfEthnicities> Search_VwUsageOfEthnicities { get; set; } // vw_UsageOfEthnicities
        IDbSet<Search_VwUsageOfFaculty> Search_VwUsageOfFaculty { get; set; } // vw_UsageOfFaculty
        IDbSet<Search_VwUsageOfFacultyRanks> Search_VwUsageOfFacultyRanks { get; set; } // vw_UsageOfFacultyRanks
        IDbSet<Search_VwUsageOfFacultyTracks> Search_VwUsageOfFacultyTracks { get; set; } // vw_UsageOfFacultyTracks
        IDbSet<Search_VwUsageOfFundingRoles> Search_VwUsageOfFundingRoles { get; set; } // vw_UsageOfFundingRoles
        IDbSet<Search_VwUsageOfGenders> Search_VwUsageOfGenders { get; set; } // vw_UsageOfGenders
        IDbSet<Search_VwUsageOfInstitutions> Search_VwUsageOfInstitutions { get; set; } // vw_UsageOfInstitutions
        IDbSet<Search_VwUsageOfOrganizations> Search_VwUsageOfOrganizations { get; set; } // vw_UsageOfOrganizations
        IDbSet<Search_VwUsageOfOrganizationTypes> Search_VwUsageOfOrganizationTypes { get; set; } // vw_UsageOfOrganizationTypes
        IDbSet<Search_VwUsageOfPrograms> Search_VwUsageOfPrograms { get; set; } // vw_UsageOfPrograms
        IDbSet<Search_VwUsageOfStateProvinces> Search_VwUsageOfStateProvinces { get; set; } // vw_UsageOfStateProvinces
        IDbSet<StateProvince> StateProvinces { get; set; } // StateProvince
        IDbSet<StatusType> StatusTypes { get; set; } // StatusTypes
        IDbSet<Support> Supports { get; set; } // Support
        IDbSet<SupportOrganization> SupportOrganizations { get; set; } // SupportOrganization
        IDbSet<SupportSource> SupportSources { get; set; } // SupportSource
        IDbSet<SupportStatu> SupportStatus { get; set; } // SupportStatus
        IDbSet<SupportType> SupportTypes { get; set; } // SupportType
        IDbSet<Sysdiagram> Sysdiagrams { get; set; } // sysdiagrams
        IDbSet<TestScoreType> TestScoreTypes { get; set; } // TestScoreType
        IDbSet<TraineeStatu> TraineeStatus { get; set; } // TraineeStatus
        IDbSet<TraineeSummaryInfo> TraineeSummaryInfoes { get; set; } // TraineeSummaryInfo
        IDbSet<TrainingGrant> TrainingGrants { get; set; } // TrainingGrant
        IDbSet<TrainingGrantStatu> TrainingGrantStatus { get; set; } // TrainingGrantStatus
        IDbSet<TrainingGrantSupportFunding> TrainingGrantSupportFundings { get; set; } // TrainingGrantSupportFunding
        IDbSet<TrainingGrantSupportGrant> TrainingGrantSupportGrants { get; set; } // TrainingGrantSupportGrant
        IDbSet<TrainingGrantTrainee> TrainingGrantTrainees { get; set; } // TrainingGrantTrainee
        IDbSet<TrainingPeriod> TrainingPeriods { get; set; } // TrainingPeriod
        IDbSet<WorkHistory> WorkHistories { get; set; } // WorkHistory

        int SaveChanges();
    }

}

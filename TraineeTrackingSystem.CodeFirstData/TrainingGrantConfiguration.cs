// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // TrainingGrant
    internal class TrainingGrantConfiguration : EntityTypeConfiguration<TrainingGrant>
    {
        public TrainingGrantConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".TrainingGrant");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.FundingId).HasColumnName("FundingId").IsRequired();
            Property(x => x.DoctoralLevelId).HasColumnName("DoctoralLevelId").IsOptional();
            Property(x => x.NumPredocPositionsRequested).HasColumnName("NumPredocPositionsRequested").IsOptional();
            Property(x => x.NumPostdocPositionsRequested).HasColumnName("NumPostdocPositionsRequested").IsOptional();
            Property(x => x.NumPredocPositionsAwarded).HasColumnName("NumPredocPositionsAwarded").IsOptional();
            Property(x => x.NumPostdocPositionsAwarded).HasColumnName("NumPostdocPositionsAwarded").IsOptional();
            Property(x => x.PredocSupportMonthsAwarded).HasColumnName("PredocSupportMonthsAwarded").IsOptional();
            Property(x => x.PostdocSupportMonthsAwarded).HasColumnName("PostdocSupportMonthsAwarded").IsOptional();
            Property(x => x.PreviousTrainingGrantId).HasColumnName("PreviousTrainingGrantId").IsOptional();
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
            Property(x => x.ApplicantPoolAcademicYear).HasColumnName("ApplicantPoolAcademicYear").IsOptional();
            Property(x => x.TrainingGrantStatusId).HasColumnName("TrainingGrantStatusId").IsRequired();

            // Foreign keys
            HasRequired(a => a.Funding).WithMany(b => b.TrainingGrants).HasForeignKey(c => c.FundingId); // FK_TrainingGrant_Funding
            HasOptional(a => a.DoctoralLevel).WithMany(b => b.TrainingGrants).HasForeignKey(c => c.DoctoralLevelId); // FK_TrainingGrant_DoctoralLevel
            HasOptional(a => a.TrainingGrant_PreviousTrainingGrantId).WithMany(b => b.TrainingGrants).HasForeignKey(c => c.PreviousTrainingGrantId); // FK_TrainingGrant_PreviousTrainingGrant
            HasRequired(a => a.TrainingGrantStatu).WithMany(b => b.TrainingGrants).HasForeignKey(c => c.TrainingGrantStatusId); // FK_TrainingGrant_TrainingGrantStatus
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // TrainingGrantSupportGrant
    internal class TrainingGrantSupportGrantConfiguration : EntityTypeConfiguration<TrainingGrantSupportGrant>
    {
        public TrainingGrantSupportGrantConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".TrainingGrantSupportGrant");
            HasKey(x => new { x.TrainingGrantId, x.SupportTrainingGrantId });

            Property(x => x.TrainingGrantId).HasColumnName("TrainingGrantId").IsRequired();
            Property(x => x.SupportTrainingGrantId).HasColumnName("SupportTrainingGrantId").IsRequired();
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();

            // Foreign keys
            HasRequired(a => a.TrainingGrant).WithMany(b => b.TrainingGrantSupportGrants).HasForeignKey(c => c.SupportTrainingGrantId); // FK_TrainingGrantSupportGrant_SupportTrainingGrant
        }
    }

}

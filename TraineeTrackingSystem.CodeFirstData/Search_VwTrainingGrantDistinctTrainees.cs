// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingGrantDistinctTrainees
    public class Search_VwTrainingGrantDistinctTrainees
    {
        public Guid TrainingGrantId { get; set; } // TrainingGrantId
        public Guid FundingId { get; set; } // FundingId
        public string Title { get; set; } // Title
        public Guid MenteeId { get; set; } // MenteeId
        public Guid MenteePersonId { get; set; } // MenteePersonId
        public string TraineeFullName { get; set; } // TraineeFullName
        public int? DepartmentId { get; set; } // DepartmentId
        public string DepartmentName { get; set; } // DepartmentName
        public Guid? ProgramId { get; set; } // ProgramId
        public string ProgramTitle { get; set; } // ProgramTitle
        public int? MenteeTypeId { get; set; } // MenteeTypeId
        public bool IsTrainingGrantEligible { get; set; } // IsTrainingGrantEligible
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_UsageOfFundingRoles
    public class Search_VwUsageOfFundingRoles
    {
        public int FundingRoleId { get; set; } // FundingRoleId
        public string Name { get; set; } // Name
        public bool IsTrainingGrantRole { get; set; } // IsTrainingGrantRole
        public int? TotalFundingFaculty { get; set; } // TotalFundingFaculty
        public int? TotalTrainingGrantFaculty { get; set; } // TotalTrainingGrantFaculty
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // FacultyAcademicData
    public class FacultyAcademicData
    {
        public int Id { get; set; } // Id (Primary key)
        public Guid PersonId { get; set; } // PersonId
        public int? AcademicDegreeId { get; set; } // AcademicDegreeId
        public string AreaOfStudy { get; set; } // AreaOfStudy
        public int? DegreeYear { get; set; } // DegreeYear
        public int? InstitutionId { get; set; } // InstitutionId
        public string OtherDegree { get; set; } // OtherDegree
        public bool IsDeleted { get; set; } // IsDeleted
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy

        // Foreign keys
        public virtual AcademicDegree AcademicDegree { get; set; } // FK_FacultyAcademicData_AcademicDegree
        public virtual Institution Institution { get; set; } // FK_FacultyAcademicData_Institution
        public virtual Person Person { get; set; } // FK_FacultyAcademicData_Person

        public FacultyAcademicData()
        {
            IsDeleted = false;
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // TraineeStatus
    public class TraineeStatu
    {
        public int Id { get; set; } // Id (Primary key)
        public string Name { get; set; } // Name
        public int DoctoralLevelId { get; set; } // DoctoralLevelId

        // Reverse navigation
        public virtual ICollection<TrainingGrantTrainee> TrainingGrantTrainees_PostdocTraineeStatusId { get; set; } // TrainingGrantTrainee.FK_TrainingGrantTrainee_TraineeStatusPostdoc
        public virtual ICollection<TrainingGrantTrainee> TrainingGrantTrainees_PredocTraineeStatusId { get; set; } // TrainingGrantTrainee.FK_TrainingGrantTrainee_TraineeStatusPredoc

        public TraineeStatu()
        {
            TrainingGrantTrainees_PostdocTraineeStatusId = new List<TrainingGrantTrainee>();
            TrainingGrantTrainees_PredocTraineeStatusId = new List<TrainingGrantTrainee>();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingGrantApplicants
    public class Search_VwTrainingGrantApplicants
    {
        public Guid TrainingGrantId { get; set; } // TrainingGrantId
        public Guid FundingId { get; set; } // FundingId
        public string Title { get; set; } // Title
        public Guid ApplicantGuid { get; set; } // ApplicantGUID
        public Guid ApplicantPersonId { get; set; } // ApplicantPersonId
        public int ApplicantId { get; set; } // ApplicantId
        public int? ApplicantTypeId { get; set; } // ApplicantTypeId
        public string ApplicantType { get; set; } // ApplicantType
        public int? ApplicantDeptId { get; set; } // ApplicantDeptId
        public string ApplicantDept { get; set; } // ApplicantDept
        public int? YearEntered { get; set; } // YearEntered
        public string ApplicantPrograms { get; set; } // ApplicantPrograms
        public int? ApplicantPoolAcademicYear { get; set; } // ApplicantPoolAcademicYear
    }

}

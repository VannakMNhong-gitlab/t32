// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Reports
    public class Report
    {
        public int Id { get; set; } // Id (Primary key)
        public string Name { get; set; } // Name
        public bool IsDeleted { get; set; } // IsDeleted
        public string Title { get; set; } // Title
        public DateTime ModifiedDate { get; set; } // ModifiedDate
        public int? TypeId { get; set; } // Type_Id
        public string ReportPath { get; set; } // ReportPath
        public string Description { get; set; } // Description
        public int? StatusTypeId { get; set; } // StatusTypeId
        public bool IsPredoc { get; set; } // IsPredoc
        public bool IsPostdoc { get; set; } // IsPostdoc
        public bool IsNewOnly { get; set; } // IsNewOnly
        public bool IsRenewalOnly { get; set; } // IsRenewalOnly

        // Foreign keys
        public virtual ReportType ReportType { get; set; } // FK_dbo.Reports_dbo.ReportTypes_Type_Id
        public virtual StatusType StatusType { get; set; } // FK_dbo.Reports_dbo.StatusTypes_StatusTypeId

        public Report()
        {
            IsPredoc = false;
            IsPostdoc = false;
            IsNewOnly = false;
            IsRenewalOnly = false;
        }
    }

}

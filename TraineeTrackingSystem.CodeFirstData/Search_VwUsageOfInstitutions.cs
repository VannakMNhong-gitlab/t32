// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_UsageOfInstitutions
    public class Search_VwUsageOfInstitutions
    {
        public int InstitutionId { get; set; } // InstitutionId
        public string InstitutionName { get; set; } // InstitutionName
        public int? TotalAcademicHistory { get; set; } // TotalAcademicHistory
        public int? TotalFacultyAcademics { get; set; } // TotalFacultyAcademics
        public int? TotalFunding { get; set; } // TotalFunding
        public int? TotalGradeRecord { get; set; } // TotalGradeRecord
        public int? TotalMenteeSupport { get; set; } // TotalMenteeSupport
        public int? TotalTrainingPeriods { get; set; } // TotalTrainingPeriods
        public int? TotalWorkHistory { get; set; } // TotalWorkHistory
    }

}

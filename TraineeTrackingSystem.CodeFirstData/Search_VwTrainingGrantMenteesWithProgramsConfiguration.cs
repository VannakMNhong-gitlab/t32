// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingGrantMenteesWithPrograms
    internal class Search_VwTrainingGrantMenteesWithProgramsConfiguration : EntityTypeConfiguration<Search_VwTrainingGrantMenteesWithPrograms>
    {
        public Search_VwTrainingGrantMenteesWithProgramsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_TrainingGrantMenteesWithPrograms");
            HasKey(x => new { x.FacultyPersonId, x.IsTrainingGrantEligible, x.MenteePersonId, x.MenteeId, x.StudentId, x.FacultyId, x.FundingId, x.TrainingGrantId });

            Property(x => x.TrainingGrantId).HasColumnName("TrainingGrantId").IsRequired();
            Property(x => x.FundingId).HasColumnName("FundingId").IsRequired();
            Property(x => x.TrainingGrantTitle).HasColumnName("TrainingGrantTitle").IsOptional();
            Property(x => x.TrainingGrantProjectedStartDate).HasColumnName("TrainingGrantProjectedStartDate").IsOptional();
            Property(x => x.TrainingGrantProjectedEndDate).HasColumnName("TrainingGrantProjectedEndDate").IsOptional();
            Property(x => x.TrainingGrantDateStarted).HasColumnName("TrainingGrantDateStarted").IsOptional();
            Property(x => x.TrainingGrantDateEnded).HasColumnName("TrainingGrantDateEnded").IsOptional();
            Property(x => x.TrainingGrantTypeId).HasColumnName("TrainingGrantTypeId").IsOptional();
            Property(x => x.TrainingGrantType).HasColumnName("TrainingGrantType").IsOptional().HasMaxLength(150);
            Property(x => x.FacultyId).HasColumnName("FacultyId").IsRequired();
            Property(x => x.FacultyPersonId).HasColumnName("FacultyPersonId").IsRequired();
            Property(x => x.FacultyLastName).HasColumnName("FacultyLastName").IsOptional().HasMaxLength(75);
            Property(x => x.FacultyFirstName).HasColumnName("FacultyFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.FacultyFullName).HasColumnName("FacultyFullName").IsOptional().HasMaxLength(178);
            Property(x => x.MenteeId).HasColumnName("MenteeId").IsRequired();
            Property(x => x.StudentId).HasColumnName("StudentId").IsRequired().HasMaxLength(50);
            Property(x => x.MenteePersonId).HasColumnName("MenteePersonId").IsRequired();
            Property(x => x.MenteeLastName).HasColumnName("MenteeLastName").IsOptional().HasMaxLength(75);
            Property(x => x.MenteeFirstName).HasColumnName("MenteeFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MenteeFullName).HasColumnName("MenteeFullName").IsOptional().HasMaxLength(178);
            Property(x => x.IsUnderrepresentedMinority).HasColumnName("IsUnderrepresentedMinority").IsOptional();
            Property(x => x.IsIndividualWithDisabilities).HasColumnName("IsIndividualWithDisabilities").IsOptional();
            Property(x => x.IsFromDisadvantagedBkgd).HasColumnName("IsFromDisadvantagedBkgd").IsOptional();
            Property(x => x.MenteeDeptId).HasColumnName("MenteeDeptId").IsOptional();
            Property(x => x.MenteeDept).HasColumnName("MenteeDept").IsOptional().HasMaxLength(250);
            Property(x => x.MenteeTypeId).HasColumnName("MenteeTypeId").IsOptional();
            Property(x => x.MenteeType).HasColumnName("MenteeType").IsOptional().HasMaxLength(150);
            Property(x => x.IsTrainingGrantEligible).HasColumnName("IsTrainingGrantEligible").IsRequired();
            Property(x => x.MenteeProgramId).HasColumnName("MenteeProgramId").IsOptional();
            Property(x => x.MenteeProgramTitle).HasColumnName("MenteeProgramTitle").IsOptional().HasMaxLength(250);
            Property(x => x.TrainingPeriodYearStarted).HasColumnName("TrainingPeriodYearStarted").IsOptional();
            Property(x => x.TrainingPeriodYearEnded).HasColumnName("TrainingPeriodYearEnded").IsOptional();
        }
    }

}

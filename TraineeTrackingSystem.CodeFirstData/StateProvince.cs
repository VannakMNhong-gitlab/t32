// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // StateProvince
    public class StateProvince
    {
        public int Id { get; set; } // Id (Primary key)
        public string Abbreviation { get; set; } // Abbreviation
        public string FullName { get; set; } // FullName
        public int? CountryId { get; set; } // CountryId
        public bool IsDeleted { get; set; } // IsDeleted

        // Reverse navigation
        public virtual ICollection<ContactAddress> ContactAddresses { get; set; } // ContactAddress.FK_ContactAddress_StateProvince
        public virtual ICollection<Institution> Institutions { get; set; } // Institution.FK_Institution_StateProvince

        // Foreign keys
        public virtual Country Country { get; set; } // FK_StateProvince_Country

        public StateProvince()
        {
            IsDeleted = false;
            ContactAddresses = new List<ContactAddress>();
            Institutions = new List<Institution>();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingGrantGeneralData
    internal class Search_VwTrainingGrantGeneralDataConfiguration : EntityTypeConfiguration<Search_VwTrainingGrantGeneralData>
    {
        public Search_VwTrainingGrantGeneralDataConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_TrainingGrantGeneralData");
            HasKey(x => new { x.TrainingGrantStatusId, x.PdPersonId, x.FundingId, x.IsRenewal, x.Id, x.PdFacultyId });

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.PreviousTrainingGrantId).HasColumnName("PreviousTrainingGrantId").IsOptional();
            Property(x => x.FundingId).HasColumnName("FundingId").IsRequired();
            Property(x => x.SponsorAwardNumber).HasColumnName("SponsorAwardNumber").IsOptional().HasMaxLength(50);
            Property(x => x.Title).HasColumnName("Title").IsOptional();
            Property(x => x.TrainingGrantStatusId).HasColumnName("TrainingGrantStatusId").IsRequired();
            Property(x => x.TrainingGrantStatus).HasColumnName("TrainingGrantStatus").IsOptional().HasMaxLength(150);
            Property(x => x.DateProjectedStart).HasColumnName("DateProjectedStart").IsOptional();
            Property(x => x.DateProjectedEnd).HasColumnName("DateProjectedEnd").IsOptional();
            Property(x => x.SponsorId).HasColumnName("SponsorId").IsOptional();
            Property(x => x.SponsorName).HasColumnName("SponsorName").IsOptional().HasMaxLength(250);
            Property(x => x.SponsorReferenceNumber).HasColumnName("SponsorReferenceNumber").IsOptional().HasMaxLength(150);
            Property(x => x.GrtNumber).HasColumnName("GRTNumber").IsOptional().HasMaxLength(20);
            Property(x => x.PrimeAward).HasColumnName("PrimeAward").IsOptional().HasMaxLength(250);
            Property(x => x.DateTimeGrantStarted).HasColumnName("DateTimeGrantStarted").IsOptional();
            Property(x => x.DateTimeGrantEnded).HasColumnName("DateTimeGrantEnded").IsOptional();
            Property(x => x.FundingTypeId).HasColumnName("FundingTypeId").IsOptional();
            Property(x => x.FundingType).HasColumnName("FundingType").IsOptional().HasMaxLength(150);
            Property(x => x.IsRenewal).HasColumnName("IsRenewal").IsRequired();
            Property(x => x.DoctoralLevelId).HasColumnName("DoctoralLevelId").IsOptional();
            Property(x => x.DoctoralLevel).HasColumnName("DoctoralLevel").IsOptional().HasMaxLength(150);
            Property(x => x.NumPredocPositionsRequested).HasColumnName("NumPredocPositionsRequested").IsOptional();
            Property(x => x.NumPostdocPositionsRequested).HasColumnName("NumPostdocPositionsRequested").IsOptional();
            Property(x => x.NumPredocPositionsAwarded).HasColumnName("NumPredocPositionsAwarded").IsOptional();
            Property(x => x.NumPostdocPositionsAwarded).HasColumnName("NumPostdocPositionsAwarded").IsOptional();
            Property(x => x.PredocSupportMonthsAwarded).HasColumnName("PredocSupportMonthsAwarded").IsOptional();
            Property(x => x.PostdocSupportMonthsAwarded).HasColumnName("PostdocSupportMonthsAwarded").IsOptional();
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.LastUpdatedByName).HasColumnName("LastUpdatedByName").IsOptional().HasMaxLength(129);
            Property(x => x.PdFacultyId).HasColumnName("PdFacultyId").IsRequired();
            Property(x => x.PdPersonId).HasColumnName("PdPersonId").IsRequired();
            Property(x => x.PdLastName).HasColumnName("PdLastName").IsOptional().HasMaxLength(75);
            Property(x => x.PdFirstName).HasColumnName("PdFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.PdFullName).HasColumnName("PdFullName").IsOptional().HasMaxLength(178);
            Property(x => x.DisplayStartDate).HasColumnName("DisplayStartDate").IsOptional();
            Property(x => x.DisplayEndDate).HasColumnName("DisplayEndDate").IsOptional();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // TrainingGrantSupportFunding
    public class TrainingGrantSupportFunding
    {
        public Guid TrainingGrantId { get; set; } // TrainingGrantId (Primary key)
        public Guid FundingId { get; set; } // FundingId (Primary key)
        public bool IsDeleted { get; set; } // IsDeleted

        // Foreign keys
        public virtual Funding Funding { get; set; } // FK_TrainingGrantSupportFunding_Funding

        public TrainingGrantSupportFunding()
        {
            IsDeleted = false;
        }
    }

}

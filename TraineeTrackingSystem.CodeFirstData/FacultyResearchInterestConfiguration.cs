// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // FacultyResearchInterest
    internal class FacultyResearchInterestConfiguration : EntityTypeConfiguration<FacultyResearchInterest>
    {
        public FacultyResearchInterestConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".FacultyResearchInterest");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.ResearchInterest).HasColumnName("ResearchInterest").IsRequired();
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);

            // Foreign keys
            HasRequired(a => a.Person).WithMany(b => b.FacultyResearchInterests).HasForeignKey(c => c.PersonId); // FK_FacultyResearchInterest_Person
            HasMany(t => t.TrainingGrants).WithMany(t => t.FacultyResearchInterests).Map(m => 
            {
                m.ToTable("TrainingGrantFacultyResearchInterest");
                m.MapLeftKey("FacultyResearchInterestId");
                m.MapRightKey("TrainingGrantId");
            });
        }
    }

}

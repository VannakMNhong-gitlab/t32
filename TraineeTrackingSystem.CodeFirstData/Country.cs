// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Country
    public class Country
    {
        public int Id { get; set; } // Id (Primary key)
        public string Name { get; set; } // Name
        public bool IsDeleted { get; set; } // IsDeleted

        // Reverse navigation
        public virtual ICollection<ContactAddress> ContactAddresses { get; set; } // ContactAddress.FK_ContactAddress_Country
        public virtual ICollection<Institution> Institutions { get; set; } // Institution.FK_Institution_Country
        public virtual ICollection<StateProvince> StateProvinces { get; set; } // StateProvince.FK_StateProvince_Country

        public Country()
        {
            IsDeleted = false;
            ContactAddresses = new List<ContactAddress>();
            Institutions = new List<Institution>();
            StateProvinces = new List<StateProvince>();
        }
    }

}

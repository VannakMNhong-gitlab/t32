// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_UsageOfFacultyRanks
    internal class Search_VwUsageOfFacultyRanksConfiguration : EntityTypeConfiguration<Search_VwUsageOfFacultyRanks>
    {
        public Search_VwUsageOfFacultyRanksConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_UsageOfFacultyRanks");
            HasKey(x => new { x.FacultyRankId, x.Rank });

            Property(x => x.FacultyRankId).HasColumnName("FacultyRankId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Rank).HasColumnName("Rank").IsRequired().HasMaxLength(150);
            Property(x => x.TotalFaculty).HasColumnName("TotalFaculty").IsOptional();
        }
    }

}

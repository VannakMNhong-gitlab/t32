// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FacultyResearchInterests
    internal class Search_VwFacultyResearchInterestsConfiguration : EntityTypeConfiguration<Search_VwFacultyResearchInterests>
    {
        public Search_VwFacultyResearchInterestsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_FacultyResearchInterests");
            HasKey(x => new { x.PersonId, x.FacultyId });

            Property(x => x.FacultyId).HasColumnName("FacultyId").IsRequired();
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.FacultyLastName).HasColumnName("FacultyLastName").IsOptional().HasMaxLength(75);
            Property(x => x.FacultyFirstName).HasColumnName("FacultyFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.FacultyMiddleName).HasColumnName("FacultyMiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.FullName).HasColumnName("FullName").IsOptional().HasMaxLength(178);
            Property(x => x.ResearchInterestId).HasColumnName("ResearchInterestId").IsOptional();
            Property(x => x.ResearchInterest).HasColumnName("ResearchInterest").IsOptional();
            Property(x => x.ResearchInterestDateLastUpdated).HasColumnName("ResearchInterestDateLastUpdated").IsOptional();
            Property(x => x.ResearchInterestLastUpdatedBy).HasColumnName("ResearchInterestLastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.ResearchInterestLastUpdatedByName).HasColumnName("ResearchInterestLastUpdatedByName").IsOptional().HasMaxLength(129);
        }
    }

}

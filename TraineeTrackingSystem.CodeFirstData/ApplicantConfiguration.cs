// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Applicant
    internal class ApplicantConfiguration : EntityTypeConfiguration<Applicant>
    {
        public ApplicantConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".Applicant");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.ApplicantId).HasColumnName("ApplicantId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.IsTrainingGrantEligible).HasColumnName("IsTrainingGrantEligible").IsRequired();
            Property(x => x.DoctoralLevelId).HasColumnName("DoctoralLevelId").IsOptional();
            Property(x => x.DepartmentId).HasColumnName("DepartmentId").IsOptional();
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();

            // Foreign keys
            HasRequired(a => a.Person).WithMany(b => b.Applicants).HasForeignKey(c => c.PersonId); // FK_Applicant_Person
            HasOptional(a => a.DoctoralLevel).WithMany(b => b.Applicants).HasForeignKey(c => c.DoctoralLevelId); // FK_Applicant_DoctoralLevel
            HasOptional(a => a.Organization).WithMany(b => b.Applicants).HasForeignKey(c => c.DepartmentId); // FK_Applicant_Organization
            HasMany(t => t.TrainingGrants).WithMany(t => t.Applicants).Map(m => 
            {
                m.ToTable("PreviousTrainingGrantApplicant");
                m.MapLeftKey("ApplicantId");
                m.MapRightKey("TrainingGrantId");
            });
        }
    }

}

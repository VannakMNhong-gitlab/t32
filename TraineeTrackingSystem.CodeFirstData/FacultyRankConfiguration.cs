// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // FacultyRank
    internal class FacultyRankConfiguration : EntityTypeConfiguration<FacultyRank>
    {
        public FacultyRankConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".FacultyRank");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Rank).HasColumnName("Rank").IsRequired().HasMaxLength(150);
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
        }
    }

}

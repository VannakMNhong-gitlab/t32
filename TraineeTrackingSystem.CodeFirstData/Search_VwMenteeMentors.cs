// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_MenteeMentors
    public class Search_VwMenteeMentors
    {
        public Guid TrainingPeriodId { get; set; } // TrainingPeriodId
        public Guid MenteePersonId { get; set; } // MenteePersonId
        public Guid? MenteeId { get; set; } // MenteeId
        public string MenteeStudentId { get; set; } // MenteeStudentId
        public string FirstName { get; set; } // FirstName
        public string MiddleName { get; set; } // MiddleName
        public string LastName { get; set; } // LastName
        public string MenteeFullName { get; set; } // MenteeFullName
        public string MenteeType { get; set; } // MenteeType
        public Guid? MentorPersonId { get; set; } // MentorPersonId
        public Guid? FacultyId { get; set; } // FacultyId
        public string MentorFirstName { get; set; } // MentorFirstName
        public string MentorMiddleName { get; set; } // MentorMiddleName
        public string MentorLastName { get; set; } // MentorLastName
        public string MentorFullName { get; set; } // MentorFullName
        public string MentorEmployeeId { get; set; } // MentorEmployeeId
        public int? MentorPrimaryOrganizationId { get; set; } // MentorPrimaryOrganizationId
        public string MentorPrimaryOrganization { get; set; } // MentorPrimaryOrganization
        public int? InstitutionId { get; set; } // InstitutionId
        public string InstitutionIdentifier { get; set; } // InstitutionIdentifier
        public string InstitutionName { get; set; } // InstitutionName
        public string InstitutionCity { get; set; } // InstitutionCity
        public int? InstitutionStateId { get; set; } // InstitutionStateId
        public string InstitutionStateAbbreviation { get; set; } // InstitutionStateAbbreviation
        public string InstitutionStateFullName { get; set; } // InstitutionStateFullName
        public int? InstitutionCountryId { get; set; } // InstitutionCountryId
        public string InstitutionCountry { get; set; } // InstitutionCountry
        public int? YearStarted { get; set; } // YearStarted
        public int? YearEnded { get; set; } // YearEnded
        public int? CompletedDegreeId { get; set; } // CompletedDegreeId
        public string CompletedDegree { get; set; } // CompletedDegree
        public int? DegreeSoughtId { get; set; } // DegreeSoughtId
        public string DegreeSought { get; set; } // DegreeSought
        public string ResearchProjectTitle { get; set; } // ResearchProjectTitle
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public string LastUpdatedByName { get; set; } // LastUpdatedByName
    }

}

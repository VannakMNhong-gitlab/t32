// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FundingFaculty
    internal class Search_VwFundingFacultyConfiguration : EntityTypeConfiguration<Search_VwFundingFaculty>
    {
        public Search_VwFundingFacultyConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_FundingFaculty");
            HasKey(x => new { x.Id, x.FacultyId, x.FacultyRole, x.FacultyPersonId, x.FacultyRoleId, x.FundingStatusId });

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.Title).HasColumnName("Title").IsOptional();
            Property(x => x.FundingStatusId).HasColumnName("FundingStatusId").IsRequired();
            Property(x => x.FundingStatus).HasColumnName("FundingStatus").IsOptional().HasMaxLength(150);
            Property(x => x.DateProjectedStart).HasColumnName("DateProjectedStart").IsOptional();
            Property(x => x.DateProjectedEnd).HasColumnName("DateProjectedEnd").IsOptional();
            Property(x => x.DateTimeFundingStarted).HasColumnName("DateTimeFundingStarted").IsOptional();
            Property(x => x.DateTimeFundingEnded).HasColumnName("DateTimeFundingEnded").IsOptional();
            Property(x => x.FundingTypeId).HasColumnName("FundingTypeId").IsOptional();
            Property(x => x.FundingType).HasColumnName("FundingType").IsOptional().HasMaxLength(150);
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.FacultyId).HasColumnName("FacultyId").IsRequired();
            Property(x => x.FacultyPersonId).HasColumnName("FacultyPersonId").IsRequired();
            Property(x => x.FacultyLastName).HasColumnName("FacultyLastName").IsOptional().HasMaxLength(75);
            Property(x => x.FacultyFirstName).HasColumnName("FacultyFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.FacultyFullName).HasColumnName("FacultyFullName").IsOptional().HasMaxLength(178);
            Property(x => x.FacultyRoleId).HasColumnName("FacultyRoleId").IsRequired();
            Property(x => x.FacultyRole).HasColumnName("FacultyRole").IsRequired().HasMaxLength(150);
            Property(x => x.SponsorId).HasColumnName("SponsorId").IsOptional();
            Property(x => x.SponsorName).HasColumnName("SponsorName").IsOptional().HasMaxLength(250);
            Property(x => x.SponsorAwardNumber).HasColumnName("SponsorAwardNumber").IsOptional().HasMaxLength(50);
            Property(x => x.FundingFacultyDateLastUpdated).HasColumnName("FundingFacultyDateLastUpdated").IsOptional();
            Property(x => x.FundingFacultyLastUpdatedBy).HasColumnName("FundingFacultyLastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.FundingFacultyLastUpdatedByName).HasColumnName("FundingFacultyLastUpdatedByName").IsOptional().HasMaxLength(129);
        }
    }

}

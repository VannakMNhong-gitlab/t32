// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // AcademicHistory
    internal class AcademicHistoryConfiguration : EntityTypeConfiguration<AcademicHistory>
    {
        public AcademicHistoryConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".AcademicHistory");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.InstitutionId).HasColumnName("InstitutionId").IsRequired();
            Property(x => x.YearStarted).HasColumnName("YearStarted").IsOptional();
            Property(x => x.YearEnded).HasColumnName("YearEnded").IsOptional();
            Property(x => x.AcademicDegreeId).HasColumnName("AcademicDegreeId").IsOptional();
            Property(x => x.AreaOfStudy).HasColumnName("AreaOfStudy").IsOptional().HasMaxLength(150);
            Property(x => x.YearDegreeCompleted).HasColumnName("YearDegreeCompleted").IsOptional();
            Property(x => x.IsUndergraduate).HasColumnName("IsUndergraduate").IsRequired();
            Property(x => x.ResearchProjectTitle).HasColumnName("ResearchProjectTitle").IsOptional().HasMaxLength(500);
            Property(x => x.DoctoralThesis).HasColumnName("DoctoralThesis").IsOptional().HasMaxLength(500);
            Property(x => x.ResearchAdvisor).HasColumnName("ResearchAdvisor").IsOptional().HasMaxLength(250);
            Property(x => x.IsResidency).HasColumnName("IsResidency").IsRequired();
            Property(x => x.ResidencyInstitutionId).HasColumnName("ResidencyInstitutionId").IsOptional();
            Property(x => x.ResidencySpecialization).HasColumnName("ResidencySpecialization").IsOptional().HasMaxLength(500);
            Property(x => x.ResidencyPgy).HasColumnName("ResidencyPGY").IsOptional().HasMaxLength(500);
            Property(x => x.ResidencyAdvisor).HasColumnName("ResidencyAdvisor").IsOptional().HasMaxLength(250);
            Property(x => x.ResidencyYearStarted).HasColumnName("ResidencyYearStarted").IsOptional();
            Property(x => x.ResidencyYearEnded).HasColumnName("ResidencyYearEnded").IsOptional();
            Property(x => x.IsNonMedicalTransfer).HasColumnName("IsNonMedicalTransfer").IsRequired();
            Property(x => x.IsPhDTransfer).HasColumnName("IsPhDTransfer").IsRequired();
            Property(x => x.LabResearch).HasColumnName("LabResearch").IsOptional().HasMaxLength(500);
            Property(x => x.TrainingDoctoralLevelId).HasColumnName("TrainingDoctoralLevelId").IsOptional();
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.Comments).HasColumnName("Comments").IsOptional();
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
            Property(x => x.DateStarted).HasColumnName("DateStarted").IsOptional();
            Property(x => x.DateEnded).HasColumnName("DateEnded").IsOptional();
            Property(x => x.DateDegreeCompleted).HasColumnName("DateDegreeCompleted").IsOptional();

            // Foreign keys
            HasRequired(a => a.Person).WithMany(b => b.AcademicHistories).HasForeignKey(c => c.PersonId); // FK_AcademicHistory_Person
            HasRequired(a => a.Institution_InstitutionId).WithMany(b => b.AcademicHistories_InstitutionId).HasForeignKey(c => c.InstitutionId); // FK_AcademicHistory_Institution
            HasOptional(a => a.AcademicDegree).WithMany(b => b.AcademicHistories).HasForeignKey(c => c.AcademicDegreeId); // FK_AcademicHistory_AcademicDegree
            HasOptional(a => a.Institution_ResidencyInstitutionId).WithMany(b => b.AcademicHistories_ResidencyInstitutionId).HasForeignKey(c => c.ResidencyInstitutionId); // FK_AcademicHistory_ResidencyInstitution
            HasOptional(a => a.DoctoralLevel).WithMany(b => b.AcademicHistories).HasForeignKey(c => c.TrainingDoctoralLevelId); // FK_AcademicHistory_DoctoralLevel
        }
    }

}

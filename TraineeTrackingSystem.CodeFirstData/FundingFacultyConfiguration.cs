// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // FundingFaculty
    internal class FundingFacultyConfiguration : EntityTypeConfiguration<FundingFaculty>
    {
        public FundingFacultyConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".FundingFaculty");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.FacultyId).HasColumnName("FacultyId").IsRequired();
            Property(x => x.FundingId).HasColumnName("FundingId").IsRequired();
            Property(x => x.DateStarted).HasColumnName("DateStarted").IsOptional();
            Property(x => x.DateEnded).HasColumnName("DateEnded").IsOptional();
            Property(x => x.PrimaryRoleId).HasColumnName("PrimaryRoleId").IsOptional();
            Property(x => x.SecondaryRoleId).HasColumnName("SecondaryRoleId").IsOptional();
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);

            // Foreign keys
            HasRequired(a => a.Faculty).WithMany(b => b.FundingFaculties).HasForeignKey(c => c.FacultyId); // FK_FundingFaculty_Faculty
            HasRequired(a => a.Funding).WithMany(b => b.FundingFaculties).HasForeignKey(c => c.FundingId); // FK_FundingFaculty_Funding
            HasOptional(a => a.FundingRole_PrimaryRoleId).WithMany(b => b.FundingFaculties_PrimaryRoleId).HasForeignKey(c => c.PrimaryRoleId); // FK_FundingFaculty_PrimFundingRole
            HasOptional(a => a.FundingRole_SecondaryRoleId).WithMany(b => b.FundingFaculties_SecondaryRoleId).HasForeignKey(c => c.SecondaryRoleId); // FK_FundingFaculty_SecFundingRole
        }
    }

}

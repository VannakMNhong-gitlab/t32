// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_AcademicDegrees
    internal class Search_VwAcademicDegreesConfiguration : EntityTypeConfiguration<Search_VwAcademicDegrees>
    {
        public Search_VwAcademicDegreesConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_AcademicDegrees");
            HasKey(x => new { x.SortOrder, x.Name, x.AcademicDegreeId });

            Property(x => x.AcademicDegreeId).HasColumnName("AcademicDegreeId").IsRequired();
            Property(x => x.Name).HasColumnName("Name").IsRequired().HasMaxLength(25);
            Property(x => x.Description).HasColumnName("Description").IsOptional().HasMaxLength(250);
            Property(x => x.SortOrder).HasColumnName("SortOrder").IsRequired();
            Property(x => x.TotalDegreeUsage).HasColumnName("TotalDegreeUsage").IsOptional();
        }
    }

}

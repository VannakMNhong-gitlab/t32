// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FacultyAcademicData
    internal class Search_VwFacultyAcademicDataConfiguration : EntityTypeConfiguration<Search_VwFacultyAcademicData>
    {
        public Search_VwFacultyAcademicDataConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_FacultyAcademicData");
            HasKey(x => new { x.IsActive, x.FacultyId, x.DisplayId, x.EmployeeId, x.PersonId });

            Property(x => x.FacultyId).HasColumnName("FacultyId").IsRequired();
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.FirstName).HasColumnName("FirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MiddleName).HasColumnName("MiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.LastName).HasColumnName("LastName").IsOptional().HasMaxLength(75);
            Property(x => x.FullName).HasColumnName("FullName").IsOptional().HasMaxLength(178);
            Property(x => x.DisplayId).HasColumnName("DisplayId").IsRequired();
            Property(x => x.EmployeeId).HasColumnName("EmployeeId").IsRequired().HasMaxLength(50);
            Property(x => x.FacultyDateLastUpdated).HasColumnName("FacultyDateLastUpdated").IsOptional();
            Property(x => x.FacultyLastUpdatedBy).HasColumnName("FacultyLastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.FacultyLastUpdatedByName).HasColumnName("FacultyLastUpdatedByName").IsOptional().HasMaxLength(129);
            Property(x => x.Degree).HasColumnName("Degree").IsOptional().HasMaxLength(25);
            Property(x => x.AreaOfStudy).HasColumnName("AreaOfStudy").IsOptional().HasMaxLength(150);
            Property(x => x.DegreeYear).HasColumnName("DegreeYear").IsOptional();
            Property(x => x.DegreeInstitutionId).HasColumnName("DegreeInstitutionId").IsOptional();
            Property(x => x.DegreeInstitution).HasColumnName("DegreeInstitution").IsOptional().HasMaxLength(250);
            Property(x => x.DegreeInstitutionCity).HasColumnName("DegreeInstitutionCity").IsOptional().HasMaxLength(150);
            Property(x => x.DegreeInstitutionState).HasColumnName("DegreeInstitutionState").IsOptional().HasMaxLength(50);
            Property(x => x.DegreeInstitutionCountry).HasColumnName("DegreeInstitutionCountry").IsOptional().HasMaxLength(150);
            Property(x => x.IsActive).HasColumnName("IsActive").IsRequired();
            Property(x => x.AcademicDataDateLastUpdated).HasColumnName("AcademicDataDateLastUpdated").IsOptional();
            Property(x => x.AcademicDataLastUpdatedBy).HasColumnName("AcademicDataLastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.AcademicDataLastUpdatedByName).HasColumnName("AcademicDataLastUpdatedByName").IsOptional().HasMaxLength(129);
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // FundingProgramProjects
    internal class FundingProgramProjectConfiguration : EntityTypeConfiguration<FundingProgramProject>
    {
        public FundingProgramProjectConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".FundingProgramProjects");
            HasKey(x => new { x.FundingId, x.ProgramProjectId });

            Property(x => x.FundingId).HasColumnName("FundingId").IsRequired();
            Property(x => x.ProgramProjectId).HasColumnName("ProgramProjectId").IsRequired();

            // Foreign keys
            HasRequired(a => a.Funding).WithMany(b => b.FundingProgramProjects).HasForeignKey(c => c.FundingId); // FK_FundingProgramProjects_Funding
        }
    }

}

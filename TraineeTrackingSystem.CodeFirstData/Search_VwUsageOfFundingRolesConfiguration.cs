// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_UsageOfFundingRoles
    internal class Search_VwUsageOfFundingRolesConfiguration : EntityTypeConfiguration<Search_VwUsageOfFundingRoles>
    {
        public Search_VwUsageOfFundingRolesConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_UsageOfFundingRoles");
            HasKey(x => new { x.IsTrainingGrantRole, x.FundingRoleId, x.Name });

            Property(x => x.FundingRoleId).HasColumnName("FundingRoleId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Name).HasColumnName("Name").IsRequired().HasMaxLength(150);
            Property(x => x.IsTrainingGrantRole).HasColumnName("IsTrainingGrantRole").IsRequired();
            Property(x => x.TotalFundingFaculty).HasColumnName("TotalFundingFaculty").IsOptional();
            Property(x => x.TotalTrainingGrantFaculty).HasColumnName("TotalTrainingGrantFaculty").IsOptional();
        }
    }

}

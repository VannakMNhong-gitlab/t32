// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // FundingFaculty
    public class FundingFaculty
    {
        public Guid Id { get; set; } // Id (Primary key)
        public Guid FacultyId { get; set; } // FacultyId
        public Guid FundingId { get; set; } // FundingId
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded
        public int? PrimaryRoleId { get; set; } // PrimaryRoleId
        public int? SecondaryRoleId { get; set; } // SecondaryRoleId
        public bool IsDeleted { get; set; } // IsDeleted
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy

        // Foreign keys
        public virtual Faculty Faculty { get; set; } // FK_FundingFaculty_Faculty
        public virtual Funding Funding { get; set; } // FK_FundingFaculty_Funding
        public virtual FundingRole FundingRole_PrimaryRoleId { get; set; } // FK_FundingFaculty_PrimFundingRole
        public virtual FundingRole FundingRole_SecondaryRoleId { get; set; } // FK_FundingFaculty_SecFundingRole

        public FundingFaculty()
        {
            Id = System.Guid.NewGuid();
            IsDeleted = false;
        }
    }

}

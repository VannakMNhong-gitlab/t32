// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_T32Table5A
    internal class Reports_VwT32Table5AConfiguration : EntityTypeConfiguration<Reports_VwT32Table5A>
    {
        public Reports_VwT32Table5AConfiguration(string schema = "Reports")
        {
            ToTable(schema + ".vw_T32Table5A");
            HasKey(x => new { x.TrainingGrantId, x.FacultyPersonId, x.Title, x.FacultyId, x.IsRenewal });

            Property(x => x.FacultyId).HasColumnName("FacultyId").IsRequired();
            Property(x => x.FacultyPersonId).HasColumnName("FacultyPersonId").IsRequired();
            Property(x => x.FacultyLastName).HasColumnName("FacultyLastName").IsOptional().HasMaxLength(75);
            Property(x => x.FacultyFirstName).HasColumnName("FacultyFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.FacultyFullName).HasColumnName("FacultyFullName").IsOptional().HasMaxLength(178);
            Property(x => x.FacultyNameAbbrev).HasColumnName("FacultyNameAbbrev").IsOptional().HasMaxLength(79);
            Property(x => x.TrainingGrantId).HasColumnName("TrainingGrantId").IsRequired();
            Property(x => x.Title).HasColumnName("Title").IsRequired();
            Property(x => x.IsRenewal).HasColumnName("IsRenewal").IsRequired();
            Property(x => x.MenteeId).HasColumnName("MenteeId").IsOptional();
            Property(x => x.StudentId).HasColumnName("StudentId").IsOptional().HasMaxLength(50);
            Property(x => x.MenteePersonId).HasColumnName("MenteePersonId").IsOptional();
            Property(x => x.MenteeLastName).HasColumnName("MenteeLastName").IsOptional().HasMaxLength(75);
            Property(x => x.MenteeFirstName).HasColumnName("MenteeFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MenteeFullName).HasColumnName("MenteeFullName").IsOptional().HasMaxLength(178);
            Property(x => x.MenteeNameAbbrev).HasColumnName("MenteeNameAbbrev").IsOptional().HasMaxLength(79);
            Property(x => x.MenteeTypeId).HasColumnName("MenteeTypeId").IsOptional();
            Property(x => x.MenteeType).HasColumnName("MenteeType").IsOptional().HasMaxLength(150);
            Property(x => x.IsTrainingGrantEligible).HasColumnName("IsTrainingGrantEligible").IsOptional();
            Property(x => x.TrainingPeriodYearStarted).HasColumnName("TrainingPeriodYearStarted").IsOptional();
            Property(x => x.TrainingPeriodYearEnded).HasColumnName("TrainingPeriodYearEnded").IsOptional();
            Property(x => x.TrainingPeriodInstitutionId).HasColumnName("TrainingPeriodInstitutionId").IsOptional();
            Property(x => x.TrainingPeriodInstitution).HasColumnName("TrainingPeriodInstitution").IsOptional().HasMaxLength(250);
            Property(x => x.TrainingPeriodDegreeId).HasColumnName("TrainingPeriodDegreeId").IsOptional();
            Property(x => x.TrainingPeriodDegree).HasColumnName("TrainingPeriodDegree").IsOptional().HasMaxLength(25);
            Property(x => x.ResearchProjectTitle).HasColumnName("ResearchProjectTitle").IsOptional().HasMaxLength(250);
            Property(x => x.SupportSourceText).HasColumnName("SupportSourceText").IsOptional();
            Property(x => x.PositionTitle).HasColumnName("PositionTitle").IsOptional().HasMaxLength(200);
            Property(x => x.DateStartedPosition).HasColumnName("DateStartedPosition").IsOptional();
            Property(x => x.DateEndedPosition).HasColumnName("DateEndedPosition").IsOptional();
        }
    }

}

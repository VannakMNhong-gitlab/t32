// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_T32Table4
    internal class Reports_VwT32Table4Configuration : EntityTypeConfiguration<Reports_VwT32Table4>
    {
        public Reports_VwT32Table4Configuration(string schema = "Reports")
        {
            ToTable(schema + ".vw_T32Table4");
            HasKey(x => new { x.FacultyId, x.FacultyPersonId, x.TrainingGrantId });

            Property(x => x.FacultyId).HasColumnName("FacultyId").IsRequired();
            Property(x => x.FacultyPersonId).HasColumnName("FacultyPersonId").IsRequired();
            Property(x => x.FacultyLastName).HasColumnName("FacultyLastName").IsOptional().HasMaxLength(75);
            Property(x => x.FacultyFirstName).HasColumnName("FacultyFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.FacultyFullName).HasColumnName("FacultyFullName").IsOptional().HasMaxLength(178);
            Property(x => x.FacultyFullNameAbbrev).HasColumnName("FacultyFullNameAbbrev").IsOptional().HasMaxLength(79);
            Property(x => x.FacultyRoleId).HasColumnName("FacultyRoleId").IsOptional();
            Property(x => x.FacultyRole).HasColumnName("FacultyRole").IsOptional().HasMaxLength(150);
            Property(x => x.FundingId).HasColumnName("FundingId").IsOptional();
            Property(x => x.TrainingGrantId).HasColumnName("TrainingGrantId").IsRequired();
            Property(x => x.Title).HasColumnName("Title").IsOptional();
            Property(x => x.StatusId).HasColumnName("StatusId").IsOptional();
            Property(x => x.StatusName).HasColumnName("StatusName").IsOptional().HasMaxLength(150);
            Property(x => x.SponsorId).HasColumnName("SponsorId").IsOptional();
            Property(x => x.SponsorName).HasColumnName("SponsorName").IsOptional().HasMaxLength(250);
            Property(x => x.SponsorAwardNumber).HasColumnName("SponsorAwardNumber").IsOptional().HasMaxLength(50);
            Property(x => x.DateProjectedStart).HasColumnName("DateProjectedStart").IsOptional();
            Property(x => x.DateProjectedEnd).HasColumnName("DateProjectedEnd").IsOptional();
            Property(x => x.DateStarted).HasColumnName("DateStarted").IsOptional();
            Property(x => x.DateEnded).HasColumnName("DateEnded").IsOptional();
            Property(x => x.GrtNumber).HasColumnName("GRTNumber").IsOptional().HasMaxLength(20);
            Property(x => x.PrimeAward).HasColumnName("PrimeAward").IsOptional().HasMaxLength(250);
        }
    }

}

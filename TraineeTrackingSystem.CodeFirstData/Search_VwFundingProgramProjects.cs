// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FundingProgramProjects
    public class Search_VwFundingProgramProjects
    {
        public Guid ParentFundId { get; set; } // ParentFundId
        public string ParentFundSponsorAwardNumber { get; set; } // ParentFundSponsorAwardNumber
        public string ParentFundTitle { get; set; } // ParentFundTitle
        public int ParentFundStatusId { get; set; } // ParentFundStatusId
        public string ParentFundStatus { get; set; } // ParentFundStatus
        public int? ParentFundSponsorId { get; set; } // ParentFundSponsorId
        public string ParentFundSponsor { get; set; } // ParentFundSponsor
        public string ParentFundSponsorReferenceNumber { get; set; } // ParentFundSponsorReferenceNumber
        public string ParentFundGrtNumber { get; set; } // ParentFundGRTNumber
        public DateTime? ParentFundDateStarted { get; set; } // ParentFundDateStarted
        public DateTime? ParentFundDateEnded { get; set; } // ParentFundDateEnded
        public int? ParentFundTypeId { get; set; } // ParentFundTypeId
        public string ParentFundType { get; set; } // ParentFundType
        public string ParentFundPiLastName { get; set; } // ParentFundPILastName
        public string ParentFundPiFirstName { get; set; } // ParentFundPIFirstName
        public string ParentFundPiFullName { get; set; } // ParentFundPIFullName
        public DateTime? ParentFundDateLastUpdated { get; set; } // ParentFundDateLastUpdated
        public string ParentFundLastUpdatedBy { get; set; } // ParentFundLastUpdatedBy
        public string ParentFundLastUpdatedByName { get; set; } // ParentFundLastUpdatedByName
        public int ParentFundDisplayId { get; set; } // ParentFundDisplayId
        public DateTime? ParentFundDateProjectedStart { get; set; } // ParentFundDateProjectedStart
        public DateTime? ParentFundDateProjectedEnd { get; set; } // ParentFundDateProjectedEnd
        public Guid? ParentFundProgramProjectPdId { get; set; } // ParentFundProgramProjectPDId
        public string ParentFundProgramProjectPdLastName { get; set; } // ParentFundProgramProjectPDLastName
        public string ParentFundProgramProjectPdFirstName { get; set; } // ParentFundProgramProjectPDFirstName
        public string ParentFundProgramProjectPdFullName { get; set; } // ParentFundProgramProjectPDFullName
        public Guid ProjectFundId { get; set; } // ProjectFundId
        public string ProjectFundSponsorAwardNumber { get; set; } // ProjectFundSponsorAwardNumber
        public string ProjectFundTitle { get; set; } // ProjectFundTitle
        public int ProjectFundStatusId { get; set; } // ProjectFundStatusId
        public string ProjectFundStatus { get; set; } // ProjectFundStatus
        public int? ProjectFundSponsorId { get; set; } // ProjectFundSponsorId
        public string ProjectFundSponsor { get; set; } // ProjectFundSponsor
        public string ProjectFundSponsorReferenceNumber { get; set; } // ProjectFundSponsorReferenceNumber
        public string ProjectFundGrtNumber { get; set; } // ProjectFundGRTNumber
        public DateTime? ProjectFundDateStarted { get; set; } // ProjectFundDateStarted
        public DateTime? ProjectFundDateEnded { get; set; } // ProjectFundDateEnded
        public int? ProjectFundTypeId { get; set; } // ProjectFundTypeId
        public string ProjectFundType { get; set; } // ProjectFundType
        public DateTime? ProjectFundDateLastUpdated { get; set; } // ProjectFundDateLastUpdated
        public string ProjectFundLastUpdatedBy { get; set; } // ProjectFundLastUpdatedBy
        public string ProjectFundLastUpdatedByName { get; set; } // ProjectFundLastUpdatedByName
        public int ProjectFundDisplayId { get; set; } // ProjectFundDisplayId
        public DateTime? ProjectFundDateProjectedStart { get; set; } // ProjectFundDateProjectedStart
        public DateTime? ProjectFundDateProjectedEnd { get; set; } // ProjectFundDateProjectedEnd
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FacultyAppointments
    public class Search_VwFacultyAppointments
    {
        public int FacultyAppointmentId { get; set; } // FacultyAppointmentId
        public Guid FacultyPersonId { get; set; } // FacultyPersonId
        public string FirstName { get; set; } // FirstName
        public string MiddleName { get; set; } // MiddleName
        public string LastName { get; set; } // LastName
        public string FullName { get; set; } // FullName
        public int InstitutionId { get; set; } // InstitutionId
        public string InstitutionName { get; set; } // InstitutionName
        public int? FacultyTrackId { get; set; } // FacultyTrackId
        public string FacultyTrack { get; set; } // FacultyTrack
        public int? ClinicalFacultyPathwayId { get; set; } // ClinicalFacultyPathwayId
        public string ClinicalFacultyPathway { get; set; } // ClinicalFacultyPathway
        public int? FacultyRankId { get; set; } // FacultyRankId
        public string FacultyRank { get; set; } // FacultyRank
        public string OtherAffiliations { get; set; } // OtherAffiliations
        public string OtherTitles { get; set; } // OtherTitles
        public Decimal? PercentFte { get; set; } // PercentFTE
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded
        public string Comment { get; set; } // Comment
        public DateTime? AppointmentDateLastUpdated { get; set; } // AppointmentDateLastUpdated
        public string AppointmentLastUpdatedBy { get; set; } // AppointmentLastUpdatedBy
        public string AppointmentLastUpdatedByName { get; set; } // AppointmentLastUpdatedByName
    }

}

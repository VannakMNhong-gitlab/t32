// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_StateProvinces
    internal class Search_VwStateProvincesConfiguration : EntityTypeConfiguration<Search_VwStateProvinces>
    {
        public Search_VwStateProvincesConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_StateProvinces");
            HasKey(x => x.StateProvinceId);

            Property(x => x.StateProvinceId).HasColumnName("StateProvinceId").IsRequired();
            Property(x => x.Abbreviation).HasColumnName("Abbreviation").IsOptional().HasMaxLength(50);
            Property(x => x.FullName).HasColumnName("FullName").IsOptional().HasMaxLength(50);
            Property(x => x.CountryId).HasColumnName("CountryId").IsOptional();
            Property(x => x.Country).HasColumnName("Country").IsOptional().HasMaxLength(150);
            Property(x => x.TotalStateProvinceUsage).HasColumnName("TotalStateProvinceUsage").IsOptional();
        }
    }

}

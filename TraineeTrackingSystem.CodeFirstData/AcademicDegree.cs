// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // AcademicDegree
    public class AcademicDegree
    {
        public int Id { get; set; } // Id (Primary key)
        public string Name { get; set; } // Name
        public string Description { get; set; } // Description
        public int SortOrder { get; set; } // SortOrder
        public bool IsDeleted { get; set; } // IsDeleted

        // Reverse navigation
        public virtual ICollection<AcademicHistory> AcademicHistories { get; set; } // AcademicHistory.FK_AcademicHistory_AcademicDegree
        public virtual ICollection<FacultyAcademicData> FacultyAcademicDatas { get; set; } // FacultyAcademicData.FK_FacultyAcademicData_AcademicDegree
        public virtual ICollection<TrainingPeriod> TrainingPeriods_AcademicDegreeId { get; set; } // TrainingPeriod.FK_TrainingPeriod_AcademicDegree
        public virtual ICollection<TrainingPeriod> TrainingPeriods_DegreeSoughtId { get; set; } // TrainingPeriod.FK_TrainingPeriod_DegreeSought

        public AcademicDegree()
        {
            IsDeleted = false;
            AcademicHistories = new List<AcademicHistory>();
            FacultyAcademicDatas = new List<FacultyAcademicData>();
            TrainingPeriods_AcademicDegreeId = new List<TrainingPeriod>();
            TrainingPeriods_DegreeSoughtId = new List<TrainingPeriod>();
        }
    }

}

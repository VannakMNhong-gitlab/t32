// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // ContactEmail
    internal class ContactEmailConfiguration : EntityTypeConfiguration<ContactEmail>
    {
        public ContactEmailConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".ContactEmail");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.EmailAddress).HasColumnName("EmailAddress").IsRequired().HasMaxLength(500);
            Property(x => x.ContactEntityTypeId).HasColumnName("ContactEntityTypeId").IsRequired();
            Property(x => x.IsPrimary).HasColumnName("IsPrimary").IsRequired();
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();

            // Foreign keys
            HasRequired(a => a.Person).WithMany(b => b.ContactEmails).HasForeignKey(c => c.PersonId); // FK_ContactEmail_Person
            HasRequired(a => a.ContactEntityType).WithMany(b => b.ContactEmails).HasForeignKey(c => c.ContactEntityTypeId); // FK_ContactEmail_ContactEntityType
        }
    }

}

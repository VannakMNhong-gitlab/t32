// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_T32Table3
    public class Reports_VwT32Table3
    {
        public Guid FacultyId { get; set; } // FacultyId
        public Guid FacultyPersonId { get; set; } // FacultyPersonId
        public string FacultyLastName { get; set; } // FacultyLastName
        public string FacultyFirstName { get; set; } // FacultyFirstName
        public string FacultyFullName { get; set; } // FacultyFullName
        public string FacultyFullNameAbbrev { get; set; } // FacultyFullNameAbbrev
        public Guid SelectedTrainingGrantId { get; set; } // SelectedTrainingGrantId
        public Guid? SupportedTrainingGrantId { get; set; } // SupportedTrainingGrantId
        public Guid? SupportedTgFundingId { get; set; } // SupportedTGFundingId
        public string SupportedTgTitle { get; set; } // SupportedTGTitle
        public Guid SupportedTgFacultyId { get; set; } // SupportedTGFacultyId
        public Guid SupportedTgFacultyPersonId { get; set; } // SupportedTGFacultyPersonId
        public string SupportedTgFacultyLastName { get; set; } // SupportedTGFacultyLastName
        public string SupportedTgFacultyFirstName { get; set; } // SupportedTGFacultyFirstName
        public string SupportedTgFacultyFullName { get; set; } // SupportedTGFacultyFullName
        public string SupportedTgFacultyFullNameAbbrev { get; set; } // SupportedTGFacultyFullNameAbbrev
        public int? SupportedTgStatusId { get; set; } // SupportedTGStatusId
        public string SupportedTgStatusName { get; set; } // SupportedTGStatusName
        public int? SupportedTgSponsorId { get; set; } // SupportedTGSponsorId
        public string SupportedTgSponsorName { get; set; } // SupportedTGSponsorName
        public string SupportedTgSponsorAwardNumber { get; set; } // SupportedTGSponsorAwardNumber
        public DateTime? SupportedDateProjectedStart { get; set; } // SupportedDateProjectedStart
        public DateTime? SupportedDateProjectedEnd { get; set; } // SupportedDateProjectedEnd
        public DateTime? SupportedTgDateStarted { get; set; } // SupportedTGDateStarted
        public DateTime? SupportedTgDateEnded { get; set; } // SupportedTGDateEnded
        public string SupportedTggrtNumber { get; set; } // SupportedTGGRTNumber
        public string SupportedTgPrimeAward { get; set; } // SupportedTGPrimeAward
        public Guid? PdFacultyId { get; set; } // PdFacultyId
        public Guid? PdPersonId { get; set; } // PdPersonId
        public string PdLastName { get; set; } // PdLastName
        public string PdFirstName { get; set; } // PdFirstName
        public string PdFullName { get; set; } // PdFullName
        public string PdNameAbbrev { get; set; } // PDNameAbbrev
        public int? PdPrimaryOrgId { get; set; } // PDPrimaryOrgId
        public string PdPrimaryOrg { get; set; } // PDPrimaryOrg
        public int? TotalFaculty { get; set; } // TotalFaculty
        public int? NumPredocPositionsRequested { get; set; } // NumPredocPositionsRequested
        public int? NumPostdocPositionsRequested { get; set; } // NumPostdocPositionsRequested
        public int? NumPreDocTraineesAppointed { get; set; } // NumPreDocTraineesAppointed
        public int? NumPostDocTraineesAppointed { get; set; } // NumPostDocTraineesAppointed
    }

}

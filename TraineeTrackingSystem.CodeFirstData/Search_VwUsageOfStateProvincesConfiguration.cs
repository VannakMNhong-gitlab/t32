// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_UsageOfStateProvinces
    internal class Search_VwUsageOfStateProvincesConfiguration : EntityTypeConfiguration<Search_VwUsageOfStateProvinces>
    {
        public Search_VwUsageOfStateProvincesConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_UsageOfStateProvinces");
            HasKey(x => x.StateProvinceId);

            Property(x => x.StateProvinceId).HasColumnName("StateProvinceId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Abbreviation).HasColumnName("Abbreviation").IsOptional().HasMaxLength(50);
            Property(x => x.FullName).HasColumnName("FullName").IsOptional().HasMaxLength(50);
            Property(x => x.TotalContactAddress).HasColumnName("TotalContactAddress").IsOptional();
            Property(x => x.TotalInstitutions).HasColumnName("TotalInstitutions").IsOptional();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // FacultyAppointment
    public class FacultyAppointment
    {
        public int Id { get; set; } // Id (Primary key)
        public Guid PersonId { get; set; } // PersonId
        public int InstitutionId { get; set; } // InstitutionId
        public int? FacultyTrackId { get; set; } // FacultyTrackId
        public int? ClinicalFacultyPathwayId { get; set; } // ClinicalFacultyPathwayId
        public int? FacultyRankId { get; set; } // FacultyRankId
        public string OtherAffiliations { get; set; } // OtherAffiliations
        public string OtherTitles { get; set; } // OtherTitles
        public Decimal? PercentFte { get; set; } // PercentFTE
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded
        public string Comment { get; set; } // Comment
        public bool IsDeleted { get; set; } // IsDeleted
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy

        // Foreign keys
        public virtual ClinicalFacultyPathway ClinicalFacultyPathway { get; set; } // FK_FacultyAppointment_ClinicalFacultyPathway
        public virtual FacultyRank FacultyRank { get; set; } // FK_FacultyAppointment_FacultyRank
        public virtual FacultyTrack FacultyTrack { get; set; } // FK_FacultyAppointment_FacultyTrack
        public virtual Institution Institution { get; set; } // FK_FacultyAppointment_Institution
        public virtual Person Person { get; set; } // FK_FacultyAppointment_Person

        public FacultyAppointment()
        {
            IsDeleted = false;
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_Reports
    internal class Search_VwReportsConfiguration : EntityTypeConfiguration<Search_VwReports>
    {
        public Search_VwReportsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_Reports");
            HasKey(x => new { x.IsPostdoc, x.IsRenewalOnly, x.IsNewOnly, x.StatusType, x.Id, x.ModifiedDate, x.ReportType, x.IsPredoc });

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.Name).HasColumnName("Name").IsOptional();
            Property(x => x.Title).HasColumnName("Title").IsOptional();
            Property(x => x.ModifiedDate).HasColumnName("ModifiedDate").IsRequired();
            Property(x => x.ReportTypeId).HasColumnName("ReportTypeId").IsOptional();
            Property(x => x.ReportType).HasColumnName("ReportType").IsRequired();
            Property(x => x.ReportPath).HasColumnName("ReportPath").IsOptional();
            Property(x => x.Description).HasColumnName("Description").IsOptional();
            Property(x => x.StatusTypeId).HasColumnName("StatusTypeId").IsOptional();
            Property(x => x.StatusType).HasColumnName("StatusType").IsRequired();
            Property(x => x.IsPredoc).HasColumnName("IsPredoc").IsRequired();
            Property(x => x.IsPostdoc).HasColumnName("IsPostdoc").IsRequired();
            Property(x => x.IsNewOnly).HasColumnName("IsNewOnly").IsRequired();
            Property(x => x.IsRenewalOnly).HasColumnName("IsRenewalOnly").IsRequired();
        }
    }

}

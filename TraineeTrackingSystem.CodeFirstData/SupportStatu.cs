// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // SupportStatus
    public class SupportStatu
    {
        public int Id { get; set; } // Id (Primary key)
        public string Name { get; set; } // Name
        public int? SortOrder { get; set; } // SortOrder

        // Reverse navigation
        public virtual ICollection<Support> Supports { get; set; } // Support.FK_Support_SupportStatus

        public SupportStatu()
        {
            Supports = new List<Support>();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_ClinicalFacultyPathways
    public class Search_VwClinicalFacultyPathways
    {
        public int ClinicalFacultyPathwayId { get; set; } // ClinicalFacultyPathwayId
        public string ClinicalFacultyPathway { get; set; } // ClinicalFacultyPathway
        public int? TotalClinicalFacultyPathwayUsage { get; set; } // TotalClinicalFacultyPathwayUsage
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_UsageOfCountries
    internal class Search_VwUsageOfCountriesConfiguration : EntityTypeConfiguration<Search_VwUsageOfCountries>
    {
        public Search_VwUsageOfCountriesConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_UsageOfCountries");
            HasKey(x => new { x.CountryId, x.CountryName });

            Property(x => x.CountryId).HasColumnName("CountryId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CountryName).HasColumnName("CountryName").IsRequired().HasMaxLength(150);
            Property(x => x.TotalContactAddress).HasColumnName("TotalContactAddress").IsOptional();
            Property(x => x.TotalInstitutions).HasColumnName("TotalInstitutions").IsOptional();
            Property(x => x.TotalStateProvince).HasColumnName("TotalStateProvince").IsOptional();
        }
    }

}

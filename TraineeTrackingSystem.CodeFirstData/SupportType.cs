// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // SupportType
    public class SupportType
    {
        public int Id { get; set; } // Id (Primary key)
        public string Name { get; set; } // Name
        public string Abbreviation { get; set; } // Abbreviation

        // Reverse navigation
        public virtual ICollection<Support> Supports { get; set; } // Support.FK_Support_SupportType

        public SupportType()
        {
            Supports = new List<Support>();
        }
    }

}

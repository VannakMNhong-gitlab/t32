// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_UsageOfPrograms
    internal class Search_VwUsageOfProgramsConfiguration : EntityTypeConfiguration<Search_VwUsageOfPrograms>
    {
        public Search_VwUsageOfProgramsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_UsageOfPrograms");
            HasKey(x => new { x.ProgramTitle, x.ProgramId });

            Property(x => x.ProgramId).HasColumnName("ProgramId").IsRequired();
            Property(x => x.ProgramTitle).HasColumnName("ProgramTitle").IsRequired().HasMaxLength(250);
            Property(x => x.TotalApplicantPrograms).HasColumnName("TotalApplicantPrograms").IsOptional();
            Property(x => x.TotalFacultyPrograms).HasColumnName("TotalFacultyPrograms").IsOptional();
            Property(x => x.TotalTrainingGrantPrograms).HasColumnName("TotalTrainingGrantPrograms").IsOptional();
            Property(x => x.TotalTrainingPeriodPrograms).HasColumnName("TotalTrainingPeriodPrograms").IsOptional();
        }
    }

}

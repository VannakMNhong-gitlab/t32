// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingGrantTrainees
    public class Search_VwTrainingGrantTrainees
    {
        public Guid TrainingGrantId { get; set; } // TrainingGrantId
        public Guid FundingId { get; set; } // FundingId
        public Guid TrainingGrantTraineeId { get; set; } // TrainingGrantTraineeId
        public DateTime? AppointmentStartDate { get; set; } // AppointmentStartDate
        public DateTime? AppointmentEndDate { get; set; } // AppointmentEndDate
        public string Title { get; set; } // Title
        public Guid TraineeId { get; set; } // TraineeId
        public Guid MenteeId { get; set; } // MenteeId
        public Guid MenteePersonId { get; set; } // MenteePersonId
        public string MenteeLastName { get; set; } // MenteeLastName
        public string MenteeFirstName { get; set; } // MenteeFirstName
        public string MenteeFullName { get; set; } // MenteeFullName
        public int? MenteeTypeId { get; set; } // MenteeTypeId
        public string MenteeType { get; set; } // MenteeType
        public int? YearStarted { get; set; } // YearStarted
        public int? YearEnded { get; set; } // YearEnded
        public int? DepartmentId { get; set; } // DepartmentId
        public string DepartmentName { get; set; } // DepartmentName
        public string InstitutionAssociation { get; set; } // InstitutionAssociation
        public Guid? ProgramId { get; set; } // ProgramId
        public string ProgramTitle { get; set; } // ProgramTitle
        public string ResearchExperience { get; set; } // ResearchExperience
        public int? DegreeSoughtId { get; set; } // DegreeSoughtId
        public string DegreeSought { get; set; } // DegreeSought
        public string ProgramStatus { get; set; } // ProgramStatus
        public DateTime? TraineeDateLastUpdated { get; set; } // TraineeDateLastUpdated
        public string TraineeLastUpdatedBy { get; set; } // TraineeLastUpdatedBy
        public string TraineeLastUpdatedByName { get; set; } // TraineeLastUpdatedByName
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // TrainingGrantTrainee
    internal class TrainingGrantTraineeConfiguration : EntityTypeConfiguration<TrainingGrantTrainee>
    {
        public TrainingGrantTraineeConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".TrainingGrantTrainee");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.TrainingGrantId).HasColumnName("TrainingGrantId").IsRequired();
            Property(x => x.MenteeId).HasColumnName("MenteeId").IsRequired();
            Property(x => x.DateStarted).HasColumnName("DateStarted").IsOptional();
            Property(x => x.DateEnded).HasColumnName("DateEnded").IsOptional();
            Property(x => x.DoctoralLevelId).HasColumnName("DoctoralLevelId").IsOptional();
            Property(x => x.PredocTraineeStatusId).HasColumnName("PredocTraineeStatusId").IsOptional();
            Property(x => x.PredocReasonForLeaving).HasColumnName("PredocReasonForLeaving").IsOptional().HasMaxLength(1000);
            Property(x => x.PostdocTraineeStatusId).HasColumnName("PostdocTraineeStatusId").IsOptional();
            Property(x => x.PostdocReasonForLeaving).HasColumnName("PostdocReasonForLeaving").IsOptional().HasMaxLength(1000);
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
            Property(x => x.TrainingPeriodId).HasColumnName("TrainingPeriodId").IsOptional();

            // Foreign keys
            HasRequired(a => a.TrainingGrant).WithMany(b => b.TrainingGrantTrainees).HasForeignKey(c => c.TrainingGrantId); // FK_TrainingGrantTrainee_TrainingGrant
            HasRequired(a => a.Mentee).WithMany(b => b.TrainingGrantTrainees).HasForeignKey(c => c.MenteeId); // FK_TrainingGrantTrainee_Mentee
            HasOptional(a => a.DoctoralLevel).WithMany(b => b.TrainingGrantTrainees).HasForeignKey(c => c.DoctoralLevelId); // FK_TrainingGrantTrainee_DoctoralLevel
            HasOptional(a => a.TraineeStatu_PredocTraineeStatusId).WithMany(b => b.TrainingGrantTrainees_PredocTraineeStatusId).HasForeignKey(c => c.PredocTraineeStatusId); // FK_TrainingGrantTrainee_TraineeStatusPredoc
            HasOptional(a => a.TraineeStatu_PostdocTraineeStatusId).WithMany(b => b.TrainingGrantTrainees_PostdocTraineeStatusId).HasForeignKey(c => c.PostdocTraineeStatusId); // FK_TrainingGrantTrainee_TraineeStatusPostdoc
            HasOptional(a => a.TrainingPeriod).WithMany(b => b.TrainingGrantTrainees).HasForeignKey(c => c.TrainingPeriodId); // FK_TrainingGrantTrainee_TrainingPeriod
        }
    }

}

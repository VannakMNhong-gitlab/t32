// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_OrganizationTypes
    public class Search_VwOrganizationTypes
    {
        public int OrganizationTypeId { get; set; } // OrganizationTypeId
        public string OrganizationType { get; set; } // OrganizationType
        public int? TotalOrganizationTypeUsage { get; set; } // TotalOrganizationTypeUsage
    }

}

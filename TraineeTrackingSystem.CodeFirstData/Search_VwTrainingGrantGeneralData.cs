// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingGrantGeneralData
    public class Search_VwTrainingGrantGeneralData
    {
        public Guid Id { get; set; } // Id
        public Guid? PreviousTrainingGrantId { get; set; } // PreviousTrainingGrantId
        public Guid FundingId { get; set; } // FundingId
        public string SponsorAwardNumber { get; set; } // SponsorAwardNumber
        public string Title { get; set; } // Title
        public int TrainingGrantStatusId { get; set; } // TrainingGrantStatusId
        public string TrainingGrantStatus { get; set; } // TrainingGrantStatus
        public DateTime? DateProjectedStart { get; set; } // DateProjectedStart
        public DateTime? DateProjectedEnd { get; set; } // DateProjectedEnd
        public int? SponsorId { get; set; } // SponsorId
        public string SponsorName { get; set; } // SponsorName
        public string SponsorReferenceNumber { get; set; } // SponsorReferenceNumber
        public string GrtNumber { get; set; } // GRTNumber
        public string PrimeAward { get; set; } // PrimeAward
        public DateTime? DateTimeGrantStarted { get; set; } // DateTimeGrantStarted
        public DateTime? DateTimeGrantEnded { get; set; } // DateTimeGrantEnded
        public int? FundingTypeId { get; set; } // FundingTypeId
        public string FundingType { get; set; } // FundingType
        public bool IsRenewal { get; set; } // IsRenewal
        public int? DoctoralLevelId { get; set; } // DoctoralLevelId
        public string DoctoralLevel { get; set; } // DoctoralLevel
        public int? NumPredocPositionsRequested { get; set; } // NumPredocPositionsRequested
        public int? NumPostdocPositionsRequested { get; set; } // NumPostdocPositionsRequested
        public int? NumPredocPositionsAwarded { get; set; } // NumPredocPositionsAwarded
        public int? NumPostdocPositionsAwarded { get; set; } // NumPostdocPositionsAwarded
        public int? PredocSupportMonthsAwarded { get; set; } // PredocSupportMonthsAwarded
        public int? PostdocSupportMonthsAwarded { get; set; } // PostdocSupportMonthsAwarded
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public string LastUpdatedByName { get; set; } // LastUpdatedByName
        public Guid PdFacultyId { get; set; } // PdFacultyId
        public Guid PdPersonId { get; set; } // PdPersonId
        public string PdLastName { get; set; } // PdLastName
        public string PdFirstName { get; set; } // PdFirstName
        public string PdFullName { get; set; } // PdFullName
        public DateTime? DisplayStartDate { get; set; } // DisplayStartDate
        public DateTime? DisplayEndDate { get; set; } // DisplayEndDate
    }

}

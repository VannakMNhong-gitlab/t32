// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_Organizations
    internal class Search_VwOrganizationsConfiguration : EntityTypeConfiguration<Search_VwOrganizations>
    {
        public Search_VwOrganizationsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_Organizations");
            HasKey(x => new { x.OrganizationTypeId, x.Id, x.DisplayName, x.OrganizationId });

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.OrganizationId).HasColumnName("OrganizationId").IsRequired().HasMaxLength(100);
            Property(x => x.DisplayName).HasColumnName("DisplayName").IsRequired().HasMaxLength(250);
            Property(x => x.HrName).HasColumnName("HRName").IsOptional().HasMaxLength(250);
            Property(x => x.OrganizationTypeId).HasColumnName("OrganizationTypeId").IsRequired();
            Property(x => x.OrganizationType).HasColumnName("OrganizationType").IsOptional().HasMaxLength(150);
            Property(x => x.ParentOrganizationId).HasColumnName("ParentOrganizationId").IsOptional();
            Property(x => x.ParentOrgId).HasColumnName("ParentOrgId").IsOptional().HasMaxLength(100);
            Property(x => x.ParentOrgDisplayName).HasColumnName("ParentOrgDisplayName").IsOptional().HasMaxLength(250);
            Property(x => x.ParentOrgHrName).HasColumnName("ParentOrgHRName").IsOptional().HasMaxLength(250);
            Property(x => x.ParentOrgTypeId).HasColumnName("ParentOrgTypeId").IsOptional();
            Property(x => x.ParentOrgType).HasColumnName("ParentOrgType").IsOptional().HasMaxLength(150);
            Property(x => x.TotalOrganizationUsage).HasColumnName("TotalOrganizationUsage").IsOptional();
        }
    }

}

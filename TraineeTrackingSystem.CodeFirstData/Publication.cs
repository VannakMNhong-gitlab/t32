// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Publication
    public class Publication
    {
        public Guid Id { get; set; } // Id (Primary key)
        public Guid AuthorId { get; set; } // AuthorId
        public Guid? CoAuthorId { get; set; } // CoAuthorId
        public string Title { get; set; } // Title
        public int? YearPublished { get; set; } // YearPublished
        public DateTime? DateEpub { get; set; } // DateEpub
        public string Abstract { get; set; } // Abstract
        public string Pmcid { get; set; } // PMCID
        public string Pmid { get; set; } // PMID
        public string ManuscriptId { get; set; } // ManuscriptId
        public string Journal { get; set; } // Journal
        public string Volume { get; set; } // Volume
        public string Issue { get; set; } // Issue
        public string Pagination { get; set; } // Pagination
        public string Citation { get; set; } // Citation
        public bool IsPublicAccess { get; set; } // IsPublicAccess
        public int IsMenteeFirstAuthor { get; set; } // IsMenteeFirstAuthor
        public int IsAdvisorCoAuthor { get; set; } // IsAdvisorCoAuthor
        public string AuthorList { get; set; } // AuthorList
        public string Doi { get; set; } // DOI
        public DateTime? DatePublicationLastUpdated { get; set; } // DatePublicationLastUpdated
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public bool IsDeleted { get; set; } // IsDeleted

        // Foreign keys
        public virtual Person Person_AuthorId { get; set; } // FK_Publication_PersonAuthor
        public virtual Person Person_CoAuthorId { get; set; } // FK_Publication_PersonCoAuthor

        public Publication()
        {
            Id = System.Guid.NewGuid();
            Abstract = "0";
            IsPublicAccess = true;
            IsMenteeFirstAuthor = 0;
            IsAdvisorCoAuthor = 0;
            IsDeleted = false;
        }
    }

}

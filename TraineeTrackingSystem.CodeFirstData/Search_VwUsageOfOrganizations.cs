// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_UsageOfOrganizations
    public class Search_VwUsageOfOrganizations
    {
        public int OrganizationId { get; set; } // OrganizationId
        public string OrganizationName { get; set; } // OrganizationName
        public int? TotalApplicantDepts { get; set; } // TotalApplicantDepts
        public int? TotalFacultyOrgs { get; set; } // TotalFacultyOrgs
        public int? TotalMenteeDepts { get; set; } // TotalMenteeDepts
        public int? TotalTrainingGrantDepts { get; set; } // TotalTrainingGrantDepts
    }

}

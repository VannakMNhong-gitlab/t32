// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_MenteeAcademicData
    public class Search_VwMenteeAcademicData
    {
        public Guid MenteeId { get; set; } // MenteeId
        public Guid MenteePersonId { get; set; } // MenteePersonId
        public string FirstName { get; set; } // FirstName
        public string MiddleName { get; set; } // MiddleName
        public string LastName { get; set; } // LastName
        public string FullName { get; set; } // FullName
        public int DisplayId { get; set; } // DisplayId
        public string StudentId { get; set; } // StudentId
        public bool IsTrainingGrantEligible { get; set; } // IsTrainingGrantEligible
        public bool WasRecruitedToLab { get; set; } // WasRecruitedToLab
        public string Department { get; set; } // Department
        public string InstitutionAssociation { get; set; } // InstitutionAssociation
        public DateTime? MenteeDateLastUpdated { get; set; } // MenteeDateLastUpdated
        public string MenteeLastUpdatedBy { get; set; } // MenteeLastUpdatedBy
        public string MenteeLastUpdatedByName { get; set; } // MenteeLastUpdatedByName
        public string Program { get; set; } // Program
        public Decimal? Gpa { get; set; } // GPA
        public Decimal? GpaScale { get; set; } // GPAScale
        public int? GradingInstitutionId { get; set; } // GradingInstitutionId
        public string GradingInstitution { get; set; } // GradingInstitution
        public int? TestScoreTypeId { get; set; } // TestScoreTypeId
        public string TestScoreType { get; set; } // TestScoreType
        public int? YearTested { get; set; } // YearTested
        public Decimal? GreScoreVerbal { get; set; } // GREScoreVerbal
        public Decimal? GreScoreQuantitative { get; set; } // GREScoreQuantitative
        public Decimal? GreScoreAnalytical { get; set; } // GREScoreAnalytical
        public Decimal? GreScoreSubject { get; set; } // GREScoreSubject
        public Decimal? GrePercentileVerbal { get; set; } // GREPercentileVerbal
        public Decimal? GrePercentileQuantitative { get; set; } // GREPercentileQuantitative
        public Decimal? GrePercentileAnalytical { get; set; } // GREPercentileAnalytical
        public Decimal? GrePercentileSubject { get; set; } // GREPercentileSubject
        public Decimal? McatScoreVerbalReasoning { get; set; } // MCATScoreVerbalReasoning
        public Decimal? McatScorePhysicalSciences { get; set; } // MCATScorePhysicalSciences
        public Decimal? McatScoreBiologicalSciences { get; set; } // MCATScoreBiologicalSciences
        public string McatScoreWriting { get; set; } // MCATScoreWriting
        public Decimal? McatPercentile { get; set; } // MCATPercentile
        public string Degree { get; set; } // Degree
        public int? DegreeInstitutionId { get; set; } // DegreeInstitutionId
        public string DegreeInstitution { get; set; } // DegreeInstitution
        public string DegreeInstitutionStateProvince { get; set; } // DegreeInstitutionStateProvince
        public string DegreeInstitutionCountry { get; set; } // DegreeInstitutionCountry
        public string DegreeAreaOfStudy { get; set; } // DegreeAreaOfStudy
        public int? DegreeYearStarted { get; set; } // DegreeYearStarted
        public int? DegreeYearEnded { get; set; } // DegreeYearEnded
        public int? YearDegreeCompleted { get; set; } // YearDegreeCompleted
        public bool? DegreeIsUndergrad { get; set; } // DegreeIsUndergrad
        public string DegreeResearchProjTitle { get; set; } // DegreeResearchProjTitle
        public string DegreeResearchAdvisor { get; set; } // DegreeResearchAdvisor
        public string DegreeDoctoralThesis { get; set; } // DegreeDoctoralThesis
        public bool? DegreeIsResidency { get; set; } // DegreeIsResidency
        public int? ResidencyInstitutionId { get; set; } // ResidencyInstitutionId
        public string ResidencyInstitution { get; set; } // ResidencyInstitution
        public string ResidencySpecialization { get; set; } // ResidencySpecialization
        public string ResidencyPgy { get; set; } // ResidencyPGY
        public string ResidencyAdvisor { get; set; } // ResidencyAdvisor
        public int? ResidencyYearStarted { get; set; } // ResidencyYearStarted
        public int? ResidencyYearEnded { get; set; } // ResidencyYearEnded
        public bool? IsNonMedicalTransfer { get; set; } // IsNonMedicalTransfer
        public bool? IsPhDTransfer { get; set; } // IsPhDTransfer
        public DateTime? HistoryDateLastUpdated { get; set; } // HistoryDateLastUpdated
        public string HistoryLastUpdatedBy { get; set; } // HistoryLastUpdatedBy
        public string HistoryLastUpdatedByName { get; set; } // HistoryLastUpdatedByName
        public string DegreeTrainingDoctoralLevel { get; set; } // DegreeTrainingDoctoralLevel
    }

}

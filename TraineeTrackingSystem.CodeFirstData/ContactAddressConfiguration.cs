// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // ContactAddress
    internal class ContactAddressConfiguration : EntityTypeConfiguration<ContactAddress>
    {
        public ContactAddressConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".ContactAddress");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.AddressLine1).HasColumnName("AddressLine1").IsOptional().HasMaxLength(500);
            Property(x => x.AddressLine2).HasColumnName("AddressLine2").IsOptional().HasMaxLength(500);
            Property(x => x.AddressLine3).HasColumnName("AddressLine3").IsOptional().HasMaxLength(500);
            Property(x => x.City).HasColumnName("City").IsOptional().HasMaxLength(100);
            Property(x => x.StateId).HasColumnName("StateId").IsOptional();
            Property(x => x.PostalCode).HasColumnName("PostalCode").IsOptional().HasMaxLength(10);
            Property(x => x.ContactEntityTypeId).HasColumnName("ContactEntityTypeId").IsRequired();
            Property(x => x.IsPrimary).HasColumnName("IsPrimary").IsRequired();
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
            Property(x => x.CountryId).HasColumnName("CountryId").IsOptional();

            // Foreign keys
            HasRequired(a => a.Person).WithMany(b => b.ContactAddresses).HasForeignKey(c => c.PersonId); // FK_ContactAddress_Person
            HasOptional(a => a.StateProvince).WithMany(b => b.ContactAddresses).HasForeignKey(c => c.StateId); // FK_ContactAddress_StateProvince
            HasRequired(a => a.ContactEntityType).WithMany(b => b.ContactAddresses).HasForeignKey(c => c.ContactEntityTypeId); // FK_ContactAddress_ContactEntityType
            HasOptional(a => a.Country).WithMany(b => b.ContactAddresses).HasForeignKey(c => c.CountryId); // FK_ContactAddress_Country
        }
    }

}

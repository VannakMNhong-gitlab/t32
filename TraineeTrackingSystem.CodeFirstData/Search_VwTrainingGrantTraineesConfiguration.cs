// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingGrantTrainees
    internal class Search_VwTrainingGrantTraineesConfiguration : EntityTypeConfiguration<Search_VwTrainingGrantTrainees>
    {
        public Search_VwTrainingGrantTraineesConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_TrainingGrantTrainees");
            HasKey(x => new { x.MenteePersonId, x.FundingId, x.TrainingGrantId, x.MenteeId, x.TrainingGrantTraineeId, x.TraineeId });

            Property(x => x.TrainingGrantId).HasColumnName("TrainingGrantId").IsRequired();
            Property(x => x.FundingId).HasColumnName("FundingId").IsRequired();
            Property(x => x.TrainingGrantTraineeId).HasColumnName("TrainingGrantTraineeId").IsRequired();
            Property(x => x.AppointmentStartDate).HasColumnName("AppointmentStartDate").IsOptional();
            Property(x => x.AppointmentEndDate).HasColumnName("AppointmentEndDate").IsOptional();
            Property(x => x.Title).HasColumnName("Title").IsOptional();
            Property(x => x.TraineeId).HasColumnName("TraineeId").IsRequired();
            Property(x => x.MenteeId).HasColumnName("MenteeId").IsRequired();
            Property(x => x.MenteePersonId).HasColumnName("MenteePersonId").IsRequired();
            Property(x => x.MenteeLastName).HasColumnName("MenteeLastName").IsOptional().HasMaxLength(75);
            Property(x => x.MenteeFirstName).HasColumnName("MenteeFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MenteeFullName).HasColumnName("MenteeFullName").IsOptional().HasMaxLength(178);
            Property(x => x.MenteeTypeId).HasColumnName("MenteeTypeId").IsOptional();
            Property(x => x.MenteeType).HasColumnName("MenteeType").IsOptional().HasMaxLength(150);
            Property(x => x.YearStarted).HasColumnName("YearStarted").IsOptional();
            Property(x => x.YearEnded).HasColumnName("YearEnded").IsOptional();
            Property(x => x.DepartmentId).HasColumnName("DepartmentId").IsOptional();
            Property(x => x.DepartmentName).HasColumnName("DepartmentName").IsOptional().HasMaxLength(250);
            Property(x => x.InstitutionAssociation).HasColumnName("InstitutionAssociation").IsOptional().HasMaxLength(50);
            Property(x => x.ProgramId).HasColumnName("ProgramId").IsOptional();
            Property(x => x.ProgramTitle).HasColumnName("ProgramTitle").IsOptional().HasMaxLength(250);
            Property(x => x.ResearchExperience).HasColumnName("ResearchExperience").IsOptional().HasMaxLength(250);
            Property(x => x.DegreeSoughtId).HasColumnName("DegreeSoughtId").IsOptional();
            Property(x => x.DegreeSought).HasColumnName("DegreeSought").IsOptional().HasMaxLength(25);
            Property(x => x.ProgramStatus).HasColumnName("ProgramStatus").IsOptional().HasMaxLength(150);
            Property(x => x.TraineeDateLastUpdated).HasColumnName("TraineeDateLastUpdated").IsOptional();
            Property(x => x.TraineeLastUpdatedBy).HasColumnName("TraineeLastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.TraineeLastUpdatedByName).HasColumnName("TraineeLastUpdatedByName").IsOptional().HasMaxLength(129);
        }
    }

}

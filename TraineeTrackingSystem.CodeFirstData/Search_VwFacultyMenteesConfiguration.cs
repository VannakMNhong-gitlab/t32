// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FacultyMentees
    internal class Search_VwFacultyMenteesConfiguration : EntityTypeConfiguration<Search_VwFacultyMentees>
    {
        public Search_VwFacultyMenteesConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_FacultyMentees");
            HasKey(x => new { x.TrainingPeriodId, x.MenteePersonId, x.MentorPersonId, x.DoctoralLevelId, x.FacultyId });

            Property(x => x.TrainingPeriodId).HasColumnName("TrainingPeriodId").IsRequired();
            Property(x => x.FacultyId).HasColumnName("FacultyId").IsRequired();
            Property(x => x.MentorPersonId).HasColumnName("MentorPersonId").IsRequired();
            Property(x => x.MentorFirstName).HasColumnName("MentorFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MentorMiddleName).HasColumnName("MentorMiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.MentorLastName).HasColumnName("MentorLastName").IsOptional().HasMaxLength(75);
            Property(x => x.MentorFullName).HasColumnName("MentorFullName").IsOptional().HasMaxLength(178);
            Property(x => x.PrimaryOrganizationId).HasColumnName("PrimaryOrganizationId").IsOptional();
            Property(x => x.MentorPrimaryOrganization).HasColumnName("MentorPrimaryOrganization").IsOptional().HasMaxLength(250);
            Property(x => x.YearStarted).HasColumnName("YearStarted").IsOptional();
            Property(x => x.YearEnded).HasColumnName("YearEnded").IsOptional();
            Property(x => x.DoctoralLevelId).HasColumnName("DoctoralLevelId").IsRequired();
            Property(x => x.MenteePersonId).HasColumnName("MenteePersonId").IsRequired();
            Property(x => x.MenteeFirstName).HasColumnName("MenteeFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MenteeMiddleName).HasColumnName("MenteeMiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.MenteeLastName).HasColumnName("MenteeLastName").IsOptional().HasMaxLength(75);
            Property(x => x.MenteeFullName).HasColumnName("MenteeFullName").IsOptional().HasMaxLength(178);
        }
    }

}

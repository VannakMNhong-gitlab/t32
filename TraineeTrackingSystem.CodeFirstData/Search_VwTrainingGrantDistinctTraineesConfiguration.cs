// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingGrantDistinctTrainees
    internal class Search_VwTrainingGrantDistinctTraineesConfiguration : EntityTypeConfiguration<Search_VwTrainingGrantDistinctTrainees>
    {
        public Search_VwTrainingGrantDistinctTraineesConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_TrainingGrantDistinctTrainees");
            HasKey(x => new { x.MenteeId, x.MenteePersonId, x.IsTrainingGrantEligible, x.FundingId, x.TrainingGrantId });

            Property(x => x.TrainingGrantId).HasColumnName("TrainingGrantId").IsRequired();
            Property(x => x.FundingId).HasColumnName("FundingId").IsRequired();
            Property(x => x.Title).HasColumnName("Title").IsOptional();
            Property(x => x.MenteeId).HasColumnName("MenteeId").IsRequired();
            Property(x => x.MenteePersonId).HasColumnName("MenteePersonId").IsRequired();
            Property(x => x.TraineeFullName).HasColumnName("TraineeFullName").IsOptional().HasMaxLength(178);
            Property(x => x.DepartmentId).HasColumnName("DepartmentId").IsOptional();
            Property(x => x.DepartmentName).HasColumnName("DepartmentName").IsOptional().HasMaxLength(250);
            Property(x => x.ProgramId).HasColumnName("ProgramId").IsOptional();
            Property(x => x.ProgramTitle).HasColumnName("ProgramTitle").IsOptional().HasMaxLength(250);
            Property(x => x.MenteeTypeId).HasColumnName("MenteeTypeId").IsOptional();
            Property(x => x.IsTrainingGrantEligible).HasColumnName("IsTrainingGrantEligible").IsRequired();
        }
    }

}

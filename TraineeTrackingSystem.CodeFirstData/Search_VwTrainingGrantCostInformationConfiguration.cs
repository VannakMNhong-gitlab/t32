// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingGrantCostInformation
    internal class Search_VwTrainingGrantCostInformationConfiguration : EntityTypeConfiguration<Search_VwTrainingGrantCostInformation>
    {
        public Search_VwTrainingGrantCostInformationConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_TrainingGrantCostInformation");
            HasKey(x => new { x.FundingId, x.TrainingGrantId });

            Property(x => x.TrainingGrantId).HasColumnName("TrainingGrantId").IsRequired();
            Property(x => x.FundingId).HasColumnName("FundingId").IsRequired();
            Property(x => x.FundingCostId).HasColumnName("FundingCostId").IsOptional();
            Property(x => x.SponsorAwardNumber).HasColumnName("SponsorAwardNumber").IsOptional().HasMaxLength(50);
            Property(x => x.Title).HasColumnName("Title").IsOptional();
            Property(x => x.DateProjectedStart).HasColumnName("DateProjectedStart").IsOptional();
            Property(x => x.DateProjectedEnd).HasColumnName("DateProjectedEnd").IsOptional();
            Property(x => x.DateTimeFundingStarted).HasColumnName("DateTimeFundingStarted").IsOptional();
            Property(x => x.DateTimeFundingEnded).HasColumnName("DateTimeFundingEnded").IsOptional();
            Property(x => x.CostYear).HasColumnName("CostYear").IsOptional();
            Property(x => x.DateCostsStarted).HasColumnName("DateCostsStarted").IsOptional();
            Property(x => x.DateCostsEnded).HasColumnName("DateCostsEnded").IsOptional();
            Property(x => x.BudgetPeriodStatusId).HasColumnName("BudgetPeriodStatusId").IsOptional();
            Property(x => x.BudgetPeriodStatus).HasColumnName("BudgetPeriodStatus").IsOptional().HasMaxLength(150);
            Property(x => x.CurrentYearDirectCosts).HasColumnName("CurrentYearDirectCosts").IsOptional().HasPrecision(10,2);
            Property(x => x.TotalDirectCosts).HasColumnName("TotalDirectCosts").IsOptional().HasPrecision(10,2);
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.LastUpdatedByName).HasColumnName("LastUpdatedByName").IsOptional().HasMaxLength(129);
        }
    }

}

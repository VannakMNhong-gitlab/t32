// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Program
    internal class ProgramConfiguration : EntityTypeConfiguration<Program>
    {
        public ProgramConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".Program");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.Title).HasColumnName("Title").IsRequired().HasMaxLength(250);
            Property(x => x.DisplayId).HasColumnName("DisplayId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
            HasMany(t => t.TrainingGrants).WithMany(t => t.Programs).Map(m => 
            {
                m.ToTable("TrainingGrantProgram");
                m.MapLeftKey("ProgramId");
                m.MapRightKey("TrainingGrantId");
            });
            HasMany(t => t.TrainingPeriods).WithMany(t => t.Programs).Map(m => 
            {
                m.ToTable("TrainingPeriodProgram");
                m.MapLeftKey("ProgramId");
                m.MapRightKey("TrainingPeriodId");
            });
        }
    }

}

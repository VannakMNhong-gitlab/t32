// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // TrainingPeriod
    public class TrainingPeriod
    {
        public Guid Id { get; set; } // Id (Primary key)
        public Guid PersonId { get; set; } // PersonId
        public int? InstitutionId { get; set; } // InstitutionId
        public int DoctoralLevelId { get; set; } // DoctoralLevelId
        public int? YearStarted { get; set; } // YearStarted
        public int? YearEnded { get; set; } // YearEnded
        public int? AcademicDegreeId { get; set; } // AcademicDegreeId
        public string ResearchProjectTitle { get; set; } // ResearchProjectTitle
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public bool IsDeleted { get; set; } // IsDeleted
        public int? DegreeSoughtId { get; set; } // DegreeSoughtId

        // Reverse navigation
        public virtual ICollection<Faculty> Faculties { get; set; } // Many to many mapping
        public virtual ICollection<Program> Programs { get; set; } // Many to many mapping
        public virtual ICollection<TrainingGrantTrainee> TrainingGrantTrainees { get; set; } // TrainingGrantTrainee.FK_TrainingGrantTrainee_TrainingPeriod

        // Foreign keys
        public virtual AcademicDegree AcademicDegree_AcademicDegreeId { get; set; } // FK_TrainingPeriod_AcademicDegree
        public virtual AcademicDegree AcademicDegree_DegreeSoughtId { get; set; } // FK_TrainingPeriod_DegreeSought
        public virtual DoctoralLevel DoctoralLevel { get; set; } // FK_TrainingPeriod_DoctoralLevel
        public virtual Institution Institution { get; set; } // FK_TrainingPeriod_Institution
        public virtual Person Person { get; set; } // FK_TrainingPeriod_Person

        public TrainingPeriod()
        {
            Id = System.Guid.NewGuid();
            IsDeleted = false;
            TrainingGrantTrainees = new List<TrainingGrantTrainee>();
            Programs = new List<Program>();
            Faculties = new List<Faculty>();
        }
    }

}

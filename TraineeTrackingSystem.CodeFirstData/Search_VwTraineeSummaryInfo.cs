// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TraineeSummaryInfo
    public class Search_VwTraineeSummaryInfo
    {
        public Guid TraineeSummaryInfoId { get; set; } // TraineeSummaryInfoId
        public Guid TrainingGrantId { get; set; } // TrainingGrantId
        public string TrainingGrantTitle { get; set; } // TrainingGrantTitle
        public int DoctoralLevelId { get; set; } // DoctoralLevelId
        public string DoctoralLevel { get; set; } // DoctoralLevel
        public DateTime? DateSummaryStarted { get; set; } // DateSummaryStarted
        public DateTime? DateSummaryEnded { get; set; } // DateSummaryEnded
        public int? PositionsAwarded { get; set; } // PositionsAwarded
        public int? SupportMonthsAwarded { get; set; } // SupportMonthsAwarded
        public int? TraineesAppointed { get; set; } // TraineesAppointed
        public int? SupportMonthsUsed { get; set; } // SupportMonthsUsed
        public int? UrmTraineesAppointed { get; set; } // URMTraineesAppointed
        public int? DisabilitiesTraineesAppointed { get; set; } // DisabilitiesTraineesAppointed
        public int? DisadvantagedTraineesAppointed { get; set; } // DisadvantagedTraineesAppointed
        public int? UrmSupportMonthsUsed { get; set; } // URMSupportMonthsUsed
        public int? DisabilitiesSupportMonthsUsed { get; set; } // DisabilitiesSupportMonthsUsed
        public int? DisadvantagedSupportMonthsUsed { get; set; } // DisadvantagedSupportMonthsUsed
        public int? NumMdAppointed { get; set; } // NumMDAppointed
        public int? NumMdPhDAppointed { get; set; } // NumMDPhDAppointed
        public int? NumPhDAppointed { get; set; } // NumPhDAppointed
        public int? NumOtherDegreeAppointed { get; set; } // NumOtherDegreeAppointed
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public string LastUpdatedByName { get; set; } // LastUpdatedByName
    }

}

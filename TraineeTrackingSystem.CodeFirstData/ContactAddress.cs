// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // ContactAddress
    public class ContactAddress
    {
        public int Id { get; set; } // Id (Primary key)
        public Guid PersonId { get; set; } // PersonId
        public string AddressLine1 { get; set; } // AddressLine1
        public string AddressLine2 { get; set; } // AddressLine2
        public string AddressLine3 { get; set; } // AddressLine3
        public string City { get; set; } // City
        public int? StateId { get; set; } // StateId
        public string PostalCode { get; set; } // PostalCode
        public int ContactEntityTypeId { get; set; } // ContactEntityTypeId
        public bool IsPrimary { get; set; } // IsPrimary
        public bool IsDeleted { get; set; } // IsDeleted
        public int? CountryId { get; set; } // CountryId

        // Foreign keys
        public virtual ContactEntityType ContactEntityType { get; set; } // FK_ContactAddress_ContactEntityType
        public virtual Country Country { get; set; } // FK_ContactAddress_Country
        public virtual Person Person { get; set; } // FK_ContactAddress_Person
        public virtual StateProvince StateProvince { get; set; } // FK_ContactAddress_StateProvince

        public ContactAddress()
        {
            ContactEntityTypeId = 1;
            IsPrimary = true;
            IsDeleted = false;
            CountryId = 1;
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TraineeCurrentSummaryInfo
    internal class Search_VwTraineeCurrentSummaryInfoConfiguration : EntityTypeConfiguration<Search_VwTraineeCurrentSummaryInfo>
    {
        public Search_VwTraineeCurrentSummaryInfoConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_TraineeCurrentSummaryInfo");
            HasKey(x => new { x.DoctoralLevelId, x.TrainingGrantId });

            Property(x => x.TrainingGrantId).HasColumnName("TrainingGrantId").IsRequired();
            Property(x => x.DoctoralLevelId).HasColumnName("DoctoralLevelId").IsRequired();
            Property(x => x.CurrentPredocTraineeSummaryInfoId).HasColumnName("CurrentPredocTraineeSummaryInfoId").IsOptional();
            Property(x => x.CurrentPostdocTraineeSummaryInfoId).HasColumnName("CurrentPostdocTraineeSummaryInfoId").IsOptional();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Program
    public class Program
    {
        public Guid Id { get; set; } // Id (Primary key)
        public string Title { get; set; } // Title
        public int DisplayId { get; set; } // DisplayId
        public bool IsDeleted { get; set; } // IsDeleted

        // Reverse navigation
        public virtual ICollection<Faculty> Faculties { get; set; } // Many to many mapping
        public virtual ICollection<Person> People { get; set; } // Many to many mapping
        public virtual ICollection<TrainingGrant> TrainingGrants { get; set; } // Many to many mapping
        public virtual ICollection<TrainingPeriod> TrainingPeriods { get; set; } // Many to many mapping

        public Program()
        {
            Id = System.Guid.NewGuid();
            IsDeleted = false;
            Faculties = new List<Faculty>();
            TrainingGrants = new List<TrainingGrant>();
            TrainingPeriods = new List<TrainingPeriod>();
            People = new List<Person>();
        }
    }

}

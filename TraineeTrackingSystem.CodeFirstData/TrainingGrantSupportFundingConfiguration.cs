// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // TrainingGrantSupportFunding
    internal class TrainingGrantSupportFundingConfiguration : EntityTypeConfiguration<TrainingGrantSupportFunding>
    {
        public TrainingGrantSupportFundingConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".TrainingGrantSupportFunding");
            HasKey(x => new { x.TrainingGrantId, x.FundingId });

            Property(x => x.TrainingGrantId).HasColumnName("TrainingGrantId").IsRequired();
            Property(x => x.FundingId).HasColumnName("FundingId").IsRequired();
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();

            // Foreign keys
            HasRequired(a => a.Funding).WithMany(b => b.TrainingGrantSupportFundings).HasForeignKey(c => c.FundingId); // FK_TrainingGrantSupportFunding_Funding
        }
    }

}

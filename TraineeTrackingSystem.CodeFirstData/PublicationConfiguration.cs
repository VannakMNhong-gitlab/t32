// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Publication
    internal class PublicationConfiguration : EntityTypeConfiguration<Publication>
    {
        public PublicationConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".Publication");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.AuthorId).HasColumnName("AuthorId").IsRequired();
            Property(x => x.CoAuthorId).HasColumnName("CoAuthorId").IsOptional();
            Property(x => x.Title).HasColumnName("Title").IsOptional();
            Property(x => x.YearPublished).HasColumnName("YearPublished").IsOptional();
            Property(x => x.DateEpub).HasColumnName("DateEpub").IsOptional();
            Property(x => x.Abstract).HasColumnName("Abstract").IsOptional();
            Property(x => x.Pmcid).HasColumnName("PMCID").IsOptional().HasMaxLength(15);
            Property(x => x.Pmid).HasColumnName("PMID").IsOptional().HasMaxLength(15);
            Property(x => x.ManuscriptId).HasColumnName("ManuscriptId").IsOptional().HasMaxLength(15);
            Property(x => x.Journal).HasColumnName("Journal").IsOptional();
            Property(x => x.Volume).HasColumnName("Volume").IsOptional().HasMaxLength(100);
            Property(x => x.Issue).HasColumnName("Issue").IsOptional().HasMaxLength(100);
            Property(x => x.Pagination).HasColumnName("Pagination").IsOptional().HasMaxLength(50);
            Property(x => x.Citation).HasColumnName("Citation").IsOptional();
            Property(x => x.IsPublicAccess).HasColumnName("IsPublicAccess").IsRequired();
            Property(x => x.IsMenteeFirstAuthor).HasColumnName("IsMenteeFirstAuthor").IsRequired();
            Property(x => x.IsAdvisorCoAuthor).HasColumnName("IsAdvisorCoAuthor").IsRequired();
            Property(x => x.AuthorList).HasColumnName("AuthorList").IsOptional();
            Property(x => x.Doi).HasColumnName("DOI").IsOptional().HasMaxLength(250);
            Property(x => x.DatePublicationLastUpdated).HasColumnName("DatePublicationLastUpdated").IsOptional();
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();

            // Foreign keys
            HasRequired(a => a.Person_AuthorId).WithMany(b => b.Publications_AuthorId).HasForeignKey(c => c.AuthorId); // FK_Publication_PersonAuthor
            HasOptional(a => a.Person_CoAuthorId).WithMany(b => b.Publications_CoAuthorId).HasForeignKey(c => c.CoAuthorId); // FK_Publication_PersonCoAuthor
        }
    }

}

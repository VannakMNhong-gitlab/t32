// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Roles
    public class Role
    {
        public int Id { get; set; } // Id (Primary key)
        public string RoleName { get; set; } // RoleName
        public string Description { get; set; } // Description
        public bool IsActive { get; set; } // IsActive
        public bool IsDeleted { get; set; } // IsDeleted

        // Reverse navigation
        public virtual ICollection<PersonRole> PersonRoles { get; set; } // Many to many mapping

        public Role()
        {
            IsActive = true;
            IsDeleted = false;
            PersonRoles = new List<PersonRole>();
        }
    }

}

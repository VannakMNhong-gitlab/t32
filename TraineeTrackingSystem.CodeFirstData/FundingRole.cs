// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // FundingRole
    public class FundingRole
    {
        public int Id { get; set; } // Id (Primary key)
        public string Name { get; set; } // Name
        public bool IsTrainingGrantRole { get; set; } // IsTrainingGrantRole
        public bool IsDeleted { get; set; } // IsDeleted

        // Reverse navigation
        public virtual ICollection<FundingFaculty> FundingFaculties_PrimaryRoleId { get; set; } // FundingFaculty.FK_FundingFaculty_PrimFundingRole
        public virtual ICollection<FundingFaculty> FundingFaculties_SecondaryRoleId { get; set; } // FundingFaculty.FK_FundingFaculty_SecFundingRole

        public FundingRole()
        {
            IsTrainingGrantRole = false;
            IsDeleted = false;
            FundingFaculties_PrimaryRoleId = new List<FundingFaculty>();
            FundingFaculties_SecondaryRoleId = new List<FundingFaculty>();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Person
    internal class PersonConfiguration : EntityTypeConfiguration<Person>
    {
        public PersonConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".Person");
            HasKey(x => x.PersonId);

            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.FirstName).HasColumnName("FirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MiddleName).HasColumnName("MiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.LastName).HasColumnName("LastName").IsOptional().HasMaxLength(75);
            Property(x => x.IsUnderrepresentedMinority).HasColumnName("IsUnderrepresentedMinority").IsOptional();
            Property(x => x.IsIndividualWithDisabilities).HasColumnName("IsIndividualWithDisabilities").IsOptional();
            Property(x => x.IsFromDisadvantagedBkgd).HasColumnName("IsFromDisadvantagedBkgd").IsOptional();
            Property(x => x.DateCreated).HasColumnName("DateCreated").IsRequired();
            Property(x => x.DisplayId).HasColumnName("DisplayId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.GenderId).HasColumnName("GenderId").IsOptional();
            Property(x => x.GenderAtBirthId).HasColumnName("GenderAtBirthId").IsOptional();
            Property(x => x.EthnicityId).HasColumnName("EthnicityId").IsOptional();

            // Foreign keys
            HasOptional(a => a.Gender).WithMany(b => b.People).HasForeignKey(c => c.GenderId); // FK_Person_Gender
            HasOptional(a => a.Ethnicity).WithMany(b => b.People).HasForeignKey(c => c.EthnicityId); // FK_Person_Ethnicity
            HasMany(t => t.Programs).WithMany(t => t.People).Map(m => 
            {
                m.ToTable("ApplicantProgram");
                m.MapLeftKey("PersonId");
                m.MapRightKey("ProgramId");
            });
        }
    }

}

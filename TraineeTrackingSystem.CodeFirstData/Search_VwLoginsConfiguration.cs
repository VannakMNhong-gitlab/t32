// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_Logins
    internal class Search_VwLoginsConfiguration : EntityTypeConfiguration<Search_VwLogins>
    {
        public Search_VwLoginsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_Logins");
            HasKey(x => x.LoginId);

            Property(x => x.LoginId).HasColumnName("LoginId").IsRequired();
            Property(x => x.ExternalId).HasColumnName("ExternalId").IsOptional().HasMaxLength(100);
            Property(x => x.Username).HasColumnName("Username").IsOptional().HasMaxLength(100);
            Property(x => x.PersonId).HasColumnName("PersonId").IsOptional();
            Property(x => x.FirstName).HasColumnName("FirstName").IsOptional().HasMaxLength(50);
            Property(x => x.LastName).HasColumnName("LastName").IsOptional().HasMaxLength(75);
            Property(x => x.MiddleName).HasColumnName("MiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.DateCreated).HasColumnName("DateCreated").IsOptional();
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
        }
    }

}

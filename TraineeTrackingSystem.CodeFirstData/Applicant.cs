// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Applicant
    public class Applicant
    {
        public Guid Id { get; set; } // Id (Primary key)
        public Guid PersonId { get; set; } // PersonId
        public int ApplicantId { get; set; } // ApplicantId
        public bool IsTrainingGrantEligible { get; set; } // IsTrainingGrantEligible
        public int? DoctoralLevelId { get; set; } // DoctoralLevelId
        public int? DepartmentId { get; set; } // DepartmentId
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public bool IsDeleted { get; set; } // IsDeleted

        // Reverse navigation
        public virtual ICollection<TrainingGrant> TrainingGrants { get; set; } // Many to many mapping

        // Foreign keys
        public virtual DoctoralLevel DoctoralLevel { get; set; } // FK_Applicant_DoctoralLevel
        public virtual Organization Organization { get; set; } // FK_Applicant_Organization
        public virtual Person Person { get; set; } // FK_Applicant_Person

        public Applicant()
        {
            Id = System.Guid.NewGuid();
            IsTrainingGrantEligible = false;
            IsDeleted = false;
            TrainingGrants = new List<TrainingGrant>();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FacultyData
    internal class Search_VwFacultyDataConfiguration : EntityTypeConfiguration<Search_VwFacultyData>
    {
        public Search_VwFacultyDataConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_FacultyData");
            HasKey(x => new { x.EmployeeId, x.Id, x.PersonId, x.IsActive, x.DisplayId });

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.FirstName).HasColumnName("FirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MiddleName).HasColumnName("MiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.LastName).HasColumnName("LastName").IsOptional().HasMaxLength(75);
            Property(x => x.FullName).HasColumnName("FullName").IsOptional().HasMaxLength(178);
            Property(x => x.DisplayId).HasColumnName("DisplayId").IsRequired();
            Property(x => x.EmployeeId).HasColumnName("EmployeeId").IsRequired().HasMaxLength(50);
            Property(x => x.Rank).HasColumnName("Rank").IsOptional().HasMaxLength(150);
            Property(x => x.PrimaryOrganization).HasColumnName("PrimaryOrganization").IsOptional().HasMaxLength(250);
            Property(x => x.SecondaryOrganization).HasColumnName("SecondaryOrganization").IsOptional().HasMaxLength(250);
            Property(x => x.ResearchInterest).HasColumnName("ResearchInterest").IsOptional();
            Property(x => x.Degree).HasColumnName("Degree").IsOptional().HasMaxLength(25);
            Property(x => x.AreaOfStudy).HasColumnName("AreaOfStudy").IsOptional().HasMaxLength(150);
            Property(x => x.DegreeYear).HasColumnName("DegreeYear").IsOptional();
            Property(x => x.DegreeInstitutionId).HasColumnName("DegreeInstitutionId").IsOptional();
            Property(x => x.DegreeInstitution).HasColumnName("DegreeInstitution").IsOptional().HasMaxLength(250);
            Property(x => x.DegreeInstitutionStateProvince).HasColumnName("DegreeInstitutionStateProvince").IsOptional().HasMaxLength(50);
            Property(x => x.DegreeInstitutionCountry).HasColumnName("DegreeInstitutionCountry").IsOptional().HasMaxLength(150);
            Property(x => x.OtherAffiliations).HasColumnName("OtherAffiliations").IsOptional().HasMaxLength(1000);
            Property(x => x.OtherTitles).HasColumnName("OtherTitles").IsOptional().HasMaxLength(250);
            Property(x => x.IsActive).HasColumnName("IsActive").IsRequired();
            Property(x => x.FacultyDateLastUpdated).HasColumnName("FacultyDateLastUpdated").IsOptional();
            Property(x => x.FacultyLastUpdatedBy).HasColumnName("FacultyLastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.FacultyLastUpdatedByName).HasColumnName("FacultyLastUpdatedByName").IsOptional().HasMaxLength(129);
        }
    }

}

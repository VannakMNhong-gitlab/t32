// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_Ethnicities
    internal class Search_VwEthnicitiesConfiguration : EntityTypeConfiguration<Search_VwEthnicities>
    {
        public Search_VwEthnicitiesConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_Ethnicities");
            HasKey(x => new { x.Ethnicity, x.EthnicityId });

            Property(x => x.EthnicityId).HasColumnName("EthnicityId").IsRequired();
            Property(x => x.Ethnicity).HasColumnName("Ethnicity").IsRequired().HasMaxLength(300);
            Property(x => x.TotalEthnicityUsage).HasColumnName("TotalEthnicityUsage").IsOptional();
        }
    }

}

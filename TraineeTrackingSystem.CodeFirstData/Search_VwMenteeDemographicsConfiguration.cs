// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_MenteeDemographics
    internal class Search_VwMenteeDemographicsConfiguration : EntityTypeConfiguration<Search_VwMenteeDemographics>
    {
        public Search_VwMenteeDemographicsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_MenteeDemographics");
            HasKey(x => new { x.Id, x.WasRecruitedToLab, x.IsTrainingGrantEligible, x.PersonId, x.StudentId, x.DisplayId });

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.FirstName).HasColumnName("FirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MiddleName).HasColumnName("MiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.LastName).HasColumnName("LastName").IsOptional().HasMaxLength(75);
            Property(x => x.FullName).HasColumnName("FullName").IsOptional().HasMaxLength(178);
            Property(x => x.DisplayId).HasColumnName("DisplayId").IsRequired();
            Property(x => x.StudentId).HasColumnName("StudentId").IsRequired().HasMaxLength(50);
            Property(x => x.IsTrainingGrantEligible).HasColumnName("IsTrainingGrantEligible").IsRequired();
            Property(x => x.WasRecruitedToLab).HasColumnName("WasRecruitedToLab").IsRequired();
            Property(x => x.IsFromDisadvantagedBkgd).HasColumnName("IsFromDisadvantagedBkgd").IsOptional();
            Property(x => x.IsIndividualWithDisabilities).HasColumnName("IsIndividualWithDisabilities").IsOptional();
            Property(x => x.IsUnderrepresentedMinority).HasColumnName("IsUnderrepresentedMinority").IsOptional();
            Property(x => x.DepartmentId).HasColumnName("DepartmentId").IsOptional();
            Property(x => x.Department).HasColumnName("Department").IsOptional().HasMaxLength(250);
            Property(x => x.InstitutionAssociation).HasColumnName("InstitutionAssociation").IsOptional().HasMaxLength(50);
            Property(x => x.MenteeDateLastUpdated).HasColumnName("MenteeDateLastUpdated").IsOptional();
            Property(x => x.MenteeLastUpdatedBy).HasColumnName("MenteeLastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.MenteeLastUpdatedByName).HasColumnName("MenteeLastUpdatedByName").IsOptional().HasMaxLength(129);
            Property(x => x.ProgramId).HasColumnName("ProgramId").IsOptional();
            Property(x => x.ProgramTitle).HasColumnName("ProgramTitle").IsOptional().HasMaxLength(250);
            Property(x => x.IsPrimaryEmail).HasColumnName("IsPrimaryEmail").IsOptional();
            Property(x => x.EmailAddress).HasColumnName("EmailAddress").IsOptional().HasMaxLength(500);
            Property(x => x.MailingAddress).HasColumnName("MailingAddress").IsOptional().HasMaxLength(1668);
            Property(x => x.AddressLine1).HasColumnName("AddressLine1").IsOptional().HasMaxLength(500);
            Property(x => x.AddressLine2).HasColumnName("AddressLine2").IsOptional().HasMaxLength(500);
            Property(x => x.AddressLine3).HasColumnName("AddressLine3").IsOptional().HasMaxLength(500);
            Property(x => x.AddressCity).HasColumnName("AddressCity").IsOptional().HasMaxLength(100);
            Property(x => x.AddressStateId).HasColumnName("AddressStateId").IsOptional();
            Property(x => x.AddressState).HasColumnName("AddressState").IsOptional().HasMaxLength(50);
            Property(x => x.AddressPostalCode).HasColumnName("AddressPostalCode").IsOptional().HasMaxLength(10);
            Property(x => x.PhoneNumber).HasColumnName("PhoneNumber").IsOptional().HasMaxLength(25);
        }
    }

}

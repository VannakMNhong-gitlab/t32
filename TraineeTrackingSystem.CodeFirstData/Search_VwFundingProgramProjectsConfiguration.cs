// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FundingProgramProjects
    internal class Search_VwFundingProgramProjectsConfiguration : EntityTypeConfiguration<Search_VwFundingProgramProjects>
    {
        public Search_VwFundingProgramProjectsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_FundingProgramProjects");
            HasKey(x => new { x.ParentFundType, x.ParentFundTitle, x.ParentFundStatusId, x.ProjectFundType, x.ProjectFundDisplayId, x.ProjectFundStatusId, x.ParentFundId, x.ProjectFundStatus, x.ParentFundDisplayId, x.ProjectFundTitle, x.ProjectFundId, x.ParentFundStatus });

            Property(x => x.ParentFundId).HasColumnName("ParentFundId").IsRequired();
            Property(x => x.ParentFundSponsorAwardNumber).HasColumnName("ParentFundSponsorAwardNumber").IsOptional().HasMaxLength(50);
            Property(x => x.ParentFundTitle).HasColumnName("ParentFundTitle").IsRequired();
            Property(x => x.ParentFundStatusId).HasColumnName("ParentFundStatusId").IsRequired();
            Property(x => x.ParentFundStatus).HasColumnName("ParentFundStatus").IsRequired().HasMaxLength(150);
            Property(x => x.ParentFundSponsorId).HasColumnName("ParentFundSponsorId").IsOptional();
            Property(x => x.ParentFundSponsor).HasColumnName("ParentFundSponsor").IsOptional().HasMaxLength(250);
            Property(x => x.ParentFundSponsorReferenceNumber).HasColumnName("ParentFundSponsorReferenceNumber").IsOptional().HasMaxLength(150);
            Property(x => x.ParentFundGrtNumber).HasColumnName("ParentFundGRTNumber").IsOptional().HasMaxLength(20);
            Property(x => x.ParentFundDateStarted).HasColumnName("ParentFundDateStarted").IsOptional();
            Property(x => x.ParentFundDateEnded).HasColumnName("ParentFundDateEnded").IsOptional();
            Property(x => x.ParentFundTypeId).HasColumnName("ParentFundTypeId").IsOptional();
            Property(x => x.ParentFundType).HasColumnName("ParentFundType").IsRequired().HasMaxLength(150);
            Property(x => x.ParentFundPiLastName).HasColumnName("ParentFundPILastName").IsOptional().HasMaxLength(75);
            Property(x => x.ParentFundPiFirstName).HasColumnName("ParentFundPIFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.ParentFundPiFullName).HasColumnName("ParentFundPIFullName").IsOptional().HasMaxLength(178);
            Property(x => x.ParentFundDateLastUpdated).HasColumnName("ParentFundDateLastUpdated").IsOptional();
            Property(x => x.ParentFundLastUpdatedBy).HasColumnName("ParentFundLastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.ParentFundLastUpdatedByName).HasColumnName("ParentFundLastUpdatedByName").IsOptional().HasMaxLength(129);
            Property(x => x.ParentFundDisplayId).HasColumnName("ParentFundDisplayId").IsRequired();
            Property(x => x.ParentFundDateProjectedStart).HasColumnName("ParentFundDateProjectedStart").IsOptional();
            Property(x => x.ParentFundDateProjectedEnd).HasColumnName("ParentFundDateProjectedEnd").IsOptional();
            Property(x => x.ParentFundProgramProjectPdId).HasColumnName("ParentFundProgramProjectPDId").IsOptional();
            Property(x => x.ParentFundProgramProjectPdLastName).HasColumnName("ParentFundProgramProjectPDLastName").IsOptional().HasMaxLength(75);
            Property(x => x.ParentFundProgramProjectPdFirstName).HasColumnName("ParentFundProgramProjectPDFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.ParentFundProgramProjectPdFullName).HasColumnName("ParentFundProgramProjectPDFullName").IsOptional().HasMaxLength(178);
            Property(x => x.ProjectFundId).HasColumnName("ProjectFundId").IsRequired();
            Property(x => x.ProjectFundSponsorAwardNumber).HasColumnName("ProjectFundSponsorAwardNumber").IsOptional().HasMaxLength(50);
            Property(x => x.ProjectFundTitle).HasColumnName("ProjectFundTitle").IsRequired();
            Property(x => x.ProjectFundStatusId).HasColumnName("ProjectFundStatusId").IsRequired();
            Property(x => x.ProjectFundStatus).HasColumnName("ProjectFundStatus").IsRequired().HasMaxLength(150);
            Property(x => x.ProjectFundSponsorId).HasColumnName("ProjectFundSponsorId").IsOptional();
            Property(x => x.ProjectFundSponsor).HasColumnName("ProjectFundSponsor").IsOptional().HasMaxLength(250);
            Property(x => x.ProjectFundSponsorReferenceNumber).HasColumnName("ProjectFundSponsorReferenceNumber").IsOptional().HasMaxLength(150);
            Property(x => x.ProjectFundGrtNumber).HasColumnName("ProjectFundGRTNumber").IsOptional().HasMaxLength(20);
            Property(x => x.ProjectFundDateStarted).HasColumnName("ProjectFundDateStarted").IsOptional();
            Property(x => x.ProjectFundDateEnded).HasColumnName("ProjectFundDateEnded").IsOptional();
            Property(x => x.ProjectFundTypeId).HasColumnName("ProjectFundTypeId").IsOptional();
            Property(x => x.ProjectFundType).HasColumnName("ProjectFundType").IsRequired().HasMaxLength(150);
            Property(x => x.ProjectFundDateLastUpdated).HasColumnName("ProjectFundDateLastUpdated").IsOptional();
            Property(x => x.ProjectFundLastUpdatedBy).HasColumnName("ProjectFundLastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.ProjectFundLastUpdatedByName).HasColumnName("ProjectFundLastUpdatedByName").IsOptional().HasMaxLength(129);
            Property(x => x.ProjectFundDisplayId).HasColumnName("ProjectFundDisplayId").IsRequired();
            Property(x => x.ProjectFundDateProjectedStart).HasColumnName("ProjectFundDateProjectedStart").IsOptional();
            Property(x => x.ProjectFundDateProjectedEnd).HasColumnName("ProjectFundDateProjectedEnd").IsOptional();
        }
    }

}

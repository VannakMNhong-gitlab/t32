// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_MenteeWorkHistory
    public class Search_VwMenteeWorkHistory
    {
        public Guid MenteeId { get; set; } // MenteeId
        public Guid MenteePersonId { get; set; } // MenteePersonId
        public string FirstName { get; set; } // FirstName
        public string MiddleName { get; set; } // MiddleName
        public string LastName { get; set; } // LastName
        public string FullName { get; set; } // FullName
        public int DisplayId { get; set; } // DisplayId
        public string StudentId { get; set; } // StudentId
        public bool IsTrainingGrantEligible { get; set; } // IsTrainingGrantEligible
        public bool WasRecruitedToLab { get; set; } // WasRecruitedToLab
        public string Department { get; set; } // Department
        public string InstitutionAssociation { get; set; } // InstitutionAssociation
        public DateTime? MenteeDateLastUpdated { get; set; } // MenteeDateLastUpdated
        public string MenteeLastUpdatedBy { get; set; } // MenteeLastUpdatedBy
        public string MenteeLastUpdatedByName { get; set; } // MenteeLastUpdatedByName
        public int? WorkHistoryId { get; set; } // WorkHistoryId
        public int? InstitutionId { get; set; } // InstitutionId
        public string Institution { get; set; } // Institution
        public DateTime? DateStartedPosition { get; set; } // DateStartedPosition
        public DateTime? DateEndedPosition { get; set; } // DateEndedPosition
        public string PositionTitle { get; set; } // PositionTitle
        public string PositionLocation { get; set; } // PositionLocation
        public DateTime? WorkHistDateLastUpdated { get; set; } // WorkHistDateLastUpdated
        public string WorkHistLastUpdatedBy { get; set; } // WorkHistLastUpdatedBy
        public string WorkHistLastUpdatedByName { get; set; } // WorkHistLastUpdatedByName
    }

}

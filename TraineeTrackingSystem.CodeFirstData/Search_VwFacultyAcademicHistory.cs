// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FacultyAcademicHistory
    public class Search_VwFacultyAcademicHistory
    {
        public Guid FacultyId { get; set; } // FacultyId
        public Guid PersonId { get; set; } // PersonId
        public string FirstName { get; set; } // FirstName
        public string MiddleName { get; set; } // MiddleName
        public string LastName { get; set; } // LastName
        public string FullName { get; set; } // FullName
        public int DisplayId { get; set; } // DisplayId
        public string EmployeeId { get; set; } // EmployeeId
        public bool IsActive { get; set; } // IsActive
        public DateTime? FacultyDateLastUpdated { get; set; } // FacultyDateLastUpdated
        public string FacultyLastUpdatedBy { get; set; } // FacultyLastUpdatedBy
        public string FacultyLastUpdatedByName { get; set; } // FacultyLastUpdatedByName
        public string Degree { get; set; } // Degree
        public string AreaOfStudy { get; set; } // AreaOfStudy
        public string ResearchExperience { get; set; } // ResearchExperience
        public DateTime? DegreeDateStarted { get; set; } // DegreeDateStarted
        public DateTime? DegreeDateEnded { get; set; } // DegreeDateEnded
        public DateTime? DateDegreeCompleted { get; set; } // DateDegreeCompleted
        public int? DegreeInstitutionId { get; set; } // DegreeInstitutionId
        public string DegreeInstitution { get; set; } // DegreeInstitution
        public string DegreeInstitutionCity { get; set; } // DegreeInstitutionCity
        public string DegreeInstitutionState { get; set; } // DegreeInstitutionState
        public string DegreeInstitutionCountry { get; set; } // DegreeInstitutionCountry
        public string Comments { get; set; } // Comments
        public DateTime? AcademicDataDateLastUpdated { get; set; } // AcademicDataDateLastUpdated
        public string AcademicDataLastUpdatedBy { get; set; } // AcademicDataLastUpdatedBy
        public string AcademicDataLastUpdatedByName { get; set; } // AcademicDataLastUpdatedByName
    }

}

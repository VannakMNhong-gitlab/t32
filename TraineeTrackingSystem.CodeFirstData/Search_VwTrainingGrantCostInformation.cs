// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingGrantCostInformation
    public class Search_VwTrainingGrantCostInformation
    {
        public Guid TrainingGrantId { get; set; } // TrainingGrantId
        public Guid FundingId { get; set; } // FundingId
        public int? FundingCostId { get; set; } // FundingCostId
        public string SponsorAwardNumber { get; set; } // SponsorAwardNumber
        public string Title { get; set; } // Title
        public DateTime? DateProjectedStart { get; set; } // DateProjectedStart
        public DateTime? DateProjectedEnd { get; set; } // DateProjectedEnd
        public DateTime? DateTimeFundingStarted { get; set; } // DateTimeFundingStarted
        public DateTime? DateTimeFundingEnded { get; set; } // DateTimeFundingEnded
        public int? CostYear { get; set; } // CostYear
        public DateTime? DateCostsStarted { get; set; } // DateCostsStarted
        public DateTime? DateCostsEnded { get; set; } // DateCostsEnded
        public int? BudgetPeriodStatusId { get; set; } // BudgetPeriodStatusId
        public string BudgetPeriodStatus { get; set; } // BudgetPeriodStatus
        public Decimal? CurrentYearDirectCosts { get; set; } // CurrentYearDirectCosts
        public Decimal? TotalDirectCosts { get; set; } // TotalDirectCosts
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public string LastUpdatedByName { get; set; } // LastUpdatedByName
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // OrganizationType
    public class OrganizationType
    {
        public int Id { get; set; } // Id (Primary key)
        public string OrganizationType_ { get; set; } // OrganizationType
        public bool IsDeleted { get; set; } // IsDeleted

        // Reverse navigation
        public virtual ICollection<Organization> Organizations { get; set; } // Organization.FK_Organization_OrganizationType

        public OrganizationType()
        {
            IsDeleted = false;
            Organizations = new List<Organization>();
        }
    }

}

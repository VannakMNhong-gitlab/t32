// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingGrantSupportGrants
    public class Search_VwTrainingGrantSupportGrants
    {
        public Guid Id { get; set; } // Id
        public Guid FundingId { get; set; } // FundingId
        public int? DoctoralLevelId { get; set; } // DoctoralLevelId
        public int? NumPredocPositionsRequested { get; set; } // NumPredocPositionsRequested
        public int? NumPostdocPositionsRequested { get; set; } // NumPostdocPositionsRequested
        public int? NumPredocPositionsAwarded { get; set; } // NumPredocPositionsAwarded
        public int? NumPostdocPositionsAwarded { get; set; } // NumPostdocPositionsAwarded
        public int? PredocSupportMonthsAwarded { get; set; } // PredocSupportMonthsAwarded
        public int? PostdocSupportMonthsAwarded { get; set; } // PostdocSupportMonthsAwarded
        public Guid? PreviousTrainingGrantId { get; set; } // PreviousTrainingGrantId
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public bool IsDeleted { get; set; } // IsDeleted
        public int? ApplicantPoolAcademicYear { get; set; } // ApplicantPoolAcademicYear
        public DateTime? ProjectedStartDate { get; set; } // ProjectedStartDate
    }

}

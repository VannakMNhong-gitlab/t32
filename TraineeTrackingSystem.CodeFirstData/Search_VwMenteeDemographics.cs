// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_MenteeDemographics
    public class Search_VwMenteeDemographics
    {
        public Guid Id { get; set; } // Id
        public Guid PersonId { get; set; } // PersonId
        public string FirstName { get; set; } // FirstName
        public string MiddleName { get; set; } // MiddleName
        public string LastName { get; set; } // LastName
        public string FullName { get; set; } // FullName
        public int DisplayId { get; set; } // DisplayId
        public string StudentId { get; set; } // StudentId
        public bool IsTrainingGrantEligible { get; set; } // IsTrainingGrantEligible
        public bool WasRecruitedToLab { get; set; } // WasRecruitedToLab
        public int? IsFromDisadvantagedBkgd { get; set; } // IsFromDisadvantagedBkgd
        public int? IsIndividualWithDisabilities { get; set; } // IsIndividualWithDisabilities
        public int? IsUnderrepresentedMinority { get; set; } // IsUnderrepresentedMinority
        public int? DepartmentId { get; set; } // DepartmentId
        public string Department { get; set; } // Department
        public string InstitutionAssociation { get; set; } // InstitutionAssociation
        public DateTime? MenteeDateLastUpdated { get; set; } // MenteeDateLastUpdated
        public string MenteeLastUpdatedBy { get; set; } // MenteeLastUpdatedBy
        public string MenteeLastUpdatedByName { get; set; } // MenteeLastUpdatedByName
        public Guid? ProgramId { get; set; } // ProgramId
        public string ProgramTitle { get; set; } // ProgramTitle
        public int? IsPrimaryEmail { get; set; } // IsPrimaryEmail
        public string EmailAddress { get; set; } // EmailAddress
        public string MailingAddress { get; set; } // MailingAddress
        public string AddressLine1 { get; set; } // AddressLine1
        public string AddressLine2 { get; set; } // AddressLine2
        public string AddressLine3 { get; set; } // AddressLine3
        public string AddressCity { get; set; } // AddressCity
        public int? AddressStateId { get; set; } // AddressStateId
        public string AddressState { get; set; } // AddressState
        public string AddressPostalCode { get; set; } // AddressPostalCode
        public string PhoneNumber { get; set; } // PhoneNumber
    }

}

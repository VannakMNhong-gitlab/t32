// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FacultyMentees
    public class Search_VwFacultyMentees
    {
        public Guid TrainingPeriodId { get; set; } // TrainingPeriodId
        public Guid FacultyId { get; set; } // FacultyId
        public Guid MentorPersonId { get; set; } // MentorPersonId
        public string MentorFirstName { get; set; } // MentorFirstName
        public string MentorMiddleName { get; set; } // MentorMiddleName
        public string MentorLastName { get; set; } // MentorLastName
        public string MentorFullName { get; set; } // MentorFullName
        public int? PrimaryOrganizationId { get; set; } // PrimaryOrganizationId
        public string MentorPrimaryOrganization { get; set; } // MentorPrimaryOrganization
        public int? YearStarted { get; set; } // YearStarted
        public int? YearEnded { get; set; } // YearEnded
        public int DoctoralLevelId { get; set; } // DoctoralLevelId
        public Guid MenteePersonId { get; set; } // MenteePersonId
        public string MenteeFirstName { get; set; } // MenteeFirstName
        public string MenteeMiddleName { get; set; } // MenteeMiddleName
        public string MenteeLastName { get; set; } // MenteeLastName
        public string MenteeFullName { get; set; } // MenteeFullName
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FacultyDemographics
    public class Search_VwFacultyDemographics
    {
        public Guid Id { get; set; } // Id
        public Guid PersonId { get; set; } // PersonId
        public string FirstName { get; set; } // FirstName
        public string MiddleName { get; set; } // MiddleName
        public string LastName { get; set; } // LastName
        public string FullName { get; set; } // FullName
        public int DisplayId { get; set; } // DisplayId
        public string EmployeeId { get; set; } // EmployeeId
        public string Rank { get; set; } // Rank
        public int? PositionDepartmentId { get; set; } // PositionDepartmentId
        public string PositionDepartment { get; set; } // PositionDepartment
        public int? PrimaryOrganizationId { get; set; } // PrimaryOrganizationId
        public string PrimaryOrganization { get; set; } // PrimaryOrganization
        public int? PositionOrganizationId { get; set; } // PositionOrganizationId
        public string PositionOrganization { get; set; } // PositionOrganization
        public string SecondaryOrganization { get; set; } // SecondaryOrganization
        public Guid? MentorId { get; set; } // MentorId
        public string MentorFirstName { get; set; } // MentorFirstName
        public string MentorMiddleName { get; set; } // MentorMiddleName
        public string MentorLastName { get; set; } // MentorLastName
        public string MentorFullName { get; set; } // MentorFullName
        public string OtherAffiliations { get; set; } // OtherAffiliations
        public string OtherTitles { get; set; } // OtherTitles
        public bool IsActive { get; set; } // IsActive
        public int? GenderId { get; set; } // GenderId
        public string Gender { get; set; } // Gender
        public int? GenderAtBirthId { get; set; } // GenderAtBirthId
        public string GenderAtBirth { get; set; } // GenderAtBirth
        public int? EthnicityId { get; set; } // EthnicityId
        public string Ethnicity { get; set; } // Ethnicity
        public DateTime? FacultyDateLastUpdated { get; set; } // FacultyDateLastUpdated
        public string FacultyLastUpdatedBy { get; set; } // FacultyLastUpdatedBy
        public string FacultyLastUpdatedByName { get; set; } // FacultyLastUpdatedByName
    }

}

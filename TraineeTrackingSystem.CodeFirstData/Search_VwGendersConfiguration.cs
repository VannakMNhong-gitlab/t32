// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_Genders
    internal class Search_VwGendersConfiguration : EntityTypeConfiguration<Search_VwGenders>
    {
        public Search_VwGendersConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_Genders");
            HasKey(x => new { x.GenderId, x.Gender });

            Property(x => x.GenderId).HasColumnName("GenderId").IsRequired();
            Property(x => x.Gender).HasColumnName("Gender").IsRequired().HasMaxLength(100);
            Property(x => x.TotalGenderUsage).HasColumnName("TotalGenderUsage").IsOptional();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FacultyData
    public class Search_VwFacultyData
    {
        public Guid Id { get; set; } // Id
        public Guid PersonId { get; set; } // PersonId
        public string FirstName { get; set; } // FirstName
        public string MiddleName { get; set; } // MiddleName
        public string LastName { get; set; } // LastName
        public string FullName { get; set; } // FullName
        public int DisplayId { get; set; } // DisplayId
        public string EmployeeId { get; set; } // EmployeeId
        public string Rank { get; set; } // Rank
        public string PrimaryOrganization { get; set; } // PrimaryOrganization
        public string SecondaryOrganization { get; set; } // SecondaryOrganization
        public string ResearchInterest { get; set; } // ResearchInterest
        public string Degree { get; set; } // Degree
        public string AreaOfStudy { get; set; } // AreaOfStudy
        public int? DegreeYear { get; set; } // DegreeYear
        public int? DegreeInstitutionId { get; set; } // DegreeInstitutionId
        public string DegreeInstitution { get; set; } // DegreeInstitution
        public string DegreeInstitutionStateProvince { get; set; } // DegreeInstitutionStateProvince
        public string DegreeInstitutionCountry { get; set; } // DegreeInstitutionCountry
        public string OtherAffiliations { get; set; } // OtherAffiliations
        public string OtherTitles { get; set; } // OtherTitles
        public bool IsActive { get; set; } // IsActive
        public DateTime? FacultyDateLastUpdated { get; set; } // FacultyDateLastUpdated
        public string FacultyLastUpdatedBy { get; set; } // FacultyLastUpdatedBy
        public string FacultyLastUpdatedByName { get; set; } // FacultyLastUpdatedByName
    }

}

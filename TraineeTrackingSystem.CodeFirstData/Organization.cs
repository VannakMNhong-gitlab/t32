// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Organization
    public class Organization
    {
        public int Id { get; set; } // Id (Primary key)
        public string OrganizationId { get; set; } // OrganizationId
        public int OrganizationTypeId { get; set; } // OrganizationTypeId
        public string DisplayName { get; set; } // DisplayName
        public string HrName { get; set; } // HRName
        public int? ParentOrganizationId { get; set; } // ParentOrganizationId
        public bool IsDeleted { get; set; } // IsDeleted

        // Reverse navigation
        public virtual ICollection<Applicant> Applicants { get; set; } // Applicant.FK_Applicant_Organization
        public virtual ICollection<Faculty> Faculties_PrimaryOrganizationId { get; set; } // Faculty.FK_Faculty_PrimaryOrganization
        public virtual ICollection<Faculty> Faculties_SecondaryOrganizationId { get; set; } // Faculty.FK_Faculty_SecondaryOrganization
        public virtual ICollection<Faculty> Faculties_TiuId { get; set; } // Faculty.FK_Faculty_TIU
        public virtual ICollection<Mentee> Mentees { get; set; } // Mentee.FK_Mentee_Organization
        public virtual ICollection<TrainingGrant> TrainingGrants { get; set; } // Many to many mapping

        // Foreign keys
        public virtual OrganizationType OrganizationType { get; set; } // FK_Organization_OrganizationType

        public Organization()
        {
            IsDeleted = false;
            Applicants = new List<Applicant>();
            Faculties_PrimaryOrganizationId = new List<Faculty>();
            Faculties_SecondaryOrganizationId = new List<Faculty>();
            Faculties_TiuId = new List<Faculty>();
            Mentees = new List<Mentee>();
            TrainingGrants = new List<TrainingGrant>();
        }
    }

}

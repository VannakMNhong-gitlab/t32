// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // AcademicHistory
    public class AcademicHistory
    {
        public Guid Id { get; set; } // Id (Primary key)
        public Guid PersonId { get; set; } // PersonId
        public int InstitutionId { get; set; } // InstitutionId
        public int? YearStarted { get; set; } // YearStarted
        public int? YearEnded { get; set; } // YearEnded
        public int? AcademicDegreeId { get; set; } // AcademicDegreeId
        public string AreaOfStudy { get; set; } // AreaOfStudy
        public int? YearDegreeCompleted { get; set; } // YearDegreeCompleted
        public bool IsUndergraduate { get; set; } // IsUndergraduate
        public string ResearchProjectTitle { get; set; } // ResearchProjectTitle
        public string DoctoralThesis { get; set; } // DoctoralThesis
        public string ResearchAdvisor { get; set; } // ResearchAdvisor
        public bool IsResidency { get; set; } // IsResidency
        public int? ResidencyInstitutionId { get; set; } // ResidencyInstitutionId
        public string ResidencySpecialization { get; set; } // ResidencySpecialization
        public string ResidencyPgy { get; set; } // ResidencyPGY
        public string ResidencyAdvisor { get; set; } // ResidencyAdvisor
        public int? ResidencyYearStarted { get; set; } // ResidencyYearStarted
        public int? ResidencyYearEnded { get; set; } // ResidencyYearEnded
        public bool IsNonMedicalTransfer { get; set; } // IsNonMedicalTransfer
        public bool IsPhDTransfer { get; set; } // IsPhDTransfer
        public string LabResearch { get; set; } // LabResearch
        public int? TrainingDoctoralLevelId { get; set; } // TrainingDoctoralLevelId
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public string Comments { get; set; } // Comments
        public bool IsDeleted { get; set; } // IsDeleted
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded
        public DateTime? DateDegreeCompleted { get; set; } // DateDegreeCompleted

        // Foreign keys
        public virtual AcademicDegree AcademicDegree { get; set; } // FK_AcademicHistory_AcademicDegree
        public virtual DoctoralLevel DoctoralLevel { get; set; } // FK_AcademicHistory_DoctoralLevel
        public virtual Institution Institution_InstitutionId { get; set; } // FK_AcademicHistory_Institution
        public virtual Institution Institution_ResidencyInstitutionId { get; set; } // FK_AcademicHistory_ResidencyInstitution
        public virtual Person Person { get; set; } // FK_AcademicHistory_Person

        public AcademicHistory()
        {
            Id = System.Guid.NewGuid();
            IsUndergraduate = false;
            IsResidency = false;
            IsNonMedicalTransfer = false;
            IsPhDTransfer = false;
            IsDeleted = false;
        }
    }

}

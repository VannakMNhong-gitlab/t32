// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_Logins
    public class Search_VwLogins
    {
        public int LoginId { get; set; } // LoginId
        public string ExternalId { get; set; } // ExternalId
        public string Username { get; set; } // Username
        public Guid? PersonId { get; set; } // PersonId
        public string FirstName { get; set; } // FirstName
        public string LastName { get; set; } // LastName
        public string MiddleName { get; set; } // MiddleName
        public DateTime? DateCreated { get; set; } // DateCreated
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingGrantFacultyWithResearchInterests
    public class Search_VwTrainingGrantFacultyWithResearchInterests
    {
        public Guid TrainingGrantId { get; set; } // TrainingGrantId
        public Guid FundingId { get; set; } // FundingId
        public string Title { get; set; } // Title
        public int TrainingGrantStatusId { get; set; } // TrainingGrantStatusId
        public string TrainingGrantStatus { get; set; } // TrainingGrantStatus
        public DateTime? DateProjectedStart { get; set; } // DateProjectedStart
        public DateTime? DateProjectedEnd { get; set; } // DateProjectedEnd
        public DateTime? DateTimeFundingStarted { get; set; } // DateTimeFundingStarted
        public DateTime? DateTimeFundingEnded { get; set; } // DateTimeFundingEnded
        public int? FundingTypeId { get; set; } // FundingTypeId
        public string FundingType { get; set; } // FundingType
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public string LastUpdatedByName { get; set; } // LastUpdatedByName
        public Guid FacultyId { get; set; } // FacultyId
        public Guid? FacultyPersonId { get; set; } // FacultyPersonId
        public string FacultyLastName { get; set; } // FacultyLastName
        public string FacultyFirstName { get; set; } // FacultyFirstName
        public string FacultyFullName { get; set; } // FacultyFullName
        public int? FacultyPrimaryRoleId { get; set; } // FacultyPrimaryRoleId
        public string FacultyPrimaryRole { get; set; } // FacultyPrimaryRole
        public int? FacultySecondaryRoleId { get; set; } // FacultySecondaryRoleId
        public string FacultySecondaryRole { get; set; } // FacultySecondaryRole
        public int ResearchInterestId { get; set; } // ResearchInterestId
        public string ResearchInterest { get; set; } // ResearchInterest
        public DateTime? ResearchInterestDateLastUpdated { get; set; } // ResearchInterestDateLastUpdated
        public string ResearchInterestLastUpdatedBy { get; set; } // ResearchInterestLastUpdatedBy
        public string ResearchInterestLastUpdatedByName { get; set; } // ResearchInterestLastUpdatedByName
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_Programs
    internal class Search_VwProgramsConfiguration : EntityTypeConfiguration<Search_VwPrograms>
    {
        public Search_VwProgramsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_Programs");
            HasKey(x => new { x.ProgramTitle, x.ProgramDisplayId, x.ProgramId });

            Property(x => x.ProgramId).HasColumnName("ProgramId").IsRequired();
            Property(x => x.ProgramTitle).HasColumnName("ProgramTitle").IsRequired().HasMaxLength(250);
            Property(x => x.ProgramDisplayId).HasColumnName("ProgramDisplayId").IsRequired();
            Property(x => x.TotalProgramUsage).HasColumnName("TotalProgramUsage").IsOptional();
        }
    }

}

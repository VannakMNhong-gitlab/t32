// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_UsageOfOrganizations
    internal class Search_VwUsageOfOrganizationsConfiguration : EntityTypeConfiguration<Search_VwUsageOfOrganizations>
    {
        public Search_VwUsageOfOrganizationsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_UsageOfOrganizations");
            HasKey(x => new { x.OrganizationId, x.OrganizationName });

            Property(x => x.OrganizationId).HasColumnName("OrganizationId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.OrganizationName).HasColumnName("OrganizationName").IsRequired().HasMaxLength(250);
            Property(x => x.TotalApplicantDepts).HasColumnName("TotalApplicantDepts").IsOptional();
            Property(x => x.TotalFacultyOrgs).HasColumnName("TotalFacultyOrgs").IsOptional();
            Property(x => x.TotalMenteeDepts).HasColumnName("TotalMenteeDepts").IsOptional();
            Property(x => x.TotalTrainingGrantDepts).HasColumnName("TotalTrainingGrantDepts").IsOptional();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_ApplicantDemographics
    internal class Search_VwApplicantDemographicsConfiguration : EntityTypeConfiguration<Search_VwApplicantDemographics>
    {
        public Search_VwApplicantDemographicsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_ApplicantDemographics");
            HasKey(x => new { x.Id, x.IsTrainingGrantEligible, x.ApplicantId, x.PersonId });

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.FirstName).HasColumnName("FirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MiddleName).HasColumnName("MiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.LastName).HasColumnName("LastName").IsOptional().HasMaxLength(75);
            Property(x => x.FullName).HasColumnName("FullName").IsOptional().HasMaxLength(178);
            Property(x => x.ApplicantId).HasColumnName("ApplicantId").IsRequired();
            Property(x => x.IsTrainingGrantEligible).HasColumnName("IsTrainingGrantEligible").IsRequired();
            Property(x => x.IsFromDisadvantagedBkgd).HasColumnName("IsFromDisadvantagedBkgd").IsOptional();
            Property(x => x.IsIndividualWithDisabilities).HasColumnName("IsIndividualWithDisabilities").IsOptional();
            Property(x => x.IsUnderrepresentedMinority).HasColumnName("IsUnderrepresentedMinority").IsOptional();
            Property(x => x.DepartmentId).HasColumnName("DepartmentId").IsOptional();
            Property(x => x.Department).HasColumnName("Department").IsOptional().HasMaxLength(250);
            Property(x => x.ApplicantTypeId).HasColumnName("ApplicantTypeId").IsOptional();
            Property(x => x.ApplicantType).HasColumnName("ApplicantType").IsOptional().HasMaxLength(150);
            Property(x => x.YearEntered).HasColumnName("YearEntered").IsOptional();
            Property(x => x.ApplicantDateLastUpdated).HasColumnName("ApplicantDateLastUpdated").IsOptional();
            Property(x => x.ApplicantLastUpdatedBy).HasColumnName("ApplicantLastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.ApplicantLastUpdatedByName).HasColumnName("ApplicantLastUpdatedByName").IsOptional().HasMaxLength(129);
        }
    }

}

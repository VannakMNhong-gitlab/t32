// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // MenteeSupport
    internal class MenteeSupportConfiguration : EntityTypeConfiguration<MenteeSupport>
    {
        public MenteeSupportConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".MenteeSupport");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.InstitutionId).HasColumnName("InstitutionId").IsRequired();
            Property(x => x.DateStarted).HasColumnName("DateStarted").IsOptional();
            Property(x => x.DateEnded).HasColumnName("DateEnded").IsOptional();
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
            Property(x => x.SupportSourceText).HasColumnName("SupportSourceText").IsOptional();

            // Foreign keys
            HasRequired(a => a.Person).WithMany(b => b.MenteeSupports).HasForeignKey(c => c.PersonId); // FK_MenteeSupport_Person
            HasRequired(a => a.Institution).WithMany(b => b.MenteeSupports).HasForeignKey(c => c.InstitutionId); // FK_MenteeSupport_Institution
            HasMany(t => t.SupportSources).WithMany(t => t.MenteeSupports).Map(m => 
            {
                m.ToTable("MenteeSupportSupportSource");
                m.MapLeftKey("MenteeSupportId");
                m.MapRightKey("SupportSourceId");
            });
        }
    }

}

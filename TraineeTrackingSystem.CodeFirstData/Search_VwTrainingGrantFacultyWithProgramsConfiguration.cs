// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingGrantFacultyWithPrograms
    internal class Search_VwTrainingGrantFacultyWithProgramsConfiguration : EntityTypeConfiguration<Search_VwTrainingGrantFacultyWithPrograms>
    {
        public Search_VwTrainingGrantFacultyWithProgramsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_TrainingGrantFacultyWithPrograms");
            HasKey(x => new { x.FacultyPersonId, x.TrainingGrantStatusId, x.FundingId, x.FacultyId, x.TrainingGrantId });

            Property(x => x.TrainingGrantId).HasColumnName("TrainingGrantId").IsRequired();
            Property(x => x.FundingId).HasColumnName("FundingId").IsRequired();
            Property(x => x.Title).HasColumnName("Title").IsOptional();
            Property(x => x.TrainingGrantStatusId).HasColumnName("TrainingGrantStatusId").IsRequired();
            Property(x => x.TrainingGrantStatus).HasColumnName("TrainingGrantStatus").IsOptional().HasMaxLength(150);
            Property(x => x.DateProjectedStart).HasColumnName("DateProjectedStart").IsOptional();
            Property(x => x.DateProjectedEnd).HasColumnName("DateProjectedEnd").IsOptional();
            Property(x => x.DateTimeFundingStarted).HasColumnName("DateTimeFundingStarted").IsOptional();
            Property(x => x.DateTimeFundingEnded).HasColumnName("DateTimeFundingEnded").IsOptional();
            Property(x => x.FundingTypeId).HasColumnName("FundingTypeId").IsOptional();
            Property(x => x.FundingType).HasColumnName("FundingType").IsOptional().HasMaxLength(150);
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.LastUpdatedByName).HasColumnName("LastUpdatedByName").IsOptional().HasMaxLength(129);
            Property(x => x.FacultyId).HasColumnName("FacultyId").IsRequired();
            Property(x => x.FacultyPersonId).HasColumnName("FacultyPersonId").IsRequired();
            Property(x => x.FacultyLastName).HasColumnName("FacultyLastName").IsOptional().HasMaxLength(75);
            Property(x => x.FacultyFirstName).HasColumnName("FacultyFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.FacultyFullName).HasColumnName("FacultyFullName").IsOptional().HasMaxLength(178);
            Property(x => x.FacultyPrimaryOrgId).HasColumnName("FacultyPrimaryOrgId").IsOptional();
            Property(x => x.FacultyPrimaryOrg).HasColumnName("FacultyPrimaryOrg").IsOptional().HasMaxLength(250);
            Property(x => x.FacultyPrimaryRoleId).HasColumnName("FacultyPrimaryRoleId").IsOptional();
            Property(x => x.FacultyPrimaryRole).HasColumnName("FacultyPrimaryRole").IsOptional().HasMaxLength(150);
            Property(x => x.FacultySecondaryRoleId).HasColumnName("FacultySecondaryRoleId").IsOptional();
            Property(x => x.FacultySecondaryRole).HasColumnName("FacultySecondaryRole").IsOptional().HasMaxLength(150);
            Property(x => x.SponsorId).HasColumnName("SponsorId").IsOptional();
            Property(x => x.SponsorName).HasColumnName("SponsorName").IsOptional().HasMaxLength(250);
            Property(x => x.SponsorAwardNumber).HasColumnName("SponsorAwardNumber").IsOptional().HasMaxLength(50);
            Property(x => x.FundingFacultyDateLastUpdated).HasColumnName("FundingFacultyDateLastUpdated").IsOptional();
            Property(x => x.FundingFacultyLastUpdatedBy).HasColumnName("FundingFacultyLastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.FundingFacultyLastUpdatedByName).HasColumnName("FundingFacultyLastUpdatedByName").IsOptional().HasMaxLength(129);
            Property(x => x.FacultyProgramId).HasColumnName("FacultyProgramId").IsOptional();
            Property(x => x.FacultyProgramDisplayId).HasColumnName("FacultyProgramDisplayId").IsOptional();
            Property(x => x.FacultyProgramTitle).HasColumnName("FacultyProgramTitle").IsOptional().HasMaxLength(250);
        }
    }

}

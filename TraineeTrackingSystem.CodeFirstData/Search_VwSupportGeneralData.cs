// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_SupportGeneralData
    public class Search_VwSupportGeneralData
    {
        public Guid SupportId { get; set; } // SupportId
        public string SupportTitle { get; set; } // SupportTitle
        public int SupportDisplayId { get; set; } // SupportDisplayId
        public int? SupportTypeId { get; set; } // SupportTypeId
        public string SupportType { get; set; } // SupportType
        public string SupportTypeAbbreviation { get; set; } // SupportTypeAbbreviation
        public string SupportTypeFullName { get; set; } // SupportTypeFullName
        public int SupportStatusId { get; set; } // SupportStatusId
        public string SupportStatus { get; set; } // SupportStatus
        public string SupportNumber { get; set; } // SupportNumber
        public int? SupportOrganizationId { get; set; } // SupportOrganizationId
        public string SupportOrganization { get; set; } // SupportOrganization
        public string SupportOrganizationAbbreviation { get; set; } // SupportOrganizationAbbreviation
        public string SupportOrganizationFullName { get; set; } // SupportOrganizationFullName
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded
        public DateTime? DateCreated { get; set; } // DateCreated
        public string CreatedBy { get; set; } // CreatedBy
        public string CreatedByName { get; set; } // CreatedByName
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public string LastUpdatedByName { get; set; } // LastUpdatedByName
    }

}

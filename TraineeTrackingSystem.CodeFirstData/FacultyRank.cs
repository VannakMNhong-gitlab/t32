// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // FacultyRank
    public class FacultyRank
    {
        public int Id { get; set; } // Id (Primary key)
        public string Rank { get; set; } // Rank
        public bool IsDeleted { get; set; } // IsDeleted

        // Reverse navigation
        public virtual ICollection<Faculty> Faculties { get; set; } // Faculty.FK_Faculty_FacultyRank
        public virtual ICollection<FacultyAppointment> FacultyAppointments { get; set; } // FacultyAppointment.FK_FacultyAppointment_FacultyRank

        public FacultyRank()
        {
            IsDeleted = false;
            Faculties = new List<Faculty>();
            FacultyAppointments = new List<FacultyAppointment>();
        }
    }

}

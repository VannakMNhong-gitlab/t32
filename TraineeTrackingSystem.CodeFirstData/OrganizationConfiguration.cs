// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Organization
    internal class OrganizationConfiguration : EntityTypeConfiguration<Organization>
    {
        public OrganizationConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".Organization");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.OrganizationId).HasColumnName("OrganizationId").IsRequired().HasMaxLength(100);
            Property(x => x.OrganizationTypeId).HasColumnName("OrganizationTypeId").IsRequired();
            Property(x => x.DisplayName).HasColumnName("DisplayName").IsRequired().HasMaxLength(250);
            Property(x => x.HrName).HasColumnName("HRName").IsOptional().HasMaxLength(250);
            Property(x => x.ParentOrganizationId).HasColumnName("ParentOrganizationId").IsOptional();
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();

            // Foreign keys
            HasRequired(a => a.OrganizationType).WithMany(b => b.Organizations).HasForeignKey(c => c.OrganizationTypeId); // FK_Organization_OrganizationType
            HasMany(t => t.TrainingGrants).WithMany(t => t.Organizations).Map(m => 
            {
                m.ToTable("TrainingGrantDepartment");
                m.MapLeftKey("OrganizationId");
                m.MapRightKey("TrainingGrantId");
            });
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingGrantMentees
    public class Search_VwTrainingGrantMentees
    {
        public Guid TrainingGrantId { get; set; } // TrainingGrantId
        public Guid FundingId { get; set; } // FundingId
        public string TrainingGrantTitle { get; set; } // TrainingGrantTitle
        public DateTime? TrainingGrantProjectedStartDate { get; set; } // TrainingGrantProjectedStartDate
        public DateTime? TrainingGrantProjectedEndDate { get; set; } // TrainingGrantProjectedEndDate
        public DateTime? TrainingGrantDateStarted { get; set; } // TrainingGrantDateStarted
        public DateTime? TrainingGrantDateEnded { get; set; } // TrainingGrantDateEnded
        public int? TrainingGrantTypeId { get; set; } // TrainingGrantTypeId
        public string TrainingGrantType { get; set; } // TrainingGrantType
        public Guid FacultyId { get; set; } // FacultyId
        public Guid FacultyPersonId { get; set; } // FacultyPersonId
        public string FacultyLastName { get; set; } // FacultyLastName
        public string FacultyFirstName { get; set; } // FacultyFirstName
        public string FacultyFullName { get; set; } // FacultyFullName
        public Guid MenteeId { get; set; } // MenteeId
        public string StudentId { get; set; } // StudentId
        public Guid MenteePersonId { get; set; } // MenteePersonId
        public string MenteeLastName { get; set; } // MenteeLastName
        public string MenteeFirstName { get; set; } // MenteeFirstName
        public string MenteeFullName { get; set; } // MenteeFullName
        public int? TrainingPeriodYearStarted { get; set; } // TrainingPeriodYearStarted
        public int? TrainingPeriodYearEnded { get; set; } // TrainingPeriodYearEnded
        public int? MenteeTypeId { get; set; } // MenteeTypeId
        public string MenteeType { get; set; } // MenteeType
        public int? MenteeDepartmentId { get; set; } // MenteeDepartmentId
        public string MenteeDepartment { get; set; } // MenteeDepartment
        public bool IsTrainingGrantEligible { get; set; } // IsTrainingGrantEligible
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Funding
    public class Funding
    {
        public Guid Id { get; set; } // Id (Primary key)
        public string SponsorAwardNumber { get; set; } // SponsorAwardNumber
        public string Title { get; set; } // Title
        public int FundingStatusId { get; set; } // FundingStatusId
        public int? SponsorId { get; set; } // SponsorId
        public string SponsorReferenceNumber { get; set; } // SponsorReferenceNumber
        public string GrtNumber { get; set; } // GRTNumber
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded
        public int? FundingTypeId { get; set; } // FundingTypeId
        public bool IsRenewal { get; set; } // IsRenewal
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public bool IsDeleted { get; set; } // IsDeleted
        public string PrimeAward { get; set; } // PrimeAward
        public int DisplayId { get; set; } // DisplayId
        public DateTime? DateProjectedStart { get; set; } // DateProjectedStart
        public DateTime? DateProjectedEnd { get; set; } // DateProjectedEnd
        public Guid? ProgramProjectPdId { get; set; } // ProgramProjectPDId
        public int? PrimeSponsorId { get; set; } // PrimeSponsorId
        public Guid? PrimeAwardPiId { get; set; } // PrimeAwardPIId

        // Reverse navigation
        public virtual ICollection<FundingDirectCost> FundingDirectCosts { get; set; } // FundingDirectCost.FK_FundingDirectCost_Funding
        public virtual ICollection<FundingFaculty> FundingFaculties { get; set; } // FundingFaculty.FK_FundingFaculty_Funding
        public virtual ICollection<FundingProgramProject> FundingProgramProjects { get; set; } // Many to many mapping
        public virtual ICollection<TrainingGrant> TrainingGrants { get; set; } // TrainingGrant.FK_TrainingGrant_Funding
        public virtual ICollection<TrainingGrantSupportFunding> TrainingGrantSupportFundings { get; set; } // Many to many mapping

        // Foreign keys
        public virtual Faculty Faculty_PrimeAwardPiId { get; set; } // FK_Funding_FacultyPrimeAwardPI
        public virtual Faculty Faculty_ProgramProjectPdId { get; set; } // FK_Funding_FacultyProgPD
        public virtual FundingStatu FundingStatu { get; set; } // FK_Funding_FundingStatus
        public virtual FundingType FundingType { get; set; } // FK_Funding_FundingType
        public virtual Institution Institution_PrimeSponsorId { get; set; } // FK_Funding_InstitutionPrimeSponsor
        public virtual Institution Institution_SponsorId { get; set; } // FK_Funding_Sponsor

        public Funding()
        {
            Id = System.Guid.NewGuid();
            FundingStatusId = 1;
            IsRenewal = false;
            IsDeleted = false;
            FundingDirectCosts = new List<FundingDirectCost>();
            FundingFaculties = new List<FundingFaculty>();
            FundingProgramProjects = new List<FundingProgramProject>();
            TrainingGrants = new List<TrainingGrant>();
            TrainingGrantSupportFundings = new List<TrainingGrantSupportFunding>();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Reports
    internal class ReportConfiguration : EntityTypeConfiguration<Report>
    {
        public ReportConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".Reports");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Name).HasColumnName("Name").IsOptional();
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
            Property(x => x.Title).HasColumnName("Title").IsOptional();
            Property(x => x.ModifiedDate).HasColumnName("ModifiedDate").IsRequired();
            Property(x => x.TypeId).HasColumnName("Type_Id").IsOptional();
            Property(x => x.ReportPath).HasColumnName("ReportPath").IsOptional();
            Property(x => x.Description).HasColumnName("Description").IsOptional();
            Property(x => x.StatusTypeId).HasColumnName("StatusTypeId").IsOptional();
            Property(x => x.IsPredoc).HasColumnName("IsPredoc").IsRequired();
            Property(x => x.IsPostdoc).HasColumnName("IsPostdoc").IsRequired();
            Property(x => x.IsNewOnly).HasColumnName("IsNewOnly").IsRequired();
            Property(x => x.IsRenewalOnly).HasColumnName("IsRenewalOnly").IsRequired();

            // Foreign keys
            HasOptional(a => a.ReportType).WithMany(b => b.Reports).HasForeignKey(c => c.TypeId); // FK_dbo.Reports_dbo.ReportTypes_Type_Id
            HasOptional(a => a.StatusType).WithMany(b => b.Reports).HasForeignKey(c => c.StatusTypeId); // FK_dbo.Reports_dbo.StatusTypes_StatusTypeId
        }
    }

}

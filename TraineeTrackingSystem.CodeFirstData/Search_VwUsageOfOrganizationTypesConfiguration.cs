// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_UsageOfOrganizationTypes
    internal class Search_VwUsageOfOrganizationTypesConfiguration : EntityTypeConfiguration<Search_VwUsageOfOrganizationTypes>
    {
        public Search_VwUsageOfOrganizationTypesConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_UsageOfOrganizationTypes");
            HasKey(x => new { x.OrganizationType, x.OrganizationTypeId });

            Property(x => x.OrganizationTypeId).HasColumnName("OrganizationTypeId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.OrganizationType).HasColumnName("OrganizationType").IsRequired().HasMaxLength(150);
            Property(x => x.TotalOrganization).HasColumnName("TotalOrganization").IsOptional();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingPeriodPrograms
    public class Search_VwTrainingPeriodPrograms
    {
        public Guid TrainingPeriodId { get; set; } // TrainingPeriodId
        public Guid PersonId { get; set; } // PersonId
        public int? InstitutionId { get; set; } // InstitutionId
        public string InstitutionIdentifier { get; set; } // InstitutionIdentifier
        public string InstitutionName { get; set; } // InstitutionName
        public string InstitutionCity { get; set; } // InstitutionCity
        public int? InstitutionStateId { get; set; } // InstitutionStateId
        public string InstitutionStateAbbreviation { get; set; } // InstitutionStateAbbreviation
        public string InstitutionStateFullName { get; set; } // InstitutionStateFullName
        public int? InstitutionCountryId { get; set; } // InstitutionCountryId
        public string InstitutionCountry { get; set; } // InstitutionCountry
        public int DoctoralLevelId { get; set; } // DoctoralLevelId
        public string DoctoralLevel { get; set; } // DoctoralLevel
        public int? YearStarted { get; set; } // YearStarted
        public int? YearEnded { get; set; } // YearEnded
        public int? AcademicDegreeId { get; set; } // AcademicDegreeId
        public string AcademicDegree { get; set; } // AcademicDegree
        public int? DegreeSoughtId { get; set; } // DegreeSoughtId
        public string DegreeSought { get; set; } // DegreeSought
        public string ResearchProjectTitle { get; set; } // ResearchProjectTitle
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public string LastUpdatedByName { get; set; } // LastUpdatedByName
        public Guid ProgramId { get; set; } // ProgramId
        public int ProgramDisplayId { get; set; } // ProgramDisplayId
        public string ProgramTitle { get; set; } // ProgramTitle
    }

}

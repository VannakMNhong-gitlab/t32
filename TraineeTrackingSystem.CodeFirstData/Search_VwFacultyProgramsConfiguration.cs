// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FacultyPrograms
    internal class Search_VwFacultyProgramsConfiguration : EntityTypeConfiguration<Search_VwFacultyPrograms>
    {
        public Search_VwFacultyProgramsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_FacultyPrograms");
            HasKey(x => new { x.ProgramId, x.ProgramDisplayId, x.FacultyPersonId, x.FacultyId, x.EmployeeId, x.ProgramTitle });

            Property(x => x.FacultyId).HasColumnName("FacultyId").IsRequired();
            Property(x => x.FacultyPersonId).HasColumnName("FacultyPersonId").IsRequired();
            Property(x => x.FirstName).HasColumnName("FirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MiddleName).HasColumnName("MiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.LastName).HasColumnName("LastName").IsOptional().HasMaxLength(75);
            Property(x => x.FullName).HasColumnName("FullName").IsOptional().HasMaxLength(178);
            Property(x => x.EmployeeId).HasColumnName("EmployeeId").IsRequired().HasMaxLength(50);
            Property(x => x.ProgramId).HasColumnName("ProgramId").IsRequired();
            Property(x => x.ProgramDisplayId).HasColumnName("ProgramDisplayId").IsRequired();
            Property(x => x.ProgramTitle).HasColumnName("ProgramTitle").IsRequired().HasMaxLength(250);
        }
    }

}

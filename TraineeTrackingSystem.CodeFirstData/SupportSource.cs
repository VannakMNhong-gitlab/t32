// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // SupportSource
    public class SupportSource
    {
        public int Id { get; set; } // Id (Primary key)
        public string SupportTitle { get; set; } // SupportTitle
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public bool IsDeleted { get; set; } // IsDeleted

        // Reverse navigation
        public virtual ICollection<MenteeSupport> MenteeSupports { get; set; } // Many to many mapping
        public virtual ICollection<WorkHistory> WorkHistories { get; set; } // Many to many mapping

        public SupportSource()
        {
            IsDeleted = false;
            MenteeSupports = new List<MenteeSupport>();
            WorkHistories = new List<WorkHistory>();
        }
    }

}

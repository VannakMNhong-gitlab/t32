// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    public class TTSEntities : DbContext, ITTSEntities
    {
        public IDbSet<AcademicDegree> AcademicDegrees { get; set; } // AcademicDegree
        public IDbSet<AcademicHistory> AcademicHistories { get; set; } // AcademicHistory
        public IDbSet<Applicant> Applicants { get; set; } // Applicant
        public IDbSet<BudgetPeriodStatu> BudgetPeriodStatus { get; set; } // BudgetPeriodStatus
        public IDbSet<ClinicalFacultyPathway> ClinicalFacultyPathways { get; set; } // ClinicalFacultyPathway
        public IDbSet<ContactAddress> ContactAddresses { get; set; } // ContactAddress
        public IDbSet<ContactEmail> ContactEmails { get; set; } // ContactEmail
        public IDbSet<ContactEntityType> ContactEntityTypes { get; set; } // ContactEntityType
        public IDbSet<ContactPhone> ContactPhones { get; set; } // ContactPhone
        public IDbSet<Country> Countries { get; set; } // Country
        public IDbSet<DoctoralLevel> DoctoralLevels { get; set; } // DoctoralLevel
        public IDbSet<Ethnicity> Ethnicities { get; set; } // Ethnicity
        public IDbSet<Faculty> Faculties { get; set; } // Faculty
        public IDbSet<FacultyAcademicData> FacultyAcademicDatas { get; set; } // FacultyAcademicData
        public IDbSet<FacultyAppointment> FacultyAppointments { get; set; } // FacultyAppointment
        public IDbSet<FacultyRank> FacultyRanks { get; set; } // FacultyRank
        public IDbSet<FacultyResearchInterest> FacultyResearchInterests { get; set; } // FacultyResearchInterest
        public IDbSet<FacultyTrack> FacultyTracks { get; set; } // FacultyTrack
        public IDbSet<Funding> Fundings { get; set; } // Funding
        public IDbSet<FundingDirectCost> FundingDirectCosts { get; set; } // FundingDirectCost
        public IDbSet<FundingFaculty> FundingFaculties { get; set; } // FundingFaculty
        public IDbSet<FundingProgramProject> FundingProgramProjects { get; set; } // FundingProgramProjects
        public IDbSet<FundingRole> FundingRoles { get; set; } // FundingRole
        public IDbSet<FundingStatu> FundingStatus { get; set; } // FundingStatus
        public IDbSet<FundingType> FundingTypes { get; set; } // FundingType
        public IDbSet<Gender> Genders { get; set; } // Gender
        public IDbSet<GradeRecord> GradeRecords { get; set; } // GradeRecord
        public IDbSet<Institution> Institutions { get; set; } // Institution
        public IDbSet<InstitutionAssociation> InstitutionAssociations { get; set; } // InstitutionAssociation
        public IDbSet<InstitutionType> InstitutionTypes { get; set; } // InstitutionType
        public IDbSet<LeaveOfAbsence> LeaveOfAbsences { get; set; } // LeaveOfAbsence
        public IDbSet<Login> Logins { get; set; } // Logins
        public IDbSet<MedicalRank> MedicalRanks { get; set; } // MedicalRank
        public IDbSet<Mentee> Mentees { get; set; } // Mentee
        public IDbSet<MenteeSupport> MenteeSupports { get; set; } // MenteeSupport
        public IDbSet<Organization> Organizations { get; set; } // Organization
        public IDbSet<OrganizationType> OrganizationTypes { get; set; } // OrganizationType
        public IDbSet<OsuTimeline> OsuTimelines { get; set; } // OSUTimeline
        public IDbSet<Person> People { get; set; } // Person
        public IDbSet<PersonRole> PersonRoles { get; set; } // PersonRoles
        public IDbSet<Program> Programs { get; set; } // Program
        public IDbSet<Publication> Publications { get; set; } // Publication
        public IDbSet<Report> Reports { get; set; } // Reports
        public IDbSet<Reports_VwT32Table10> Reports_VwT32Table10 { get; set; } // vw_T32Table10
        public IDbSet<Reports_VwT32Table1N> Reports_VwT32Table1N { get; set; } // vw_T32Table1N
        public IDbSet<Reports_VwT32Table1R> Reports_VwT32Table1R { get; set; } // vw_T32Table1R
        public IDbSet<Reports_VwT32Table3> Reports_VwT32Table3 { get; set; } // vw_T32Table3
        public IDbSet<Reports_VwT32Table4> Reports_VwT32Table4 { get; set; } // vw_T32Table4
        public IDbSet<Reports_VwT32Table5A> Reports_VwT32Table5A { get; set; } // vw_T32Table5A
        public IDbSet<Reports_VwT32Table5B> Reports_VwT32Table5B { get; set; } // vw_T32Table5B
        public IDbSet<Reports_VwT32Table7A> Reports_VwT32Table7A { get; set; } // vw_T32Table7A
        public IDbSet<Reports_VwT32Table7B> Reports_VwT32Table7B { get; set; } // vw_T32Table7B
        public IDbSet<Reports_VwT32Table8A> Reports_VwT32Table8A { get; set; } // vw_T32Table8A
        public IDbSet<Reports_VwT32Table8B> Reports_VwT32Table8B { get; set; } // vw_T32Table8B
        public IDbSet<Reports_VwT32Table9A> Reports_VwT32Table9A { get; set; } // vw_T32Table9A
        public IDbSet<Reports_VwT32Table9B> Reports_VwT32Table9B { get; set; } // vw_T32Table9B
        public IDbSet<Reports_VwTrainingGrantTotals> Reports_VwTrainingGrantTotals { get; set; } // vw_TrainingGrantTotals
        public IDbSet<ReportType> ReportTypes { get; set; } // ReportTypes
        public IDbSet<Role> Roles { get; set; } // Roles
        public IDbSet<Search_VwAcademicDegrees> Search_VwAcademicDegrees { get; set; } // vw_AcademicDegrees
        public IDbSet<Search_VwApplicantAcademicHistory> Search_VwApplicantAcademicHistory { get; set; } // vw_ApplicantAcademicHistory
        public IDbSet<Search_VwApplicantData> Search_VwApplicantData { get; set; } // vw_ApplicantData
        public IDbSet<Search_VwApplicantDemographics> Search_VwApplicantDemographics { get; set; } // vw_ApplicantDemographics
        public IDbSet<Search_VwApplicantPrograms> Search_VwApplicantPrograms { get; set; } // vw_ApplicantPrograms
        public IDbSet<Search_VwClinicalFacultyPathways> Search_VwClinicalFacultyPathways { get; set; } // vw_ClinicalFacultyPathways
        public IDbSet<Search_VwCountries> Search_VwCountries { get; set; } // vw_Countries
        public IDbSet<Search_VwEthnicities> Search_VwEthnicities { get; set; } // vw_Ethnicities
        public IDbSet<Search_VwFacultyAcademicData> Search_VwFacultyAcademicData { get; set; } // vw_FacultyAcademicData
        public IDbSet<Search_VwFacultyAcademicHistory> Search_VwFacultyAcademicHistory { get; set; } // vw_FacultyAcademicHistory
        public IDbSet<Search_VwFacultyAppointments> Search_VwFacultyAppointments { get; set; } // vw_FacultyAppointments
        public IDbSet<Search_VwFacultyData> Search_VwFacultyData { get; set; } // vw_FacultyData
        public IDbSet<Search_VwFacultyDemographics> Search_VwFacultyDemographics { get; set; } // vw_FacultyDemographics
        public IDbSet<Search_VwFacultyFundingWithDirectCosts> Search_VwFacultyFundingWithDirectCosts { get; set; } // vw_FacultyFundingWithDirectCosts
        public IDbSet<Search_VwFacultyGrantsAndFunding> Search_VwFacultyGrantsAndFunding { get; set; } // vw_FacultyGrantsAndFunding
        public IDbSet<Search_VwFacultyMentees> Search_VwFacultyMentees { get; set; } // vw_FacultyMentees
        public IDbSet<Search_VwFacultyPrograms> Search_VwFacultyPrograms { get; set; } // vw_FacultyPrograms
        public IDbSet<Search_VwFacultyRanks> Search_VwFacultyRanks { get; set; } // vw_FacultyRanks
        public IDbSet<Search_VwFacultyResearchInterests> Search_VwFacultyResearchInterests { get; set; } // vw_FacultyResearchInterests
        public IDbSet<Search_VwFacultyTracks> Search_VwFacultyTracks { get; set; } // vw_FacultyTracks
        public IDbSet<Search_VwFundingCostInformation> Search_VwFundingCostInformation { get; set; } // vw_FundingCostInformation
        public IDbSet<Search_VwFundingDemographics> Search_VwFundingDemographics { get; set; } // vw_FundingDemographics
        public IDbSet<Search_VwFundingFaculty> Search_VwFundingFaculty { get; set; } // vw_FundingFaculty
        public IDbSet<Search_VwFundingProgramProjectData> Search_VwFundingProgramProjectData { get; set; } // vw_FundingProgramProjectData
        public IDbSet<Search_VwFundingProgramProjects> Search_VwFundingProgramProjects { get; set; } // vw_FundingProgramProjects
        public IDbSet<Search_VwFundingRoles> Search_VwFundingRoles { get; set; } // vw_FundingRoles
        public IDbSet<Search_VwFundingSubAwardData> Search_VwFundingSubAwardData { get; set; } // vw_FundingSubAwardData
        public IDbSet<Search_VwGenders> Search_VwGenders { get; set; } // vw_Genders
        public IDbSet<Search_VwGeneratedIDs> Search_VwGeneratedIDs { get; set; } // vw_GeneratedIDs
        public IDbSet<Search_VwInstitutions> Search_VwInstitutions { get; set; } // vw_Institutions
        public IDbSet<Search_VwLogins> Search_VwLogins { get; set; } // vw_Logins
        public IDbSet<Search_VwMenteeAcademicData> Search_VwMenteeAcademicData { get; set; } // vw_MenteeAcademicData
        public IDbSet<Search_VwMenteeData> Search_VwMenteeData { get; set; } // vw_MenteeData
        public IDbSet<Search_VwMenteeDemographics> Search_VwMenteeDemographics { get; set; } // vw_MenteeDemographics
        public IDbSet<Search_VwMenteeMentors> Search_VwMenteeMentors { get; set; } // vw_MenteeMentors
        public IDbSet<Search_VwMenteePublications> Search_VwMenteePublications { get; set; } // vw_MenteePublications
        public IDbSet<Search_VwMenteeTrainingPeriods> Search_VwMenteeTrainingPeriods { get; set; } // vw_MenteeTrainingPeriods
        public IDbSet<Search_VwMenteeWorkHistory> Search_VwMenteeWorkHistory { get; set; } // vw_MenteeWorkHistory
        public IDbSet<Search_VwOrganizations> Search_VwOrganizations { get; set; } // vw_Organizations
        public IDbSet<Search_VwOrganizationTypes> Search_VwOrganizationTypes { get; set; } // vw_OrganizationTypes
        public IDbSet<Search_VwPerson> Search_VwPerson { get; set; } // vw_Person
        public IDbSet<Search_VwPersonDemographics> Search_VwPersonDemographics { get; set; } // vw_PersonDemographics
        public IDbSet<Search_VwPersonOrphans> Search_VwPersonOrphans { get; set; } // vw_PersonOrphans
        public IDbSet<Search_VwPersonRoles> Search_VwPersonRoles { get; set; } // vw_PersonRoles
        public IDbSet<Search_VwPrograms> Search_VwPrograms { get; set; } // vw_Programs
        public IDbSet<Search_VwReports> Search_VwReports { get; set; } // vw_Reports
        public IDbSet<Search_VwStateProvinces> Search_VwStateProvinces { get; set; } // vw_StateProvinces
        public IDbSet<Search_VwSupportGeneralData> Search_VwSupportGeneralData { get; set; } // vw_SupportGeneralData
        public IDbSet<Search_VwSupportOrganizations> Search_VwSupportOrganizations { get; set; } // vw_SupportOrganizations
        public IDbSet<Search_VwSupportTypes> Search_VwSupportTypes { get; set; } // vw_SupportTypes
        public IDbSet<Search_VwTraineeCurrentSummaryInfo> Search_VwTraineeCurrentSummaryInfo { get; set; } // vw_TraineeCurrentSummaryInfo
        public IDbSet<Search_VwTraineeSummaryInfo> Search_VwTraineeSummaryInfo { get; set; } // vw_TraineeSummaryInfo
        public IDbSet<Search_VwTrainingGrantApplicants> Search_VwTrainingGrantApplicants { get; set; } // vw_TrainingGrantApplicants
        public IDbSet<Search_VwTrainingGrantCostInformation> Search_VwTrainingGrantCostInformation { get; set; } // vw_TrainingGrantCostInformation
        public IDbSet<Search_VwTrainingGrantDepartments> Search_VwTrainingGrantDepartments { get; set; } // vw_TrainingGrantDepartments
        public IDbSet<Search_VwTrainingGrantDistinctTrainees> Search_VwTrainingGrantDistinctTrainees { get; set; } // vw_TrainingGrantDistinctTrainees
        public IDbSet<Search_VwTrainingGrantFaculty> Search_VwTrainingGrantFaculty { get; set; } // vw_TrainingGrantFaculty
        public IDbSet<Search_VwTrainingGrantFacultyWithPrograms> Search_VwTrainingGrantFacultyWithPrograms { get; set; } // vw_TrainingGrantFacultyWithPrograms
        public IDbSet<Search_VwTrainingGrantFacultyWithResearchInterests> Search_VwTrainingGrantFacultyWithResearchInterests { get; set; } // vw_TrainingGrantFacultyWithResearchInterests
        public IDbSet<Search_VwTrainingGrantGeneralData> Search_VwTrainingGrantGeneralData { get; set; } // vw_TrainingGrantGeneralData
        public IDbSet<Search_VwTrainingGrantMentees> Search_VwTrainingGrantMentees { get; set; } // vw_TrainingGrantMentees
        public IDbSet<Search_VwTrainingGrantMenteesWithPrograms> Search_VwTrainingGrantMenteesWithPrograms { get; set; } // vw_TrainingGrantMenteesWithPrograms
        public IDbSet<Search_VwTrainingGrantPrograms> Search_VwTrainingGrantPrograms { get; set; } // vw_TrainingGrantPrograms
        public IDbSet<Search_VwTrainingGrantSupportGrants> Search_VwTrainingGrantSupportGrants { get; set; } // vw_TrainingGrantSupportGrants
        public IDbSet<Search_VwTrainingGrantTrainees> Search_VwTrainingGrantTrainees { get; set; } // vw_TrainingGrantTrainees
        public IDbSet<Search_VwTrainingPeriodMentors> Search_VwTrainingPeriodMentors { get; set; } // vw_TrainingPeriodMentors
        public IDbSet<Search_VwTrainingPeriodPrograms> Search_VwTrainingPeriodPrograms { get; set; } // vw_TrainingPeriodPrograms
        public IDbSet<Search_VwUsageOfAcademicDegrees> Search_VwUsageOfAcademicDegrees { get; set; } // vw_UsageOfAcademicDegrees
        public IDbSet<Search_VwUsageOfClinicalFacultyPathways> Search_VwUsageOfClinicalFacultyPathways { get; set; } // vw_UsageOfClinicalFacultyPathways
        public IDbSet<Search_VwUsageOfCountries> Search_VwUsageOfCountries { get; set; } // vw_UsageOfCountries
        public IDbSet<Search_VwUsageOfEthnicities> Search_VwUsageOfEthnicities { get; set; } // vw_UsageOfEthnicities
        public IDbSet<Search_VwUsageOfFaculty> Search_VwUsageOfFaculty { get; set; } // vw_UsageOfFaculty
        public IDbSet<Search_VwUsageOfFacultyRanks> Search_VwUsageOfFacultyRanks { get; set; } // vw_UsageOfFacultyRanks
        public IDbSet<Search_VwUsageOfFacultyTracks> Search_VwUsageOfFacultyTracks { get; set; } // vw_UsageOfFacultyTracks
        public IDbSet<Search_VwUsageOfFundingRoles> Search_VwUsageOfFundingRoles { get; set; } // vw_UsageOfFundingRoles
        public IDbSet<Search_VwUsageOfGenders> Search_VwUsageOfGenders { get; set; } // vw_UsageOfGenders
        public IDbSet<Search_VwUsageOfInstitutions> Search_VwUsageOfInstitutions { get; set; } // vw_UsageOfInstitutions
        public IDbSet<Search_VwUsageOfOrganizations> Search_VwUsageOfOrganizations { get; set; } // vw_UsageOfOrganizations
        public IDbSet<Search_VwUsageOfOrganizationTypes> Search_VwUsageOfOrganizationTypes { get; set; } // vw_UsageOfOrganizationTypes
        public IDbSet<Search_VwUsageOfPrograms> Search_VwUsageOfPrograms { get; set; } // vw_UsageOfPrograms
        public IDbSet<Search_VwUsageOfStateProvinces> Search_VwUsageOfStateProvinces { get; set; } // vw_UsageOfStateProvinces
        public IDbSet<StateProvince> StateProvinces { get; set; } // StateProvince
        public IDbSet<StatusType> StatusTypes { get; set; } // StatusTypes
        public IDbSet<Support> Supports { get; set; } // Support
        public IDbSet<SupportOrganization> SupportOrganizations { get; set; } // SupportOrganization
        public IDbSet<SupportSource> SupportSources { get; set; } // SupportSource
        public IDbSet<SupportStatu> SupportStatus { get; set; } // SupportStatus
        public IDbSet<SupportType> SupportTypes { get; set; } // SupportType
        public IDbSet<Sysdiagram> Sysdiagrams { get; set; } // sysdiagrams
        public IDbSet<TestScoreType> TestScoreTypes { get; set; } // TestScoreType
        public IDbSet<TraineeStatu> TraineeStatus { get; set; } // TraineeStatus
        public IDbSet<TraineeSummaryInfo> TraineeSummaryInfoes { get; set; } // TraineeSummaryInfo
        public IDbSet<TrainingGrant> TrainingGrants { get; set; } // TrainingGrant
        public IDbSet<TrainingGrantStatu> TrainingGrantStatus { get; set; } // TrainingGrantStatus
        public IDbSet<TrainingGrantSupportFunding> TrainingGrantSupportFundings { get; set; } // TrainingGrantSupportFunding
        public IDbSet<TrainingGrantSupportGrant> TrainingGrantSupportGrants { get; set; } // TrainingGrantSupportGrant
        public IDbSet<TrainingGrantTrainee> TrainingGrantTrainees { get; set; } // TrainingGrantTrainee
        public IDbSet<TrainingPeriod> TrainingPeriods { get; set; } // TrainingPeriod
        public IDbSet<WorkHistory> WorkHistories { get; set; } // WorkHistory

        static TTSEntities()
        {
            Database.SetInitializer<TTSEntities>(null);
        }

        public TTSEntities()
            : base("Name=TTSEntities")
        {
        }

        public TTSEntities(string connectionString) : base(connectionString)
        {
        }

        public TTSEntities(string connectionString, System.Data.Entity.Infrastructure.DbCompiledModel model) : base(connectionString, model)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new AcademicDegreeConfiguration());
            modelBuilder.Configurations.Add(new AcademicHistoryConfiguration());
            modelBuilder.Configurations.Add(new ApplicantConfiguration());
            modelBuilder.Configurations.Add(new BudgetPeriodStatuConfiguration());
            modelBuilder.Configurations.Add(new ClinicalFacultyPathwayConfiguration());
            modelBuilder.Configurations.Add(new ContactAddressConfiguration());
            modelBuilder.Configurations.Add(new ContactEmailConfiguration());
            modelBuilder.Configurations.Add(new ContactEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new ContactPhoneConfiguration());
            modelBuilder.Configurations.Add(new CountryConfiguration());
            modelBuilder.Configurations.Add(new DoctoralLevelConfiguration());
            modelBuilder.Configurations.Add(new EthnicityConfiguration());
            modelBuilder.Configurations.Add(new FacultyConfiguration());
            modelBuilder.Configurations.Add(new FacultyAcademicDataConfiguration());
            modelBuilder.Configurations.Add(new FacultyAppointmentConfiguration());
            modelBuilder.Configurations.Add(new FacultyRankConfiguration());
            modelBuilder.Configurations.Add(new FacultyResearchInterestConfiguration());
            modelBuilder.Configurations.Add(new FacultyTrackConfiguration());
            modelBuilder.Configurations.Add(new FundingConfiguration());
            modelBuilder.Configurations.Add(new FundingDirectCostConfiguration());
            modelBuilder.Configurations.Add(new FundingFacultyConfiguration());
            modelBuilder.Configurations.Add(new FundingProgramProjectConfiguration());
            modelBuilder.Configurations.Add(new FundingRoleConfiguration());
            modelBuilder.Configurations.Add(new FundingStatuConfiguration());
            modelBuilder.Configurations.Add(new FundingTypeConfiguration());
            modelBuilder.Configurations.Add(new GenderConfiguration());
            modelBuilder.Configurations.Add(new GradeRecordConfiguration());
            modelBuilder.Configurations.Add(new InstitutionConfiguration());
            modelBuilder.Configurations.Add(new InstitutionAssociationConfiguration());
            modelBuilder.Configurations.Add(new InstitutionTypeConfiguration());
            modelBuilder.Configurations.Add(new LeaveOfAbsenceConfiguration());
            modelBuilder.Configurations.Add(new LoginConfiguration());
            modelBuilder.Configurations.Add(new MedicalRankConfiguration());
            modelBuilder.Configurations.Add(new MenteeConfiguration());
            modelBuilder.Configurations.Add(new MenteeSupportConfiguration());
            modelBuilder.Configurations.Add(new OrganizationConfiguration());
            modelBuilder.Configurations.Add(new OrganizationTypeConfiguration());
            modelBuilder.Configurations.Add(new OsuTimelineConfiguration());
            modelBuilder.Configurations.Add(new PersonConfiguration());
            modelBuilder.Configurations.Add(new PersonRoleConfiguration());
            modelBuilder.Configurations.Add(new ProgramConfiguration());
            modelBuilder.Configurations.Add(new PublicationConfiguration());
            modelBuilder.Configurations.Add(new ReportConfiguration());
            modelBuilder.Configurations.Add(new Reports_VwT32Table10Configuration());
            modelBuilder.Configurations.Add(new Reports_VwT32Table1NConfiguration());
            modelBuilder.Configurations.Add(new Reports_VwT32Table1RConfiguration());
            modelBuilder.Configurations.Add(new Reports_VwT32Table3Configuration());
            modelBuilder.Configurations.Add(new Reports_VwT32Table4Configuration());
            modelBuilder.Configurations.Add(new Reports_VwT32Table5AConfiguration());
            modelBuilder.Configurations.Add(new Reports_VwT32Table5BConfiguration());
            modelBuilder.Configurations.Add(new Reports_VwT32Table7AConfiguration());
            modelBuilder.Configurations.Add(new Reports_VwT32Table7BConfiguration());
            modelBuilder.Configurations.Add(new Reports_VwT32Table8AConfiguration());
            modelBuilder.Configurations.Add(new Reports_VwT32Table8BConfiguration());
            modelBuilder.Configurations.Add(new Reports_VwT32Table9AConfiguration());
            modelBuilder.Configurations.Add(new Reports_VwT32Table9BConfiguration());
            modelBuilder.Configurations.Add(new Reports_VwTrainingGrantTotalsConfiguration());
            modelBuilder.Configurations.Add(new ReportTypeConfiguration());
            modelBuilder.Configurations.Add(new RoleConfiguration());
            modelBuilder.Configurations.Add(new Search_VwAcademicDegreesConfiguration());
            modelBuilder.Configurations.Add(new Search_VwApplicantAcademicHistoryConfiguration());
            modelBuilder.Configurations.Add(new Search_VwApplicantDataConfiguration());
            modelBuilder.Configurations.Add(new Search_VwApplicantDemographicsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwApplicantProgramsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwClinicalFacultyPathwaysConfiguration());
            modelBuilder.Configurations.Add(new Search_VwCountriesConfiguration());
            modelBuilder.Configurations.Add(new Search_VwEthnicitiesConfiguration());
            modelBuilder.Configurations.Add(new Search_VwFacultyAcademicDataConfiguration());
            modelBuilder.Configurations.Add(new Search_VwFacultyAcademicHistoryConfiguration());
            modelBuilder.Configurations.Add(new Search_VwFacultyAppointmentsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwFacultyDataConfiguration());
            modelBuilder.Configurations.Add(new Search_VwFacultyDemographicsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwFacultyFundingWithDirectCostsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwFacultyGrantsAndFundingConfiguration());
            modelBuilder.Configurations.Add(new Search_VwFacultyMenteesConfiguration());
            modelBuilder.Configurations.Add(new Search_VwFacultyProgramsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwFacultyRanksConfiguration());
            modelBuilder.Configurations.Add(new Search_VwFacultyResearchInterestsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwFacultyTracksConfiguration());
            modelBuilder.Configurations.Add(new Search_VwFundingCostInformationConfiguration());
            modelBuilder.Configurations.Add(new Search_VwFundingDemographicsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwFundingFacultyConfiguration());
            modelBuilder.Configurations.Add(new Search_VwFundingProgramProjectDataConfiguration());
            modelBuilder.Configurations.Add(new Search_VwFundingProgramProjectsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwFundingRolesConfiguration());
            modelBuilder.Configurations.Add(new Search_VwFundingSubAwardDataConfiguration());
            modelBuilder.Configurations.Add(new Search_VwGendersConfiguration());
            modelBuilder.Configurations.Add(new Search_VwGeneratedIDsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwInstitutionsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwLoginsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwMenteeAcademicDataConfiguration());
            modelBuilder.Configurations.Add(new Search_VwMenteeDataConfiguration());
            modelBuilder.Configurations.Add(new Search_VwMenteeDemographicsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwMenteeMentorsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwMenteePublicationsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwMenteeTrainingPeriodsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwMenteeWorkHistoryConfiguration());
            modelBuilder.Configurations.Add(new Search_VwOrganizationsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwOrganizationTypesConfiguration());
            modelBuilder.Configurations.Add(new Search_VwPersonConfiguration());
            modelBuilder.Configurations.Add(new Search_VwPersonDemographicsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwPersonOrphansConfiguration());
            modelBuilder.Configurations.Add(new Search_VwPersonRolesConfiguration());
            modelBuilder.Configurations.Add(new Search_VwProgramsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwReportsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwStateProvincesConfiguration());
            modelBuilder.Configurations.Add(new Search_VwSupportGeneralDataConfiguration());
            modelBuilder.Configurations.Add(new Search_VwSupportOrganizationsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwSupportTypesConfiguration());
            modelBuilder.Configurations.Add(new Search_VwTraineeCurrentSummaryInfoConfiguration());
            modelBuilder.Configurations.Add(new Search_VwTraineeSummaryInfoConfiguration());
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantApplicantsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantCostInformationConfiguration());
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantDepartmentsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantDistinctTraineesConfiguration());
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantFacultyConfiguration());
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantFacultyWithProgramsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantFacultyWithResearchInterestsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantGeneralDataConfiguration());
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantMenteesConfiguration());
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantMenteesWithProgramsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantProgramsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantSupportGrantsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantTraineesConfiguration());
            modelBuilder.Configurations.Add(new Search_VwTrainingPeriodMentorsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwTrainingPeriodProgramsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwUsageOfAcademicDegreesConfiguration());
            modelBuilder.Configurations.Add(new Search_VwUsageOfClinicalFacultyPathwaysConfiguration());
            modelBuilder.Configurations.Add(new Search_VwUsageOfCountriesConfiguration());
            modelBuilder.Configurations.Add(new Search_VwUsageOfEthnicitiesConfiguration());
            modelBuilder.Configurations.Add(new Search_VwUsageOfFacultyConfiguration());
            modelBuilder.Configurations.Add(new Search_VwUsageOfFacultyRanksConfiguration());
            modelBuilder.Configurations.Add(new Search_VwUsageOfFacultyTracksConfiguration());
            modelBuilder.Configurations.Add(new Search_VwUsageOfFundingRolesConfiguration());
            modelBuilder.Configurations.Add(new Search_VwUsageOfGendersConfiguration());
            modelBuilder.Configurations.Add(new Search_VwUsageOfInstitutionsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwUsageOfOrganizationsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwUsageOfOrganizationTypesConfiguration());
            modelBuilder.Configurations.Add(new Search_VwUsageOfProgramsConfiguration());
            modelBuilder.Configurations.Add(new Search_VwUsageOfStateProvincesConfiguration());
            modelBuilder.Configurations.Add(new StateProvinceConfiguration());
            modelBuilder.Configurations.Add(new StatusTypeConfiguration());
            modelBuilder.Configurations.Add(new SupportConfiguration());
            modelBuilder.Configurations.Add(new SupportOrganizationConfiguration());
            modelBuilder.Configurations.Add(new SupportSourceConfiguration());
            modelBuilder.Configurations.Add(new SupportStatuConfiguration());
            modelBuilder.Configurations.Add(new SupportTypeConfiguration());
            modelBuilder.Configurations.Add(new SysdiagramConfiguration());
            modelBuilder.Configurations.Add(new TestScoreTypeConfiguration());
            modelBuilder.Configurations.Add(new TraineeStatuConfiguration());
            modelBuilder.Configurations.Add(new TraineeSummaryInfoConfiguration());
            modelBuilder.Configurations.Add(new TrainingGrantConfiguration());
            modelBuilder.Configurations.Add(new TrainingGrantStatuConfiguration());
            modelBuilder.Configurations.Add(new TrainingGrantSupportFundingConfiguration());
            modelBuilder.Configurations.Add(new TrainingGrantSupportGrantConfiguration());
            modelBuilder.Configurations.Add(new TrainingGrantTraineeConfiguration());
            modelBuilder.Configurations.Add(new TrainingPeriodConfiguration());
            modelBuilder.Configurations.Add(new WorkHistoryConfiguration());
        }

        public static DbModelBuilder CreateModel(DbModelBuilder modelBuilder, string schema)
        {
            modelBuilder.Configurations.Add(new AcademicDegreeConfiguration(schema));
            modelBuilder.Configurations.Add(new AcademicHistoryConfiguration(schema));
            modelBuilder.Configurations.Add(new ApplicantConfiguration(schema));
            modelBuilder.Configurations.Add(new BudgetPeriodStatuConfiguration(schema));
            modelBuilder.Configurations.Add(new ClinicalFacultyPathwayConfiguration(schema));
            modelBuilder.Configurations.Add(new ContactAddressConfiguration(schema));
            modelBuilder.Configurations.Add(new ContactEmailConfiguration(schema));
            modelBuilder.Configurations.Add(new ContactEntityTypeConfiguration(schema));
            modelBuilder.Configurations.Add(new ContactPhoneConfiguration(schema));
            modelBuilder.Configurations.Add(new CountryConfiguration(schema));
            modelBuilder.Configurations.Add(new DoctoralLevelConfiguration(schema));
            modelBuilder.Configurations.Add(new EthnicityConfiguration(schema));
            modelBuilder.Configurations.Add(new FacultyConfiguration(schema));
            modelBuilder.Configurations.Add(new FacultyAcademicDataConfiguration(schema));
            modelBuilder.Configurations.Add(new FacultyAppointmentConfiguration(schema));
            modelBuilder.Configurations.Add(new FacultyRankConfiguration(schema));
            modelBuilder.Configurations.Add(new FacultyResearchInterestConfiguration(schema));
            modelBuilder.Configurations.Add(new FacultyTrackConfiguration(schema));
            modelBuilder.Configurations.Add(new FundingConfiguration(schema));
            modelBuilder.Configurations.Add(new FundingDirectCostConfiguration(schema));
            modelBuilder.Configurations.Add(new FundingFacultyConfiguration(schema));
            modelBuilder.Configurations.Add(new FundingProgramProjectConfiguration(schema));
            modelBuilder.Configurations.Add(new FundingRoleConfiguration(schema));
            modelBuilder.Configurations.Add(new FundingStatuConfiguration(schema));
            modelBuilder.Configurations.Add(new FundingTypeConfiguration(schema));
            modelBuilder.Configurations.Add(new GenderConfiguration(schema));
            modelBuilder.Configurations.Add(new GradeRecordConfiguration(schema));
            modelBuilder.Configurations.Add(new InstitutionConfiguration(schema));
            modelBuilder.Configurations.Add(new InstitutionAssociationConfiguration(schema));
            modelBuilder.Configurations.Add(new InstitutionTypeConfiguration(schema));
            modelBuilder.Configurations.Add(new LeaveOfAbsenceConfiguration(schema));
            modelBuilder.Configurations.Add(new LoginConfiguration(schema));
            modelBuilder.Configurations.Add(new MedicalRankConfiguration(schema));
            modelBuilder.Configurations.Add(new MenteeConfiguration(schema));
            modelBuilder.Configurations.Add(new MenteeSupportConfiguration(schema));
            modelBuilder.Configurations.Add(new OrganizationConfiguration(schema));
            modelBuilder.Configurations.Add(new OrganizationTypeConfiguration(schema));
            modelBuilder.Configurations.Add(new OsuTimelineConfiguration(schema));
            modelBuilder.Configurations.Add(new PersonConfiguration(schema));
            modelBuilder.Configurations.Add(new PersonRoleConfiguration(schema));
            modelBuilder.Configurations.Add(new ProgramConfiguration(schema));
            modelBuilder.Configurations.Add(new PublicationConfiguration(schema));
            modelBuilder.Configurations.Add(new ReportConfiguration(schema));
            modelBuilder.Configurations.Add(new Reports_VwT32Table10Configuration(schema));
            modelBuilder.Configurations.Add(new Reports_VwT32Table1NConfiguration(schema));
            modelBuilder.Configurations.Add(new Reports_VwT32Table1RConfiguration(schema));
            modelBuilder.Configurations.Add(new Reports_VwT32Table3Configuration(schema));
            modelBuilder.Configurations.Add(new Reports_VwT32Table4Configuration(schema));
            modelBuilder.Configurations.Add(new Reports_VwT32Table5AConfiguration(schema));
            modelBuilder.Configurations.Add(new Reports_VwT32Table5BConfiguration(schema));
            modelBuilder.Configurations.Add(new Reports_VwT32Table7AConfiguration(schema));
            modelBuilder.Configurations.Add(new Reports_VwT32Table7BConfiguration(schema));
            modelBuilder.Configurations.Add(new Reports_VwT32Table8AConfiguration(schema));
            modelBuilder.Configurations.Add(new Reports_VwT32Table8BConfiguration(schema));
            modelBuilder.Configurations.Add(new Reports_VwT32Table9AConfiguration(schema));
            modelBuilder.Configurations.Add(new Reports_VwT32Table9BConfiguration(schema));
            modelBuilder.Configurations.Add(new Reports_VwTrainingGrantTotalsConfiguration(schema));
            modelBuilder.Configurations.Add(new ReportTypeConfiguration(schema));
            modelBuilder.Configurations.Add(new RoleConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwAcademicDegreesConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwApplicantAcademicHistoryConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwApplicantDataConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwApplicantDemographicsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwApplicantProgramsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwClinicalFacultyPathwaysConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwCountriesConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwEthnicitiesConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwFacultyAcademicDataConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwFacultyAcademicHistoryConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwFacultyAppointmentsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwFacultyDataConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwFacultyDemographicsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwFacultyFundingWithDirectCostsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwFacultyGrantsAndFundingConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwFacultyMenteesConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwFacultyProgramsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwFacultyRanksConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwFacultyResearchInterestsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwFacultyTracksConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwFundingCostInformationConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwFundingDemographicsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwFundingFacultyConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwFundingProgramProjectDataConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwFundingProgramProjectsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwFundingRolesConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwFundingSubAwardDataConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwGendersConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwGeneratedIDsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwInstitutionsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwLoginsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwMenteeAcademicDataConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwMenteeDataConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwMenteeDemographicsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwMenteeMentorsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwMenteePublicationsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwMenteeTrainingPeriodsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwMenteeWorkHistoryConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwOrganizationsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwOrganizationTypesConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwPersonConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwPersonDemographicsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwPersonOrphansConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwPersonRolesConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwProgramsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwReportsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwStateProvincesConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwSupportGeneralDataConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwSupportOrganizationsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwSupportTypesConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwTraineeCurrentSummaryInfoConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwTraineeSummaryInfoConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantApplicantsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantCostInformationConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantDepartmentsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantDistinctTraineesConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantFacultyConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantFacultyWithProgramsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantFacultyWithResearchInterestsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantGeneralDataConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantMenteesConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantMenteesWithProgramsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantProgramsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantSupportGrantsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwTrainingGrantTraineesConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwTrainingPeriodMentorsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwTrainingPeriodProgramsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwUsageOfAcademicDegreesConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwUsageOfClinicalFacultyPathwaysConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwUsageOfCountriesConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwUsageOfEthnicitiesConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwUsageOfFacultyConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwUsageOfFacultyRanksConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwUsageOfFacultyTracksConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwUsageOfFundingRolesConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwUsageOfGendersConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwUsageOfInstitutionsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwUsageOfOrganizationsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwUsageOfOrganizationTypesConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwUsageOfProgramsConfiguration(schema));
            modelBuilder.Configurations.Add(new Search_VwUsageOfStateProvincesConfiguration(schema));
            modelBuilder.Configurations.Add(new StateProvinceConfiguration(schema));
            modelBuilder.Configurations.Add(new StatusTypeConfiguration(schema));
            modelBuilder.Configurations.Add(new SupportConfiguration(schema));
            modelBuilder.Configurations.Add(new SupportOrganizationConfiguration(schema));
            modelBuilder.Configurations.Add(new SupportSourceConfiguration(schema));
            modelBuilder.Configurations.Add(new SupportStatuConfiguration(schema));
            modelBuilder.Configurations.Add(new SupportTypeConfiguration(schema));
            modelBuilder.Configurations.Add(new SysdiagramConfiguration(schema));
            modelBuilder.Configurations.Add(new TestScoreTypeConfiguration(schema));
            modelBuilder.Configurations.Add(new TraineeStatuConfiguration(schema));
            modelBuilder.Configurations.Add(new TraineeSummaryInfoConfiguration(schema));
            modelBuilder.Configurations.Add(new TrainingGrantConfiguration(schema));
            modelBuilder.Configurations.Add(new TrainingGrantStatuConfiguration(schema));
            modelBuilder.Configurations.Add(new TrainingGrantSupportFundingConfiguration(schema));
            modelBuilder.Configurations.Add(new TrainingGrantSupportGrantConfiguration(schema));
            modelBuilder.Configurations.Add(new TrainingGrantTraineeConfiguration(schema));
            modelBuilder.Configurations.Add(new TrainingPeriodConfiguration(schema));
            modelBuilder.Configurations.Add(new WorkHistoryConfiguration(schema));
            return modelBuilder;
        }
    }
}

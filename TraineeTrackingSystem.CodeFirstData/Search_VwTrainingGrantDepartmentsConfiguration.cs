// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingGrantDepartments
    internal class Search_VwTrainingGrantDepartmentsConfiguration : EntityTypeConfiguration<Search_VwTrainingGrantDepartments>
    {
        public Search_VwTrainingGrantDepartmentsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_TrainingGrantDepartments");
            HasKey(x => new { x.TrainingGrantId, x.DeptIdentifier, x.DepartmentId, x.DeptDisplayName });

            Property(x => x.TrainingGrantId).HasColumnName("TrainingGrantId").IsRequired();
            Property(x => x.SponsorId).HasColumnName("SponsorId").IsOptional();
            Property(x => x.InstitutionIdentifier).HasColumnName("InstitutionIdentifier").IsOptional().HasMaxLength(150);
            Property(x => x.InstitutionName).HasColumnName("InstitutionName").IsOptional().HasMaxLength(250);
            Property(x => x.InstitutionCity).HasColumnName("InstitutionCity").IsOptional().HasMaxLength(150);
            Property(x => x.InstitutionStateId).HasColumnName("InstitutionStateId").IsOptional();
            Property(x => x.InstitutionStateAbbreviation).HasColumnName("InstitutionStateAbbreviation").IsOptional().HasMaxLength(50);
            Property(x => x.InstitutionStateFullName).HasColumnName("InstitutionStateFullName").IsOptional().HasMaxLength(50);
            Property(x => x.InstitutionCountryId).HasColumnName("InstitutionCountryId").IsOptional();
            Property(x => x.InstitutionCountry).HasColumnName("InstitutionCountry").IsOptional().HasMaxLength(150);
            Property(x => x.DoctoralLevelId).HasColumnName("DoctoralLevelId").IsOptional();
            Property(x => x.DoctoralLevel).HasColumnName("DoctoralLevel").IsOptional().HasMaxLength(150);
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.LastUpdatedByName).HasColumnName("LastUpdatedByName").IsOptional().HasMaxLength(129);
            Property(x => x.DepartmentId).HasColumnName("DepartmentId").IsRequired();
            Property(x => x.DeptIdentifier).HasColumnName("DeptIdentifier").IsRequired().HasMaxLength(100);
            Property(x => x.DeptDisplayName).HasColumnName("DeptDisplayName").IsRequired().HasMaxLength(250);
            Property(x => x.DeptHrName).HasColumnName("DeptHRName").IsOptional().HasMaxLength(250);
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_UsageOfInstitutions
    internal class Search_VwUsageOfInstitutionsConfiguration : EntityTypeConfiguration<Search_VwUsageOfInstitutions>
    {
        public Search_VwUsageOfInstitutionsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_UsageOfInstitutions");
            HasKey(x => new { x.InstitutionId, x.InstitutionName });

            Property(x => x.InstitutionId).HasColumnName("InstitutionId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.InstitutionName).HasColumnName("InstitutionName").IsRequired().HasMaxLength(250);
            Property(x => x.TotalAcademicHistory).HasColumnName("TotalAcademicHistory").IsOptional();
            Property(x => x.TotalFacultyAcademics).HasColumnName("TotalFacultyAcademics").IsOptional();
            Property(x => x.TotalFunding).HasColumnName("TotalFunding").IsOptional();
            Property(x => x.TotalGradeRecord).HasColumnName("TotalGradeRecord").IsOptional();
            Property(x => x.TotalMenteeSupport).HasColumnName("TotalMenteeSupport").IsOptional();
            Property(x => x.TotalTrainingPeriods).HasColumnName("TotalTrainingPeriods").IsOptional();
            Property(x => x.TotalWorkHistory).HasColumnName("TotalWorkHistory").IsOptional();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_UsageOfFacultyTracks
    internal class Search_VwUsageOfFacultyTracksConfiguration : EntityTypeConfiguration<Search_VwUsageOfFacultyTracks>
    {
        public Search_VwUsageOfFacultyTracksConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_UsageOfFacultyTracks");
            HasKey(x => new { x.FacultyTrackId, x.FacultyTrack });

            Property(x => x.FacultyTrackId).HasColumnName("FacultyTrackId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.FacultyTrack).HasColumnName("FacultyTrack").IsRequired().HasMaxLength(500);
            Property(x => x.TotalFacultyTracks).HasColumnName("TotalFacultyTracks").IsOptional();
        }
    }

}

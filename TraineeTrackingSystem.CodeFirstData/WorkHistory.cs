// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // WorkHistory
    public class WorkHistory
    {
        public int Id { get; set; } // Id (Primary key)
        public Guid PersonId { get; set; } // PersonId
        public DateTime? DateStartedPosition { get; set; } // DateStartedPosition
        public DateTime? DateEndedPosition { get; set; } // DateEndedPosition
        public int? InstitutionId { get; set; } // InstitutionId
        public string PositionTitle { get; set; } // PositionTitle
        public string PositionDescription { get; set; } // PositionDescription
        public string PositionLocation { get; set; } // PositionLocation
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public bool IsDeleted { get; set; } // IsDeleted
        public string SupportSourceText { get; set; } // SupportSourceText

        // Reverse navigation
        public virtual ICollection<SupportSource> SupportSources { get; set; } // Many to many mapping

        // Foreign keys
        public virtual Institution Institution { get; set; } // FK_WorkHistory_Institution
        public virtual Person Person { get; set; } // FK_WorkHistory_Person

        public WorkHistory()
        {
            IsDeleted = false;
            SupportSources = new List<SupportSource>();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FacultyResearchInterests
    public class Search_VwFacultyResearchInterests
    {
        public Guid FacultyId { get; set; } // FacultyId
        public Guid PersonId { get; set; } // PersonId
        public string FacultyLastName { get; set; } // FacultyLastName
        public string FacultyFirstName { get; set; } // FacultyFirstName
        public string FacultyMiddleName { get; set; } // FacultyMiddleName
        public string FullName { get; set; } // FullName
        public int? ResearchInterestId { get; set; } // ResearchInterestId
        public string ResearchInterest { get; set; } // ResearchInterest
        public DateTime? ResearchInterestDateLastUpdated { get; set; } // ResearchInterestDateLastUpdated
        public string ResearchInterestLastUpdatedBy { get; set; } // ResearchInterestLastUpdatedBy
        public string ResearchInterestLastUpdatedByName { get; set; } // ResearchInterestLastUpdatedByName
    }

}

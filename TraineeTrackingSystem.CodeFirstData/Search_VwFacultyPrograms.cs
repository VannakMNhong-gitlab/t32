// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FacultyPrograms
    public class Search_VwFacultyPrograms
    {
        public Guid FacultyId { get; set; } // FacultyId
        public Guid FacultyPersonId { get; set; } // FacultyPersonId
        public string FirstName { get; set; } // FirstName
        public string MiddleName { get; set; } // MiddleName
        public string LastName { get; set; } // LastName
        public string FullName { get; set; } // FullName
        public string EmployeeId { get; set; } // EmployeeId
        public Guid ProgramId { get; set; } // ProgramId
        public int ProgramDisplayId { get; set; } // ProgramDisplayId
        public string ProgramTitle { get; set; } // ProgramTitle
    }

}

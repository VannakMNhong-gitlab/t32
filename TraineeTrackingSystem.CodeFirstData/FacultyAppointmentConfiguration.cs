// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // FacultyAppointment
    internal class FacultyAppointmentConfiguration : EntityTypeConfiguration<FacultyAppointment>
    {
        public FacultyAppointmentConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".FacultyAppointment");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.InstitutionId).HasColumnName("InstitutionId").IsRequired();
            Property(x => x.FacultyTrackId).HasColumnName("FacultyTrackId").IsOptional();
            Property(x => x.ClinicalFacultyPathwayId).HasColumnName("ClinicalFacultyPathwayId").IsOptional();
            Property(x => x.FacultyRankId).HasColumnName("FacultyRankId").IsOptional();
            Property(x => x.OtherAffiliations).HasColumnName("OtherAffiliations").IsOptional().HasMaxLength(1500);
            Property(x => x.OtherTitles).HasColumnName("OtherTitles").IsOptional().HasMaxLength(1500);
            Property(x => x.PercentFte).HasColumnName("PercentFTE").IsOptional().HasPrecision(10,2);
            Property(x => x.DateStarted).HasColumnName("DateStarted").IsOptional();
            Property(x => x.DateEnded).HasColumnName("DateEnded").IsOptional();
            Property(x => x.Comment).HasColumnName("Comment").IsOptional();
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);

            // Foreign keys
            HasRequired(a => a.Person).WithMany(b => b.FacultyAppointments).HasForeignKey(c => c.PersonId); // FK_FacultyAppointment_Person
            HasRequired(a => a.Institution).WithMany(b => b.FacultyAppointments).HasForeignKey(c => c.InstitutionId); // FK_FacultyAppointment_Institution
            HasOptional(a => a.FacultyTrack).WithMany(b => b.FacultyAppointments).HasForeignKey(c => c.FacultyTrackId); // FK_FacultyAppointment_FacultyTrack
            HasOptional(a => a.ClinicalFacultyPathway).WithMany(b => b.FacultyAppointments).HasForeignKey(c => c.ClinicalFacultyPathwayId); // FK_FacultyAppointment_ClinicalFacultyPathway
            HasOptional(a => a.FacultyRank).WithMany(b => b.FacultyAppointments).HasForeignKey(c => c.FacultyRankId); // FK_FacultyAppointment_FacultyRank
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_GeneratedIDs
    internal class Search_VwGeneratedIDsConfiguration : EntityTypeConfiguration<Search_VwGeneratedIDs>
    {
        public Search_VwGeneratedIDsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_GeneratedIDs");
            HasKey(x => x.IdType);

            Property(x => x.GeneratedId).HasColumnName("GeneratedId").IsOptional().HasMaxLength(50);
            Property(x => x.IdType).HasColumnName("IDType").IsRequired().HasMaxLength(9);
        }
    }

}

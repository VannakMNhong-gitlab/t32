// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // FacultyAcademicData
    internal class FacultyAcademicDataConfiguration : EntityTypeConfiguration<FacultyAcademicData>
    {
        public FacultyAcademicDataConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".FacultyAcademicData");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.AcademicDegreeId).HasColumnName("AcademicDegreeId").IsOptional();
            Property(x => x.AreaOfStudy).HasColumnName("AreaOfStudy").IsOptional().HasMaxLength(150);
            Property(x => x.DegreeYear).HasColumnName("DegreeYear").IsOptional();
            Property(x => x.InstitutionId).HasColumnName("InstitutionId").IsOptional();
            Property(x => x.OtherDegree).HasColumnName("OtherDegree").IsOptional().HasMaxLength(100);
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);

            // Foreign keys
            HasRequired(a => a.Person).WithMany(b => b.FacultyAcademicDatas).HasForeignKey(c => c.PersonId); // FK_FacultyAcademicData_Person
            HasOptional(a => a.AcademicDegree).WithMany(b => b.FacultyAcademicDatas).HasForeignKey(c => c.AcademicDegreeId); // FK_FacultyAcademicData_AcademicDegree
            HasOptional(a => a.Institution).WithMany(b => b.FacultyAcademicDatas).HasForeignKey(c => c.InstitutionId); // FK_FacultyAcademicData_Institution
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Faculty
    public class Faculty
    {
        public Guid Id { get; set; } // Id (Primary key)
        public Guid PersonId { get; set; } // PersonId
        public string EmployeeId { get; set; } // EmployeeId
        public int? FacultyRankId { get; set; } // FacultyRankId
        public int? PrimaryOrganizationId { get; set; } // PrimaryOrganizationId
        public int? SecondaryOrganizationId { get; set; } // SecondaryOrganizationId
        public string OtherAffiliations { get; set; } // OtherAffiliations
        public string OtherTitles { get; set; } // OtherTitles
        public string CreatedBy { get; set; } // CreatedBy
        public DateTime? DateCreated { get; set; } // DateCreated
        public bool IsActive { get; set; } // IsActive
        public int? TiuId { get; set; } // TIUId
        public bool IsDeleted { get; set; } // IsDeleted
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public Guid? MentorId { get; set; } // MentorId
        public int? PositionDepartmentId { get; set; } // PositionDepartmentId

        // Reverse navigation
        public virtual ICollection<Funding> Fundings_PrimeAwardPiId { get; set; } // Funding.FK_Funding_FacultyPrimeAwardPI
        public virtual ICollection<Funding> Fundings_ProgramProjectPdId { get; set; } // Funding.FK_Funding_FacultyProgPD
        public virtual ICollection<FundingFaculty> FundingFaculties { get; set; } // FundingFaculty.FK_FundingFaculty_Faculty
        public virtual ICollection<Program> Programs { get; set; } // Many to many mapping
        public virtual ICollection<TrainingPeriod> TrainingPeriods { get; set; } // Many to many mapping

        // Foreign keys
        public virtual FacultyRank FacultyRank { get; set; } // FK_Faculty_FacultyRank
        public virtual Organization Organization_PrimaryOrganizationId { get; set; } // FK_Faculty_PrimaryOrganization
        public virtual Organization Organization_SecondaryOrganizationId { get; set; } // FK_Faculty_SecondaryOrganization
        public virtual Organization Organization_TiuId { get; set; } // FK_Faculty_TIU
        public virtual Person Person { get; set; } // FK_Faculty_Person

        public Faculty()
        {
            Id = System.Guid.NewGuid();
            DateCreated = System.DateTime.Now;
            IsActive = true;
            IsDeleted = false;
            Fundings_PrimeAwardPiId = new List<Funding>();
            Fundings_ProgramProjectPdId = new List<Funding>();
            FundingFaculties = new List<FundingFaculty>();
            Programs = new List<Program>();
            TrainingPeriods = new List<TrainingPeriod>();
        }
    }

}

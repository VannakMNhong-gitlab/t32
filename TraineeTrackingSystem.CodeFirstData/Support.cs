// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Support
    public class Support
    {
        public Guid Id { get; set; } // Id (Primary key)
        public string Title { get; set; } // Title
        public int DisplayId { get; set; } // DisplayId
        public int? SupportTypeId { get; set; } // SupportTypeId
        public int SupportStatusId { get; set; } // SupportStatusId
        public string SupportNumber { get; set; } // SupportNumber
        public int? SupportOrganizationId { get; set; } // SupportOrganizationId
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded
        public DateTime? DateCreated { get; set; } // DateCreated
        public string CreatedBy { get; set; } // CreatedBy
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public bool IsDeleted { get; set; } // IsDeleted

        // Foreign keys
        public virtual SupportOrganization SupportOrganization { get; set; } // FK_Support_SupportOrganization
        public virtual SupportStatu SupportStatu { get; set; } // FK_Support_SupportStatus
        public virtual SupportType SupportType { get; set; } // FK_Support_SupportType

        public Support()
        {
            Id = System.Guid.NewGuid();
            SupportStatusId = 1;
            IsDeleted = false;
        }
    }

}

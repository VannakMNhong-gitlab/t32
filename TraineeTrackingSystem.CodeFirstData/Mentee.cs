// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Mentee
    public class Mentee
    {
        public Guid Id { get; set; } // Id (Primary key)
        public Guid PersonId { get; set; } // PersonId
        public string StudentId { get; set; } // StudentId
        public int? InstitutionAssociationId { get; set; } // InstitutionAssociationId
        public int? DepartmentId { get; set; } // DepartmentId
        public bool IsTrainingGrantEligible { get; set; } // IsTrainingGrantEligible
        public bool WasRecruitedToLab { get; set; } // WasRecruitedToLab
        public bool IsDeleted { get; set; } // IsDeleted
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public DateTime? DateCreated { get; set; } // DateCreated
        public string CreatedBy { get; set; } // CreatedBy

        // Reverse navigation
        public virtual ICollection<TrainingGrantTrainee> TrainingGrantTrainees { get; set; } // TrainingGrantTrainee.FK_TrainingGrantTrainee_Mentee

        // Foreign keys
        public virtual InstitutionAssociation InstitutionAssociation { get; set; } // FK_Mentee_InstitutionAssociation
        public virtual Organization Organization { get; set; } // FK_Mentee_Organization
        public virtual Person Person { get; set; } // FK_Mentee_Person

        public Mentee()
        {
            Id = System.Guid.NewGuid();
            IsTrainingGrantEligible = false;
            WasRecruitedToLab = false;
            IsDeleted = false;
            DateCreated = System.DateTime.Now;
            TrainingGrantTrainees = new List<TrainingGrantTrainee>();
        }
    }

}

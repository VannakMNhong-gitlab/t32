// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_UsageOfPrograms
    public class Search_VwUsageOfPrograms
    {
        public Guid ProgramId { get; set; } // ProgramId
        public string ProgramTitle { get; set; } // ProgramTitle
        public int? TotalApplicantPrograms { get; set; } // TotalApplicantPrograms
        public int? TotalFacultyPrograms { get; set; } // TotalFacultyPrograms
        public int? TotalTrainingGrantPrograms { get; set; } // TotalTrainingGrantPrograms
        public int? TotalTrainingPeriodPrograms { get; set; } // TotalTrainingPeriodPrograms
    }

}

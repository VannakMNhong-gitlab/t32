// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Logins
    internal class LoginConfiguration : EntityTypeConfiguration<Login>
    {
        public LoginConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".Logins");
            HasKey(x => x.LoginId);

            Property(x => x.LoginId).HasColumnName("LoginId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.ExternalId).HasColumnName("ExternalId").IsOptional().HasMaxLength(100);
            Property(x => x.PersonId).HasColumnName("PersonId").IsOptional();
            Property(x => x.IsActive).HasColumnName("IsActive").IsRequired();
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.DateCreated).HasColumnName("DateCreated").IsOptional();
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.Username).HasColumnName("Username").IsOptional().HasMaxLength(50);

            // Foreign keys
            HasOptional(a => a.Person).WithMany(b => b.Logins).HasForeignKey(c => c.PersonId); // FK_Logins_Person
        }
    }

}

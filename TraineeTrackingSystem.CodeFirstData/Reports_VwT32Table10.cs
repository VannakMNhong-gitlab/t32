// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_T32Table10
    public class Reports_VwT32Table10
    {
        public Guid TrainingGrantId { get; set; } // TrainingGrantId
        public bool IsRenewal { get; set; } // IsRenewal
        public int? DoctoralLevelId { get; set; } // DoctoralLevelId
        public string DoctoralLevel { get; set; } // DoctoralLevel
        public int Column4ItemTypeId { get; set; } // Column4ItemTypeId
        public string Column4ItemType { get; set; } // Column4ItemType
        public int? Column4ItemId { get; set; } // Column4ItemId
        public string Column4ItemName { get; set; } // Column4ItemName
    }

}

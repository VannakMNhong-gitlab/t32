// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_PersonRoles
    internal class Search_VwPersonRolesConfiguration : EntityTypeConfiguration<Search_VwPersonRoles>
    {
        public Search_VwPersonRolesConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_PersonRoles");
            HasKey(x => new { x.PersonId, x.LoginId });

            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.FirstName).HasColumnName("FirstName").IsOptional().HasMaxLength(50);
            Property(x => x.LastName).HasColumnName("LastName").IsOptional().HasMaxLength(75);
            Property(x => x.MiddleName).HasColumnName("MiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.FullName).HasColumnName("FullName").IsOptional().HasMaxLength(178);
            Property(x => x.LoginId).HasColumnName("LoginId").IsRequired();
            Property(x => x.ExternalId).HasColumnName("ExternalId").IsOptional().HasMaxLength(100);
            Property(x => x.Username).HasColumnName("Username").IsOptional().HasMaxLength(100);
            Property(x => x.RoleId).HasColumnName("RoleId").IsOptional();
            Property(x => x.RoleName).HasColumnName("RoleName").IsOptional().HasMaxLength(150);
            Property(x => x.IsActive).HasColumnName("IsActive").IsOptional();
            Property(x => x.Description).HasColumnName("Description").IsOptional().HasMaxLength(1000);
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.LastUpdatedByName).HasColumnName("LastUpdatedByName").IsOptional().HasMaxLength(129);
        }
    }

}

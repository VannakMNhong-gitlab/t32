// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_PersonOrphans
    internal class Search_VwPersonOrphansConfiguration : EntityTypeConfiguration<Search_VwPersonOrphans>
    {
        public Search_VwPersonOrphansConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_PersonOrphans");
            HasKey(x => new { x.IsApplicant, x.IsMentee, x.PersonId, x.IsFaculty, x.DisplayId, x.DateCreated });

            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.DisplayId).HasColumnName("DisplayId").IsRequired();
            Property(x => x.FirstName).HasColumnName("FirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MiddleName).HasColumnName("MiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.LastName).HasColumnName("LastName").IsOptional().HasMaxLength(75);
            Property(x => x.FullName).HasColumnName("FullName").IsOptional().HasMaxLength(178);
            Property(x => x.IsUnderrepresentedMinority).HasColumnName("IsUnderrepresentedMinority").IsOptional();
            Property(x => x.IsIndividualWithDisabilities).HasColumnName("IsIndividualWithDisabilities").IsOptional();
            Property(x => x.IsFromDisadvantagedBkgd).HasColumnName("IsFromDisadvantagedBkgd").IsOptional();
            Property(x => x.GenderId).HasColumnName("GenderId").IsOptional();
            Property(x => x.Gender).HasColumnName("Gender").IsOptional().HasMaxLength(100);
            Property(x => x.GenderAtBirthId).HasColumnName("GenderAtBirthId").IsOptional();
            Property(x => x.GenderAtBirth).HasColumnName("GenderAtBirth").IsOptional().HasMaxLength(100);
            Property(x => x.EthnicityId).HasColumnName("EthnicityId").IsOptional();
            Property(x => x.Ethnicity).HasColumnName("Ethnicity").IsOptional().HasMaxLength(300);
            Property(x => x.DateCreated).HasColumnName("DateCreated").IsRequired();
            Property(x => x.EmployeeId).HasColumnName("EmployeeId").IsOptional().HasMaxLength(50);
            Property(x => x.StudentId).HasColumnName("StudentId").IsOptional().HasMaxLength(50);
            Property(x => x.ApplicantId).HasColumnName("ApplicantId").IsOptional();
            Property(x => x.IsFaculty).HasColumnName("IsFaculty").IsRequired();
            Property(x => x.IsMentee).HasColumnName("IsMentee").IsRequired();
            Property(x => x.IsApplicant).HasColumnName("IsApplicant").IsRequired();
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.LastUpdatedByName).HasColumnName("LastUpdatedByName").IsOptional().HasMaxLength(129);
            Property(x => x.Username).HasColumnName("Username").IsOptional().HasMaxLength(100);
            Property(x => x.IsActive).HasColumnName("IsActive").IsOptional();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TraineeCurrentSummaryInfo
    public class Search_VwTraineeCurrentSummaryInfo
    {
        public Guid TrainingGrantId { get; set; } // TrainingGrantId
        public int DoctoralLevelId { get; set; } // DoctoralLevelId
        public Guid? CurrentPredocTraineeSummaryInfoId { get; set; } // CurrentPredocTraineeSummaryInfoId
        public Guid? CurrentPostdocTraineeSummaryInfoId { get; set; } // CurrentPostdocTraineeSummaryInfoId
    }

}

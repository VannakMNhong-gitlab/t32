// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_Countries
    internal class Search_VwCountriesConfiguration : EntityTypeConfiguration<Search_VwCountries>
    {
        public Search_VwCountriesConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_Countries");
            HasKey(x => new { x.CountryId, x.Country });

            Property(x => x.CountryId).HasColumnName("CountryId").IsRequired();
            Property(x => x.Country).HasColumnName("Country").IsRequired().HasMaxLength(150);
            Property(x => x.TotalCountryUsage).HasColumnName("TotalCountryUsage").IsOptional();
        }
    }

}

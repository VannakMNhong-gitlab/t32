// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // ContactEntityType
    public class ContactEntityType
    {
        public int Id { get; set; } // Id (Primary key)
        public string TypeName { get; set; } // TypeName

        // Reverse navigation
        public virtual ICollection<ContactAddress> ContactAddresses { get; set; } // ContactAddress.FK_ContactAddress_ContactEntityType
        public virtual ICollection<ContactEmail> ContactEmails { get; set; } // ContactEmail.FK_ContactEmail_ContactEntityType
        public virtual ICollection<ContactPhone> ContactPhones { get; set; } // ContactPhone.FK_ContactPhone_ContactEntityType

        public ContactEntityType()
        {
            ContactAddresses = new List<ContactAddress>();
            ContactEmails = new List<ContactEmail>();
            ContactPhones = new List<ContactPhone>();
        }
    }

}

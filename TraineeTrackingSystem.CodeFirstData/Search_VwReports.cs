// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_Reports
    public class Search_VwReports
    {
        public int Id { get; set; } // Id
        public string Name { get; set; } // Name
        public string Title { get; set; } // Title
        public DateTime ModifiedDate { get; set; } // ModifiedDate
        public int? ReportTypeId { get; set; } // ReportTypeId
        public string ReportType { get; set; } // ReportType
        public string ReportPath { get; set; } // ReportPath
        public string Description { get; set; } // Description
        public int? StatusTypeId { get; set; } // StatusTypeId
        public string StatusType { get; set; } // StatusType
        public bool IsPredoc { get; set; } // IsPredoc
        public bool IsPostdoc { get; set; } // IsPostdoc
        public bool IsNewOnly { get; set; } // IsNewOnly
        public bool IsRenewalOnly { get; set; } // IsRenewalOnly
    }

}

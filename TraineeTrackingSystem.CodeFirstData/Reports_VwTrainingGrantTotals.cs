// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingGrantTotals
    public class Reports_VwTrainingGrantTotals
    {
        public Guid TrainingGrantId { get; set; } // TrainingGrantId
        public string TrainingGrantTitle { get; set; } // TrainingGrantTitle
        public int? TotalFaculty { get; set; } // TotalFaculty
        public int? TotalMentees { get; set; } // TotalMentees
        public int? TotalApplicants { get; set; } // TotalApplicants
        public int? TotalTrainees { get; set; } // TotalTrainees
        public int? TotalPredocTrainees { get; set; } // TotalPredocTrainees
        public int? TotalPostdocTrainees { get; set; } // TotalPostdocTrainees
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_MenteePublications
    internal class Search_VwMenteePublicationsConfiguration : EntityTypeConfiguration<Search_VwMenteePublications>
    {
        public Search_VwMenteePublicationsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_MenteePublications");
            HasKey(x => new { x.MenteeId, x.StudentId, x.DisplayId, x.IsTrainingGrantEligible, x.MenteePersonId });

            Property(x => x.MenteeId).HasColumnName("MenteeId").IsRequired();
            Property(x => x.MenteePersonId).HasColumnName("MenteePersonId").IsRequired();
            Property(x => x.FirstName).HasColumnName("FirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MiddleName).HasColumnName("MiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.LastName).HasColumnName("LastName").IsOptional().HasMaxLength(75);
            Property(x => x.FullName).HasColumnName("FullName").IsOptional().HasMaxLength(178);
            Property(x => x.DisplayId).HasColumnName("DisplayId").IsRequired();
            Property(x => x.StudentId).HasColumnName("StudentId").IsRequired().HasMaxLength(50);
            Property(x => x.IsTrainingGrantEligible).HasColumnName("IsTrainingGrantEligible").IsRequired();
            Property(x => x.MenteeDateLastUpdated).HasColumnName("MenteeDateLastUpdated").IsOptional();
            Property(x => x.MenteeLastUpdatedBy).HasColumnName("MenteeLastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.MenteeLastUpdatedByName).HasColumnName("MenteeLastUpdatedByName").IsOptional().HasMaxLength(129);
            Property(x => x.Department).HasColumnName("Department").IsOptional().HasMaxLength(250);
            Property(x => x.PubGuid).HasColumnName("Pub_GUID").IsOptional();
            Property(x => x.PubTitle).HasColumnName("PubTitle").IsOptional();
            Property(x => x.PubYearPublished).HasColumnName("PubYearPublished").IsOptional();
            Property(x => x.PubAbstract).HasColumnName("PubAbstract").IsOptional();
            Property(x => x.PubPmcid).HasColumnName("PubPMCID").IsOptional().HasMaxLength(15);
            Property(x => x.PubPmid).HasColumnName("PubPMID").IsOptional().HasMaxLength(15);
            Property(x => x.PubManuscriptId).HasColumnName("PubManuscriptId").IsOptional().HasMaxLength(15);
            Property(x => x.PubJournal).HasColumnName("PubJournal").IsOptional();
            Property(x => x.PubVolume).HasColumnName("PubVolume").IsOptional().HasMaxLength(100);
            Property(x => x.PubIssue).HasColumnName("PubIssue").IsOptional().HasMaxLength(100);
            Property(x => x.PubPagination).HasColumnName("PubPagination").IsOptional().HasMaxLength(50);
            Property(x => x.PubCitation).HasColumnName("PubCitation").IsOptional();
            Property(x => x.PubIsPublicAccess).HasColumnName("PubIsPublicAccess").IsOptional();
            Property(x => x.PubIsMenteeFirstAuthor).HasColumnName("PubIsMenteeFirstAuthor").IsOptional();
            Property(x => x.PubIsAdvisorCoAuthor).HasColumnName("PubIsAdvisorCoAuthor").IsOptional();
            Property(x => x.PubAuthorList).HasColumnName("PubAuthorList").IsOptional();
            Property(x => x.PubDatePublicationLastUpdated).HasColumnName("PubDatePublicationLastUpdated").IsOptional();
            Property(x => x.PubDateLastUpdated).HasColumnName("PubDateLastUpdated").IsOptional();
            Property(x => x.PubLastUpdatedBy).HasColumnName("PubLastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.PubLastUpdatedByName).HasColumnName("PubLastUpdatedByName").IsOptional().HasMaxLength(129);
        }
    }

}

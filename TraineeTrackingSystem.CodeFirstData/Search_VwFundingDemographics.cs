// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FundingDemographics
    public class Search_VwFundingDemographics
    {
        public Guid Id { get; set; } // Id
        public Guid PiFacultyId { get; set; } // PIFacultyId
        public Guid PiPersonId { get; set; } // PIPersonId
        public string SponsorAwardNumber { get; set; } // SponsorAwardNumber
        public string Title { get; set; } // Title
        public int FundingStatusId { get; set; } // FundingStatusId
        public string FundingStatus { get; set; } // FundingStatus
        public DateTime? DateProjectedStart { get; set; } // DateProjectedStart
        public DateTime? DateProjectedEnd { get; set; } // DateProjectedEnd
        public int? SponsorId { get; set; } // SponsorId
        public string SponsorName { get; set; } // SponsorName
        public string SponsorReferenceNumber { get; set; } // SponsorReferenceNumber
        public string GrtNumber { get; set; } // GRTNumber
        public DateTime? DateTimeFundingStarted { get; set; } // DateTimeFundingStarted
        public DateTime? DateTimeFundingEnded { get; set; } // DateTimeFundingEnded
        public int? FundingTypeId { get; set; } // FundingTypeId
        public string FundingType { get; set; } // FundingType
        public bool IsRenewal { get; set; } // IsRenewal
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public string LastUpdatedByName { get; set; } // LastUpdatedByName
        public string PiLastName { get; set; } // PILastName
        public string PiFirstName { get; set; } // PIFirstName
        public string PiFullName { get; set; } // PIFullName
        public Guid? ProgramProjectPdId { get; set; } // ProgramProjectPDId
        public string ProgramProjectPdLastName { get; set; } // ProgramProjectPDLastName
        public string ProgramProjectPdFirstName { get; set; } // ProgramProjectPDFirstName
        public string ProgramProjectPdFullName { get; set; } // ProgramProjectPDFullName
        public string PrimeAwardNumber { get; set; } // PrimeAwardNumber
        public int? PrimeSponsorId { get; set; } // PrimeSponsorId
        public string PrimeSponsorName { get; set; } // PrimeSponsorName
        public Guid? PrimeAwardPiId { get; set; } // PrimeAwardPIId
        public string PrimeAwardPiLastName { get; set; } // PrimeAwardPILastName
        public string PrimeAwardPiFirstName { get; set; } // PrimeAwardPIFirstName
        public string PrimeAwardPiFullName { get; set; } // PrimeAwardPIFullName
        public DateTime? DisplayStartDate { get; set; } // DisplayStartDate
        public DateTime? DisplayEndDate { get; set; } // DisplayEndDate
    }

}

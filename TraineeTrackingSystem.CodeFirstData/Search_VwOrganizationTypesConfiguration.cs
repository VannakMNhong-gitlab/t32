// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_OrganizationTypes
    internal class Search_VwOrganizationTypesConfiguration : EntityTypeConfiguration<Search_VwOrganizationTypes>
    {
        public Search_VwOrganizationTypesConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_OrganizationTypes");
            HasKey(x => new { x.OrganizationType, x.OrganizationTypeId });

            Property(x => x.OrganizationTypeId).HasColumnName("OrganizationTypeId").IsRequired();
            Property(x => x.OrganizationType).HasColumnName("OrganizationType").IsRequired().HasMaxLength(150);
            Property(x => x.TotalOrganizationTypeUsage).HasColumnName("TotalOrganizationTypeUsage").IsOptional();
        }
    }

}

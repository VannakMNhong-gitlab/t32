// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_T32Table5B
    public class Reports_VwT32Table5B
    {
        public Guid FacultyId { get; set; } // FacultyId
        public Guid FacultyPersonId { get; set; } // FacultyPersonId
        public string FacultyLastName { get; set; } // FacultyLastName
        public string FacultyFirstName { get; set; } // FacultyFirstName
        public string FacultyFullName { get; set; } // FacultyFullName
        public string FacultyNameAbbrev { get; set; } // FacultyNameAbbrev
        public Guid TrainingGrantId { get; set; } // TrainingGrantId
        public Guid FundingId { get; set; } // FundingId
        public string Title { get; set; } // Title
        public bool IsRenewal { get; set; } // IsRenewal
        public Guid? MenteeId { get; set; } // MenteeId
        public string StudentId { get; set; } // StudentId
        public Guid? MenteePersonId { get; set; } // MenteePersonId
        public string MenteeLastName { get; set; } // MenteeLastName
        public string MenteeFirstName { get; set; } // MenteeFirstName
        public string MenteeFullName { get; set; } // MenteeFullName
        public string MenteeNameAbbrev { get; set; } // MenteeNameAbbrev
        public int? MenteeTypeId { get; set; } // MenteeTypeId
        public string MenteeType { get; set; } // MenteeType
        public bool? IsTrainingGrantEligible { get; set; } // IsTrainingGrantEligible
        public int? TrainingPeriodYearStarted { get; set; } // TrainingPeriodYearStarted
        public int? TrainingPeriodYearEnded { get; set; } // TrainingPeriodYearEnded
        public int? TrainingPeriodInstitutionId { get; set; } // TrainingPeriodInstitutionId
        public string TrainingPeriodInstitution { get; set; } // TrainingPeriodInstitution
        public int? TrainingPeriodDegreeId { get; set; } // TrainingPeriodDegreeId
        public string TrainingPeriodDegree { get; set; } // TrainingPeriodDegree
        public string ResearchProjectTitle { get; set; } // ResearchProjectTitle
        public string SupportSourceText { get; set; } // SupportSourceText
        public string PositionTitle { get; set; } // PositionTitle
        public DateTime? DateStartedPosition { get; set; } // DateStartedPosition
        public DateTime? DateEndedPosition { get; set; } // DateEndedPosition
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // OSUTimeline
    internal class OsuTimelineConfiguration : EntityTypeConfiguration<OsuTimeline>
    {
        public OsuTimelineConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".OSUTimeline");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.YearEntered).HasColumnName("YearEntered").IsOptional();
            Property(x => x.DateApplied).HasColumnName("DateApplied").IsOptional();
            Property(x => x.WasInterviewed).HasColumnName("WasInterviewed").IsOptional();
            Property(x => x.WasOfferedPosition).HasColumnName("WasOfferedPosition").IsOptional();
            Property(x => x.AcceptedOffer).HasColumnName("AcceptedOffer").IsOptional();
            Property(x => x.WasEnrolled).HasColumnName("WasEnrolled").IsOptional();
            Property(x => x.EnteredProgram).HasColumnName("EnteredProgram").IsOptional();
            Property(x => x.DateStarted).HasColumnName("DateStarted").IsOptional();
            Property(x => x.DateEnded).HasColumnName("DateEnded").IsOptional();
            Property(x => x.DeclinedOffer).HasColumnName("DeclinedOffer").IsOptional();
            Property(x => x.DeclinedAltInstitutionId).HasColumnName("Declined_AltInstitutionId").IsOptional();
            Property(x => x.WasConsideredForAdmission).HasColumnName("WasConsideredForAdmission").IsOptional();
            Property(x => x.WasAccepted).HasColumnName("WasAccepted").IsOptional();
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();

            // Foreign keys
            HasRequired(a => a.Person).WithMany(b => b.OsuTimelines).HasForeignKey(c => c.PersonId); // FK_OSUTimeline_Person
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FacultyTracks
    internal class Search_VwFacultyTracksConfiguration : EntityTypeConfiguration<Search_VwFacultyTracks>
    {
        public Search_VwFacultyTracksConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_FacultyTracks");
            HasKey(x => new { x.FacultyTrack, x.FacultyTrackId });

            Property(x => x.FacultyTrackId).HasColumnName("FacultyTrackId").IsRequired();
            Property(x => x.FacultyTrack).HasColumnName("FacultyTrack").IsRequired().HasMaxLength(500);
            Property(x => x.TotalFacultyTrackUsage).HasColumnName("TotalFacultyTrackUsage").IsOptional();
        }
    }

}

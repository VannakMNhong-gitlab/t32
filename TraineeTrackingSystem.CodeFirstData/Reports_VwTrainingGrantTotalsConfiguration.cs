// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingGrantTotals
    internal class Reports_VwTrainingGrantTotalsConfiguration : EntityTypeConfiguration<Reports_VwTrainingGrantTotals>
    {
        public Reports_VwTrainingGrantTotalsConfiguration(string schema = "Reports")
        {
            ToTable(schema + ".vw_TrainingGrantTotals");
            HasKey(x => x.TrainingGrantId);

            Property(x => x.TrainingGrantId).HasColumnName("TrainingGrantId").IsRequired();
            Property(x => x.TrainingGrantTitle).HasColumnName("TrainingGrantTitle").IsOptional();
            Property(x => x.TotalFaculty).HasColumnName("TotalFaculty").IsOptional();
            Property(x => x.TotalMentees).HasColumnName("TotalMentees").IsOptional();
            Property(x => x.TotalApplicants).HasColumnName("TotalApplicants").IsOptional();
            Property(x => x.TotalTrainees).HasColumnName("TotalTrainees").IsOptional();
            Property(x => x.TotalPredocTrainees).HasColumnName("TotalPredocTrainees").IsOptional();
            Property(x => x.TotalPostdocTrainees).HasColumnName("TotalPostdocTrainees").IsOptional();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingPeriodMentors
    internal class Search_VwTrainingPeriodMentorsConfiguration : EntityTypeConfiguration<Search_VwTrainingPeriodMentors>
    {
        public Search_VwTrainingPeriodMentorsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_TrainingPeriodMentors");
            HasKey(x => new { x.EmployeeId, x.TrainingPeriodId, x.FacultyId, x.MentorPersonId });

            Property(x => x.TrainingPeriodId).HasColumnName("TrainingPeriodId").IsRequired();
            Property(x => x.FacultyId).HasColumnName("FacultyId").IsRequired();
            Property(x => x.MentorPersonId).HasColumnName("MentorPersonId").IsRequired();
            Property(x => x.MentorFirstName).HasColumnName("MentorFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MentorMiddleName).HasColumnName("MentorMiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.MentorLastName).HasColumnName("MentorLastName").IsOptional().HasMaxLength(75);
            Property(x => x.MentorFullName).HasColumnName("MentorFullName").IsOptional().HasMaxLength(178);
            Property(x => x.EmployeeId).HasColumnName("EmployeeId").IsRequired().HasMaxLength(50);
            Property(x => x.PrimaryOrganizationId).HasColumnName("PrimaryOrganizationId").IsOptional();
            Property(x => x.MentorPrimaryOrganization).HasColumnName("MentorPrimaryOrganization").IsOptional().HasMaxLength(250);
            Property(x => x.InstitutionId).HasColumnName("InstitutionId").IsOptional();
            Property(x => x.InstitutionIdentifier).HasColumnName("InstitutionIdentifier").IsOptional().HasMaxLength(150);
            Property(x => x.InstitutionName).HasColumnName("InstitutionName").IsOptional().HasMaxLength(250);
            Property(x => x.InstitutionCity).HasColumnName("InstitutionCity").IsOptional().HasMaxLength(150);
            Property(x => x.InstitutionStateId).HasColumnName("InstitutionStateId").IsOptional();
            Property(x => x.InstitutionStateAbbreviation).HasColumnName("InstitutionStateAbbreviation").IsOptional().HasMaxLength(50);
            Property(x => x.InstitutionStateFullName).HasColumnName("InstitutionStateFullName").IsOptional().HasMaxLength(50);
            Property(x => x.InstitutionCountryId).HasColumnName("InstitutionCountryId").IsOptional();
            Property(x => x.InstitutionCountry).HasColumnName("InstitutionCountry").IsOptional().HasMaxLength(150);
            Property(x => x.YearStarted).HasColumnName("YearStarted").IsOptional();
            Property(x => x.YearEnded).HasColumnName("YearEnded").IsOptional();
            Property(x => x.ResearchProjectTitle).HasColumnName("ResearchProjectTitle").IsOptional().HasMaxLength(250);
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.LastUpdatedByName).HasColumnName("LastUpdatedByName").IsOptional().HasMaxLength(129);
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_ApplicantData
    public class Search_VwApplicantData
    {
        public Guid Id { get; set; } // Id
        public Guid PersonId { get; set; } // PersonId
        public string FirstName { get; set; } // FirstName
        public string MiddleName { get; set; } // MiddleName
        public string LastName { get; set; } // LastName
        public string FullName { get; set; } // FullName
        public int ApplicantId { get; set; } // ApplicantId
        public bool IsTrainingGrantEligible { get; set; } // IsTrainingGrantEligible
        public int? IsFromDisadvantagedBkgd { get; set; } // IsFromDisadvantagedBkgd
        public int? IsIndividualWithDisabilities { get; set; } // IsIndividualWithDisabilities
        public int? IsUnderrepresentedMinority { get; set; } // IsUnderrepresentedMinority
        public int? DepartmentId { get; set; } // DepartmentId
        public string Department { get; set; } // Department
        public int? ApplicantTypeId { get; set; } // ApplicantTypeId
        public string ApplicantType { get; set; } // ApplicantType
        public Guid ProgramId { get; set; } // ProgramId
        public string Program { get; set; } // Program
        public int? YearEntered { get; set; } // YearEntered
        public DateTime? ApplicantDateLastUpdated { get; set; } // ApplicantDateLastUpdated
        public string ApplicantLastUpdatedBy { get; set; } // ApplicantLastUpdatedBy
        public string ApplicantLastUpdatedByName { get; set; } // ApplicantLastUpdatedByName
    }

}

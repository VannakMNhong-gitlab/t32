// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_UsageOfClinicalFacultyPathways
    internal class Search_VwUsageOfClinicalFacultyPathwaysConfiguration : EntityTypeConfiguration<Search_VwUsageOfClinicalFacultyPathways>
    {
        public Search_VwUsageOfClinicalFacultyPathwaysConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_UsageOfClinicalFacultyPathways");
            HasKey(x => new { x.ClinicalFacultyPathwayId, x.ClinicalFacultyPathway });

            Property(x => x.ClinicalFacultyPathwayId).HasColumnName("ClinicalFacultyPathwayId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.ClinicalFacultyPathway).HasColumnName("ClinicalFacultyPathway").IsRequired().HasMaxLength(500);
            Property(x => x.TotalClinicalFacultyPathways).HasColumnName("TotalClinicalFacultyPathways").IsOptional();
        }
    }

}

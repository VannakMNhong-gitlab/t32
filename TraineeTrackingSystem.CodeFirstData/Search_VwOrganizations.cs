// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_Organizations
    public class Search_VwOrganizations
    {
        public int Id { get; set; } // Id
        public string OrganizationId { get; set; } // OrganizationId
        public string DisplayName { get; set; } // DisplayName
        public string HrName { get; set; } // HRName
        public int OrganizationTypeId { get; set; } // OrganizationTypeId
        public string OrganizationType { get; set; } // OrganizationType
        public int? ParentOrganizationId { get; set; } // ParentOrganizationId
        public string ParentOrgId { get; set; } // ParentOrgId
        public string ParentOrgDisplayName { get; set; } // ParentOrgDisplayName
        public string ParentOrgHrName { get; set; } // ParentOrgHRName
        public int? ParentOrgTypeId { get; set; } // ParentOrgTypeId
        public string ParentOrgType { get; set; } // ParentOrgType
        public int? TotalOrganizationUsage { get; set; } // TotalOrganizationUsage
    }

}

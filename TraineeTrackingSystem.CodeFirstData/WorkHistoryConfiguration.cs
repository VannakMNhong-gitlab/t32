// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // WorkHistory
    internal class WorkHistoryConfiguration : EntityTypeConfiguration<WorkHistory>
    {
        public WorkHistoryConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".WorkHistory");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.DateStartedPosition).HasColumnName("DateStartedPosition").IsOptional();
            Property(x => x.DateEndedPosition).HasColumnName("DateEndedPosition").IsOptional();
            Property(x => x.InstitutionId).HasColumnName("InstitutionId").IsOptional();
            Property(x => x.PositionTitle).HasColumnName("PositionTitle").IsOptional().HasMaxLength(200);
            Property(x => x.PositionDescription).HasColumnName("PositionDescription").IsOptional().HasMaxLength(500);
            Property(x => x.PositionLocation).HasColumnName("PositionLocation").IsOptional().HasMaxLength(200);
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
            Property(x => x.SupportSourceText).HasColumnName("SupportSourceText").IsOptional();

            // Foreign keys
            HasRequired(a => a.Person).WithMany(b => b.WorkHistories).HasForeignKey(c => c.PersonId); // FK_WorkHistory_Person
            HasOptional(a => a.Institution).WithMany(b => b.WorkHistories).HasForeignKey(c => c.InstitutionId); // FK_WorkHistory_Institution
        }
    }

}

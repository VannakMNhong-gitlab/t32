// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_T32Table4
    public class Reports_VwT32Table4
    {
        public Guid FacultyId { get; set; } // FacultyId
        public Guid FacultyPersonId { get; set; } // FacultyPersonId
        public string FacultyLastName { get; set; } // FacultyLastName
        public string FacultyFirstName { get; set; } // FacultyFirstName
        public string FacultyFullName { get; set; } // FacultyFullName
        public string FacultyFullNameAbbrev { get; set; } // FacultyFullNameAbbrev
        public int? FacultyRoleId { get; set; } // FacultyRoleId
        public string FacultyRole { get; set; } // FacultyRole
        public Guid? FundingId { get; set; } // FundingId
        public Guid TrainingGrantId { get; set; } // TrainingGrantId
        public string Title { get; set; } // Title
        public int? StatusId { get; set; } // StatusId
        public string StatusName { get; set; } // StatusName
        public int? SponsorId { get; set; } // SponsorId
        public string SponsorName { get; set; } // SponsorName
        public string SponsorAwardNumber { get; set; } // SponsorAwardNumber
        public DateTime? DateProjectedStart { get; set; } // DateProjectedStart
        public DateTime? DateProjectedEnd { get; set; } // DateProjectedEnd
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded
        public string GrtNumber { get; set; } // GRTNumber
        public string PrimeAward { get; set; } // PrimeAward
    }

}

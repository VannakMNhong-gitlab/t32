// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FacultyAppointments
    internal class Search_VwFacultyAppointmentsConfiguration : EntityTypeConfiguration<Search_VwFacultyAppointments>
    {
        public Search_VwFacultyAppointmentsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_FacultyAppointments");
            HasKey(x => new { x.InstitutionName, x.FacultyPersonId, x.InstitutionId, x.FacultyAppointmentId });

            Property(x => x.FacultyAppointmentId).HasColumnName("FacultyAppointmentId").IsRequired();
            Property(x => x.FacultyPersonId).HasColumnName("FacultyPersonId").IsRequired();
            Property(x => x.FirstName).HasColumnName("FirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MiddleName).HasColumnName("MiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.LastName).HasColumnName("LastName").IsOptional().HasMaxLength(75);
            Property(x => x.FullName).HasColumnName("FullName").IsOptional().HasMaxLength(178);
            Property(x => x.InstitutionId).HasColumnName("InstitutionId").IsRequired();
            Property(x => x.InstitutionName).HasColumnName("InstitutionName").IsRequired().HasMaxLength(250);
            Property(x => x.FacultyTrackId).HasColumnName("FacultyTrackId").IsOptional();
            Property(x => x.FacultyTrack).HasColumnName("FacultyTrack").IsOptional().HasMaxLength(500);
            Property(x => x.ClinicalFacultyPathwayId).HasColumnName("ClinicalFacultyPathwayId").IsOptional();
            Property(x => x.ClinicalFacultyPathway).HasColumnName("ClinicalFacultyPathway").IsOptional().HasMaxLength(500);
            Property(x => x.FacultyRankId).HasColumnName("FacultyRankId").IsOptional();
            Property(x => x.FacultyRank).HasColumnName("FacultyRank").IsOptional().HasMaxLength(150);
            Property(x => x.OtherAffiliations).HasColumnName("OtherAffiliations").IsOptional().HasMaxLength(1500);
            Property(x => x.OtherTitles).HasColumnName("OtherTitles").IsOptional().HasMaxLength(1500);
            Property(x => x.PercentFte).HasColumnName("PercentFTE").IsOptional().HasPrecision(10,2);
            Property(x => x.DateStarted).HasColumnName("DateStarted").IsOptional();
            Property(x => x.DateEnded).HasColumnName("DateEnded").IsOptional();
            Property(x => x.Comment).HasColumnName("Comment").IsOptional();
            Property(x => x.AppointmentDateLastUpdated).HasColumnName("AppointmentDateLastUpdated").IsOptional();
            Property(x => x.AppointmentLastUpdatedBy).HasColumnName("AppointmentLastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.AppointmentLastUpdatedByName).HasColumnName("AppointmentLastUpdatedByName").IsOptional().HasMaxLength(129);
        }
    }

}

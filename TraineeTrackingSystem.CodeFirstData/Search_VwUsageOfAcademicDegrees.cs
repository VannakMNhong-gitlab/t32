// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_UsageOfAcademicDegrees
    public class Search_VwUsageOfAcademicDegrees
    {
        public int DegreeId { get; set; } // DegreeId
        public string DegreeName { get; set; } // DegreeName
        public int? TotalAcademicHistory { get; set; } // TotalAcademicHistory
        public int? TotalFacultyAcademics { get; set; } // TotalFacultyAcademics
        public int? TotalTrainingPeriods { get; set; } // TotalTrainingPeriods
    }

}

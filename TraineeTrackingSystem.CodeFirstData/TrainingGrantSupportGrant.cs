// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // TrainingGrantSupportGrant
    public class TrainingGrantSupportGrant
    {
        public Guid TrainingGrantId { get; set; } // TrainingGrantId (Primary key)
        public Guid SupportTrainingGrantId { get; set; } // SupportTrainingGrantId (Primary key)
        public bool IsDeleted { get; set; } // IsDeleted

        // Foreign keys
        public virtual TrainingGrant TrainingGrant { get; set; } // FK_TrainingGrantSupportGrant_SupportTrainingGrant

        public TrainingGrantSupportGrant()
        {
            IsDeleted = false;
        }
    }

}

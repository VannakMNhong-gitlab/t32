// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FundingRoles
    internal class Search_VwFundingRolesConfiguration : EntityTypeConfiguration<Search_VwFundingRoles>
    {
        public Search_VwFundingRolesConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_FundingRoles");
            HasKey(x => new { x.IsTrainingGrantRole, x.FundingRoleId, x.Name });

            Property(x => x.FundingRoleId).HasColumnName("FundingRoleId").IsRequired();
            Property(x => x.Name).HasColumnName("Name").IsRequired().HasMaxLength(150);
            Property(x => x.IsTrainingGrantRole).HasColumnName("IsTrainingGrantRole").IsRequired();
            Property(x => x.TotalFundingRoleUsage).HasColumnName("TotalFundingRoleUsage").IsOptional();
            Property(x => x.TotalTrainingGrantRoleUsage).HasColumnName("TotalTrainingGrantRoleUsage").IsOptional();
        }
    }

}

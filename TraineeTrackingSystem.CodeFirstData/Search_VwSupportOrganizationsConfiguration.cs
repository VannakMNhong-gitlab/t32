// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_SupportOrganizations
    internal class Search_VwSupportOrganizationsConfiguration : EntityTypeConfiguration<Search_VwSupportOrganizations>
    {
        public Search_VwSupportOrganizationsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_SupportOrganizations");
            HasKey(x => new { x.Id, x.Name });

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Name).HasColumnName("Name").IsRequired().HasMaxLength(150);
            Property(x => x.Abbreviation).HasColumnName("Abbreviation").IsOptional().HasMaxLength(50);
            Property(x => x.FullName).HasColumnName("FullName").IsOptional().HasMaxLength(203);
            Property(x => x.SortOrder).HasColumnName("SortOrder").IsOptional();
        }
    }

}

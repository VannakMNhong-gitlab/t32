// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Support
    internal class SupportConfiguration : EntityTypeConfiguration<Support>
    {
        public SupportConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".Support");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.Title).HasColumnName("Title").IsRequired();
            Property(x => x.DisplayId).HasColumnName("DisplayId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.SupportTypeId).HasColumnName("SupportTypeId").IsOptional();
            Property(x => x.SupportStatusId).HasColumnName("SupportStatusId").IsRequired();
            Property(x => x.SupportNumber).HasColumnName("SupportNumber").IsOptional().HasMaxLength(20);
            Property(x => x.SupportOrganizationId).HasColumnName("SupportOrganizationId").IsOptional();
            Property(x => x.DateStarted).HasColumnName("DateStarted").IsOptional();
            Property(x => x.DateEnded).HasColumnName("DateEnded").IsOptional();
            Property(x => x.DateCreated).HasColumnName("DateCreated").IsOptional();
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();

            // Foreign keys
            HasOptional(a => a.SupportType).WithMany(b => b.Supports).HasForeignKey(c => c.SupportTypeId); // FK_Support_SupportType
            HasRequired(a => a.SupportStatu).WithMany(b => b.Supports).HasForeignKey(c => c.SupportStatusId); // FK_Support_SupportStatus
            HasOptional(a => a.SupportOrganization).WithMany(b => b.Supports).HasForeignKey(c => c.SupportOrganizationId); // FK_Support_SupportOrganization
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_UsageOfGenders
    internal class Search_VwUsageOfGendersConfiguration : EntityTypeConfiguration<Search_VwUsageOfGenders>
    {
        public Search_VwUsageOfGendersConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_UsageOfGenders");
            HasKey(x => new { x.Gender, x.GenderId });

            Property(x => x.GenderId).HasColumnName("GenderId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Gender).HasColumnName("Gender").IsRequired().HasMaxLength(100);
            Property(x => x.TotalGenders).HasColumnName("TotalGenders").IsOptional();
        }
    }

}

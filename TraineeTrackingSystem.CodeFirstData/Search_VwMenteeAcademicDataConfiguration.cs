// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_MenteeAcademicData
    internal class Search_VwMenteeAcademicDataConfiguration : EntityTypeConfiguration<Search_VwMenteeAcademicData>
    {
        public Search_VwMenteeAcademicDataConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_MenteeAcademicData");
            HasKey(x => new { x.WasRecruitedToLab, x.MenteeId, x.StudentId, x.DisplayId, x.MenteePersonId, x.IsTrainingGrantEligible });

            Property(x => x.MenteeId).HasColumnName("MenteeId").IsRequired();
            Property(x => x.MenteePersonId).HasColumnName("MenteePersonId").IsRequired();
            Property(x => x.FirstName).HasColumnName("FirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MiddleName).HasColumnName("MiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.LastName).HasColumnName("LastName").IsOptional().HasMaxLength(75);
            Property(x => x.FullName).HasColumnName("FullName").IsOptional().HasMaxLength(178);
            Property(x => x.DisplayId).HasColumnName("DisplayId").IsRequired();
            Property(x => x.StudentId).HasColumnName("StudentId").IsRequired().HasMaxLength(50);
            Property(x => x.IsTrainingGrantEligible).HasColumnName("IsTrainingGrantEligible").IsRequired();
            Property(x => x.WasRecruitedToLab).HasColumnName("WasRecruitedToLab").IsRequired();
            Property(x => x.Department).HasColumnName("Department").IsOptional().HasMaxLength(250);
            Property(x => x.InstitutionAssociation).HasColumnName("InstitutionAssociation").IsOptional().HasMaxLength(50);
            Property(x => x.MenteeDateLastUpdated).HasColumnName("MenteeDateLastUpdated").IsOptional();
            Property(x => x.MenteeLastUpdatedBy).HasColumnName("MenteeLastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.MenteeLastUpdatedByName).HasColumnName("MenteeLastUpdatedByName").IsOptional().HasMaxLength(129);
            Property(x => x.Program).HasColumnName("Program").IsOptional().HasMaxLength(250);
            Property(x => x.Gpa).HasColumnName("GPA").IsOptional().HasPrecision(10,2);
            Property(x => x.GpaScale).HasColumnName("GPAScale").IsOptional().HasPrecision(10,2);
            Property(x => x.GradingInstitutionId).HasColumnName("GradingInstitutionId").IsOptional();
            Property(x => x.GradingInstitution).HasColumnName("GradingInstitution").IsOptional().HasMaxLength(250);
            Property(x => x.TestScoreTypeId).HasColumnName("TestScoreTypeId").IsOptional();
            Property(x => x.TestScoreType).HasColumnName("TestScoreType").IsOptional().HasMaxLength(50);
            Property(x => x.YearTested).HasColumnName("YearTested").IsOptional();
            Property(x => x.GreScoreVerbal).HasColumnName("GREScoreVerbal").IsOptional().HasPrecision(10,2);
            Property(x => x.GreScoreQuantitative).HasColumnName("GREScoreQuantitative").IsOptional().HasPrecision(10,2);
            Property(x => x.GreScoreAnalytical).HasColumnName("GREScoreAnalytical").IsOptional().HasPrecision(10,2);
            Property(x => x.GreScoreSubject).HasColumnName("GREScoreSubject").IsOptional().HasPrecision(10,2);
            Property(x => x.GrePercentileVerbal).HasColumnName("GREPercentileVerbal").IsOptional().HasPrecision(10,2);
            Property(x => x.GrePercentileQuantitative).HasColumnName("GREPercentileQuantitative").IsOptional().HasPrecision(10,2);
            Property(x => x.GrePercentileAnalytical).HasColumnName("GREPercentileAnalytical").IsOptional().HasPrecision(10,2);
            Property(x => x.GrePercentileSubject).HasColumnName("GREPercentileSubject").IsOptional().HasPrecision(10,2);
            Property(x => x.McatScoreVerbalReasoning).HasColumnName("MCATScoreVerbalReasoning").IsOptional().HasPrecision(10,2);
            Property(x => x.McatScorePhysicalSciences).HasColumnName("MCATScorePhysicalSciences").IsOptional().HasPrecision(10,2);
            Property(x => x.McatScoreBiologicalSciences).HasColumnName("MCATScoreBiologicalSciences").IsOptional().HasPrecision(10,2);
            Property(x => x.McatScoreWriting).HasColumnName("MCATScoreWriting").IsOptional().HasMaxLength(5);
            Property(x => x.McatPercentile).HasColumnName("MCATPercentile").IsOptional().HasPrecision(10,2);
            Property(x => x.Degree).HasColumnName("Degree").IsOptional().HasMaxLength(25);
            Property(x => x.DegreeInstitutionId).HasColumnName("DegreeInstitutionId").IsOptional();
            Property(x => x.DegreeInstitution).HasColumnName("DegreeInstitution").IsOptional().HasMaxLength(250);
            Property(x => x.DegreeInstitutionStateProvince).HasColumnName("DegreeInstitutionStateProvince").IsOptional().HasMaxLength(50);
            Property(x => x.DegreeInstitutionCountry).HasColumnName("DegreeInstitutionCountry").IsOptional().HasMaxLength(150);
            Property(x => x.DegreeAreaOfStudy).HasColumnName("DegreeAreaOfStudy").IsOptional().HasMaxLength(150);
            Property(x => x.DegreeYearStarted).HasColumnName("DegreeYearStarted").IsOptional();
            Property(x => x.DegreeYearEnded).HasColumnName("DegreeYearEnded").IsOptional();
            Property(x => x.YearDegreeCompleted).HasColumnName("YearDegreeCompleted").IsOptional();
            Property(x => x.DegreeIsUndergrad).HasColumnName("DegreeIsUndergrad").IsOptional();
            Property(x => x.DegreeResearchProjTitle).HasColumnName("DegreeResearchProjTitle").IsOptional().HasMaxLength(500);
            Property(x => x.DegreeResearchAdvisor).HasColumnName("DegreeResearchAdvisor").IsOptional().HasMaxLength(250);
            Property(x => x.DegreeDoctoralThesis).HasColumnName("DegreeDoctoralThesis").IsOptional().HasMaxLength(500);
            Property(x => x.DegreeIsResidency).HasColumnName("DegreeIsResidency").IsOptional();
            Property(x => x.ResidencyInstitutionId).HasColumnName("ResidencyInstitutionId").IsOptional();
            Property(x => x.ResidencyInstitution).HasColumnName("ResidencyInstitution").IsOptional().HasMaxLength(250);
            Property(x => x.ResidencySpecialization).HasColumnName("ResidencySpecialization").IsOptional().HasMaxLength(500);
            Property(x => x.ResidencyPgy).HasColumnName("ResidencyPGY").IsOptional().HasMaxLength(500);
            Property(x => x.ResidencyAdvisor).HasColumnName("ResidencyAdvisor").IsOptional().HasMaxLength(250);
            Property(x => x.ResidencyYearStarted).HasColumnName("ResidencyYearStarted").IsOptional();
            Property(x => x.ResidencyYearEnded).HasColumnName("ResidencyYearEnded").IsOptional();
            Property(x => x.IsNonMedicalTransfer).HasColumnName("IsNonMedicalTransfer").IsOptional();
            Property(x => x.IsPhDTransfer).HasColumnName("IsPhDTransfer").IsOptional();
            Property(x => x.HistoryDateLastUpdated).HasColumnName("HistoryDateLastUpdated").IsOptional();
            Property(x => x.HistoryLastUpdatedBy).HasColumnName("HistoryLastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.HistoryLastUpdatedByName).HasColumnName("HistoryLastUpdatedByName").IsOptional().HasMaxLength(129);
            Property(x => x.DegreeTrainingDoctoralLevel).HasColumnName("DegreeTrainingDoctoralLevel").IsOptional().HasMaxLength(150);
        }
    }

}

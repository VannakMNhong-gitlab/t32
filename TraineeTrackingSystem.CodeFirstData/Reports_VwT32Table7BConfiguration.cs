// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_T32Table7B
    internal class Reports_VwT32Table7BConfiguration : EntityTypeConfiguration<Reports_VwT32Table7B>
    {
        public Reports_VwT32Table7BConfiguration(string schema = "Reports")
        {
            ToTable(schema + ".vw_T32Table7B");
            HasKey(x => new { x.TrainingGrantId, x.Column1ItemTypeId, x.IsRenewal, x.Column1ItemType });

            Property(x => x.TrainingGrantId).HasColumnName("TrainingGrantId").IsRequired();
            Property(x => x.IsRenewal).HasColumnName("IsRenewal").IsRequired();
            Property(x => x.DoctoralLevelId).HasColumnName("DoctoralLevelId").IsOptional();
            Property(x => x.DoctoralLevel).HasColumnName("DoctoralLevel").IsOptional().HasMaxLength(150);
            Property(x => x.Column1ItemTypeId).HasColumnName("Column1ItemTypeId").IsRequired();
            Property(x => x.Column1ItemType).HasColumnName("Column1ItemType").IsRequired().HasMaxLength(10);
            Property(x => x.Column1ItemId).HasColumnName("Column1ItemId").IsOptional();
            Property(x => x.Column1ItemName).HasColumnName("Column1ItemName").IsOptional().HasMaxLength(250);
        }
    }

}

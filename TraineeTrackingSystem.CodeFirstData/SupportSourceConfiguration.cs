// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // SupportSource
    internal class SupportSourceConfiguration : EntityTypeConfiguration<SupportSource>
    {
        public SupportSourceConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".SupportSource");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.SupportTitle).HasColumnName("SupportTitle").IsRequired().HasMaxLength(500);
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
            HasMany(t => t.WorkHistories).WithMany(t => t.SupportSources).Map(m => 
            {
                m.ToTable("WorkHistorySupportSource");
                m.MapLeftKey("SupportSourceId");
                m.MapRightKey("WorkHistoryId");
            });
        }
    }

}

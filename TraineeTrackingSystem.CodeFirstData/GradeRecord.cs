// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // GradeRecord
    public class GradeRecord
    {
        public Guid Id { get; set; } // Id (Primary key)
        public Guid PersonId { get; set; } // PersonId
        public int? InstitutionId { get; set; } // InstitutionId
        public Decimal? Gpa { get; set; } // GPA
        public Decimal? GpaScale { get; set; } // GPAScale
        public int? YearTested { get; set; } // YearTested
        public int? TestScoreTypeId { get; set; } // TestScoreTypeId
        public Decimal? GreScoreVerbal { get; set; } // GREScoreVerbal
        public Decimal? GreScoreQuantitative { get; set; } // GREScoreQuantitative
        public Decimal? GreScoreAnalytical { get; set; } // GREScoreAnalytical
        public Decimal? GreScoreSubject { get; set; } // GREScoreSubject
        public Decimal? GrePercentileVerbal { get; set; } // GREPercentileVerbal
        public Decimal? GrePercentileQuantitative { get; set; } // GREPercentileQuantitative
        public Decimal? GrePercentileAnalytical { get; set; } // GREPercentileAnalytical
        public Decimal? GrePercentileSubject { get; set; } // GREPercentileSubject
        public Decimal? McatScoreVerbalReasoning { get; set; } // MCATScoreVerbalReasoning
        public Decimal? McatScorePhysicalSciences { get; set; } // MCATScorePhysicalSciences
        public Decimal? McatScoreBiologicalSciences { get; set; } // MCATScoreBiologicalSciences
        public string McatScoreWriting { get; set; } // MCATScoreWriting
        public Decimal? McatPercentile { get; set; } // MCATPercentile
        public string GpaComment { get; set; } // GPAComment
        public bool IsDeleted { get; set; } // IsDeleted

        // Foreign keys
        public virtual Institution Institution { get; set; } // FK_GradeRecord_Institution
        public virtual Person Person { get; set; } // FK_GradeRecord_Person
        public virtual TestScoreType TestScoreType { get; set; } // FK_GradeRecord_TestScoreType

        public GradeRecord()
        {
            Id = System.Guid.NewGuid();
            IsDeleted = false;
        }
    }

}

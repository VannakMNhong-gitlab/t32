// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingGrantDepartments
    public class Search_VwTrainingGrantDepartments
    {
        public Guid TrainingGrantId { get; set; } // TrainingGrantId
        public int? SponsorId { get; set; } // SponsorId
        public string InstitutionIdentifier { get; set; } // InstitutionIdentifier
        public string InstitutionName { get; set; } // InstitutionName
        public string InstitutionCity { get; set; } // InstitutionCity
        public int? InstitutionStateId { get; set; } // InstitutionStateId
        public string InstitutionStateAbbreviation { get; set; } // InstitutionStateAbbreviation
        public string InstitutionStateFullName { get; set; } // InstitutionStateFullName
        public int? InstitutionCountryId { get; set; } // InstitutionCountryId
        public string InstitutionCountry { get; set; } // InstitutionCountry
        public int? DoctoralLevelId { get; set; } // DoctoralLevelId
        public string DoctoralLevel { get; set; } // DoctoralLevel
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public string LastUpdatedByName { get; set; } // LastUpdatedByName
        public int DepartmentId { get; set; } // DepartmentId
        public string DeptIdentifier { get; set; } // DeptIdentifier
        public string DeptDisplayName { get; set; } // DeptDisplayName
        public string DeptHrName { get; set; } // DeptHRName
    }

}

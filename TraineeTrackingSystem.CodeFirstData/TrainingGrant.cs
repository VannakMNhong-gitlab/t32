// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // TrainingGrant
    public class TrainingGrant
    {
        public Guid Id { get; set; } // Id (Primary key)
        public Guid FundingId { get; set; } // FundingId
        public int? DoctoralLevelId { get; set; } // DoctoralLevelId
        public int? NumPredocPositionsRequested { get; set; } // NumPredocPositionsRequested
        public int? NumPostdocPositionsRequested { get; set; } // NumPostdocPositionsRequested
        public int? NumPredocPositionsAwarded { get; set; } // NumPredocPositionsAwarded
        public int? NumPostdocPositionsAwarded { get; set; } // NumPostdocPositionsAwarded
        public int? PredocSupportMonthsAwarded { get; set; } // PredocSupportMonthsAwarded
        public int? PostdocSupportMonthsAwarded { get; set; } // PostdocSupportMonthsAwarded
        public Guid? PreviousTrainingGrantId { get; set; } // PreviousTrainingGrantId
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public bool IsDeleted { get; set; } // IsDeleted
        public int? ApplicantPoolAcademicYear { get; set; } // ApplicantPoolAcademicYear
        public int TrainingGrantStatusId { get; set; } // TrainingGrantStatusId

        // Reverse navigation
        public virtual ICollection<Applicant> Applicants { get; set; } // Many to many mapping
        public virtual ICollection<FacultyResearchInterest> FacultyResearchInterests { get; set; } // Many to many mapping
        public virtual ICollection<Organization> Organizations { get; set; } // Many to many mapping
        public virtual ICollection<Program> Programs { get; set; } // Many to many mapping
        public virtual ICollection<TraineeSummaryInfo> TraineeSummaryInfoes { get; set; } // TraineeSummaryInfo.FK_TraineeSummaryInfo_TrainingGrant
        public virtual ICollection<TrainingGrant> TrainingGrants { get; set; } // TrainingGrant.FK_TrainingGrant_PreviousTrainingGrant
        public virtual ICollection<TrainingGrantSupportGrant> TrainingGrantSupportGrants { get; set; } // Many to many mapping
        public virtual ICollection<TrainingGrantTrainee> TrainingGrantTrainees { get; set; } // TrainingGrantTrainee.FK_TrainingGrantTrainee_TrainingGrant

        // Foreign keys
        public virtual DoctoralLevel DoctoralLevel { get; set; } // FK_TrainingGrant_DoctoralLevel
        public virtual Funding Funding { get; set; } // FK_TrainingGrant_Funding
        public virtual TrainingGrant TrainingGrant_PreviousTrainingGrantId { get; set; } // FK_TrainingGrant_PreviousTrainingGrant
        public virtual TrainingGrantStatu TrainingGrantStatu { get; set; } // FK_TrainingGrant_TrainingGrantStatus

        public TrainingGrant()
        {
            Id = System.Guid.NewGuid();
            IsDeleted = false;
            TrainingGrantStatusId = 1;
            TraineeSummaryInfoes = new List<TraineeSummaryInfo>();
            TrainingGrants = new List<TrainingGrant>();
            TrainingGrantSupportGrants = new List<TrainingGrantSupportGrant>();
            TrainingGrantTrainees = new List<TrainingGrantTrainee>();
            Applicants = new List<Applicant>();
            Programs = new List<Program>();
            Organizations = new List<Organization>();
            FacultyResearchInterests = new List<FacultyResearchInterest>();
        }
    }

}

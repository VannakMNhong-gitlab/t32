// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Funding
    internal class FundingConfiguration : EntityTypeConfiguration<Funding>
    {
        public FundingConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".Funding");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.SponsorAwardNumber).HasColumnName("SponsorAwardNumber").IsOptional().HasMaxLength(50);
            Property(x => x.Title).HasColumnName("Title").IsRequired();
            Property(x => x.FundingStatusId).HasColumnName("FundingStatusId").IsRequired();
            Property(x => x.SponsorId).HasColumnName("SponsorId").IsOptional();
            Property(x => x.SponsorReferenceNumber).HasColumnName("SponsorReferenceNumber").IsOptional().HasMaxLength(150);
            Property(x => x.GrtNumber).HasColumnName("GRTNumber").IsOptional().HasMaxLength(20);
            Property(x => x.DateStarted).HasColumnName("DateStarted").IsOptional();
            Property(x => x.DateEnded).HasColumnName("DateEnded").IsOptional();
            Property(x => x.FundingTypeId).HasColumnName("FundingTypeId").IsOptional();
            Property(x => x.IsRenewal).HasColumnName("IsRenewal").IsRequired();
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
            Property(x => x.PrimeAward).HasColumnName("PrimeAward").IsOptional().HasMaxLength(250);
            Property(x => x.DisplayId).HasColumnName("DisplayId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.DateProjectedStart).HasColumnName("DateProjectedStart").IsOptional();
            Property(x => x.DateProjectedEnd).HasColumnName("DateProjectedEnd").IsOptional();
            Property(x => x.ProgramProjectPdId).HasColumnName("ProgramProjectPDId").IsOptional();
            Property(x => x.PrimeSponsorId).HasColumnName("PrimeSponsorId").IsOptional();
            Property(x => x.PrimeAwardPiId).HasColumnName("PrimeAwardPIId").IsOptional();

            // Foreign keys
            HasRequired(a => a.FundingStatu).WithMany(b => b.Fundings).HasForeignKey(c => c.FundingStatusId); // FK_Funding_FundingStatus
            HasOptional(a => a.Institution_SponsorId).WithMany(b => b.Fundings_SponsorId).HasForeignKey(c => c.SponsorId); // FK_Funding_Sponsor
            HasOptional(a => a.FundingType).WithMany(b => b.Fundings).HasForeignKey(c => c.FundingTypeId); // FK_Funding_FundingType
            HasOptional(a => a.Faculty_ProgramProjectPdId).WithMany(b => b.Fundings_ProgramProjectPdId).HasForeignKey(c => c.ProgramProjectPdId); // FK_Funding_FacultyProgPD
            HasOptional(a => a.Institution_PrimeSponsorId).WithMany(b => b.Fundings_PrimeSponsorId).HasForeignKey(c => c.PrimeSponsorId); // FK_Funding_InstitutionPrimeSponsor
            HasOptional(a => a.Faculty_PrimeAwardPiId).WithMany(b => b.Fundings_PrimeAwardPiId).HasForeignKey(c => c.PrimeAwardPiId); // FK_Funding_FacultyPrimeAwardPI
        }
    }

}

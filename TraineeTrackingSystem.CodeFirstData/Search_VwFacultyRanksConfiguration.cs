// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FacultyRanks
    internal class Search_VwFacultyRanksConfiguration : EntityTypeConfiguration<Search_VwFacultyRanks>
    {
        public Search_VwFacultyRanksConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_FacultyRanks");
            HasKey(x => new { x.FacultyRankId, x.Rank });

            Property(x => x.FacultyRankId).HasColumnName("FacultyRankId").IsRequired();
            Property(x => x.Rank).HasColumnName("Rank").IsRequired().HasMaxLength(150);
            Property(x => x.TotalFacultyRankUsage).HasColumnName("TotalFacultyRankUsage").IsOptional();
        }
    }

}

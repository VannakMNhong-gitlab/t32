// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // ContactEmail
    public class ContactEmail
    {
        public int Id { get; set; } // Id (Primary key)
        public Guid PersonId { get; set; } // PersonId
        public string EmailAddress { get; set; } // EmailAddress
        public int ContactEntityTypeId { get; set; } // ContactEntityTypeId
        public bool IsPrimary { get; set; } // IsPrimary
        public bool IsDeleted { get; set; } // IsDeleted

        // Foreign keys
        public virtual ContactEntityType ContactEntityType { get; set; } // FK_ContactEmail_ContactEntityType
        public virtual Person Person { get; set; } // FK_ContactEmail_Person

        public ContactEmail()
        {
            ContactEntityTypeId = 1;
            IsPrimary = true;
            IsDeleted = false;
        }
    }

}

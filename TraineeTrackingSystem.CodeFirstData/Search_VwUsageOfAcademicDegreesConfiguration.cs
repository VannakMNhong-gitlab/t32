// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_UsageOfAcademicDegrees
    internal class Search_VwUsageOfAcademicDegreesConfiguration : EntityTypeConfiguration<Search_VwUsageOfAcademicDegrees>
    {
        public Search_VwUsageOfAcademicDegreesConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_UsageOfAcademicDegrees");
            HasKey(x => new { x.DegreeId, x.DegreeName });

            Property(x => x.DegreeId).HasColumnName("DegreeId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.DegreeName).HasColumnName("DegreeName").IsRequired().HasMaxLength(25);
            Property(x => x.TotalAcademicHistory).HasColumnName("TotalAcademicHistory").IsOptional();
            Property(x => x.TotalFacultyAcademics).HasColumnName("TotalFacultyAcademics").IsOptional();
            Property(x => x.TotalTrainingPeriods).HasColumnName("TotalTrainingPeriods").IsOptional();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // LeaveOfAbsence
    internal class LeaveOfAbsenceConfiguration : EntityTypeConfiguration<LeaveOfAbsence>
    {
        public LeaveOfAbsenceConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".LeaveOfAbsence");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.DateStarted).HasColumnName("DateStarted").IsRequired();
            Property(x => x.DateEnded).HasColumnName("DateEnded").IsOptional();
            Property(x => x.IsOfficial).HasColumnName("IsOfficial").IsRequired();
            Property(x => x.Reason).HasColumnName("Reason").IsOptional().HasMaxLength(1000);
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_TrainingPeriodMentors
    public class Search_VwTrainingPeriodMentors
    {
        public Guid TrainingPeriodId { get; set; } // TrainingPeriodId
        public Guid FacultyId { get; set; } // FacultyId
        public Guid MentorPersonId { get; set; } // MentorPersonId
        public string MentorFirstName { get; set; } // MentorFirstName
        public string MentorMiddleName { get; set; } // MentorMiddleName
        public string MentorLastName { get; set; } // MentorLastName
        public string MentorFullName { get; set; } // MentorFullName
        public string EmployeeId { get; set; } // EmployeeId
        public int? PrimaryOrganizationId { get; set; } // PrimaryOrganizationId
        public string MentorPrimaryOrganization { get; set; } // MentorPrimaryOrganization
        public int? InstitutionId { get; set; } // InstitutionId
        public string InstitutionIdentifier { get; set; } // InstitutionIdentifier
        public string InstitutionName { get; set; } // InstitutionName
        public string InstitutionCity { get; set; } // InstitutionCity
        public int? InstitutionStateId { get; set; } // InstitutionStateId
        public string InstitutionStateAbbreviation { get; set; } // InstitutionStateAbbreviation
        public string InstitutionStateFullName { get; set; } // InstitutionStateFullName
        public int? InstitutionCountryId { get; set; } // InstitutionCountryId
        public string InstitutionCountry { get; set; } // InstitutionCountry
        public int? YearStarted { get; set; } // YearStarted
        public int? YearEnded { get; set; } // YearEnded
        public string ResearchProjectTitle { get; set; } // ResearchProjectTitle
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public string LastUpdatedByName { get; set; } // LastUpdatedByName
    }

}

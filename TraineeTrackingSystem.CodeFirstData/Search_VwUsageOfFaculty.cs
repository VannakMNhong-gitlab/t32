// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_UsageOfFaculty
    public class Search_VwUsageOfFaculty
    {
        public Guid FacultyId { get; set; } // FacultyId
        public Guid PersonId { get; set; } // PersonId
        public string LastName { get; set; } // LastName
        public string FirstName { get; set; } // FirstName
        public string MiddleName { get; set; } // MiddleName
        public int? TotalFacultyAcademics { get; set; } // TotalFacultyAcademics
        public int? TotalFacultyPrograms { get; set; } // TotalFacultyPrograms
        public int? TotalFacultyResearchInterests { get; set; } // TotalFacultyResearchInterests
        public int? TotalFundings { get; set; } // TotalFundings
        public int? TotalTrainingGrants { get; set; } // TotalTrainingGrants
        public int? TotalTrainingPeriods { get; set; } // TotalTrainingPeriods
    }

}

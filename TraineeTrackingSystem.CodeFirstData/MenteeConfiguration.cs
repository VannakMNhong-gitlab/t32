// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Mentee
    internal class MenteeConfiguration : EntityTypeConfiguration<Mentee>
    {
        public MenteeConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".Mentee");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.StudentId).HasColumnName("StudentId").IsRequired().HasMaxLength(50);
            Property(x => x.InstitutionAssociationId).HasColumnName("InstitutionAssociationId").IsOptional();
            Property(x => x.DepartmentId).HasColumnName("DepartmentId").IsOptional();
            Property(x => x.IsTrainingGrantEligible).HasColumnName("IsTrainingGrantEligible").IsRequired();
            Property(x => x.WasRecruitedToLab).HasColumnName("WasRecruitedToLab").IsRequired();
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.DateCreated).HasColumnName("DateCreated").IsOptional();
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional().HasMaxLength(50);

            // Foreign keys
            HasRequired(a => a.Person).WithMany(b => b.Mentees).HasForeignKey(c => c.PersonId); // FK_Mentee_Person
            HasOptional(a => a.InstitutionAssociation).WithMany(b => b.Mentees).HasForeignKey(c => c.InstitutionAssociationId); // FK_Mentee_InstitutionAssociation
            HasOptional(a => a.Organization).WithMany(b => b.Mentees).HasForeignKey(c => c.DepartmentId); // FK_Mentee_Organization
        }
    }

}

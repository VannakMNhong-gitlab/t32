// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // TrainingPeriod
    internal class TrainingPeriodConfiguration : EntityTypeConfiguration<TrainingPeriod>
    {
        public TrainingPeriodConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".TrainingPeriod");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired();
            Property(x => x.InstitutionId).HasColumnName("InstitutionId").IsOptional();
            Property(x => x.DoctoralLevelId).HasColumnName("DoctoralLevelId").IsRequired();
            Property(x => x.YearStarted).HasColumnName("YearStarted").IsOptional();
            Property(x => x.YearEnded).HasColumnName("YearEnded").IsOptional();
            Property(x => x.AcademicDegreeId).HasColumnName("AcademicDegreeId").IsOptional();
            Property(x => x.ResearchProjectTitle).HasColumnName("ResearchProjectTitle").IsOptional().HasMaxLength(250);
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.IsDeleted).HasColumnName("IsDeleted").IsRequired();
            Property(x => x.DegreeSoughtId).HasColumnName("DegreeSoughtId").IsOptional();

            // Foreign keys
            HasRequired(a => a.Person).WithMany(b => b.TrainingPeriods).HasForeignKey(c => c.PersonId); // FK_TrainingPeriod_Person
            HasOptional(a => a.Institution).WithMany(b => b.TrainingPeriods).HasForeignKey(c => c.InstitutionId); // FK_TrainingPeriod_Institution
            HasRequired(a => a.DoctoralLevel).WithMany(b => b.TrainingPeriods).HasForeignKey(c => c.DoctoralLevelId); // FK_TrainingPeriod_DoctoralLevel
            HasOptional(a => a.AcademicDegree_AcademicDegreeId).WithMany(b => b.TrainingPeriods_AcademicDegreeId).HasForeignKey(c => c.AcademicDegreeId); // FK_TrainingPeriod_AcademicDegree
            HasOptional(a => a.AcademicDegree_DegreeSoughtId).WithMany(b => b.TrainingPeriods_DegreeSoughtId).HasForeignKey(c => c.DegreeSoughtId); // FK_TrainingPeriod_DegreeSought
        }
    }

}

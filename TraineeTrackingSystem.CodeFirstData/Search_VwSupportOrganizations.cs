// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_SupportOrganizations
    public class Search_VwSupportOrganizations
    {
        public int Id { get; set; } // Id
        public string Name { get; set; } // Name
        public string Abbreviation { get; set; } // Abbreviation
        public string FullName { get; set; } // FullName
        public int? SortOrder { get; set; } // SortOrder
    }

}

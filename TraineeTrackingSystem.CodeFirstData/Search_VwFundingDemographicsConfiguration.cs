// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FundingDemographics
    internal class Search_VwFundingDemographicsConfiguration : EntityTypeConfiguration<Search_VwFundingDemographics>
    {
        public Search_VwFundingDemographicsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_FundingDemographics");
            HasKey(x => new { x.FundingStatusId, x.Title, x.IsRenewal, x.PiFacultyId, x.PiPersonId, x.Id });

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.PiFacultyId).HasColumnName("PIFacultyId").IsRequired();
            Property(x => x.PiPersonId).HasColumnName("PIPersonId").IsRequired();
            Property(x => x.SponsorAwardNumber).HasColumnName("SponsorAwardNumber").IsOptional().HasMaxLength(50);
            Property(x => x.Title).HasColumnName("Title").IsRequired();
            Property(x => x.FundingStatusId).HasColumnName("FundingStatusId").IsRequired();
            Property(x => x.FundingStatus).HasColumnName("FundingStatus").IsOptional().HasMaxLength(150);
            Property(x => x.DateProjectedStart).HasColumnName("DateProjectedStart").IsOptional();
            Property(x => x.DateProjectedEnd).HasColumnName("DateProjectedEnd").IsOptional();
            Property(x => x.SponsorId).HasColumnName("SponsorId").IsOptional();
            Property(x => x.SponsorName).HasColumnName("SponsorName").IsOptional().HasMaxLength(250);
            Property(x => x.SponsorReferenceNumber).HasColumnName("SponsorReferenceNumber").IsOptional().HasMaxLength(150);
            Property(x => x.GrtNumber).HasColumnName("GRTNumber").IsOptional().HasMaxLength(20);
            Property(x => x.DateTimeFundingStarted).HasColumnName("DateTimeFundingStarted").IsOptional();
            Property(x => x.DateTimeFundingEnded).HasColumnName("DateTimeFundingEnded").IsOptional();
            Property(x => x.FundingTypeId).HasColumnName("FundingTypeId").IsOptional();
            Property(x => x.FundingType).HasColumnName("FundingType").IsOptional().HasMaxLength(150);
            Property(x => x.IsRenewal).HasColumnName("IsRenewal").IsRequired();
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.LastUpdatedByName).HasColumnName("LastUpdatedByName").IsOptional().HasMaxLength(129);
            Property(x => x.PiLastName).HasColumnName("PILastName").IsOptional().HasMaxLength(75);
            Property(x => x.PiFirstName).HasColumnName("PIFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.PiFullName).HasColumnName("PIFullName").IsOptional().HasMaxLength(178);
            Property(x => x.ProgramProjectPdId).HasColumnName("ProgramProjectPDId").IsOptional();
            Property(x => x.ProgramProjectPdLastName).HasColumnName("ProgramProjectPDLastName").IsOptional().HasMaxLength(75);
            Property(x => x.ProgramProjectPdFirstName).HasColumnName("ProgramProjectPDFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.ProgramProjectPdFullName).HasColumnName("ProgramProjectPDFullName").IsOptional().HasMaxLength(178);
            Property(x => x.PrimeAwardNumber).HasColumnName("PrimeAwardNumber").IsOptional().HasMaxLength(250);
            Property(x => x.PrimeSponsorId).HasColumnName("PrimeSponsorId").IsOptional();
            Property(x => x.PrimeSponsorName).HasColumnName("PrimeSponsorName").IsOptional().HasMaxLength(250);
            Property(x => x.PrimeAwardPiId).HasColumnName("PrimeAwardPIId").IsOptional();
            Property(x => x.PrimeAwardPiLastName).HasColumnName("PrimeAwardPILastName").IsOptional().HasMaxLength(75);
            Property(x => x.PrimeAwardPiFirstName).HasColumnName("PrimeAwardPIFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.PrimeAwardPiFullName).HasColumnName("PrimeAwardPIFullName").IsOptional().HasMaxLength(178);
            Property(x => x.DisplayStartDate).HasColumnName("DisplayStartDate").IsOptional();
            Property(x => x.DisplayEndDate).HasColumnName("DisplayEndDate").IsOptional();
        }
    }

}

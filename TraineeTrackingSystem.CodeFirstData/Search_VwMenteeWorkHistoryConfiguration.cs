// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_MenteeWorkHistory
    internal class Search_VwMenteeWorkHistoryConfiguration : EntityTypeConfiguration<Search_VwMenteeWorkHistory>
    {
        public Search_VwMenteeWorkHistoryConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_MenteeWorkHistory");
            HasKey(x => new { x.DisplayId, x.IsTrainingGrantEligible, x.MenteePersonId, x.MenteeId, x.StudentId, x.WasRecruitedToLab });

            Property(x => x.MenteeId).HasColumnName("MenteeId").IsRequired();
            Property(x => x.MenteePersonId).HasColumnName("MenteePersonId").IsRequired();
            Property(x => x.FirstName).HasColumnName("FirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MiddleName).HasColumnName("MiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.LastName).HasColumnName("LastName").IsOptional().HasMaxLength(75);
            Property(x => x.FullName).HasColumnName("FullName").IsOptional().HasMaxLength(178);
            Property(x => x.DisplayId).HasColumnName("DisplayId").IsRequired();
            Property(x => x.StudentId).HasColumnName("StudentId").IsRequired().HasMaxLength(50);
            Property(x => x.IsTrainingGrantEligible).HasColumnName("IsTrainingGrantEligible").IsRequired();
            Property(x => x.WasRecruitedToLab).HasColumnName("WasRecruitedToLab").IsRequired();
            Property(x => x.Department).HasColumnName("Department").IsOptional().HasMaxLength(250);
            Property(x => x.InstitutionAssociation).HasColumnName("InstitutionAssociation").IsOptional().HasMaxLength(50);
            Property(x => x.MenteeDateLastUpdated).HasColumnName("MenteeDateLastUpdated").IsOptional();
            Property(x => x.MenteeLastUpdatedBy).HasColumnName("MenteeLastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.MenteeLastUpdatedByName).HasColumnName("MenteeLastUpdatedByName").IsOptional().HasMaxLength(129);
            Property(x => x.WorkHistoryId).HasColumnName("WorkHistoryId").IsOptional();
            Property(x => x.InstitutionId).HasColumnName("InstitutionId").IsOptional();
            Property(x => x.Institution).HasColumnName("Institution").IsOptional().HasMaxLength(250);
            Property(x => x.DateStartedPosition).HasColumnName("DateStartedPosition").IsOptional();
            Property(x => x.DateEndedPosition).HasColumnName("DateEndedPosition").IsOptional();
            Property(x => x.PositionTitle).HasColumnName("PositionTitle").IsOptional().HasMaxLength(200);
            Property(x => x.PositionLocation).HasColumnName("PositionLocation").IsOptional().HasMaxLength(200);
            Property(x => x.WorkHistDateLastUpdated).HasColumnName("WorkHistDateLastUpdated").IsOptional();
            Property(x => x.WorkHistLastUpdatedBy).HasColumnName("WorkHistLastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.WorkHistLastUpdatedByName).HasColumnName("WorkHistLastUpdatedByName").IsOptional().HasMaxLength(129);
        }
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_Institutions
    internal class Search_VwInstitutionsConfiguration : EntityTypeConfiguration<Search_VwInstitutions>
    {
        public Search_VwInstitutionsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_Institutions");
            HasKey(x => new { x.Name, x.InstitutionTypeId, x.Id });

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.Name).HasColumnName("Name").IsRequired().HasMaxLength(250);
            Property(x => x.InstitutionIdentifier).HasColumnName("InstitutionIdentifier").IsOptional().HasMaxLength(150);
            Property(x => x.City).HasColumnName("City").IsOptional().HasMaxLength(150);
            Property(x => x.StateProvinceId).HasColumnName("StateProvinceId").IsOptional();
            Property(x => x.StateProvinceAbbreviation).HasColumnName("StateProvinceAbbreviation").IsOptional().HasMaxLength(50);
            Property(x => x.StateProvinceFullName).HasColumnName("StateProvinceFullName").IsOptional().HasMaxLength(50);
            Property(x => x.CountryId).HasColumnName("CountryId").IsOptional();
            Property(x => x.InstitutionTypeId).HasColumnName("InstitutionTypeId").IsRequired();
            Property(x => x.InstitutionType).HasColumnName("InstitutionType").IsOptional().HasMaxLength(150);
            Property(x => x.Country).HasColumnName("Country").IsOptional().HasMaxLength(150);
            Property(x => x.TotalInstitutionUsage).HasColumnName("TotalInstitutionUsage").IsOptional();
        }
    }

}

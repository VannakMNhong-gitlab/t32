// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // PersonRoles
    public class PersonRole
    {
        public int LoginId { get; set; } // LoginId (Primary key)
        public int RoleId { get; set; } // RoleId (Primary key)
        public bool IsActive { get; set; } // IsActive
        public bool IsDeleted { get; set; } // IsDeleted
        public DateTime DateAssigned { get; set; } // DateAssigned
        public string AssignedBy { get; set; } // AssignedBy
        public DateTime? DateRemoved { get; set; } // DateRemoved
        public string RemovedBy { get; set; } // RemovedBy
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy

        // Foreign keys
        public virtual Login Login { get; set; } // FK_PersonRoles_Logins
        public virtual Role Role { get; set; } // FK_PersonRoles_Roles

        public PersonRole()
        {
            IsActive = true;
            IsDeleted = false;
        }
    }

}

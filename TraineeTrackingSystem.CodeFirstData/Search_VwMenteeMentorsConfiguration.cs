// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_MenteeMentors
    internal class Search_VwMenteeMentorsConfiguration : EntityTypeConfiguration<Search_VwMenteeMentors>
    {
        public Search_VwMenteeMentorsConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_MenteeMentors");
            HasKey(x => new { x.MenteePersonId, x.TrainingPeriodId });

            Property(x => x.TrainingPeriodId).HasColumnName("TrainingPeriodId").IsRequired();
            Property(x => x.MenteePersonId).HasColumnName("MenteePersonId").IsRequired();
            Property(x => x.MenteeId).HasColumnName("MenteeId").IsOptional();
            Property(x => x.MenteeStudentId).HasColumnName("MenteeStudentId").IsOptional().HasMaxLength(50);
            Property(x => x.FirstName).HasColumnName("FirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MiddleName).HasColumnName("MiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.LastName).HasColumnName("LastName").IsOptional().HasMaxLength(75);
            Property(x => x.MenteeFullName).HasColumnName("MenteeFullName").IsOptional().HasMaxLength(178);
            Property(x => x.MenteeType).HasColumnName("MenteeType").IsOptional().HasMaxLength(150);
            Property(x => x.MentorPersonId).HasColumnName("MentorPersonId").IsOptional();
            Property(x => x.FacultyId).HasColumnName("FacultyId").IsOptional();
            Property(x => x.MentorFirstName).HasColumnName("MentorFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.MentorMiddleName).HasColumnName("MentorMiddleName").IsOptional().HasMaxLength(50);
            Property(x => x.MentorLastName).HasColumnName("MentorLastName").IsOptional().HasMaxLength(75);
            Property(x => x.MentorFullName).HasColumnName("MentorFullName").IsOptional().HasMaxLength(178);
            Property(x => x.MentorEmployeeId).HasColumnName("MentorEmployeeId").IsOptional().HasMaxLength(50);
            Property(x => x.MentorPrimaryOrganizationId).HasColumnName("MentorPrimaryOrganizationId").IsOptional();
            Property(x => x.MentorPrimaryOrganization).HasColumnName("MentorPrimaryOrganization").IsOptional().HasMaxLength(250);
            Property(x => x.InstitutionId).HasColumnName("InstitutionId").IsOptional();
            Property(x => x.InstitutionIdentifier).HasColumnName("InstitutionIdentifier").IsOptional().HasMaxLength(150);
            Property(x => x.InstitutionName).HasColumnName("InstitutionName").IsOptional().HasMaxLength(250);
            Property(x => x.InstitutionCity).HasColumnName("InstitutionCity").IsOptional().HasMaxLength(150);
            Property(x => x.InstitutionStateId).HasColumnName("InstitutionStateId").IsOptional();
            Property(x => x.InstitutionStateAbbreviation).HasColumnName("InstitutionStateAbbreviation").IsOptional().HasMaxLength(50);
            Property(x => x.InstitutionStateFullName).HasColumnName("InstitutionStateFullName").IsOptional().HasMaxLength(50);
            Property(x => x.InstitutionCountryId).HasColumnName("InstitutionCountryId").IsOptional();
            Property(x => x.InstitutionCountry).HasColumnName("InstitutionCountry").IsOptional().HasMaxLength(150);
            Property(x => x.YearStarted).HasColumnName("YearStarted").IsOptional();
            Property(x => x.YearEnded).HasColumnName("YearEnded").IsOptional();
            Property(x => x.CompletedDegreeId).HasColumnName("CompletedDegreeId").IsOptional();
            Property(x => x.CompletedDegree).HasColumnName("CompletedDegree").IsOptional().HasMaxLength(25);
            Property(x => x.DegreeSoughtId).HasColumnName("DegreeSoughtId").IsOptional();
            Property(x => x.DegreeSought).HasColumnName("DegreeSought").IsOptional().HasMaxLength(25);
            Property(x => x.ResearchProjectTitle).HasColumnName("ResearchProjectTitle").IsOptional().HasMaxLength(250);
            Property(x => x.DateLastUpdated).HasColumnName("DateLastUpdated").IsOptional();
            Property(x => x.LastUpdatedBy).HasColumnName("LastUpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.LastUpdatedByName).HasColumnName("LastUpdatedByName").IsOptional().HasMaxLength(129);
        }
    }

}

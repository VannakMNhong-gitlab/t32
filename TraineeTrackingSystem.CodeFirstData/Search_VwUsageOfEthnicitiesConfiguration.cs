// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_UsageOfEthnicities
    internal class Search_VwUsageOfEthnicitiesConfiguration : EntityTypeConfiguration<Search_VwUsageOfEthnicities>
    {
        public Search_VwUsageOfEthnicitiesConfiguration(string schema = "Search")
        {
            ToTable(schema + ".vw_UsageOfEthnicities");
            HasKey(x => new { x.EthnicityId, x.Ethnicity });

            Property(x => x.EthnicityId).HasColumnName("EthnicityId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Ethnicity).HasColumnName("Ethnicity").IsRequired().HasMaxLength(300);
            Property(x => x.TotalEthnicities).HasColumnName("TotalEthnicities").IsOptional();
        }
    }

}

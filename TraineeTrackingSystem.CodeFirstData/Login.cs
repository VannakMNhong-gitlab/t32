// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // Logins
    public class Login
    {
        public int LoginId { get; set; } // LoginId (Primary key)
        public string ExternalId { get; set; } // ExternalId
        public Guid? PersonId { get; set; } // PersonId
        public bool IsActive { get; set; } // IsActive
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public DateTime? DateCreated { get; set; } // DateCreated
        public string CreatedBy { get; set; } // CreatedBy
        public string Username { get; set; } // Username

        // Reverse navigation
        public virtual ICollection<PersonRole> PersonRoles { get; set; } // Many to many mapping

        // Foreign keys
        public virtual Person Person { get; set; } // FK_Logins_Person

        public Login()
        {
            IsActive = true;
            DateCreated = System.DateTime.Now;
            PersonRoles = new List<PersonRole>();
        }
    }

}

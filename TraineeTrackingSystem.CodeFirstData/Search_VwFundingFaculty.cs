// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_FundingFaculty
    public class Search_VwFundingFaculty
    {
        public Guid Id { get; set; } // Id
        public string Title { get; set; } // Title
        public int FundingStatusId { get; set; } // FundingStatusId
        public string FundingStatus { get; set; } // FundingStatus
        public DateTime? DateProjectedStart { get; set; } // DateProjectedStart
        public DateTime? DateProjectedEnd { get; set; } // DateProjectedEnd
        public DateTime? DateTimeFundingStarted { get; set; } // DateTimeFundingStarted
        public DateTime? DateTimeFundingEnded { get; set; } // DateTimeFundingEnded
        public int? FundingTypeId { get; set; } // FundingTypeId
        public string FundingType { get; set; } // FundingType
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public Guid FacultyId { get; set; } // FacultyId
        public Guid FacultyPersonId { get; set; } // FacultyPersonId
        public string FacultyLastName { get; set; } // FacultyLastName
        public string FacultyFirstName { get; set; } // FacultyFirstName
        public string FacultyFullName { get; set; } // FacultyFullName
        public int FacultyRoleId { get; set; } // FacultyRoleId
        public string FacultyRole { get; set; } // FacultyRole
        public int? SponsorId { get; set; } // SponsorId
        public string SponsorName { get; set; } // SponsorName
        public string SponsorAwardNumber { get; set; } // SponsorAwardNumber
        public DateTime? FundingFacultyDateLastUpdated { get; set; } // FundingFacultyDateLastUpdated
        public string FundingFacultyLastUpdatedBy { get; set; } // FundingFacultyLastUpdatedBy
        public string FundingFacultyLastUpdatedByName { get; set; } // FundingFacultyLastUpdatedByName
    }

}

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;
//using System.ComponentModel.DataAnnotations.Schema;

namespace TraineeTrackingSystem.Data
{
    // vw_T32Table3
    internal class Reports_VwT32Table3Configuration : EntityTypeConfiguration<Reports_VwT32Table3>
    {
        public Reports_VwT32Table3Configuration(string schema = "Reports")
        {
            ToTable(schema + ".vw_T32Table3");
            HasKey(x => new { x.FacultyPersonId, x.SupportedTgFacultyId, x.FacultyId, x.SelectedTrainingGrantId, x.SupportedTgFacultyPersonId });

            Property(x => x.FacultyId).HasColumnName("FacultyId").IsRequired();
            Property(x => x.FacultyPersonId).HasColumnName("FacultyPersonId").IsRequired();
            Property(x => x.FacultyLastName).HasColumnName("FacultyLastName").IsOptional().HasMaxLength(75);
            Property(x => x.FacultyFirstName).HasColumnName("FacultyFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.FacultyFullName).HasColumnName("FacultyFullName").IsOptional().HasMaxLength(178);
            Property(x => x.FacultyFullNameAbbrev).HasColumnName("FacultyFullNameAbbrev").IsOptional().HasMaxLength(79);
            Property(x => x.SelectedTrainingGrantId).HasColumnName("SelectedTrainingGrantId").IsRequired();
            Property(x => x.SupportedTrainingGrantId).HasColumnName("SupportedTrainingGrantId").IsOptional();
            Property(x => x.SupportedTgFundingId).HasColumnName("SupportedTGFundingId").IsOptional();
            Property(x => x.SupportedTgTitle).HasColumnName("SupportedTGTitle").IsOptional();
            Property(x => x.SupportedTgFacultyId).HasColumnName("SupportedTGFacultyId").IsRequired();
            Property(x => x.SupportedTgFacultyPersonId).HasColumnName("SupportedTGFacultyPersonId").IsRequired();
            Property(x => x.SupportedTgFacultyLastName).HasColumnName("SupportedTGFacultyLastName").IsOptional().HasMaxLength(75);
            Property(x => x.SupportedTgFacultyFirstName).HasColumnName("SupportedTGFacultyFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.SupportedTgFacultyFullName).HasColumnName("SupportedTGFacultyFullName").IsOptional().HasMaxLength(178);
            Property(x => x.SupportedTgFacultyFullNameAbbrev).HasColumnName("SupportedTGFacultyFullNameAbbrev").IsOptional().HasMaxLength(79);
            Property(x => x.SupportedTgStatusId).HasColumnName("SupportedTGStatusId").IsOptional();
            Property(x => x.SupportedTgStatusName).HasColumnName("SupportedTGStatusName").IsOptional().HasMaxLength(150);
            Property(x => x.SupportedTgSponsorId).HasColumnName("SupportedTGSponsorId").IsOptional();
            Property(x => x.SupportedTgSponsorName).HasColumnName("SupportedTGSponsorName").IsOptional().HasMaxLength(250);
            Property(x => x.SupportedTgSponsorAwardNumber).HasColumnName("SupportedTGSponsorAwardNumber").IsOptional().HasMaxLength(50);
            Property(x => x.SupportedDateProjectedStart).HasColumnName("SupportedDateProjectedStart").IsOptional();
            Property(x => x.SupportedDateProjectedEnd).HasColumnName("SupportedDateProjectedEnd").IsOptional();
            Property(x => x.SupportedTgDateStarted).HasColumnName("SupportedTGDateStarted").IsOptional();
            Property(x => x.SupportedTgDateEnded).HasColumnName("SupportedTGDateEnded").IsOptional();
            Property(x => x.SupportedTggrtNumber).HasColumnName("SupportedTGGRTNumber").IsOptional().HasMaxLength(20);
            Property(x => x.SupportedTgPrimeAward).HasColumnName("SupportedTGPrimeAward").IsOptional().HasMaxLength(250);
            Property(x => x.PdFacultyId).HasColumnName("PdFacultyId").IsOptional();
            Property(x => x.PdPersonId).HasColumnName("PdPersonId").IsOptional();
            Property(x => x.PdLastName).HasColumnName("PdLastName").IsOptional().HasMaxLength(75);
            Property(x => x.PdFirstName).HasColumnName("PdFirstName").IsOptional().HasMaxLength(50);
            Property(x => x.PdFullName).HasColumnName("PdFullName").IsOptional().HasMaxLength(178);
            Property(x => x.PdNameAbbrev).HasColumnName("PDNameAbbrev").IsOptional().HasMaxLength(79);
            Property(x => x.PdPrimaryOrgId).HasColumnName("PDPrimaryOrgId").IsOptional();
            Property(x => x.PdPrimaryOrg).HasColumnName("PDPrimaryOrg").IsOptional().HasMaxLength(250);
            Property(x => x.TotalFaculty).HasColumnName("TotalFaculty").IsOptional();
            Property(x => x.NumPredocPositionsRequested).HasColumnName("NumPredocPositionsRequested").IsOptional();
            Property(x => x.NumPostdocPositionsRequested).HasColumnName("NumPostdocPositionsRequested").IsOptional();
            Property(x => x.NumPreDocTraineesAppointed).HasColumnName("NumPreDocTraineesAppointed").IsOptional();
            Property(x => x.NumPostDocTraineesAppointed).HasColumnName("NumPostDocTraineesAppointed").IsOptional();
        }
    }

}

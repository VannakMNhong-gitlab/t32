﻿using System;
using System.Security.Cryptography;
#region Assembly MVC5FrameworkAD.CommonUtility.dll, v1.0.0.0
// C:\Projects\ECA\Clinical\Ambulatory Skills\Source\AmbulatorySkills\AmbulatorySkills\References\MVC5FrameworkAD.CommonUtility.dll
#endregion



namespace TraineeTrackingSystem.Common
{
    public static class Security
    {
        //public static bool CheckTokenAndLanId(string token, string ipAddress, out string lanId);
        private static readonly RSACryptoServiceProvider rsaProvider;
        private const string rsaXml = @"<RSAKeyValue><Modulus>uNHjcV1k8CyhAhkqL0m9WvAaYB3XH7GH8tF1/cFeVICgKBj/I3RKtbl3RDOaRSXd5ilrSPC/VOkHuKP0JaYhIAUwk5lO3Nh10DOaRgX8XdY4HO8G49+AUOnVvK/3HXKdrNGSNF7jdUdI5pK54ioLpkXps0V1giM0TxX1KjEr9V0=</Modulus><Exponent>AQAB</Exponent><P>7zrtXqYRZudsw2a+9K9Bjr/TlMZrXy+c18RB7VUPXURvFPCHTEeneGfMBlTW+WDXSzPvAyuHeTFvBOqAIRrz4Q==</P><Q>xcaMZmwxH6aveTI51qc5SR+d9hSvTnMgxAWqTdoa8NIKM68W0raeA4+mvRFKSOndr2mQgZ8WVDMVBqBYMabw/Q==</Q><DP>IPvy9nXXOGT4Lgjgk0QGufPg7EZfT4R1D5rTrDFqKOXimvR+8xl+Ea/eBZF1AnVDue6zKbC6wQ77y6W5fNcGgQ==</DP><DQ>QIak7A3EgTUa7wnKARSSBh+Ao/bfs1KwkAPhKoFMZOHCXlZztc/LAhJuLiVj/SIAJ+jjMsc6u8HmzECoRZrX6Q==</DQ><InverseQ>JicnHAWtaYdQYLHcR1rv8rdBGqEsMZOQSA9nrnBgtLUaNKNAJ9r6RNO7h4OcSn2zWJXRdKj62S8RGEdeZsoNbQ==</InverseQ><D>q6IW4sdFnpkDaMxv1XxYQaQqCkqL5Z3bT07fkSjK7hSJJtOX1JgBbWNv88EsKbzxpfWe8RzJwhOqHPj9GR1/MgRqyUTEhtc7InBnIA4v0W48b+XVl8EcMNjQteRNb2njuKMcDRYHQ2xAFxlZgRJ3pl0zxffNe8CI+NiSRbcTroE=</D></RSAKeyValue>";

        static Security()
        {
            rsaProvider = new RSACryptoServiceProvider();
            rsaProvider.FromXmlString(rsaXml);
        }

        public static string GetUserLoginName(string pUserName)
        {
            string userLogin = pUserName;

            if (pUserName.StartsWith("OSUMC", StringComparison.CurrentCultureIgnoreCase)) userLogin = pUserName.Remove(0, 6);

            return userLogin;
        }

        public static bool IsEvFormAdmin(System.Security.Principal.IPrincipal currentUser)
        {
            //TODO: remove this when in production.
            return currentUser.IsInRole("evForm.Admin");
            //return true;
        }

        public static string CreateToken(string lanId, string ipAddress)
        {
            return Encrypt(lanId + ";" + ipAddress + ";" + DateTime.Now.ToShortDateString());
        }


        public static bool CheckToken(string token, string ipAddress)
        {
            string lanId = string.Empty;

            return CheckTokenAndLanId(token, ipAddress, out lanId);
        }
        //public static bool CheckTokenAndLanId(string token, string ipAddress, out string lanId);
        public static bool CheckTokenAndLanId(string token, string ipAddress, out string lanId)
        {
            lanId = string.Empty;

            if (String.IsNullOrEmpty(token))
            {
                return false;
            }

            string decrypted = Decrypt(token);

            if (decrypted != null)
            {
                //Split lanId and IP address
                string[] values = decrypted.Split(new char[] { ';' });
                lanId = values[0];
                string ip = values[1];
                string datestring = values[2];

                //Check on both IP and date

                return ((String.Compare(values[1], ipAddress, true) == 0)
                         &&
                         (DateTime.Now.ToShortDateString() == datestring)
                       );
            }
            else
            {
                return false;
            }
        }


        public static string Decrypt(string data)
        {
            try
            {
                //from the key in the container.
                byte[] dataByte = Convert.FromBase64String(data);
                byte[] decryptedByte = rsaProvider.Decrypt(dataByte, false);
                return System.Text.Encoding.Unicode.GetString(decryptedByte);
            }
            catch (Exception e)
            {
                //return null;
                return e.Message;
            }
        }

        public static string Encrypt(string data)
        {
            try
            {
                //from the key in the container.
                byte[] dataByte = System.Text.Encoding.Unicode.GetBytes(data);

                byte[] encryptedByte = rsaProvider.Encrypt(dataByte, false);

                return Convert.ToBase64String(encryptedByte); //For passing to another page //Decrypt
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}

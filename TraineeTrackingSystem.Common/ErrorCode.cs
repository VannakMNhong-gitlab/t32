﻿namespace TraineeTrackingSystem.Common
{
    public enum ErrorCode
    {
        Success = 0,
        InsertFailure,
        UpdateFailure,
        DeleteFailure,
        InvalidParameters,
        AccessFailure,
        DeniedAccess,
        NotAllowed,
        NotFound,
        NotUnique,
        MultipleRecordsFound,
        RecordExisits,
        RecordApproved
    }
    public class ErrorMessage
    {
        public string ErrorCode { get; set; }

        public string ReturnId { get; set; }

        public string Message { get; set; }


        public ErrorMessage(string errorCode, string pMessage, string pId)
        {
            this.ErrorCode = errorCode;
            this.Message = pMessage;
            this.ReturnId = pId;
        }
    }
}
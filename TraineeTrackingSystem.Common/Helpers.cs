﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Net.Mail;

namespace TraineeTrackingSystem.Common
{
    public static class Helper
    {
        public static List<Principal> GetGroupsForUser(string userName, string beginWith = "")
        {
            List<Principal> result = new List<Principal>();

            // set up domain context - if you do a lot of requests, you might
            // want to create that outside the method and pass it in as a parameter
            PrincipalContext ctx = new PrincipalContext(ContextType.Domain);

            string adPasswd = System.Configuration.ConfigurationManager.AppSettings["LDAPAuthPassword"];
            string adUsername = System.Configuration.ConfigurationManager.AppSettings["LDAPAuthUsername"]; ;

            using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, "OSUMC", adUsername, adPasswd))
            {
                UserPrincipal user = UserPrincipal.FindByIdentity(pc, userName);

                // get the user's groups
                if (user != null)
                {

                    PrincipalSearchResult<Principal> groups = user.GetGroups(pc);

                    foreach (Principal gp in user.GetGroups(pc))
                    {
                        if (string.IsNullOrEmpty(gp.SamAccountName))
                            continue;
                        if (!string.IsNullOrEmpty(beginWith) && !gp.SamAccountName.StartsWith(beginWith))
                            continue;
                        result.Add(gp);
                    }
                }

            }
            return result;

        }


        /// <summary>
        /// Adds the given number of business days to the <see cref="DateTime"/>.
        /// </summary>
        /// <param name="current">The date to be changed.</param>
        /// <param name="days">Number of business days to be added/Subtracted.</param>
        /// <returns>A <see cref="DateTime"/> increased/decrease by a given number of business days.</returns>
        public static DateTime AddBusinessDays(this DateTime current, int days)
        {
            var sign = Math.Sign(days);
            var unsignedDays = Math.Abs(days);
            for (var i = 0; i < unsignedDays; i++)
            {
                do
                {
                    current = current.AddDays(sign);
                }
                while (current.DayOfWeek == DayOfWeek.Saturday ||
                    current.DayOfWeek == DayOfWeek.Sunday);
            }
            return current;
        }

        //public static int Weekdays(DateTime dtmStart, DateTime dtmEnd)
        //{
        //    // This function includes the start and end date in the count if they fall on a weekday
        //    int dowStart = ((int)dtmStart.DayOfWeek == 0 ? 7 : (int)dtmStart.DayOfWeek);
        //    int dowEnd = ((int)dtmEnd.DayOfWeek == 0 ? 7 : (int)dtmEnd.DayOfWeek);
        //    TimeSpan tSpan = dtmEnd - dtmStart;
        //    if (dowStart <= dowEnd)
        //    {
        //        return (((tSpan.Days / 7) * 5) + Math.Max((Math.Min((dowEnd + 1), 6) - dowStart), 0));
        //    }
        //    return (((tSpan.Days / 7) * 5) + Math.Min((dowEnd + 6) - Math.Min(dowStart, 6), 5));
        //}


        public static int SkipBusinessDayInMinutes(int delayMinutes)
        {
            DateTime current = DateTime.Now;

            int days = delayMinutes / 1440;

            DateTime future = current.AddBusinessDays(days);
            TimeSpan span = future - current;

            return (int)span.TotalMinutes;


        }

        public static DateTime AddBusinessDays1(DateTime date, int days)
        {
            if (days == 0) return date;

            if (date.DayOfWeek == DayOfWeek.Saturday)
            {
                date = date.AddDays(2);
                days -= 1;
            }
            else if (date.DayOfWeek == DayOfWeek.Sunday)
            {
                date = date.AddDays(1);
                days -= 1;
            }

            date = date.AddDays(days / 5 * 7);
            int extraDays = days % 5;

            if ((int)date.DayOfWeek + extraDays > 5)
            {
                extraDays += 2;
            }

            return date.AddDays(extraDays);

        }


        //public static int CalculateWeekends(DateTime DateTime1, DateTime DateTime2)
        //{
        //    int iReturn = 0;
        //    TimeSpan xTimeSpan;
        //    if (DateTime2 > DateTime1)
        //        xTimeSpan = DateTime2.Subtract(DateTime1);
        //    else
        //        xTimeSpan = DateTime1.Subtract(DateTime2);
        //    int iDays = 5 + System.Convert.ToInt32(xTimeSpan.TotalDays);
        //    iReturn = (iDays / 7);
        //    return iReturn;
        ////}


        private const int MONDAY = 1;
        private const int TUESDAY = 2;
        private const int WEDNESDAY = 3;
        private const int THURSDAY = 4;
        private const int FRIDAY = 5;
        private const int SATURDAY = 6;
        private const int SUNDAY = 7;


        /// <summary>
        /// Observe that the method calculates by including the start day and end day
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        //public static void GetWeekendDaysBetween(DateTime startDate, DateTime endDate, out int saturdays, out int sundays)
        //{
        //    saturdays = -1;
        //    sundays = -1;
        //    if (endDate < startDate)
        //        return;
        //    TimeSpan timeBetween = endDate.Subtract(startDate);
        //    int weekendsBetween = timeBetween.Days / 7;
        //    sundays = weekendsBetween;
        //    saturdays = weekendsBetween;
        //    int startDay = GetDayOfWeekNumber(startDate.DayOfWeek);
        //    int endDay = GetDayOfWeekNumber(endDate.DayOfWeek);
        //    if (startDay > endDay)
        //    {
        //        sundays++;
        //        saturdays += (startDay < SUNDAY) ? 1 : 0;
        //    }
        //    else if (startDay < endDay)
        //    {
        //        //We don't have to care about sundays here, since we are excluding the last day
        //        //There will only be another saturday, if the end day is a sunday
        //        saturdays += (endDay == SUNDAY) ? 1 : 0;
        //    }

        //    //since the above logic doesn't inculde end day, chekc the end day here
        //    saturdays += (endDay == SATURDAY) ? 1 : 0;
        //    sundays += (endDay == SUNDAY) ? 1 : 0;
        //}


        ///// <summary>
        ///// it's not  a good idea to rely on the face that the enums have specific values, I wrote this method
        ///// If you are satisfied with using the integer value of the enums, just remember that Sundays will then have the value 0
        ///// </summary>
        ///// <param name="day"></param>
        ///// <returns></returns>
        //private static int GetDayOfWeekNumber(DayOfWeek day)
        //{
        //    switch (day)
        //    {
        //        case DayOfWeek.Monday:
        //            return MONDAY;
        //        case DayOfWeek.Tuesday:
        //            return TUESDAY;
        //        case DayOfWeek.Wednesday:
        //            return WEDNESDAY;
        //        case DayOfWeek.Thursday:
        //            return THURSDAY;
        //        case DayOfWeek.Friday:
        //            return FRIDAY;
        //        case DayOfWeek.Saturday:
        //            return SATURDAY;
        //        case DayOfWeek.Sunday:
        //            return SUNDAY;
        //        default:
        //            throw new ArgumentException("Invalid day!");
        //    }
        //}


        public static void SendEmail(string fromAddress, string toAddress, string subject, string body, string smtpClientHost = "smtp.osumc.edu")
        {


            try
            {
                var smtpClient = new SmtpClient(smtpClientHost);
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = true;

                MailMessage msg = new MailMessage();

                msg.Body = body;
                msg.Subject = subject;
                msg.From = new MailAddress(fromAddress);

                var toAddressArray = toAddress.Split(';');

                foreach (var t in toAddressArray)
                {
                    if (string.IsNullOrEmpty(t))
                        continue;
                    msg.To.Add(new MailAddress(t));
                }


                //var msg = new MailMessage("admin@contoso.com", "yingping.mao@osumc.edu",
                //                          string.Format("eeeewwwwwwwwwwwww{0}", "aaa"), "sss") { IsBodyHtml = true };
                smtpClient.Send(msg);
            }
            //catch (Exception e)
            catch(Exception)
            {

            }
        }


    }
}

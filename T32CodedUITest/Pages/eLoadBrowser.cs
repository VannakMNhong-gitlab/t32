﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace T32CodedUITest.Pages
{
    /// <summary>
    /// MVN - created 9/1/2016
    /// </summary>
    class eLoadBrowser
    {
        private Microsoft.VisualStudio.TestTools.UITesting.BrowserWindow browser;

        public eLoadBrowser(Microsoft.VisualStudio.TestTools.UITesting.BrowserWindow browser)
        {
            // TODO: Complete member initialization
            this.browser = browser;
        }

        /// <summary>
        /// MVN - 9/1/2016
        /// This method called by CodedUITestMethod1 method
        /// Essentially loading IE, in the search property box, accept the text => Hello World
        /// </summary>
        public void eSearch()
        {
            HtmlEdit searchInput = new HtmlEdit(browser);
            //Then add the UI controller property 
            searchInput.SearchProperties.Add(HtmlEdit.PropertyNames.Id, "lst-ib");

            searchInput.Text = "Hello World";
            //btnG => this button exist inside the browser object
            HtmlButton searchBtn = new HtmlButton(browser);
            searchBtn.SearchProperties.Add(HtmlButton.PropertyNames.Name, "btnG");
            Mouse.Click(searchBtn);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
///Building framework
using T32CodedUITest.Pages;
using T32CodedUITest.Tests;

namespace T32CodedUITest.Tests
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    public class CodedUITest1
    {
        /// <summary>
        /// MVN - created 9/1/2016
        /// This method just to ensure that the google web browser presentation is loaded 
        /// and perform the search hello world contents....
        /// Calls the eSearch() function.
        /// NOTE: One TestMethod = one testCase 
        [TestMethod]
        public void CodedUITestMethodSample()
        {
            string url = "http://www.google.com";
            BrowserWindow browser = BrowserWindow.Launch(url);
            browser.Maximized = true;
            eLoadBrowser eLoadingIE = new eLoadBrowser(browser);


            eLoadingIE.eSearch();

        }
        /// <summary>
        /// MVN - create 9/1/2016 - test login page
        /// </summary>
        [TestMethod]
        public void CodedUITestT32LoginPage()
        {
            string url = "http://www.google.com";
            BrowserWindow browser = BrowserWindow.Launch(url);
            browser.Maximized = true;
            eLoadBrowser eLoadingIE = new eLoadBrowser(browser);
            this.UIMap.CodedUITestT32login();
        }


        public UIMap UIMap
        {
            get
            {
                if ((this.map == null))
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }
}

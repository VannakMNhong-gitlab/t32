﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;

using TraineeTrackingSystem.Common;
using TraineeTrackingSystem.Web.Common;

namespace TraineeTrackingSystem.Web
{
    public partial class TraineeTrackingSystemRedirect : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //this request center home url
            string returnUrl = ConfigurationManager.AppSettings["TraineeTrackingSystemUrl"];

            var token = Security.CreateToken(User.Identity.GetUserName(), HttpContext.Current.Request.UserHostAddress);

            string entryUrl = "";
            NameValueCollection data = new NameValueCollection();

            ////The request center home url
            data.Add("ReturnUrl", returnUrl);

            ////add the token
            data.Add("token", token);

            ////where the redirect request comes from 
            string redirectType = Request.QueryString["RedirectType"];

            if (string.IsNullOrEmpty(redirectType))
                redirectType = "";
            
             //MY TODO
            //the redirect purpose
            data.Add("RedirectType", redirectType);

            //string requestTypeID = Request.QueryString["RequestTypeID"];
            //if (string.IsNullOrEmpty(requestTypeID))
            //    requestTypeID = "";


            //entryUrl = GetEntryUrlByRequestType(requestTypeID);

            //switch (redirectType.ToLowerInvariant())
            //{
            //    case "newrequest":

            //        data.Add("RequestTypeID", requestTypeID);

            //        break;

            //    case "respond":

            //        //data.Add("RespondUrl",  Server.UrlEncode(Request.QueryString["RespondUrl"]));

            //        data.Add("RespondUrl", (Request.QueryString["RespondUrl"]));

            //        break;

            //    default:  // if nothing match, return to application home page
            //        entryUrl = returnUrl;
            //        break;

            //}

            HttpHelper.RedirectAndPOST(this.Page, entryUrl, data);
        }
        
        // MY TODO
        //private string GetEntryUrlByRequestType(string requestTypeID)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
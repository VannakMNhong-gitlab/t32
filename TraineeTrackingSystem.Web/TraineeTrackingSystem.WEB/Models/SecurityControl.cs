﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using System.Web.Mvc;
using System.Web.Routing;
using TraineeTrackingSystem.Web.Entity;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.Models
{
    /// <summary>
    /// MVN - define name of roles as constant string
    /// </summary>
    public static class SecurityControl
    {
        //Define name of roles as constant string 
        //public static bool IsSuperUser(IPrincipal user){
        //    return user.IsInRole(CustomRoles.Administrator.ToString());// || user.IsInRole(CustomRoles.Administrator.ToString());
        //}
        public static bool IsAdminUser(IPrincipal user)
        {
            return user.IsInRole(CustomRoles.Administrator.ToString());// || user.IsInRole(CustomRoles.SuperUser.ToString());
        }
        public static bool IsEditUser(IPrincipal user){
            return user.IsInRole(CustomRoles.Administrator.ToString())
                   || user.IsInRole(CustomRoles.Editor.ToString());

        }
        public static bool IsReaderUser(IPrincipal user){
            return user.IsInRole(CustomRoles.Administrator.ToString())
                    || user.IsInRole(CustomRoles.Editor.ToString())
                    || user.IsInRole(CustomRoles.Reader.ToString());
        }
        
        
        
    }
    /// <summary>
    /// MVN - 4/13/2016 - define user role throu this custom AuthorizeAttribute
    /// that accepts Enum as parameters in the constructor.  It will inherit from 
    /// the standard Sytem.Web.Mvc.AuthorizeAttribute
    /// </summary>
    //public class AuthorizeUserAttribute : AuthorizeAttribute
    public class AuthorizeUserAttribute : AuthorizeAttribute
    {
        /// mvn - custom property
        /// Authorize multiple roles
        public string AccessLevel { get; set; }
        //public new CustomRoles Roles;  // new keyword will hide base class Roles Property
        /// <summary>
        /// MVN - Customize the authority attributes to allow multiple roles, 
        /// which can be used on the controller classes
        /// </summary>
        /// <param name="roles"></param>
        //public AuthorizeUserAttribute(params string[] roles)
        //    : base(){
        //        AccessLevel = string.Join(",", roles);
        //}
    
        /// <summary>
        /// MVN - Authorize multiple roles
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        protected override bool AuthorizeCore(HttpContextBase httpContext){
            if (httpContext == null)
                throw new ArgumentNullException("httpContext");
            // NEXT - ensure the user is authenicated
            if (!httpContext.User.Identity.IsAuthenticated)
                return false;

            var isAuthorized = base.AuthorizeCore(httpContext);
            if (!isAuthorized){
                return false;
            }

            switch (AccessLevel){
                //case "SuperUser":
                //    return SecurityControl.IsSuperUser(httpContext.User);

                case "Administrator":
                    return SecurityControl.IsAdminUser(httpContext.User);

                case "Editor":
                    return SecurityControl.IsEditUser(httpContext.User);
           
                case "Reader":
                    return SecurityControl.IsReaderUser(httpContext.User);

                default:
                    /// MVN - allows to load the individual request page index
                    return isAuthorized;
            }

        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var isUnAuthorized = base.AuthorizeCore(filterContext.HttpContext);

            if (isUnAuthorized){
                try{
                    //User is login but having no access right to a specific page
                    filterContext.Result = new RedirectToRouteResult(
                                new RouteValueDictionary(
                                    new
                                    {
                                        controller = "Home",
                                        action = "UnAuthorized",
                                        pageName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName
                                    })
                                );
                }catch (Exception ex){

                }
            }else{
                    //User is not login or session timeout, redirect to login page by default
                    //Note: the  base.HandleUnauthorizedRequest(filterContext) does not return 403 to Ajax calls
                    if (filterContext.HttpContext.Request.IsAjaxRequest()){
                        filterContext.HttpContext.Response.StatusCode = 403;
                        filterContext.Result = new JsonResult{
                            Data = new{
                                Error = "403 - Forbidden: Access is denied"
                            },
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet
                        };
                    }else{
                        base.HandleUnauthorizedRequest(filterContext);
                    }
            }
        }

        //public string AccessRole { get; set; }

        //public string AccessRoleIs { get; set; }

        //public CustomRoles Lib { get; set; }
    }
}
﻿namespace TraineeTrackingSystem.Web.Models
{
    public class User
    {
        public string UserName { get; set; }
        public string GivenName { get; set; }
        public string SurName { get; set; }

        public string Email { get; set; }
        public string Phone { get; set; }
        public string FullName
        {
            get
            {
                return GivenName + " " + SurName;
            }
        }

        //; delimited role list
        public string Roles { get; set; }

        public string DeptID { get; set; }

        public string DepartmentName { get; set; }
    }
}

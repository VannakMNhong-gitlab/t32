﻿

$(document).ready(function () {
    $('.mobile-menu').click(function () {
        if ($(this).hasClass('menu-open')) {
            $('.app-nav ul').slideUp();
            $(this).removeClass('menu-open');
        } else {
            $('.app-nav ul').slideDown();
            $(this).addClass('menu-open');
        }
    });


    $('.content.read-only h1.section-title').click(function () {
        if ($(this).next('fieldset').is(':hidden')) {
            $(this).next('fieldset').slideDown();
            $(this).addClass('open');
        }
    });
    /*
    $('#documentForm input#fileData, #documentForm input#fileData1, #documentForm input#fileData2, #documentForm input#fileData3').change( function(){
    	var fileName = $(this).val();
    	$('#documentForm p').before('<div class="document-filename">'+fileName+'</div>');
    });
    $('form#documentForm input#btnUploadFile').click( function(){
    	$('.document-filename').remove();
    });*/
});

//check is string a valid date
function isDate(txtDate)
{
    var currVal = txtDate;
    if(currVal == '')
        return false;
    
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?
    
    if (dtArray == null) 
        return false;
    
    //Checks for mm/dd/yyyy format.
    dtMonth = dtArray[1];
    dtDay= dtArray[3];
    dtYear = dtArray[5];        
    
    if (dtMonth < 1 || dtMonth > 12) 
        return false;
    else if (dtDay < 1 || dtDay> 31) 
        return false;
    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) 
        return false;
    else if (dtMonth == 2) 
    {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay> 29 || (dtDay ==29 && !isleap)) 
            return false;
    }
    return true;
}



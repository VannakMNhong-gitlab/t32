﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.Entity
{
    //public class ASOEnum
    //{
    //}
    /// <summary>
    /// MVN - the following Enum where the roles are declared 
    /// corresponding what defined in the database =>PersonRoles table.
    /// </summary>
    [Serializable]
    [Flags] //  When Enum mark with “Flags“ attribute it will work as bit field
    public enum CustomRoles
    {
        Reader = 1,
        Editor = 2,
        Administrator = 3
        //SuperUser = 4 //,
        //IsDeleted = 1
    }
    //public static class CustomRoles
    //{
    //    public const string SuperUser = "SuperUser";
    //    public const string Administrator = "Administrator";
    //    public const string Editor = "Editor";
    //    public const string Reader = "Reader";
    //}
    public enum ErrorCode
    {
        Success = 0,
        InsertFailure,
        UpdateFailure,
        DeleteFailure,
        InvalidParameters,
        AccessFailure,
        DeniedAccess,
        NotAllowed,
        NotFound,
        NotUnique,
        MultipleRecordsFound,
        RecordExisits,
        RecordApproved,
        NetworkFailure,
        NotKnownFailure,

    }
    public class ErrorMessage
    {
        public string ErrorCode { get; set; }

        public string ReturnId { get; set; }

        public string Message { get; set; }


        public ErrorMessage(string errorCode, string pMessage, string pId)
        {
            this.ErrorCode = errorCode;
            this.Message = pMessage;
            this.ReturnId = pId;
        }
    }
}
﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TraineeTrackingSystem.Web.Startup))]
namespace TraineeTrackingSystem.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
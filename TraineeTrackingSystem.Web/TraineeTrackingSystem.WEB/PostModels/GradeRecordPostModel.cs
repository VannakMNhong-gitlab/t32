﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class GradeRecordPostModel
    {
        //public Guid Id { get; set; } // Id (Primary key)
        public Guid PersonId { get; set; } // PersonId

        //FROM GradeRecord
        public Decimal? Gpa { get; set; } // GPA
        public Decimal? GpaScale { get; set; } // GPAScale    
        public Decimal? GreScoreVerbal { get; set; } // GREScoreVerbal - GreScoreVerbal
        public Decimal? GrePercentileVerbal { get; set; } // GREPercentileVerbal - GrePercentileVerbal
        public Decimal? GreScoreQuantitative { get; set; } // GREScoreQuantitative (score) - GreScoreQuantitative
        public Decimal? GrePercentileQuantitative { get; set; } // GREPercentileQuantitative (Percentile) - GrePercentileQuantitative
        public Decimal? GreScoreAnalytical { get; set; } // GREScoreAnalytical - GreScoreAnalytical       
        public Decimal? GrePercentileAnalytical { get; set; } // GREPercentileAnalytical - GrePercentileAnalytical
        public int? GreScoreSubject { get; set; } // GREScoreSubject - GreScoreSubject
        public Decimal? GrePercentileSubject { get; set; } // GREPercentileSubject (Percentile) - GrePercentileSubject
        public Decimal? McatScoreVerbalReasoning { get; set; } // MCATScoreVerbalReasoning - McatScoreVerbalReasoning
        public Decimal? McatScorePhysicalSciences { get; set; } // MCATScorePhysicalSciences - McatScorePhysicalSciences
        public Decimal? McatScoreBiologicalSciences { get; set; } // MCATScoreBiologicalSciences - McatScoreBiologicalSciences
        public string McatScoreWriting { get; set; } // MCATScoreWriting - McatScoreWriting
        public Decimal? McatPercentile { get; set; } // MCATPercentile - McatPercentile      
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TraineeTrackingSystem.Web.PostModels
{
    class SystemApplicationRole
    {
        public int RoleId { get; set; } // RoleId (Primary key)
        public string RoleName { get; set; } // RoleName
    }
}

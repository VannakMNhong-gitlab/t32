﻿namespace TraineeTrackingSystem.Web.PostModels
{
    public class NewAreaOfStudyPostModel
    {
        public string Name { get; set; }
    }
}
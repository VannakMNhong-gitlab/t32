﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class AdminFacultyTrackPostModel
    {
        public int? Id { get; set; } // FacultyTrackId
        public string FacultyTrack { get; set; } // FacultyTrack
        public int? TotalFacultyTrackUsage { get; set; } // TotalFacultyTrackUsage
    }
}
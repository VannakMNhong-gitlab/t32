﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TraineeTrackingSystem.Web.ViewModels;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class FacultySupportPostModel
    {
        public Guid Id { get; set; } // Id       
        public Guid FundingId { get; set; } // FundingId        
        public string FacultyRole { get; set; }
        public int? FacultyRoleId { get; set; }
        public Guid FacultyId { get; set; }
        public string FacultyFullName { get; set; }
        public Guid TrainingGrantId { get; set; }
        // Used for Training Grants only
        public int? FacultySecondaryRoleId { get; set; }
        public string FacultySecondaryRole { get; set; }

        public virtual ICollection<int> ResearchInterests { get; set; }


        //MVN - added these for the grid selection and include back to the dropdown
        public IEnumerable<KeyValuePair<Guid?, string>> FundingFacultiesList { get; set; }                  // Faculty Dropdown
        public IEnumerable<KeyValuePair<int, string>> RoleList { get; set; }                                // Roles DropdownList
        public virtual ICollection<Guid> TrainingPeriodMentors { get; set; }
        public object DateLastUpdated { get; set; }
        public string LastUpdatedBy { get; set; }
        ///MVN - added 9/24 - this will be used for maintaining the facultySupport dropdown list
        ///for all faculties that on this list, will essentially remove from the dropdown list
        public IEnumerable<FacultySupportViewModel> FacultySupportGridView { get; set; } 
    }
}
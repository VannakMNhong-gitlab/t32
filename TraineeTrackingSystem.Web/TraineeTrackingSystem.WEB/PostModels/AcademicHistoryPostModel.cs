﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.PostModels
{
    //public class ApplicantDegreePostModel
    public class AcademicHistoryPostModel
    {
        public Guid PersonId { get; set; }
        //public int? Id { get; set; }
        public Guid Id { get; set; }
        public string AcademicDegree { get; set; }
        //public int DegreeYear { get; set; }
        public int? YearStarted { get; set; } // YearStarted - Year Entered 
        public int? YearEnded { get; set; } // YearEnded
        public int? YearDegreeCompleted { get; set; } // YearDegreeCompleted
        public string Institution { get; set; }
        public bool IsResidency { get; set; } // IsResidency
        public string ResidencyInstitution { get; set; }
        public string AreaOfStudy { get; set; } // AreaOfStudy
        public string ResearchProjectTitle { get; set; } // ResearchProjectTitle
        public string DoctoralThesis { get; set; } // DoctoralThesis
        public string ResearchAdvisor { get; set; } // ResearchAdvisor

        public string ResidencyAdvisor { get; set; } // ResidencyAdvisor
        public int? ResidencyYearStarted { get; set; } // ResidencyYearStarted
        public int? ResidencyYearEnded { get; set; } // ResidencyYearEnded
        public string Comments { get; set; } // Comments
    }
}
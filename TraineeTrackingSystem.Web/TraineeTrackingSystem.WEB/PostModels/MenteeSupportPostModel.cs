﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class MenteeSupportPostModel
    {

        public int Id { get; set; }         // MenteeSupportId
        public Guid PersonId { get; set; }  // PersonId of the Mentee
        public int MenteeSupportId { get; set; }
        public int? InstitutionId { get; set; } // InstitutionId
        public string Institution { get; set; }
        public string SupportSourceText { get; set; } // SupportSourceText
        public bool IsDeleted { get; set; }

        //public int CurrentSupportSourceId { get; set; }
        //public string CurrentSupportSourceTitle { get; set; }

        //public virtual ICollection<int> MenteeSupports { get; set; }



        public object DateLastUpdated { get; set; }

        public string LastUpdatedBy { get; set; }
    }
}
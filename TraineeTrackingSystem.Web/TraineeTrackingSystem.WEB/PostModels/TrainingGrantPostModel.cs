﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class TrainingGrantPostModel
    {
        public Guid Id { get; set; }

        public Guid FundingId { get; set; } // FundingId

        public virtual ICollection<Guid> TrainingGrantPrograms { get; set; }    // Collection of saved/selected Programs on this TrainingGrant
        public virtual ICollection<int> TrainingGrantDepartments { get; set; } // Collection of saved/selected Depts on this TrainingGrant

        public Guid PdFacultyId { get; set; }
        public string PdFullName { get; set; }

        public string Title { get; set; }
        public int? InstitutionId { get; set; } // InstitutionId
        public string SponsorName { get; set; }
        public int TrainingGrantStatusId { get; set; } // TrainingGrantStatusId
        public IEnumerable<KeyValuePair<int, string>> TrainingGrantStatusName { get; set; }
        public string GrtNumber { get; set; } // GRTNumber
        public string SponsorReferenceNumber { get; set; } // SponsorReferenceNumber
        public DateTime? DateProjectedStart { get; set; }
        public DateTime? DateProjectedEnd { get; set; } // DateProjectedEnd
        public int? DoctoralLevelId { get; set; } // DoctoralLevelId
        public string DoctoralLevel { get; set; }
        public bool IsRenewal { get; set; } // IsRenewal

        public int? NumPredocPositionsRequested { get; set; } // NumPredocPositionsRequested
        public int? NumPostdocPositionsRequested { get; set; } // NumPostdocPositionsRequested
        public int? NumPredocPositionsAwarded { get; set; } // NumPredocPositionsAwarded
        public int? NumPostdocPositionsAwarded { get; set; } // NumPostdocPositionsAwarded
        public int? PredocSupportMonthsAwarded { get; set; } // PredocSupportMonthsAwarded
        public int? PostdocSupportMonthsAwarded { get; set; } // PostdocSupportMonthsAwarded
        public Guid? PreviousTrainingGrantId { get; set; } // PreviousTrainingGrantId
        public int ApplicantPoolAcademicYear { get; set; }

        //public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        //public string LastUpdatedBy { get; set; } // LastUpdatedBy

        public virtual Funding Funding { get; set; }

        //public virtual ICollection<Guid> TrainingPeriodPrograms { get; set; }

        //public virtual ICollection<Guid> TrainingPeriodMentors { get; set; }


        //MVN - added this 3-30-2015
        public Guid MenteeId { get; set; }
        public Guid? TrainingPeriodId { get; set; } // Use this to set the trainingPeriodId in the TrainingGrantTrainee table
        public bool IsTrainingGrantEligible { get; set; } // IsTrainingGrantEligible
    }
}
﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class AdminFacultyEthnicityPostModel
    {
        public int? Id { get; set; } // EthnicityId
        public string Ethnicity { get; set; } // Ethnicity
        public int? TotalEthnicityUsage { get; set; } // TotalEthnicityUsage
    }
}
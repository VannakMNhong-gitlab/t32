﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class SubAwardPostModel
    {
        public Guid Id { get; set; }

        public string PrimeAwardNumber { get; set; }
        public int PrimeSponsorId { get; set; }
        public string PrimeSponsorName { get; set; }
        public Guid? PrimeAwardPIId { get; set; }
        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.PostModels
{
    // From FUNDING table
    public class AwardInformationPostModel
    {
        public Guid Id { get; set; }  // This will be used as reference to the Training Grant table
        public Guid FundingId { get; set; } // PK FundingId 
        public string SponsorAwardNumber { get; set; } // SponsorAwardNumber
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded
    }
}
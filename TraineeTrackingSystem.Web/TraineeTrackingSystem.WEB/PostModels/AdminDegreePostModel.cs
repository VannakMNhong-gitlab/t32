﻿using System;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class AdminDegreePostModel
    {
        public int? Id { get; set; }
        public string AcademicDegree { get; set; }
        public int? TotalDegreeUsage { get; set; } // TotalStateProvinceUsage
    }
}
﻿using System;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class ResearchInterestPostModel
    {
        public Guid PersonId { get; set; }
        public int? Id { get; set; }
        //public DateTime? DateEntered { get; set; }
        public int? DepartmentId { get; set; }
        public string ResearchInterest { get; set; }
    }
}
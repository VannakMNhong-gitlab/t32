﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class AdminFacultyRankPostModel
    {
        public int? Id { get; set; } // FacultyRankId
        public string Rank { get; set; } // Rank
        public int? TotalFacultyRankUsage { get; set; }
    }
}
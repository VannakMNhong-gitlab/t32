﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TraineeTrackingSystem.Data;
using TraineeTrackingSystem.Web.ViewModels;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class PersonPostModel
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string EmployeeId { get; set; }
        public int? PositionDepartmentId { get; set; }
        public int? PositionOrganizationId { get; set; }
        public Guid? MentorId { get; set; } // MentorId
        public int? GenderId { get; set; } // PersonId
        public int? GenderAtBirthId { get; set; }
        public int? EthnicityId { get; set; }
        public int RankId { get; set; }
        public int DegreeYear { get; set; }
        public string OtherDegree { get; set; }
        public string AcadmicDegreeName { get; set;} 
        public int OrganizationId { get; set; }
        public int SecondOrgId { get; set; }
        public int TIUId { get; set; }
        /// MVN - these codes are no longer needed
        //public string OtherAffiliations { get; set; }
        //public string OtherTitles { get; set; }

        // -- FROM PERSON 
        public int? IsUnderrepresentedMinority { get; set; } // IsUnderrepresentedMinority
        public int? IsIndividualWithDisabilities { get; set; } // IsIndividualWithDisabilities
        public int? IsFromDisadvantagedBkgd { get; set; } // IsFromDisadvantagedBkgd

        public DateTime DateCreated { get; set; } // DateCreated
        public Guid PersonId { get; set; } // PersonId

        // -- FROM APPLICANT
        public int ApplicantId { get; set; } // ApplicantId
        public bool IsTrainingGrantEligible { get; set; } // IsTrainingGrantEligible

        // -- FROM ACADEMIC HISTORY
        public int DoctoralLevelId { get; set; } // DoctoralLevelId
        public int? DepartmentId { get; set; } // DepartmentId
        public int? YearEntered { get; set; } // YearEntered =>applicant.PersonId =>AcademicHistory.PersonId THIS give AcademicHistory.YearEnded
        //public int? DegreeEnteredYear { get; set; }
        // public AcademicHistory AcademicHistories { get; set; } // AcademicHistory.FK_AcademicHistory_Person   
        public ApplicantsGeneralViewModel ApplicantInformations { get; set; }

        public virtual ICollection<Guid> FacultyPrograms { get; set; }
    }
}

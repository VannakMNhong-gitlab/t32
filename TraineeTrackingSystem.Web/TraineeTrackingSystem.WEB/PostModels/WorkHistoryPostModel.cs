﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class WorkHistoryPostModel
    {

        public int Id { get; set; }         // MenteeSupportId
        public Guid PersonId { get; set; }  // PersonId of the Mentee
        public int MenteeSupportId { get; set; }
        public int? InstitutionId { get; set; } // InstitutionId
        public string Institution { get; set; }
        public DateTime? DateStarted { get; set; } // DateStartedPosition
        public DateTime? DateEnded { get; set; } // DateEndedPosition

        public string PositionTitle { get; set; } // PositionTitle
        public string PositionDescription { get; set; } // PositionDescription
        public string PositionLocation { get; set; } // PositionLocation
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public bool IsDeleted { get; set; }

        public string SupportSourceText { get; set; } // SupportSourceText

        //public int CurrentSupportSourceId { get; set; }
        //public string CurrentSupportSourceTitle { get; set; }

        //public virtual ICollection<int> SupportSources { get; set; }


    }
}
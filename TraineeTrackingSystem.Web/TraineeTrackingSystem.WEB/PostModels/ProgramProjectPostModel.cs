﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class ProgramProjectPostModel
    {
        public Guid Id { get; set; }        

        public Guid? ProgramProjectPdId { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class SupportPostModel
    {

        public Guid Id { get; set; } // Id (Primary key)
        public string Title { get; set; } // Title
        public int DisplayId { get; set; } // DisplayId
        public int? SupportTypeId { get; set; } // SupportTypeId
       // public IEnumerable<KeyValuePair<int, string>> SupportTypeName { get; set; }
        public int SupportStatusId { get; set; } // SupportStatusId
        //public IEnumerable<KeyValuePair<int, string>> SupportStatusName { get; set; }
        public string SupportNumber { get; set; } // SupportNumber
        public int? SupportOrganizationId { get; set; } // SupportOrganizationId
        //public IEnumerable<KeyValuePair<int, string>> SupportOrganizationName { get; set; }
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded
        public DateTime? DateCreated { get; set; } // DateCreated
        public string CreatedBy { get; set; } // CreatedBy
        public string CreatedByName { get; set; }
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public string LastUpdatedByName { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;


namespace TraineeTrackingSystem.Web.PostModels
{
    public class StateAdminPostModel
    {
        public int? Id { get; set; } // Id (Primary key)
        public string Abbreviation { get; set; } // Abbreviation
        public string StateFullName { get; set; } // FullName
        public int? CountryId { get; set; } // CountryId
        public string Country { get; set; } // Name 
        public int? TotalStateProvinceUsage { get; set; } // TotalStateProvinceUsage
    }
}
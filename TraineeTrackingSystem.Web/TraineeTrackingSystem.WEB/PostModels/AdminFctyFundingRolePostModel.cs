﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class AdminFctyFundingRolePostModel
    {
        public int? Id { get; set; } // FundingRoleId
        public string Name { get; set; } // Name
        public int? TotalFundingRoleUsage { get; set; } // TotalFundingRoleUsage
        public int? TotalTrainingGrantRoleUsage { get; set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Principal;
using TraineeTrackingSystem.Data;
namespace TraineeTrackingSystem.Web.PostModels
{
    public class MenteePostModel
    {
        public Guid Id { get; set; }
        public Guid PersonId { get; set; } // PersonId
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string StudentId { get; set; }

        public int? InstitutionAssociationId { get; set; } // InstitutionAssociationId
        public int? DepartmentId { get; set; } // DepartmentId
        //public int? DoctoralLevelId { get; set; } // DoctoralLevelId
        public bool IsTrainingGrantEligible { get; set; } // IsTrainingGrantEligible
        public bool WasRecruitedToLab { get; set; } // WasRecruitedToLab

        //public string EmailAddress1 { get; set; }
        //public string EmailAddress2 { get; set; }
        //public string PhoneNumber { get; set; }
        //public string MailingAddressLine1 { get; set; }
        //public string MailingAddressLine2 { get; set; }
        //public string MailingAddressLine3 { get; set; }
        //public string MailingAddressCity { get; set; }
        //public int? MailingAddressStateId { get; set; }
        //public string MailingAddressPostalCode { get; set; }
        //public int? MailingAddressCountryId { get; set; }

        public int? IsUnderrepresentedMinority { get; set; }
        public int? IsIndividualWithDisabilities { get; set; }
        public int? IsFromDisadvantagedBkgd { get; set; }
        public IPrincipal User { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class InstAdminPostModel
    {
        public int? Id { get; set; } // Id (Primary key)
        public string InstitutionIdentifier { get; set; } // InstitutionIdentifier
        public string Name { get; set; } // Name
        public string City { get; set; } // City
        public int? StateId { get; set; } // StateId
        public string StateName { get; set; }
        public IEnumerable<KeyValuePair<int, String>> States { get; set; }
        public IEnumerable<KeyValuePair<int, String>> Countries { get; set; }
        public int? CountryId { get; set; } // CountryId
        public string CountryName { get; set; }
        public int? TotalInstitutionUsage { get; set; } // TotalInstitutionUsage
    }
}
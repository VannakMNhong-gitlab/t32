﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class TraineeDetailedPostModel
    {
        public Guid Id { get; set; } // Id   
        public Guid TrainingGrantId { get; set; } // TrainingGrantId
        public Guid FundingId { get; set; }
        public Guid TrainingGrantTraineeId { get; set; }
        public Guid MenteeId { get; set; } // MenteeId
        public Guid? MenteePersonId { get; set; }
        public string MenteeFullName { get; set; }
        //public string InstitutionAssociation { get; set; }
        //public string ResearchExperience { get; set; }
        
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded

        // MVN - added Accept Client Side PostBack
        public int? InstitutionAssociationId { get; set; } // InstitutionAssociationId
        public string InstitutionAssociation { get; set; }
        public string ResearchProjectTitle { get; set; }

        /*  ***** TOBE USED collecting ProgramStatus => based on the current DoctoralLevel then display the TraineeStatus **** */
        public string TraineeStatus { get; set; }
        //public IEnumerable<KeyValuePair<int, String>> TraineeStatus { get; set; }
        public string PreDocProgramStatus { get; set; }
        public string PostDocProgramStatus { get; set; }
        /* ********************************************************** */

        public Guid? TrainingPeriodId { get; set; } // TrainingPeriodId
        public Guid PersonId { get; set; } // PersonId
      
        public int? PredocTraineeStatusId { get; set; } // PredocTraineeStatusId
        public int? PostdocTraineeStatusId { get; set; } // PostdocTraineeStatusId

        public int DoctoralLevelId { get; set; } // DoctoralLevelId
        public string PredocReasonForLeaving { get; set; } // PredocReasonForLeaving
        public string PostdocReasonForLeaving { get; set; } // PostdocReasonForLeaving

        //Testing
        public TraineeStatu TraineeStatu_PredocTraineeStatusId { get; set; } // FK_TrainingGrantTrainee_TraineeStatusPredoc
        public TraineeStatu TraineeStatu_PostdocTraineeStatusId { get; set; } // FK_TrainingGrantTrainee_TraineeStatusPostdoc
    }
}
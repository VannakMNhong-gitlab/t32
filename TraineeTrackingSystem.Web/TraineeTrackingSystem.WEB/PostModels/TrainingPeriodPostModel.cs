﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class TrainingPeriodPostModel
    {
        public Guid Id { get; set; }

        public Guid PersonId { get; set; }  // PersonId of the Mentee
        public int? InstitutionId { get; set; } // InstitutionId
        public string Institution { get; set; }
        public int DoctoralLevelId { get; set; } // DoctoralLevelId
        public string DoctoralLevel { get; set; }
        public int? YearStarted { get; set; } // YearStarted
        public int? YearEnded { get; set; } // YearEnded
        public int? AcademicDegreeId { get; set; } // AcademicDegreeId
        public string AcademicDegree { get; set; }
        public string AcademicDegreeSought { get; set; }
        public string ResearchProjectTitle { get; set; } // ResearchProjectTitle
        // MVN 
        public Guid ProgramId { get; set; } // ProgramId
        public int? DegreeSoughtId { get; set; } // DegreeSoughtId
        public string DegreeSought { get; set; }
        ///public string Programs { get; set; }
        public virtual ICollection<Guid> TrainingPeriodPrograms { get; set; }

        public virtual ICollection<Guid> TrainingPeriodMentors { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class AdminFacultyGenderPostModel
    {
        public int? Id { get; set; } // GenderId
        public string Gender { get; set; } // Gender
        public int? TotalGenderUsage { get; set; } // TotalGenderUsage
    }
}
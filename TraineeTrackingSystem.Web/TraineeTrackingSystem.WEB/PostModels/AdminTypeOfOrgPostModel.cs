﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class AdminTypeOfOrgPostModel
    {
        public int? Id { get; set; } // OrganizationTypeId
        public string OrganizationType { get; set; } // OrganizationType
        public int? TotalOrganization { get; set; } // TotalOrganization
       
    }
}
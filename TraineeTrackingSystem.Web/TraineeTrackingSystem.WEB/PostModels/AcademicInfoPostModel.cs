﻿using System;
using System.Collections.Generic;


namespace TraineeTrackingSystem.Web.PostModels
{
    /// <summary>
    /// MVN - this will be used to update Academic History Information records
    /// </summary>
    public class AcademicInfoPostModel
    {
        public Guid Id { get; set; } // Id (Primary key) - to be used for facultyId
        public Guid PersonId { get; set; }
        public int InstitutionId { get; set; } // InstitutionId
        public string InstName { get; set; }
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded
        public string DateStartedText { get; set; }
        public string DateEndedText { get; set; }
        public int? AcademicDegreeId { get; set; } // AcademicDegreeId
        public string AcademicDegree { get; set; } // Degree
        public DateTime? DateDegreeCompleted { get; set; } // DateDegreeCompleted
        public string AreaOfStudy { get; set; } // AreaOfStudy
        public string ResearchProjectTitle { get; set; } // ResearchProjectTitle
        public string Comments { get; set; } // Comments
    }
}
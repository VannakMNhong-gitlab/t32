﻿using System;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class PublicationPostModel
    {
        public Guid AuthorId { get; set; }
        public Guid Id { get; set; }
        public Guid PersonId { get; set; } // MVN added this
        public string StudentId { get; set; } // MVN added this
        public DateTime? DateEntered { get; set; }
        //public string PMId { get; set; }
        // FROM Publication table
        public string Pmcid { get; set; } // PMCID
        public string Pmid { get; set; } // PMID
        //public bool IsMenteeFirstAuthor { get; set; } // IsMenteeFirstAuthor
        //public bool IsAdvisorCoAuthor { get; set; } // IsAdvisorCoAuthor
        public int IsMenteeFirstAuthor { get; set; } // IsMenteeFirstAuthor
        public int IsAdvisorCoAuthor { get; set; } // IsAdvisorCoAuthor
        public string AuthorList { get; set; } // AuthorList
        public string Title { get; set; } // Title
        public string Journal { get; set; } // Journal
        public DateTime? DateEpub { get; set; } // DateEpub
        public string Doi { get; set; } // DOI
        public DateTime? DatePublicationLastUpdated { get; set; } // DatePublicationLastUpdated
        public int? YearPublished { get; set; } // YearPublished
        public string Volume { get; set; } // Volume
        public string Issue { get; set; } // Issue
        public string Pagination { get; set; } // Pagination
        public string Citation { get; set; }
        public string Abstract { get; set; } // Abstract
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class ApplicantPostModel
    {
        public Guid Id { get; set; } // Id (Primary key)
        public Guid PersonId { get; set; } // PersonId
        public int? newAppId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public int? YearEntered { get; set; }

        // Needs applicantType =>FROM DOCTORAL    
        public int? DoctoralLevelId { get; set; } // DoctoralLevelId
        public int? DepartmentId { get; set; } // DepartmentId
        //public int DepartmentId { get; set; } // DepartmentId
        //public Guid ProgramId { get; set; }
        public virtual ICollection<Guid> ProgramsCollection { get; set; }

        // Need training eligible => FROM APPLICANT  
        public int ApplicantId { get; set; } // ApplicantId
        public bool IsTrainingGrantEligible { get; set; } // IsTrainingGrantEligible
        
        //Test
        //public virtual IEnumerable<KeyValuePair<Guid, string>> SelectedPrograms { get; set; }

        // => FROM PERSON 
        public int? IsUnderrepresentedMinority { get; set; }
        public int? IsIndividualWithDisabilities { get; set; }
        public int? IsFromDisadvantagedBkgd { get; set; }

        //FROM GradeRecord
        public Decimal? Gpa { get; set; } // GPA
        public Decimal? GpaScale { get; set; } // GPAScale    

        //FROM OsuTimeLine
        public bool WasInterviewed { get; set; } // WasInterviewed
        public bool AcceptedOffer { get; set; } // AcceptedOffer Post-doc
        public bool WasAccepted { get; set; } // WasAccepted - Pre-doc
        public bool WasEnrolled { get; set; } // WasEnrolled
        public Decimal? GreScoreVerbal { get; set; } // GREScoreVerbal - GreScoreVerbal
        public Decimal? GrePercentileVerbal { get; set; } // GREPercentileVerbal - GrePercentileVerbal
        public Decimal? GreScoreQuantitative { get; set; } // GREScoreQuantitative (score) - GreScoreQuantitative
        public Decimal? GrePercentileQuantitative { get; set; } // GREPercentileQuantitative (Percentile) - GrePercentileQuantitative
        public Decimal? GreScoreAnalytical { get; set; } // GREScoreAnalytical - GreScoreAnalytical       
        public Decimal? GrePercentileAnalytical { get; set; } // GREPercentileAnalytical - GrePercentileAnalytical
        public int? GreScoreSubject { get; set; } // GREScoreSubject - GreScoreSubject
        public Decimal? GrePercentileSubject { get; set; } // GREPercentileSubject (Percentile) - GrePercentileSubject
        public Decimal? McatScoreVerbalReasoning { get; set; } // MCATScoreVerbalReasoning - McatScoreVerbalReasoning
        public Decimal? McatScorePhysicalSciences { get; set; } // MCATScorePhysicalSciences - McatScorePhysicalSciences
        public Decimal? McatScoreBiologicalSciences { get; set; } // MCATScoreBiologicalSciences - McatScoreBiologicalSciences
        public string McatScoreWriting { get; set; } // MCATScoreWriting - McatScoreWriting
        public Decimal? McatPercentile { get; set; } // MCATPercentile - McatPercentile      
        public bool WasOfferedPosition { get; set; } // WasOfferedPosition
        public bool EnteredProgram { get; set; } // EnteredProgram
    }
}
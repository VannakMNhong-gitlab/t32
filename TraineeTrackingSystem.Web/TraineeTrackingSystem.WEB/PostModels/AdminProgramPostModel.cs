﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class AdminProgramPostModel
    {
        public Guid Id { get; set; } // ProgramId
        public string ProgramTitle { get; set; } // ProgramTitle
        public int ProgramDisplayId { get; set; } // ProgramDisplayId
        public int? TotalProgramUsage { get; set; } // TotalProgramUsage
    }
}
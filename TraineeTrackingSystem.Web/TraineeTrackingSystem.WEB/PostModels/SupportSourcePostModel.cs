﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class SupportSourcePostModel
    {


        public int Id { get; set; }         // SupportSourceId
        //public Guid PersonId { get; set; }  // PersonId of the Mentee
        public int MenteeSupportId { get; set; }
        public string SupportTitle { get; set; }
        public bool IsDeleted { get; set; }

    }
}
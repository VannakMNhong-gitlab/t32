﻿namespace TraineeTrackingSystem.Web.PostModels
{
    public class NewStateProvincePostModel
    {
        public string Abbreviation { get; set; }
        public string FullName { get; set; }
    }
}
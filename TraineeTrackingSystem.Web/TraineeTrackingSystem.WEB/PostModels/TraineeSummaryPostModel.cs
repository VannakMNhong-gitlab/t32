﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class TraineeSummaryPostModel
    {
        public Guid TraineeSummaryId { get; set; }  // This id replaces the Id to reduce some confusion 
        public Guid TrainingGrantId { get; set; }
        //public Guid Id { get; set; }            //MVN 6/17/2015 => no longer needed     
        //public Guid FundingId { get; set; } // PK FundingId 
        //public int? NumPredocPositionsAwarded { get; set; } // Number of Pre-doc Positions Awarded
        //public int? PredocSupportMonthsAwarded { get; set; } // Number of Months of Support per Pre-doc Position Awarded
        //public int? NumPostdocPositionsAwarded { get; set; } // Number of Post-doc Positions Awarded
        //public int? PostdocSupportMonthsAwarded { get; set; } // Number of Months of Support per Post-doc Position Awarded
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded
        public int? PositionsAwarded { get; set; } // PositionsAwarded
        public int? SupportMonthsAwarded { get; set; } // SupportMonthsAwarded
        public int? TraineesAppointed { get; set; } // TraineesAppointed
        public int? SupportMonthsUsed { get; set; } // SupportMonthsUsed
        public int? UrmTraineesAppointed { get; set; } // URMTraineesAppointed
        public int? DisabilitiesTraineesAppointed { get; set; } // DisabilitiesTraineesAppointed
        public int? DisadvantagedTraineesAppointed { get; set; } // DisadvantagedTraineesAppointed
        public int? UrmSupportMonthsUsed { get; set; } // URMSupportMonthsUsed
        public int? DisabilitiesSupportMonthsUsed { get; set; } // DisabilitiesSupportMonthsUsed
        public int? DisadvantagedSupportMonthsUsed { get; set; } // DisadvantagedSupportMonthsUsed
        public int? NumMdAppointed { get; set; } // NumMDAppointed
        public int? NumMdPhDAppointed { get; set; } // NumMDPhDAppointed
        public int? NumPhDAppointed { get; set; } // NumPhDAppointed
        public int? NumOtherDegreeAppointed { get; set; } // NumOtherDegreeAppointed
        public int DoctoralLevelId { get; set; } // DoctoralLevelId
        //public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        //public string LastUpdatedBy { get; set; } // LastUpdatedBy
        
    }
}
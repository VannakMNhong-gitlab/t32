﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class FCTYResearchInterestPostModel
    {
        public System.Guid PersonId { get; set; }
        public Nullable<System.DateTime> DateEntered { get; set; }
        public string ResearchInterest { get; set; }
        public string ResearchInterestDeptDisplayName { get; set; }
        public string EmployeeId { get; set; }
        public int FacultyRankId { get; set; }
        public int PrimaryOrganizationId { get; set; }
        public int SecondaryOrganizationId { get; set; }
        public string OtherTitles { get; set; }
        
    }
}
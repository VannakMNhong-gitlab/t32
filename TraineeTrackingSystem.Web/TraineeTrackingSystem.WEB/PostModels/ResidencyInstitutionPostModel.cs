﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class ResidencyInstitutionPostModel
    {
        public string Name { get; set; } // Name
        public bool IsResidency { get; set; } // IsResidency
        public string City { get; set; } // City
        public int? StateId { get; set; } // StateId
        public int CountryId { get; set; } // CountryId
    }
}
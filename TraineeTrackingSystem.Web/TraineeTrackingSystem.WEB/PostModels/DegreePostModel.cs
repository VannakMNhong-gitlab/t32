﻿using System;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class DegreePostModel
    {
        public Guid PersonId { get; set; }
        public int? Id { get; set; }
        public string AcademicDegree { get; set; }
        public int? DegreeYear { get; set; }
        public string Institution { get; set; }       
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class AppointmentInfoPostModel
    {
        public int? Id { get; set; } // Faculty Appointment Id
        public Guid PersonId { get; set; } // PersonId
        public int InstitutionId { get; set; } // InstitutionId
        public string InstName { get; set; }
        public int? FacultyTrackId { get; set; } // FacultyTrackId
        public string FacultyTrack { get; set; } // FacultyTrack        
        public int? ClinicalFacultyPathwayId { get; set; } // ClinicalFacultyPathwayId
        public string ClinicalFacultyPathway { get; set; } // ClinicalFacultyPathway
        public int? FacultyRankId { get; set; } // FacultyRankId
        public string FacultyRank { get; set; }
        public string OtherAffiliations { get; set; } // OtherAffiliations
        public string OtherTitles { get; set; } // OtherTitles
        public Decimal? PercentFte { get; set; } // PercentFTE
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded
        public string Comment { get; set; } // Comment
        public string StrDateLastUpdated { get; set; }
        public string StrLastUpdatedBy { get; set; }
    }
}
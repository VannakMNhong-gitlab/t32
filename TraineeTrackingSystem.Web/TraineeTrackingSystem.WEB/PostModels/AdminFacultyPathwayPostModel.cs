﻿using System;
using System.Collections.Generic;


namespace TraineeTrackingSystem.Web.PostModels
{
    public class AdminFacultyPathwayPostModel
    {
        public int? Id { get; set; } // ClinicalFacultyPathwayId
        public string ClinicalFacultyPathway { get; set; } // ClinicalFacultyPathway
        public int? TotalClinicalFacultyPathwayUsage { get; set; } // TotalClinicalFacultyPathwayUsage
    }
}
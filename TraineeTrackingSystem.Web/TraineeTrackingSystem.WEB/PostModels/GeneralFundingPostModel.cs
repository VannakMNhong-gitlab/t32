﻿using System;
using TraineeTrackingSystem.Web.ViewModels;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class GeneralFundingPostModel
    {
        //public Guid FFTyId { get; set; }  // Created this to reduce some confusion of Id in Funding, in FundingFaculty and Faculty tables
        public Guid Id { get; set; } // Id       
        public Guid FundingId { get; set; } // FundingId        
        public int? SponsorId { get; set; } // SponsorId       
        public string SponsorName { get; set; } // SponsorName 
        public string SponsorReferenceNumber { get; set; } // SponsorReferenceNumber
        public int FundingStatusId { get; set; } // FundingStatusId
        public IEnumerable<KeyValuePair<int, string>> FundingStatusName { get; set; }       
        //Jan28/2015public IEnumerable<KeyValuePair<Guid?, string>> FundingFacultiesList { get; set; }
        public string Title { get; set; } // Title
        public string GrtNumber { get; set; } // GRTNumber
        public int? FundingTypeId { get; set; }
        public string FundingType { get; set; }
        //public bool IsRenewal { get; set; } // IsRenewal
        public string PrimeAward { get; set; } // PrimeAward
        public Guid PiFacultyId { get; set; } // PIPersonId
        //public string FacultyFullName { get; set; } // FullName
        public string PiFullName { get; set; } // PIFullName
        //public int PrimaryRoleId { get; set; } // PrimaryRoleId
        //public string Name { get; set; } // Name
        public string FacultyRole { get; set; }
        public int? FacultyRoleId { get; set; }
        public string selectedId { get; set; }
        
        //Test ******************************
        public Guid FacultyId { get; set; }
        public string FacultyFullName { get; set; }

        // MVN - Updated 5/14/2015 ->added Date Project start/end
        public DateTime? DateProjectedStart { get; set; } // DateProjectedStart
        public DateTime? DateProjectedEnd { get; set; } // DateProjectedEnd
    }
}
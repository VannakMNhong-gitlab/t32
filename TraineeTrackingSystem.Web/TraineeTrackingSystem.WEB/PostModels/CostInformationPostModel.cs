﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.PostModels
{
    // From FundingDirectCost table
    public class CostInformationPostModel
    {
        // Year =>funding 2/3/2015 just year - no dropdown IN FundingDirectCost
        // Needs 6 addtional properties here 
        public int? Id { get; set; }         //TO be used int eh Grid selection and data-binding 
        public Guid FundingId { get; set; } // PK FundingId 
        public int? CostYear { get; set; } // CostYear Funding.Id = FundingDirectCost.FundingId
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded
        public Decimal? CurrentYearDirectCosts { get; set; } // CurrentYearDirectCosts Funding.Id = FundingDirectCost.FundingId
        public Decimal? TotalDirectCosts { get; set; } // TotalDirectCosts Funding.Id = FundingDirectCost.FundingId
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public int? BudgetPeriodStatusId { get; set; } // FundingStatusId
        //public bool IsCurrent { get; set; }
        public IEnumerable<KeyValuePair<int, string>> BudgetPeriodStatuses { get; set; }
        public string BudgetPeriodStatus { get; set; }
        //public virtual ICollection<int> BudgetPeriodStatusesCollection { get; set; }
    }
}
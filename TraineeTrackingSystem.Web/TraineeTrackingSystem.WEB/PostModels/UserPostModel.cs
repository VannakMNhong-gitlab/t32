﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using TraineeTrackingSystem.Data;
using TraineeTrackingSystem.Web.ViewModels;

namespace TraineeTrackingSystem.Web.PostModels
{
    public class UserPostModel
    {
        public Guid Id { get; set; }
        public Guid? PersonId { get; set; } // PersonId
        //[Column("LoginId")]
        //[Key]
        public int LoginId { get; set; } // LoginId (Primary key)
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }      
        public string RoleName { get; set; } // RoleName
        public int RoleId { get; set; } // RoleId (Primary key)
        //public virtual ICollection<int> PersonRoles { get; set; }
        public virtual ICollection<int> PersonRoles { get; set; }
        //public List<SystemApplicationRole> PersonRoles { get; set; }
        public virtual ICollection<int> Roles { get; set; }
        public bool IsActive { get; set; } // IsActive
    }
}
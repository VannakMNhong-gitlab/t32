﻿namespace TraineeTrackingSystem.Web.PostModels
{
    public class NewCountryPostModel
    {
        public string Name { get; set; }
    }
}
﻿using System.Web;
using System.Web.Optimization;

namespace TraineeTrackingSystem.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        //  public static void RegisterBundles(BundleCollection bundles)
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
              "~/Scripts/Kendo/js/kendo.all.min.js",
              "~/Scripts/kendo/js/kendo.aspnetmvc.min.js",
              "~/Scripts/kiui-0.0.5.js"));


            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"
                //,
                //"~/Scripts/jquery.unobtrusive-ajax.min.js",
                //"~/Scripts/jquery.validate.min.js"
                //"~/Scripts/jquery.validate.unobtrusive.min.js"
                        ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/respond").Include(
                      "~/Scripts/respond.js"));



            bundles.Add(new StyleBundle("~/Content/css").Include(
                       "~/Content/kiui.min.css",
                      "~/Content/bootstrap.css"));

            bundles.Add(new StyleBundle("~/Content/kendo/styles/css").Include(
                      "~/Content/Kendo/styles/kendo.common.min.css",
                     "~/Content/Kendo/styles/kendo.rtl.min.css",
                     "~/Content/Kendo/styles/kendo.default.min.css"));



            //bundle OSUMC style, addition application styles can be bundled here 
            //bundles.Add(new StyleBundle("~/Content/osuwmc").Include(
            //                                "~/Content/themes/OSUWMC/app-base.css"));
            /// MVN - added 2/26/2016 to resolve Font issues => "~/Content/themes/OSUWMC/osuwmc.css",
            bundles.Add(new StyleBundle("~/Content/themes/OSUWMC/css").Include(
                                            "~/Content/themes/OSUWMC/osuwmc.css",
                                            "~/Content/themes/OSUWMC/trainee.css"));

            //bundle OSUMC JavaScript, addition application JavaScripts can be bundled here 
            /*bundles.Add(new ScriptBundle("~/bundles/osuwmc").Include(
                      "~/scripts/OSUWMC*",
                      "~/scripts/AdminTabs.js"));*/
            // MVN - added TTSAdmin.js 2/18/2014 for the T32 application specific
            //bundle OSUMC JavaScript, addition application JavaScripts can be bundled here 
            bundles.Add(new ScriptBundle("~/bundles/osuwmc").Include(
                       "~/Scripts/OSUWMC/osuwmc.js", "~/Scripts/OSUWMC/placeholder.js"));
            bundles.IgnoreList.Clear();

        }
    }
}



//using System.Web;
//using System.Web.Optimization;

//namespace TraineeTrackingSystem.Web
//{
//    public class BundleConfig
//    {
//        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
//        public static void RegisterBundles(BundleCollection bundles)
//        {
//            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
//                        "~/Scripts/jquery-{version}.js"));

//            // Use the development version of Modernizr to develop with and learn from. Then, when you're
//            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
//            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
//                        "~/Scripts/modernizr-*"));

//            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
//                      "~/Scripts/bootstrap.js"));

//            bundles.Add(new ScriptBundle("~/bundles/respond").Include(
//                      "~/Scripts/respond.js"));

//            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
//                    "~/Scripts/Kendo/js/kendo.all.min.js"));

//            bundles.Add(new StyleBundle("~/Content/css").Include(
//                      "~/Content/bootstrap.css",
//                      "~/Content/site.css",
//                      "~/Content/Kendo/styles/kendo.common.min.css",
//                      "~/Content/Kendo/styles/kendo.rtl.min.css",
//                      "~/Content/Kendo/styles/kendo.default.min.css"));

//            //bundle OSUMC style, addition application styles can be bundled here 
//            bundles.Add(new StyleBundle("~/Content/osuwmc").Include(
//                                            "~/Content/themes/OSUWMC/OSUWMC.css"));

//            //bundle OSUMC JavaScript, addition application JavaScripts can be bundled here 
//            bundles.Add(new ScriptBundle("~/bundles/osuwmc").Include(
//                      "~/scripts/OSUWMC*"));
//        }
//    }
//}

﻿using System;
using System.Web.Mvc;
using System.IdentityModel.Services;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.AspNet.Identity;
using System.Configuration;
using TraineeTrackingSystem.Common;
using TraineeTrackingSystem.Web.Models;


namespace TraineeTrackingSystem.Web.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// MVN - All users with a MedCenterId will be able to login to the application but only see the 
        /// contents with the assigned roles from the application database 
        /// </summary>
        /// <param name="pageName"></param>
        /// <returns></returns>
       // [AuthorizeUser(AccessLevel = "IsDeleted")]
        public ActionResult UnAuthorized(string pageName)
        {
            ViewBag.pageName = pageName;        //allows specific name to display at the UX 
            return View();
        }
        public ActionResult Logout()
        {

            WSFederationAuthenticationModule authModule = FederatedAuthentication.WSFederationAuthenticationModule;

            //clear local cookie
            authModule.SignOut(false);

            //initiate federated sign out request to the STS
            SignOutRequestMessage signOutRequestMessage = new SignOutRequestMessage(new Uri(authModule.Issuer), authModule.Realm);
            String queryString = signOutRequestMessage.WriteQueryString();
            return new RedirectResult(queryString);
        }

        [OutputCache(Duration = 20, VaryByParam = "none")]
        //[Authorize]
        //[AuthorizeUser(AccessLevel = "Reader")]
        //[AuthorizeUser(AccessLevel = "Editor")]
        //[AuthorizeUser(AccessLevel = "Administrator")]
        //[AuthorizeUser(AccessLevel = "SuperUser")]
        public ActionResult TopNavagationBar()
        {
            string link = "";

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["TraineeTrackingSystemApiBase"]);

                var token = Security.CreateToken(User.Identity.GetUserName(), System.Web.HttpContext.Current.Request.UserHostAddress);

                //    token = "token=" + Server.UrlEncode(token);

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                var aw = client.GetAsync("api/TopNavigation?enviroment=" + ConfigurationManager.AppSettings["Enviroment"] + "&token=" + Server.UrlEncode(token) + "&userName="
                                + User.Identity.GetUserName()).Result;

                if (aw.IsSuccessStatusCode)
                {
                    //link = aw.Content.ReadAsAsync<string>().Result;
                    link = aw.Content.ReadAsStringAsync().Result;
                }

            }

            return Content(link);
        }
        
    }
}
﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.Controllers
{
    public class MenteePublicationsController : ApiController
    {
        private TTSEntities _db = new TTSEntities();

        // GET: api/_Publication
        public IQueryable<Publication> GetPublications()
        {
            return _db.Publications;
        }

        // GET: api/_Publications/5
        [ResponseType(typeof(Publication))]
        public IHttpActionResult GetPublication(int id)
        {
            Publication publication = _db.Publications.Find(id);
            if (publication == null)
            {
                return NotFound();
            }

            return Ok(publication);
        }

        // PUT: api/_Publications/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPublication(System.Guid id, Publication publication)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != publication.Id)
            {
                return BadRequest();
            }

            _db.Entry(publication).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PublicationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/_Publications
        [ResponseType(typeof(Publication))]
        public IHttpActionResult PostPublication(Publication publication)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _db.Publications.Add(publication);
            _db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = publication.Id }, publication);
        }

        // DELETE: api/_Publications/5
        [ResponseType(typeof(Publication))]
        public IHttpActionResult DeletePublication(int id)
        {
            Publication publication = _db.Publications.Find(id);
            if (publication == null)
            {
                return NotFound();
            }

            _db.Publications.Remove(publication);
            _db.SaveChanges();

            return Ok(publication);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PublicationExists(System.Guid id)
        {
            return _db.Publications.Count(e => e.Id == id) > 0;
        }
    }
}
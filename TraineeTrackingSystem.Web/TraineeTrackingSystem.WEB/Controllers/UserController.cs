﻿//using Kendo.Mvc.UI;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//using TraineeTrackingSystem.Data;
//using TraineeTrackingSystem.Repository;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using TraineeTrackingSystem.Common;
using TraineeTrackingSystem.Repository;
using TraineeTrackingSystem.Data;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using TraineeTrackingSystem.Web.Common;
using TraineeTrackingSystem.Web.PostModels;
using TraineeTrackingSystem.Web.ViewModels;
// added tracking date and user update
using System.Security.Claims;
using TraineeTrackingSystem.Web.Models;
using System.Net;


namespace TraineeTrackingSystem.Web.Controllers
{
    /// <summary>
    /// MVN - this will be used for User Management
    /// </summary>
    [Authorize]
    [AuthorizeUser(AccessLevel = "Administrator")] 
    public class UserController : Controller
    {
        private readonly CommonDataRepository commonRepository;
        private readonly UserRepository userRepository;
        //private readonly UserRolesRepository usrRoleRepository;
        private TTSEntities db = new TTSEntities();
        private readonly TTSEntities _context;
        string inValidationResult = "ERROR! Not Unique record. Not saved.";
        private readonly string _LastUpdateDATEFORMAT = "{0:M/dd/yyy hh:mm:tt}";
        /// mvn - build User Management constructor
        public UserController()
        {
            _context = new TTSEntities();
            this.userRepository = new UserRepository(_context);
            this.commonRepository = new CommonDataRepository(_context);
        }
        protected override void Dispose(bool disposing)
        {
            userRepository.Dispose();
            base.Dispose(disposing);
        }
        /// <summary>
        /// MVN - this will be used for the User management landing page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Index()
        {
            //return View();
            var usersIndex = userRepository.GetPersonRoleFromViewTbSearch_VwPersonRoles();
            //var usersIndex = userRepository.GetUserFromViewTbSearch_VwLogins();
            return View(usersIndex);
        }
        public ActionResult Users_Read([DataSourceRequest] DataSourceRequest request)
        {
            //return Json(userRepository.GetPersonRoleFromViewTbSearch_VwPersonRoles().ToDataSourceResult(request));
            //return Json(userRepository.GetUserFromViewTbSearch_VwLogins().ToDataSourceResult(request));
            return Json(userRepository.GetPersonRoleFromViewTbSearch_VwPersonRoles().ToDataSourceResult(request));
        }
        
        // GET: User/Create
        /// <summary>
        /// MVN - Update Intelecent Help info 4/27/2016
        /// This will be called when the UI Create Button is click, essentially load the view 
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            return View();
        }
       /// <summary>
       /// MVN - this handle HTTP request handler 
       /// </summary>
       /// <param name="inValidationRslt"></param>
       /// <returns></returns>
        protected ActionResult HandledBadRequestContentActionResult(string inValidationRslt)
        {
            return Content(inValidationRslt);
        }
        /////////// <summary>
        /////////// MVN - 5/4/2016 
        /////////// I am adding this for future expansion to get user info from AD, passing in user userName, firstName and LastName
        /////////// </summary>
        /////////// <param name="userMedID"></param>
        /////////// <param name="userFName"></param>
        /////////// <param name="userLName"></param>
        /////////// <returns></returns>
        ////////[OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        ////////public JsonResult GetUser(string userMedID, string userFName, string userLName)
        ////////{
        ////////    UserRolesRepository usrRoleRepository = new T32UserRolesRepository();
        ////////    if (userMedID == null || userMedID.ToLower().Equals("undefined") || userMedID.Trim().Length == 0) return Json("", JsonRequestBehavior.AllowGet);
        ////////    var domainUser = usrRoleRepository.GetMedCenterUser(userFName, userLName, userMedID);
        ////////    if (domainUser == null) return Json("", JsonRequestBehavior.AllowGet);

        ////////    //string medCenterId = "isla08";
        ////////    List<Login> w = new List<Login>();
        ////////    if (userMedID == null) return Json(w, JsonRequestBehavior.AllowGet);
        ////////    var result = usrRoleRepository.GetUserByMedCenterId(userMedID);
        ////////    return Json(result.ToString(a => a.DateLastUpdated), JsonRequestBehavior.AllowGet);
        ////////}

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult SearchingASOUsers(string partialName)
        {
            List<OSU.Medical.Entity.Employee> users = new List<OSU.Medical.Entity.Employee>();
            //UserRolesRepository usrRoleRepository = new T32UserRolesRepository();
            if (partialName.Trim().Length > 0)
            {
                var _identity = (System.Security.Claims.ClaimsPrincipal)(this.User);
                List<string> auth_roles = _identity.Claims.Where(c => c.Type == System.Security.Claims.ClaimTypes.Role).Select(c => c.Value).ToList();
                #region
                ///TODO HERE
                //users = usrRoleRepository.GetUsersListByPartialNameFromASOUser(partialName, auth_roles);
                #endregion
            }
            return Json(users, JsonRequestBehavior.AllowGet);
        }   

        [HttpGet]
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Edit(int loginId)
        {
            //var loginRecord = userRepository.GetLoginInfoFROMLoginTbl(loginId);
            var loginRecord = userRepository.GetPersonRoleFromViewTbSearch_VwPersonRolesById(loginId);
            var role = userRepository.GetRoleFromRoleTbl().ToList();
            var userVM = new UsersViewModel{
                LoginId = loginRecord.LoginId,
                PersonId = loginRecord.PersonId,
                LastName = loginRecord.LastName,
                MiddleName = loginRecord.MiddleName,
                FirstName = loginRecord.FirstName,
                Username = loginRecord.Username,
                IsActive = loginRecord.IsActive,
                Rolname = loginRecord.RoleName,
                RoleId = loginRecord.RoleId,//.HasValue ? loginRecord.RoleId.Value: null,
                Roles = role.Select(c => new KeyValuePair<int, string>(c.Id, c.RoleName)),

                StrDateLastUpdated = (loginRecord.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, loginRecord.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(loginRecord.LastUpdatedBy),
            };
            return View(new EditUserViewModel{
                UserViewModel = userVM
            });
        }
        /// <summary>
        /// MVN - Created 4/28/2016 
        /// This will be used for adding new user to the T32 login table, 
        /// when the UI AJAX function call by user click the Create button
        /// NOTE:  This feature only look in the T32 table and check username whether in the table or not
        /// assuming the Admin role already know the AD loginId.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult CreateLoginUser(UserPostModel model)
        {
            /// FIRST - SEARCH the login table to make sure
            /// the MedCenterId is not exist, if exist SEND HttpRequest handler
            /// IF NOT Create new login account in the LOGINS talbe      
            Login newLoginAccount = null;
            if (model.UserName != null){
                if (!(_context.Set<Login>().Any(u => u.Username == model.UserName))){
                    Login newLogin = userRepository.AddNewLoginAcct(model.LastName, model.FirstName, model.MiddleName, model.UserName, (ClaimsPrincipal)User);
                    newLoginAccount = newLogin;
                }else{
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return HandledBadRequestContentActionResult(inValidationResult);
                }
            }
            return Json(newLoginAccount.LoginId);
        }
        public ActionResult UpdateUser(UserPostModel model)
        {
            /// Ensure user exsists in the personRole table
            bool isUpdate = model.LoginId.Equals(null) || !_context.Set<Login>()
                .Any(o => o.LoginId == model.LoginId);

            var userLoginRec = isUpdate
                ? new Login()
                : _context.Set<Login>()
                .Single(p => p.LoginId == model.LoginId);


            userLoginRec.Person.LastName = model.LastName;
            userLoginRec.Person.FirstName = model.FirstName;
            userLoginRec.Person.MiddleName = model.MiddleName;
            userLoginRec.Username = model.UserName;
            userLoginRec.IsActive = model.IsActive;

            userLoginRec.DateLastUpdated = DateTime.Now;
            userLoginRec.LastUpdatedBy = (((ClaimsPrincipal)User)
                    .Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
            _context.SaveChanges();

            var recordNotFound = model.LoginId.Equals(0) ||
                                !_context.Set<PersonRole>().Any(pR => pR.LoginId == model.LoginId);
            var newPRInfo = recordNotFound
                       ? new PersonRole() : _context.Set<PersonRole>()//.Find(userLoginRec.PersonRoles.Where(p => p.LoginId == model.LoginId).FirstOrDefault().RoleId);
                       .FirstOrDefault(p => p.LoginId == model.LoginId);

            if (model.RoleId != 0){           
                ///CREATE PersonRole Record
                if (recordNotFound){
                    if (newPRInfo.LoginId == 0 && newPRInfo.RoleId == 0){
                        newPRInfo.LoginId = model.LoginId;
                        newPRInfo.RoleId = model.RoleId;
                        newPRInfo.IsActive = true;
                        newPRInfo.DateAssigned = DateTime.Now;
                        newPRInfo.DateLastUpdated = DateTime.Now;
                        newPRInfo.LastUpdatedBy = (((ClaimsPrincipal)User)
                            .Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
                        newPRInfo.AssignedBy = (((ClaimsPrincipal)User)
                            .Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
                        _context.Set<PersonRole>().Add(newPRInfo);
                        _context.SaveChanges();
                    }                        
                }else{ ///PersonRoleRecord Found 
                    if (newPRInfo.Login.PersonRoles.Count > 0){
                        /// FIRST - remove what found in the PersonRole table 
                        if (model.RoleId != newPRInfo.RoleId){
                            newPRInfo.IsActive = false;
                            newPRInfo.RemovedBy = (((ClaimsPrincipal)User)
                           .Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
                            _context.Set<PersonRole>().Remove(newPRInfo);
                            _context.SaveChanges();
                        }
                        
                        /// SECOND - then add the UI dropdown selection back in to the PersonRole talbe, 
                        /// Recording the following: DateRemove, RemovedBy, DateLastUpdated 
                        /// and LastUpdatedBy should be collected again
                        if (model.RoleId != newPRInfo.RoleId){
                            newPRInfo.LoginId = model.LoginId;
                            newPRInfo.RoleId = model.RoleId;
                            newPRInfo.IsActive = true;
                            newPRInfo.DateRemoved = DateTime.Now;
                            newPRInfo.DateLastUpdated = DateTime.Now;
                            newPRInfo.LastUpdatedBy = (((ClaimsPrincipal)User)
                                .Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
                            newPRInfo.AssignedBy = (((ClaimsPrincipal)User)
                                .Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
                            _context.Set<PersonRole>().Add(newPRInfo);
                            _context.SaveChanges();
                        }
                        ///Otherwise do nothing....
                    }
                }
            }
            
            return Json(true);

        }
        private Login GetLoginInfo(int loginId)
        {
            var query = from loginInfo in _context.Logins
                        where loginInfo.LoginId == loginId
                        select loginInfo;
            return query.Single();
        }
        private PersonRole GetUser(int loginId)
        {
            var query = from u in _context.PersonRoles
                        where u.LoginId == loginId
                        select u;
            return query.Single();
        }
    }
}

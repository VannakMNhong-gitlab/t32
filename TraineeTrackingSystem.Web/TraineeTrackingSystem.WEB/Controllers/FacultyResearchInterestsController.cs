﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.Controllers
{
    public class FacultyResearchInterestsController : ApiController
    {
        private TTSEntities db = new TTSEntities();

        // GET: api/_FacultyResearchInterests
        public IQueryable<FacultyResearchInterest> GetFacultyResearchInterests()
        {
            return db.FacultyResearchInterests;
        }

        // GET: api/_FacultyResearchInterests/5
        [ResponseType(typeof(FacultyResearchInterest))]
        public IHttpActionResult GetFacultyResearchInterest(int id)
        {
            FacultyResearchInterest facultyResearchInterest = db.FacultyResearchInterests.Find(id);
            if (facultyResearchInterest == null)
            {
                return NotFound();
            }

            return Ok(facultyResearchInterest);
        }

        // PUT: api/_FacultyResearchInterests/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFacultyResearchInterest(int id, FacultyResearchInterest facultyResearchInterest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != facultyResearchInterest.Id)
            {
                return BadRequest();
            }

            db.Entry(facultyResearchInterest).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FacultyResearchInterestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/_FacultyResearchInterests
        [ResponseType(typeof(FacultyResearchInterest))]
        public IHttpActionResult PostFacultyResearchInterest(FacultyResearchInterest facultyResearchInterest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.FacultyResearchInterests.Add(facultyResearchInterest);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = facultyResearchInterest.Id }, facultyResearchInterest);
        }

        // DELETE: api/_FacultyResearchInterests/5
        [ResponseType(typeof(FacultyResearchInterest))]
        public IHttpActionResult DeleteFacultyResearchInterest(int id)
        {
            FacultyResearchInterest facultyResearchInterest = db.FacultyResearchInterests.Find(id);
            if (facultyResearchInterest == null)
            {
                return NotFound();
            }

            db.FacultyResearchInterests.Remove(facultyResearchInterest);
            db.SaveChanges();

            return Ok(facultyResearchInterest);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FacultyResearchInterestExists(int id)
        {
            return db.FacultyResearchInterests.Count(e => e.Id == id) > 0;
        }
    }
}
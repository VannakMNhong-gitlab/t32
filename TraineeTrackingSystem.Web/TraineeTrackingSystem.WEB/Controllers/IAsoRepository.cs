﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TraineeTrackingSystem.Web.Models;
using System.Threading.Tasks;

namespace TraineeTrackingSystem.Web.Controllers
{
    public interface IAsoRepository : IDisposable
    {
        User GetUserByMedCenterId(string medCenterId);
    }
}

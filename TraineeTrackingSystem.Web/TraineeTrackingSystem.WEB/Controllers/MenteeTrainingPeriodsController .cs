﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.Controllers
{
    public class MenteeTrainingPeriodsController : ApiController
    {
        private TTSEntities _db = new TTSEntities();

        // GET: api/_TrainingPeriod
        public IQueryable<TrainingPeriod> GetTrainingPeriods()
        {
            return _db.TrainingPeriods;
        }

        // GET: api/_Publications/5
        [ResponseType(typeof(TrainingPeriod))]
        public IHttpActionResult GetTrainingPeriod(int id)
        {
            TrainingPeriod trainingPeriod = _db.TrainingPeriods.Find(id);
            if (trainingPeriod == null)
            {
                return NotFound();
            }

            return Ok(trainingPeriod);
        }

        // PUT: api/_TrainingPeriods/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTrainingPeriod(System.Guid id, TrainingPeriod trainingPeriod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != trainingPeriod.Id)
            {
                return BadRequest();
            }

            _db.Entry(trainingPeriod).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TrainingPeriodExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/_TrainingPeriods
        [ResponseType(typeof(TrainingPeriod))]
        public IHttpActionResult PostTrainingPeriod(TrainingPeriod trainingPeriod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _db.TrainingPeriods.Add(trainingPeriod);
            _db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = trainingPeriod.Id }, trainingPeriod);
        }

        // DELETE: api/_TrainingPeriods/5
        [ResponseType(typeof(TrainingPeriod))]
        public IHttpActionResult DeleteTrainingPeriod(int id)
        {
            TrainingPeriod trainingPeriod = _db.TrainingPeriods.Find(id);
            if (trainingPeriod == null)
            {
                return NotFound();
            }

            _db.TrainingPeriods.Remove(trainingPeriod);
            _db.SaveChanges();

            return Ok(trainingPeriod);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TrainingPeriodExists(System.Guid id)
        {
            return _db.TrainingPeriods.Count(e => e.Id == id) > 0;
        }
    }
}
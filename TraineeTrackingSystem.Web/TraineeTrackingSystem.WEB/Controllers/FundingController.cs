﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using TraineeTrackingSystem.Common;
using TraineeTrackingSystem.Repository;
using TraineeTrackingSystem.Data;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using TraineeTrackingSystem.Web.PostModels;
using TraineeTrackingSystem.Web.ViewModels;
using TraineeTrackingSystem.Web.Common;
// added tracking date and user update
using System.Security.Claims;
using System.Net;
using System.Net.Mime;
using TraineeTrackingSystem.Web.Models;


namespace TraineeTrackingSystem.Web.Controllers
{
    [AuthorizeUser(AccessLevel = "Editor")]
    //[AuthorizeUser(MultipleRoles = "Administor, SuperUser,Editor")]
    public class FundingController : Controller
    {
        private readonly FundingRepository fundingRepository;
        private readonly CommonDataRepository commonRepository;
        private readonly TTSEntities _context;
        private readonly string _DATEFORMAT = "{0:yyyy-MM-dd}";     //"{0:MM/dd/yyyy}";
        
        //This globle variable will be used to convert date to specific format 
        private readonly string _LastUpdateDATEFORMAT = "{0:M/dd/yyy hh:mm:tt}";
        public FundingController()
        {
            _context = new TTSEntities();
            this.fundingRepository = new FundingRepository(_context);
            this.commonRepository = new CommonDataRepository(_context);
        }
        protected override void Dispose(bool disposing)
        {
            fundingRepository.Dispose();
            commonRepository.Dispose();
            base.Dispose(disposing);
        }
        [HttpGet]
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        //[AuthorizeUser(AccessLevel = "SuperUser, Administrator, Editor")]
        public ActionResult Index()
        {
            var fundingDemographics = fundingRepository.GET_Search_VwFundingDemographics();

            return View(fundingDemographics);
        }
        
        public virtual JsonResult Fundings_Read([DataSourceRequest] DataSourceRequest GET_request)
        {
            return Json(fundingRepository.GET_Search_VwFundingDemographics().ToDataSourceResult(GET_request));
        }

        public ActionResult AddFunding()
        {
            return View();
        }

        // GET: /Funding/Create
        public ActionResult Create()
        {
            var states = _context.StateProvinces.ToList();
            var countries = _context.Countries.ToList();
            var getFacultyFullname = commonRepository.GetFacultyList().ToList();
            var fundingTypes = commonRepository.GetFundingTypeList().ToList();
            var fundingViewModel = new FundingsViewModel
            { 
                FundingFacultiesList = getFacultyFullname.Where(f => f.IsActive == true && f.FullName != null).Select(f => new KeyValuePair<Guid?, string>(f.Id, f.FullName)),
                States = states.Select(s => new KeyValuePair<int, string>(s.Id, s.Abbreviation)),
                Countries = countries.Select(c => new KeyValuePair<int, string>(c.Id, c.Name)),
                FundingTypes = fundingTypes.Select(ft => new KeyValuePair<int, string>(ft.Id, ft.Name))
            };
            return View(new EditFundingViewModel{
                FundingInformations = fundingViewModel
            });
        }

        /// <summary>
        /// MVN - This private method is to provide the hiding fields in more elegant way and much more easily to maintaining.  
        /// If there is logic changed, we can just update this method, as appose to multiple places.  
        /// </summary>
        /// <param name="fdStatusId"></param>
        /// <param name="fundingId"></param>
        private void HideFields(int? fdStatusId, Guid fundingId)
        {
            var currFunding = fundingRepository.GetFundingbyId(fundingId);

            var currentStatus = currFunding.FundingStatusId;
            // hide if Status is Not Active or Completed
            ViewBag.IsNotPending = !((currentStatus < 2) || (currentStatus == 2)) ? true : false;
            ViewBag.IsAwardInfoHidden = !((currentStatus == 4) || (currentStatus == 5)) ? true : false;
            ViewBag.IsCostInfoHidden = !((currentStatus == 4) || (currentStatus == 5)) ? true : false;

            var currentType = currFunding.FundingTypeId;
            /// MVN **********************************************************
            /// Modified Date: 9/8/2015 - Hide the participating Faculty and their role, 
            /// if the Program Project => 1 is selected
            /// **************************************************************
            ViewBag.LastUpdated = (currFunding.DateLastUpdated.HasValue == false) ? true : false;
            ViewBag.PrimeLastUpdated = (currFunding.DateLastUpdated.HasValue == false) ? true : false;
            ViewBag.AwardLastUpdated = (currFunding.DateLastUpdated.HasValue == false) ? true : false;
            ViewBag.IsParticipatingHidden = (currentType == 1) ? true : false;
            ViewBag.IsProgramProjectHidden = (currentType != 1) ? true : false;
            ViewBag.IsPrimeInfoHidden = (currentType != 3) ? true : false;
        }
        [HttpGet]
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Edit(Guid id)
        {
            var currFunding = fundingRepository.GetFundingbyId(id);
            var fundingStatusTypes = fundingRepository.GetFundingStatus().ToList();
            var states = _context.StateProvinces.ToList();
            var countries = _context.Countries.ToList();
            var fundingTypes = commonRepository.GetFundingTypeList().ToList();
            var fullFacultyPDPIList = commonRepository.GetFacultyList().ToList();
            var getFacultyFullname = commonRepository.GetFacultyList().ToList();
            //var sponsors = commonRepository.GetSponsorList().ToList();

            
            var roles = _context.FundingRoles.ToList();

            var budgetPeriodStatus = fundingRepository.GetBudgetPeriodStatus().ToList();
            /* ------------------------- MVN -------------------------------------------------------- */
            /// MVN - Note, compare the Search_VwFacultyDemographics (dropdown lsit) Id 
            /// to => FundingFaculty (Grid) FacultyId, If found, exclude them.
            var gridFftyList = currFunding.FundingFaculties.Where(f => f.IsDeleted == false);
            getFacultyFullname.RemoveAll(x => gridFftyList.Any(y => y.FacultyId == x.Id));
            /* -------------------------------------------------------------------------------------- */
           
            var funding = commonRepository.GetFDLastUpdateInfo(currFunding.Id);
            /// *************************************************
            /// General information Edit_View
            /// 1/27/2015
            /// *************************************************
            var fundingGeneralViewModel = new FundingViewModel
            {
                FundingId = id,    
                Title = currFunding.Title,
                SponsorId = currFunding.SponsorId,
                SponsorName = currFunding.Institution_SponsorId != null
                        ? currFunding.Institution_SponsorId.Name
                        : string.Empty,

                SponsorReferenceNumber = currFunding.SponsorReferenceNumber,
                FundingStatusId = (currFunding.FundingStatusId == 1) ? 0 : currFunding.FundingStatusId,
                FundingStatusName = fundingStatusTypes.Where(f => f.Id != 1).Select(f => new KeyValuePair<int, string>(f.Id, f.Name)),
                
                States = states.Select(s => new KeyValuePair<int, string>(s.Id, s.Abbreviation)),
                Countries = countries.Select(c => new KeyValuePair<int, string>(c.Id, c.Name)),

                GrtNumber = currFunding.GrtNumber,

                FundingTypeId = currFunding.FundingTypeId.HasValue ? currFunding.FundingTypeId.Value : 0,
                FundingTypes = fundingTypes.Select(ft => new KeyValuePair<int, string>(ft.Id, ft.Name)),
                DateProjectedStart = currFunding.DateProjectedStart,
                DateProjectedEnd = currFunding.DateProjectedEnd,
                StrDateLastUpdated = (currFunding.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, currFunding.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(currFunding.LastUpdatedBy)
            };

            /// ****************************************************************
            /// Participating Faculty and Their Roles Edit_View (Grid)
            /// 1/27/2015
            /// ****************************************************************
            var facultySupportView = new FacultySupportsViewModel
            {
                /// MVN - commented this out, for consistency, use bellow code instead 
                //FundingId = id,  //Funding =>Id
                FundingId = currFunding.Id,
                /// ************************************************************
                /// Add Faculty Information dropdown List - lists all faculties
                /// 1/27/2015
                /// 5/18/2015 - fundingFacultiesList need to exclude the faculty that already selected in the grid
                /// IMPORTANT NOTE:  FacultyId from FundingFaculty map to Id in Search_VwFacultyDemographics table  
                /// ************************************************************
                FundingFacultiesList = getFacultyFullname.Select(f => new KeyValuePair<Guid?, string>(f.Id, f.FullName)),  
                RoleList = roles.Where(r => r.IsTrainingGrantRole == false).Select(r => new KeyValuePair<int, string>(r.Id, r.Name)),
                
                /// MVN - Participating Faculty and their roles - the Grid list
                FacultySupportGridView = currFunding.FundingFaculties.Where(f => f.IsDeleted == false)
                                            .Select(facutySupport => new FacultySupportViewModel{
                                                Id = facutySupport.Id, 
                                                FacultyId = facutySupport.FacultyId,
                                                FacultyFullName = commonRepository.GetPersonFullName(facutySupport.Faculty.PersonId),
                                                FacultyRoleId = facutySupport.PrimaryRoleId,
                                                FacultyRole = facutySupport.FundingRole_PrimaryRoleId.Name,                                       
                                                DateLastUpdated = facutySupport.DateLastUpdated,
                                                LastUpdatedBy = (facutySupport.LastUpdatedBy != null) ? facutySupport.LastUpdatedBy : string.Empty,                                         
                                                StrDateLastUpdated = (facutySupport.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, facutySupport.DateLastUpdated) : string.Empty,                      
                                                StrLastUpdatedBy = (commonRepository.GetLoginInfo(facutySupport.LastUpdatedBy) != null) ? commonRepository.GetLoginInfo(facutySupport.LastUpdatedBy) : string.Empty                                                                      
                                            })
            };
            var programProjectView = new FundingProgramProjectsViewModel
            {
                FundingId = id,
                ProgramDirectorId = currFunding.ProgramProjectPdId != null ? currFunding.ProgramProjectPdId : Guid.Empty,
                ProgramDirectors = fullFacultyPDPIList.Select(f => new KeyValuePair<Guid, string>(f.Id, f.FullName)),
                ProgramProjects = commonRepository.GetSearch_VwFundingProgramProjectData(currFunding)
                    .Select(f => new FundingProgramProjectViewModel
                    {
                        ProjectId = f.Id,
                        PIFullName = f.PiFullName,
                        Title = f.Title,
                        DateStartedText = ((f.FundingStatusId == 4) || (f.FundingStatusId == 5)) ? String.Format(_DATEFORMAT,f.DateTimeFundingStarted) 
                                : String.Format(_DATEFORMAT,f.DateProjectedStart),
                        DateEndedText = ((f.FundingStatusId == 4) || (f.FundingStatusId == 5)) ? String.Format(_DATEFORMAT, f.DateTimeFundingEnded)
                                : String.Format(_DATEFORMAT, f.DateProjectedEnd),
                        DateLastUpdated = f.DateLastUpdated,
                        LastUpdatedBy = (f.LastUpdatedBy != null) ? f.LastUpdatedBy : string.Empty,                                         
                        StrDateLastUpdated = (f.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, f.DateLastUpdated) : string.Empty,                      
                        StrLastUpdatedBy = (commonRepository.GetLoginInfo(f.LastUpdatedBy) != null) ? commonRepository.GetLoginInfo(f.LastUpdatedBy) : string.Empty                                               
                    })
            };
            var subAwardView = new FundingSubAwardViewModel
            {
                Id = id,
                PrimeAwardNumber = currFunding.PrimeAward,
                PrimeSponsorId = currFunding.PrimeSponsorId,
                PrimeSponsorName = currFunding.Institution_PrimeSponsorId != null
                        ? currFunding.Institution_PrimeSponsorId.Name
                        : string.Empty,
                PrimeAwardPIId = currFunding.PrimeAwardPiId != null ? currFunding.PrimeAwardPiId : Guid.Empty,
                PrimaryInvestigators = fullFacultyPDPIList.Select(f => new KeyValuePair<Guid, string>(f.Id, f.FullName)),
                StrDateLastUpdated = (currFunding.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, currFunding.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(currFunding.LastUpdatedBy)
            };
            var awardInfoView = new AwardInformationViewModel
            {
                FundingId = id,
                SponsorAwardNumber = currFunding.SponsorAwardNumber,
                DateStarted = currFunding.DateStarted,
                DateEnded = currFunding.DateEnded,

                StrDateLastUpdated = (currFunding.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, currFunding.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(currFunding.LastUpdatedBy)
            };
            //var fddCost = commonRepository.GetFDDCostLastUpdateInfo(currFunding.Id);
            //var status = commonRepository.GetFDDCostsList().ToList();
            var costInfoView = new CostsInformationViewModel
            {
                FundingId = id,
                // read from the data model 
                
                BudgetPeriodStatuses = budgetPeriodStatus.Select(f => new KeyValuePair<int, string>(f.Id, f.Name)),
                CostInformationGridView = currFunding.FundingDirectCosts.Where(c => c.IsDeleted == false)
                                                                      .Select(cost => new CostInformationViewModel{
                    Id = cost.Id,                //setting the PK FundingDirectCost Id
                    DateStarted = cost.DateStarted,     // for the grid row selection                                           
                    DateStartedText = String.Format(_DATEFORMAT, cost.DateStarted),
                    DateEnded = cost.DateEnded,         // for the grid row selection
                    DateEndedText = String.Format(_DATEFORMAT, cost.DateEnded),
                    CurrentYearDirectCosts = cost.CurrentYearDirectCosts,

                    TotalDirectCosts = cost.TotalDirectCosts,
                    /// MVN - Added codes bellow so it will not blow up when BudgetPeriodStatus is null
                    BudgetPeriodStatusId = cost.BudgetPeriodStatusId,
                    BudgetPeriodStatus = (cost.BudgetPeriodStatu != null) ? cost.BudgetPeriodStatu.Name : string.Empty,
                    DateLastUpdated = cost.DateLastUpdated,
                 
                    StrDateLastUpdated = (cost.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, cost.DateLastUpdated) : string.Empty,
                    LastUpdatedBy = (cost.LastUpdatedBy != null) ? cost.LastUpdatedBy : string.Empty,                                                                       
                    StrLastUpdatedBy = (commonRepository.GetLoginInfo(cost.LastUpdatedBy) != null) ? commonRepository.GetLoginInfo(cost.LastUpdatedBy) : string.Empty
                                                                      
                })

            };

            HideFields(currFunding.FundingStatusId, id);
            return View(new EditFundingViewModel
            {
                FundingGeneralViewModel = fundingGeneralViewModel,
                FacultySupportGridView = facultySupportView,
                ProgramProjectsGridView = programProjectView,
                SubAwardGridView = subAwardView,
                AwardInformationView = awardInfoView,
                CostInformationGridView = costInfoView
            });
        }

        public ActionResult UpdateFacultySupport(FacultySupportPostModel updateFacultySupportModel)
        {
            bool isNew = updateFacultySupportModel.Id.Equals(Guid.Empty) || !_context.Set<FundingFaculty>()
                       .Any(ffty => ffty.Id == updateFacultySupportModel.Id);

            var newFFtySupport = isNew
                        ? new FundingFaculty()
                        : _context.Set<FundingFaculty>()
                        .Single(ffty => ffty.Id == updateFacultySupportModel.Id);

            newFFtySupport.FundingId = updateFacultySupportModel.FundingId;
            newFFtySupport.FacultyId = updateFacultySupportModel.FacultyId;
            newFFtySupport.PrimaryRoleId = updateFacultySupportModel.FacultyRoleId;

            newFFtySupport.DateLastUpdated = DateTime.Now;
            newFFtySupport.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);

            if (isNew){
                _context.Set<FundingFaculty>().Add(newFFtySupport);
            }
            _context.SaveChanges();
            /* ---------------------------- dynamically handles the UI grid and drop down list ---------------------- */
            var currFunding = fundingRepository.GetFundingbyId(updateFacultySupportModel.FundingId);      
            var facultyDrpdwnList = commonRepository.GetFacultyList().ToList();
            var roles = _context.FundingRoles.ToList();

            var facPersonId = commonRepository.GetPersonIdForFacultyId(newFFtySupport.FacultyId);
            var facultyGridList = currFunding.FundingFaculties.Where(f => f.IsDeleted == false);

            facultyDrpdwnList.RemoveAll(x => facultyGridList.Any(y => y.FacultyId == x.Id));
            /* ------------------------------------------------------------------------------------------------------ */         
            var updateFtySupport = new FacultySupportViewModel{
                Id = newFFtySupport.Id,
                FundingId = newFFtySupport.FundingId,
                FacultyId = newFFtySupport.FacultyId,
                FacultyFullName = updateFacultySupportModel.FacultyFullName,
                FacultyRoleId = newFFtySupport.PrimaryRoleId,
                FacultyRole = newFFtySupport.FundingRole_PrimaryRoleId.Name,
                StrDateLastUpdated = (newFFtySupport.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, newFFtySupport.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(newFFtySupport.LastUpdatedBy),
                FundingFacultiesList = facultyDrpdwnList.Select(f => new KeyValuePair<Guid?, string>(f.Id, f.FullName)),
                RoleList = roles.Where(r => r.IsTrainingGrantRole == false).Select(r => new KeyValuePair<int, string>(r.Id, r.Name))
            };          
            return new JsonCamelCaseResult(updateFtySupport, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AjaxRequestForAddFtyBtn(FacultySupportPostModel model)
        {
            var currFunding = fundingRepository.GetFundingbyId(model.FundingId);
            var getFacultyFullname = commonRepository.GetFacultyList().ToList();
            var gridFftyList = currFunding.FundingFaculties.Where(f => f.IsDeleted == false);
            getFacultyFullname.RemoveAll(x => gridFftyList.Any(y => y.FacultyId == x.Id));
            var fundingFacultiesList = getFacultyFullname.Select(f => new KeyValuePair<Guid?, string>(f.Id, f.FullName));
            return new JsonCamelCaseResult(fundingFacultiesList, JsonRequestBehavior.AllowGet);  
        }

        /// <summary>
        /// MVN - THIS post back from the UI when the grid row selection is selected
        /// PURPOSE:  Essentially, this code helps maintaining the two lists: Grid and Dropdown list, dynamically
        /// The value from the grid selection will be added back to the faculty dropdown list at the UI
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult AddBackToDropDownListFacultySupport(FacultySupportPostModel model)
        {       
            var currFunding = fundingRepository.GetFundingbyId(model.FundingId);
            var facultyDrpdwnList = commonRepository.GetFacultyList().ToList();
            var roles = _context.FundingRoles.ToList();
            var facPersonId = commonRepository.GetPersonIdForFacultyId(model.FacultyId);
            var facultyGridList = currFunding.FundingFaculties.Where(f => f.IsDeleted == false);
            facultyDrpdwnList.RemoveAll(x => facultyGridList.Any(y => y.FacultyId == x.Id));
            bool isSelected = model.Id.Equals(Guid.Empty) || !_context.Set<FundingFaculty>()
                      .Any(ffty => ffty.Id == model.Id);

            var newFFtySupport = isSelected
                        ? new FundingFaculty()
                        : _context.Set<FundingFaculty>()
                        .Single(ffty => ffty.Id == model.Id);

            /* ------------------------------------------------------------------------------------------------------ */
            var addFtySupportBktoDDL = new FacultySupportViewModel{
                Id = newFFtySupport.Id,
                FundingId = newFFtySupport.FundingId,
                FacultyId = newFFtySupport.FacultyId,
                FacultyFullName = model.FacultyFullName,
                FacultyRoleId = newFFtySupport.PrimaryRoleId,
                FacultyRole = newFFtySupport.FundingRole_PrimaryRoleId.Name,
                FundingFacultiesList = facultyDrpdwnList.Select(f => new KeyValuePair<Guid?, string>(f.Id, f.FullName))
            };

            return new JsonCamelCaseResult(addFtySupportBktoDDL, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// MVN ->REMOVE Row from the Grid
        /// Funding -> in the Participating Faculty and Their Roles section, check to make sure at least one PI associated with Funding
        /// This method is utilizing the JavaScript notification with Toastr API
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RemoveFtySupport(Guid id)
        {
            var facultySupport = _context.Set<FundingFaculty>().Find(id);
            //var facultyDpdwnList = commonRepository.GetFacultyList().ToList();
            if (facultySupport.PrimaryRoleId == 1){
                var safeToDelete = _context.FundingFaculties.Where(f => f.PrimaryRoleId == 1 && f.FundingId == facultySupport.FundingId && f.IsDeleted == false)
                                                        .Count() > 1 ? true : false;
                if (safeToDelete){
                    facultySupport.IsDeleted = true;
                }else{
                    // Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    //THIS => essentially return back to the UI, utilizing JavaScript libraries toastr.error(data.responseText) 
                    return Content("false");
                }
            }else{
                facultySupport.IsDeleted = true;
            }

            _context.SaveChanges();  
            return Content("true");
        }

        public ActionResult SaveUpdateProgramProjectInfo(ProgramProjectPostModel programProjectInfo)
        {
            var updateProgramProject = _context.Set<Funding>()
                .FirstOrDefault(fund => fund.Id == programProjectInfo.Id);

            updateProgramProject.ProgramProjectPdId = programProjectInfo.ProgramProjectPdId;

            updateProgramProject.DateLastUpdated = DateTime.Now;
            updateProgramProject.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
            _context.SaveChanges();

            return Json(true);
        }

        public ActionResult SaveUpdateSubAwardInfo(SubAwardPostModel subAwardInfo)
        {
            var updateSubAward = _context.Set<Funding>()
                .FirstOrDefault(fund => fund.Id == subAwardInfo.Id);

            updateSubAward.PrimeAward = subAwardInfo.PrimeAwardNumber;
            Institution primeSponsorName = null;
            if (_context.Set<Institution>().Any(i => i.Name == subAwardInfo.PrimeSponsorName))
            {
                primeSponsorName = _context.Set<Institution>().Single(i => i.Name == subAwardInfo.PrimeSponsorName);
                updateSubAward.PrimeSponsorId = primeSponsorName.Id;
            } 
            updateSubAward.PrimeAwardPiId = subAwardInfo.PrimeAwardPIId;

            updateSubAward.DateLastUpdated = DateTime.Now;
            updateSubAward.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
            _context.SaveChanges();

            return Json(true);
        }

        public ActionResult SaveUpdateAwardInfo(AwardInformationPostModel saveUpdateAwardInfo)
        {
            var saveUpdateFtyCostInfo = _context.Set<Funding>()
                .FirstOrDefault(fund => fund.Id == saveUpdateAwardInfo.FundingId);
            saveUpdateFtyCostInfo.SponsorAwardNumber = saveUpdateAwardInfo.SponsorAwardNumber;
            saveUpdateFtyCostInfo.DateStarted = saveUpdateAwardInfo.DateStarted;
            saveUpdateFtyCostInfo.DateEnded = saveUpdateAwardInfo.DateEnded;
            //MVN - added Date and user activity tracking ....
            saveUpdateFtyCostInfo.DateLastUpdated = DateTime.Now;
            saveUpdateFtyCostInfo.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
            _context.SaveChanges();

            return Json(true);
        }
        public ActionResult SaveCostInformation(CostInformationPostModel saveUpdateCostInfoModel)
        {
            bool isNew = !saveUpdateCostInfoModel.Id.HasValue || !_context.Set<FundingDirectCost>()
                       .Any(fdcst => fdcst.Id == saveUpdateCostInfoModel.Id);        //Compare the Id in PostModel to FundingDirectCost

            var newFdCstInfo = isNew
                        ? new FundingDirectCost() : _context.Set<FundingDirectCost>().Find(saveUpdateCostInfoModel.Id);

            newFdCstInfo.FundingId = saveUpdateCostInfoModel.FundingId;
            newFdCstInfo.DateStarted = saveUpdateCostInfoModel.DateStarted;
            newFdCstInfo.DateEnded = saveUpdateCostInfoModel.DateEnded;
            newFdCstInfo.CurrentYearDirectCosts = saveUpdateCostInfoModel.CurrentYearDirectCosts;
            newFdCstInfo.TotalDirectCosts = saveUpdateCostInfoModel.TotalDirectCosts;

            DateTime setUpdateDate = DateTime.Now;
            newFdCstInfo.DateLastUpdated = DateTime.Now;
            newFdCstInfo.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);

           
            /// MVN - first check BudgetPeriodStatusId sent from the UI
            if (saveUpdateCostInfoModel.BudgetPeriodStatusId == 1){
                var FundingDC = commonRepository.GetFDDCostsList(newFdCstInfo.FundingId);
                foreach (var result in FundingDC){
                    /// Then from the server, check and count the budgetPeriodStatusId that is 1
                    var countResult = commonRepository.GetFDDCostsList(newFdCstInfo.FundingId)
                            .Where(b => b.BudgetPeriodStatusId == 1).Count() > 0 ? true : false;
                    if (countResult == true){
                        result.BudgetPeriodStatusId = 2;
                    }else{
                        //set to the value that was sent by the UI
                        newFdCstInfo.BudgetPeriodStatusId = saveUpdateCostInfoModel.BudgetPeriodStatusId;
                    }

                }
                newFdCstInfo.BudgetPeriodStatusId = saveUpdateCostInfoModel.BudgetPeriodStatusId;
            }
            
            
            // MVN - This code handles if there is nothing selected from the UI dropdownlist
            if (saveUpdateCostInfoModel.BudgetPeriodStatusId == 0){
                saveUpdateCostInfoModel.BudgetPeriodStatusId = newFdCstInfo.BudgetPeriodStatusId;
            }

            /// MVN - This will handle Add New Direct Costs
            if (isNew){
                newFdCstInfo.BudgetPeriodStatusId = saveUpdateCostInfoModel.BudgetPeriodStatusId;
                _context.Set<FundingDirectCost>().Add(newFdCstInfo);                
            }
            _context.SaveChanges();
            
            var budgetPeriodStatus = fundingRepository.GetBudgetPeriodStatus().ToList();
            var currFunding = _context.Fundings.FirstOrDefault(f => f.Id == saveUpdateCostInfoModel.FundingId);
            /// MVN Grid Refresh....
            var costInfoView = new CostsInformationViewModel{
                FundingId = saveUpdateCostInfoModel.FundingId,               
                BudgetPeriodStatuses = budgetPeriodStatus.Select(f => new KeyValuePair<int, string>(f.Id, f.Name)),
                CostInformationGridView = currFunding.FundingDirectCosts.Where(c => c.IsDeleted == false)
                                                                      .Select(cost => new CostInformationViewModel{
                                                                          Id = cost.Id,
                                                                          FundingId = cost.FundingId,
                                                                          DateStarted = cost.DateStarted,
                                                                          DateStartedText = String.Format(_DATEFORMAT, cost.DateStarted),
                                                                          DateEnded = cost.DateEnded,
                                                                          DateEndedText = String.Format(_DATEFORMAT, cost.DateEnded),
                                                                          CurrentYearDirectCosts = cost.CurrentYearDirectCosts,                                                                       
                                                                          BudgetPeriodStatusId = cost.BudgetPeriodStatusId,
                                                                          BudgetPeriodStatus = (cost.BudgetPeriodStatu != null)? cost.BudgetPeriodStatu.Name : string.Empty,
                                                                          TotalDirectCosts = cost.TotalDirectCosts,                                                                       
                                                                          StrDateLastUpdated = (cost.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, cost.DateLastUpdated) : string.Empty,                                                                         
                                                                          StrLastUpdatedBy = commonRepository.GetLoginInfo(cost.LastUpdatedBy)
                                                                      })

            };
            //_context.SaveChanges();
            //THIS => Essentially refresh the UI Grid
            return new JsonCamelCaseResult(costInfoView, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult UpdateGeneralInformation(GeneralFundingPostModel updateFundingModel)
        {
            //var currFunding = _context.Fundings.ToList();            
            var fundingStatusTypes = fundingRepository.GetFundingStatus().ToList();            
            var updateFunding = new Funding();
            updateFunding.Id = updateFundingModel.FundingId;
            updateFunding.Title = updateFundingModel.Title;
            updateFunding.SponsorReferenceNumber = updateFundingModel.SponsorReferenceNumber;           
            updateFunding.FundingStatusId = updateFundingModel.FundingStatusId;

            Institution sponsorName = null;
            if (_context.Set<Institution>().Any(ac => ac.Name == updateFundingModel.SponsorName))
            {
                sponsorName = _context.Set<Institution>().Single(ac => ac.Name == updateFundingModel.SponsorName);
                updateFunding.SponsorId = sponsorName.Id;
            } 
            updateFunding.GrtNumber = updateFundingModel.GrtNumber;
            updateFunding.FundingTypeId = updateFundingModel.FundingTypeId;
            updateFunding.DateProjectedStart = updateFundingModel.DateProjectedStart;
            updateFunding.DateProjectedEnd = updateFundingModel.DateProjectedEnd;
            commonRepository.AddOrUpdateFundingRd(updateFunding, (ClaimsPrincipal) User);
            HideFields(updateFunding.FundingStatusId, updateFunding.Id);

            //MVN - added hideField for participating facult and their role
            HideFields(updateFunding.FundingTypeId, updateFunding.Id);

            // Track last updated date and user
            updateFunding.DateLastUpdated = DateTime.Now;
            updateFunding.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
            return Json(true);           
        }

        public ActionResult CreateFunding(GeneralFundingPostModel newFundingModel)
        {
            var sponsorId = (newFundingModel.SponsorId == 0) ? null : newFundingModel.SponsorId;
            var newfunding = new Funding
            {
                Id = Guid.NewGuid(),
                Title = newFundingModel.Title,
                GrtNumber = newFundingModel.GrtNumber,
                SponsorId = sponsorId,
                FundingTypeId = newFundingModel.FundingTypeId,
                /// MVN - customer wanted PI to be selected in the Program Director and Project partial view
                /// Therefore, facultyId set to programProjectPId, see
                /// TASK # 10454
                ProgramProjectPdId = newFundingModel.FacultyId
                // customer does not want to collect isRewal - for now 1/20/2015            
                //IsRenewal = newFundingModel.IsRenewal,
            };
            Funding newFundings = fundingRepository.AddFunding(newfunding);
            newFundings.DateLastUpdated = DateTime.Now;
            newFundings.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
            Institution institution = null;
            
            if (newFundingModel.SponsorName != null)
            {
                if (_context.Set<Institution>().Any(ac => ac.Name == newFundingModel.SponsorName))
                {
                    institution = _context.Set<Institution>().Single(ac => ac.Name == newFundingModel.SponsorName);                    
                    newfunding.SponsorId = institution.Id;
                }
            }
            bool isNew = newFundingModel.Id.Equals(null) || !_context.Set<FundingFaculty>()
               .Any(p => p.Id == newFundingModel.FacultyId);

            var newFundingFaculty = isNew
                ? new FundingFaculty()
                : _context.Set<FundingFaculty>()
                .Include(fund => fund.Faculty)
                .Single(fund => fund.FacultyId == newFundingModel.FacultyId && fund.PrimaryRoleId == 1); 

            if (newFundingModel.FacultyId != null)
            {
                commonRepository.AddUpdateFundingFaculty(newFundingModel.FacultyId, newfunding.Id, 1, 0);
            }
            return Json(newFundings.Id);
        }
    }
}

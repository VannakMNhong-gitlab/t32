﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;

using System.Security.Principal;
using System.DirectoryServices.AccountManagement;

using TraineeTrackingSystem.Web.Models;
using System.Configuration;
using TraineeTrackingSystem.Common;
using TraineeTrackingSystem.Web.ViewModels;
using OSU.Medical.Data.Enterprise.User;
using TraineeTrackingSystem.Repository;
using TraineeTrackingSystem.Data;
using TraineeTrackingSystem.Web.Entity;


namespace TraineeTrackingSystem.Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        //private readonly T32UserRolesRepository userRepository;
        //private readonly TTSEntities _context;
        //public AccountController()
        //{
        //    _context = new TTSEntities();
        //    userRepository = new T32UserRolesRepository(_context);
        //}
        //protected override void Dispose(bool disposing)
        //{
        //    userRepository.Dispose();
        //    base.Dispose(disposing);
        //}
        
        //public User loginUser;
        public AccountController()
           ///MVN 4/25/2016 to resolve slow throughput issue
           ///: this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

        public AccountController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }

        public UserManager<ApplicationUser> UserManager { get; private set; }

        //
        // GET: /Account/Login
        /// <summary>
        /// MVN - loading the login form => EvForm
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Login(string returnUrl, string token)
        {
            ViewBag.ReturnUrl = returnUrl;
            string MSG = "Please log in before trying to access pages within Honey Badger";

            if (!String.IsNullOrEmpty(token)){
                ApplicationUser user = ValidateUserPasswordAndGroup(null, null, null, token);
                //Now generate the claim token for the user
                if (user != null && user.Roles != null && user.Roles.Count > 0){
                    SignInAsync(user, false);
                    return RedirectToLocal(returnUrl);
                }
            }

            if (String.IsNullOrEmpty(token)){    
                ModelState.AddModelError("", MSG);
            }
            return View();
        }

        //
        // POST: /Account/Login
        /// <summary>
        /// MVN - handling the login user and password validation in the AD
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            ApplicationUser user = null;
            string MSG1 = "You are not authorized to view any content within Honey Badger.";
            string MSG2 = "Please contact an Application Administrator to obtain access to"; 
            string MSG3 = "the application.";         
            string InvalidUsrNmPswrdMSG = "Invalid username or password."; 

            if (ModelState.IsValid){
               //validate MedCenterId and Password 
                user = ValidateUserPasswordAndGroup(model.UserName, model.Password, model.ImpersonateUserName, null);
                //Now generate the claim token for the user
                if (user != null && user.Id != ""){
                    OsuLDAPUserManager.InsertAuthenticatedUserRoles(user.UserName, this.Session.SessionID, new string[0], returnUrl, this.Request.ServerVariables.Get("LOCAL_ADDR"), this.Request.Url.AbsoluteUri, this.Request.ServerVariables.Get("APPL_PHYSICAL_PATH"));
                    await SignInAsync(user, model.RememberMe);
                    return RedirectToLocal(returnUrl);
                }
            }
            
            /// NOT AUTHORIZE ->RE-DISPLAY login FORM 
            if (user != null && user.Id == ""){
                ModelState.AddModelError("", MSG1
                                            + Environment.NewLine
                                            + Environment.NewLine
                                            + MSG2
                                            + Environment.NewLine
                                            + MSG3);

            }else{
                /// INVALID MedCenter ID or Password
                ModelState.AddModelError("", InvalidUsrNmPswrdMSG);
            }
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        /// <summary>
        /// MVN - AD user validation
        /// </summary>
        /// <param name="pUserName"></param>
        /// <param name="pPassword"></param>
        /// <param name="pImpersonateUserName"></param>
        /// <param name="pToken"></param>
        /// <returns></returns>
        private ApplicationUser ValidateUserPasswordAndGroup(string pUserName, string pPassword, string pImpersonateUserName, string pToken)
        {
            ApplicationUser user = null;
            bool chkPwd = true;

            //If token exists, check token first
            if (!String.IsNullOrEmpty(pToken)){
                string lanId = string.Empty;
                // Domain Id and assigned token
                if (Security.CheckTokenAndLanId(pToken, Request.UserHostAddress, out lanId)){
                    pUserName = lanId;
                    pPassword = "";
                    chkPwd = false;
                }
            }

            ////////check if config as test auth - MVN - Commented out 5/2/2016 
            //////if (System.Configuration.ConfigurationManager.AppSettings["UseTestingAuthentication"] != null && System.Configuration.ConfigurationManager.AppSettings["UseTestingAuthentication"] == "true")
            //////{
            //////    chkPwd = false;
            //////}
            string adPasswd = System.Configuration.ConfigurationManager.AppSettings["LDAPAuthPassword"].ToString();
            string adUsername = System.Configuration.ConfigurationManager.AppSettings["LDAPAuthUsername"].ToString();

            string domainName = ConfigurationManager.AppSettings["AdDomainName"];
            domainName = string.IsNullOrEmpty(domainName) ? "OSUMC" : domainName;

            using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, "OSUMC", pUserName, pPassword)){
                if (chkPwd){
                    if (!pc.ValidateCredentials(pUserName, pPassword)) return null;
                }
                /// MVN - TODO: this code broke when OSU login --
                string userLogin = (String.IsNullOrEmpty(pImpersonateUserName) ? pUserName : pImpersonateUserName);
                //Check if need to impersonate to other user
                UserPrincipal up = UserPrincipal.FindByIdentity(pc, userLogin);
                if (up != null){
                    user = new ApplicationUser();
                    user.Id = up.Sid.ToString();
                    user.UserName = up.SamAccountName;


                    var claim = new IdentityUserClaim();
                    claim.ClaimType = ClaimTypes.Email;
                    claim.ClaimValue = up.EmailAddress;
                    user.Claims.Add(claim);

                    claim = new IdentityUserClaim();
                    claim.ClaimType = ClaimTypes.GivenName;
                    claim.ClaimValue = up.GivenName;
                    user.Claims.Add(claim);

                    claim = new IdentityUserClaim();
                    claim.ClaimType = ClaimTypes.Surname;
                    claim.ClaimValue = up.Surname;
                    user.Claims.Add(claim);

                    claim = new IdentityUserClaim();
                    claim.ClaimType = ClaimTypes.OtherPhone;
                    claim.ClaimValue = up.VoiceTelephoneNumber;
                    user.Claims.Add(claim);

                    /// MVN - 4/13/2016 For the T32 application User Role will be handled in the database application tables
                    /// Get user Roles from the database, access through the interface 
                    UserRolesRepository usrRoleRepository = new T32UserRolesRepository();

                    var dbUser = usrRoleRepository.GetUserByMedCenterId(user.UserName);
                    ///FIRST:  check for active user
                    if (dbUser != null && dbUser.IsActive == true){
                        user.Id = dbUser.LoginId.ToString();
                        if (dbUser.PersonRoles != null && dbUser.PersonRoles.Count() > 0){
                            foreach (var r in dbUser.PersonRoles){

                                IdentityUserRole role = new IdentityUserRole();
                                // role.Role = new IdentityRole(group.SamAccountName);
                                //BUGBUG: role id may be changed here
                                var roleName = Enum.GetName(typeof(CustomRoles), r.RoleId);

                                if (string.IsNullOrEmpty(roleName)){
                                    roleName = r.Role.Description;
                                }
                                role.RoleId = roleName;

                                user.Roles.Add(role);

                            }
                        }else{
                            //TODO: remove this check if allow employee with scores to login
                            user.Id = "";
                        }
                    }else{
                        //Not allow everyone to login if they don't have a score or not one of the roles assigned
                        user.Id = "";
                    }
                }
            }

            return user;
        }
        /// <summary>
        /// MVN - Domain Sign in Asynchronous 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="isPersistent"></param>
        /// <returns></returns>
        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            //var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            //Note: manually create the claims to save user roles from AD or SQL
            var claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
            claims.Add(new Claim(ClaimTypes.Name, user.UserName));

            var result = user.Claims.Where(p => p.ClaimType == ClaimTypes.Email).FirstOrDefault().ClaimValue ?? "";
            claims.Add(new Claim(ClaimTypes.Email, result));

            result = user.Claims.Where(p => p.ClaimType == ClaimTypes.GivenName).FirstOrDefault().ClaimValue ?? user.UserName;
            claims.Add(new Claim(ClaimTypes.GivenName, result));


            result = user.Claims.Where(p => p.ClaimType == ClaimTypes.Surname).FirstOrDefault().ClaimValue ?? user.UserName;
            claims.Add(new Claim(ClaimTypes.Surname, result));


            result = user.Claims.Where(p => p.ClaimType == ClaimTypes.OtherPhone).FirstOrDefault().ClaimValue ?? "";
            claims.Add(new Claim(ClaimTypes.OtherPhone, result));

            foreach (IdentityUserRole role in user.Roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role.RoleId));

            }



            var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);

            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);


        }
        //private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        //{
        //    loginUser = new User();

        //    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
        //    //var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
        //    //Note: manually create the claims to save user roles from AD or SQL
        //    var claims = new List<Claim>();
        //    claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
        //    claims.Add(new Claim(ClaimTypes.Name, user.UserName));
        //    loginUser.UserName = user.UserName;

        //    var result = user.Claims.Where(p => p.ClaimType == ClaimTypes.Email).FirstOrDefault().ClaimValue ?? "";
        //    claims.Add(new Claim(ClaimTypes.Email, result));
        //    loginUser.Email = result;

        //    result = user.Claims.Where(p => p.ClaimType == ClaimTypes.GivenName).FirstOrDefault().ClaimValue ?? user.UserName;
        //    claims.Add(new Claim(ClaimTypes.GivenName, result));
        //    loginUser.GivenName = result;

        //    result = user.Claims.Where(p => p.ClaimType == ClaimTypes.Surname).FirstOrDefault().ClaimValue ?? user.UserName;
        //    claims.Add(new Claim(ClaimTypes.Surname, result));
        //    loginUser.SurName = result;


        //    result = user.Claims.Where(p => p.ClaimType == ClaimTypes.OtherPhone).FirstOrDefault().ClaimValue ?? "";
        //    claims.Add(new Claim(ClaimTypes.OtherPhone, result));
        //    loginUser.Phone = result;

        //    foreach (IdentityUserRole role in user.Roles)
        //    {
        //        //claims.Add(new Claim(ClaimTypes.Role, role.Role.Name));
        //        //if (string.IsNullOrEmpty(loginUser.Roles))
        //        //{
        //        //    loginUser.Roles = role.Role.Name + ";";
        //        //}
        //        //else
        //        //{
        //        //    loginUser.Roles += role.Role.Name + ";";
        //        //}
        //        claims.Add(new Claim(ClaimTypes.Role, role.RoleId));
        //        if (string.IsNullOrEmpty(loginUser.Roles)){
        //            loginUser.Roles = role.RoleId + ";";
        //        }else{
        //            loginUser.Roles += role.RoleId + ";";
        //        }
        //    }

        //    var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);

        //    AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);


        //}

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }
        /// <summary>
        /// MVN - redirect URL Original code
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        //private ActionResult RedirectToLocal(string returnUrl)
        //{
        //    if (Url.IsLocalUrl(returnUrl))
        //    {
        //        return Redirect(returnUrl);
        //    }
        //    else
        //    {
        //        return RedirectToAction("Index", "Home");
        //    }
        //}
        /// <summary>
        /// MVN - Redirect URL 
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        private ActionResult RedirectToLocal(string returnUrl)
        {

            if (Url.IsLocalUrl(returnUrl) && !returnUrl.ToLower().Contains(@"account/logoff")){
                return Redirect(returnUrl);
            }else{
                return RedirectToAction("Index", "Home");
            }
        }
        /////////// <summary>
        /////////// MVN - this redirect URL to login page with specific error message
        /////////// </summary>
        /////////// <param name="returnUrl"></param>
        /////////// <returns></returns>
        ////////private ActionResult RedirectToMessageLog(string returnUrl)
        ////////{

        ////////    if (Url.IsLocalUrl(returnUrl) && !returnUrl.ToLower().Contains(@"account/logoff"))
        ////////    {
        ////////        return Redirect(returnUrl);
        ////////    }
        ////////    else
        ////////    {
        ////////        ModelState.AddModelError("", "Please log in before trying to access pages within Honey Badger.");
        ////////        return RedirectToAction("Index", "Home");
        ////////    }
        ////////}
        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}
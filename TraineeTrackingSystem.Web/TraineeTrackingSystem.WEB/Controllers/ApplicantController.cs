﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using TraineeTrackingSystem.Common;
using TraineeTrackingSystem.Repository;
using TraineeTrackingSystem.Data;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using TraineeTrackingSystem.Web.PostModels;
using TraineeTrackingSystem.Web.ViewModels;
using TraineeTrackingSystem.Web.Common;
// added tracking date and user update
using System.Security.Claims;
using TraineeTrackingSystem.Web.Models;

namespace TraineeTrackingSystem.Web.Controllers
{
    [AuthorizeUser(AccessLevel = "Editor")]
    //[AuthorizeUser(MultipleRoles = "Administor, SuperUser,Editor")]
    public class ApplicantController : Controller
    {
        private readonly ApplicantRepository applicantRepository;
        private readonly FacultyRepository facultyRepository;
        private readonly CommonDataRepository commonRepository;
        private readonly TTSEntities _context;
        //This globle variable will be used to convert date to specific format 
        private readonly string _LastUpdateDATEFORMAT = "{0:M/dd/yyy hh:mm:tt}";
        public ApplicantController()
        {
            _context = new TTSEntities();
            applicantRepository = new ApplicantRepository(_context);
            facultyRepository = new FacultyRepository(_context);
            this.commonRepository = new CommonDataRepository(_context);
        }
        protected override void Dispose(bool disposing)
        {
            applicantRepository.Dispose();
            facultyRepository.Dispose();
            commonRepository.Dispose();
            base.Dispose(disposing);
        }
        [HttpGet]
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        //[AuthorizeUser(AccessLevel = "SuperUser, Administrator, Editor")]
        public ActionResult Index()
        {
            var facultyApplicantDemographics = applicantRepository.Get_Search_VwApplicantDemographics();
            return View(facultyApplicantDemographics);
        }

        //public ActionResult Create(Organization org, GradeRecord grade, OsuTimeline osu, Program prog)
        public ActionResult Create(Organization org, GradeRecord grade, OsuTimeline osu, Program prog, Applicant apl)
        {
            var organizationType = commonRepository.GetOrganizationList().ToList();
            var programType = commonRepository.GetProgramList().ToList();
            var programList = commonRepository.GetProgramList().ToList();
            var applicantsGeneralViewModel = new ApplicantsGeneralViewModel
            {
                DepartmentId = org.Id,
                Departments = organizationType.Select(o => new KeyValuePair<int, string>(o.Id, o.DisplayName)),     
                ProgramId = prog.Id,
                ///////// MVN - replaced this code, instead
                //////Programs = programList.Select(p => new KeyValuePair<Guid, string>(p.Id, p.Title)),
                //////SelectedPrograms = commonRepository.GetProgFromViewTable().Select(p => p.ProgramId),
                Programs = programType.Select(p => new KeyValuePair<Guid, string>(p.Id, p.Title))
            };
            return View(new EditApplicantViewModel
            {
                ApplicantInformations = applicantsGeneralViewModel
            });
        }


        public ActionResult CreateNewApplicant(ApplicantPostModel newApplicantModel)
        {
            var departmentId = (newApplicantModel.DepartmentId == 0) ? null : newApplicantModel.DepartmentId;
            var yearEntered = (newApplicantModel.YearEntered == 0) ? null : newApplicantModel.YearEntered;
            var newPerson = new Person
            {
                PersonId = Guid.NewGuid(),
                FirstName = newApplicantModel.FirstName,
                LastName = newApplicantModel.LastName,
                MiddleName = newApplicantModel.MiddleName,
                DateCreated = DateTime.Now,

                IsUnderrepresentedMinority = newApplicantModel.IsUnderrepresentedMinority,
                IsIndividualWithDisabilities = newApplicantModel.IsIndividualWithDisabilities,
                IsFromDisadvantagedBkgd = newApplicantModel.IsFromDisadvantagedBkgd,
                DateLastUpdated = DateTime.Now,
                LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value)
            };
            
            var applicant = new Applicant
            {
                PersonId = newPerson.PersonId,
                DoctoralLevelId = newApplicantModel.DoctoralLevelId,
           
                //DepartmentId = newApplicantModel.DepartmentId,
                DepartmentId = departmentId,
                IsTrainingGrantEligible = newApplicantModel.IsTrainingGrantEligible
            };

            var grade = new GradeRecord
            {
                PersonId = newPerson.PersonId,
            };
            var osutimeLine = new OsuTimeline
            {
                PersonId = newPerson.PersonId,
                //YearEntered = newApplicantModel.YearEntered,
                YearEntered = yearEntered
            };
            Applicant newApplicant = applicantRepository.AddNewApplicant(newPerson, applicant, grade, osutimeLine);
            /// MVN - tracking user activity added 10/01/2015
            newApplicant.DateLastUpdated = DateTime.Now;
            newApplicant.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
            _context.SaveChanges();

            bool isNew = newApplicantModel.PersonId.Equals(null) || !_context.Set<Person>()
                .Any(o => o.PersonId == newApplicantModel.PersonId);

            // PERSON table =>Many to many
            var newApplicantProg = isNew
                ? new Person()
                : _context.Set<Person>()
                .Include(appProg => appProg.Programs)
                .Single(appProg => appProg.PersonId == newApplicantModel.PersonId);
            // Performing the ADD here>>>
            if (newApplicantModel.ProgramsCollection != null)
            {
                foreach (var selectedProgramId in newApplicantModel.ProgramsCollection)
                {
                    if (newPerson.Programs.All(pro => pro.Id != selectedProgramId))
                    {
                        var addProgram = _context.Set<Program>().Find(selectedProgramId);
                        newPerson.Programs.Add(addProgram);
                    }
                }
            }

            newApplicantProg.PersonId = newApplicantModel.PersonId;
            if (isNew)
            {
                _context.Set<Person>().Add(newApplicantProg);
            }          
            _context.SaveChanges();       

            return Json(newApplicant.Id);         
        }

        public ActionResult CreateNewPredoc(ApplicantPostModel newPreDocModel)
        {
            var newPerson = new Person
            {
                PersonId = newPreDocModel.PersonId
            };
            var newApplicant = new Applicant
            {
                ApplicantId = newPreDocModel.ApplicantId
            };
            var newGPA = new GradeRecord
            {
                PersonId = newPerson.PersonId,
                Gpa = newPreDocModel.Gpa,
                GpaScale = newPreDocModel.GpaScale,
                GreScoreVerbal = newPreDocModel.GreScoreVerbal,
                GrePercentileVerbal = newPreDocModel.GrePercentileVerbal,
                GreScoreQuantitative = newPreDocModel.GreScoreQuantitative,
                GrePercentileQuantitative = newPreDocModel.GrePercentileQuantitative,
                GreScoreAnalytical = newPreDocModel.GreScoreAnalytical,
                GrePercentileAnalytical = newPreDocModel.GreScoreAnalytical,
                GreScoreSubject = newPreDocModel.GreScoreSubject,
                GrePercentileSubject = newPreDocModel.GrePercentileSubject,
                McatScoreVerbalReasoning = newPreDocModel.McatScoreVerbalReasoning,
                McatScorePhysicalSciences = newPreDocModel.McatScorePhysicalSciences,
                McatScoreBiologicalSciences = newPreDocModel.McatScoreBiologicalSciences,
                McatScoreWriting = newPreDocModel.McatScoreWriting,
                McatPercentile = newPreDocModel.McatPercentile
            };
            var osutimeLine = new OsuTimeline
            {
                PersonId = newPerson.PersonId,
                WasInterviewed = newPreDocModel.WasInterviewed,
                AcceptedOffer = newPreDocModel.AcceptedOffer,
                WasAccepted = newPreDocModel.WasAccepted, 
                WasEnrolled = newPreDocModel.WasEnrolled
            };
            Applicant addApplicant = applicantRepository.AddNewPredDocApplicant(newPerson, newApplicant, newGPA, osutimeLine);
            _context.SaveChanges();
            return Json(addApplicant.Id);
        }
        /// <summary>
        /// MVN - This private method is to provide the hiding fields for a more elegant way and much more easily to maintaining.  
        /// If there is logic changed, we can just update this method, as appose to multiple places.  
        /// </summary>
        /// <param name="fdStatusId"></param>
        /// <param name="fundingId"></param>
        private void HideFields(Guid applicantId)
        {
            var currApplicant = applicantRepository.GetApplicantByPersonId(applicantId);  
            // hidding or displaying the lastUpdate Info for General Information
            ViewBag.LastUpdated = (currApplicant.DateLastUpdated.HasValue == false) ? true : false;
            
        }

        public ActionResult Edit(Guid id)
        {
            var currApplicant = applicantRepository.GetApplicantByPersonId(id);           
            // MVN - 11-05-2014 to fix the department sorting issue
            //var organizationType = facultyRepository.GetOrganization().ToList();
            var organizationType = commonRepository.GetOrganizationList().ToList();
            var states = _context.StateProvinces.ToList();
            var countries = _context.Countries.ToList();
            var programList = commonRepository.GetProgramList().ToList();
            var getAppProg = commonRepository.GetApplicantProgramList(id);
            // 10/8/15: MPC -- Removed extra database hit -- returned data wasn't even being used!
            //var applicant = commonRepository.GetAPLastUpdateInfo(currApplicant.PersonId);
            var applicantGeneralViewModel = new ApplicantGeneralViewModel
            {
                Id = id,
                ApplicantId = currApplicant.ApplicantId,
                PersonId = currApplicant.PersonId,
                DoctoralLevelId = currApplicant.DoctoralLevelId.HasValue ? currApplicant.DoctoralLevelId.Value : 0,
                DepartmentId = currApplicant.DepartmentId.HasValue ? currApplicant.DepartmentId.Value : 0,
                Departments = organizationType.Select(o => new KeyValuePair<int, string>(o.Id, o.DisplayName)),
                // This code resove UNABLE to cast object issue
                // As .FirstOrDefault return a ROUTELINQs type object 
                YearEntered = currApplicant.Person.OsuTimelines.FirstOrDefault().YearEntered,               
                //YearEntered = getApplicant.Person.OsuTimelines.FirstOrDefault(y => y.YearEntered.HasValue).YearEntered,
                
                IsTrainingGrantEligible = currApplicant.IsTrainingGrantEligible,
                IsUnderrepresentedMinority = currApplicant.Person.IsUnderrepresentedMinority,
                IsIndividualWithDisabilities = currApplicant.Person.IsIndividualWithDisabilities,
                IsFromDisadvantagedBkgd = currApplicant.Person.IsFromDisadvantagedBkgd,
                Programs = programList.Select(p => new KeyValuePair<Guid, string>(p.Id, p.Title)),

                SelectedPrograms = commonRepository.GetApplicantProgramList(currApplicant.PersonId).Select(p => p.ProgramId),
                StrDateLastUpdated = (currApplicant.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, currApplicant.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(currApplicant.LastUpdatedBy)
            };
            
            var getGrade = _context.Set<GradeRecord>()
                .Include(g => g.Person)
                .FirstOrDefault(g => g.PersonId == currApplicant.PersonId);
            var getOSUTimeLine = _context.Set<OsuTimeline>()
                .Include(o => o.Person)
                .FirstOrDefault(o => o.PersonId == currApplicant.PersonId);
            var appPreDocViewModel = new AppPreDocViewModel
            {
                Id = id,
                PersonId = currApplicant.PersonId,
                Gpa = getGrade.Gpa,
                GpaScale = getGrade.GpaScale,
                GreScoreVerbal = getGrade.GreScoreVerbal,
                GrePercentileVerbal = getGrade.GrePercentileVerbal,
                GreScoreQuantitative = getGrade.GreScoreQuantitative,
                GrePercentileQuantitative = getGrade.GrePercentileQuantitative,
                GreScoreAnalytical = getGrade.GreScoreAnalytical,
                GrePercentileAnalytical = getGrade.GrePercentileAnalytical,
                GreScoreSubject = getGrade.GreScoreSubject,
                GrePercentileSubject = getGrade.GrePercentileSubject,
                McatScoreVerbalReasoning = getGrade.McatScoreVerbalReasoning,
                McatScorePhysicalSciences = getGrade.McatScorePhysicalSciences,
                McatScoreBiologicalSciences = getGrade.McatScoreBiologicalSciences,
                McatScoreWriting = getGrade.McatScoreWriting,
                McatPercentile = getGrade.McatPercentile,
                WasInterviewed = getOSUTimeLine.WasInterviewed,
                WasAccepted = getOSUTimeLine.WasAccepted,
                WasEnrolled = getOSUTimeLine.WasEnrolled
            };
            var appPosDocViewModel = new AppPostDocViewModel
            {
                Id = id,
                PersonId = currApplicant.PersonId,
                WasOfferedPosition = getOSUTimeLine.WasOfferedPosition,
                AcceptedOffer = getOSUTimeLine.AcceptedOffer,
                EnteredProgram = getOSUTimeLine.EnteredProgram,
            };
            // 10/8/15: MPC -- Removed extra database hit -- returned data wasn't even being used!
            //var academicHistories = commonRepository.GetAHLastUpdateInfo(currApplicant.PersonId);
            var academicHistoryInformationViewModel = new AcademicsHistoryViewModel
            {
                PersonId = currApplicant.PersonId,
                States = states.Select(s => new KeyValuePair<int, string>(s.Id, s.Abbreviation)),
                Countries = countries.Select(c => new KeyValuePair<int, string>(c.Id, c.Name)),
                AcademicHistoryInformations = currApplicant.Person.AcademicHistories
                    .Where(academicHistory => academicHistory.IsDeleted == false)    
                    .Select(academicHistory => new AcademicHistoryViewModel{
                       Id = academicHistory.Id,
                       InstitutionName = academicHistory.Institution_InstitutionId != null ? academicHistory.Institution_InstitutionId.Name : commonRepository.GetDefaultInstitution().Name,
                       YearStarted = academicHistory.YearStarted,
                       YearEnded = academicHistory.YearEnded,
                       YearDegreeCompleted = academicHistory.YearDegreeCompleted,
                       AreaOfStudy = academicHistory.AreaOfStudy,
                       ResearchProjectTitle = academicHistory.ResearchProjectTitle,
                       DoctoralThesis = academicHistory.DoctoralThesis,
                       ResearchAdvisor = academicHistory.ResearchAdvisor,
                       ResidencyInstitutionId = academicHistory.ResidencyInstitutionId,
                       ResidencyTrainingInstName =
                           academicHistory.Institution_ResidencyInstitutionId != null
                               ? academicHistory.Institution_ResidencyInstitutionId.Name
                               : string.Empty,

                       ResidencyAdvisor = academicHistory.ResidencyAdvisor,
                       ResidencyYearStarted = academicHistory.ResidencyYearStarted,
                       ResidencyYearEnded = academicHistory.ResidencyYearEnded,
                       Comments = academicHistory.Comments,

                       AcademicDegreeId = academicHistory.AcademicDegreeId,
                       AcademicDegreeName = academicHistory.AcademicDegree != null ? academicHistory.AcademicDegree.Name : string.Empty,

                       CityName =
                           academicHistory.Institution_InstitutionId != null
                               ? academicHistory.Institution_InstitutionId.City
                               : string.Empty,
                       StateName =
                           academicHistory.Institution_InstitutionId != null && academicHistory.Institution_InstitutionId.StateProvince != null
                               ? (academicHistory.Institution_InstitutionId.StateProvince.Abbreviation.Length > 0
                                       ? academicHistory.Institution_InstitutionId.StateProvince.Abbreviation
                                       : academicHistory.Institution_InstitutionId.StateProvince.FullName)
                               : string.Empty,
                       CountryName =
                           academicHistory.Institution_InstitutionId != null
                               ? academicHistory.Institution_InstitutionId.Country.Name
                               : string.Empty,
                       /// MVN - added tracking user activity feature here 
                       StrDateLastUpdated = (academicHistory.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, academicHistory.DateLastUpdated) : string.Empty,
                       StrLastUpdatedBy = commonRepository.GetLoginInfo(academicHistory.LastUpdatedBy)
                   })

            };
            HideFields(currApplicant.Id);
            return View(new EditApplicantViewModel
            {
                ApplicantGeneralViewModel = applicantGeneralViewModel,       
                AppPreViewModel = appPreDocViewModel,
                AppPostDocViewModel = appPosDocViewModel,
                AcademicHistoryInformations = academicHistoryInformationViewModel
            });

        }
    
        [HttpPost]
        public ActionResult UpdateApplicant(ApplicantPostModel updateApplicantModel)
        {
            var departmentId = (updateApplicantModel.DepartmentId == 0) ? null : updateApplicantModel.DepartmentId;
            var applicant = _context.Set<Applicant>()
                .Include(a => a.Person)
                .Single(a => a.PersonId == updateApplicantModel.PersonId);
  
            applicant.DoctoralLevelId = updateApplicantModel.DoctoralLevelId;
            // To resolve the NULL FK issue
            if (updateApplicantModel.DepartmentId > 0) {
                //applicant.DepartmentId = updateApplicantModel.DepartmentId;
                applicant.DepartmentId = departmentId;
            }
            
            // This code resove UNABLE to cast object issue
                // As .FirstOrDefault return a ROUTELINQs type object 
            //YearEntered = getApplicant.Person.OsuTimelines.FirstOrDefault().YearEntered,       
            applicant.Person.OsuTimelines.FirstOrDefault().YearEntered = updateApplicantModel.YearEntered;
            //applicant.Person.OsuTimelines.FirstOrDefault(y => y.YearEntered.HasValue).YearEntered = updateApplicantModel.YearEntered;
            applicant.IsTrainingGrantEligible = updateApplicantModel.IsTrainingGrantEligible;
            applicant.Person.IsUnderrepresentedMinority = updateApplicantModel.IsUnderrepresentedMinority;
            applicant.Person.IsIndividualWithDisabilities = updateApplicantModel.IsIndividualWithDisabilities;
            applicant.Person.IsFromDisadvantagedBkgd = updateApplicantModel.IsFromDisadvantagedBkgd;
           // _context.SaveChanges();
 
            //bool isUpdate = updateApplicantModel.PersonId.Equals(null) || !_context.Set<Person>()
            //   .Any(o => o.PersonId == updateApplicantModel.PersonId);
            bool isUpdate = updateApplicantModel.Id.Equals(null) || !_context.Set<Person>()
               .Any(o => o.PersonId == updateApplicantModel.Id);


            // 10/8/15: MPC -- Replaced the previous, non-working functionality (wouldn't save or delete Programs)
            var savedPrograms = applicant.Person.Programs.ToList();

            // ADD newly selected Programs to database
            if (updateApplicantModel.ProgramsCollection != null)
            {
                foreach (var selectedProgramId in updateApplicantModel.ProgramsCollection)
                {
                    if (applicant.Person.Programs.All(p => p.Id != selectedProgramId))
                    {
                        var addProgram = _context.Set<Program>().Find(selectedProgramId);
                        applicant.Person.Programs.Add(addProgram);
                    }
                }
            }

            // DELETE previously-saved, just removed Programs from database
            foreach (var savedProgram in savedPrograms)
            {
                if (updateApplicantModel.ProgramsCollection != null) {
                    if (!updateApplicantModel.ProgramsCollection.Contains(savedProgram.Id))
                    {
                        var remProgram = _context.Set<Program>().Find(savedProgram.Id);
                        applicant.Person.Programs.Remove(remProgram);
                    }

                } else {
                    var remProgram = _context.Set<Program>().Find(savedProgram.Id);
                    applicant.Person.Programs.Remove(remProgram);
                }
            }



            //var applicantProgTobeUpdate = isUpdate
            //   ? new Person()
            //   : _context.Set<Person>()
            //   .Include(appProg => appProg.Programs)
            //   .Single(appProg => appProg.PersonId == updateApplicantModel.PersonId);
 
            //// Performing the ADD
            //if (updateApplicantModel.ProgramsCollection != null)
            //{
            //    foreach (var selectedProgramId in updateApplicantModel.ProgramsCollection)
            //    {                  
            //        if (applicantProgTobeUpdate.Programs.All(pro => pro.Id != selectedProgramId))
            //        {
            //            var addProgram = _context.Set<Program>().Find(selectedProgramId);
            //            applicantProgTobeUpdate.Programs.Add(addProgram);
            //        }           
            //    }                
            //}

            //// Performing the REMOVE
            //var result = applicantProgTobeUpdate.Programs.ToList();
            //foreach (var removeProg in result)
            //{
            //    if (!updateApplicantModel.ProgramsCollection.Contains(removeProg.Id))
            //    {
            //        var progToBeRemoved = _context.Set<Program>().Find(removeProg.Id);
            //        applicantProgTobeUpdate.Programs.Remove(progToBeRemoved);
            //    }
            //}

            //applicantProgTobeUpdate.PersonId = applicantProgTobeUpdate.PersonId;
            //if (isUpdate)
            //{
            //    _context.Set<Person>().Add(applicantProgTobeUpdate);
            //}
            // Tracking date and user when the form is altered
            applicant.DateLastUpdated = DateTime.Now;
            applicant.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);

            _context.SaveChanges();

            return Json(true);
        }
        public ActionResult UpdateApplicantPredoc(ApplicantPostModel updatePredDocApplModel)
        {
            var updateGrade = _context.Set<GradeRecord>()
                .Include(a => a.Person)
                .Single(a => a.PersonId == updatePredDocApplModel.PersonId);
            updateGrade.Gpa = updatePredDocApplModel.Gpa;
            updateGrade.GpaScale = updatePredDocApplModel.GpaScale;           
            _context.SaveChanges();

            var updateOSUTimeLine = _context.Set<OsuTimeline>()
                .Include(o => o.Person)
                .FirstOrDefault(o => o.PersonId == updatePredDocApplModel.PersonId);
            updateOSUTimeLine.WasInterviewed = updatePredDocApplModel.WasInterviewed;
            updateOSUTimeLine.WasAccepted = updatePredDocApplModel.WasAccepted;
            updateOSUTimeLine.WasEnrolled = updatePredDocApplModel.WasEnrolled;         
            _context.SaveChanges();
            return Json(true);
        }
        public ActionResult SaveGREScores(ApplicantPostModel updatePredDocApplModel){
            var updateGrade = _context.Set<GradeRecord>()
                .Include(a => a.Person)
                .Single(a => a.PersonId == updatePredDocApplModel.PersonId);
            updateGrade.GreScoreVerbal = updatePredDocApplModel.GreScoreVerbal;
            updateGrade.GrePercentileVerbal = updatePredDocApplModel.GrePercentileVerbal;
            updateGrade.GreScoreQuantitative = updatePredDocApplModel.GreScoreQuantitative;
            updateGrade.GrePercentileQuantitative = updatePredDocApplModel.GrePercentileQuantitative;
            updateGrade.GreScoreAnalytical = updatePredDocApplModel.GreScoreAnalytical;
            updateGrade.GrePercentileAnalytical = updatePredDocApplModel.GrePercentileAnalytical;
            updateGrade.GreScoreSubject = updatePredDocApplModel.GreScoreSubject;
            updateGrade.GrePercentileSubject = updatePredDocApplModel.GrePercentileSubject;
            _context.SaveChanges();
            return Json(true);
        }
        public ActionResult SaveMCATScores (ApplicantPostModel updatePredDocApplModel){
            var updateGrade = _context.Set<GradeRecord>()
                .Include(a => a.Person)
                .Single(a => a.PersonId == updatePredDocApplModel.PersonId);

            updateGrade.McatScoreVerbalReasoning = updatePredDocApplModel.McatScoreVerbalReasoning;
            updateGrade.McatScorePhysicalSciences = updatePredDocApplModel.McatScorePhysicalSciences;
            updateGrade.McatScoreBiologicalSciences = updatePredDocApplModel.McatScoreBiologicalSciences;
            updateGrade.McatScoreWriting = updatePredDocApplModel.McatScoreWriting;
            updateGrade.McatPercentile = updatePredDocApplModel.McatPercentile;
            _context.SaveChanges();
            return Json(true);
        }
        public ActionResult UpdateApplicantPostdoc(ApplicantPostModel updatePostDocApplModel)
        {           
            var updateOSUTimeLine = _context.Set<OsuTimeline>()
                .Include(o => o.Person)
                .FirstOrDefault(o => o.PersonId == updatePostDocApplModel.PersonId);
            updateOSUTimeLine.WasOfferedPosition = updatePostDocApplModel.WasOfferedPosition;
            updateOSUTimeLine.AcceptedOffer = updatePostDocApplModel.AcceptedOffer;
            updateOSUTimeLine.EnteredProgram = updatePostDocApplModel.EnteredProgram;

            _context.SaveChanges();
            return Json(true);
        }

        public ActionResult UpdateAcadmicHistory(AcademicHistoryPostModel updateAcademicHistoryModel)
        {
            bool isNew = updateAcademicHistoryModel.Id.Equals(null) || !_context.Set<AcademicHistory>()
                .Any(o => o.Id == updateAcademicHistoryModel.Id);

            var degreeInfo = isNew
                ? new AcademicHistory()
                : _context.Set<AcademicHistory>()
                //.Find(updateAcademicHistoryModel.Id);
                .Single(tp => tp.Id == updateAcademicHistoryModel.Id);

            Institution institution = null;
            if (updateAcademicHistoryModel.Institution != null)
            {
                if (_context.Set<Institution>().Any(ac => ac.Name == updateAcademicHistoryModel.Institution))
                {
                    institution = _context.Set<Institution>().Single(ac => ac.Name == updateAcademicHistoryModel.Institution);
                    degreeInfo.InstitutionId = institution.Id;
                }
            }

            degreeInfo.YearStarted = updateAcademicHistoryModel.YearStarted;
            degreeInfo.YearEnded = updateAcademicHistoryModel.YearEnded;

            AcademicDegree academicDegree = null;
            if (updateAcademicHistoryModel.AcademicDegree != null)
            {
                if (_context.Set<AcademicDegree>().Any(ac => ac.Name == updateAcademicHistoryModel.AcademicDegree))
                {
                    academicDegree = _context.Set<AcademicDegree>()
                        .Single(ac => ac.Name == updateAcademicHistoryModel.AcademicDegree);
                    degreeInfo.AcademicDegreeId = academicDegree.Id;
                }
            }
            degreeInfo.AreaOfStudy = updateAcademicHistoryModel.AreaOfStudy;
            degreeInfo.YearDegreeCompleted = updateAcademicHistoryModel.YearDegreeCompleted;
           
            degreeInfo.ResearchProjectTitle = updateAcademicHistoryModel.ResearchProjectTitle;
            degreeInfo.DoctoralThesis = updateAcademicHistoryModel.DoctoralThesis;
            degreeInfo.ResearchAdvisor = updateAcademicHistoryModel.ResearchAdvisor;
            

            Institution residencyInstitution = null;
            if (updateAcademicHistoryModel.ResidencyInstitution != null)
            {
                if (_context.Set<Institution>().Any(ac => ac.Name == updateAcademicHistoryModel.ResidencyInstitution))
                {
                    residencyInstitution = _context.Set<Institution>().Single(ac => ac.Name == updateAcademicHistoryModel.ResidencyInstitution);
                    degreeInfo.ResidencyInstitutionId = residencyInstitution.Id;
                    degreeInfo.IsResidency = true;
                }
            }

            degreeInfo.ResidencyAdvisor = updateAcademicHistoryModel.ResidencyAdvisor;
            degreeInfo.ResidencyYearStarted = updateAcademicHistoryModel.ResidencyYearStarted;
            degreeInfo.ResidencyYearEnded = updateAcademicHistoryModel.ResidencyYearEnded;

            degreeInfo.Comments = updateAcademicHistoryModel.Comments;

            degreeInfo.DateLastUpdated = DateTime.Now;
            degreeInfo.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);

            degreeInfo.PersonId = updateAcademicHistoryModel.PersonId;
            if (isNew)
            {
                _context.Set<AcademicHistory>().Add(degreeInfo);
            }
            _context.SaveChanges();


            var degreeViewModel = new AcademicHistoryViewModel
            {
                Id = degreeInfo.Id,
                InstitutionName = degreeInfo.Institution_InstitutionId.Name,
                YearStarted = updateAcademicHistoryModel.YearStarted,
                YearEnded = updateAcademicHistoryModel.YearEnded,
                AcademicDegreeName = degreeInfo.AcademicDegreeId.HasValue ? degreeInfo.AcademicDegree.Name : string.Empty,
                AreaOfStudy = updateAcademicHistoryModel.AreaOfStudy,
                YearDegreeCompleted = updateAcademicHistoryModel.YearDegreeCompleted,

                ResearchProjectTitle = updateAcademicHistoryModel.ResearchProjectTitle,
                DoctoralThesis = updateAcademicHistoryModel.DoctoralThesis,
                ResearchAdvisor = updateAcademicHistoryModel.ResearchAdvisor,

                ResidencyInstitutionId = residencyInstitution != null ? residencyInstitution.Id : 0,
                ResidencyTrainingInstName = residencyInstitution != null ? residencyInstitution.Name : string.Empty,
                ResidencyAdvisor = updateAcademicHistoryModel.ResidencyAdvisor,
                ResidencyYearStarted = updateAcademicHistoryModel.ResidencyYearStarted,
                ResidencyYearEnded = updateAcademicHistoryModel.ResidencyYearEnded,
                /// MVN added tracking user acitivity feature
                StrDateLastUpdated = (degreeInfo.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, degreeInfo.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(degreeInfo.LastUpdatedBy),
                Comments = degreeInfo.Comments
            };

            return new JsonCamelCaseResult(degreeViewModel, JsonRequestBehavior.AllowGet);
        }
      
        [HttpPost]
        public ActionResult DeleteAcademicHistory(Guid id)
        {
            var facultyAcademicHistory = _context.Set<AcademicHistory>().Find(id);
            if (facultyAcademicHistory != null)
            {
                _context.Set<AcademicHistory>().Remove(facultyAcademicHistory);
                _context.SaveChanges();
            }
            return Json(id);
        }

        public ActionResult Applicants_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(applicantRepository.Get_Search_VwApplicantDemographics().ToDataSourceResult(request));
        }
    }
}

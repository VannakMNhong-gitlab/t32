﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Configuration;
using TraineeTrackingSystem.Common;
using TraineeTrackingSystem.Repository;
using TraineeTrackingSystem.Data;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using TraineeTrackingSystem.Web.Common;
using TraineeTrackingSystem.Web.PostModels;
using TraineeTrackingSystem.Web.ViewModels;
// added tracking date and user update
using System.Security.Claims;
using System.Collections;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

namespace TraineeTrackingSystem.Web.Controllers
{
    //public class ReportController : BaseController
    public class ReportController : Controller
    {
        private readonly CommonDataRepository commonRepository;
        private TTSEntities db = new TTSEntities();
        private readonly int _trainingGrantReportId = 29;
        private readonly int _fundingReportId = 26;
        private readonly int _facultyReportId = 30;
        private readonly int _applicantReportId = 31;
        private readonly int _menteeReportId = 25;

        private readonly TTSEntities _context;
        //
        // GET: /Report/
        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ReportController()
        {
            _context = new TTSEntities();
            //this.menteeRepository = new MenteeRepository(_context);
            this.commonRepository = new CommonDataRepository(_context);
        }

        protected override void Dispose(bool disposing)
        {
            //menteeRepository.Dispose();
            base.Dispose(disposing);
        }

        [HttpGet]
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        // GET: Mentee
        public ActionResult Index()
        {

            LoadReportsPage();
            return View();
        }

        // Called by the Landing page to display/sort relevant data in the grid
        //public virtual JsonResult Reports_Read([DataSourceRequest] DataSourceRequest request)
        //{
        //    //return Json(menteeRepository.GetSearch_VwMenteeDemographics().ToDataSourceResult(request));
        //    return Json(commonRepository.GetSearch_VwReports().ToDataSourceResult(request));
        //}

        private ActionResult LoadReportsPage()
        {
            var reportList = commonRepository.GetSearch_VwReports("T32").ToList();
            var trainingGrantList = commonRepository.GetSearch_VwTrainingGrantGeneralData().ToList();
            var applicantList = commonRepository.GetApplicantIdList(0).ToList();
            var facultyList = commonRepository.GetFacultyNonNullFullNameList().ToList();
            var menteeList = commonRepository.GetMenteeNonNullFullNameList().ToList();
            var fundingList = commonRepository.GetSearch_VwFundingDemographics().ToList();
            var currentDate = DateTime.Now;
            
            var rptsView = new ReportViewModel
            {
                Reports = reportList.Select(rpt => new KeyValuePair<int, string>(rpt.Id, rpt.Title)),
                GrantProposals = trainingGrantList.Select(tg => new KeyValuePair<Guid, string>(tg.Id, commonRepository.ScrubHtml(tg.Title) )),
                SelectedYear = currentDate.Year
            };
            var trainingGrantView = new ReportTrainingGrantViewModel
            {
                //SelectedYear = currentDate.Year,
                TrainingGrants = trainingGrantList.Select(tg => new KeyValuePair<Guid, string>(tg.Id, commonRepository.ScrubHtml(tg.Title))),
                IsTrainingGrantReportFound = (commonRepository.GetReportById(_trainingGrantReportId) != null ? true : false)
            };
            var fundingView = new ReportFundingViewModel
            {
                //SelectedYear = currentDate.Year,
                Fundings = fundingList.Select(f => new KeyValuePair<Guid, string>(f.Id, commonRepository.ScrubHtml(f.Title) )),
                SelectedFunding = fundingList.FirstOrDefault(f => f.Id != null).Id,
                IsFundingReportFound = (commonRepository.GetReportById(_fundingReportId) != null ? true : false)
            };
            var applicantView = new ReportApplicantViewModel
            {
                SelectedApplicantYearEntered = currentDate.Year,
                Applicants = applicantList.Select(a => new KeyValuePair<Guid, int>(a.Id, a.ApplicantId)),
                SelectedApplicant = applicantList.FirstOrDefault(a => a.Id != null).Id,
                IsApplicantReportFound = (commonRepository.GetReportById(_applicantReportId) != null ? true : false)
            };
            var facultyView = new ReportFacultyViewModel
            {
                //SelectedYear = currentDate.Year,
                FacultyMembers = facultyList.Select(f => new KeyValuePair<Guid, string>(f.Id, f.FullName)),
                SelectedFaculty = facultyList.FirstOrDefault(f => f.Id != null).Id,
                IsFacultyReportFound = (commonRepository.GetReportById(_facultyReportId) != null ? true : false)
            };
            var menteeView = new ReportMenteeViewModel
            {
                //SelectedYear = currentDate.Year,
                Mentees = menteeList.Select(m => new KeyValuePair<Guid, string>(m.Id, m.FullName)),
                SelectedMentee = menteeList.FirstOrDefault(m => m.Id != null).Id,
                IsMenteeReportFound = (commonRepository.GetReportById(_menteeReportId) != null ? true : false)
            };

            return View(new EditReportViewModel
            {
                ReportGeneralInfo = rptsView,
                TrainingGrantDetails = trainingGrantView,
                FundingDetails = fundingView,
                ApplicantDetails = applicantView,
                FacultyDetails = facultyView,
                MenteeDetails = menteeView
            });
            //return View(rptsView);
        }

        public ActionResult PrintT32Report(FormCollection pForm)
        {
            // Find requested report, it's path, and it's format
            Report selectedRpt = commonRepository.GetReportById(Convert.ToInt32(pForm["reportDropDown"]));

            if (selectedRpt != null)
            {
                string mCurrentReportPath = GetReportPathName(selectedRpt.Name);
                //string mCurrentReportPath = "https://securecollaborateqa.osumc.edu/sites/COM/TraineeTracking/Reports/T32_Table2.rdl";
                string mFormat = "excel";
                OSU.Medical.BusFacade.Enterprise.Web.ReportingFacade2010 mReportFacade = new OSU.Medical.BusFacade.Enterprise.Web.ReportingFacade2010();

                OSU.Medical.BusFacade.Enterprise.Web.ReportFormat mRptFormat = mReportFacade.GetReportFormat(mFormat);

                // Create Parameter List with entries from UI form
                List<DictionaryEntry> alParams = new List<DictionaryEntry>();

                DictionaryEntry deTrainingGrant = new DictionaryEntry("TrainingGrantId", pForm["grantProposalDropDown"]);//"D159F84F-5D07-4462-BBC1-F9A8A3714E45");
                alParams.Add(deTrainingGrant);

                //DictionaryEntry deCurrentYear = new DictionaryEntry("CurrentYear", rptModel.CurrentYear);
                //alParams.Add(deCurrentYear);

                // At least 1 parameter is required for this report - if none were returned, return null, otherwise, create report
                if (alParams.Count > 0)
                {
                    string tgName = commonRepository.GetTrainingGrantTitle(new Guid(pForm["grantProposalDropDown"]));

                    // Since Training Grant Title is rich text, remove all HTML formatting so it can be used in filename
                    if (tgName != null)
                    {
                        tgName = commonRepository.ScrubHtml(tgName);
                    }

                    byte[] mRpt = mReportFacade.RenderReport(mCurrentReportPath, mRptFormat, alParams.ToArray());
                    return File(mRpt, "vnd.ms-excel", selectedRpt.Name + "_" + tgName + ".xls");
                
                }
                else
                {
                    return null;
                }

            }
            else
            {
                return null;
            }
            
        }

        public ActionResult PrintTrainingGrantReport(FormCollection pForm)
        {
            // Find requested report, it's path, and it's format
            Report selectedRpt = commonRepository.GetReportById(_trainingGrantReportId);

            if (selectedRpt != null)
            {
                string mCurrentReportPath = GetReportPathName(selectedRpt.Name);
                string mFormat = "pdf";
                OSU.Medical.BusFacade.Enterprise.Web.ReportingFacade2010 mReportFacade = new OSU.Medical.BusFacade.Enterprise.Web.ReportingFacade2010();

                OSU.Medical.BusFacade.Enterprise.Web.ReportFormat mRptFormat = mReportFacade.GetReportFormat(mFormat);

                // Create Parameter List with entries from UI form
                List<DictionaryEntry> alParams = new List<DictionaryEntry>();

                DictionaryEntry deTrainingGrant = new DictionaryEntry("TrainingGrantId", pForm["trainingGrantDropDown"]);//"D159F84F-5D07-4462-BBC1-F9A8A3714E45");
                alParams.Add(deTrainingGrant);

                //DictionaryEntry deCurrentYear = new DictionaryEntry("CurrentYear", rptModel.CurrentYear);
                //alParams.Add(deCurrentYear);

                // At least 1 parameter is required for this report - if none were returned, return null, otherwise, create report
                if (alParams.Count > 0)
                {
                    string tgName = commonRepository.GetTrainingGrantTitle(new Guid(pForm["trainingGrantDropDown"]));

                    // Since Training Grant Title is rich text, remove all HTML formatting so it can be used in filename
                    if (tgName != null)
                    {
                        tgName = commonRepository.ScrubHtml(tgName);
                    }

                    byte[] mRpt = mReportFacade.RenderReport(mCurrentReportPath, mRptFormat, alParams.ToArray());
                    return File(mRpt, "application/pdf", selectedRpt.Name + "_" + tgName + ".pdf");

                }
                else
                {
                    return null;
                }

            }
            else
            {
                return null;
            }

        }


        public ActionResult PrintFundingReport(FormCollection pForm)
        {
            // Find requested report, it's path, and it's format
            Report selectedRpt = commonRepository.GetReportById(_fundingReportId);

            if (selectedRpt != null)
            {
                string mCurrentReportPath = GetReportPathName(selectedRpt.Name);
                //string mCurrentReportPath = "https://securecollaborateqa.osumc.edu/sites/COM/TraineeTracking/Reports/T32_Table2.rdl";
                string mFormat = "pdf";
                OSU.Medical.BusFacade.Enterprise.Web.ReportingFacade2010 mReportFacade = new OSU.Medical.BusFacade.Enterprise.Web.ReportingFacade2010();

                OSU.Medical.BusFacade.Enterprise.Web.ReportFormat mRptFormat = mReportFacade.GetReportFormat(mFormat);

                // Create Parameter List with entries from UI form
                List<DictionaryEntry> alParams = new List<DictionaryEntry>();

                DictionaryEntry deFunding = new DictionaryEntry("FundingId", pForm["fundingDropDown"]); //"C3E3716A-AC4D-4419-8259-2DD00D142A0F");
                alParams.Add(deFunding);

                //DictionaryEntry deCurrentYear = new DictionaryEntry("CurrentYear", rptModel.CurrentYear);
                //alParams.Add(deCurrentYear);

                // At least 1 parameter is required for this report - if none were returned, return null, otherwise, create report
                if (alParams.Count > 0)
                {
                    string fundingTitle = commonRepository.GetFundingTitle(new Guid(pForm["fundingDropDown"]));

                    // Since Funding Title is rich text, remove all HTML formatting so it can be used in filename
                    if (fundingTitle != null) {
                        fundingTitle = commonRepository.ScrubHtml(fundingTitle);
                    }

                    byte[] mRpt = mReportFacade.RenderReport(mCurrentReportPath, mRptFormat, alParams.ToArray());
                    return File(mRpt, "application/pdf", selectedRpt.Name + "_" + fundingTitle + ".pdf"); //selectedRpt.Name + ".pdf");
                }
                else
                {
                    return null;
                }

            }
            else
            {
                return null;
            }
        }


        public ActionResult PrintApplicantReport(FormCollection pForm)
        {
            // Find requested report, it's path, and it's format
            Report selectedRpt = commonRepository.GetReportById(_applicantReportId);

            if (selectedRpt != null)
            {
                string mCurrentReportPath = GetReportPathName(selectedRpt.Name);
                string mFormat = "pdf";
                OSU.Medical.BusFacade.Enterprise.Web.ReportingFacade2010 mReportFacade = new OSU.Medical.BusFacade.Enterprise.Web.ReportingFacade2010();

                OSU.Medical.BusFacade.Enterprise.Web.ReportFormat mRptFormat = mReportFacade.GetReportFormat(mFormat);

                // Create Parameter List with entries from UI form
                List<DictionaryEntry> alParams = new List<DictionaryEntry>();

                //DictionaryEntry deYearEntered = new DictionaryEntry("YearEntered", pForm["inputApplicantYearEntered"]); // rptModel.SelectedApplicantYearEntered);
                //alParams.Add(deYearEntered);

                DictionaryEntry deApplicant = new DictionaryEntry("ApplicantId", pForm["applicantDropDown"]); //"C3E3716A-AC4D-4419-8259-2DD00D142A0F");
                alParams.Add(deApplicant);

                // At least 1 parameter is required for this report - if none were returned, return null, otherwise, create report
                if (alParams.Count > 0)
                {
                    // MPC 5/24/16: Applicants are currently de-identified, so this fxn doesn't yet exist, though there are discussions about changing that
                    //string applicantName = commonRepository.GetApplicantFullName(new Guid(pForm["applicantDropDown"]));

                    string applicantId = commonRepository.GetApplicantIdByGuid(new Guid(pForm["applicantDropDown"]));

                    byte[] mRpt = mReportFacade.RenderReport(mCurrentReportPath, mRptFormat, alParams.ToArray());
                    return File(mRpt, "application/pdf", selectedRpt.Name + "_" + applicantId + ".pdf"); //selectedRpt.Name + ".pdf");
                }
                else
                {
                    return null;
                }

            }
            else
            {
                return null;
            }
        }


        public ActionResult PrintFacultyReport(FormCollection pForm)
        {
            // Find requested report, it's path, and it's format
            Report selectedRpt = commonRepository.GetReportById(_facultyReportId);

            if (selectedRpt != null)
            {
                string mCurrentReportPath = GetReportPathName(selectedRpt.Name);
                string mFormat = "pdf";
                OSU.Medical.BusFacade.Enterprise.Web.ReportingFacade2010 mReportFacade = new OSU.Medical.BusFacade.Enterprise.Web.ReportingFacade2010();

                OSU.Medical.BusFacade.Enterprise.Web.ReportFormat mRptFormat = mReportFacade.GetReportFormat(mFormat);

                // Create Parameter List with entries from UI form
                List<DictionaryEntry> alParams = new List<DictionaryEntry>();

                DictionaryEntry deFaculty = new DictionaryEntry("FacultyId", pForm["facultyDropDown"]); //"C3E3716A-AC4D-4419-8259-2DD00D142A0F");
                alParams.Add(deFaculty);

                //DictionaryEntry deCurrentYear = new DictionaryEntry("CurrentYear", rptModel.CurrentYear);
                //alParams.Add(deCurrentYear);

                // At least 1 parameter is required for this report - if none were returned, return null, otherwise, create report
                if (alParams.Count > 0)
                {
                    string facultyName = commonRepository.GetFacultyMemberFullName(new Guid(pForm["facultyDropDown"]));

                    byte[] mRpt = mReportFacade.RenderReport(mCurrentReportPath, mRptFormat, alParams.ToArray());
                    return File(mRpt, "application/pdf", selectedRpt.Name + "_" + facultyName + ".pdf"); //selectedRpt.Name + ".pdf");
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }



        public ActionResult PrintMenteeReport(FormCollection pForm)
        {
            // Find requested report, it's path, and it's format
            Report selectedRpt = commonRepository.GetReportById(_menteeReportId);

            if (selectedRpt != null)
            {
                string mCurrentReportPath = GetReportPathName(selectedRpt.Name);
                string mFormat = "pdf";
                OSU.Medical.BusFacade.Enterprise.Web.ReportingFacade2010 mReportFacade = new OSU.Medical.BusFacade.Enterprise.Web.ReportingFacade2010();

                OSU.Medical.BusFacade.Enterprise.Web.ReportFormat mRptFormat = mReportFacade.GetReportFormat(mFormat);

                // Create Parameter List with entries from UI form
                List<DictionaryEntry> alParams = new List<DictionaryEntry>();

                DictionaryEntry deMentee = new DictionaryEntry("MenteeId", pForm["menteeDropDown"]); //"C3E3716A-AC4D-4419-8259-2DD00D142A0F");
                alParams.Add(deMentee);

                 //DictionaryEntry deCurrentYear = new DictionaryEntry("CurrentYear", rptModel.CurrentYear);
                //alParams.Add(deCurrentYear);

                // At least 1 parameter is required for this report - if none were returned, return null, otherwise, create report
                if (alParams.Count > 0)
                {
                    string menteeName = commonRepository.GetMenteeFullName(new Guid(pForm["menteeDropDown"]));

                    byte[] mRpt = mReportFacade.RenderReport(mCurrentReportPath, mRptFormat, alParams.ToArray());
                    return File(mRpt, "application/pdf", selectedRpt.Name + "_" + menteeName + ".pdf"); //selectedRpt.Name + ".pdf");
                }
                else
                {
                    return null;
                }
            } else
            {
                return null;
            }

        }

        // ** MOLLEY: The following would need the iTextSharp library(s) -- couldn't determine if it's free or not **
        //public FileStreamResult pdf()
        //{
        //    MemoryStream workStream = new MemoryStream();
        //    Document document = new Document();
        //    PdfWriter.GetInstance(document, workStream).CloseStream = false;

        //    document.Open();
        //    document.Add(new Paragraph("Hello World"));
        //    document.Add(new Paragraph(DateTime.Now.ToString()));
        //    document.Close();

        //    byte[] byteInfo = workStream.ToArray();
        //    workStream.Write(byteInfo, 0, byteInfo.Length);
        //    workStream.Position = 0;

        //    return new FileStreamResult(workStream, "application/pdf");
        //}


        //public ActionResult PrintReport_FORM(FormCollection pForm)
        //{
        //    //if (CheckIfRecordsExists ("Employee",mySearchUserID,myTestBegDate,myTestEndDate) > 0 ) 
        //    {
        //        //string mCurrentReportPath = string.Format(System.Configuration.ConfigurationManager.AppSettings.Get("IndivSummaryRpt"));
        //        //var report = _context.Set<Report>()
        //        //    //.Include(rep => rep.ReportType.Name)
        //        //    //.Include(rep => rep.StatusType.Name)
        //        //    //.Include(rep => rep.Roles)
        //        //    .Single(r => r.Id.ToString() == pForm["ReportId"]);

        //        //string mCurrentReportPath = GetReportPathName(report.Name);
        //        string mCurrentReportPath = "https://securecollaborateqa.osumc.edu/sites/COM/TraineeTracking/Reports/T32_Table2.rdl";
        //        string mFormat = "Excel";
        //        OSU.Medical.BusFacade.Enterprise.Web.ReportingFacade2010 mReportFacade = new OSU.Medical.BusFacade.Enterprise.Web.ReportingFacade2010();

        //        OSU.Medical.BusFacade.Enterprise.Web.ReportFormat mRptFormat = mReportFacade.GetReportFormat(mFormat);

        //        List<DictionaryEntry> alParams = new List<DictionaryEntry>();

        //        //DictionaryEntry deTrainingGrant = new DictionaryEntry("T32Id", pForm["tgID"].ToString());
        //        DictionaryEntry deTrainingGrant = new DictionaryEntry("T32Id", "D159F84F-5D07-4462-BBC1-F9A8A3714E45");
        //        alParams.Add(deTrainingGrant);

        //        //DictionaryEntry deCurrentYear = new DictionaryEntry("CurrentYear", pForm["currentYear"].ToString());
        //        //alParams.Add(deCurrentYear);

        //        //DictionaryEntry deTestEndDate = new DictionaryEntry("TestEndDate", pForm["hToDate"].ToString());
        //        //alParams.Add(deTestEndDate);

        //        byte[] mRpt = mReportFacade.RenderReport(mCurrentReportPath, mRptFormat, alParams.ToArray());
        //        return File(mRpt, "application/pdf",  "test.xls");
        //    }

        //    //return Json("", JsonRequestBehavior.AllowGet);
        //}

        private string GetReportPathName(string name)
        {
            string reportPathName;
            string reportPath = ConfigurationManager.AppSettings["ReportingService.ReportServerUrl"];
            if (reportPath == null)
            {
                reportPathName = @"/" + name;
            }
            else
            {
                reportPathName = reportPath + @"/" + name + ".rdl";
            }
            return reportPathName;
        }

        // ==== VITALS CODE ====
        //[HttpGet]
        //public ActionResult OpenReportPageWithParameter(int reportId, Guid trainingGrantId)
        //{

        //    //Check the report permission
        //    var report = _context.Set<Report>()
        //        .Single(r => r.Id == reportId);


        //    var rptTypes = _context.ReportTypes.ToList();
        //    var generalSection = new ReportGeneralSectionViewModel
        //    {
        //        Title = report.Title,
        //        ReportTypes = rptTypes.Select(rt => new KeyValuePair<int, string>(rt.Id, rt.Name)),
        //    };

        //    string name = report.Name;
        //    return View("ReportView", new ReportViewModel
        //    {
        //        GeneralSection = generalSection,
        //        Id = reportId,
        //        Name = name,
        //        Path = GetReportPathName(name),
        //        //Username = ConfigurationManager.AppSettings["MvcReportViewer.Username"],
        //        //Password = ConfigurationManager.AppSettings["MvcReportViewer.Password"]
        //    });
        //}


        

////////        //
////////        // GET: /Report/Delete/5
////////        public ActionResult Delete(int id)
////////        {
////////            return View();
////////        }

////////        //
////////        // POST: /Report/Delete/5
////////        [HttpPost]
////////        public ActionResult Delete(int id, FormCollection collection)
////////        {
////////            try
////////            {
////////                // TODO: Add delete logic here

////////                return RedirectToAction("Index");
////////            }
////////            catch
////////            {
////////                return View();
////////            }
////////        }



////////        public void ExportToExcel<T>(List<T> list)
////////        {
////////            int columnCount = 0;

////////            DateTime StartTime = DateTime.Now;

////////            StringBuilder rowData = new StringBuilder();

////////            PropertyInfo[] properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

////////            rowData.Append("<Row ss:StyleID=\"s62\">");
////////            foreach (PropertyInfo p in properties)
////////            {
////////                if (p.PropertyType.Name != "EntityCollection`1" && p.PropertyType.Name != "EntityReference`1" && p.PropertyType.Name != p.Name)
////////                {
////////                    columnCount++;
////////                    rowData.Append("<Cell><Data ss:Type=\"String\">" + p.Name + "</Data></Cell>");
////////                }
////////                else
////////                    break;

////////            }
////////            rowData.Append("</Row>");

////////            foreach (T item in list)
////////            {
////////                rowData.Append("<Row>");
////////                for (int x = 0; x < columnCount; x++) //each (PropertyInfo p in properties)
////////                {
////////                    object o = properties[x].GetValue(item, null);
////////                    string value = o == null ? "" : o.ToString();
////////                    rowData.Append("<Cell><Data ss:Type=\"String\">" + value + "</Data></Cell>");

////////                }
////////                rowData.Append("</Row>");
////////            }

////////            var sheet = @"<?xml version=""1.0""?>
////////                    <?mso-application progid=""Excel.Sheet""?>
////////                    <Workbook xmlns=""urn:schemas-microsoft-com:office:spreadsheet""
////////                        xmlns:o=""urn:schemas-microsoft-com:office:office""
////////                        xmlns:x=""urn:schemas-microsoft-com:office:excel""
////////                        xmlns:ss=""urn:schemas-microsoft-com:office:spreadsheet""
////////                        xmlns:html=""http://www.w3.org/TR/REC-html40"">
////////                        <DocumentProperties xmlns=""urn:schemas-microsoft-com:office:office"">
////////                            <Author>MSADMIN</Author>
////////                            <LastAuthor>MSADMIN</LastAuthor>
////////                            <Created>2011-07-12T23:40:11Z</Created>
////////                            <Company>Microsoft</Company>
////////                            <Version>12.00</Version>
////////                        </DocumentProperties>
////////                        <ExcelWorkbook xmlns=""urn:schemas-microsoft-com:office:excel"">
////////                            <WindowHeight>6600</WindowHeight>
////////                            <WindowWidth>12255</WindowWidth>
////////                            <WindowTopX>0</WindowTopX>
////////                            <WindowTopY>60</WindowTopY>
////////                            <ProtectStructure>False</ProtectStructure>
////////                            <ProtectWindows>False</ProtectWindows>
////////                        </ExcelWorkbook>
////////                        <Styles>
////////                            <Style ss:ID=""Default"" ss:Name=""Normal"">
////////                                <Alignment ss:Vertical=""Bottom""/>
////////                                <Borders/>
////////                                <Font ss:FontName=""Calibri"" x:Family=""Swiss"" ss:Size=""11"" ss:Color=""#000000""/>
////////                                <Interior/>
////////                                <NumberFormat/>
////////                                <Protection/>
////////                            </Style>
////////                            <Style ss:ID=""s62"">
////////                                <Font ss:FontName=""Calibri"" x:Family=""Swiss"" ss:Size=""11"" ss:Color=""#000000""
////////                                    ss:Bold=""1""/>
////////                            </Style>
////////                        </Styles>
////////                        <Worksheet ss:Name=""Sheet1"">
////////                            <Table ss:ExpandedColumnCount=""" + (properties.Count() + 1) + @""" ss:ExpandedRowCount=""" + (list.Count() + 1) + @""" x:FullColumns=""1""
////////                                x:FullRows=""1"" ss:DefaultRowHeight=""15"">
////////                                " + rowData.ToString() + @"
////////                            </Table>
////////                            <WorksheetOptions xmlns=""urn:schemas-microsoft-com:office:excel"">
////////                                <PageSetup>
////////                                    <Header x:Margin=""0.3""/>
////////                                    <Footer x:Margin=""0.3""/>
////////                                    <PageMargins x:Bottom=""0.75"" x:Left=""0.7"" x:Right=""0.7"" x:Top=""0.75""/>
////////                                </PageSetup>
////////                                <Print>
////////                                    <ValidPrinterInfo/>
////////                                    <HorizontalResolution>300</HorizontalResolution>
////////                                    <VerticalResolution>300</VerticalResolution>
////////                                </Print>
////////                                <Selected/>
////////                                <Panes>
////////                                    <Pane>
////////                                        <Number>3</Number>
////////                                        <ActiveCol>2</ActiveCol>
////////                                    </Pane>
////////                                </Panes>
////////                                <ProtectObjects>False</ProtectObjects>
////////                                <ProtectScenarios>False</ProtectScenarios>
////////                            </WorksheetOptions>
////////                        </Worksheet>
////////                        <Worksheet ss:Name=""Sheet2"">
////////                            <Table ss:ExpandedColumnCount=""1"" ss:ExpandedRowCount=""1"" x:FullColumns=""1""
////////                                x:FullRows=""1"" ss:DefaultRowHeight=""15"">
////////                            </Table>
////////                            <WorksheetOptions xmlns=""urn:schemas-microsoft-com:office:excel"">
////////                                <PageSetup>
////////                                    <Header x:Margin=""0.3""/>
////////                                    <Footer x:Margin=""0.3""/>
////////                                    <PageMargins x:Bottom=""0.75"" x:Left=""0.7"" x:Right=""0.7"" x:Top=""0.75""/>
////////                                </PageSetup>
////////                                <ProtectObjects>False</ProtectObjects>
////////                                <ProtectScenarios>False</ProtectScenarios>
////////                            </WorksheetOptions>
////////                        </Worksheet>
////////                        <Worksheet ss:Name=""Sheet3"">
////////                            <Table ss:ExpandedColumnCount=""1"" ss:ExpandedRowCount=""1"" x:FullColumns=""1""
////////                                x:FullRows=""1"" ss:DefaultRowHeight=""15"">
////////                            </Table>
////////                            <WorksheetOptions xmlns=""urn:schemas-microsoft-com:office:excel"">
////////                                <PageSetup>
////////                                    <Header x:Margin=""0.3""/>
////////                                    <Footer x:Margin=""0.3""/>
////////                                    <PageMargins x:Bottom=""0.75"" x:Left=""0.7"" x:Right=""0.7"" x:Top=""0.75""/>
////////                                </PageSetup>
////////                                <ProtectObjects>False</ProtectObjects>
////////                                <ProtectScenarios>False</ProtectScenarios>
////////                            </WorksheetOptions>
////////                        </Worksheet>
////////                    </Workbook>";

////////            System.Diagnostics.Debug.Print(StartTime.ToString() + " - " + DateTime.Now);
////////            System.Diagnostics.Debug.Print((DateTime.Now - StartTime).ToString());

////////            string attachment = "attachment; filename=Report.xml";
////////            Response.ClearContent();
////////            Response.AddHeader("content-disposition", attachment);
////////            Response.Write(sheet);
////////            Response.ContentType = "application/ms-excel";
////////            Response.End();

////////        }


        //public void ExportToExcel(DataTable dt)
        //{
        //    if (dt.Rows.Count > 0)
        //    {
        //        string filename = "DownloadMobileNoExcel.xls";
        //        System.IO.StringWriter tw = new System.IO.StringWriter();
        //        System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
        //        DataGrid dgGrid = new DataGrid();
        //        dgGrid.DataSource = dt;
        //        dgGrid.DataBind();

        //        //Get the HTML for the control.
        //        dgGrid.RenderControl(hw);
        //        //Write the HTML back to the browser.
        //        //Response.ContentType = application/vnd.ms-excel;
        //        Response.ContentType = "application/vnd.ms-excel";
        //        Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + "");

        //        Response.Write(tw.ToString());
        //        Response.End();
        //    }
        //}
    }
}

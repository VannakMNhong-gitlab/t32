﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using TraineeTrackingSystem.Common;
using TraineeTrackingSystem.Repository;
using TraineeTrackingSystem.Data;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using TraineeTrackingSystem.Web.Common;
using TraineeTrackingSystem.Web.PostModels;
using TraineeTrackingSystem.Web.ViewModels;
using System.Security.Claims;
using System.Net;
using System.Xml;
using System.Security.Principal;
using TraineeTrackingSystem.Web.Models;

namespace TraineeTrackingSystem.Web.Controllers
{
    [AuthorizeUser(AccessLevel = "Editor")]
    //[AuthorizeUser(MultipleRoles = "Administor, SuperUser,Editor")]
    public class MenteeController : Controller
    {
        private readonly MenteeRepository menteeRepository;
        private readonly CommonDataRepository commonRepository;
        private readonly TTSEntities _context;
        private readonly string _DATEFORMAT = "{0:yyyy-MM-dd}";     //"{0:MM/dd/yyyy}";
        //This globle variable will be used to convert date to specific format 
        private readonly string _LastUpdateDATEFORMAT = "{0:M/dd/yyy hh:mm:tt}";
        public MenteeController()
        {
            _context = new TTSEntities();
            this.menteeRepository = new MenteeRepository(_context);
            this.commonRepository = new CommonDataRepository(_context);
        }

        protected override void Dispose(bool disposing)
        {
            menteeRepository.Dispose();
            base.Dispose(disposing);
        }

        [HttpGet]
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        //[AuthorizeUser(AccessLevel = "SuperUser, Administrator, Editor")]
        // GET: Mentee
        public ActionResult Index()
        {

            var menteeData = menteeRepository.GetSearch_VwMenteeDemographics();
            return View(menteeData);
        }

        // DO NOT DELETE - Called by the Mentee Landing page to display/sort relevant Mentee data in the grid
        public virtual JsonResult Mentees_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(menteeRepository.GetSearch_VwMenteeDemographics().ToDataSourceResult(request));
        }

        // GET: Mentee/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

 
        // GET: Mentee/Create
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult CreateMentee(MenteePostModel newPersonModel)
        {
            //Create Person, get person Id, and then create mentee
            var newPerson = new Person
            {
                PersonId = Guid.NewGuid(),
                FirstName = newPersonModel.FirstName,
                LastName = newPersonModel.LastName,
                MiddleName = newPersonModel.MiddleName,
                DateCreated = DateTime.Now,
                ///// MVN 
                ///// Track last updated date and user
                DateLastUpdated = DateTime.Now,
                LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value)
            };

            // 10/8/15: MPC: Removed this extra overhead, since it wasn't even being used or saved
            //Mentee newMentee = new Mentee();
            //newMentee.DateLastUpdated = DateTime.Now;
            //newMentee.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);

            Mentee newMentee = menteeRepository.AddMentee(newPerson, newPersonModel.StudentId, newPersonModel.IsTrainingGrantEligible);//, User);
            
            return Json(newMentee.Id);
        }
        /// <summary>
        /// MVN - This private method is to provide the hiding fields for a more elegant way and much more easily to maintaining.  
        /// If there is logic changed, we can just update this method, as appose to multiple places.  
        /// </summary>
        /// <param name="fdStatusId"></param>
        /// <param name="fundingId"></param>
        private void HideFields(Guid? personId, Guid menteeId)
        {
            var currMentee = menteeRepository.GetMenteeById(menteeId);
            // hidding or displaying the lastUpdate Info for General Information
            ViewBag.LastUpdated = (currMentee.DateLastUpdated.HasValue == false) ? true : false;
            
        }


        // POST: Mentee/Edit/5
        public ActionResult Edit(Guid id)
        {
            var currMentee = menteeRepository.GetMenteeById(id);
            var organizationType = commonRepository.GetOrganizationList().ToList();
            var programList = commonRepository.GetProgramList().ToList();
            var mentorList = commonRepository.GetFacultyList().ToList();
            var states = _context.StateProvinces.OrderBy(s => s.FullName).ToList();
            var countries = _context.Countries.ToList();
            //var trackUsrActivity = commonRepository.GetMTLastUpdateInfo(currMentee.PersonId);
           
            if (currMentee == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
            }
            // Use to get DateLastUpdate and lastUpdatedBy only
           // var mentee = commonRepository.GetMTLastUpdateInfo(currMentee.PersonId);
            var menteeGeneralVMInfo = new MenteeGeneralViewModel
            {
                Id = id,
                PersonId = currMentee.PersonId,

                //StrDateLastUpdated = (trackUsrActivity != null) ? String.Format(_LastUpdateDATEFORMAT, trackUsrActivity.DateLastUpdated) : string.Empty,
                //LastUpdatedBy = (mentee != null) ? mentee.LastUpdatedBy : string.Empty,
                //StrLastUpdatedBy = commonRepository.GetLoginInfo(trackUsrActivity.LastUpdatedBy),

                // 10/8/15: MPC -- Removed extra database hit by using the data already returned in original db call
                StrDateLastUpdated = (currMentee.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, currMentee.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(currMentee.LastUpdatedBy),

                AddressStates = states.Select(s => new KeyValuePair<int, string>(s.Id, s.FullName)),
                AddressCountries = countries.Select(c => new KeyValuePair<int, string>(c.Id, c.Name)),

                FirstName = currMentee.Person.FirstName,
                LastName = currMentee.Person.LastName,
                MiddleName = currMentee.Person.MiddleName,
                StudentId = currMentee.StudentId,

                InstitutionAssociationId = currMentee.InstitutionAssociationId,

                DepartmentId = currMentee.DepartmentId.HasValue ? currMentee.DepartmentId.Value : 0,
                Departments = organizationType.Select(o => new KeyValuePair<int, string>(o.Id, o.DisplayName)),

                //EmailAddress1 = currMentee.Person.ContactEmails.Count(e => e.IsPrimary == true) > 0 ?
                //    currMentee.Person.ContactEmails.FirstOrDefault(e => e.IsPrimary == true).EmailAddress : string.Empty,
                //EmailAddress2 = currMentee.Person.ContactEmails.Count(e => e.IsPrimary == false) > 0 ?
                //    currMentee.Person.ContactEmails.FirstOrDefault(e => e.IsPrimary == false).EmailAddress : string.Empty,

                //PhoneNumber = currMentee.Person.ContactPhones.Count(e => e.IsPrimary == true) > 0 ?
                //    currMentee.Person.ContactPhones.FirstOrDefault(e => e.IsPrimary == true).PhoneNumber : string.Empty,

                //MailingAddressLine1 = currMentee.Person.ContactAddresses.Count(e => e.IsPrimary == true) > 0 ?
                //    currMentee.Person.ContactAddresses.FirstOrDefault(e => e.IsPrimary == true).AddressLine1 : string.Empty,

                //MailingAddressLine2 = currMentee.Person.ContactAddresses.Count(e => e.IsPrimary == true) > 0 ?
                //    currMentee.Person.ContactAddresses.FirstOrDefault(e => e.IsPrimary == true).AddressLine2 : string.Empty,

                //MailingAddressLine3 = currMentee.Person.ContactAddresses.Count(e => e.IsPrimary == true) > 0 ?
                //    currMentee.Person.ContactAddresses.FirstOrDefault(e => e.IsPrimary == true).AddressLine3 : string.Empty,

                //MailingAddressCity = currMentee.Person.ContactAddresses.Count(e => e.IsPrimary == true) > 0 ?
                //    currMentee.Person.ContactAddresses.FirstOrDefault(e => e.IsPrimary == true).City : string.Empty,

                //MailingAddressStateId = currMentee.Person.ContactAddresses.Count(e => e.IsPrimary == true) > 0
                //    && currMentee.Person.ContactAddresses.FirstOrDefault(e => e.IsPrimary == true).StateId.HasValue ?
                //    currMentee.Person.ContactAddresses.FirstOrDefault(e => e.IsPrimary == true).StateId.Value : 0,

                //MailingAddressPostalCode = currMentee.Person.ContactAddresses.Count(e => e.IsPrimary == true) > 0 ?
                //    currMentee.Person.ContactAddresses.FirstOrDefault(e => e.IsPrimary == true).PostalCode : string.Empty,

                //MailingAddressCountryId = currMentee.Person.ContactAddresses.Count(e => e.IsPrimary == true) > 0
                //    && currMentee.Person.ContactAddresses.FirstOrDefault(e => e.IsPrimary == true).CountryId.HasValue ?
                //    currMentee.Person.ContactAddresses.FirstOrDefault(e => e.IsPrimary == true).CountryId.Value : 1,

                IsTrainingGrantEligible = currMentee.IsTrainingGrantEligible,
                IsUnderrepresentedMinority = currMentee.Person.IsUnderrepresentedMinority,
                IsIndividualWithDisabilities = currMentee.Person.IsIndividualWithDisabilities,
                IsFromDisadvantagedBkgd = currMentee.Person.IsFromDisadvantagedBkgd
            };
            var getGrade = _context.Set<GradeRecord>()
                .Include(g => g.Person)
                .FirstOrDefault(g => g.PersonId == currMentee.PersonId);

            var applicantInformationVM = new AppPreDocViewModel
            {
                Id = id,
                PersonId = currMentee.PersonId
            };

            if(getGrade != null) {
                applicantInformationVM.HasGREScores = false;
                applicantInformationVM.HasMCATScores = false;

                // If any GRE Scores/Percentiles are filled in, set HasGREScores to True
                if (getGrade.GrePercentileAnalytical != null || getGrade.GrePercentileQuantitative != null || getGrade.GrePercentileSubject != null 
                    || getGrade.GrePercentileVerbal != null || getGrade.GreScoreAnalytical != null || getGrade.GreScoreQuantitative != null 
                    || getGrade.GreScoreSubject != null || getGrade.GreScoreVerbal != null)
                {
                    applicantInformationVM.HasGREScores = true;
                }

                // If any MCAT Scores/Percentiles are filled in, set HasMCATScores to True
                if (getGrade.McatPercentile != null || getGrade.McatScoreBiologicalSciences != null || getGrade.McatScorePhysicalSciences != null
                    || getGrade.McatScoreVerbalReasoning != null || getGrade.McatScoreWriting != null)
                {
                    applicantInformationVM.HasMCATScores = true;
                }

                applicantInformationVM.Gpa = getGrade.Gpa;
                applicantInformationVM.GpaScale = getGrade.GpaScale;
                applicantInformationVM.GreScoreVerbal = getGrade.GreScoreVerbal;
                applicantInformationVM.GrePercentileVerbal = getGrade.GrePercentileVerbal;
                applicantInformationVM.GreScoreQuantitative = getGrade.GreScoreQuantitative;
                applicantInformationVM.GrePercentileQuantitative = getGrade.GrePercentileQuantitative;
                applicantInformationVM.GreScoreAnalytical = getGrade.GreScoreAnalytical;
                applicantInformationVM.GrePercentileAnalytical = getGrade.GrePercentileAnalytical;
                applicantInformationVM.GreScoreSubject = getGrade.GreScoreSubject;
                applicantInformationVM.GrePercentileSubject = getGrade.GrePercentileSubject;
                applicantInformationVM.McatScoreVerbalReasoning = getGrade.McatScoreVerbalReasoning;
                applicantInformationVM.McatScorePhysicalSciences = getGrade.McatScorePhysicalSciences;
                applicantInformationVM.McatScoreBiologicalSciences = getGrade.McatScoreBiologicalSciences;
                applicantInformationVM.McatScoreWriting = getGrade.McatScoreWriting;
                applicantInformationVM.McatPercentile = getGrade.McatPercentile;
            };

            // 10/8/15: MPC -- Removed extra database hit -- returned data wasn't even being used!
            // Use to get DateLastUpdate and lastUpdatedBy only
            //var trainingPeriod = commonRepository.GetTPLastUpdateInfo(currMentee.PersonId);

            var menteeTrainingPeriods = new MenteeTrainingPeriodsViewModel
            {
                PersonId = currMentee.PersonId,
                ///MVN - added 9/14/2015 passing Mentee Id into the training period partial view section
                ///
                //Id = mentee.Id,
                DepartmentId = currMentee.DepartmentId.HasValue ? currMentee.DepartmentId.Value : 0,
                Departments = organizationType.Select(o => new KeyValuePair<int, string>(o.Id, o.DisplayName)),
                // *********************************** */

                States = states.Select(s => new KeyValuePair<int, string>(s.Id, s.Abbreviation)),
                Countries = countries.Select(c => new KeyValuePair<int, string>(c.Id, c.Name)),
                Programs = programList.Select(p => new KeyValuePair<Guid, string>(p.Id, p.Title)),
                Mentors = mentorList.Select(m => new KeyValuePair<Guid, string>(m.Id, m.FullName)),

                //////////StrDateLastUpdated = (trainingPeriod != null) ? String.Format(_LastUpdateDATEFORMAT, trainingPeriod.DateLastUpdated) : string.Empty,
                //////////LastUpdatedBy = (trainingPeriod != null) ? trainingPeriod.LastUpdatedBy : string.Empty,
                
                //MenteeTrainingPeriods = currMentee.Person.TrainingPeriods
                //    .Where(tp => tp.IsDeleted == false)
                //    .Select(tp => new MenteeTrainingPeriodViewModel
                //    {
                //        Id = tp.Id,
                //        DoctoralLevelId = tp.DoctoralLevel.Id,
                //        DoctoralLevel = tp.DoctoralLevel.LevelName,
                //        InstitutionName = tp.InstitutionId.HasValue ? tp.Institution.Name : commonRepository.GetDefaultInstitution().Name,
                //        YearStarted = tp.YearStarted,
                //        YearEnded = tp.YearEnded,
                //        AcademicDegreeName = tp.AcademicDegreeId.HasValue ? tp.AcademicDegree.Name : string.Empty,
                //        AcademicDegreeSoughtName = tp.DegreeSoughtId.HasValue ? tp.AcademicDegree.Name:string.Empty,
                //        ResearchProjectTitle = tp.ResearchProjectTitle,
                //        //SelectedPrograms = commonRepository.GetTrainingPeriodProgramList(tp.Id).Select(p => p.ProgramId),
                //        ProgramId = commonRepository.GetTrainingPeriodProgram(tp.Id),

                //        //AcademicDegreeSoughtId = (tp.AcademicDegreeSoughtId != null) ? tp.AcademicDegreeSoughtId : null,

                //        SelectedMentors = commonRepository.GetTrainingPeriodMentorList(tp.Id).Select(m => m.FacultyId)
                //    }
                //)
                
                /* ********************************** MVN modified this 7/28/2015 ************************************* 
                                                      Add academic Ids'           
                 * */
                MenteeTrainingPeriods = currMentee.Person.TrainingPeriods
                    .Where(tp => tp.IsDeleted == false)
                    .Select(tp => new MenteeTrainingPeriodViewModel
                    {
                        Id = tp.Id,
                        DoctoralLevelId = tp.DoctoralLevel.Id,
                        DoctoralLevel = tp.DoctoralLevel.LevelName,
                        InstitutionName = tp.InstitutionId.HasValue ? tp.Institution.Name : commonRepository.GetDefaultInstitution().Name,
                        YearStarted = tp.YearStarted,
                        YearEnded = tp.YearEnded,
                        AcademicDegreeName = tp.AcademicDegreeId.HasValue ? tp.AcademicDegree_AcademicDegreeId.Name : string.Empty,
                        AcademicDegreeSoughtName = tp.DegreeSoughtId.HasValue ? tp.AcademicDegree_DegreeSoughtId.Name : string.Empty,
                        ResearchProjectTitle = tp.ResearchProjectTitle,
                        // MultiSelect code bellow ....
                        //SelectedPrograms = commonRepository.GetTrainingPeriodProgramList(tp.Id).Select(p => p.ProgramId),
                        ProgramId = commonRepository.GetTrainingPeriodProgram(tp.Id),                        
                        SelectedMentors = commonRepository.GetTrainingPeriodMentorList(tp.Id).Select(m => m.FacultyId),
                        StrDateLastUpdated = (tp.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, tp.DateLastUpdated) : string.Empty,
                        StrLastUpdatedBy = commonRepository.GetLoginInfo(tp.LastUpdatedBy)
                    }
                )

            };

            // 10/8/15: MPC -- Removed extra database hit -- returned data wasn't even being used!
            //var academicHistory = commonRepository.GetAHLastUpdateInfo(currMentee.PersonId);
            var academicHistoryInformations = new AcademicsHistoryViewModel
            {
                PersonId = currMentee.PersonId,
                States = states.Select(s => new KeyValuePair<int, string>(s.Id, s.Abbreviation)),
                Countries = countries.Select(c => new KeyValuePair<int, string>(c.Id, c.Name)),
                //StrDateLastUpdated = (academicHistory != null) ? String.Format(_LastUpdateDATEFORMAT, academicHistory.DateLastUpdated) : string.Empty,
                //LastUpdatedBy = (academicHistory != null) ? academicHistory.LastUpdatedBy : string.Empty,
                AcademicHistoryInformations = currMentee.Person.AcademicHistories
                    .Where(ah => ah.IsDeleted == false)
                    .Select(ah => new AcademicHistoryViewModel
                    {
                        Id = ah.Id,
                        //InstitutionName = ah.Institution_InstitutionId == null ? ah.Institution_InstitutionId.Name : commonRepository.GetDefaultInstitution().Name,
                        //InstitutionName = tp.InstitutionId.HasValue ? tp.Institution.Name : commonRepository.GetDefaultInstitution().Name,
                        InstitutionName = ah.Institution_InstitutionId != null ? ah.Institution_InstitutionId.Name : commonRepository.GetDefaultInstitution().Name,
                        YearStarted = ah.YearStarted,
                        YearEnded = ah.YearEnded,
                        AcademicDegreeName = ah.AcademicDegreeId.HasValue ? ah.AcademicDegree.Name : string.Empty,                        
                        AreaOfStudy = ah.AreaOfStudy,
                        YearDegreeCompleted = ah.YearDegreeCompleted,

                        ResearchProjectTitle = ah.ResearchProjectTitle,
                        DoctoralThesis = ah.DoctoralThesis,
                        ResearchAdvisor = ah.ResearchAdvisor,

                        ResidencyInstitutionId = ah.ResidencyInstitutionId,
                        ResidencyTrainingInstName = ah.Institution_ResidencyInstitutionId != null ? ah.Institution_ResidencyInstitutionId.Name : string.Empty,
                        ResidencyAdvisor = ah.ResidencyAdvisor,
                        ResidencyYearStarted = ah.ResidencyYearStarted,
                        ResidencyYearEnded = ah.ResidencyYearEnded,
                        Comments = ah.Comments,
                        StrDateLastUpdated = (ah.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, ah.DateLastUpdated) : string.Empty,
                        StrLastUpdatedBy = commonRepository.GetLoginInfo(ah.LastUpdatedBy)
                    }
                )

            };
            // 10/8/15: MPC -- Removed extra database hit -- returned data wasn't even being used!
            //var menteeSupport = commonRepository.GetMSLastUpdateInfo(currMentee.PersonId);
            var menteeSupports = new MenteeSupportsViewModel
            {
                PersonId = currMentee.PersonId,
                States = states.Select(s => new KeyValuePair<int, string>(s.Id, s.Abbreviation)),
                Countries = countries.Select(c => new KeyValuePair<int, string>(c.Id, c.Name)),
  
                MenteeSupports = currMentee.Person.MenteeSupports
                    .Where(ms => ms.IsDeleted == false)
                    .Select(ms => new MenteeSupportViewModel
                    {
                        Id = ms.Id,
                        //InstitutionName = (ms.InstitutionId != null) ? ms.Institution.Name : commonRepository.GetDefaultInstitution().Name
                        //InstitutionName = ms.Institution.Name,
                        InstitutionName = ms.Institution != null ? ms.Institution.Name : commonRepository.GetDefaultInstitution().Name,
                        SupportSourceText = ms.SupportSourceText,
                        StrDateLastUpdated = (ms.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, ms.DateLastUpdated) : string.Empty,
                        StrLastUpdatedBy = commonRepository.GetLoginInfo(ms.LastUpdatedBy)
                        //MenteeCurrentSupports = new SupportSourcesViewModel
                        //    {
                        //        CurrentSupportSources = ms.SupportSources
                        //            .Where(cs => cs.IsDeleted == false)
                        //            .Select(cs => new SupportSourceViewModel
                        //            {
                        //                Id = cs.Id,
                        //                SupportTitle = cs.SupportTitle
                        //            })
                        //    }
                    }
                )

            };
            // 10/8/15: MPC -- Removed extra database hit -- returned data wasn't even being used!
            //var workHistory = commonRepository.GetWHLastUpdateInfo(currMentee.PersonId);
            var traineePositions = new WorkHistoriesViewModel
            {
                PersonId = currMentee.PersonId,
                States = states.Select(s => new KeyValuePair<int, string>(s.Id, s.Abbreviation)),
                Countries = countries.Select(c => new KeyValuePair<int, string>(c.Id, c.Name)),
                
                
                TraineePositions = currMentee.Person.WorkHistories
                    .Where(tp => tp.IsDeleted == false)
                    .Select(tp => new WorkHistoryViewModel
                    {
                        Id = tp.Id,
                        //InstitutionName = (ms.InstitutionId != null) ? ms.Institution.Name : commonRepository.GetDefaultInstitution().Name
                        InstitutionName = (tp.Institution != null) ? tp.Institution.Name : string.Empty,
                        PositionTitle = tp.PositionTitle,
                        DateStarted = tp.DateStartedPosition,
                        DateStartedText = String.Format(_DATEFORMAT, tp.DateStartedPosition),
                        DateEnded = tp.DateEndedPosition,
                        DateEndedText = String.Format(_DATEFORMAT, tp.DateEndedPosition),
                        SupportSourceText = tp.SupportSourceText,

                        StrDateLastUpdated = (tp.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, tp.DateLastUpdated) : string.Empty,
                        StrLastUpdatedBy = commonRepository.GetLoginInfo(tp.LastUpdatedBy)
                        //PositionSupports = new SupportSourcesViewModel
                        //{
                        //    CurrentSupportSources = tp.SupportSources
                        //        .Where(cs => cs.IsDeleted == false)
                        //        .Select(cs => new SupportSourceViewModel
                        //        {
                        //            Id = cs.Id,
                        //            SupportTitle = cs.SupportTitle
                        //        })
                        //}
                    }
                )

            };

            var menteePublications = new MenteePublicationsViewModel
            {
                PersonId = currMentee.PersonId,
                MenteePublications = currMentee.Person.Publications_AuthorId.Where(p => p.IsDeleted == false)
                .Select(p => new MenteePublicationViewModel
                {
                    Id = p.Id,
                    //PMCId = p.Pmcid,
                    //PMId = p.Pmid,
                    //IsMenteeFirstAuthor = p.IsMenteeFirstAuthor,
                    //IsAdvisorCoAuthor = p.IsAdvisorCoAuthor,
                    //AuthorList = p.AuthorList,
                    //Title = p.Title,
                    //Journal = p.Journal,
                    //DateEpub = p.DateEpub,
                    //Doi = p.Doi,
                    //DatePublicationLastUpdated = p.DatePublicationLastUpdated,
                    YearPublished = p.YearPublished,
                    //Volume = p.Volume,
                    //Issue = p.Issue,
                    //Pagination = p.Pagination,
                    Citation = p.Citation,
                    //Abstract = p.Abstract,
                    StrDateLastUpdated = (p.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, p.DateLastUpdated) : string.Empty,
                    StrLastUpdatedBy = commonRepository.GetLoginInfo(p.LastUpdatedBy)
                })
            };
            HideFields(currMentee.PersonId, id);
            return View(new EditMenteeViewModel
            {
                MenteeGeneralInfo = menteeGeneralVMInfo,
                ApplicantInformation = applicantInformationVM,
                MenteeTrainingPeriods = menteeTrainingPeriods,
                MenteeAcademicHistories = academicHistoryInformations,
                MenteeSupports = menteeSupports,
                TraineePositions = traineePositions,
                MenteePublications = menteePublications,
                MenteeGeneralViewModel = currMentee.Person.Mentees
            });
        }

        public ActionResult UpdateMentee(MenteePostModel updateMenteeModel)
        {
            var mentee = _context.Set<Mentee>()
                .Include(m => m.Person)
                .Single(m => m.PersonId == updateMenteeModel.PersonId);

            mentee.DateLastUpdated = DateTime.Now;

            mentee.Person.FirstName = updateMenteeModel.FirstName;
            mentee.Person.LastName = updateMenteeModel.LastName;
            mentee.Person.MiddleName = updateMenteeModel.MiddleName;
            mentee.StudentId = updateMenteeModel.StudentId;
            mentee.InstitutionAssociationId = updateMenteeModel.InstitutionAssociationId;

            if (updateMenteeModel.DepartmentId > 0)
            {
                mentee.DepartmentId = updateMenteeModel.DepartmentId;
            }

            mentee.IsTrainingGrantEligible = updateMenteeModel.IsTrainingGrantEligible;
            mentee.Person.IsUnderrepresentedMinority = updateMenteeModel.IsUnderrepresentedMinority;
            mentee.Person.IsIndividualWithDisabilities = updateMenteeModel.IsIndividualWithDisabilities;
            mentee.Person.IsFromDisadvantagedBkgd = updateMenteeModel.IsFromDisadvantagedBkgd;

            //////// If there was no Primary Email Address before, a new record will need to be created; otherwise update existing
            //////if (updateMenteeModel.EmailAddress1 != null)
            //////{
            //////    commonRepository.AddUpdateEmailAddress(mentee.PersonId, updateMenteeModel.EmailAddress1, 1, true);
            //////}

            //////// If there was no 2nd Email Address before, a new record will need to be created; otherwise update existing
            //////if (updateMenteeModel.EmailAddress2 != null)
            //////{
            //////    commonRepository.AddUpdateEmailAddress(mentee.PersonId, updateMenteeModel.EmailAddress2, 1, false);
            //////}

            ////// If there was no Phone Number before, a new record will need to be created; otherwise update existing
            ////if (updateMenteeModel.PhoneNumber != null)
            ////{
            ////    commonRepository.AddUpdatePhoneNumber(mentee.PersonId, updateMenteeModel.PhoneNumber, 1, true);
            ////}

            ////// IF ANY of the Address fields are filled in, consider this an address to be added/updated
            ////// If there was no Address before, a new record will need to be created; otherwise update existing
            ////if ((updateMenteeModel.MailingAddressLine1 != null) || (updateMenteeModel.MailingAddressLine2 != null) || (updateMenteeModel.MailingAddressLine3 != null)
            ////    || (updateMenteeModel.MailingAddressCity != null) || (updateMenteeModel.MailingAddressStateId > 0)
            ////    || (updateMenteeModel.MailingAddressPostalCode != null) || (updateMenteeModel.MailingAddressCountryId > 0))
            ////{
            ////    commonRepository.AddUpdateAddress(mentee.PersonId, updateMenteeModel.MailingAddressLine1,
            ////        updateMenteeModel.MailingAddressLine2, updateMenteeModel.MailingAddressLine3, updateMenteeModel.MailingAddressCity,
            ////        updateMenteeModel.MailingAddressStateId, updateMenteeModel.MailingAddressPostalCode, updateMenteeModel.MailingAddressCountryId, 1, true);
            ////}

            // Track last updated date and user
            mentee.DateLastUpdated = DateTime.Now;
            mentee.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);

            _context.SaveChanges();

            return Json(true);
        }

        public ActionResult UpdateApplicantInfo(ApplicantPostModel updateApplicantInfoModel)
        {
            var gradeRecord = new GradeRecord();

            gradeRecord.PersonId = updateApplicantInfoModel.PersonId;
            gradeRecord.Gpa = updateApplicantInfoModel.Gpa;
            gradeRecord.GpaScale = updateApplicantInfoModel.GpaScale;

            commonRepository.AddUpdateGradeRecord(gradeRecord);
            return Json(true);
        }

        public ActionResult SaveGREScores(GradeRecordPostModel updateApplicantInfoModel)
        {
            var gradeRecord = new GradeRecord();

            gradeRecord.PersonId = updateApplicantInfoModel.PersonId;
            gradeRecord.GreScoreVerbal = updateApplicantInfoModel.GreScoreVerbal;
            gradeRecord.GrePercentileVerbal = updateApplicantInfoModel.GrePercentileVerbal;
            gradeRecord.GreScoreQuantitative = updateApplicantInfoModel.GreScoreQuantitative;
            gradeRecord.GrePercentileQuantitative = updateApplicantInfoModel.GrePercentileQuantitative;
            gradeRecord.GreScoreAnalytical = updateApplicantInfoModel.GreScoreAnalytical;
            gradeRecord.GrePercentileAnalytical = updateApplicantInfoModel.GrePercentileAnalytical;
            gradeRecord.GreScoreSubject = updateApplicantInfoModel.GreScoreSubject;
            gradeRecord.GrePercentileSubject = updateApplicantInfoModel.GrePercentileSubject;

            commonRepository.AddUpdateGradeRecord(gradeRecord);
            return Json(true);
        }

        public ActionResult SaveMCATScores(GradeRecordPostModel updateApplicantInfoModel)
        {
            var gradeRecord = new GradeRecord();

            gradeRecord.PersonId = updateApplicantInfoModel.PersonId;
            gradeRecord.McatScoreVerbalReasoning = updateApplicantInfoModel.McatScoreVerbalReasoning;
            gradeRecord.McatScorePhysicalSciences = updateApplicantInfoModel.McatScorePhysicalSciences;
            gradeRecord.McatScoreBiologicalSciences = updateApplicantInfoModel.McatScoreBiologicalSciences;
            gradeRecord.McatScoreWriting = updateApplicantInfoModel.McatScoreWriting;
            gradeRecord.McatPercentile = updateApplicantInfoModel.McatPercentile;

            commonRepository.AddUpdateGradeRecord(gradeRecord);
            return Json(true);
        }



        public ActionResult UpdateTrainingPeriod(TrainingPeriodPostModel updateTrainingPeriodModel)
        {
            //check if it's a new or existing Training Period
            bool isNew = updateTrainingPeriodModel.Id.Equals(null) || !_context.Set<TrainingPeriod>()
                .Any(o => o.Id == updateTrainingPeriodModel.Id);

            var trainingPeriod = isNew
                ? new TrainingPeriod()
                : _context.Set<TrainingPeriod>()
                .Include(tp => tp.Programs)
                .Include(tp => tp.Faculties)
                .Single(tp => tp.Id == updateTrainingPeriodModel.Id);

            if (_context.Set<AcademicDegree>().Any(ac => ac.Name == updateTrainingPeriodModel.AcademicDegree))
            {
                AcademicDegree academicDegree = _context.Set<AcademicDegree>()
                    .Single(ac => ac.Name == updateTrainingPeriodModel.AcademicDegree);
                trainingPeriod.AcademicDegreeId = academicDegree.Id;
            }

            if (_context.Set<AcademicDegree>().Any(ac => ac.Name == updateTrainingPeriodModel.DegreeSought))
            {
                AcademicDegree degreeSought = _context.Set<AcademicDegree>()
                    .Single(ac => ac.Name == updateTrainingPeriodModel.DegreeSought);
                trainingPeriod.DegreeSoughtId = degreeSought.Id;
            }

            trainingPeriod.DoctoralLevelId = updateTrainingPeriodModel.DoctoralLevelId;
            trainingPeriod.YearStarted = updateTrainingPeriodModel.YearStarted;
            trainingPeriod.YearEnded = updateTrainingPeriodModel.YearEnded;
            trainingPeriod.ResearchProjectTitle = updateTrainingPeriodModel.ResearchProjectTitle;

            Institution institution = null;

            if (_context.Set<Institution>().Any(ac => ac.Name == updateTrainingPeriodModel.Institution))
            {
                institution = _context.Set<Institution>().Single(ac => ac.Name == updateTrainingPeriodModel.Institution);
                trainingPeriod.InstitutionId = institution.Id;
            }

            if ((updateTrainingPeriodModel.ProgramId != Guid.Empty) && (updateTrainingPeriodModel.ProgramId != null))
            {
                // If this Training Period already has a program record, then just remove it and add the new one
                // If not, create a new one in the TrainingPeriodProgram table
                var addProgram = _context.Set<Program>().Find(updateTrainingPeriodModel.ProgramId);

                if (trainingPeriod.Programs.Count > 0)
                {
                    var remProgram = _context.Set<Program>().Find(trainingPeriod.Programs.FirstOrDefault().Id);
                    trainingPeriod.Programs.Remove(remProgram);
                }
                trainingPeriod.Programs.Add(addProgram);
            }

            // *** CODE BELOW IS FOR MULTI-SELECT PROGRAMS (LEAVE IN CASE THEY WANT TO CHANGE IT BACK) ***
            //var savedPrograms = trainingPeriod.Programs.ToList();

            //// ADD newly selected Programs to database
            //if (updateTrainingPeriodModel.TrainingPeriodPrograms != null)
            //{
            //    foreach (var selectedProgramId in updateTrainingPeriodModel.TrainingPeriodPrograms)
            //    {
            //        if (trainingPeriod.Programs.All(trp => trp.Id != selectedProgramId))
            //        {
            //            var addProgram = _context.Set<Program>().Find(selectedProgramId);
            //            trainingPeriod.Programs.Add(addProgram);
            //        }
            //    }
            //}

            // DELETE previously-saved, just removed Programs from database
            //foreach (var savedProgram in savedPrograms)
            //{
            //    if (!updateTrainingPeriodModel.TrainingPeriodPrograms.Contains(savedProgram.Id))
            //    {
            //        var remProgram = _context.Set<Program>().Find(savedProgram.Id);
            //        trainingPeriod.Programs.Remove(remProgram);
            //    }
            //}
            // *** ***

            var savedMentors = trainingPeriod.Faculties.ToList();

            // ADD newly selected Mentors to database
            if (updateTrainingPeriodModel.TrainingPeriodMentors != null)
            {
                foreach (var selectedMentorId in updateTrainingPeriodModel.TrainingPeriodMentors)
                {
                    if (trainingPeriod.Faculties.All(trp => trp.Id != selectedMentorId))
                    {
                        //var addMentor = _context.Set<Faculty>().First(f => f.Id == selectedMentorId && f.IsDeleted == false);
                        var addMentor = _context.Set<Faculty>().Find(selectedMentorId);
                        trainingPeriod.Faculties.Add(addMentor);
                    }
                }
            }

            // DELETE previously-saved, just removed Mentors from database
            foreach (var savedMentor in savedMentors)
            {
                if (!updateTrainingPeriodModel.TrainingPeriodMentors.Contains(savedMentor.Id))
                {
                    //var remMentor = _context.Set<Faculty>().First(f => f.Id == savedMentor.Id && f.IsDeleted == false);//.Find(savedMentor.Id);
                    var remMentor = _context.Set<Faculty>().Find(savedMentor.Id);
                    trainingPeriod.Faculties.Remove(remMentor);
                }
            }

            trainingPeriod.PersonId = updateTrainingPeriodModel.PersonId;
            if (isNew)
            {
                _context.Set<TrainingPeriod>().Add(trainingPeriod);
            }
            // Track last updated date and user
            trainingPeriod.DateLastUpdated = DateTime.Now;
            trainingPeriod.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
            _context.SaveChanges();
            /// mVn -> code working to resolve program and department selection issue
            var programList = commonRepository.GetProgramList().ToList();
            // Update grid with updated/new data????
            var trainingPeriodViewModel = new MenteeTrainingPeriodViewModel
            {
                Id = trainingPeriod.Id,
                PersonId = trainingPeriod.PersonId,
                DoctoralLevelId = trainingPeriod.DoctoralLevelId,
                DoctoralLevel = commonRepository.GetMenteeType(trainingPeriod.DoctoralLevelId),
                AcademicDegreeId = updateTrainingPeriodModel.AcademicDegreeId.HasValue ? updateTrainingPeriodModel.AcademicDegreeId : null,
                AcademicDegreeName = updateTrainingPeriodModel.AcademicDegree,

                AcademicDegreeSoughtId = updateTrainingPeriodModel.DegreeSoughtId.HasValue ? updateTrainingPeriodModel.DegreeSoughtId : null,
                AcademicDegreeSoughtName = updateTrainingPeriodModel.AcademicDegreeSought,

                InstitutionId = institution != null ? institution.Id : 0,
                InstitutionName = institution != null ? institution.Name : string.Empty,
                YearStarted = updateTrainingPeriodModel.YearStarted.HasValue ? updateTrainingPeriodModel.YearStarted : null,
                YearEnded = updateTrainingPeriodModel.YearEnded.HasValue ? updateTrainingPeriodModel.YearEnded : null,
                ResearchProjectTitle = trainingPeriod.ResearchProjectTitle,
                //SelectedPrograms = commonRepository.GetTrainingPeriodProgramList(trainingPeriod.Id).Select(p => p.ProgramId),
                //ProgramId = updateTrainingPeriodModel.ProgramId,
                ProgramId = commonRepository.GetTrainingPeriodProgram(trainingPeriod.Id),
                SelectedMentors = commonRepository.GetTrainingPeriodMentorList(trainingPeriod.Id).Select(m => m.FacultyId),
                /// MVN added 9/17/2015 tracking user activity 
                StrDateLastUpdated = (trainingPeriod.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, trainingPeriod.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(trainingPeriod.LastUpdatedBy),
                /// mVn - working to resolve selection of program and department check issue
                Programs = programList.Select(p => new KeyValuePair<Guid, string>(p.Id, p.Title)),
            };
            return new JsonCamelCaseResult(trainingPeriodViewModel, JsonRequestBehavior.AllowGet);

        }

        public ActionResult DeleteTrainingPeriod(Guid id)
        {
            var trainingPeriod = _context.Set<TrainingPeriod>().Find(id);
            if (trainingPeriod != null)
            {
                trainingPeriod.IsDeleted = true;
                _context.SaveChanges();
            }
            return Json(id);
        }

        public ActionResult UpdateAcademicHistory(AcademicHistoryPostModel updateAcademicHistoryModel)
        {
            //check if it's a new or existing Training Period
            bool isNew = updateAcademicHistoryModel.Id.Equals(null) || !_context.Set<AcademicHistory>()
                .Any(o => o.Id == updateAcademicHistoryModel.Id);

            var academicHistory = isNew
                ? new AcademicHistory()
                : _context.Set<AcademicHistory>()
                .Single(tp => tp.Id == updateAcademicHistoryModel.Id);

            Institution institution = null;
            if (updateAcademicHistoryModel.Institution != null)
            {
                if (_context.Set<Institution>().Any(ac => ac.Name == updateAcademicHistoryModel.Institution))
                {
                    institution = _context.Set<Institution>().Single(ac => ac.Name == updateAcademicHistoryModel.Institution);
                    academicHistory.InstitutionId = institution.Id;
                }
            }
            
            academicHistory.YearStarted = updateAcademicHistoryModel.YearStarted;
            academicHistory.YearEnded = updateAcademicHistoryModel.YearEnded;

            AcademicDegree academicDegree = null;
            if (updateAcademicHistoryModel.AcademicDegree != null)
            {
                if (_context.Set<AcademicDegree>().Any(ac => ac.Name == updateAcademicHistoryModel.AcademicDegree))
                {
                    academicDegree = _context.Set<AcademicDegree>()
                        .Single(ac => ac.Name == updateAcademicHistoryModel.AcademicDegree);
                    academicHistory.AcademicDegreeId = academicDegree.Id;
                }
            }

            academicHistory.AreaOfStudy = updateAcademicHistoryModel.AreaOfStudy;
            academicHistory.YearDegreeCompleted = updateAcademicHistoryModel.YearDegreeCompleted;

            academicHistory.ResearchProjectTitle = updateAcademicHistoryModel.ResearchProjectTitle;
            academicHistory.DoctoralThesis = updateAcademicHistoryModel.DoctoralThesis;
            academicHistory.ResearchAdvisor = updateAcademicHistoryModel.ResearchAdvisor;

            Institution residencyInstitution = null;
            if (updateAcademicHistoryModel.ResidencyInstitution != null)
            {
                if (_context.Set<Institution>().Any(ac => ac.Name == updateAcademicHistoryModel.ResidencyInstitution))
                {
                    residencyInstitution = _context.Set<Institution>().Single(ac => ac.Name == updateAcademicHistoryModel.ResidencyInstitution);
                    academicHistory.ResidencyInstitutionId = residencyInstitution.Id;
                    academicHistory.IsResidency = true;
                }
            }

            academicHistory.ResidencyAdvisor = updateAcademicHistoryModel.ResidencyAdvisor;
            academicHistory.ResidencyYearStarted = updateAcademicHistoryModel.ResidencyYearStarted;
            academicHistory.ResidencyYearEnded = updateAcademicHistoryModel.ResidencyYearEnded;

            academicHistory.Comments = updateAcademicHistoryModel.Comments;

            // Track last updated date and user
            academicHistory.DateLastUpdated = DateTime.Now;
            academicHistory.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);

            academicHistory.PersonId = updateAcademicHistoryModel.PersonId;
            if (isNew)
            {
                _context.Set<AcademicHistory>().Add(academicHistory);
            }
            _context.SaveChanges();

            // Update grid with updated/new data????
            var academicHistoryViewModel = new AcademicHistoryViewModel
            {
                Id = academicHistory.Id,

                InstitutionName = academicHistory.Institution_InstitutionId.Name,
                YearStarted = updateAcademicHistoryModel.YearStarted,
                YearEnded = updateAcademicHistoryModel.YearEnded,
                AcademicDegreeName = academicHistory.AcademicDegreeId.HasValue ? academicHistory.AcademicDegree.Name : string.Empty,
                AreaOfStudy = updateAcademicHistoryModel.AreaOfStudy,
                YearDegreeCompleted = updateAcademicHistoryModel.YearDegreeCompleted,

                ResearchProjectTitle = updateAcademicHistoryModel.ResearchProjectTitle,
                DoctoralThesis = updateAcademicHistoryModel.DoctoralThesis,
                ResearchAdvisor = updateAcademicHistoryModel.ResearchAdvisor,

                ResidencyInstitutionId = residencyInstitution != null ? residencyInstitution.Id : 0,
                ResidencyTrainingInstName = residencyInstitution != null ? residencyInstitution.Name : string.Empty,
                ResidencyAdvisor = updateAcademicHistoryModel.ResidencyAdvisor,
                ResidencyYearStarted = updateAcademicHistoryModel.ResidencyYearStarted,
                ResidencyYearEnded = updateAcademicHistoryModel.ResidencyYearEnded,
                Comments = academicHistory.Comments,
                /// MVN - added 9/17/2015 tracking info
                StrDateLastUpdated = (academicHistory.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, academicHistory.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(academicHistory.LastUpdatedBy)
            };
            return new JsonCamelCaseResult(academicHistoryViewModel, JsonRequestBehavior.AllowGet);

        }

        public ActionResult DeleteAcademicHistory(Guid id)
        {
            var academicHistory = _context.Set<AcademicHistory>().Find(id);
            if (academicHistory != null)
            {
                academicHistory.IsDeleted = true;
                _context.SaveChanges();
            }
            return Json(id);
        }
        // MVN - Commented out 11/26/2014 - due to exception error in 650 as Id 00000000
        //public ActionResult UpdatePublication(PublicationPostModel updatePublicationModel)
        //{
        //    //check if it's a new or existing Publication
        //    bool isNew = true; //////////  ?????!updatePublicationModel.Id.HasValue || !_context.Set<Publication>().Any(p => p.Id == updatePublicationModel.Id);
        //    var publication = isNew
        //        ? new Publication()
        //        : _context.Set<Publication>().Find(updatePublicationModel.Id);
        //    //publication.DateEntered = updatePublicationModel.DateEntered;
        //    //publication.PMID = updatePublicationModel.PMId;



        /*  PURPOSE: Updates Sub-Form: Current Mentee Current Support; 
         *                   Db Table: MenteeSupport  */
        public ActionResult UpdateMenteeSupport(MenteeSupportPostModel updateMenteeSupportModel)
        {
            //check if it's a new or existing Mentee Support
            bool isNew = updateMenteeSupportModel.Id.Equals(null) || !_context.Set<MenteeSupport>()
                .Any(o => o.Id == updateMenteeSupportModel.Id);

            var menteeSupport = isNew
                ? new MenteeSupport()
                : _context.Set<MenteeSupport>()
                .Single(ms => ms.Id == updateMenteeSupportModel.Id);

            Institution institution = null;
            if (updateMenteeSupportModel.Institution != null)
            {
                if (_context.Set<Institution>().Any(ms => ms.Name == updateMenteeSupportModel.Institution))
                {
                    institution = _context.Set<Institution>().Single(ms => ms.Name == updateMenteeSupportModel.Institution);
                    menteeSupport.InstitutionId = institution.Id;
                }
            }

            menteeSupport.SupportSourceText = updateMenteeSupportModel.SupportSourceText;

            // Track last updated date and user
            menteeSupport.DateLastUpdated = DateTime.Now;
            menteeSupport.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);


            menteeSupport.PersonId = updateMenteeSupportModel.PersonId;
            if (isNew)
            {
                _context.Set<MenteeSupport>().Add(menteeSupport);
            }
            _context.SaveChanges();
            
            // Update grid with updated/new data????
            var menteeSupportViewModel = new MenteeSupportViewModel
            {
                Id = menteeSupport.Id,
                InstitutionName = menteeSupport.Institution.Name,
                SupportSourceText = menteeSupport.SupportSourceText,
                /// MVN -> 9/17/2015 added tracking info here>>>>
                StrDateLastUpdated = (menteeSupport.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, menteeSupport.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(menteeSupport.LastUpdatedBy)
            };
            
            return new JsonCamelCaseResult(menteeSupportViewModel, JsonRequestBehavior.AllowGet);

        }


        /**** NOT CURRENTLY WORKING - WAS TAKING TOO LONG SO SWITCHED TO SUPPORT SOURCE TEXT FIELD ************/
        /* 
         * PURPOSE: Add/Update a Support Source, and if it's a new one, attach it to the MenteeSupport record
         * UPDATES: Popup: Add/Update Support Information 
         *              ON Sub-Form: Current Mentee Current Support; 
         *          Db Tables:  SupportSource
         *                      MenteeSupportSupportSource  
         *****************************************************/
        //public ActionResult UpdateCurrentSupportSource(MenteeSupportPostModel updateCurrentSupportModel)
        //{
        //    var currUser = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
            
        //    // Make sure Support Source info has been passed in before saving anything
        //    if (updateCurrentSupportModel == null)
        //    {
        //        return new JsonCamelCaseResult(null, JsonRequestBehavior.AllowGet);
        //    }

        //    //check if it's a new or existing Support Source record
        //    bool isNewSS = updateCurrentSupportModel.CurrentSupportSourceId.Equals(null) || !_context.Set<SupportSource>()
        //        .Any(ss => ss.Id == updateCurrentSupportModel.CurrentSupportSourceId);

        //    var supportSource = isNewSS
        //        ? new SupportSource()
        //        : _context.Set<SupportSource>()
        //        .Single(ss => ss.Id == updateCurrentSupportModel.CurrentSupportSourceId);

        //    supportSource.SupportTitle = updateCurrentSupportModel.CurrentSupportSourceTitle;

        //    // Track last updated date and user
        //    supportSource.DateLastUpdated = DateTime.Now;
        //    supportSource.LastUpdatedBy = currUser;

        //    // Save/Create the Support Source
        //    if (isNewSS)
        //    {
        //        _context.Set<SupportSource>().Add(supportSource);

        //    }
        //    _context.SaveChanges();


        //    // Grab the Mentee Support record that this Support Source will be associated with
        //    var menteeSupport = _context.Set<MenteeSupport>()
        //                            .Single(ms => ms.Id == updateCurrentSupportModel.Id);

        //    if (isNewSS)
        //    {
        //        // Add the new Support Source to the Mentee Support record
        //        menteeSupport.SupportSources.Add(supportSource);
        //    }

        //    // Track last updated date and user
        //    menteeSupport.DateLastUpdated = DateTime.Now;
        //    menteeSupport.LastUpdatedBy = currUser;

        //    _context.SaveChanges();

        //    var currentSupportSources = new SupportSourceViewModel
        //    {
        //        Id = supportSource.Id,
        //        SupportTitle = supportSource.SupportTitle,
        //        IsNew = isNewSS
        //    };

        //    //var menteeSupports = new MenteeSupportsViewModel
        //    //{
        //    //    PersonId = menteeSupport.PersonId,
        //    //    SelectedId = menteeSupport.Id,
        //    //    SelectedSupportSourceId = supportSource.Id,
        //    //    MenteeSupports = menteeSupport.Person.MenteeSupports
        //    //        .Where(ms => ms.IsDeleted == false)
        //    //        .Select(ms => new MenteeSupportViewModel
        //    //        {
        //    //            Id = ms.Id,
        //    //            //InstitutionName = (ms.InstitutionId != null) ? ms.Institution.Name : commonRepository.GetDefaultInstitution().Name
        //    //            InstitutionName = ms.Institution.Name,
        //    //            MenteeCurrentSupports = new SupportSourcesViewModel
        //    //            {
        //    //                CurrentSupportSources = ms.SupportSources
        //    //                    .Where(cs => cs.IsDeleted == false)
        //    //                    .Select(cs => new SupportSourceViewModel
        //    //                    {
        //    //                        Id = cs.Id,
        //    //                        SupportTitle = cs.SupportTitle
        //    //                    })
        //    //            }
        //    //        }
        //    //    )

        //    //};

        //    return new JsonCamelCaseResult(currentSupportSources, JsonRequestBehavior.AllowGet);          

        //}

        public ActionResult DeleteMenteeSupport(int id)
        {
            var menteeSupport = _context.Set<MenteeSupport>().Find(id);
            if (menteeSupport != null)
            {
                menteeSupport.IsDeleted = true;
                _context.SaveChanges();
            }
            return Json(id);
        }

        public ActionResult DeleteSupportSource(int id)
        {
            var supportSource = _context.Set<SupportSource>().Find(id);
            if (supportSource != null)
            {
                supportSource.IsDeleted = true;
                _context.SaveChanges();
            }
            return Json(id);
        }

        /*  PURPOSE: Updates Sub-Form: Past Trainee Current Position; 
         *                   Db Table: WorkHistory  */
        public ActionResult UpdateTraineePosition(WorkHistoryPostModel updateTraineePositionModel)
        {
            //check if it's a new or existing Work History record
            bool isNew = updateTraineePositionModel.Id.Equals(null) || !_context.Set<WorkHistory>()
                .Any(o => o.Id == updateTraineePositionModel.Id);

            var traineePosition = isNew
                ? new WorkHistory()
                : _context.Set<WorkHistory>()
                .Single(ms => ms.Id == updateTraineePositionModel.Id);

            Institution institution = null;
            if (updateTraineePositionModel.Institution != null)
            {
                if (_context.Set<Institution>().Any(ms => ms.Name == updateTraineePositionModel.Institution))
                {
                    institution = _context.Set<Institution>().Single(ms => ms.Name == updateTraineePositionModel.Institution);
                    traineePosition.InstitutionId = institution.Id;
                }
            }

            traineePosition.PositionTitle = updateTraineePositionModel.PositionTitle;
            traineePosition.DateStartedPosition = updateTraineePositionModel.DateStarted;
            traineePosition.DateEndedPosition = updateTraineePositionModel.DateEnded;
            traineePosition.SupportSourceText = updateTraineePositionModel.SupportSourceText;

            // Track last updated date and user
            traineePosition.DateLastUpdated = DateTime.Now;
            traineePosition.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);

            traineePosition.PersonId = updateTraineePositionModel.PersonId;
            if (isNew)
            {
                _context.Set<WorkHistory>().Add(traineePosition);
            }
            _context.SaveChanges();


            // Update grid with updated/new data????
            var traineePositionVM = new WorkHistoryViewModel
            {
                Id = traineePosition.Id,
                InstitutionName = (traineePosition.Institution != null) ? traineePosition.Institution.Name : null,
                PositionTitle = traineePosition.PositionTitle,
                DateStarted = traineePosition.DateStartedPosition,
                DateStartedText = String.Format(_DATEFORMAT, traineePosition.DateStartedPosition),
                DateEnded = traineePosition.DateEndedPosition,
                DateEndedText = String.Format(_DATEFORMAT, traineePosition.DateEndedPosition),
                SupportSourceText = traineePosition.SupportSourceText,

                /// MVN -> 9/17/2015 added tracking info here>>>>
                StrDateLastUpdated = (traineePosition.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, traineePosition.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(traineePosition.LastUpdatedBy)
            };

            return new JsonCamelCaseResult(traineePositionVM, JsonRequestBehavior.AllowGet);

        }


        /**** NOT CURRENTLY WORKING - WAS TAKING TOO LONG SO SWITCHED TO SUPPORT SOURCE TEXT FIELD ************/
        /*****************************************************
         * PURPOSE: Add/Update a Support Source, and if it's a new one, attach it to the WorkHistory record
         * UPDATES: Popup: Add/Update Support Information 
         *              ON Sub-Form: Past Trainee Current Position; 
         *          Db Tables:  SupportSource
         *                      WorkHistorySupportSource  
         *****************************************************/
        //public ActionResult UpdatePositionSupportSource(WorkHistoryPostModel updatePositionSupportModel)
        //{
        //    var currUser = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);

        //    // Make sure Support Source info has been passed in before saving anything
        //    if (updatePositionSupportModel == null)
        //    {
        //        return new JsonCamelCaseResult(null, JsonRequestBehavior.AllowGet);
        //    }

        //    //check if it's a new or existing Support Source record
        //    bool isNewSS = updatePositionSupportModel.CurrentSupportSourceId.Equals(null) || !_context.Set<SupportSource>()
        //        .Any(ss => ss.Id == updatePositionSupportModel.CurrentSupportSourceId);

        //    var supportSource = isNewSS
        //        ? new SupportSource()
        //        : _context.Set<SupportSource>()
        //        .Single(ss => ss.Id == updatePositionSupportModel.CurrentSupportSourceId);

        //    supportSource.SupportTitle = updatePositionSupportModel.CurrentSupportSourceTitle;

        //    // Track last updated date and user
        //    supportSource.DateLastUpdated = DateTime.Now;
        //    supportSource.LastUpdatedBy = currUser;

        //    // Save/Create the Support Source
        //    if (isNewSS)
        //    {
        //        _context.Set<SupportSource>().Add(supportSource);

        //    }
        //    _context.SaveChanges();


        //    // Grab the Work History record that this Support Source will be associated with
        //    var workHistory = _context.Set<WorkHistory>()
        //                            .Single(ms => ms.Id == updatePositionSupportModel.Id);

        //    if (isNewSS)
        //    {
        //        // Add the new Support Source to the Mentee Support record
        //        workHistory.SupportSources.Add(supportSource);
        //    }

        //    // Track last updated date and user
        //    workHistory.DateLastUpdated = DateTime.Now;
        //    workHistory.LastUpdatedBy = currUser;

        //    _context.SaveChanges();

        //    var currentSupportSources = new SupportSourceViewModel
        //    {
        //        Id = supportSource.Id,
        //        SupportTitle = supportSource.SupportTitle
        //    };

        //    return new JsonCamelCaseResult(currentSupportSources, JsonRequestBehavior.AllowGet);

        //}

        public ActionResult DeleteTraineePosition(int id)
        {
            var traineePosition = _context.Set<WorkHistory>().Find(id);
            if (traineePosition != null)
            {
                traineePosition.IsDeleted = true;
                _context.SaveChanges();
            }
            return Json(id);
        }


        //[HttpPost]
        public ActionResult UpdatePublications(PublicationPostModel updatePublicationModel)
        {
            bool isNew = updatePublicationModel.Id.Equals(null) || !_context.Set<Publication>()
               .Any(p => p.Id == updatePublicationModel.Id);
    
            var publication = isNew
                ? new Publication()
                : _context.Set<Publication>()
                .Find(updatePublicationModel.Id);
     
            //publication.Pmcid = updatePublicationModel.Pmcid;
            //publication.Pmid = updatePublicationModel.Pmid;
            //publication.IsMenteeFirstAuthor = updatePublicationModel.IsMenteeFirstAuthor;
            //publication.Title = updatePublicationModel.Title;
            //publication.IsAdvisorCoAuthor = updatePublicationModel.IsAdvisorCoAuthor;
            //publication.AuthorList = updatePublicationModel.AuthorList;
            //publication.Journal = updatePublicationModel.Journal;
            //publication.DateEpub = updatePublicationModel.DateEpub;
            //publication.Doi = updatePublicationModel.Doi;
            publication.YearPublished = updatePublicationModel.YearPublished;
            //publication.Volume = updatePublicationModel.Volume;
            //publication.Issue = updatePublicationModel.Issue;
            //publication.Pagination = updatePublicationModel.Pagination;
            publication.Citation = updatePublicationModel.Citation;
            //publication.Abstract = updatePublicationModel.Abstract;
            /// MVN - added tracking user activity 9/30/2015
            // Track last updated date and user
            publication.DateLastUpdated = DateTime.Now;
            publication.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);

            publication.AuthorId = updatePublicationModel.PersonId;
            if (isNew)
            {
                _context.Set<Publication>().Add(publication);
            }
            _context.SaveChanges();

            var updateMenteeTotheGrid = new MenteePublicationViewModel
            {
                Id = publication.Id,
                AuthorId = updatePublicationModel.PersonId,
                //PMCId = updatePublicationModel.Pmcid,
                //PMId = updatePublicationModel.Pmid,
                //IsMenteeFirstAuthor = updatePublicationModel.IsMenteeFirstAuthor,
                //Title = updatePublicationModel.Title,
                //IsAdvisorCoAuthor = updatePublicationModel.IsAdvisorCoAuthor,
                //AuthorList = updatePublicationModel.AuthorList,
                //Journal = updatePublicationModel.Journal,
                //DateEpub = updatePublicationModel.DateEpub,
                //Doi = updatePublicationModel.Doi,
                YearPublished = updatePublicationModel.YearPublished,
                //Volume = updatePublicationModel.Volume,
                //Issue = updatePublicationModel.Issue,
                //Pagination = updatePublicationModel.Pagination,
                Citation = updatePublicationModel.Citation,
                //Abstract = updatePublicationModel.Abstract,

                /// MVN -> 9/17/2015 added tracking info here>>>>
                StrDateLastUpdated = (publication.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, publication.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(publication.LastUpdatedBy)
            };

            //Return back to the UI and Allow GET request-Essentially refresh grid
            return new JsonCamelCaseResult(updateMenteeTotheGrid, JsonRequestBehavior.AllowGet);
        }

        ///// <summary>
        ///// PubMed Publication search using the NIH.Gov website
        ///// </summary>
        ///// <returns></returns>
        //[HttpGet]
        //public ActionResult PubMedSearchResult()
        //{
        //    var pubMed = new List<object>();


        //    //pubMed.Add(new { Title = "Ghostbusters", Genre = "Comedy", Year = 1984 });
        //    //pubMed.Add(new { Title = "Gone with Wind", Genre = "Drama", Year = 1939 });
        //    //pubMed.Add(new { Title = "Star Wars", Genre = "Science Fiction", Year = 1977 });

        //    return Json(pubMed, JsonRequestBehavior.AllowGet);
        //}
        ///// <summary>
        ///// I am still working on this!!!
        ///// I might need this for XML
        ///// </summary>
        ///// <param name="serviceUrl"></param>
        ///// <returns></returns>
 
        //public string getServiceResult(string serviceUrl)
        //{
        //    HttpWebRequest HttpWReq;
        //    HttpWebResponse HttpWResp;
        //    HttpWReq = (HttpWebRequest)WebRequest.Create(serviceUrl);
        //    HttpWReq.Method = "GET";
        //    HttpWResp = (HttpWebResponse)HttpWReq.GetResponse();
        //    if (HttpWResp.StatusCode == HttpStatusCode.OK)
        //    {
        //        //Consume webservice with basic XML reading, assumes it returns (one) string
        //        XmlReader reader = XmlReader.Create(HttpWResp.GetResponseStream());
        //        while (reader.Read())
        //        {
        //            reader.MoveToFirstAttribute();
        //            if (reader.NodeType == XmlNodeType.Text)
        //            {
        //                return reader.Value;
        //            }
        //        }
        //        return String.Empty;
        //    }
        //    else
        //    {
        //        throw new Exception("Error on remote IP to Country service: " + HttpWResp.StatusCode.ToString());
        //    }
        //}

        [HttpPost]
        public ActionResult DeletePublication(Guid id)
        {
            var publication = _context.Set<Publication>().Find(id);
            if (publication != null)
            {
                publication.IsDeleted = true;
                //_context.Set<Publication>().Remove(publication);
                _context.SaveChanges();
            }
            return Json(id);
        }

        // GET: Mentee/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Mentee/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult PostMentee(string saveButton, string menteeMidName, string menteeFName, string menteeLName, int? studentId)
        {
            Mentee mentee = new Mentee();

            if (ModelState.IsValid)
            {
                if (!String.IsNullOrEmpty(studentId.ToString()) ||
                    !String.IsNullOrEmpty(menteeLName) ||
                    !String.IsNullOrEmpty(menteeFName)
                    )
                {
                    var result = menteeRepository.getMenteeById(studentId);

                    // THIS IS FOR FUTURE EXPANSION
                    if (result != null)
                    {
                        // RECORD FOUND - flag active or not active
                        //faculty.Id = Id.ToString();
                        //result.Id = facultyRepository.getFacultyByID(Id);
                        mentee.StudentId = studentId.ToString();
                        mentee.Person.LastName = menteeLName;
                        mentee.Person.MiddleName = menteeMidName;
                        mentee.Person.FirstName = menteeFName;
                        //result = menteeRepository.getMenteeByID(facultyRankId);

                        menteeRepository.AddUpdateMentee(result);
                        return RedirectToAction("Index");

                    }
                    // NO RECORD FOUND
                    mentee.StudentId = studentId.ToString();
                    mentee.Person.LastName = menteeLName;
                    mentee.Person.MiddleName = menteeMidName;
                    mentee.Person.FirstName = menteeFName;
                    //menteeRepository.getFacultyByID(facultyRankId);
                    menteeRepository.AddUpdateMentee(mentee);
                    return RedirectToAction("Index");
                }
            }
            return View("Index");
        }
        ///ToDO Add Hiding method here
    }
}

﻿//using System;
//using System.Data.Entity;
//using System.Linq;
//using System.Net;
//using System.Web.Mvc;
//using TraineeTrackingSystem.Data;

//namespace TraineeTrackingSystem.Web.Controllers
//{
//    public class StaffController : Controller
//    {
//        private TTSEntities db = new TTSEntities();

//        // GET: /Staff/
//        public ActionResult Index()
//        {
//            var faculties = db.Faculties.Include(f => f.FacultyRank).Include(f => f.Person).Include(f => f.Organization).Include(f => f.Organization1).Include(f => f.Person1);
//            return View(faculties.ToList());
//        }

//        // GET: /Staff/Details/5
//        public ActionResult Details(Guid? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            Faculty faculty = db.Faculties.Find(id);
//            if (faculty == null)
//            {
//                return HttpNotFound();
//            }
//            return View(faculty);
//        }

//        // GET: /Staff/Create
//        public ActionResult Create()
//        {
//            ViewBag.FacultyRankId = new SelectList(db.FacultyRanks, "Id", "Rank");
//            ViewBag.PersonId = new SelectList(db.People, "PersonId", "FirstName");
//            ViewBag.PrimaryOrganizationId = new SelectList(db.Organizations, "Id", "OrganizationId");
//            ViewBag.SecondaryOrganizationId = new SelectList(db.Organizations, "Id", "OrganizationId");
//            ViewBag.CreatedBy = new SelectList(db.People, "PersonId", "FirstName");
//            return View();
//        }

//        // POST: /Staff/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include="Id,PersonId,EmployeeId,FacultyRankId,PrimaryOrganizationId,SecondaryOrganizationId,OtherAffliliations,OtherTitles,CreatedBy")] Faculty faculty)
//        {
//            if (ModelState.IsValid)
//            {
//                faculty.Id = Guid.NewGuid();
//                db.Faculties.Add(faculty);
//                db.SaveChanges();
//                return RedirectToAction("Index");
//            }

//            ViewBag.FacultyRankId = new SelectList(db.FacultyRanks, "Id", "Rank", faculty.FacultyRankId);
//            ViewBag.PersonId = new SelectList(db.People, "PersonId", "FirstName", faculty.PersonId);
//            ViewBag.PrimaryOrganizationId = new SelectList(db.Organizations, "Id", "OrganizationId", faculty.PrimaryOrganizationId);
//            ViewBag.SecondaryOrganizationId = new SelectList(db.Organizations, "Id", "OrganizationId", faculty.SecondaryOrganizationId);
//            ViewBag.CreatedBy = new SelectList(db.People, "PersonId", "FirstName", faculty.CreatedBy);
//            return View(faculty);
//        }

//        // GET: /Staff/Edit/5
//        public ActionResult Edit(Guid? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            Faculty faculty = db.Faculties.Find(id);
//            if (faculty == null)
//            {
//                return HttpNotFound();
//            }
//            ViewBag.FacultyRankId = new SelectList(db.FacultyRanks, "Id", "Rank", faculty.FacultyRankId);
//            ViewBag.PersonId = new SelectList(db.People, "PersonId", "FirstName", faculty.PersonId);
//            ViewBag.PrimaryOrganizationId = new SelectList(db.Organizations, "Id", "OrganizationId", faculty.PrimaryOrganizationId);
//            ViewBag.SecondaryOrganizationId = new SelectList(db.Organizations, "Id", "OrganizationId", faculty.SecondaryOrganizationId);
//            ViewBag.CreatedBy = new SelectList(db.People, "PersonId", "FirstName", faculty.CreatedBy);
//            return View(faculty);
//        }

//        // POST: /Staff/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include="Id,PersonId,EmployeeId,FacultyRankId,PrimaryOrganizationId,SecondaryOrganizationId,OtherAffliliations,OtherTitles,CreatedBy")] Faculty faculty)
//        {
//            if (ModelState.IsValid)
//            {
//                db.Entry(faculty).State = EntityState.Modified;
//                db.SaveChanges();
//                return RedirectToAction("Index");
//            }
//            ViewBag.FacultyRankId = new SelectList(db.FacultyRanks, "Id", "Rank", faculty.FacultyRankId);
//            ViewBag.PersonId = new SelectList(db.People, "PersonId", "FirstName", faculty.PersonId);
//            ViewBag.PrimaryOrganizationId = new SelectList(db.Organizations, "Id", "OrganizationId", faculty.PrimaryOrganizationId);
//            ViewBag.SecondaryOrganizationId = new SelectList(db.Organizations, "Id", "OrganizationId", faculty.SecondaryOrganizationId);
//            ViewBag.CreatedBy = new SelectList(db.People, "PersonId", "FirstName", faculty.CreatedBy);
//            return View(faculty);
//        }

//        // GET: /Staff/Delete/5
//        public ActionResult Delete(Guid? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            Faculty faculty = db.Faculties.Find(id);
//            if (faculty == null)
//            {
//                return HttpNotFound();
//            }
//            return View(faculty);
//        }

//        // POST: /Staff/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(Guid id)
//        {
//            Faculty faculty = db.Faculties.Find(id);
//            db.Faculties.Remove(faculty);
//            db.SaveChanges();
//            return RedirectToAction("Index");
//        }

//        protected override void Dispose(bool disposing)
//        {
//            if (disposing)
//            {
//                db.Dispose();
//            }
//            base.Dispose(disposing);
//        }
//    }
//}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using TraineeTrackingSystem.Common;
using TraineeTrackingSystem.Repository;
using TraineeTrackingSystem.Data;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using TraineeTrackingSystem.Web.Common;
using TraineeTrackingSystem.Web.PostModels;
using TraineeTrackingSystem.Web.ViewModels;
using System.Security.Claims;
using System.Net;
using System.Xml;
using System.Security.Principal;

namespace TraineeTrackingSystem.Web.Controllers
{
    public class SupportController : Controller
    {
        private readonly SupportRepository supportRepository;
        private readonly CommonDataRepository commonRepository;
        private readonly TTSEntities _context;
        private readonly string _DATEFORMAT = "{0:yyyy-MM-dd}";     //"{0:MM/dd/yyyy}";
        //This globle variable will be used to convert date to specific format 
        private readonly string _LastUpdateDATEFORMAT = "{0:M/dd/yyy hh:mm:tt}";
        public SupportController()
        {
            _context = new TTSEntities();
            this.supportRepository = new SupportRepository(_context);
            this.commonRepository = new CommonDataRepository(_context);
        }

        protected override void Dispose(bool disposing)
        {
            supportRepository.Dispose();
            base.Dispose(disposing);
        }

        [HttpGet]
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        // GET: Support records
        public ActionResult Index()
        {

            var sptData = supportRepository.GetSearch_VwSupportGeneralData();
            return View(sptData);
        }

        // DO NOT DELETE - Called by the Landing page to display/sort relevant data in the grid
        public virtual JsonResult Support_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(supportRepository.GetSearch_VwSupportGeneralData().ToDataSourceResult(request));
        }

        // GET: Support/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Support/Create
        public ActionResult Create()
        {
            var fullSupportTypeList = commonRepository.GetSupportTypeList().ToList();
            var fullSupportOrgList = commonRepository.GetSupportOrganizationList().ToList();
            
            var supportViewModel = new SupportViewModel
            {
                //FacultyList = fullFacultyList.Where(f => f.IsActive == true && f.FullName != null).Select(f => new KeyValuePair<Guid?, string>(f.Id, f.FullName)),
                SupportTypes = fullSupportTypeList.Select(st => new KeyValuePair<int, string>(st.Id, st.FullName)),
                SupportOrganizations = fullSupportOrgList.Select(so => new KeyValuePair<int, string>(so.Id, so.FullName)),
                //SelectedSupportOrganization = string.Empty

            };
            return View(new EditSupportViewModel
            {
                SupportGeneralInfo = supportViewModel
            });
        }

        public ActionResult CreateSupport(SupportPostModel newSupportModel)
        {
            var CreatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
            Support newSupport = supportRepository.AddSupport(newSupportModel.Title, newSupportModel.SupportTypeId, newSupportModel.SupportNumber, newSupportModel.SupportOrganizationId, newSupportModel.DateStarted, newSupportModel.DateEnded, CreatedBy);

            return Json(newSupport.Id);
        }
        /// <summary>
        /// MVN - This private method is to provide the hiding fields for a more elegant way and much more easily to maintaining.  
        /// If there is logic changed, we can just update this method, as appose to multiple places.  
        /// </summary>
        /// <param name="fdStatusId"></param>
        /// <param name="fundingId"></param>
        //private void HideFields(Guid? personId, Guid menteeId)
        //{
        //    var currMentee = menteeRepository.GetMenteeById(menteeId);
        //    // hidding or displaying the lastUpdate Info for General Information
        //    ViewBag.LastUpdated = (currMentee.DateLastUpdated.HasValue == false) ? true : false;
            
        //}
        

        // POST: Support/Edit/5
        public ActionResult Edit(Guid id)
        {
            var currSupport = supportRepository.GetSupportById(id);
            //var currSupport = supportRepository.GetSearch_VwSupportGeneralDataById(id);
            var fullSupportTypeList = commonRepository.GetSupportTypeList().ToList();
            var fullSupportOrgList = commonRepository.GetSupportOrganizationList().ToList();
           
            if (currSupport == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
            }

            var supportGeneralInfo = new SupportViewModel
            {
                Id = id,
                Title = currSupport.Title,
                SupportTypeId = currSupport.SupportTypeId.HasValue ? currSupport.SupportTypeId.Value : 0,
                SupportTypes = fullSupportTypeList.Select(st => new KeyValuePair<int, string>(st.Id, st.FullName)),
                SupportNumber = currSupport.SupportNumber,
                SupportOrganizationId = currSupport.SupportOrganizationId.HasValue ? currSupport.SupportOrganizationId.Value : 0,
                SupportOrganizations = fullSupportOrgList.Select(so => new KeyValuePair<int, string>(so.Id, so.FullName)),
                DateStarted = currSupport.DateStarted,
                DateEnded = currSupport.DateEnded,
                StrDateLastUpdated = (currSupport.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, currSupport.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(currSupport.LastUpdatedBy),

            };
                        
            //HideFields(currMentee.PersonId, id);
            return View(new EditSupportViewModel
            {
                SupportGeneralInfo = supportGeneralInfo
            });
        }

        public ActionResult UpdateSupport(SupportPostModel updateSupportModel)
        {
            var support = _context.Set<Support>()
                .Single(spt => spt.Id == updateSupportModel.Id);

            support.Title = updateSupportModel.Title;
            support.SupportNumber = updateSupportModel.SupportNumber;
            support.DateStarted = updateSupportModel.DateStarted;
            support.DateEnded = updateSupportModel.DateEnded;

            if (updateSupportModel.SupportTypeId > 0)
            {
                support.SupportTypeId = updateSupportModel.SupportTypeId;
            }

            if (updateSupportModel.SupportOrganizationId > 0)
            {
                support.SupportOrganizationId = updateSupportModel.SupportOrganizationId;
            }

            // Track last updated date and user
            support.DateLastUpdated = DateTime.Now;
            support.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);

            _context.SaveChanges();

            return Json(true);
        }

        
        //public ActionResult DeleteSupportSource(int id)
        //{
        //    var supportSource = _context.Set<SupportSource>().Find(id);
        //    if (supportSource != null)
        //    {
        //        supportSource.IsDeleted = true;
        //        _context.SaveChanges();
        //    }
        //    return Json(id);
        //}

        /*  PURPOSE: Updates Sub-Form: Past Trainee Current Position; 
         *                   Db Table: WorkHistory  */
        //public ActionResult UpdateTraineePosition(WorkHistoryPostModel updateTraineePositionModel)
        //{
        //    //check if it's a new or existing Work History record
        //    bool isNew = updateTraineePositionModel.Id.Equals(null) || !_context.Set<WorkHistory>()
        //        .Any(o => o.Id == updateTraineePositionModel.Id);

        //    var traineePosition = isNew
        //        ? new WorkHistory()
        //        : _context.Set<WorkHistory>()
        //        .Single(ms => ms.Id == updateTraineePositionModel.Id);

        //    Institution institution = null;
        //    if (updateTraineePositionModel.Institution != null)
        //    {
        //        if (_context.Set<Institution>().Any(ms => ms.Name == updateTraineePositionModel.Institution))
        //        {
        //            institution = _context.Set<Institution>().Single(ms => ms.Name == updateTraineePositionModel.Institution);
        //            traineePosition.InstitutionId = institution.Id;
        //        }
        //    }

        //    traineePosition.PositionTitle = updateTraineePositionModel.PositionTitle;
        //    traineePosition.DateStartedPosition = updateTraineePositionModel.DateStarted;
        //    traineePosition.DateEndedPosition = updateTraineePositionModel.DateEnded;
        //    traineePosition.SupportSourceText = updateTraineePositionModel.SupportSourceText;

        //    // Track last updated date and user
        //    traineePosition.DateLastUpdated = DateTime.Now;
        //    traineePosition.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);

        //    traineePosition.PersonId = updateTraineePositionModel.PersonId;
        //    if (isNew)
        //    {
        //        _context.Set<WorkHistory>().Add(traineePosition);
        //    }
        //    _context.SaveChanges();


        //    // Update grid with updated/new data????
        //    var traineePositionVM = new WorkHistoryViewModel
        //    {
        //        Id = traineePosition.Id,
        //        InstitutionName = (traineePosition.Institution != null) ? traineePosition.Institution.Name : null,
        //        PositionTitle = traineePosition.PositionTitle,
        //        DateStarted = traineePosition.DateStartedPosition,
        //        DateStartedText = String.Format(_DATEFORMAT, traineePosition.DateStartedPosition),
        //        DateEnded = traineePosition.DateEndedPosition,
        //        DateEndedText = String.Format(_DATEFORMAT, traineePosition.DateEndedPosition),
        //        SupportSourceText = traineePosition.SupportSourceText,

        //        /// MVN -> 9/17/2015 added tracking info here>>>>
        //        StrDateLastUpdated = (traineePosition.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, traineePosition.DateLastUpdated) : string.Empty,
        //        StrLastUpdatedBy = commonRepository.GetLoginInfo(traineePosition.LastUpdatedBy)
        //    };

        //    return new JsonCamelCaseResult(traineePositionVM, JsonRequestBehavior.AllowGet);

        //}

        //public ActionResult DeleteTraineePosition(int id)
        //{
        //    var traineePosition = _context.Set<WorkHistory>().Find(id);
        //    if (traineePosition != null)
        //    {
        //        traineePosition.IsDeleted = true;
        //        _context.SaveChanges();
        //    }
        //    return Json(id);
        //}


        // GET: Support/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Support/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //[HttpPost]
        //public ActionResult PostMentee(string saveButton, string menteeMidName, string menteeFName, string menteeLName, int? studentId)
        //{
        //    Mentee mentee = new Mentee();

        //    if (ModelState.IsValid)
        //    {
        //        if (!String.IsNullOrEmpty(studentId.ToString()) ||
        //            !String.IsNullOrEmpty(menteeLName) ||
        //            !String.IsNullOrEmpty(menteeFName)
        //            )
        //        {
        //            var result = menteeRepository.getMenteeById(studentId);

        //            // THIS IS FOR FUTURE EXPANSION
        //            if (result != null)
        //            {
        //                // RECORD FOUND - flag active or not active
        //                //faculty.Id = Id.ToString();
        //                //result.Id = facultyRepository.getFacultyByID(Id);
        //                mentee.StudentId = studentId.ToString();
        //                mentee.Person.LastName = menteeLName;
        //                mentee.Person.MiddleName = menteeMidName;
        //                mentee.Person.FirstName = menteeFName;
        //                //result = menteeRepository.getMenteeByID(facultyRankId);

        //                menteeRepository.AddUpdateMentee(result);
        //                return RedirectToAction("Index");

        //            }
        //            // NO RECORD FOUND
        //            mentee.StudentId = studentId.ToString();
        //            mentee.Person.LastName = menteeLName;
        //            mentee.Person.MiddleName = menteeMidName;
        //            mentee.Person.FirstName = menteeFName;
        //            //menteeRepository.getFacultyByID(facultyRankId);
        //            menteeRepository.AddUpdateMentee(mentee);
        //            return RedirectToAction("Index");
        //        }
        //    }
        //    return View("Index");
        //}
        ///ToDO Add Hiding method here
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

//using TraineeTrackingSystem.Common;
//using TraineeTrackingSystem.Repository;
//using TraineeTrackingSystem.Data;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;

using TraineeTrackingSystem.Repository;
using TraineeTrackingSystem.Common;
using TraineeTrackingSystem.Data;
namespace TraineeTrackingSystem.Web.Controllers
{
    public class BaseController : Controller
    {
        protected readonly string baseRedirectUrl = ConfigurationManager.AppSettings["TraineeTrackingSystemRedirect"];
        protected readonly string enviromentSetting = ConfigurationManager.AppSettings["Enviroment"];
        /// <summary>
        /// MVN - added 4/13/2016 for the userRole accessiability 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        //private readonly UserRolesRepository usrRoleRepository;
        protected readonly UserRolesRepository usrRoleRepository;
        public BaseController() : this(new T32UserRolesRepository()) { }
        public BaseController(UserRolesRepository repository)
        {
            usrRoleRepository = repository;

        }
        protected override void Dispose(bool disposing)
        {
            usrRoleRepository.Dispose();
            base.Dispose(disposing);
        }

        public string AttachTokenToUrl(string url)
        {
            var token = Security.CreateToken(User.Identity.GetUserName(), System.Web.HttpContext.Current.Request.UserHostAddress);

            return url + "?token=" + Server.UrlEncode(token);

        }
    }
}
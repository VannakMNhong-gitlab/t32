﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using TraineeTrackingSystem.Common;
using TraineeTrackingSystem.Repository;
using TraineeTrackingSystem.Data;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using TraineeTrackingSystem.Web.Common;
using TraineeTrackingSystem.Web.PostModels;
using TraineeTrackingSystem.Web.ViewModels;
// added tracking date and user update
using System.Security.Claims;
using TraineeTrackingSystem.Web.Models;

namespace TraineeTrackingSystem.Web.Controllers
{
    [AuthorizeUser(AccessLevel = "Editor")]
    //[AuthorizeUser(MultipleRoles = "Administor, SuperUser,Editor")]
    public class FacultyController : Controller
    {
        private readonly FacultyRepository facultyRepository;
        private readonly PersonRepository personRepository;
        private readonly CommonDataRepository commonRepository;
        private const string firstName = "";
        private const string lastName = "";
        private const string middleName = "";
        private readonly string _DATEFORMAT = "{0:yyy-MM-dd}";
        private readonly string _LastUpdateDATEFORMAT = "{0:M/dd/yyy hh:mm:tt}";

        private readonly TTSEntities _context;

        public FacultyController()
        {
            _context = new TTSEntities();
            facultyRepository = new FacultyRepository(_context);
            personRepository = new PersonRepository(_context);
            this.commonRepository = new CommonDataRepository(_context);
        }

        protected override void Dispose(bool disposing)
        {
            facultyRepository.Dispose();
            base.Dispose(disposing);
            commonRepository.Dispose();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpGet]
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        //[AuthorizeUser(AccessLevel = "SuperUser, Administrator, Editor")]
        public ActionResult Index()
        {

            var facultyDemographics = facultyRepository.GetSearch_VwFacultyDemographics();
            return View(facultyDemographics);
        }

        public ActionResult UpdateDegree(DegreePostModel updateDegreeModel)
        {
            bool isNew = !updateDegreeModel.Id.HasValue || !_context.Set<FacultyAcademicData>()
                .Any(o => o.Id == updateDegreeModel.Id);
            var degreeInfo = isNew
                ? new FacultyAcademicData() : _context.Set<FacultyAcademicData>().Find(updateDegreeModel.Id);
            if (_context.Set<AcademicDegree>().Any(ac => ac.Name == updateDegreeModel.AcademicDegree))
            {
                AcademicDegree academicDegree=_context.Set<AcademicDegree>()
                    .Single(ac => ac.Name == updateDegreeModel.AcademicDegree);
                degreeInfo.AcademicDegreeId = academicDegree.Id;
            }

            if (updateDegreeModel.DegreeYear > 0)
            {
                degreeInfo.DegreeYear = updateDegreeModel.DegreeYear;
            }
            degreeInfo.DateLastUpdated = DateTime.Now;
            degreeInfo.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);

            Institution institution=null;

            if (_context.Set<Institution>().Any(ac => ac.Name == updateDegreeModel.Institution))
            {
                institution = _context.Set<Institution>().Single(ac => ac.Name == updateDegreeModel.Institution);
                degreeInfo.InstitutionId = institution.Id;
            }
                
            
            degreeInfo.PersonId = updateDegreeModel.PersonId;
            if (isNew)
            {
                _context.Set<FacultyAcademicData>().Add(degreeInfo);
            }
            _context.SaveChanges();

            
            var degreeViewModel = new AcademicDegreeViewModel
            {
                Id=degreeInfo.Id,
                //AcademicDegreeName=updateDegreeModel.AcademicDegree,
                AcademicDegreeName = updateDegreeModel.AcademicDegree != null ? updateDegreeModel.AcademicDegree : string.Empty,
                InstitutionId = institution != null ? institution.Id : 0,
                InstitutionName = institution != null ? institution.Name : string.Empty,
                DegreeYear = updateDegreeModel.DegreeYear,
                CityName = institution.City,
                StateName = institution.StateProvince != null ? institution.StateProvince.FullName : string.Empty,
                CountryName = institution.Country != null ? institution.Country.Name : string.Empty,
                StrDateLastUpdated = (degreeInfo.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, degreeInfo.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(degreeInfo.LastUpdatedBy),
            };

            return new JsonCamelCaseResult(degreeViewModel,JsonRequestBehavior.AllowGet);
        }


        public ActionResult DeleteDegree(int id)
        {
            var facultyDegree = _context.Set<FacultyAcademicData>().Find(id);
            if (facultyDegree != null)
            {
                _context.Set<FacultyAcademicData>().Remove(facultyDegree);
                _context.SaveChanges();
            }
            return Json(id);
        }

        public ActionResult UpdateResearchInterest(ResearchInterestPostModel updateDegreeModel)
        {
            //check if it's a new or existing reseach interest
            bool isNew = !updateDegreeModel.Id.HasValue || !_context.Set<FacultyResearchInterest>().Any(fr => fr.Id == updateDegreeModel.Id);
            var facultyResearchInterest = isNew
                ? new FacultyResearchInterest()
                : _context.Set<FacultyResearchInterest>().Find(updateDegreeModel.Id);
            //facultyResearchInterest.DateEntered = updateDegreeModel.DateEntered;
            facultyResearchInterest.ResearchInterest = updateDegreeModel.ResearchInterest;
            facultyResearchInterest.PersonId = updateDegreeModel.PersonId;
            if (isNew)
            {
                _context.Set<FacultyResearchInterest>().Add(facultyResearchInterest);
            }
            _context.SaveChanges();
            return Json(facultyResearchInterest.Id);
        }

        public ActionResult DeleteResearchInterest(int id)
        {
            var facultyResearchInterest = _context.Set<FacultyResearchInterest>().Find(id);
            if (facultyResearchInterest != null)
            {
                _context.Set<FacultyResearchInterest>().Remove(facultyResearchInterest);
                _context.SaveChanges();
            }
            return Json(id);
        }

        public ActionResult CreateFaculty(PersonPostModel newPersonModel)
        {
            
            //Create Person, get person Id, and then create faculty
            var newPerson=new Person
            {
                PersonId=Guid.NewGuid(),
                FirstName = newPersonModel.FirstName,
                LastName = newPersonModel.LastName,
                MiddleName = newPersonModel.MiddleName,
                //GenderId = newPersonModel.GenderPersonId.HasValue ? newPersonModel.GenderPersonId.Value: 0,
                //GenderAtBirthId = newPersonModel.GenderOfBirthPersonId.HasValue ? newPersonModel.GenderOfBirthPersonId.Value: 0,
                //EthnicityId = newPersonModel.EthnicityPersonId.HasValue ? newPersonModel.EthnicityPersonId.Value: 0,

                DateLastUpdated = DateTime.Now,
                LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value)
            };
            //if (newPersonModel.EmployeeId ==null)
            //{
            //    return View();
            //}
            Faculty newFaculty = facultyRepository.AddFaculty(newPerson, newPersonModel.EmployeeId);
            return Json(newFaculty.Id);
        }

        public ActionResult UpdateFaculty(PersonPostModel updatePersonModel)
        {
            var faculty = _context.Set<Faculty>()
                .Include(f => f.Person)
                .Single(f => f.Id == updatePersonModel.Id);
           
            faculty.Person.FirstName = updatePersonModel.FirstName;
            faculty.Person.LastName = updatePersonModel.LastName;
            faculty.Person.MiddleName = updatePersonModel.MiddleName;
            faculty.EmployeeId = updatePersonModel.EmployeeId;

            faculty.PositionDepartmentId = updatePersonModel.PositionDepartmentId.HasValue ? updatePersonModel.PositionOrganizationId.Value: 0;
            faculty.PositionDepartmentId = updatePersonModel.PositionOrganizationId.HasValue ? updatePersonModel.PositionOrganizationId.Value: 0;
            faculty.MentorId = updatePersonModel.MentorId.HasValue ? updatePersonModel.MentorId.Value: Guid.Empty;
            Person personRecTobeUpdated = null;
            if (_context.Set<Person>().Any(p => p.PersonId == updatePersonModel.PersonId))
            {
                personRecTobeUpdated = _context.Set<Person>().Single(p => p.PersonId == updatePersonModel.PersonId);
                if (updatePersonModel.GenderId != null && updatePersonModel.GenderId != 0){
                    personRecTobeUpdated.GenderId = updatePersonModel.GenderId;
                }
                if (updatePersonModel.GenderAtBirthId != null && updatePersonModel.GenderAtBirthId != 0){
                    personRecTobeUpdated.GenderAtBirthId = updatePersonModel.GenderAtBirthId;
                }
                if (updatePersonModel.EthnicityId != null && updatePersonModel.EthnicityId != 0){
                    personRecTobeUpdated.EthnicityId = updatePersonModel.EthnicityId;
                }
            }

            /// MVN - these codes are no longer needed
            ///faculty.OtherAffiliations = updatePersonModel.OtherAffiliations;
            ///faculty.OtherTitles = updatePersonModel.OtherTitles;

            //////var savedPrograms = faculty.Programs.ToList();

            //////// ADD newly selected Programs to database
            //////if (updatePersonModel.FacultyPrograms != null)
            //////{
            //////    foreach (var selectedProgramId in updatePersonModel.FacultyPrograms)
            //////    {
            //////        if (faculty.Programs.All(p => p.Id != selectedProgramId))
            //////        {
            //////            var addProgram = _context.Set<Program>().Find(selectedProgramId);
            //////            faculty.Programs.Add(addProgram);
            //////        }
            //////    }
            //////}

            //////// DELETE previously-saved, just removed Programs from database
            //////foreach (var savedProgram in savedPrograms)
            //////{
            //////    if (updatePersonModel.FacultyPrograms != null)
            //////    {
            //////        if (!updatePersonModel.FacultyPrograms.Contains(savedProgram.Id))
            //////        {
            //////            var remProgram = _context.Set<Program>().Find(savedProgram.Id);
            //////            faculty.Programs.Remove(remProgram);
            //////        }
            //////    }
            //////    else
            //////    {
            //////        var remProgram = _context.Set<Program>().Find(savedProgram.Id);
            //////        faculty.Programs.Remove(remProgram);
            //////    }
            //////}

            //////// Only update the following items in the database if they are NOT 0 
            //////// (which doesn't exist as an option in the db)
            /////////MVN - 2-19-2016 commented this out based on the new faculty page mockup 
            ////////if (updatePersonModel.RankId > 0)
            ////////{
            ////////    faculty.FacultyRankId = updatePersonModel.RankId;
            ////////}

            //faculty.Person.GenderId = updatePersonModel.GenderPersonId;
            //faculty.Person.PersonId = 
            //////if (updatePersonModel.OrganizationId > 0)
            //////{
            //////    faculty.PrimaryOrganizationId = updatePersonModel.OrganizationId;
            //////}

            //////if (updatePersonModel.SecondOrgId > 0)
            //////{
            //////    faculty.SecondaryOrganizationId = updatePersonModel.SecondOrgId;
            //////}

            ////////if (updatePersonModel.TIUId > 0)
            ////////{
            ////////    faculty.TiuId = updatePersonModel.TIUId;
            ////////}
            



            // Tracking date and user update
            faculty.DateLastUpdated = DateTime.Now;
            faculty.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
            _context.SaveChanges();
            return Json(true);
        }

        public ActionResult Edit(Guid id)
        {
            var faculty = facultyRepository.getFacultyById(id);
            /// MVN - 2-19-2016 commented this out base on new requirements.  Rank is no longer needed
            /// var rankTypes = facultyRepository.GetFacultyRank().ToList();
            var org = facultyRepository.GetOrganization().ToList();
            //var ftyObject = facultyRepository.GetAllFtyAttr().ToList();

            var person = facultyRepository.GetPerson().ToList();    
            var gender = facultyRepository.GetGender().ToList();
            var ethnicity = facultyRepository.GetEthnicity().ToList();
            //var instition = _context.Institutions.ToList();
            var facultytrackObj = facultyRepository.GetFtyTrks().ToList();
            //var personFrmPersonTbl = facultyRepository.GetOtherPersonAttr().ToList();
            //var ftyAptmnts = facultyRepository.GetFtyAptmnts().ToList();
            var ftyTrack = facultyRepository.GetFtyTrack().ToList();
            var ftyPathway = facultyRepository.GetClinicalFtyPathway().ToList();
            var ftyRnk = facultyRepository.GetFtyRnk().ToList();
            // MVN - 11-05-2014 to fix the department sorting issue
            //var organizationType = facultyRepository.GetOrganization().ToList();
            var organizationType = commonRepository.GetOrganizationList().ToList();       
            var states = _context.StateProvinces.ToList();
            var countries = _context.Countries.ToList();
            var programList = commonRepository.GetProgramList().ToList();
            var facultyGeneralViewModel = new FacultyGeneralViewMoel
            {
                Id = id,
                FirstName = faculty.Person.FirstName,
                LastName = faculty.Person.LastName,
                MiddleName = faculty.Person.MiddleName,
                EmployeeId = faculty.EmployeeId,

                PersonId = faculty.PersonId,
                
                /// see top comment 2-19-2016
                /// RankId = faculty.FacultyRankId.HasValue ? faculty.FacultyRankId.Value : 0,
                /// Ranks = rankTypes.Select(r => new KeyValuePair<int, string>(r.Id, r.Rank)),
                /// Programs = programList.Select(p => new KeyValuePair<Guid, string>(p.Id, p.Title)),
                /// SelectedPrograms = commonRepository.GetFacultyProgramList(id).Select(p => p.ProgramId),
                /// OrganizationId = faculty.PrimaryOrganizationId.HasValue ? faculty.PrimaryOrganizationId.Value : 0,
                /// Organization = organizationType.Select(o => new KeyValuePair<int, string>(o.Id, o.DisplayName)),
                PositionDepartmentId = faculty.PositionDepartmentId.HasValue ? faculty.PositionDepartmentId.Value : 0,
                PositionDepartments = org.Select(o => new KeyValuePair<int, string>(o.Id, o.DisplayName)),
                
                PositionOrganizationId = faculty.PositionDepartmentId.HasValue ? faculty.PositionDepartmentId.Value : 0,
                PositionOrganizations = org.Select(o => new KeyValuePair<int, string>(o.Id, o.DisplayName)),
                MentorId = faculty.MentorId.HasValue ? faculty.MentorId.Value: Guid.Empty,//.HasValue ? faculty.MentorId.Value: null,
                PositionMentors = person.Where(o => o.FullName != null).Select(o => new KeyValuePair<Guid, string>(o.PersonId, o.FullName)),

                GenderId = faculty.Person.GenderId.HasValue ? faculty.Person.GenderId.Value : 0,
                PositionGenders = gender.Where(g => g.Name != null).Select(o => new KeyValuePair<int?, string>(o.Id, o.Name)),
                GenderAtBirthId = faculty.Person.GenderAtBirthId.HasValue ? faculty.Person.GenderAtBirthId.Value : 0,
                PositionGenderAtBirthes = gender.Where(g => g.Name != null).Select(o => new KeyValuePair<int?, string>(o.Id, o.Name)),
                EthnicityId = faculty.Person.EthnicityId.HasValue ? faculty.Person.EthnicityId.Value : 0,
                PositionEthnicities = ethnicity.Select(e => new KeyValuePair<int?, string>(e.Id, e.Name)),
          
                //SecondOrgId = faculty.SecondaryOrganizationId.HasValue ? faculty.SecondaryOrganizationId.Value : 0,
                //SecondOrg = organizationType.Select(o => new KeyValuePair<int, string>(o.Id, o.DisplayName)),

                //TIUId = faculty.TiuId.HasValue ? faculty.TiuId.Value : 0,
                //TIU = organizationType.Select(o => new KeyValuePair<int, string>(o.Id, o.DisplayName)),

                //OtherAffiliations = faculty.OtherAffiliations,
                //OtherTitles = faculty.OtherTitles,
                //DateLastUpdated = faculty.DateLastUpdated,
                //StrDateLastUpdated = String.Format(_DATEFORMAT, faculty.DateLastUpdated),
                //StrDateLastUpdated = String.Format(_LastUpdateDATEFORMAT, faculty.DateLastUpdated),
                // LastUpdatedBy = faculty.LastUpdatedBy

                // 10/8/15: MPC -- Fixed formatting
                StrDateLastUpdated = (faculty.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, faculty.DateLastUpdated) : string.Empty,
                LastUpdatedBy = commonRepository.GetLoginInfo(faculty.LastUpdatedBy),
               
            };
            var appointmentViewModel = new AppointmentsInfoViewModel{
                PersonId = faculty.PersonId,
                FtyTrksDrpdwnDatasource = ftyTrack.Where(f => f.IsDeleted == false).Select(f => new KeyValuePair<int, string>(f.Id, f.Name)),
                ClnlFtyPthwyDrpdwnDatasource = ftyPathway.Select(c => new KeyValuePair<int, string>(c.Id, c.Name)),
                RankDrpdwnDatasource = ftyRnk.Select(r => new KeyValuePair<int, string>(r.Id, r.Rank)),
                AppointmentInformations = commonRepository.getFtyApptmntFromViewTable().Select(appointmentInfo =>new AppointmentInfoViewModel{
                    Id = appointmentInfo.FacultyAppointmentId,            // Appointment Id
                    PersonId = appointmentInfo.FacultyPersonId,
                    InstitutionId = appointmentInfo.InstitutionId,
                    InstName = (appointmentInfo.InstitutionName != null) ? appointmentInfo.InstitutionName : string.Empty,

                    FacultyTrackId = appointmentInfo.FacultyTrackId.HasValue ? appointmentInfo.FacultyTrackId : null,
                    FacultyTrack = appointmentInfo.FacultyTrack != null
                                    ? appointmentInfo.FacultyTrack
                                    : string.Empty,
                    ClinicalFacultyPathwayId = appointmentInfo.ClinicalFacultyPathwayId.HasValue ? appointmentInfo.ClinicalFacultyPathwayId : null,
                    ClinicalFacultyPathway = appointmentInfo.ClinicalFacultyPathway != null
                                            ? appointmentInfo.ClinicalFacultyPathway
                                            : string.Empty,
                    FacultyRankId = appointmentInfo.FacultyRankId.HasValue ? appointmentInfo.FacultyRankId : null,
                    FacultyRank = appointmentInfo.FacultyRank != null
                                    ? appointmentInfo.FacultyRank
                                    : string.Empty,
                    OtherAffiliations = (appointmentInfo.OtherAffiliations != null) ? appointmentInfo.OtherAffiliations : string.Empty,
                    OtherTitles = (appointmentInfo.OtherTitles != null) ? appointmentInfo.OtherTitles : string.Empty,
                    PercentFte = appointmentInfo.PercentFte,
                    DateStarted = appointmentInfo.DateStarted,     // for the grid row selection                                           
                    DateStartedText = String.Format(_DATEFORMAT, appointmentInfo.DateStarted),
                    DateEnded = appointmentInfo.DateEnded,         // for the grid row selection
                    DateEndedText = String.Format(_DATEFORMAT, appointmentInfo.DateEnded),
                    Comment = (appointmentInfo.Comment != null) ? appointmentInfo.Comment: string.Empty,
                    StrDateLastUpdated = (appointmentInfo.AppointmentDateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, appointmentInfo.AppointmentDateLastUpdated) : string.Empty,
                    StrLastUpdatedBy = commonRepository.GetLoginInfo(appointmentInfo.AppointmentLastUpdatedByName)
                })
            };
            
            var degreeInformationViewModel = new AcademicDegreesViewModel
            {
                PersonId = faculty.PersonId,
                States=states.Select(s=> new KeyValuePair<int, string>(s.Id, s.Abbreviation)),
                Countries = countries.Select(c => new KeyValuePair<int, string>(c.Id, c.Name)),
                DegreeInformations =
                    faculty.Person.FacultyAcademicDatas.Select(facultyAcademicData => new AcademicDegreeViewModel
                    {
                        Id = facultyAcademicData.Id,
                        DegreeYear = facultyAcademicData.DegreeYear,
                        InstitutionId = facultyAcademicData.InstitutionId,
                        InstitutionName =
                            facultyAcademicData.Institution != null
                                ? facultyAcademicData.Institution.Name
                                : string.Empty,
                        AcademicDegreeId = facultyAcademicData.AcademicDegreeId,
                        AcademicDegreeName =facultyAcademicData.AcademicDegree!=null? facultyAcademicData.AcademicDegree.Name:string.Empty,
                        CityName =
                            facultyAcademicData.Institution != null
                                ? facultyAcademicData.Institution.City
                                : string.Empty,
                        StateName =
                            //facultyAcademicData.Institution != null && facultyAcademicData.Institution.StateProvince != null
                            facultyAcademicData.Institution != null
                                ? (facultyAcademicData.Institution.StateProvince != null 
                                    ? (facultyAcademicData.Institution.StateProvince.Abbreviation.Length > 0 
                                            ? facultyAcademicData.Institution.StateProvince.Abbreviation 
                                            : facultyAcademicData.Institution.StateProvince.FullName)
                                    : string.Empty)
                                : string.Empty,
                        CountryName =
                            facultyAcademicData.Institution != null
                                ? (facultyAcademicData.Institution.Country != null
                                    ? (facultyAcademicData.Institution.Country.Name)
                                    : string.Empty)
                                : string.Empty,
                        StrDateLastUpdated = (facultyAcademicData.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, facultyAcademicData.DateLastUpdated) : string.Empty,
                        StrLastUpdatedBy = commonRepository.GetLoginInfo(facultyAcademicData.LastUpdatedBy)
                    })

            };
            var facultyReseachInterestViewModel = new FacultyResearchInterestsViewModel
            {
                PersonId = faculty.PersonId,
                FacultyResearchInterests =
                    faculty.Person.FacultyResearchInterests.Select(fri => new FacultyResearchInterestViewModel
                    {
                        Id = fri.Id,
                        ResearchInterest = fri.ResearchInterest
                    })
            };
            var menteeView = facultyRepository.GetSearch_VwMenteeMentors(faculty.PersonId);

            var menteeMentorViewModel = new MenteeMentorsViewModel
            {
                MentorPersonId = faculty.PersonId,  // this the mentor
                MenteesGridView = menteeView.Where(p => p.MentorPersonId == faculty.PersonId)
                    .Select(mentee => new MenteeMentorViewModel
                    {
                        MenteeId = mentee.MenteeId,
                        MenteePersonId = mentee.MenteePersonId,
                        MenteeFullName = mentee.MenteeFullName,
                        MenteeType = mentee.MenteeType,
                        YearStarted = mentee.YearStarted,
                        YearEnded = mentee.YearEnded,
                        CompletedDegree = mentee.CompletedDegree
                    })
            };
            var fundingView = facultyRepository.GetSearch_VwTrainingGrantFaculty(faculty.PersonId);
           
            var institutionTrainingGrantViewModel = new InstitutionTrainingGrants
            {
                FacultyPersonId = faculty.PersonId,
                FundingGridView = fundingView.Select(funding => new InstitutionTrainingGrant
                {
                    TrainingGrantId = funding.TrainingGrantId,                    
                    FacultyPrimaryRole = funding.FacultyPrimaryRole,
                    Title = funding.Title,
                    SponsorName = funding.SponsorName,
                    SponsorAwardNumber = funding.SponsorAwardNumber,
                    StrDateTimeFundingStarted = String.Format(_DATEFORMAT, funding.DateTimeFundingStarted),
                    DateTimeFundingStarted = funding.DateTimeFundingStarted,
                    StrDateTimeFundingEnded = String.Format(_DATEFORMAT, funding.DateTimeFundingEnded),
                    DateTimeFundingEnded = funding.DateTimeFundingEnded
                    //DateTimeFundingStarted = funding.DateTimeFundingStarted,
                    //DateTimeFundingEnded = funding.DateTimeFundingEnded,
                })
            };
            var otherFundingView = facultyRepository.GetSearch_VwFundingFaculty(faculty.PersonId);

            var otherFundingViewModel = new OtherFundingsInfo
            {
                FacultyPersonId = faculty.PersonId,
                OtherGridView = otherFundingView.Where(p => p.FacultyPersonId == faculty.PersonId)
                                                .Select(other => new OtherFundingInfo

                {
                    Id = other.Id,  // FundingFacultyId      
                    FacultyRole = other.FacultyRole,
                    Title = other.Title,
                    SponsorName = other.SponsorName,
                    SponsorAwardNumber = other.SponsorAwardNumber,
                    StrDateTimeFundingStarted = String.Format(_DATEFORMAT, other.DateTimeFundingStarted),
                    DateTimeFundingStarted = other.DateTimeFundingStarted,
                    StrDateTimeFundingEnded = String.Format(_DATEFORMAT, other.DateTimeFundingEnded),
                    DateTimeFundingEnded = other.DateTimeFundingEnded
                    //DateTimeFundingStarted = other.DateTimeFundingStarted,
                    //DateTimeFundingEnded = other.DateTimeFundingEnded                  
                })
            };
            var academicViewModel = new AcademicsInfoViewModel{
                FacultyId = faculty.Id,
                PersonId = faculty.PersonId,
                AcademicInformations = commonRepository.GetFtyAcademicHistoryObject().Where(a => a.PersonId == faculty.PersonId)
                                                       .Select(academicHistory => new AcademicInfoViewModel{
                    Id = academicHistory.Id,          
                    PersonId = academicHistory.PersonId,
                    InstitutionId = academicHistory.InstitutionId,      //Institution is required FK and will not except NULL value
                    InstName = (academicHistory.Institution_InstitutionId.Name != null) 
                                    ? academicHistory.Institution_InstitutionId.Name 
                                    : commonRepository.GetDefaultInstitution().Name,
                  
                    DateStarted = academicHistory.DateStarted,                                            
                    DateStartedText = String.Format(_DATEFORMAT, academicHistory.DateStarted),
                    DateEnded = academicHistory.DateEnded,       
                    DateEndedText = String.Format(_DATEFORMAT, academicHistory.DateEnded),
                    /// Complete Degree
                    AcademicDegreeId = academicHistory.AcademicDegreeId,//.HasValue ? academicHistory.AcademicDegreeId: null,
                    AcademicDegree = (academicHistory.AcademicDegree != null) ? academicHistory.AcademicDegree.Name: string.Empty,
                    /// Date Degree Complete
                    DateDegreeCompleted = academicHistory.DateDegreeCompleted.HasValue ? academicHistory.DateDegreeCompleted: null,
                    AreaOfStudy = academicHistory.AreaOfStudy,
                    /// Research Experience
                    ResearchProjectTitle = academicHistory.ResearchProjectTitle,
                    Comments = (academicHistory.Comments != null) ? academicHistory.Comments: string.Empty,
                    StrDateLastUpdated = (academicHistory.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, academicHistory.DateLastUpdated) : string.Empty,
                    StrLastUpdatedBy = commonRepository.GetLoginInfo(academicHistory.LastUpdatedBy)
                })
            };

            return View(new EditFacultyViewModel
            {
                PersonGeneralViewMoel = facultyGeneralViewModel,
                ApointmentInformations = appointmentViewModel,
                AcademicInformations = academicViewModel,
                DegreeInformations = degreeInformationViewModel,
                FacultyResearchInterests = facultyReseachInterestViewModel,
                FacultyGeneralViewModel = faculty.Person.Faculties,
                MenteesGridView = menteeMentorViewModel,
                FundingGridView = institutionTrainingGrantViewModel,
                OtherGridView = otherFundingViewModel
            });

        }

        [HttpPost]
        public ActionResult UpdateAppointment(AppointmentInfoPostModel model)
        {
            bool isNew = !model.Id.HasValue || !_context.Set<FacultyAppointment>()
                .Any(o => o.Id == model.Id);
            var newApptmnt = isNew
                ? new FacultyAppointment() : _context.Set<FacultyAppointment>().Find(model.Id);

            newApptmnt.PersonId = model.PersonId;
            /// MVN - Dev notes: the nullable int can be sepcified with the syntax "int?"
            /// the HasValue and Value properties, use the nullable type to acquire the value to the instance...
            /// essentially, the int values are optional and will exception null, therefore, take value if there is any
            /// otherwide, assign null back 
            newApptmnt.FacultyTrackId = model.FacultyTrackId.HasValue ? model.FacultyTrackId: null;
            newApptmnt.ClinicalFacultyPathwayId = model.ClinicalFacultyPathwayId.HasValue ? model.ClinicalFacultyPathwayId: null;
            newApptmnt.FacultyRankId = model.FacultyRankId.HasValue ? model.FacultyRankId: null;
            newApptmnt.OtherAffiliations = (model.OtherAffiliations != null) ? model.OtherAffiliations : string.Empty;
            newApptmnt.OtherTitles = (model.OtherTitles != null) ? model.OtherTitles : string.Empty;
            newApptmnt.PercentFte = model.PercentFte;
            newApptmnt.Comment = model.Comment;
            newApptmnt.DateStarted = model.DateStarted;
            newApptmnt.DateEnded = model.DateEnded;
            ///Track user inputs ...
            DateTime setUpdateDate = DateTime.Now;
            newApptmnt.DateLastUpdated = DateTime.Now;
            newApptmnt.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);


            Institution currentInst = null;
            /// ------------ these handled the update UPDATE ------------ /
            /// customer may decided to update the instition list by adding a new instition to the list 
            if (model.InstName != null){
                if (_context.Set<Institution>().Any(insttName => insttName.Name == model.InstName)){
                    currentInst = _context.Set<Institution>().Single(inst => inst.Name == model.InstName);
                    newApptmnt.InstitutionId = currentInst.Id;
                }
            }

            

            /// ------------ these handled NEW record ------------ /
            if (isNew){
                /// Handle the autocomplete UI
                if (model.InstName != null){
                    if (_context.Set<Institution>().Any(insttName => insttName.Name == model.InstName)){
                        currentInst = _context.Set<Institution>().Single(inst => inst.Name == model.InstName);
                        newApptmnt.InstitutionId = currentInst.Id;
                    }
                }
                
                
                _context.Set<FacultyAppointment>().Add(newApptmnt);
            }
            _context.SaveChanges();

            var ftyTrack = facultyRepository.GetFtyTrack().ToList();
            var ftyPathway = facultyRepository.GetClinicalFtyPathway().ToList();
            var ftyRnk = facultyRepository.GetFtyRnk().ToList();
            var appointmentViewModel = new AppointmentInfoViewModel{
                Id = newApptmnt.Id,            // Appointment Id
                PersonId = model.PersonId,
                InstName = (newApptmnt.Institution.Name != null) ? newApptmnt.Institution.Name : string.Empty,

                FacultyTrackId = newApptmnt.FacultyTrackId,
                FacultyTrack = model.FacultyTrack != null
                                ? model.FacultyTrack
                                : string.Empty,
                FtyTrksDrpdwnDatasource = ftyTrack.Where(f => f.IsDeleted == false).Select(f => new KeyValuePair<int, string>(f.Id, f.Name)),

                ClinicalFacultyPathwayId = newApptmnt.ClinicalFacultyPathwayId,
                ClinicalFacultyPathway = model.ClinicalFacultyPathway != null
                                        ? model.ClinicalFacultyPathway
                                        : string.Empty,
                ClnlFtyPthwyDrpdwnDatasource = ftyPathway.Select(c => new KeyValuePair<int, string>(c.Id, c.Name)),

                FacultyRankId = newApptmnt.FacultyRankId,
                FacultyRank = model.FacultyRank != null
                                ? model.FacultyRank
                                : string.Empty,
                RankDrpdwnDatasource = ftyRnk.Select(r => new KeyValuePair<int, string>(r.Id, r.Rank)),

                OtherAffiliations = (newApptmnt.OtherAffiliations != null) ? newApptmnt.OtherAffiliations : string.Empty,
                OtherTitles = (newApptmnt.OtherTitles != null) ? newApptmnt.OtherTitles : string.Empty,
                PercentFte = newApptmnt.PercentFte,
                DateStarted = newApptmnt.DateStarted,     // for the grid row selection                                           
                DateStartedText = String.Format(_DATEFORMAT, newApptmnt.DateStarted),
                DateEnded = newApptmnt.DateEnded,         // for the grid row selection
                DateEndedText = String.Format(_DATEFORMAT, newApptmnt.DateEnded),
                Comment = (newApptmnt.Comment != null) ? newApptmnt.Comment : string.Empty,
                StrDateLastUpdated = (newApptmnt.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, newApptmnt.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(newApptmnt.LastUpdatedBy)               
            };
            return new JsonCamelCaseResult(appointmentViewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateAcademicHistoryInfo(AcademicInfoPostModel model)
        {
            bool isNew = model.Id.Equals(null) || !_context.Set<AcademicHistory>()
                .Any(o => o.Id == model.Id);

            var academicHistoryInfo = isNew
                ? new AcademicHistory()
                : _context.Set<AcademicHistory>()
                .Single(tp => tp.Id == model.Id);

            Institution currentInst = null;
            /// ------------ these handled the update UPDATE ------------ /
            /// customer may decided to update the instition list by adding a new instition to the list 
            if (model.InstName != null){
                if (_context.Set<Institution>().Any(insttName => insttName.Name == model.InstName)){
                    currentInst = _context.Set<Institution>().Single(inst => inst.Name == model.InstName);
                    academicHistoryInfo.InstitutionId = currentInst.Id;
                }
            }
            AcademicDegree currentAcdmcDegree = null;
            /// ------------ these handled the update the autocomplete property ------------ */
            /// customer may decided to update the academic degree list by changing the academic degree name 
            if (model.AcademicDegree != null){
                if (_context.Set<AcademicDegree>().Any(acdmcDegree => acdmcDegree.Name == model.AcademicDegree)){
                    currentAcdmcDegree = _context.Set<AcademicDegree>().Single(dgr => dgr.Name == model.AcademicDegree);
                    academicHistoryInfo.AcademicDegreeId = currentAcdmcDegree.Id;
                }
            }
            /// ------------ these handled NEW Record ------------ /
            if (isNew){
                /// Handle the autocomplete UI
                if (model.InstName != null){
                    if (_context.Set<Institution>().Any(insttName => insttName.Name == model.InstName)){
                        currentInst = _context.Set<Institution>().Single(inst => inst.Name == model.InstName);
                        academicHistoryInfo.InstitutionId = currentInst.Id;
                    }
                }
                if (model.AcademicDegree != null){
                    if (_context.Set<AcademicDegree>().Any(acdmcDegree => acdmcDegree.Name == model.AcademicDegree)){
                        currentAcdmcDegree = _context.Set<AcademicDegree>().Single(dgr => dgr.Name == model.AcademicDegree);
                        academicHistoryInfo.AcademicDegreeId = currentAcdmcDegree.Id;
                    }
                }
                _context.Set<AcademicHistory>().Add(academicHistoryInfo);
            }
            academicHistoryInfo.PersonId = model.PersonId;
            academicHistoryInfo.DateStarted = model.DateStarted;
            academicHistoryInfo.DateEnded = model.DateEnded;
            academicHistoryInfo.DateDegreeCompleted = model.DateDegreeCompleted;
            academicHistoryInfo.AreaOfStudy = model.AreaOfStudy;
            academicHistoryInfo.ResearchProjectTitle = model.ResearchProjectTitle;
            academicHistoryInfo.Comments = model.Comments;
           
            ///Track user inputs ...
            DateTime setUpdateDate = DateTime.Now;
            academicHistoryInfo.DateLastUpdated = DateTime.Now;
            academicHistoryInfo.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
            _context.SaveChanges();

            var ftyTrack = facultyRepository.GetFtyTrack().ToList();
            var ftyPathway = facultyRepository.GetClinicalFtyPathway().ToList();
            var ftyRnk = facultyRepository.GetFtyRnk().ToList();
            var academicHistroyInfoViewModel = new AcademicInfoViewModel
            {
                Id = academicHistoryInfo.Id,
                PersonId = academicHistoryInfo.PersonId,
                InstitutionId = academicHistoryInfo.InstitutionId,      //Institution is required FK and will not except NULL value
                InstName = (academicHistoryInfo.Institution_InstitutionId.Name != null)
                                ? academicHistoryInfo.Institution_InstitutionId.Name
                                : commonRepository.GetDefaultInstitution().Name,

                DateStarted = academicHistoryInfo.DateStarted,
                DateStartedText = String.Format(_DATEFORMAT, academicHistoryInfo.DateStarted),
                DateEnded = academicHistoryInfo.DateEnded,
                DateEndedText = String.Format(_DATEFORMAT, academicHistoryInfo.DateEnded),
                /// Complete Degree
                AcademicDegreeId = academicHistoryInfo.AcademicDegreeId,
                AcademicDegree = (academicHistoryInfo.AcademicDegree != null) ? academicHistoryInfo.AcademicDegree.Name : string.Empty,
                /// Date Degree Complete
                DateDegreeCompleted = academicHistoryInfo.DateDegreeCompleted.HasValue ? academicHistoryInfo.DateDegreeCompleted : null,
                AreaOfStudy = academicHistoryInfo.AreaOfStudy,
                /// Research Experience
                ResearchProjectTitle = academicHistoryInfo.ResearchProjectTitle,
                Comments = (academicHistoryInfo.Comments != null) ? academicHistoryInfo.Comments: string.Empty,
                StrDateLastUpdated = (academicHistoryInfo.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, academicHistoryInfo.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(academicHistoryInfo.LastUpdatedBy)
            };
            return new JsonCamelCaseResult(academicHistroyInfoViewModel, JsonRequestBehavior.AllowGet);
        }  

        public ActionResult Faculties_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(facultyRepository.GetSearch_VwFacultyDemographics().ToDataSourceResult(request));
        }

        public ActionResult MenteeMentor_Read([DataSourceRequest] DataSourceRequest request, Guid personId)
        {
            //return Json(facultyRepository.GetSearch_VwMenteeMentorsSort().ToDataSourceResult(request));
            return Json(facultyRepository.GetSearch_VwMenteeMentors(personId).ToDataSourceResult(request));
            //return View();
        }

        /* ------------------------------- MVN TODO: clean this method code out - no longer need -------------- */
        public JsonResult GetDegreeYear()
        {
            return Json(facultyRepository.GetFtyDegreeInfo()
                .Select(s => new { PersonId = s.PersonId, DegreeYear = s.DegreeYear })
                    .OrderBy(p => p.PersonId), JsonRequestBehavior.AllowGet);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddDegreePopup_Create([DataSourceRequest] DataSourceRequest request, FacultyAcademicData degree)
        {

            if (degree != null && ModelState.IsValid)
            {
                //productService.Create(degree);
                facultyRepository.AddUpdateDegree(degree);
            }

            return Json(new[] { degree }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult FacultyResearchEditingPopup_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(facultyRepository.getFacultyResearchInterest().ToDataSourceResult(request));
        }
       
        public ActionResult AddResearchPopup_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(facultyRepository.getDegree().ToDataSourceResult(request));
        }
        public ActionResult FacultyResearchInsterest_Read([DataSourceRequest] DataSourceRequest request)
        {
            var result = facultyRepository.getFacultyResearchInterest();
            return Json(result.ToDataSourceResult(request, record => new
            {
                record.Id,
                record.PersonId,
                record.ResearchInterest
            }));
        }

        
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddResearchPopup_Create([DataSourceRequest] DataSourceRequest request, FacultyAcademicData degree)
        {

            if (degree != null && ModelState.IsValid)
            {
                //productService.Create(degree);
                facultyRepository.AddUpdateDegree(degree);
            }

            return Json(new[] { degree }.ToDataSourceResult(request, ModelState));
        }

       
        // MVN TODO: to remove this method - no longer need
        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult getFacultyDegree()
        {
            return Json(facultyRepository.getDegree()
                .Select(s => new { AcademicDegrees = s.Id, s.Name })
                    .OrderBy(p => p.Name), JsonRequestBehavior.AllowGet);
        }

        
       
        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult getFacultyDegreeInfo()
        {
            return Json(facultyRepository.getDegreeDegreeInfo()
                .Select(s => new {
                    s.DegreeYear, 
                    FacultyAcademicData_Institution = s.Institution.City,
                    s.Institution.StateProvince,
                    s.Institution.Country
                })
                    .OrderBy(p => p.FacultyAcademicData_Institution), JsonRequestBehavior.AllowGet);
        }


        //[HttpGet]
        //[OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        //public JsonResult getFacultyRank()
        //{
        //    return Json(facultyRepository.GetFacultyRank()
        //        .Select(s => new{FacultyRankID = s.Id, Rank = s.Rank})
        //            .OrderBy(p => p.Rank), JsonRequestBehavior.AllowGet);                       
        //}

        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult getFacultyDegreeDate()
        {
            return Json(facultyRepository.getDegreeDegreeInfo()
                .Select(s => new { AcademicDegreeId = s.AcademicDegreeId, Name = s.DegreeYear })
                    .OrderBy(p => p.AcademicDegreeId), JsonRequestBehavior.AllowGet);
        }
        
        //GetTIU
        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetTIU(string orgType)
        {
            
            // reutrn DateEntered ->FacultyResearchInterest
            return Json(facultyRepository.GetFRInterestOrg()//.Where(p => p.OrganizationType.Id == 3)
                .Select(s => new { OrganizationId = s.Id, s.DisplayName })
                    .OrderBy(p => p.OrganizationId), JsonRequestBehavior.AllowGet);
        }
        // MVN TODO:  TO REMOVE 
        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetFacultyInterestDept()
        {
            // reutrn DateEntered ->FacultyResearchInterest - this code will need to be modified 
            // based on Organization type 1) Devision, 2) Department and 3)TIU
            return Json(facultyRepository.GetFRInterestOrg()
                .Select(s => new { OrganizationId = s.Id, s.DisplayName })
                    .OrderBy(p => p.OrganizationId), JsonRequestBehavior.AllowGet);
        }
        
        
        public ActionResult personPartial()
        {
            return PartialView("_PersonPartial", personRepository);
        }




        [HttpPost]
        public ActionResult AddUpdatePersonRecord(Faculty newFaculty)
        {
            ErrorMessage error = null;
            if (newFaculty != null)
            {
                ErrorCode result = personRepository.AddUpdateFaculty(newFaculty, firstName, middleName, lastName);
                if (result != ErrorCode.Success)
                {
                    error = new ErrorMessage(result.ToString("d"), "fail to add/update.", "");
                }
                else
                {
                    error = new ErrorMessage(result.ToString("d"), "", newFaculty.Id.ToString());
                }
            }
            return Json(error);
        }
    
        [HttpPost]
        public ActionResult PostFaculty(string saveButton, string facultyMidName, string facultyFrtName, string facultyLstName, int? employeeId, int? facultyRankId)
        {
            Faculty faculty = new Faculty();
           
            if (ModelState.IsValid)
            {
                if (!String.IsNullOrEmpty(employeeId.ToString()) ||
                    !String.IsNullOrEmpty(facultyLstName) || 
                    !String.IsNullOrEmpty(facultyMidName) ||
                    !String.IsNullOrEmpty(facultyMidName) ||
                    !String.IsNullOrEmpty(facultyFrtName)          
                    )
                {
                    var result = facultyRepository.getFacultyByID(employeeId);

                    // THIS IS FOR FUTURE EXPANSION
                    if (result != null)
                    {
                        // RECORD FOUND - flag active or not active
                        //faculty.Id = Id.ToString();
                        //result.Id = facultyRepository.getFacultyByID(Id);
                        faculty.EmployeeId = employeeId.ToString();
                        faculty.Person.LastName = facultyLstName;
                        faculty.Person.MiddleName = facultyMidName;
                        faculty.Person.FirstName = facultyFrtName;
                        result = facultyRepository.getFacultyByID(facultyRankId);

                        facultyRepository.AddUpdateFaculty(result);
                        return RedirectToAction("Index");

                    }
                    // NO RECORD FOUND
                    faculty.EmployeeId = employeeId.ToString();
                    faculty.Person.LastName = facultyLstName;
                    faculty.Person.MiddleName = facultyMidName;
                    faculty.Person.FirstName = facultyFrtName;
                    facultyRepository.getFacultyByID(facultyRankId);
                    facultyRepository.AddUpdateFaculty(faculty);
                    return RedirectToAction("Index");
                }
            }
            return View("Index");    
        }

        /// <summary>
        /// MVN - this mentod used return IsDeleted to true back to the UI when the Remove button is clicked
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RemoveAcademicDegreeInfo(Guid id)
        {
            var adg = _context.Set<AcademicHistory>().Find(id);
            if (adg != null){
                adg.IsDeleted = true;
                _context.SaveChanges();
            }
            return Json(id);
        }

        /// <summary>
        /// MVN - this mentod used return IsDeleted to true back to the UI when the Remove button is clicked
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RemoveFtyAptmnt(int id)
        {
            var dg = _context.Set<FacultyAppointment>().Find(id);
            if (dg != null){
                dg.IsDeleted = true;
                _context.SaveChanges();
            }
            return Json(id);
        }
    }
}

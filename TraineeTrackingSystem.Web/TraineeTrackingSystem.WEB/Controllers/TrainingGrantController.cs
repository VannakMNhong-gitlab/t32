﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using TraineeTrackingSystem.Common;
using TraineeTrackingSystem.Repository;
using TraineeTrackingSystem.Data;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using TraineeTrackingSystem.Web.Common;
using TraineeTrackingSystem.Web.PostModels;
using TraineeTrackingSystem.Web.ViewModels;
using System.Security.Claims;
using System.Net;
using System.Xml;
using System.Web.Configuration;
using System.Net.Mime;
using TraineeTrackingSystem.Web.Models;
//using TraineeTrackingSystem.Web.Entity;
//using System.Web.Security;

namespace TraineeTrackingSystem.Web.Controllers
{
    [AuthorizeUser(AccessLevel = "Editor")]
    //[AuthorizeUser(AccessLevel = "Administrator")]
    //[AuthorizeUser(AccessLevel = "SuperUser")]
    //[AuthorizeUser(AccessLevel = "Administor, SuperUser,Editor")]
    public class TrainingGrantController : Controller
    {

        private readonly TrainingGrantRepository trainingGrantRepository;
        //private readonly FundingRepository fundingRepository;
        private readonly CommonDataRepository commonRepository;
        private TTSEntities db = new TTSEntities();

        private readonly TTSEntities _context;
        // MVN - 6/6/2015 - I am commented this field out as its value is never used 
        private readonly string _DATEFORMAT = "{0:yyyy-MM-dd}";     //"{0:MM/dd/yyyy}";
        
        //This globle variable will be used to convert date to specific format 
        private readonly string _LastUpdateDATEFORMAT = "{0:M/dd/yyy hh:mm:tt}";
        public TrainingGrantController()
        {
            _context = new TTSEntities();
            this.trainingGrantRepository = new TrainingGrantRepository(_context);
            this.commonRepository = new CommonDataRepository(_context);
        }

        protected override void Dispose(bool disposing)
        {
            trainingGrantRepository.Dispose();
            base.Dispose(disposing);
        }

        [HttpGet]
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]

        // GET: Mentee
        public ActionResult Index()
        {

            var trainingGrantData = trainingGrantRepository.GetSearch_VwTrainingGrantGeneralData();
            return View(trainingGrantData);
        }

        // DO NOT DELETE - Called by the Landing page to display/sort relevant Training Grant data in the grid
        public virtual JsonResult TrainingGrants_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(trainingGrantRepository.GetSearch_VwTrainingGrantGeneralData().ToDataSourceResult(request));
        }

        //public ActionResult Search()
        //{
        //    return View();
        //}

        // GET: /TrainingGrant/Details/5
        //public ActionResult Details(Guid? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    TrainingGrant traininggrant = db.TrainingGrants.Find(id);
        //    if (traininggrant == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(traininggrant);
        //}

        // GET: /TrainingGrant/Create
        public ActionResult Create()
        {
            var fullFacultyList = commonRepository.GetFacultyList().ToList();
            var states = _context.StateProvinces.OrderBy(s => s.FullName).ToList();
            var countries = _context.Countries.ToList();
            var trainingGrantViewModel = new TrainingGrantViewModel
            {
                //FacultyList = fullFacultyList.Where(f => f.IsActive == true && f.FullName != null).Select(f => new KeyValuePair<Guid?, string>(f.Id, f.FullName)),
                FacultyList = fullFacultyList.Select(f => new KeyValuePair<Guid, string>(f.Id, f.FullName)),
                States = states.Select(s => new KeyValuePair<int, string>(s.Id, s.Abbreviation)),
                Countries = countries.Select(c => new KeyValuePair<int, string>(c.Id, c.Name))
            };
            return View(new EditTrainingGrantViewModel
            {
                TrainingGrantGeneralInfo = trainingGrantViewModel
            });
        }

        public ActionResult CreateTrainingGrant(TrainingGrantPostModel newTrainingGrantModel)
        {
            Institution institution = null;

            if (_context.Set<Institution>().Any(i => i.Name == newTrainingGrantModel.SponsorName))
            {
                institution = _context.Set<Institution>().Single(i => i.Name == newTrainingGrantModel.SponsorName);
                newTrainingGrantModel.InstitutionId = institution.Id;
            }
            
            // Create the parent Funding record that this Training Grant will pull from
            var newFunding = new Funding
            {
                Id = Guid.NewGuid(),
                Title = newTrainingGrantModel.Title,
                SponsorId = newTrainingGrantModel.InstitutionId,
            };

            // Create the TrainingGrant child record
            //MVN commented this out =>TrainingGrant newTrainingGrant = new TrainingGrant();
            // added these line of code to obtain the last update information during created new TrainingGrant
            TrainingGrant newTrainingGrant = trainingGrantRepository.AddTrainingGrant(newFunding);
            newTrainingGrant.DateLastUpdated = DateTime.Now;
            newTrainingGrant.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
            //MVN Commented this out =>newTrainingGrant = trainingGrantRepository.AddTrainingGrant(newFunding);

            // Add the PD, if selected
            if (newTrainingGrantModel.PdFacultyId.Equals(null) == false)
            {
                FundingFaculty pd = commonRepository.AddUpdateFundingFaculty(newTrainingGrantModel.PdFacultyId, newFunding.Id, 6, 0);
                _context.Set<FundingFaculty>().Add(pd);
            }

            return Json(newTrainingGrant.Id);
        }

        /// <summary>
        /// MVN - This private method is to provide the hiding fields for a more elegant way and much more easily to maintaining.  
        /// If there is logic changed, we can just update this method, as appose to multiple places.  
        /// </summary>
        /// <param name="fundingId"></param>
        /// <param name="trainingGrantId"></param>
        private void HideFields(Guid fundingId, Guid trainingGrantId)
        {
            var currTrainingGrant = trainingGrantRepository.getTrainingGrantById(trainingGrantId);
            // hidding or displaying the lastUpdate Info for General Information
            // For use in hiding/displaying appropriate Training Grant sections
            var currStatus = currTrainingGrant.TrainingGrantStatusId;
            // hidding or displaying the lastUpdate Info for General Information
            ViewBag.LastUpdated = (currTrainingGrant.DateLastUpdated.HasValue == false) ? true : false;
            // mvn - added for award info
            ViewBag.AwardLastUpdated = (currTrainingGrant.Funding.DateLastUpdated.HasValue == false) ? true : false;
            ViewBag.IsRenewal = (currTrainingGrant.Funding.IsRenewal == true) ? false : true;
            ViewBag.IsNew = (currTrainingGrant.Funding.DateProjectedStart.HasValue == false) ? true : false;
            // Award Info is hidden if Status IN Planning (1), Pending (2), and Not Funded (6)
            ViewBag.IsAwardInfoHidden = ((currStatus < 3) || (currStatus == 6)) ? true : false;
            // Cost Info is hidden if Status IN Planning (1), Pending (2), and Not Funded (6)
            ViewBag.IsCostInfoHidden = ((currStatus < 3) || (currStatus == 6)) ? true : false;
            ViewBag.IsPreviousHidden = !currTrainingGrant.Funding.IsRenewal;
        }

        [HttpGet]
        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Edit(Guid id)
        {
            var currTrainingGrant = trainingGrantRepository.getTrainingGrantById(id);

            var organizationType = commonRepository.GetOrganizationList().ToList();
            var programList = commonRepository.GetProgramList().ToList();
            var trainingGrantStatusTypes = commonRepository.GetTrainingGrantStatus().ToList();
            var states = _context.StateProvinces.OrderBy(s => s.FullName).ToList();
            var countries = _context.Countries.ToList();
            // USE => MenteeDemographics table to display fullname => post to Mentee
            var menteeList = commonRepository.GetMenteeFullNameList().ToList();
            var getMenteeFullNameList = commonRepository.GetFacultyMenteesList(currTrainingGrant).ToList();
            var getFacultyFullname = commonRepository.GetFacultyList().ToList();    //facultyDemographics    
            var roles = _context.FundingRoles.ToList();
            var budgetPeriodStatus = trainingGrantRepository.GetBudgetPeriodStatus().ToList();
            
              
            ////////////var ffty = currTrainingGrant.Funding.FundingFaculties; //fundingFaculty =>Grid item            
            ///////////////Get the is IsActive faculty and remove if found in FundingFaculty list
            ////////////getFacultyFullname.RemoveAll(x => ffty.Any(y => y.FacultyId == x.Id));
            /* ------------------------- MVN -------------------------------------------------------- */
            /// MVN - Note, compare the Search_VwFacultyDemographics (dropdown lsit) Id 
            /// to => FundingFaculty (Grid) FacultyId, If found, exclude them.
            var gridFftyList = currTrainingGrant.Funding.FundingFaculties.Where(f => f.IsDeleted == false);
            getFacultyFullname.RemoveAll(x => gridFftyList.Any(y => y.FacultyId == x.Id));
            /* -------------------------------------------------------------------------------------- */

            // Use to get DateLastUpdate and lastUpdatedBy only
            var trainingGrant = commonRepository.GetTGLastUpdateInfo(currTrainingGrant.FundingId);
            var researchInterestList = commonRepository.getFacultyResearchInterestList().ToList();           
            //var getFacultyMenteesList = commonRepository.GetSearch_VwTrainingGrantMentees(currTrainingGrant).ToList();

            var trainingGrantGeneralVM = new TrainingGrantViewModel
            {
                Id = currTrainingGrant.Id,
                FundingId = currTrainingGrant.FundingId,
                Title = currTrainingGrant.Funding.Title,
                SponsorId = currTrainingGrant.Funding.SponsorId,
                SponsorName = currTrainingGrant.Funding.Institution_SponsorId != null
                        ? currTrainingGrant.Funding.Institution_SponsorId.Name
                        : string.Empty,
                
                States = states.Select(s => new KeyValuePair<int, string>(s.Id, s.Abbreviation)),
                Countries = countries.Select(c => new KeyValuePair<int, string>(c.Id, c.Name)),
                Programs = programList.Select(p => new KeyValuePair<Guid, string>(p.Id, p.Title)),
                Departments = organizationType.Select(o => new KeyValuePair<int, string>(o.Id, o.DisplayName)),

                TrainingGrantStatusId = currTrainingGrant.TrainingGrantStatusId,
                TrainingGrantStatusName = trainingGrantStatusTypes.Select(f => new KeyValuePair<int, string>(f.Id, f.Name)),
                DateProjectedStart = currTrainingGrant.Funding.DateProjectedStart,
                DateProjectedEnd = currTrainingGrant.Funding.DateProjectedEnd,
                GrtNumber = currTrainingGrant.Funding.GrtNumber,
                SponsorReferenceNumber = currTrainingGrant.Funding.SponsorReferenceNumber,
                SelectedPrograms = commonRepository.GetTrainingGrantProgramList(currTrainingGrant.Id).Select(p => p.ProgramId),
                SelectedDepartments = commonRepository.GetTrainingGrantDepartmentList(currTrainingGrant.Id).Select(d => d.DepartmentId),
                DoctoralLevelId = currTrainingGrant.DoctoralLevelId,
                IsRenewal = currTrainingGrant.Funding.IsRenewal,
                NumPredocPositionsRequested = currTrainingGrant.NumPredocPositionsRequested,
                NumPostdocPositionsRequested = currTrainingGrant.NumPostdocPositionsRequested,
                StrDateLastUpdated = (trainingGrant.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, trainingGrant.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(trainingGrant.LastUpdatedBy)
            };
          
            var previousTrainingGrant = new TrainingGrantsViewModel
            {
                Id = currTrainingGrant.Id,
                SelectedId = currTrainingGrant.PreviousTrainingGrantId,
                PreviousTrainingGrants = trainingGrantRepository.GetSearch_VwTrainingGrantGeneralData()
                    .Where(tg => tg.Id != currTrainingGrant.Id)
                    .Select(tg => new TrainingGrantViewModel
                    {
                        Id = tg.Id,
                        FundingId = tg.FundingId,
                        PdFullName = tg.PdFullName,
                        Title = tg.Title.ToString().Trim(),
                        SponsorId = tg.SponsorId,
                        SponsorName = tg.SponsorName,
                        SponsorAwardNumber = tg.SponsorAwardNumber
                    })
            };
            var participatingFaculty = new FacultySupportsViewModel{
                Id = currTrainingGrant.Id,
                FundingId = currTrainingGrant.FundingId,
                FundingFacultiesList = getFacultyFullname.Select(f => new KeyValuePair<Guid?, string>(f.Id, f.FullName)),
                //RoleList = roles.Where(r => r.IsTrainingGrantRole == true).Select(r => new KeyValuePair<int, string>(r.Id, r.Name)),
                RoleList = roles.Where(r => r.IsTrainingGrantRole == true).Select(r => new KeyValuePair<int, string>(r.Id, r.Name)),

                FacultyInterests = researchInterestList.Select(ri => new KeyValuePair<int, string>(ri.Id, ri.ResearchInterest)),
                /// ******************** Faculty Support GRID ***********************/
                /// MVN NOTE maintaining the two lists Grid and faculty dropdown list :  when the grid is updated, the above field, 
                /// in the faculty dropdown list, is not refresh, because it outside of the grid.. so, to resolve this
                /// the best way is use the javascript reload in the UI => to reload the current document.  
                //FacultySupportGridView = (currTrainingGrant.Funding.FundingFaculties != null)? currTrainingGrant.Funding.FundingFaculties
                FacultySupportGridView = currTrainingGrant.Funding.FundingFaculties.Where(fac => fac.IsDeleted == false)
                    .Select(fac => new FacultySupportViewModel{
                        Id = fac.Id,
                        FacultyId = fac.FacultyId,
                        FacultyFullName = commonRepository.GetPersonFullName(fac.Faculty.PersonId),
                        FacultyRoleId = fac.PrimaryRoleId,
                        FacultyRole = (fac.FundingRole_PrimaryRoleId != null) ? fac.FundingRole_PrimaryRoleId.Name : string.Empty,
                        FacultySecondaryRoleId = fac.SecondaryRoleId,
                        FacultySecondaryRole = (fac.FundingRole_SecondaryRoleId != null) ? fac.FundingRole_SecondaryRoleId.Name : string.Empty,
                        FacultyPrimaryOrgId = (fac.Faculty.Organization_PrimaryOrganizationId != null)
                            ? fac.Faculty.Organization_PrimaryOrganizationId.Id : 0,
                        FacultyPrimaryOrg = (fac.Faculty.Organization_PrimaryOrganizationId != null)
                            ? fac.Faculty.Organization_PrimaryOrganizationId.DisplayName : string.Empty,
                        SelectedResearchInterests = commonRepository.GetTrainingGrantResearchInterestList(currTrainingGrant.Id, fac.FacultyId)
                            .Select(ri => ri.ResearchInterestId),
                        SelectedFacultyAllInterests = commonRepository.getFacultyResearchInterestsByFacultyId(fac.FacultyId)
                            .Select(ri => new InterestInfo { ResearchId = ri.Id, Name = ri.ResearchInterest })
                            .OrderBy(ii => ii.Name),                 
                        StrDateLastUpdated = (fac.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, fac.DateLastUpdated) : string.Empty,
                        StrLastUpdatedBy = commonRepository.GetLoginInfo(fac.LastUpdatedBy)
                    //}):null
                    })
            };
            var mentees = new TrainingGrantMenteesViewModel
            {
                TrainingGrantId = currTrainingGrant.Id,
                FacultyMentees = commonRepository.GetSearch_VwTrainingGrantMentees(currTrainingGrant)                    
                    .Select(mentee => new TrainingGrantMenteeViewModel
                    {
                        TrainingGrantId = currTrainingGrant.Id,
                        MenteeId = mentee.MenteeId,
                        MenteePersonId = mentee.MenteePersonId,
                        MenteeFullName = mentee.MenteeFullName,
                        DoctoralLevel = mentee.MenteeType,
                        YearStarted = mentee.TrainingPeriodYearStarted,
                        YearEnded = mentee.TrainingPeriodYearEnded,
                        MentorFullName = mentee.FacultyFullName
                    })
            };

            var applicants = new TrainingGrantApplicantsViewModel
            {
                TrainingGrantId = currTrainingGrant.Id,
                ApplicantPoolAcademicYear = (currTrainingGrant.ApplicantPoolAcademicYear != null) ? currTrainingGrant.ApplicantPoolAcademicYear : 0,
                TrainingGrantApplicants = commonRepository.GetSearch_VwTrainingGrantApplicants(currTrainingGrant)
                    .Where(app => app.YearEntered == currTrainingGrant.ApplicantPoolAcademicYear)
                    .Select(app => new TrainingGrantApplicantViewModel 
                    {
                        TrainingGrantId = currTrainingGrant.Id,
                        ApplicantGuid = app.ApplicantGuid,
                        ApplicantId = app.ApplicantId,
                        DoctoralLevel = app.ApplicantType,
                        DepartmentId = app.ApplicantDeptId,
                        DepartmentName = app.ApplicantDept,
                        YearEntered = app.YearEntered,
                        ProgramListFlat = app.ApplicantPrograms
                    })              
            };

            // Get all pending/active grants associated with all participating faculty on this training grant
            var associatedTGrants = commonRepository.GetOtherGrantsAssociatedToFundingId(currTrainingGrant.FundingId, true, false);

            var otherGrantsViewModel = new InstitutionTrainingGrants
            {
                TrainingGrantId = currTrainingGrant.Id,
                // Grab all of the associated grant records that have a PD
                FundingGridView = commonRepository.GetSearch_VwTrainingGrantFaculty()
                    .Where(ff => associatedTGrants.Contains(ff.TrainingGrantId) && ff.FacultyPrimaryRoleId == 6)
                    .Select(ff => new InstitutionTrainingGrant
                    {
                        TrainingGrantId = ff.TrainingGrantId,
                        PDFullName = ff.FacultyFullName,
                        SponsorName = ff.SponsorName,
                        Title = ff.Title,
                        TrainingGrantStatus = ff.TrainingGrantStatus
                    })
                    .OrderBy(ff => ff.PDFullName)
            };

            // Get all pending/active funding associated with all participating faculty on this training grant
            var associatedFunds = commonRepository.GetOtherFundingAssociatedToFundingId(currTrainingGrant.FundingId, true, false);

            var otherFundingViewModel = new OtherFundingsInfo
            {
                TrainingGrantId = currTrainingGrant.Id,
                // Grab all of the associated funding records that have a PI
                OtherGridView = commonRepository.GetSearch_VwFundingFaculty()
                    .Where(ff => associatedFunds.Contains(ff.Id) && ff.FacultyRoleId == 1)
                    .Select(ff => new OtherFundingInfo
                    {
                        Id = ff.Id,
                        PIFullName = ff.FacultyFullName,                        
                        SponsorName = ff.SponsorName,
                        Title = ff.Title,
                        FundingStatus = ff.FundingStatus
                    })
                    .OrderBy(ff => ff.PIFullName)
            };

            // MVN - adding Award Information
            // Use to get DateLastUpdate and lastUpdatedBy only
            var funding = commonRepository.GetFDLastUpdateInfo(currTrainingGrant.FundingId);
            var awardInfoView = new AwardInformationViewModel
            {
                //issue with here =>Id = id,
                ///MVN - should be the following code instead
                Id = currTrainingGrant.Id,
                FundingId = currTrainingGrant.Funding.Id,
                SponsorAwardNumber = currTrainingGrant.Funding.SponsorAwardNumber,
                DateStarted = currTrainingGrant.Funding.DateStarted,
                DateEnded = currTrainingGrant.Funding.DateEnded,
                StrDateLastUpdated = (currTrainingGrant.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, currTrainingGrant.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(currTrainingGrant.LastUpdatedBy),
            };
            var costInfoView = new CostsInformationViewModel{
                Id = id,
                FundingId = currTrainingGrant.Funding.Id,
                BudgetPeriodStatuses = budgetPeriodStatus.Select(f => new KeyValuePair<int, string>(f.Id, f.Name)),
                CostInformationGridView = currTrainingGrant.Funding.FundingDirectCosts
                                                                   .Where(c => c.IsDeleted == false)
                                                                   .Select(cost => new CostInformationViewModel{
                                                                          Id = cost.Id,                //setting the PK FundingDirectCost Id
                                                                          DateStarted = cost.DateStarted,     // for the grid row selection                                           
                                                                          DateStartedText = String.Format(_DATEFORMAT, cost.DateStarted),
                                                                          DateEnded = cost.DateEnded,         // for the grid row selection
                                                                          DateEndedText = String.Format(_DATEFORMAT, cost.DateEnded),
                                                                          CurrentYearDirectCosts = cost.CurrentYearDirectCosts,
                                                                          TotalDirectCosts = cost.TotalDirectCosts,
                                                                          BudgetPeriodStatusId = cost.BudgetPeriodStatusId,
                                                                          BudgetPeriodStatus = (cost.BudgetPeriodStatu != null)? cost.BudgetPeriodStatu.Name :string.Empty,
                                                                          DateLastUpdated = cost.DateLastUpdated,
                                                                          
                                                                          StrDateLastUpdated = (cost.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, cost.DateLastUpdated) : string.Empty,
                                                                          StrLastUpdatedBy = commonRepository.GetLoginInfo(cost.LastUpdatedBy)
                                                                      })

            };

            /***************** Trainee Detailed Information *********************************
            // MVN - 3/12/2015
            * Grid and expand 
            *********************************************************************************/
            // Use to get DateLastUpdate and lastUpdatedBy only
            var trainingGrantTrainee = commonRepository.GetTGTLastUpdateInfo(currTrainingGrant.Id);
            var traineeDetailedInfoView = new TraineesDetailedInformationViewModel
            {
                TrainingGrantId = currTrainingGrant.Id,
                MenteeListDropdown = getMenteeFullNameList.Select(m => new KeyValuePair<Guid?, string>(m.MenteeId, m.MenteeFullName)),
 
                // ******************** TRAINEE GRID ***********************/
                TraineeDetailedInformationView = currTrainingGrant.TrainingGrantTrainees
                                              .Select(trainee => new TraineeDetailedInformationViewModel
                                              {
                                                 //Id = trainee.Id,                           // Set Trainee Id
                                                 TrainingGrantTraineeId = trainee.Id,
                                                 TrainingGrantId = currTrainingGrant.Id,
                                                 MenteeId = trainee.MenteeId,
                                                 MenteePersonId = trainee.Mentee.PersonId,
                                                 IsTrainingGrantEligible = trainee.Mentee.IsTrainingGrantEligible,
                                                 // ******************** Program Status radio button **********************/
                                                 DoctoralLevelId = trainee.DoctoralLevelId,
                                                 PredocTraineeStatusId = trainee.PredocTraineeStatusId,
                                                 PostdocTraineeStatusId = trainee.PostdocTraineeStatusId,
                                                 PredocReasonForLeaving = trainee.PredocReasonForLeaving,
                                                 PostdocReasonForLeaving = trainee.PostdocReasonForLeaving,                                                 
                                                 MenteeFullName = commonRepository.GetPersonFullName(trainee.Mentee.PersonId),
                                                 InstitutionAssociationId = trainee.Mentee.InstitutionAssociationId,
                                                 InstitutionAssociation = trainee.Mentee.InstitutionAssociation != null ? trainee.Mentee.InstitutionAssociation.Name : string.Empty,
                                                 TraineeStatus = (trainee.DoctoralLevelId == 1) ? (trainee.PredocTraineeStatusId.HasValue 
                                                                                                ? trainee.TraineeStatu_PredocTraineeStatusId.Name : string.Empty) 
                                                                                                : (trainee.PostdocTraineeStatusId.HasValue 
                                                                                                ? trainee.TraineeStatu_PostdocTraineeStatusId.Name : string.Empty),
                                                 DateStarted = trainee.DateStarted,
                                                 DateEnded = trainee.DateEnded,


                                                 ResearchProjectTitle = trainee.TrainingPeriod != null ? trainee.TrainingPeriod.ResearchProjectTitle : string.Empty,
                                                 SelectedTrainingPeriodId = trainee.TrainingPeriodId.HasValue ? trainee.TrainingPeriodId.Value : Guid.Empty,
                                                 
                                                 //Tracking form altered with date and user
                                                 //DateLastUpdated = trainee.DateLastUpdated,
                                                 //LastUpdatedBy = trainee.LastUpdatedBy,
                                                 StrDateLastUpdated = (trainee.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, trainee.DateLastUpdated) : string.Empty,
                                                 StrLastUpdatedBy = commonRepository.GetLoginInfo(trainee.LastUpdatedBy),

                                                  // ******************** TRAINING PERIOD GRID ***********************/
                                                 MenteeTrainingPeriods = trainee.Mentee.Person.TrainingPeriods
                                                     .Select(tp => new MenteeTrainingPeriodViewModel
                                                     {
                                                         Id = tp.Id,                           //Set TrainingPeriod Id                   
                                                         PersonId = tp.PersonId,
                                                         InstitutionId = tp.InstitutionId,
                                                         InstitutionName = tp.InstitutionId.HasValue ? tp.Institution.Name : commonRepository.GetDefaultInstitution().Name,
                                                         DoctoralLevelId = tp.DoctoralLevel.Id,
                                                         DoctoralLevel = tp.DoctoralLevel.LevelName,                                                                                                 
                                                         AcademicDegreeId = tp.AcademicDegreeId,
                                                         // Molley added new FK for Degree and DegreeSought 7/30/2013
                                                         //AcademicDegreeName = tp.AcademicDegreeId.HasValue ? tp.AcademicDegree.Name : string.Empty,
                                                         AcademicDegreeName = tp.AcademicDegreeId.HasValue ? tp.AcademicDegree_AcademicDegreeId.Name :string.Empty,
                                                         YearStarted = tp.YearStarted,
                                                         YearEnded = tp.YearEnded,                                                                  
                                                     })
                                              })
            };

            /***************** Trainee Summary Information **********************************
            // MVN - 3/4/2015
            *********************************************************************************/
            // Use to get DateLastUpdate and lastUpdatedBy only
            var traineeSummaryInfo = commonRepository.GetTSLastUpdateInfo(currTrainingGrant.Id);
            var preDoctraineeSummaryInfoView = new TraineesSummaryInformationViewModel
            {
                TrainingGrantId = currTrainingGrant.Id,                            
                //DisadvantagedSupportMonthsUsed = traineeSummaryInfo.DisadvantagedSupportMonthsUsed.HasValue ? traineeSummaryInfo.DisadvantagedSupportMonthsUsed : null,
                 //TraineeSummaryInformationGridView = currTrainingGrant.TraineeSummaryInfoes.Where(t => t.DoctoralLevelId == 1 && t.DoctoralLevelId != null)
                //TraineeSummaryInformationGridView = (currTrainingGrant.TraineeSummaryInfoes != null) ? currTrainingGrant.TraineeSummaryInfoes.Where(t => t.DoctoralLevelId == 1 && t.DoctoralLevelId != 0)
                TraineeSummaryInformationGridView = (currTrainingGrant.TraineeSummaryInfoes != null) ? currTrainingGrant.TraineeSummaryInfoes.Where(t => t.DoctoralLevelId == 1)
                                              .Select(traineeSummary => new TraineeSummaryInformationViewModel{
                                                  TraineeSummaryId = traineeSummary.Id,
                                                  TrainingGrantId = currTrainingGrant.Id,                                                 
                                                  DateStarted = traineeSummary.DateStarted.HasValue ? traineeSummary.DateStarted : null,     // for the grid row selection                                           
                                                  DateStartedText = (traineeSummary.DateStarted != null) ? String.Format(_DATEFORMAT, traineeSummary.DateStarted) : string.Empty,
                                                  DateEnded = traineeSummary.DateEnded.HasValue ? traineeSummary.DateEnded : null,         // for the grid row selection
                                                  DateEndedText = (traineeSummary.DateEnded != null) ? String.Format(_DATEFORMAT, traineeSummary.DateEnded) : string.Empty,
                                                  PositionsAwarded = traineeSummary.PositionsAwarded.HasValue ? traineeSummary.PositionsAwarded : null,
                                                  TraineesAppointed = traineeSummary.TraineesAppointed.HasValue ? traineeSummary.TraineesAppointed : null,
                                                  UrmTraineesAppointed = traineeSummary.UrmTraineesAppointed.HasValue ? traineeSummary.UrmTraineesAppointed : null,
                                                  DisabilitiesTraineesAppointed = traineeSummary.DisabilitiesTraineesAppointed.HasValue ? traineeSummary.DisabilitiesTraineesAppointed : null,
                                                  DisadvantagedTraineesAppointed = traineeSummary.DisadvantagedTraineesAppointed.HasValue ? traineeSummary.DisadvantagedTraineesAppointed : null,

                                                  //Grid selection 
                                                  SupportMonthsAwarded = traineeSummary.SupportMonthsAwarded.HasValue ? traineeSummary.SupportMonthsAwarded : null,
                                                  SupportMonthsUsed = traineeSummary.SupportMonthsAwarded.HasValue ? traineeSummary.SupportMonthsAwarded : null,
                                                  UrmSupportMonthsUsed = traineeSummary.UrmSupportMonthsUsed.HasValue  ? traineeSummary.UrmSupportMonthsUsed : null,
                                                  DisabilitiesSupportMonthsUsed = traineeSummary.DisabilitiesSupportMonthsUsed.HasValue ? traineeSummary.DisabilitiesSupportMonthsUsed : null,
                                                  DisadvantagedSupportMonthsUsed = traineeSummary.DisadvantagedSupportMonthsUsed.HasValue ? traineeSummary.DisadvantagedSupportMonthsUsed : null,
                                                  // MVN added tracking user activity 9/18/2015
                                                  StrDateLastUpdated = (traineeSummary.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, traineeSummary.DateLastUpdated) : string.Empty,
                                                  StrLastUpdatedBy = commonRepository.GetLoginInfo(traineeSummary.LastUpdatedBy),
                                             }) : null
                                              //})
            };
            var postDoctraineeSummaryInfoViewnew = new TraineesSummaryInformationViewModel
            {
                TrainingGrantId = currTrainingGrant.Id,
               
                //StrDateLastUpdated = (traineeSummaryInfo != null) ? String.Format(_LastUpdateDATEFORMAT, traineeSummaryInfo.DateLastUpdated) : string.Empty,
                //LastUpdatedBy = (traineeSummaryInfo != null) ? traineeSummaryInfo.LastUpdatedBy : string.Empty,
                TraineeSummaryInformationGridView = (currTrainingGrant.TraineeSummaryInfoes != null) ? currTrainingGrant.TraineeSummaryInfoes.Where(t => t.DoctoralLevelId == 2)
                //TraineeSummaryInformationGridView = (currTrainingGrant.TraineeSummaryInfoes != null) ? currTrainingGrant.TraineeSummaryInfoes.Where(t => t.DoctoralLevelId == 2 && t.DoctoralLevelId != 0)
                //TraineeSummaryInformationGridView = currTrainingGrant.TraineeSummaryInfoes.Where(t => t.DoctoralLevelId == 2 && t.DoctoralLevelId != null)
                                              .Select(traineeSummary => new TraineeSummaryInformationViewModel
                                              {
                                                  TraineeSummaryId = traineeSummary.Id,
                                                  TrainingGrantId = currTrainingGrant.Id,
                                                  DateStarted = traineeSummary.DateStarted.HasValue ? traineeSummary.DateStarted : null,                                      
                                                  DateStartedText = (traineeSummary.DateStarted != null) ? String.Format(_DATEFORMAT, traineeSummary.DateStarted) : string.Empty,
                                                  DateEnded = traineeSummary.DateEnded.HasValue ? traineeSummary.DateEnded : null,
                                                  DateEndedText = (traineeSummary.DateEnded != null) ? String.Format(_DATEFORMAT, traineeSummary.DateEnded) : string.Empty,

                                                  PositionsAwarded = traineeSummary.PositionsAwarded.HasValue ? traineeSummary.PositionsAwarded : null,
                                                  TraineesAppointed = traineeSummary.TraineesAppointed.HasValue ? traineeSummary.TraineesAppointed : null,
                                                  UrmTraineesAppointed = traineeSummary.UrmTraineesAppointed.HasValue ? traineeSummary.UrmTraineesAppointed : null,
                                                  DisabilitiesTraineesAppointed = traineeSummary.DisabilitiesTraineesAppointed.HasValue ? traineeSummary.DisabilitiesTraineesAppointed : null,
                                                  DisadvantagedTraineesAppointed = traineeSummary.DisadvantagedTraineesAppointed.HasValue ? traineeSummary.DisadvantagedTraineesAppointed : null,

                                                  //Grid selection 
                                                  SupportMonthsAwarded = traineeSummary.SupportMonthsAwarded.HasValue ? traineeSummary.SupportMonthsAwarded : null,
                                                  SupportMonthsUsed = traineeSummary.SupportMonthsAwarded.HasValue ? traineeSummary.SupportMonthsAwarded : null,
                                                  UrmSupportMonthsUsed = traineeSummary.UrmSupportMonthsUsed.HasValue ? traineeSummary.UrmSupportMonthsUsed : null,
                                                  DisabilitiesSupportMonthsUsed = traineeSummary.DisabilitiesSupportMonthsUsed.HasValue ? traineeSummary.DisabilitiesSupportMonthsUsed : null,
                                                  DisadvantagedSupportMonthsUsed = traineeSummary.DisadvantagedSupportMonthsUsed.HasValue ? traineeSummary.DisadvantagedSupportMonthsUsed : null,

                                                  NumMdAppointed = traineeSummary.NumMdAppointed.HasValue ? traineeSummary.NumMdAppointed : null,
                                                  NumMdPhDAppointed = traineeSummary.NumMdPhDAppointed.HasValue ? traineeSummary.NumMdPhDAppointed : null,
                                                  NumPhDAppointed = traineeSummary.NumPhDAppointed.HasValue ? traineeSummary.NumPhDAppointed : null,
                                                  /// MVN - Development note - Keep this below as ref - this code will send 0 to UI, when the the value is null
                                                  ///NumOtherDegreeAppointed = (traineeSummary.NumOtherDegreeAppointed != null) ? traineeSummary.NumOtherDegreeAppointed : 0,
                                                  NumOtherDegreeAppointed = traineeSummary.NumOtherDegreeAppointed.HasValue ? traineeSummary.NumOtherDegreeAppointed : null,
                                                  // MVN added tracking user activity 9/18/2015
                                                  StrDateLastUpdated = (traineeSummary.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, traineeSummary.DateLastUpdated) : string.Empty,
                                                  StrLastUpdatedBy = commonRepository.GetLoginInfo(traineeSummary.LastUpdatedBy),
                                              }) : null
                                              //}) 
            };
            /// MVN - removed the following code, and replaced with HideFields method to handle all these tasks,
            /// as in general we wanted to hide lastupdate labels when there is no user update activity, however,
            /// we also want to do the same for award information 
            /// *********************** MVN ********************************************************************
            HideFields(currTrainingGrant.FundingId, id);   // pass in and TrainingGrantId
            //////// For use in hiding/displaying appropriate Training Grant sections
            //////var currStatus = currTrainingGrant.TrainingGrantStatusId;

            //////// hidding or displaying the lastUpdate Info for General Information
            //////ViewBag.LastUpdated = (trainingGrant.DateLastUpdated.HasValue == false) ? true : false;
            
            //////ViewBag.IsRenewal = (currTrainingGrant.Funding.IsRenewal == true) ? false : true;
            //////ViewBag.IsNew = (currTrainingGrant.Funding.DateProjectedStart.HasValue == false) ? true : false;
            //////// Award Info is hidden if Status IN Planning (1), Pending (2), and Not Funded (6)
            //////ViewBag.IsAwardInfoHidden = ((currStatus < 3) || (currStatus == 6)) ? true : false;
            //////// Cost Info is hidden if Status IN Planning (1), Pending (2), and Not Funded (6)
            //////ViewBag.IsCostInfoHidden = ((currStatus < 3) || (currStatus == 6)) ? true : false;
            //////ViewBag.IsPreviousHidden = !currTrainingGrant.Funding.IsRenewal;

            return View(new EditTrainingGrantViewModel
            {
                TrainingGrantGeneralInfo = trainingGrantGeneralVM,
                PreviousTrainingGrant = previousTrainingGrant,
                ParticipatingFaculty = participatingFaculty,
                FacultyMentees = mentees,
                ApplicantPool = applicants,
                TrainingGrantSupportGrants = otherGrantsViewModel,
                TrainingGrantSupportOther = otherFundingViewModel,
                AwardInfo = awardInfoView,
                CostInfo = costInfoView,
                PreDocTraineeSummaryInfo = preDoctraineeSummaryInfoView,
                PostDocTraineeSummaryInfo = postDoctraineeSummaryInfoViewnew,
                TraineeDetailedInfo = traineeDetailedInfoView,
            });
        }
        [HttpPost]
        public ActionResult UpdateFacultySupport(FacultySupportPostModel updateFacultySupportModel)
        {
            // Guid.Empty is "{00000000-0000-0000-0000-000000000000}", which located the representation range of a guid, essentially Empty!!! 
            bool isNew = updateFacultySupportModel.Id.Equals(Guid.Empty) || !_context.Set<FundingFaculty>()
                       .Any(ffty => ffty.Id == updateFacultySupportModel.Id);

            var newFFtySupport = isNew
                        ? new FundingFaculty()
                        : _context.Set<FundingFaculty>()
                        .Single(ffty => ffty.Id == updateFacultySupportModel.Id);

            var trainGrant = _context.Set<TrainingGrant>()
                .Include(tg => tg.Funding)
                .Single(tg => tg.FundingId == updateFacultySupportModel.FundingId);

            newFFtySupport.FundingId = updateFacultySupportModel.FundingId;
            newFFtySupport.FacultyId = updateFacultySupportModel.FacultyId;
            newFFtySupport.PrimaryRoleId = updateFacultySupportModel.FacultyRoleId;
            newFFtySupport.SecondaryRoleId = updateFacultySupportModel.FacultySecondaryRoleId;

            // MVN - added last update data collection here
            // Track last updated date and user
            newFFtySupport.DateLastUpdated = DateTime.Now;
            newFFtySupport.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);


            /* *********************** RESEARCH INTERESTS ************************ */
            //MVN commneted this =>var facPersonId = commonRepository.GetPersonIdForFacultyId(updateFacultySupportModel.FacultyId);
            // replaced the below code, instead
            var facPersonId = commonRepository.GetPersonIdForFacultyId(newFFtySupport.FacultyId);
            var savedResearchInterests = trainGrant.FacultyResearchInterests.Where(fri => fri.PersonId == facPersonId).ToList();
            // ADD newly selected ResearchInterests to database
            if (updateFacultySupportModel.ResearchInterests != null)
            {
                foreach (var selectedInterestId in updateFacultySupportModel.ResearchInterests)
                {
                    if (trainGrant.FacultyResearchInterests.All(ri => ri.Id != selectedInterestId))
                    {
                        var addResearchInterest = _context.Set<FacultyResearchInterest>().Find(selectedInterestId);
                        trainGrant.FacultyResearchInterests.Add(addResearchInterest);
                    }
                }
            }
            // DELETE previously-saved, just removed ResearchInterests from database
            FacultyResearchInterest remResearchInterests = new FacultyResearchInterest();
            foreach (var savedInterest in savedResearchInterests)
            {
                if (updateFacultySupportModel.ResearchInterests != null)
                {
                    if (!updateFacultySupportModel.ResearchInterests.Contains(savedInterest.Id))
                    {
                        remResearchInterests = _context.Set<FacultyResearchInterest>().Find(savedInterest.Id);
                    }
                }
                else
                {
                    remResearchInterests = _context.Set<FacultyResearchInterest>().Find(savedInterest.Id);
                }

                if (remResearchInterests != null)
                {
                    trainGrant.FacultyResearchInterests.Remove(remResearchInterests);
                }
            }


            if (isNew)
            {
                _context.Set<FundingFaculty>().Add(newFFtySupport);
            }
            _context.SaveChanges();

            // Pull Faculty member record to get their dept
            /// MVN Commented this out 9/23/2015
            ////////////newFFtySupport.Faculty = _context.Set<Faculty>()
            ////////////            .Single(fac => fac.Id == updateFacultySupportModel.FacultyId);

            /// ************************************************************
            /// Add Faculty Information dropdown List - lists all faculties
            /// 8/22/2015 - fundingFacultiesList need to exclude the faculty that already selected in the grid
            /// IMPORTANT NOTE:  FacultyId from FundingFaculty map to Id in Search_VwFacultyDemographics table  
            /// ************************************************************
            ////////var getFacultyFullname = commonRepository.GetFacultyList().ToList();
            ////////var currTrainingGrant = trainingGrantRepository.getTrainingGrantById(trainGrant.Id);
            ////////var currffty = currTrainingGrant.Funding.FundingFaculties;
            //////// Exclude all by matching facultyId in the Search_VwFacultyDemographics table
            ////////getFacultyFullname.RemoveAll(x => currffty.Any(y => y.FacultyId == x.Id));
            ////////var roles = _context.FundingRoles.ToList();
            var researchInterestList = commonRepository.getFacultyResearchInterestList().ToList();

            /* ---------------------------- dynamically handles the UI grid and drop down list ---------------------- */
            //var currFunding = fundingRepository.GetFundingbyId(updateFacultySupportModel.FundingId);
            var currTrainingGrant = trainingGrantRepository.getTrainingGrantById(trainGrant.Id);
            var facultyDrpdwnList = commonRepository.GetFacultyList().ToList();
            var roles = _context.FundingRoles.ToList();
            var facultyGridList = currTrainingGrant.Funding.FundingFaculties.Where(f => f.IsDeleted == false);
            facultyDrpdwnList.RemoveAll(x => facultyGridList.Any(y => y.FacultyId == x.Id));
            /* ------------------------------------------------------------------------------------------------------ */


            //var facultysupportGrid = new FacultySupportsViewModel
            //{

            //    Id = newFFtySupport.Id,
            //    FundingId = newFFtySupport.FundingId,
            //    /// ************************************************************
            //    /// Add Faculty Information dropdown List - lists all faculties
            //    /// 9/22/2015 - fundingFacultiesList need to exclude the faculty that already selected in the grid
            //    /// IMPORTANT NOTE:  FacultyId from FundingFaculty map to Id in Search_VwFacultyDemographics table  
            //    /// ************************************************************
            //    FundingFacultiesList = facultyDrpdwnList.Select(f => new KeyValuePair<Guid?, string>(f.Id, f.FullName)),
            //    RoleList = roles.Where(r => r.IsTrainingGrantRole == true).Select(r => new KeyValuePair<int, string>(r.Id, r.Name)),

            //    FacultyInterests = researchInterestList.Select(ri => new KeyValuePair<int, string>(ri.Id, ri.ResearchInterest)),
            //    FacultySupportGridView = (currTrainingGrant.Funding.FundingFaculties != null) ? currTrainingGrant.Funding.FundingFaculties
            //        //FacultySupportGridView = currTrainingGrant.Funding.FundingFaculties
            //        .Where(fac => fac.IsDeleted == false)
            //        .Select(fac => new FacultySupportViewModel
            //        {
            //            Id = fac.Id,
            //            FacultyId = fac.FacultyId,
            //            FacultyFullName = commonRepository.GetPersonFullName(fac.Faculty.PersonId),
            //            FacultyRoleId = fac.PrimaryRoleId,
            //            FacultyRole = (fac.FundingRole_PrimaryRoleId != null) ? fac.FundingRole_PrimaryRoleId.Name : string.Empty,
            //            FacultySecondaryRoleId = fac.SecondaryRoleId,
            //            FacultySecondaryRole = (fac.FundingRole_SecondaryRoleId != null) ? fac.FundingRole_SecondaryRoleId.Name : string.Empty,

            //            FacultyPrimaryOrgId = (fac.Faculty.Organization_PrimaryOrganizationId != null)
            //                ? fac.Faculty.Organization_PrimaryOrganizationId.Id : 0,
            //            FacultyPrimaryOrg = (fac.Faculty.Organization_PrimaryOrganizationId != null)
            //                ? fac.Faculty.Organization_PrimaryOrganizationId.DisplayName : string.Empty,

            //            SelectedResearchInterests = commonRepository.GetTrainingGrantResearchInterestList(currTrainingGrant.Id, fac.FacultyId)
            //                .Select(ri => ri.ResearchInterestId),
            //            SelectedFacultyAllInterests = commonRepository.getFacultyResearchInterestsByFacultyId(fac.FacultyId)
            //                .Select(ri => new InterestInfo { ResearchId = ri.Id, Name = ri.ResearchInterest })
            //                .OrderBy(ii => ii.Name),
            //            StrDateLastUpdated = (fac.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, fac.DateLastUpdated) : string.Empty,
            //            StrLastUpdatedBy = commonRepository.GetLoginInfo(fac.LastUpdatedBy)
            //        }) : null

            //};

            // UPDATE the grid  
            var facultysupportGrid = new FacultySupportViewModel
            {
                Id = newFFtySupport.Id,
                FundingId = newFFtySupport.FundingId,
                FacultyId = newFFtySupport.FacultyId,
                FacultyFullName = updateFacultySupportModel.FacultyFullName,
                
                FacultyRoleId = newFFtySupport.PrimaryRoleId,
                FacultyRole = (newFFtySupport.FundingRole_PrimaryRoleId != null) ? newFFtySupport.FundingRole_PrimaryRoleId.Name : string.Empty,
                FacultySecondaryRoleId = newFFtySupport.SecondaryRoleId,
                FacultySecondaryRole = (newFFtySupport.FundingRole_SecondaryRoleId != null) ? newFFtySupport.FundingRole_SecondaryRoleId.Name : string.Empty,
               
                FacultyPrimaryOrgId = (newFFtySupport.Faculty.Organization_PrimaryOrganizationId != null)
                    ? newFFtySupport.Faculty.Organization_PrimaryOrganizationId.Id : 0,
                FacultyPrimaryOrg = (newFFtySupport.Faculty.Organization_PrimaryOrganizationId != null)
                    ? newFFtySupport.Faculty.Organization_PrimaryOrganizationId.DisplayName : string.Empty,

                SelectedResearchInterests = commonRepository.GetTrainingGrantResearchInterestList(currTrainingGrant.Id, newFFtySupport.FacultyId)
                    .Select(ri => ri.ResearchInterestId),
                SelectedFacultyAllInterests = commonRepository.getFacultyResearchInterestsByFacultyId(newFFtySupport.FacultyId)
                    .Select(ri => new InterestInfo { ResearchId = ri.Id, Name = ri.ResearchInterest })
                    .OrderBy(ii => ii.Name),

                StrDateLastUpdated = (newFFtySupport.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, newFFtySupport.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(newFFtySupport.LastUpdatedBy),
                FundingFacultiesList = facultyDrpdwnList.Select(f => new KeyValuePair<Guid?, string>(f.Id, f.FullName)),
                RoleList = roles.Where(r => r.IsTrainingGrantRole == false).Select(r => new KeyValuePair<int, string>(r.Id, r.Name))
            };
            return new JsonCamelCaseResult(facultysupportGrid, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AjaxRequestForAddFtyBtn(FacultySupportPostModel model)
        {
            var currFunding = trainingGrantRepository.GetFundingbyId(model.FundingId);
            var getFacultyFullname = commonRepository.GetFacultyList().ToList();
            var gridFftyList = currFunding.FundingFaculties.Where(f => f.IsDeleted == false);
            getFacultyFullname.RemoveAll(x => gridFftyList.Any(y => y.FacultyId == x.Id));
            var fundingFacultiesList = getFacultyFullname.Select(f => new KeyValuePair<Guid?, string>(f.Id, f.FullName));
            return new JsonCamelCaseResult(fundingFacultiesList, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// MVN - THIS post back from the UI when the grid row selection is selected
        /// PURPOSE:  Essentially, this code helps maintaining the two lists: Grid and Dropdown list, to dynamically update the list  
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult AddBackToDropDownListFacultySupport(FacultySupportPostModel model)
        {
            var currFunding = trainingGrantRepository.GetFundingbyId(model.FundingId);
            var facultyDrpdwnList = commonRepository.GetFacultyList().ToList();
            var roles = _context.FundingRoles.ToList();
            var facPersonId = commonRepository.GetPersonIdForFacultyId(model.FacultyId);
            var facultyGridList = currFunding.FundingFaculties.Where(f => f.IsDeleted == false);
            facultyDrpdwnList.RemoveAll(x => facultyGridList.Any(y => y.FacultyId == x.Id));
            bool isSelected = model.Id.Equals(Guid.Empty) || !_context.Set<FundingFaculty>()
                      .Any(ffty => ffty.Id == model.Id);

            var newFFtySupport = isSelected
                        ? new FundingFaculty()
                        : _context.Set<FundingFaculty>()
                        .Single(ffty => ffty.Id == model.Id);

            var trainGrant = _context.Set<TrainingGrant>()
               .Include(tg => tg.Funding)
               .Single(tg => tg.FundingId == model.FundingId);

            var currTrainingGrant = trainingGrantRepository.getTrainingGrantById(trainGrant.Id);
            /* ------------------------------------------------------------------------------------------------------ */
            var addFtySupportBktoDDL = new FacultySupportViewModel
            {
                Id = newFFtySupport.Id,
                FundingId = newFFtySupport.FundingId,
                FacultyId = newFFtySupport.FacultyId,
                FacultyFullName = model.FacultyFullName,
                FacultyRoleId = newFFtySupport.PrimaryRoleId,
                FacultyRole = (newFFtySupport.FundingRole_PrimaryRoleId != null) ? newFFtySupport.FundingRole_PrimaryRoleId.Name : string.Empty,
                
                FacultySecondaryRoleId = newFFtySupport.SecondaryRoleId,
                FacultySecondaryRole = (newFFtySupport.FundingRole_SecondaryRoleId != null) ? newFFtySupport.FundingRole_SecondaryRoleId.Name : string.Empty,

                SelectedResearchInterests = commonRepository.GetTrainingGrantResearchInterestList(currTrainingGrant.Id, newFFtySupport.FacultyId)
                    .Select(ri => ri.ResearchInterestId),
                SelectedFacultyAllInterests = commonRepository.getFacultyResearchInterestsByFacultyId(newFFtySupport.FacultyId)
                    .Select(ri => new InterestInfo { ResearchId = ri.Id, Name = ri.ResearchInterest })
                    .OrderBy(ii => ii.Name),

                FundingFacultiesList = facultyDrpdwnList.Select(f => new KeyValuePair<Guid?, string>(f.Id, f.FullName))
            };

            return new JsonCamelCaseResult(addFtySupportBktoDDL, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// REMOVE Row from the Grid
        /// Check all participating faculty to see if there is at least one PD associated with TrainingGrant
        /// Pass in TrainingGrant ID from UI
        /// This method utilizing the JavaScript notification with Toastr API
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RemoveFtySupport(Guid id)
        {
            var fundFaculty = _context.Set<FundingFaculty>().Find(id);
            ///<summary>
            /// Date change => 9/1/2015
            /// MVN - task 9919 re-order logic when removing PI from Participating Faculty
            /// LOGIC => if removing a faculty member from the grid, 1st check that they are not a PI 
            ///
            // TODO HERE - major logic change to validate PrimaryRoleId == 1

            if (fundFaculty.PrimaryRoleId == 6)
            {
                var safeToDelete = _context.FundingFaculties
                        .Where(f => f.PrimaryRoleId == 6 && f.FundingId == fundFaculty.FundingId && f.IsDeleted == false)
                        .Count() > 1 ? true : false;

                if (safeToDelete)
                {
                    fundFaculty.IsDeleted = true;
                }
                else
                {
                    //Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ////THIS => essentially return back to the UI, utilizing JavaScript libraries toastr.error(data.responseText) 
                    //return Content("Must have at least one PD associated with this training grant", MediaTypeNames.Text.Plain);
                    return Content("false");
                }
            }
            else
            {
                fundFaculty.IsDeleted = true;
            }
            _context.SaveChanges();
            //return Json(id);
            return Content("true");
        }


        [HttpPost]
        public ActionResult RefreshFacultyMentees(TrainingGrantPostModel model)
        {
            var trainingGrant = _context.Set<TrainingGrant>().Single(tg => tg.Id == model.Id);

            var mentees = new TrainingGrantMenteesViewModel
            {
                TrainingGrantId = trainingGrant.Id,
                FacultyMentees = commonRepository.GetSearch_VwTrainingGrantMentees(trainingGrant)
                    .Select(mentee => new TrainingGrantMenteeViewModel
                    {
                        TrainingGrantId = trainingGrant.Id,
                        MenteeId = mentee.MenteeId,
                        MenteePersonId = mentee.MenteePersonId,
                        MenteeFullName = mentee.MenteeFullName,
                        DoctoralLevel = mentee.MenteeType,
                        YearStarted = mentee.TrainingPeriodYearStarted,
                        YearEnded = mentee.TrainingPeriodYearEnded,
                        MentorFullName = mentee.FacultyFullName
                    })
            };

            return new JsonCamelCaseResult(mentees, JsonRequestBehavior.AllowGet);
        }

        
       

        [HttpPost]
        public ActionResult RefreshSupportGrants(TrainingGrantPostModel model)
        {
            var trainingGrant = _context.Set<TrainingGrant>().Single(tg => tg.Id == model.Id);

            // Get all pending/active grants associated with all participating faculty on this training grant
            var associatedTGrants = commonRepository.GetOtherGrantsAssociatedToFundingId(trainingGrant.FundingId, true, false);

            var otherGrantsViewModel = new InstitutionTrainingGrants
            {
                TrainingGrantId = trainingGrant.Id,
                // Grab all of the associated grant records that have a PD
                FundingGridView = commonRepository.GetSearch_VwTrainingGrantFaculty()
                    .Where(ff => associatedTGrants.Contains(ff.TrainingGrantId) && ff.FacultyPrimaryRoleId == 6)
                    .Select(ff => new InstitutionTrainingGrant
                    {
                        TrainingGrantId = ff.TrainingGrantId,
                        PDFullName = ff.FacultyFullName,
                        SponsorName = ff.SponsorName,
                        Title = ff.Title,
                        TrainingGrantStatus = ff.TrainingGrantStatus
                    })
                    .OrderBy(ff => ff.PDFullName)
            };

            return new JsonCamelCaseResult(otherGrantsViewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RefreshSupportFunds(TrainingGrantPostModel model)
        {
            var trainingGrant = _context.Set<TrainingGrant>().Single(tg => tg.Id == model.Id);

            // Get all pending/active funding associated with all participating faculty on this training grant
            var associatedFunds = commonRepository.GetOtherFundingAssociatedToFundingId(trainingGrant.FundingId, true, false);

            var otherFundingViewModel = new OtherFundingsInfo
            {
                TrainingGrantId = trainingGrant.Id,
                // Grab all of the associated funding records that have a PI
                OtherGridView = commonRepository.GetSearch_VwFundingFaculty()
                    .Where(ff => associatedFunds.Contains(ff.Id) && ff.FacultyRoleId == 1)
                    .Select(ff => new OtherFundingInfo
                    {
                        Id = ff.Id,
                        PIFullName = ff.FacultyFullName,
                        SponsorName = ff.SponsorName,
                        Title = ff.Title,
                        FundingStatus = ff.FundingStatus
                    })
                    .OrderBy(ff => ff.PIFullName)
            };

            return new JsonCamelCaseResult(otherFundingViewModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// MVN - This method alter the TrainingGrantTrainee table
        /// </summary>
        /// <param name="saveUpdateTraineeDetailedInfoModel"></param>
        /// <returns></returns>
        public ActionResult SaveUpdateTraineeDetailedInfo(TraineeDetailedPostModel saveUpdateTraineeDetailedInfoModel)
        {
            bool isNew = saveUpdateTraineeDetailedInfoModel.TrainingGrantTraineeId.Equals(Guid.Empty) || !_context.Set<TrainingGrantTrainee>()
                       .Any(t => t.Id == saveUpdateTraineeDetailedInfoModel.TrainingGrantTraineeId);

            var newTrainee = isNew
                        ? new TrainingGrantTrainee() : _context.Set<TrainingGrantTrainee>().Find(saveUpdateTraineeDetailedInfoModel.TrainingGrantTraineeId);

            newTrainee.TrainingGrantId = saveUpdateTraineeDetailedInfoModel.TrainingGrantId;
            newTrainee.MenteeId = saveUpdateTraineeDetailedInfoModel.MenteeId;
            newTrainee.DateStarted = saveUpdateTraineeDetailedInfoModel.DateStarted;
            newTrainee.DateEnded = saveUpdateTraineeDetailedInfoModel.DateEnded;                  
            newTrainee.TrainingPeriodId = saveUpdateTraineeDetailedInfoModel.TrainingPeriodId;
            newTrainee.DoctoralLevelId = saveUpdateTraineeDetailedInfoModel.DoctoralLevelId;
            newTrainee.PredocTraineeStatusId = saveUpdateTraineeDetailedInfoModel.PredocTraineeStatusId;
            newTrainee.PostdocTraineeStatusId = saveUpdateTraineeDetailedInfoModel.PostdocTraineeStatusId;
            newTrainee.PredocReasonForLeaving = saveUpdateTraineeDetailedInfoModel.PredocReasonForLeaving;
            newTrainee.PostdocReasonForLeaving = saveUpdateTraineeDetailedInfoModel.PostdocReasonForLeaving;

            /// Tracking user activity 
            /// added 9/10/2015
            newTrainee.DateLastUpdated = DateTime.Now;
            newTrainee.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);

            if (isNew)
            {
                _context.Set<TrainingGrantTrainee>().Add(newTrainee);
            }
            _context.SaveChanges();

            var mentee = _context.Set<Mentee>().Single(m => m.Id == newTrainee.MenteeId);


            var traineeDetailedGrid = new TraineeDetailedInformationViewModel
            {
                TrainingGrantTraineeId = newTrainee.Id,
                TrainingGrantId = saveUpdateTraineeDetailedInfoModel.TrainingGrantId,
                MenteeId = saveUpdateTraineeDetailedInfoModel.MenteeId,

                MenteePersonId = mentee.PersonId,
                IsTrainingGrantEligible = mentee.IsTrainingGrantEligible,

                DoctoralLevelId = newTrainee.DoctoralLevelId,
                PredocTraineeStatusId = saveUpdateTraineeDetailedInfoModel.PredocTraineeStatusId,
                PostdocTraineeStatusId = saveUpdateTraineeDetailedInfoModel.PostdocTraineeStatusId,
                PredocReasonForLeaving = saveUpdateTraineeDetailedInfoModel.PredocReasonForLeaving,
                PostdocReasonForLeaving = saveUpdateTraineeDetailedInfoModel.PostdocReasonForLeaving,
                MenteeFullName = commonRepository.GetPersonFullName(mentee.PersonId),
                InstitutionAssociationId = saveUpdateTraineeDetailedInfoModel.InstitutionAssociationId,
                InstitutionAssociation = saveUpdateTraineeDetailedInfoModel.InstitutionAssociation,

                TraineeStatus = (newTrainee.DoctoralLevelId == 1)
                                ? (newTrainee.TraineeStatu_PredocTraineeStatusId != null
                                ? newTrainee.TraineeStatu_PredocTraineeStatusId.Name : commonRepository.GetTraineeStatus(saveUpdateTraineeDetailedInfoModel.PredocTraineeStatusId))
                                : (newTrainee.TraineeStatu_PredocTraineeStatusId != null
                                ? newTrainee.TraineeStatu_PostdocTraineeStatusId.Name : commonRepository.GetTraineeStatus(saveUpdateTraineeDetailedInfoModel.PostdocTraineeStatusId)),


                DateStarted = saveUpdateTraineeDetailedInfoModel.DateStarted,
                DateEnded = saveUpdateTraineeDetailedInfoModel.DateEnded,

                /* ******* Research Project Title from TrainingPeriod table - set back to the view Model and essentially update the grid column research experience */
                ResearchProjectTitle = saveUpdateTraineeDetailedInfoModel.ResearchProjectTitle,
                //THIS =>SET the TrainingPeriod to the Trainee table 
                SelectedTrainingPeriodId = newTrainee.TrainingPeriodId.HasValue ? newTrainee.TrainingPeriodId.Value : Guid.Empty,

                StrDateLastUpdated = (newTrainee.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, newTrainee.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(newTrainee.LastUpdatedBy),
                
                // ******************** TRAINING PERIOD GRID ***********************/          
                //MenteeTrainingPeriods = mentee.Person.TrainingPeriods.Where(tp => tp.Id == newTrainee.TrainingPeriodId)
                MenteeTrainingPeriods = mentee.Person.TrainingPeriods
                    .Select(tp => new MenteeTrainingPeriodViewModel
                    {
                        Id = tp.Id,                                     // Training Period Id
                        PersonId = tp.PersonId,
                        InstitutionId = tp.InstitutionId,               // Institution in Training Period table 

                        /* *************************  Associated Institution in Mentee Table ******************************* */
                        InstitutionAssociationId = newTrainee.Mentee.InstitutionAssociationId,
                        InstitutionAssociation = newTrainee.Mentee.InstitutionAssociation != null ? newTrainee.Mentee.InstitutionAssociation.Name : string.Empty,

                        /* ************************ Having issue with the institution is not refresh in the trainee grid ********************* */
                        //InstitutionAssociationId = tp.InstitutionId,
                        //InstitutionName = tp.InstitutionId.HasValue ? tp.Institution.Name : commonRepository.GetDefaultInstitution().Name,

                        DoctoralLevelId = tp.DoctoralLevel.Id,
                        // THIS => set the research project title from the trainingPeriod table 
                        ResearchProjectTitle = tp.ResearchProjectTitle != null ? tp.ResearchProjectTitle : string.Empty,
                        DoctoralLevel = tp.DoctoralLevel != null ? tp.DoctoralLevel.LevelName : string.Empty,
                        AcademicDegreeId = tp.AcademicDegreeId,
                        // Molley added new FK for Degree and DegreeSought name 7/13/2015
                        AcademicDegreeName = tp.AcademicDegreeId.HasValue ? tp.AcademicDegree_AcademicDegreeId.Name : string.Empty,
                        YearStarted = tp.YearStarted,
                        YearEnded = tp.YearEnded,
                        SelectedTrainingPeriodId = tp.Id,

                        StrDateLastUpdated = (tp.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, tp.DateLastUpdated) : string.Empty,
                        StrLastUpdatedBy = commonRepository.GetLoginInfo(tp.LastUpdatedBy)

                    }),

            };
            ///* the most current codes 9/30/2015 ******************************* */
            //var traineeDetailedGrid = new TraineeDetailedInformationViewModel
            //{
            //    TrainingGrantTraineeId = newTrainee.Id,
            //    TrainingGrantId = newTrainee.TrainingGrantId,
            //    MenteeId = newTrainee.MenteeId,

            //    MenteePersonId = mentee.PersonId,
            //    IsTrainingGrantEligible = mentee.IsTrainingGrantEligible,

            //    DoctoralLevelId = newTrainee.DoctoralLevelId,
            //    PredocTraineeStatusId = newTrainee.PredocTraineeStatusId,
            //    PostdocTraineeStatusId = newTrainee.PostdocTraineeStatusId,
            //    PredocReasonForLeaving = newTrainee.PredocReasonForLeaving,
            //    PostdocReasonForLeaving = newTrainee.PostdocReasonForLeaving,
            //    MenteeFullName = commonRepository.GetPersonFullName(mentee.PersonId),
            //    InstitutionAssociationId = newTrainee.Mentee.InstitutionAssociationId,
            //    InstitutionAssociation = newTrainee.Mentee.InstitutionAssociation != null ? newTrainee.Mentee.InstitutionAssociation.Name : string.Empty,

            //    TraineeStatus = (newTrainee.DoctoralLevelId == 1)
            //                    ? (newTrainee.TraineeStatu_PredocTraineeStatusId != null
            //                    ? newTrainee.TraineeStatu_PredocTraineeStatusId.Name : commonRepository.GetTraineeStatus(newTrainee.PredocTraineeStatusId))
            //                    : (newTrainee.TraineeStatu_PredocTraineeStatusId != null
            //                    ? newTrainee.TraineeStatu_PostdocTraineeStatusId.Name : commonRepository.GetTraineeStatus(newTrainee.PostdocTraineeStatusId)),


            //    DateStarted = newTrainee.DateStarted,
            //    DateEnded = newTrainee.DateEnded,

            //    /* ******* Research Project Title from TrainingPeriod table - set back to the view Model and essentially update the grid column research experience */
            //    ResearchProjectTitle = newTrainee.TrainingPeriod != null ? newTrainee.TrainingPeriod.ResearchProjectTitle : string.Empty,
            //    //THIS =>SET the TrainingPeriod to the Trainee table 
            //    SelectedTrainingPeriodId = newTrainee.TrainingPeriodId.HasValue ? newTrainee.TrainingPeriodId.Value : Guid.Empty,

            //    StrDateLastUpdated = (newTrainee.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, newTrainee.DateLastUpdated) : string.Empty,
            //    StrLastUpdatedBy = commonRepository.GetLoginInfo(newTrainee.LastUpdatedBy),

            //    /// MVN - added 9/30/2015
            //    DoctoralLevel = newTrainee.DoctoralLevel.LevelName,
                
            //    // ******************** TRAINING PERIOD GRID ***********************/          
            //    //MenteeTrainingPeriods = mentee.Person.TrainingPeriods.Where(tp => tp.Id == newTrainee.TrainingPeriodId)
            //    //MenteeTrainingPeriods = mentee.Person.TrainingPeriods
            //    MenteeTrainingPeriods = newTrainee.Mentee.Person.TrainingPeriods
            //        .Select(tp => new MenteeTrainingPeriodViewModel
            //        {
            //            Id = tp.Id,
            //            PersonId = tp.PersonId,
            //            DoctoralLevelId = tp.DoctoralLevel.Id,
            //            DoctoralLevel = tp.DoctoralLevel.LevelName,
            //            InstitutionId = tp.InstitutionId,
            //            InstitutionName = tp.InstitutionId.HasValue ? tp.Institution.Name : commonRepository.GetDefaultInstitution().Name,

            //            AcademicDegreeId = tp.AcademicDegreeId,
            //            AcademicDegreeName = tp.AcademicDegreeId.HasValue ? tp.AcademicDegree_AcademicDegreeId.Name : string.Empty,
            //            YearStarted = tp.YearStarted,
            //            YearEnded = tp.YearEnded,

            //            // working
            //            TraineeStatus = (newTrainee.DoctoralLevelId == 1)
            //                  ? (newTrainee.TraineeStatu_PredocTraineeStatusId != null
            //                  ? newTrainee.TraineeStatu_PredocTraineeStatusId.Name : commonRepository.GetTraineeStatus(newTrainee.PredocTraineeStatusId))
            //                  : (newTrainee.TraineeStatu_PredocTraineeStatusId != null
            //                  ? newTrainee.TraineeStatu_PostdocTraineeStatusId.Name : commonRepository.GetTraineeStatus(newTrainee.PostdocTraineeStatusId)),

            //            PredocReasonForLeaving = newTrainee.PredocReasonForLeaving,
            //            PostdocReasonForLeaving = newTrainee.PostdocReasonForLeaving,
            //            /// working end here 
            //            StrDateLastUpdated = (newTrainee.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, newTrainee.DateLastUpdated) : string.Empty,
            //            StrLastUpdatedBy = commonRepository.GetLoginInfo(newTrainee.LastUpdatedBy),
            //        }),

            //};
            /* **************************** the most current codes end here *********************************** */
            ////////var currTrainingGrant = trainingGrantRepository.getTrainingGrantById(newTrainee.TrainingGrantId);
            ////////var getMenteeFullNameList = commonRepository.GetFacultyMenteesList(currTrainingGrant).ToList();
            ////////var traineeDetailedGrid = new TraineesDetailedInformationViewModel
            ////////{
            ////////    TrainingGrantId = currTrainingGrant.Id,
            ////////    // ******************** DropDown uses the Mentees of Participating Faculty list instead ********************/
            ////////    //MenteeListDropdown = menteeList.Select(m => new KeyValuePair<Guid?, string>(m.Id, m.FullName)),      // to be removed =>use for testing only
            ////////    MenteeListDropdown = getMenteeFullNameList.Select(m => new KeyValuePair<Guid?, string>(m.MenteeId, m.MenteeFullName)),

            ////////    // ******************** TRAINEE GRID ***********************/
            ////////    TraineeDetailedInformationView = currTrainingGrant.TrainingGrantTrainees
            ////////                                  .Select(trainee => new TraineeDetailedInformationViewModel
            ////////                                  {
            ////////                                      TrainingGrantTraineeId = trainee.Id,
            ////////                                      TrainingGrantId = newTrainee.Id,
            ////////                                      MenteeId = trainee.MenteeId,
            ////////                                      MenteePersonId = trainee.Mentee.PersonId,
            ////////                                      IsTrainingGrantEligible = trainee.Mentee.IsTrainingGrantEligible,
            ////////                                      // ******************** Program Status radio button **********************/
            ////////                                      DoctoralLevelId = trainee.DoctoralLevelId,
            ////////                                      PredocTraineeStatusId = trainee.PredocTraineeStatusId,
            ////////                                      PostdocTraineeStatusId = trainee.PostdocTraineeStatusId,
            ////////                                      PredocReasonForLeaving = trainee.PredocReasonForLeaving,
            ////////                                      PostdocReasonForLeaving = trainee.PostdocReasonForLeaving,
            ////////                                      MenteeFullName = commonRepository.GetPersonFullName(trainee.Mentee.PersonId),
            ////////                                      InstitutionAssociationId = trainee.Mentee.InstitutionAssociationId,
            ////////                                      InstitutionAssociation = trainee.Mentee.InstitutionAssociation != null ? trainee.Mentee.InstitutionAssociation.Name : string.Empty,
            ////////                                      TraineeStatus = (trainee.DoctoralLevelId == 1) ? (trainee.PredocTraineeStatusId.HasValue
            ////////                                                                                     ? trainee.TraineeStatu_PredocTraineeStatusId.Name : string.Empty)
            ////////                                                                                     : (trainee.PostdocTraineeStatusId.HasValue
            ////////                                                                                     ? trainee.TraineeStatu_PostdocTraineeStatusId.Name : string.Empty),
            ////////                                      DateStarted = trainee.DateStarted,
            ////////                                      DateEnded = trainee.DateEnded,


            ////////                                      ResearchProjectTitle = trainee.TrainingPeriod != null ? trainee.TrainingPeriod.ResearchProjectTitle : string.Empty,
            ////////                                      SelectedTrainingPeriodId = trainee.TrainingPeriodId.HasValue ? trainee.TrainingPeriodId.Value : Guid.Empty,

            ////////                                      //Tracking form altered with date and user
            ////////                                      DateLastUpdated = trainee.DateLastUpdated,
            ////////                                      LastUpdatedBy = trainee.LastUpdatedBy,
            ////////                                      StrDateLastUpdated = (trainee.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, trainee.DateLastUpdated) : string.Empty,
            ////////                                      StrLastUpdatedBy = commonRepository.GetLoginInfo(trainee.LastUpdatedBy),

            ////////                                      // ******************** TRAINING PERIOD GRID ***********************/
            ////////                                      MenteeTrainingPeriods = trainee.Mentee.Person.TrainingPeriods
            ////////                                          .Select(tp => new MenteeTrainingPeriodViewModel
            ////////                                          {
            ////////                                              Id = tp.Id,                           //Set TrainingPeriod Id                   
            ////////                                              PersonId = tp.PersonId,
            ////////                                              InstitutionId = tp.InstitutionId,
            ////////                                              InstitutionName = tp.InstitutionId.HasValue ? tp.Institution.Name : commonRepository.GetDefaultInstitution().Name,
            ////////                                              DoctoralLevelId = tp.DoctoralLevel.Id,
            ////////                                              DoctoralLevel = tp.DoctoralLevel.LevelName,
            ////////                                              AcademicDegreeId = tp.AcademicDegreeId,
            ////////                                              AcademicDegreeName = tp.AcademicDegreeId.HasValue ? tp.AcademicDegree_AcademicDegreeId.Name : string.Empty,
            ////////                                              YearStarted = tp.YearStarted,
            ////////                                              YearEnded = tp.YearEnded,
            ////////                                              StrDateLastUpdated = (tp.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, tp.DateLastUpdated) : string.Empty,
            ////////                                              StrLastUpdatedBy = commonRepository.GetLoginInfo(tp.LastUpdatedBy),
            ////////                                          })
            ////////                                  })
            ////////};
           
            //_context.SaveChanges();
            //THIS => Essentially refresh the UI Grid
            return new JsonCamelCaseResult(traineeDetailedGrid, JsonRequestBehavior.AllowGet);
         }

        /// <summary>
        /// MVN - onChangeSelectMentee => the Mentee DropdownList section
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        //public ActionResult GetMenteeTrainingPeriods(TrainingGrantPostModel model)
        public ActionResult GetMenteeTrainingPeriods(TraineeDetailedPostModel model)
        {
            var mentee = _context.Set<Mentee>().Single(m => m.Id == model.MenteeId);
            var menteeTrainingPeriod = new MenteeTrainingPeriodsViewModel
            {
                Id = mentee.Id,                
                IsTrainingGrantEligible = mentee.IsTrainingGrantEligible,
                //MenteeTrainingPeriods = mentee.Person.TrainingPeriods.Where(tp => tp.IsDeleted == false && tp.PersonId == mentee.PersonId)
                MenteeTrainingPeriods = mentee.Person.TrainingPeriods
                                        .Select(tp => new MenteeTrainingPeriodViewModel
                                        {
                                            Id = tp.Id,                           // TrainingPeriod to Id =>ManteeTrainingPeriods
                                            MenteeId = mentee.Id,
                                            PersonId = tp.PersonId,
                                            InstitutionId = tp.InstitutionId,
                                            InstitutionAssociationId = mentee.InstitutionAssociationId,
                                            InstitutionAssociation = mentee.InstitutionAssociation != null ? mentee.InstitutionAssociation.Name : string.Empty,
                                            ResearchProjectTitle = tp.ResearchProjectTitle != null ? tp.ResearchProjectTitle : string.Empty,
                                            InstitutionName = tp.InstitutionId.HasValue ? tp.Institution.Name : commonRepository.GetDefaultInstitution().Name,
                                            DoctoralLevelId = tp.DoctoralLevel.Id,
                                            DoctoralLevel = tp.DoctoralLevel != null ? tp.DoctoralLevel.LevelName : string.Empty,
                                            AcademicDegreeId = tp.AcademicDegreeId,
                                            // Molley add new FK for Degree and DegreeSought name 
                                            //AcademicDegreeName = tp.AcademicDegreeId.HasValue ? tp.AcademicDegree.Name : string.Empty,
                                            AcademicDegreeName = tp.AcademicDegreeId.HasValue ? tp.AcademicDegree_AcademicDegreeId.Name :string.Empty,
                                            YearStarted = tp.YearStarted,
                                            YearEnded = tp.YearEnded                    
                                        })
            };
            // Refresh the Associated Training Period Grid => provide the grid selection ability in the action,
            // where the selection will then expand the row data properties
            return new JsonCamelCaseResult(menteeTrainingPeriod, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveCostInformation(CostInformationPostModel saveUpdateCostInfoModel)
        {
            bool isNew = !saveUpdateCostInfoModel.Id.HasValue || !_context.Set<FundingDirectCost>()
                       .Any(fdcst => fdcst.Id == saveUpdateCostInfoModel.Id);        //Compare the Id in PostModel to FundingDirectCost

            var newFdCstInfo = isNew
                        ? new FundingDirectCost() : _context.Set<FundingDirectCost>().Find(saveUpdateCostInfoModel.Id);
            
            newFdCstInfo.FundingId = saveUpdateCostInfoModel.FundingId;
            newFdCstInfo.DateStarted = saveUpdateCostInfoModel.DateStarted;
            newFdCstInfo.DateEnded = saveUpdateCostInfoModel.DateEnded;
            newFdCstInfo.CurrentYearDirectCosts = saveUpdateCostInfoModel.CurrentYearDirectCosts;
            newFdCstInfo.TotalDirectCosts = saveUpdateCostInfoModel.TotalDirectCosts;

            DateTime setUpdateDate = DateTime.Now;
            newFdCstInfo.DateLastUpdated = DateTime.Now;
            newFdCstInfo.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
            

            /// MVN - This handle Update first check BudgetPeriodStatusId sent from the UI
            if (saveUpdateCostInfoModel.BudgetPeriodStatusId == 1){
                var FundingDC = commonRepository.GetFDDCostsList(newFdCstInfo.FundingId);
                foreach (var result in FundingDC){
                    /// Then from the server, check and count the budgetPeriodStatusId that is 1
                    var countResult = commonRepository.GetFDDCostsList(newFdCstInfo.FundingId)
                            .Where(b => b.BudgetPeriodStatusId == 1).Count() > 0 ? true : false;                 
                        if (countResult == true){
                            result.BudgetPeriodStatusId = 2;
                        }else{
                            //set to the value that was sent by the UI
                            newFdCstInfo.BudgetPeriodStatusId = saveUpdateCostInfoModel.BudgetPeriodStatusId;
                        }
                }
                newFdCstInfo.BudgetPeriodStatusId = saveUpdateCostInfoModel.BudgetPeriodStatusId;
            }
           

            // MVN - This code handles if there is nothing selected from the UI dropdownlist
            if (saveUpdateCostInfoModel.BudgetPeriodStatusId == 0)
            {
                saveUpdateCostInfoModel.BudgetPeriodStatusId = newFdCstInfo.BudgetPeriodStatusId;
            }

            /// MVN - This will handle Add New Direct Costs
            if (isNew)
            {              
                newFdCstInfo.BudgetPeriodStatusId = saveUpdateCostInfoModel.BudgetPeriodStatusId;
                _context.Set<FundingDirectCost>().Add(newFdCstInfo);
            }
            _context.SaveChanges();
          

            var budgetPeriodStatus = trainingGrantRepository.GetBudgetPeriodStatus().ToList();
            var currFunding = _context.Fundings.FirstOrDefault(f => f.Id == saveUpdateCostInfoModel.FundingId);
           
            /// MVN Grid Refresh....
            var costInfoView = new CostsInformationViewModel{
                FundingId = saveUpdateCostInfoModel.FundingId,
                BudgetPeriodStatuses = budgetPeriodStatus.Select(f => new KeyValuePair<int, string>(f.Id, f.Name)),
                CostInformationGridView = currFunding.FundingDirectCosts.Where(c => c.IsDeleted == false)
                                                                      .Select(cost => new CostInformationViewModel{
                                                                          Id = cost.Id,
                                                                          FundingId = cost.FundingId,
                                                                          DateStarted = cost.DateStarted,
                                                                          DateStartedText = String.Format(_DATEFORMAT, cost.DateStarted),
                                                                          DateEnded = cost.DateEnded,
                                                                          DateEndedText = String.Format(_DATEFORMAT, cost.DateEnded),
                                                                          CurrentYearDirectCosts = cost.CurrentYearDirectCosts,
                                                                          BudgetPeriodStatusId = cost.BudgetPeriodStatusId,
                                                                          BudgetPeriodStatus = (cost.BudgetPeriodStatu != null) ? cost.BudgetPeriodStatu.Name : string.Empty,
                                                                          
                                                                          
                                                                          TotalDirectCosts = cost.TotalDirectCosts,                                                                   
                                                                          StrDateLastUpdated = (cost.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, cost.DateLastUpdated) : string.Empty,
                                                                          StrLastUpdatedBy = commonRepository.GetLoginInfo(cost.LastUpdatedBy)
                                                                      })

            };

            //THIS => Essentially refresh the UI Grid
            return new JsonCamelCaseResult(costInfoView, JsonRequestBehavior.AllowGet);
        }
       

        [HttpPost]
        public ActionResult UpdateGeneralInformation(TrainingGrantPostModel updateTrainingGrantModel)
        {
            var trainGrant = _context.Set<TrainingGrant>()
                .Include(tg => tg.Funding)
                .Single(tg => tg.Id == updateTrainingGrantModel.Id);

            trainGrant.FundingId = updateTrainingGrantModel.FundingId;
            trainGrant.Funding.Title = updateTrainingGrantModel.Title;

            Institution sponsorName = null;
            if (_context.Set<Institution>().Any(ac => ac.Name == updateTrainingGrantModel.SponsorName))
            {
                sponsorName = _context.Set<Institution>().Single(ac => ac.Name == updateTrainingGrantModel.SponsorName);
                trainGrant.Funding.SponsorId = sponsorName.Id;
            }

            trainGrant.TrainingGrantStatusId = updateTrainingGrantModel.TrainingGrantStatusId;
            trainGrant.Funding.DateProjectedStart = updateTrainingGrantModel.DateProjectedStart;
            trainGrant.Funding.DateProjectedEnd = updateTrainingGrantModel.DateProjectedEnd;
            trainGrant.Funding.GrtNumber = updateTrainingGrantModel.GrtNumber;
            trainGrant.Funding.SponsorReferenceNumber = updateTrainingGrantModel.SponsorReferenceNumber;

            //PROGRAMS
            var savedPrograms = trainGrant.Programs.ToList();
            //var isProgramsUpdated = false;

            // ADD newly selected Programs to database
            if (updateTrainingGrantModel.TrainingGrantPrograms != null)
            {
                foreach (var selectedProgramId in updateTrainingGrantModel.TrainingGrantPrograms)
                {
                    if (trainGrant.Programs.All(tgp => tgp.Id != selectedProgramId))
                    {
                        var addProgram = _context.Set<Program>().Find(selectedProgramId);
                        trainGrant.Programs.Add(addProgram);
                        //isProgramsUpdated = true;
                    }
                }
            }

            // DELETE previously-saved, just removed Programs from database
            Program remProgram = null;

            foreach (var savedProgram in savedPrograms)
            {
                if (updateTrainingGrantModel.TrainingGrantPrograms != null)
                {
                    if (!updateTrainingGrantModel.TrainingGrantPrograms.Contains(savedProgram.Id))
                    {
                        remProgram = _context.Set<Program>().Find(savedProgram.Id);                        
                    }
                } else {
                    remProgram = _context.Set<Program>().Find(savedProgram.Id);    
                }

                if (remProgram != null)
                {
                    trainGrant.Programs.Remove(remProgram);
                    remProgram = null;
                    //isProgramsUpdated = true;
                }
            }

            //DEPARTMENTS
            var savedDepartments = trainGrant.Organizations.ToList();
            //var isDeptsUpdated = false;

            // ADD newly selected Depts to database
            if (updateTrainingGrantModel.TrainingGrantDepartments != null)
            {
                foreach (var selectedDepartmentId in updateTrainingGrantModel.TrainingGrantDepartments)
                {
                    if (trainGrant.Organizations.All(tgd => tgd.Id != selectedDepartmentId))
                    {
                        var addDept = _context.Set<Organization>().Find(selectedDepartmentId);
                        trainGrant.Organizations.Add(addDept);
                        //isDeptsUpdated = true;
                    }
                }
            }

            // DELETE previously-saved, just removed Depts from database
            Organization remDepartment = null;

            foreach (var savedDepartment in savedDepartments)
            {
                if (updateTrainingGrantModel.TrainingGrantDepartments != null) {
                    if (!updateTrainingGrantModel.TrainingGrantDepartments.Contains(savedDepartment.Id))
                    {
                        remDepartment = _context.Set<Organization>().Find(savedDepartment.Id);
                    }
                } else {
                    remDepartment = _context.Set<Organization>().Find(savedDepartment.Id);
                }

                if (remDepartment != null)
                {
                    trainGrant.Organizations.Remove(remDepartment);
                    remDepartment = null;
                    //isDeptsUpdated = true;
                }
            }


            if (updateTrainingGrantModel.DoctoralLevelId > 0)
            {
                trainGrant.DoctoralLevelId = updateTrainingGrantModel.DoctoralLevelId;
            }

            trainGrant.Funding.IsRenewal = updateTrainingGrantModel.IsRenewal;
            trainGrant.NumPredocPositionsRequested = updateTrainingGrantModel.NumPredocPositionsRequested;
            trainGrant.NumPostdocPositionsRequested = updateTrainingGrantModel.NumPostdocPositionsRequested;

            //commonRepository.AddOrUpdateFundingRd(trainGrant, (ClaimsPrincipal)User);

            // Track last updated date and user
            trainGrant.DateLastUpdated = DateTime.Now;
            trainGrant.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);

            _context.SaveChanges();
            /// MVN - TODO remove these codes 9/18/2015.  These should be already in Edit 
            /// 
            //////// For use in hiding/displaying appropriate Training Grant sections
            //////var currStatus = trainGrant.TrainingGrantStatusId;

            //////ViewBag.IsNew = (trainGrant.Funding.DateProjectedStart.HasValue == false) ? true : false;
            //////// Award Info is hidden if Status IN Planning (1), Pending (2), and Not Funded (6)
            //////ViewBag.IsAwardInfoHidden = ((currStatus < 3) || (currStatus == 6)) ? true : false;
            //////// Cost Info is hidden if Status IN Planning (1), Pending (2), and Not Funded (6)
            //////ViewBag.IsCostInfoHidden = ((currStatus < 3) || (currStatus == 6)) ? true : false;
            //////ViewBag.IsPreviousHidden = !trainGrant.Funding.IsRenewal;
            /// *****************************************MVN ******************************************************
            return Json(true);

        }


        [HttpPost]
        public ActionResult SavePreviousTrainingGrant(TrainingGrantPostModel model)
        {
            var trainingGrant = _context.Set<TrainingGrant>().Single(tg => tg.Id == model.Id);

            if (trainingGrant != null)
            {
                trainingGrant.PreviousTrainingGrantId = model.PreviousTrainingGrantId;

                // Track last updated date and user
                trainingGrant.DateLastUpdated = DateTime.Now;
                trainingGrant.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);

                _context.SaveChanges();
            }

            return Json(false);
        }

        

        [HttpPost]
        public ActionResult UpdateApplicantPoolAcademicYear(TrainingGrantPostModel model)
        {
            var trainingGrant = _context.Set<TrainingGrant>().Single(tg => tg.Id == model.Id);

            if (trainingGrant != null) {
                trainingGrant.ApplicantPoolAcademicYear = model.ApplicantPoolAcademicYear;

                // Track last updated date and user
                trainingGrant.DateLastUpdated = DateTime.Now;
                trainingGrant.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);

                _context.SaveChanges();
            }

            var applicants = new TrainingGrantApplicantsViewModel
            {
                TrainingGrantId = trainingGrant.Id,
                ApplicantPoolAcademicYear = trainingGrant.ApplicantPoolAcademicYear,
                TrainingGrantApplicants = commonRepository.GetSearch_VwTrainingGrantApplicants(trainingGrant)
                    .Where(app => app.YearEntered == trainingGrant.ApplicantPoolAcademicYear)
                    .Select(app => new TrainingGrantApplicantViewModel
                    {
                        TrainingGrantId = trainingGrant.Id,
                        ApplicantGuid = app.ApplicantGuid,
                        ApplicantId = app.ApplicantId,
                        DoctoralLevel = app.ApplicantType,
                        DepartmentId = app.ApplicantDeptId,
                        DepartmentName = app.ApplicantDept,
                        YearEntered = app.YearEntered,
                        ProgramListFlat = app.ApplicantPrograms
                    })

            };

            return new JsonCamelCaseResult(applicants, JsonRequestBehavior.AllowGet);
            //return Json(false);
        }


        [HttpPost]
        public ActionResult SetTrainingPeriodGridSelection(TrainingGrantPostModel model)
            {
            var newTrainingPeriod = _context.Set<TrainingGrantTrainee>().Single(tg => tg.TrainingPeriodId == model.TrainingPeriodId);

            if (newTrainingPeriod != null)
            {
                newTrainingPeriod.Id = model.Id;
                newTrainingPeriod.MenteeId = model.MenteeId;
                newTrainingPeriod.TrainingPeriodId = model.TrainingPeriodId;

                // Track last updated date and user
                newTrainingPeriod.DateLastUpdated = DateTime.Now;
                newTrainingPeriod.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);

                _context.SaveChanges();
            }
            return Json(false);
        }
        
        
        

        // GET: /TrainingGrant/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrainingGrant traininggrant = db.TrainingGrants.Find(id);
            if (traininggrant == null)
            {
                return HttpNotFound();
            }
            return View(traininggrant);
        }
        /// <summary>
        /// MVN - added 2/16/2015 
        /// THIS -> to collect the Training Grant award information
        /// </summary>
        /// <param name="saveUpdateAwardInfo"></param>
        /// <returns></returns>
        public ActionResult SaveUpdateAwardInfo(AwardInformationPostModel saveUpdateAwardInfo)
        {
            var saveUpdateTGInfo = _context.Set<TrainingGrant>()
                .FirstOrDefault(training => training.Id == saveUpdateAwardInfo.Id);
            saveUpdateTGInfo.Funding.Id = saveUpdateAwardInfo.FundingId;
            saveUpdateTGInfo.Funding.SponsorAwardNumber = saveUpdateAwardInfo.SponsorAwardNumber;
            saveUpdateTGInfo.Funding.DateStarted = saveUpdateAwardInfo.DateStarted;
            saveUpdateTGInfo.Funding.DateEnded = saveUpdateAwardInfo.DateEnded;

            // Track last updated date and user
            saveUpdateTGInfo.Funding.DateLastUpdated = DateTime.Now;
            saveUpdateTGInfo.Funding.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
            _context.SaveChanges();
            var awardInfoView = new AwardInformationViewModel
            {
                Id = saveUpdateTGInfo.Id,
                FundingId = saveUpdateTGInfo.Funding.Id,
                SponsorAwardNumber = saveUpdateTGInfo.Funding.SponsorAwardNumber,
                DateStarted = saveUpdateTGInfo.Funding.DateStarted,
                DateEnded = saveUpdateTGInfo.Funding.DateEnded,
                //StrDateLastUpdated = (funding != null) ? String.Format(_LastUpdateDATEFORMAT, funding.DateLastUpdated) : string.Empty,
                //LastUpdatedBy = (funding != null) ? funding.LastUpdatedBy : string.Empty,
                StrDateLastUpdated = (saveUpdateTGInfo != null) ? String.Format(_LastUpdateDATEFORMAT, saveUpdateTGInfo.DateLastUpdated) : string.Empty,
                //LastUpdatedBy = (mentee != null) ? mentee.LastUpdatedBy : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(saveUpdateTGInfo.LastUpdatedBy),
            };
            return new JsonCamelCaseResult(awardInfoView, JsonRequestBehavior.AllowGet);

            //return Json(true);
        }
        /// <summary>
        /// MVN - added 3/4/2015
        /// THIS -> allows save/update trainee information form data collection for Pre-doc  
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult SaveUpdateTraineeSummaryInfo(TraineeSummaryPostModel model)
        {
            int DOCTORALID = 1;
            bool isNew = model.TraineeSummaryId.Equals(Guid.Empty) || !_context.Set<TraineeSummaryInfo>()
                .Any(t => t.Id == model.TraineeSummaryId);
           
            var saveUpdateTGInfo = isNew
                ? new TraineeSummaryInfo() : _context.Set<TraineeSummaryInfo>().Find(model.TraineeSummaryId);
                                //? new TraineeSummaryInfo() : _context.Set<TraineeSummaryInfo>().Single(ts => ts.Id == model.TraineeSummaryId);
            //Add New
            saveUpdateTGInfo.TrainingGrantId = model.TrainingGrantId;

            saveUpdateTGInfo.DateStarted = model.DateStarted;
            saveUpdateTGInfo.DateEnded = model.DateEnded;

            saveUpdateTGInfo.PositionsAwarded = model.PositionsAwarded;
            saveUpdateTGInfo.SupportMonthsAwarded = model.SupportMonthsAwarded;
            saveUpdateTGInfo.SupportMonthsUsed = model.SupportMonthsUsed;
            saveUpdateTGInfo.TraineesAppointed = model.TraineesAppointed;                                                  
            saveUpdateTGInfo.UrmTraineesAppointed = model.UrmTraineesAppointed;
            saveUpdateTGInfo.UrmSupportMonthsUsed = model.UrmSupportMonthsUsed;
            saveUpdateTGInfo.DisabilitiesTraineesAppointed = model.DisabilitiesTraineesAppointed;
            saveUpdateTGInfo.DisabilitiesSupportMonthsUsed = model.DisabilitiesSupportMonthsUsed;
            saveUpdateTGInfo.DisadvantagedTraineesAppointed = model.DisadvantagedTraineesAppointed;
            saveUpdateTGInfo.DisadvantagedSupportMonthsUsed = model.DisadvantagedSupportMonthsUsed;
            // Need to hardCode Doctoral Level => to 1 for a Pre_doc
            //saveUpdateTGInfo.DoctoralLevelId = model.DoctoralLevelId;
            if (isNew)
            {
                //saveUpdateTGInfo.Id = Guid.NewGuid();
                //model.TraineeSummaryId = saveUpdateTGInfo.Id;
                /*
                 * MVN - my notes => 
                        public TraineeSummaryInfo()
                        {
                            Id = System.Guid.NewGuid();
                            IsDeleted = false;
                        }
                 */
                saveUpdateTGInfo.DoctoralLevelId = DOCTORALID;
                _context.Set<TraineeSummaryInfo>().Add(saveUpdateTGInfo);
            }

            // Track last updated date and user
            saveUpdateTGInfo.DateLastUpdated = DateTime.Now;
            saveUpdateTGInfo.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
            _context.SaveChanges();
            
            //UPDATE
            var traineeSummaryVM = new TraineeSummaryInformationViewModel{
                TraineeSummaryId = saveUpdateTGInfo.Id,
                TrainingGrantId = model.TrainingGrantId,
                DateStarted = model.DateStarted,
                DateStartedText = String.Format(_DATEFORMAT, model.DateStarted),

                DateEnded = model.DateEnded,
                DateEndedText = String.Format(_DATEFORMAT, model.DateEnded),

                PositionsAwarded = model.PositionsAwarded,
                SupportMonthsAwarded = model.SupportMonthsAwarded,
                SupportMonthsUsed = model.SupportMonthsUsed,
                TraineesAppointed = model.TraineesAppointed,
                UrmTraineesAppointed = model.UrmTraineesAppointed,
                UrmSupportMonthsUsed = model.UrmSupportMonthsUsed,
                DisabilitiesTraineesAppointed = model.DisabilitiesTraineesAppointed,
                DisabilitiesSupportMonthsUsed = model.DisabilitiesSupportMonthsUsed,
                DisadvantagedTraineesAppointed = model.DisadvantagedTraineesAppointed,
                DisadvantagedSupportMonthsUsed = model.DisadvantagedSupportMonthsUsed,
                DoctoralLevelId = model.DoctoralLevelId,

                /// MVN added 9/17/2015 tracking user activity 
                StrDateLastUpdated = (saveUpdateTGInfo.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, saveUpdateTGInfo.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(saveUpdateTGInfo.LastUpdatedBy)
            };
            return new JsonCamelCaseResult(traineeSummaryVM, JsonRequestBehavior.AllowGet);
            //return Json(true);
        }
        /// <summary>
        /// MVN - added 3/4/2015
        /// THIS -> allows save/update trainee information form data collection for Post-doc 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult SaveUpdateTraineeSummaryPostDocInfo(TraineeSummaryPostModel model)
        {
            int DOCTORALID = 2;
            bool isNew = model.TraineeSummaryId.Equals(Guid.Empty) || !_context.Set<TraineeSummaryInfo>()
                .Any(t => t.Id == model.TraineeSummaryId);

            var saveUpdateTGInfo = isNew
                ? new TraineeSummaryInfo() : _context.Set<TraineeSummaryInfo>().Find(model.TraineeSummaryId);
            //? new TraineeSummaryInfo() : _context.Set<TraineeSummaryInfo>().Single(ts => ts.Id == model.TraineeSummaryId);
            //Add New
            saveUpdateTGInfo.TrainingGrantId = model.TrainingGrantId;

            saveUpdateTGInfo.DateStarted = model.DateStarted;
            saveUpdateTGInfo.DateEnded = model.DateEnded;

            saveUpdateTGInfo.PositionsAwarded = model.PositionsAwarded;
            saveUpdateTGInfo.SupportMonthsAwarded = model.SupportMonthsAwarded;
            saveUpdateTGInfo.SupportMonthsUsed = model.SupportMonthsUsed;
            saveUpdateTGInfo.TraineesAppointed = model.TraineesAppointed;
            saveUpdateTGInfo.UrmTraineesAppointed = model.UrmTraineesAppointed;
            saveUpdateTGInfo.UrmSupportMonthsUsed = model.UrmSupportMonthsUsed;
            saveUpdateTGInfo.DisabilitiesTraineesAppointed = model.DisabilitiesTraineesAppointed;
            saveUpdateTGInfo.DisabilitiesSupportMonthsUsed = model.DisabilitiesSupportMonthsUsed;
            saveUpdateTGInfo.DisadvantagedTraineesAppointed = model.DisadvantagedTraineesAppointed;
            saveUpdateTGInfo.DisadvantagedSupportMonthsUsed = model.DisadvantagedSupportMonthsUsed;

            saveUpdateTGInfo.NumMdAppointed = model.NumMdAppointed;
            saveUpdateTGInfo.NumMdPhDAppointed = model.NumMdPhDAppointed;
            saveUpdateTGInfo.NumPhDAppointed = model.NumPhDAppointed;
            saveUpdateTGInfo.NumOtherDegreeAppointed = model.NumOtherDegreeAppointed;

            // Need to hardCode Doctoral Level => to 2 for a Post_doc
            //saveUpdateTGInfo.DoctoralLevelId = model.DoctoralLevelId;
            if (isNew)
            {
                //saveUpdateTGInfo.Id = Guid.NewGuid();
                //model.TraineeSummaryId = saveUpdateTGInfo.Id;
                /*
                 * MVN - my notes => 
                        public TraineeSummaryInfo()
                        {
                            Id = System.Guid.NewGuid();
                            IsDeleted = false;
                        }
                 */
                saveUpdateTGInfo.DoctoralLevelId = DOCTORALID;
                _context.Set<TraineeSummaryInfo>().Add(saveUpdateTGInfo);
            }

            // Track last updated date and user
            saveUpdateTGInfo.DateLastUpdated = DateTime.Now;
            saveUpdateTGInfo.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
            _context.SaveChanges();

            //UPDATE
            var traineeSummaryVM = new TraineeSummaryInformationViewModel
            {
                TraineeSummaryId = saveUpdateTGInfo.Id,
                TrainingGrantId = model.TrainingGrantId,
                DateStarted = saveUpdateTGInfo.DateStarted,
                DateStartedText = String.Format(_DATEFORMAT, saveUpdateTGInfo.DateStarted),

                DateEnded = saveUpdateTGInfo.DateEnded,
                DateEndedText = String.Format(_DATEFORMAT, saveUpdateTGInfo.DateEnded),

                PositionsAwarded = model.PositionsAwarded,
                SupportMonthsAwarded = model.SupportMonthsAwarded,
                SupportMonthsUsed = model.SupportMonthsUsed,
                TraineesAppointed = model.TraineesAppointed,
                UrmTraineesAppointed = model.UrmTraineesAppointed,
                UrmSupportMonthsUsed = model.UrmSupportMonthsUsed,
                DisabilitiesTraineesAppointed = model.DisabilitiesTraineesAppointed,
                DisabilitiesSupportMonthsUsed = model.DisabilitiesSupportMonthsUsed,
                DisadvantagedTraineesAppointed = model.DisadvantagedTraineesAppointed,
                DisadvantagedSupportMonthsUsed = model.DisadvantagedSupportMonthsUsed,
                NumMdAppointed = model.NumMdAppointed,
                NumMdPhDAppointed = model.NumMdPhDAppointed,
                NumPhDAppointed = model.NumPhDAppointed,
                NumOtherDegreeAppointed = model.NumOtherDegreeAppointed,
                DoctoralLevelId = model.DoctoralLevelId,

                /// MVN added 9/17/2015 tracking user activity 
                StrDateLastUpdated = (saveUpdateTGInfo.DateLastUpdated != null) ? String.Format(_LastUpdateDATEFORMAT, saveUpdateTGInfo.DateLastUpdated) : string.Empty,
                StrLastUpdatedBy = commonRepository.GetLoginInfo(saveUpdateTGInfo.LastUpdatedBy)
            };
            return new JsonCamelCaseResult(traineeSummaryVM, JsonRequestBehavior.AllowGet);
            //return Json(true);
        }


        // POST: /TrainingGrant/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            TrainingGrant traininggrant = db.TrainingGrants.Find(id);
            db.TrainingGrants.Remove(traininggrant);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}

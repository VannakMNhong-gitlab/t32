﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using TraineeTrackingSystem.Common;
using TraineeTrackingSystem.Repository;
using TraineeTrackingSystem.Data;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using TraineeTrackingSystem.Web.Common;
using TraineeTrackingSystem.Web.PostModels;
using TraineeTrackingSystem.Web.ViewModels;
// added tracking date and user update
using System.Security.Claims;
using System.Net;
using TraineeTrackingSystem.Web.Models;

namespace TraineeTrackingSystem.Web.Controllers
{
    [Authorize]
    [AuthorizeUser(AccessLevel = "Administrator")] 
    public class AdminController : Controller
    {
        private readonly AdminRepository adminRepository;
        private readonly CommonDataRepository commonRepository;
        private TTSEntities db = new TTSEntities();

        string inValidationResult = "ERROR! Not Unique record. Not saved.";
        private readonly TTSEntities _context;
        // MVN - the Admin class uses the Repository pattern and it no longer uses LINQ classes directly
        // This Admin constructor creates an instance of the repository 
        public AdminController()
        {
            _context = new TTSEntities();
            this.adminRepository = new AdminRepository(_context);
            this.commonRepository = new CommonDataRepository(_context);
        }

        protected override void Dispose(bool disposing)
        {
            adminRepository.Dispose();
            base.Dispose(disposing);
        }
        /// MVN - modified codes 4/13/2016 
        //[Authorize]
        //[AuthorizeUser(AccessLevel = "SuperUser")]
        //[AuthorizeUser(AccessLevel = "Administrator")] 
         //[AuthorizeMultiple(AccessLevel = "SuperUser, Administrator, Editor")]
        // GET: Admin
        public ActionResult Index()
        {
            var states = _context.StateProvinces.ToList();
            var countries = _context.Countries.ToList();
            //var getInstitutionList = commonRepository.getInstFromViewTable().ToList();
   
            var institutionSponsorViewModel = new InstitutionsViewModel{
                States = states.Where(st => st.IsDeleted == false).Select(s => new KeyValuePair<int, string>(s.Id, s.Abbreviation)),
                Countries = countries.Where(c =>c.IsDeleted == false).Select(c => new KeyValuePair<int, string>(c.Id, c.Name)),
                AdminInstSponsorGridView = commonRepository.getInstFromViewTable().Select(instSponsor => new InstitutionViewModel{
                    Id = instSponsor.Id,
                    Name = (instSponsor.Name != null) ? instSponsor.Name :string.Empty,
                    City = (instSponsor.City != null) ? instSponsor.City :string.Empty,
                    StateId = instSponsor.StateProvinceId.HasValue ? instSponsor.StateProvinceId : null, // return an empty to UI
                    StateName = instSponsor.StateProvinceAbbreviation,
                    CountryId = instSponsor.CountryId,
                    CountryName = instSponsor.Country,
                    TotalInstitutionUsage = instSponsor.TotalInstitutionUsage              
                })
            };
 
            var academicdegree = new AdminDegreesVM{
                DegreeInformation = commonRepository.getDegreeFromViewTable().Select(dgs => new AdminDegreeVM{
                    Id = dgs.AcademicDegreeId,
                    AcademicDegreeName = dgs.Name,
                    TotalDegreeUsage = dgs.TotalDegreeUsage
                })
            };
            var academiccountry = new AdminCountries{
                Countries = countries.Select(c => new KeyValuePair<int, string>(c.Id, c.Name)),
                CountryInformation = commonRepository.getCountryFromViewTable().Select(country => new AdminCountry{
                    Id = country.CountryId,
                    Country = country.Country,
                    TotalCountryUsage = country.TotalCountryUsage
                })
            };
            var academicStateProvince = new AdminStates{
                Countries = countries.Where(c => c.IsDeleted == false).Select(c => new KeyValuePair<int, string>(c.Id, c.Name)),
                StateProvinceInformation = commonRepository.getStateProvinceFromViewTable().Select(state => new AdminState{
                    Id = state.StateProvinceId,
                    Abbreviation = state.Abbreviation,
                    FullName = state.FullName,
                    CountryId = state.CountryId,
                    Country = state.Country,
                    TotalStateProvinceUsage = state.TotalStateProvinceUsage.HasValue ? state.TotalStateProvinceUsage.Value: 0
                })                                                                                                                     
            };
            var organizationTypes = _context.OrganizationTypes.ToList();
            var typesOfOrg = new TypeOfOrganizations{
                OrganizationTypes = organizationTypes.Select(o => new KeyValuePair<int, string>(o.Id, o.OrganizationType_)),
                TypeOfOrganizationInformation = commonRepository.getTypeOfOrgFromViewTable().Select(org => new TypeOfOrganization{ 
                    Id = org.OrganizationTypeId,
                    OrganizationType = org.OrganizationType,
                    TotalOrganization = org.TotalOrganizationTypeUsage.HasValue ? org.TotalOrganizationTypeUsage.Value: 0
                })
            };
            var program = new AdminPrograms{
                AdminProgramsInformation = commonRepository.getProgramsFromViewTable().Select(prog => new AdminProgram{
                    Id = prog.ProgramId,
                    ProgramTitle = prog.ProgramTitle,
                    TotalProgramUsage = prog.TotalProgramUsage.HasValue ? prog.TotalProgramUsage.Value: 0
                })
            };
            var factultyRank = new AdminFacultyRanks{
                AdminFacultyRankInformation = commonRepository.getFacultyRankFromViewTable().Select(ftyRnk => new AdminFacultyRank{
                    Id = ftyRnk.FacultyRankId,
                    Rank = ftyRnk.Rank,
                    TotalFacultyRankUsage = ftyRnk.TotalFacultyRankUsage.HasValue ? ftyRnk.TotalFacultyRankUsage.Value: 0
                })
            };
            var facultyGender = new AdminFacultyGenders{
                AdminFacultyGenderInformation = commonRepository.getFacultyGenderFromViewTable().Select(ftyGdr => new AdminFacultyGender{
                    Id = ftyGdr.GenderId,
                    Gender = ftyGdr.Gender,
                    TotalGenderUsage = ftyGdr.TotalGenderUsage.HasValue ? ftyGdr.TotalGenderUsage.Value: 0
                })
            };
            var facultyEthnicity = new AdminFacultyEthnicitys{
                AdminFacultyEthnicityInformation = commonRepository.getFacultyEthnicityFromViewTable().Select(ftyEthnicity => new AdminFacultyEthnicity{
                    Id = ftyEthnicity.EthnicityId,
                    Ethnicity = ftyEthnicity.Ethnicity,
                    TotalEthnicityUsage = ftyEthnicity.TotalEthnicityUsage.HasValue ? ftyEthnicity.TotalEthnicityUsage.Value: 0
                })
            };            
            var fundingRole = new AdminFundingRoles{
                AdminFundingRoleInformation = commonRepository.getFundingRoleFromViewTable().Where(f => f.IsTrainingGrantRole == false).Select(fftyRle => new AdminFundingRole{
                    Id = fftyRle.FundingRoleId,
                    Name = fftyRle.Name,
                    TotalFundingRoleUsage = fftyRle.TotalFundingRoleUsage.HasValue ? fftyRle.TotalFundingRoleUsage.Value :0
                })
            };

            var trainingGrantRole = new AdminFundingRoles{
                AdminTrainingGrantRoleInformation = commonRepository.getFundingRoleFromViewTable().Where(f => f.IsTrainingGrantRole == true).Select(fftyRle => new AdminFundingRole{
                    Id = fftyRle.FundingRoleId,
                    Name = fftyRle.Name,
                    TotalTrainingGrantRoleUsage = fftyRle.TotalTrainingGrantRoleUsage.HasValue ? fftyRle.TotalTrainingGrantRoleUsage.Value : 0
                })
            };

            var organization = new AdminOrganizations{
                OrganizationTypes = organizationTypes.Select(o => new KeyValuePair<int, string>(o.Id, o.OrganizationType_)),
                AdminOrganizationInformation = commonRepository.getOrganizationFromViewTable().Select(org => new AdminOrganization{
                    Id = org.Id,
                    OrganizationId = org.OrganizationId, // this data define as string
                    OrganizationTypeId = org.OrganizationTypeId,
                    OrganizationType = (org.OrganizationType != null) ? org.OrganizationType : string.Empty,
                    /// type name is in another table
                    DisplayName = org.DisplayName,
                    HrName = (org.HrName != null) ? org.HrName: string.Empty,
                    TotalOrganizationUsage = org.TotalOrganizationUsage.HasValue ? org.TotalOrganizationUsage.Value : 0
                })
            };
            var facultyTrack = new AdminFacultyTracks{
                AdminFacultyTrackInformation = commonRepository.getFacultyTrackFromViewTable().Select(ftyTrack => new AdminFacultyTrack{
                    Id = ftyTrack.FacultyTrackId,
                    FacultyTrack = ftyTrack.FacultyTrack,
                    TotalFacultyTrackUsage = ftyTrack.TotalFacultyTrackUsage.HasValue ? ftyTrack.TotalFacultyTrackUsage.Value : 0
                })
            };
            var facultyPathway = new AdminFacultyPathways{
                AdminFacultyPathwayInformation = commonRepository.getFacultyPathwayFromViewTable().Select(ftyPathway => new AdminFacultyPathway{
                    Id = ftyPathway.ClinicalFacultyPathwayId,
                    ClinicalFacultyPathway = ftyPathway.ClinicalFacultyPathway,
                    TotalClinicalFacultyPathwayUsage = ftyPathway.TotalClinicalFacultyPathwayUsage.HasValue ? ftyPathway.TotalClinicalFacultyPathwayUsage.Value: 0
                })
            };
            return View(new EditAdminViewModel{
                InstitutionSponsorInfo = institutionSponsorViewModel,
                AcademicDegrees = academicdegree,
                CountryiesInfo = academiccountry,
                StateProvineInfo = academicStateProvince,
                TypeOfOrgInfo = typesOfOrg,
                ProgramInfo = program,
                FacultyRankInfo = factultyRank,
                FacultyGenderInfo = facultyGender,
                FacultyEthnicityInfo = facultyEthnicity,
                FundingRoleInfo = fundingRole,
                TrainingGrantRoleInfo = trainingGrantRole,
                OrganizationInfo = organization,
                FacultyTrackInfo = facultyTrack,
                FacultyPathwayInfo = facultyPathway
            });
        }

        

        
        [HttpPost]
        public ActionResult UpdateInstitution(InstAdminPostModel model)
        {
            bool isNew = !model.Id.HasValue || !_context.Set<Institution>()
                .Any(o => o.Id == model.Id);
            var newInst = isNew
                ? new Institution() : _context.Set<Institution>().Find(model.Id);

            newInst.Name = model.Name;
            newInst.City = model.City;
            newInst.StateId = model.StateId;
            newInst.CountryId = model.CountryId;
            newInst.InstitutionTypeId = 1;

            Institution currentInstitution = null;
            if (isNew){
                if (model.Name != null && model.CountryName != null){
                    if (_context.Set<Institution>().Any(insttName => insttName.Name == model.Name)&&
                        (_context.Set<Institution>().Any(instCtryName => instCtryName.Country.Name == model.CountryName))){
                        currentInstitution = _context.Set<Institution>().Single(st => st.Name == model.Name);
                        currentInstitution = _context.Set<Institution>().Single(ctry => ctry.Country.Name == model.CountryName);                       
                        newInst.CountryId = model.CountryId;
                        newInst.Id = currentInstitution.Id;
                    }
                    _context.Set<Institution>().Add(newInst);
                }
            }
            _context.SaveChanges();

            var states = _context.StateProvinces.ToList();
            var countries = _context.Countries.ToList();
            var institutionViewModel = new InstitutionViewModel{
                Id = newInst.Id,
                Name = (newInst.Name != null) ? newInst.Name : string.Empty,
                City = (newInst.City != null) ? newInst.City : string.Empty,
                StateId = newInst.StateId.HasValue ? newInst.StateId : 0, // return an empty to UI
                StateName = (model.StateName != null) ? model.StateName : string.Empty,
                States = states.Select(s => new KeyValuePair<int, string>(s.Id, s.Abbreviation)),
                
                CountryId = newInst.CountryId,
                CountryName = model.CountryName,
                Countries = countries.Select(c => new KeyValuePair<int, string>(c.Id, c.Name)),
                TotalInstitutionUsage = model.TotalInstitutionUsage.HasValue ? model.TotalInstitutionUsage.Value : 0
            };
            return new JsonCamelCaseResult(institutionViewModel, JsonRequestBehavior.AllowGet);
        }    
        
        
        [HttpPost]
        public ActionResult UpdateStateProvince(StateAdminPostModel model)
        {
            bool isNew = !model.Id.HasValue || !_context.Set<StateProvince>()
               .Any(o => o.Id == model.Id);
            var newStateProvince = isNew
                ? new StateProvince()
                : _context.Set<StateProvince>()
                .Include(inst => inst.Institutions)
                .Include(ctry => ctry.Country)
                .FirstOrDefault(st => st.Id == model.Id);

            newStateProvince.Abbreviation = model.Abbreviation;
            newStateProvince.FullName = model.StateFullName;
            newStateProvince.CountryId = model.CountryId;

            StateProvince currentStateProvince = null;
            if (isNew){
                /// CHECK with Abbreviation contents
                if (model.Abbreviation != null){
                    if (_context.Set<StateProvince>().Any(abb => abb.Abbreviation == model.Abbreviation)
                        && _context.Set<StateProvince>().Any(st => st.FullName == model.StateFullName)
                        && _context.Set<StateProvince>().Any(ctry => ctry.Country.Name == model.Country)){
                        currentStateProvince = _context.Set<StateProvince>().Single(st => st.FullName == model.StateFullName);
                        currentStateProvince = _context.Set<StateProvince>().Single(ctry => ctry.Country.Name == model.Country);
                        newStateProvince.CountryId = model.CountryId;
                        newStateProvince.Id = currentStateProvince.Id;
                    }
                    _context.Set<StateProvince>().Add(newStateProvince);
                /// Check without Abbreviation contents
                }else{
                    if (_context.Set<StateProvince>().Any(st => st.FullName == model.StateFullName)
                         && _context.Set<StateProvince>().Any(ctry => ctry.Country.Name == model.Country)){
                        currentStateProvince = _context.Set<StateProvince>().Single(st => st.FullName == model.StateFullName);
                        currentStateProvince = _context.Set<StateProvince>().Single(ctry => ctry.Country.Name == model.Country);
                        newStateProvince.CountryId = model.CountryId;
                        newStateProvince.Id = currentStateProvince.Id;                
                    }
                    _context.Set<StateProvince>().Add(newStateProvince);
                }
            }
            _context.SaveChanges();
            
            var countries = _context.Countries.ToList();
            var academicStateProvince = new AdminState{
                Id = newStateProvince.Id,
                Abbreviation = newStateProvince.Abbreviation,
                FullName = newStateProvince.FullName,
                CountryId = newStateProvince.CountryId,
                Country = model.Country,
                Countries = countries.Select(c => new KeyValuePair<int, string>(c.Id, c.Name)),
                TotalStateProvinceUsage = model.TotalStateProvinceUsage.HasValue ? model.TotalStateProvinceUsage.Value :0
            };

            return new JsonCamelCaseResult(academicStateProvince, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateDegree(AdminDegreePostModel model)
        {
            bool isNew = !model.Id.HasValue || !_context.Set<AcademicDegree>()
                .Any(o => o.Id == model.Id);
            var newDegreeInfo = isNew
                ? new AcademicDegree()
                : _context.Set<AcademicDegree>()
                .Find(model.Id);
            newDegreeInfo.Name = model.AcademicDegree;
            if (isNew){
                if (!(_context.Set<AcademicDegree>().Any(dg => dg.Name == model.AcademicDegree && dg.IsDeleted == false))){
                    _context.Set<AcademicDegree>().Add(newDegreeInfo);
                }else{
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return HandledBadRequestContentActionResult(inValidationResult);
                }

            }
            _context.SaveChanges();
            var degreeViewModel = new AdminDegreeVM
            {
                Id = newDegreeInfo.Id,
                AcademicDegreeName = newDegreeInfo.Name,
                TotalDegreeUsage = model.TotalDegreeUsage.HasValue ? model.TotalDegreeUsage.Value : 0
            };
            return new JsonCamelCaseResult(degreeViewModel, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateTypeOfOrganization(AdminTypeOfOrgPostModel model)
        {
            bool isNew = !model.Id.HasValue || !_context.Set<OrganizationType>()
                .Any(o => o.Id == model.Id);
            var newTypeOfOrg = isNew
                ? new OrganizationType() : _context.Set<OrganizationType>().Find(model.Id);

            newTypeOfOrg.OrganizationType_ = model.OrganizationType;
            if (isNew){
                if (!(_context.Set<OrganizationType>().Any(org => org.OrganizationType_ == model.OrganizationType && org.IsDeleted == false))){
                    _context.Set<OrganizationType>().Add(newTypeOfOrg);
                }else{
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return HandledBadRequestContentActionResult(inValidationResult);
                }
            }
            _context.SaveChanges();

            var organizationTypes = _context.OrganizationTypes.ToList();
            var typeOfOrganizationViewModel = new TypeOfOrganization
            {
                Id = newTypeOfOrg.Id,
                OrganizationType = newTypeOfOrg.OrganizationType_,
                OrganizationTypes = organizationTypes.Select(o => new KeyValuePair<int, string>(o.Id, o.OrganizationType_)),
                TotalOrganization = model.TotalOrganization.HasValue ? model.TotalOrganization.Value : 0
            };
            return new JsonCamelCaseResult(typeOfOrganizationViewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateProgram(AdminProgramPostModel model)
        {
            bool isNew = model.Id.Equals(Guid.Empty) || !_context.Set<Program>()
                .Any(o => o.Id == model.Id);
            var newProgram = isNew
                ? new Program() : _context.Set<Program>()
                .Single(prg => prg.Id == model.Id);

            newProgram.Title = model.ProgramTitle;
            if (isNew){
                if (model.ProgramTitle != null){
                    if (!(_context.Set<Program>().Any(f => f.Title == model.ProgramTitle && f.IsDeleted == false))){
                        _context.Set<Program>().Add(newProgram);
                    }else{
                        Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        return HandledBadRequestContentActionResult(inValidationResult);
                    }
                }
            }
            _context.SaveChanges();
            var programViewModel = new AdminProgram
            {
                Id = newProgram.Id,
                ProgramTitle = newProgram.Title,
                TotalProgramUsage = model.TotalProgramUsage.HasValue ? model.TotalProgramUsage.Value : 0
            };
            return new JsonCamelCaseResult(programViewModel, JsonRequestBehavior.AllowGet);
        } 

        [HttpPost]
        public ActionResult UpdateCountry(CountryAdminPostModel model)
        {
            bool isNew = !model.Id.HasValue || !_context.Set<Country>()
                .Any(o => o.Id == model.Id);
            var newCountry = isNew
                ? new Country() : _context.Set<Country>().Find(model.Id);

            newCountry.Name = model.Country;
            if (isNew){
                if (model.Country != null){
                    if (!(_context.Set<Country>().Any(f => f.Name == model.Country && f.IsDeleted == false))){
                        _context.Set<Country>().Add(newCountry);
                    }else{
                        Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        return HandledBadRequestContentActionResult(inValidationResult);
                    }
                }
            }
            _context.SaveChanges();

            var countryViewModel = new AdminCountry{
                Id = newCountry.Id,
                Country = newCountry.Name,
                TotalCountryUsage = model.TotalCountryUsage.HasValue ? model.TotalCountryUsage.Value : 0
            };
            return new JsonCamelCaseResult(countryViewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateFacultyGender(AdminFacultyGenderPostModel model)
        {
            bool isNew = !model.Id.HasValue || !_context.Set<Gender>()
                .Any(o => o.Id == model.Id);
            var newFacultyGender = isNew
                ? new Gender() : _context.Set<Gender>().Find(model.Id);

            newFacultyGender.Name = model.Gender;

            if (isNew){
                /// ADD NEW Country record
                if (model.Gender != null){
                    if (!(_context.Set<Gender>().Any(f => f.Name == model.Gender && f.IsDeleted == false))){
                        _context.Set<Gender>().Add(newFacultyGender);
                    }else{
                        ////Response.StatusDescription = HandledBadRequestContentActionResult(inValidationResult);
                        //    //(int)HttpStatusCode.BadRequest;
                        //return Content("dup");
                        Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        return HandledBadRequestContentActionResult(inValidationResult);
                    }
                }
            }
            _context.SaveChanges();

            var faclutyGenderViewModel = new AdminFacultyGender{
                Id = newFacultyGender.Id,
                Gender = newFacultyGender.Name,
                TotalGenderUsage = model.TotalGenderUsage.HasValue ? model.TotalGenderUsage.Value : 0
            };
            return new JsonCamelCaseResult(faclutyGenderViewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateFacultyTrack(AdminFacultyTrackPostModel model)
        {
            bool isNew = !model.Id.HasValue || !_context.Set<FacultyTrack>()
                .Any(o => o.Id == model.Id);
            var newFacultyTrack = isNew
                ? new FacultyTrack() : _context.Set<FacultyTrack>().Find(model.Id);

            newFacultyTrack.Name = model.FacultyTrack;

            if (isNew){
                /// ADD NEW Country record
                if (model.FacultyTrack != null){
                    if (!(_context.Set<FacultyTrack>().Any(f => f.Name == model.FacultyTrack && f.IsDeleted == false))){
                        _context.Set<FacultyTrack>().Add(newFacultyTrack);
                    }else{
                        ////Response.StatusDescription = HandledBadRequestContentActionResult(inValidationResult);
                        //    //(int)HttpStatusCode.BadRequest;
                        //return Content("dup");
                        Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        return HandledBadRequestContentActionResult(inValidationResult);
                    }
                }
            }
            _context.SaveChanges();

            var faclutyTrackViewModel = new AdminFacultyTrack
            {
                Id = newFacultyTrack.Id,
                FacultyTrack = newFacultyTrack.Name,
                TotalFacultyTrackUsage = model.TotalFacultyTrackUsage.HasValue ? model.TotalFacultyTrackUsage.Value : 0
            };
            return new JsonCamelCaseResult(faclutyTrackViewModel, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateClinicalFtyPathway(AdminFacultyPathwayPostModel model)
        {
            bool isNew = !model.Id.HasValue || !_context.Set<ClinicalFacultyPathway>()
                .Any(o => o.Id == model.Id);
            var newFacultyPathway = isNew
                ? new ClinicalFacultyPathway() : _context.Set<ClinicalFacultyPathway>().Find(model.Id);

            newFacultyPathway.Name = model.ClinicalFacultyPathway;

            if (isNew){
                /// ADD NEW Country record
                if (model.ClinicalFacultyPathway != null){
                    if (!(_context.Set<ClinicalFacultyPathway>().Any(f => f.Name == model.ClinicalFacultyPathway && f.IsDeleted == false))){
                        _context.Set<ClinicalFacultyPathway>().Add(newFacultyPathway);
                    }else{
                        ////Response.StatusDescription = HandledBadRequestContentActionResult(inValidationResult);
                        //    //(int)HttpStatusCode.BadRequest;
                        //return Content("dup");
                        Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        return HandledBadRequestContentActionResult(inValidationResult);
                    }
                }
            }
            _context.SaveChanges();

            var faclutyTrackViewModel = new AdminFacultyPathway{
                Id = newFacultyPathway.Id,
                ClinicalFacultyPathway = newFacultyPathway.Name,
                TotalClinicalFacultyPathwayUsage = model.TotalClinicalFacultyPathwayUsage.HasValue ? model.TotalClinicalFacultyPathwayUsage.Value : 0
            };
            return new JsonCamelCaseResult(faclutyTrackViewModel, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult UpdateFacultyEthnicity(AdminFacultyEthnicityPostModel model)
        {
            bool isNew = !model.Id.HasValue || !_context.Set<Ethnicity>()
                .Any(o => o.Id == model.Id);
            var newFacultyEthnicity = isNew
                ? new Ethnicity() : _context.Set<Ethnicity>().Find(model.Id);

            newFacultyEthnicity.Name = model.Ethnicity;

            if (isNew){
                /// ADD NEW Country record
                if (model.Ethnicity != null){
                    if (!(_context.Set<Ethnicity>().Any(f => f.Name == model.Ethnicity && f.IsDeleted == false)))
                    {
                        _context.Set<Ethnicity>().Add(newFacultyEthnicity);
                    }
                    else
                    {
                        ////Response.StatusDescription = HandledBadRequestContentActionResult(inValidationResult);
                        //    //(int)HttpStatusCode.BadRequest;
                        //return Content("dup");
                        Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        return HandledBadRequestContentActionResult(inValidationResult);
                    }
                }
            }
            _context.SaveChanges();

            var faclutyEthnicityViewModel = new AdminFacultyEthnicity{
                Id = newFacultyEthnicity.Id,
                Ethnicity = newFacultyEthnicity.Name,
                TotalEthnicityUsage = model.TotalEthnicityUsage.HasValue ? model.TotalEthnicityUsage.Value : 0
            };
            return new JsonCamelCaseResult(faclutyEthnicityViewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateFacultyRank(AdminFacultyRankPostModel model)
        {
            bool isNew = !model.Id.HasValue || !_context.Set<FacultyRank>()
                .Any(o => o.Id == model.Id);
            var newFacultyRank = isNew
                ? new FacultyRank() : _context.Set<FacultyRank>().Find(model.Id);

            newFacultyRank.Rank = model.Rank;
            
            if (isNew){
                /// ADD NEW Country record
                if (model.Rank != null){
                    if (!(_context.Set<FacultyRank>().Any(f => f.Rank == model.Rank && f.IsDeleted == false))){
                        _context.Set<FacultyRank>().Add(newFacultyRank);
                    }else{
                        ////Response.StatusDescription = HandledBadRequestContentActionResult(inValidationResult);
                        //    //(int)HttpStatusCode.BadRequest;
                        //return Content("dup");
                        Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        return HandledBadRequestContentActionResult(inValidationResult);
                    }             
                }             
            }
            _context.SaveChanges();

            var faclutyRankViewModel = new AdminFacultyRank{
                Id = newFacultyRank.Id,
                Rank = newFacultyRank.Rank,
                TotalFacultyRankUsage = model.TotalFacultyRankUsage.HasValue ? model.TotalFacultyRankUsage.Value : 0
            };
            return new JsonCamelCaseResult(faclutyRankViewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateFctyFundingRole(AdminFctyFundingRolePostModel model)
        {
            bool isNew = !model.Id.HasValue || !_context.Set<FundingRole>()
                .Any(o => o.Id == model.Id);
            var newFctyFundingRole = isNew
                ? new FundingRole() : _context.Set<FundingRole>().Find(model.Id);

            newFctyFundingRole.Name = model.Name;

            if (isNew){
                /// ADD NEW Country record
                if (model.Name != null){
                    if (!(_context.Set<FundingRole>().Any(f => f.Name == model.Name && f.IsDeleted == false))){
                        _context.Set<FundingRole>().Add(newFctyFundingRole);
                    }else{
                        Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        return HandledBadRequestContentActionResult(inValidationResult);
                    }
                }
            }
            _context.SaveChanges();

            var fctyFundingRoleViewModel = new AdminFundingRole
            {
                Id = newFctyFundingRole.Id,
                Name = newFctyFundingRole.Name,
                TotalFundingRoleUsage = model.TotalFundingRoleUsage.HasValue ? model.TotalFundingRoleUsage.Value : 0
            };
            return new JsonCamelCaseResult(fctyFundingRoleViewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateFctyTrainingGrantRole(AdminFctyFundingRolePostModel model)
        {
            bool isNew = !model.Id.HasValue || !_context.Set<FundingRole>()
                .Any(o => o.Id == model.Id);
            var newFctyFundingRole = isNew
                ? new FundingRole() : _context.Set<FundingRole>().Find(model.Id);

            newFctyFundingRole.Name = model.Name;

            if (isNew){
                /// ADD NEW Country record
                if (model.Name != null){
                    if (!(_context.Set<FundingRole>().Any(f => f.Name == model.Name && f.IsDeleted == false))){
                        _context.Set<FundingRole>().Add(newFctyFundingRole);
                    }else{
                        Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        return HandledBadRequestContentActionResult(inValidationResult);
                    }
                }
            }
            _context.SaveChanges();

            var fctyFundingRoleViewModel = new AdminFundingRole
            {
                Id = newFctyFundingRole.Id,
                Name = newFctyFundingRole.Name,
                TotalTrainingGrantRoleUsage = model.TotalTrainingGrantRoleUsage.HasValue ? model.TotalTrainingGrantRoleUsage.Value : 0
            };
            return new JsonCamelCaseResult(fctyFundingRoleViewModel, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateOrganization(AdminOrganizationPostModel model)
        {
            bool isNew = !model.Id.HasValue || !_context.Set<Organization>()
                .Any(o => o.Id == model.Id);
            var newOrganization = isNew
                ? new Organization() : _context.Set<Organization>().Find(model.Id);

            newOrganization.OrganizationId = model.OrganizationId;
            newOrganization.OrganizationTypeId = model.OrganizationTypeId;
            newOrganization.DisplayName = model.DisplayName;
            newOrganization.HrName = model.HrName;

            if (isNew){
                /// ADD NEW Country record
                if (model.DisplayName != null){
                    if (!(_context.Set<Organization>().Any(org => org.DisplayName == model.DisplayName && org.IsDeleted == false))){
                        _context.Set<Organization>().Add(newOrganization);
                    }else{
                        Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        return HandledBadRequestContentActionResult(inValidationResult);
                    }
                }
            }
            _context.SaveChanges();
            var organizationTypes = _context.OrganizationTypes.ToList();
            var organizationViewModel = new AdminOrganization{
                Id = newOrganization.Id,
                OrganizationId = newOrganization.OrganizationId,
                OrganizationTypeId = newOrganization.OrganizationTypeId,
                OrganizationType = model.OrganizationType,
                DisplayName = newOrganization.DisplayName,
                HrName = newOrganization.HrName,
                OrganizationTypes = organizationTypes.Select(o => new KeyValuePair<int, string>(o.Id, o.OrganizationType_)),
                TotalOrganizationUsage = model.TotalOrganizationUsage.HasValue ? model.TotalOrganizationUsage.Value : 0
            };
            return new JsonCamelCaseResult(organizationViewModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AjaxRequestForAddStPrnce(StateAdminPostModel model)
        {        
            var countries = commonRepository.getCountry().ToList().Select(c => new KeyValuePair<int, string>(c.Id, c.Name));
            return new JsonCamelCaseResult(countries, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AjaxRequestForAddOrg(AdminOrganizationPostModel model)
        {
            var org = commonRepository.getOrgType().ToList().Where(o => o.IsDeleted == false).Select(c => new KeyValuePair<int, string>(c.Id, c.OrganizationType_));
            return new JsonCamelCaseResult(org, JsonRequestBehavior.AllowGet);
        }
        protected ActionResult HandledBadRequestContentActionResult(string inValidationResult) 
        {
            return Content(inValidationResult);
        }

        /// <summary>
        /// MVN - this mentod used return IsDeleted to true back to the UI when the Remove button is clicked
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RemoveInstSponsor(int id)
        {
            var traineePosition = _context.Set<Institution>().Find(id);
            if (traineePosition != null){
                traineePosition.IsDeleted = true;
                _context.SaveChanges();
            }
            return Json(id);
        }
        /// <summary>
        /// MVN - this mentod used return IsDeleted to true back to the UI when the Remove button is clicked
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RemoveDegree(int id)
        {
            var dg = _context.Set<AcademicDegree>().Find(id);
            if (dg != null){
                dg.IsDeleted = true;
                _context.SaveChanges();
            }
            return Json(id);
        }
        /// <summary>
        /// MVN - this mentod used return IsDeleted to true back to the UI when the Remove button is clicked
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RemoveProgram(Guid id)
        {
            var progr = _context.Set<Program>().Find(id);
            if (progr != null){
                progr.IsDeleted = true;
                _context.SaveChanges();
            }
            return Json(id);
        }
        /// <summary>
        /// MVN - this mentod used return IsDeleted to true back to the UI when the Remove button is clicked
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RemoveCountry(int id)
        {
            var dg = _context.Set<Country>().Find(id);
            if (dg != null){
                dg.IsDeleted = true;
                _context.SaveChanges();
            }
            return Json(id);
        }
        /// <summary>
        /// MVN - this mentod used return IsDeleted to true back to the UI when the Remove button is clicked
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RemoveStateProvince(int id)
        {
            var dg = _context.Set<StateProvince>().Find(id);
            if (dg != null){
                dg.IsDeleted = true;
                _context.SaveChanges();
            }
            return Json(id);
        }
        /// <summary>
        /// MVN - used this method to remove Facculty track from FacultyTrack list
        /// This method will be used in the UI RemoveFctyTrk function when the button is clicked
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RemoveFctyTrk(int id)
        {
            var ftyT = _context.Set<FacultyTrack>().Find(id);
            if (ftyT != null){
                ftyT.IsDeleted = true;
                _context.SaveChanges();
            }
            return Json(id);
        }

        /// <summary>
        /// MVN - used this method to remove Facculty track from FacultyTrack list
        /// This method will be used in the UI removedFtyPthw function when the button is clicked
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RemoveClinicalFtyPthwy(int id)
        {
            var ftyPthw = _context.Set<ClinicalFacultyPathway>().Find(id);
            if (ftyPthw != null){
                ftyPthw.IsDeleted = true;
                _context.SaveChanges();
            }
            return Json(id);
        }

        /// <summary>
        /// MVN - used this method to remove gender from gender list
        /// This method will be used in the UI removedFtyGndr function when the button is clicked
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RemoveFctyGndr(int id)
        {
            var ftyG = _context.Set<Gender>().Find(id);
            if (ftyG != null){
                ftyG.IsDeleted = true;
                _context.SaveChanges();
            }
            return Json(id);
        }
        /// <summary>
        /// MVN - used this method to remove Ethnicity from the Ethnicity grid list
        /// To be called by the UI in the RemoveFtyEthnicity function when the button is clicked
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RemoveFctyEthnicity(int id)
        {
            var ftyE = _context.Set<Ethnicity>().Find(id);
            if (ftyE != null){
                ftyE.IsDeleted = true;
                _context.SaveChanges();
            }
            return Json(id);
        }
        /// <summary>
        /// MVN - this mentod used return IsDeleted to true back to the UI when the Remove button is clicked
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RemoveFctyRnk(int id)
        {
            var ftyR = _context.Set<FacultyRank>().Find(id);
            if (ftyR != null){
                ftyR.IsDeleted = true;
                _context.SaveChanges();
            }
            return Json(id);
        }
        /// <summary>
        /// MVN - this mentod used return IsDeleted to true back to the UI when the Remove button is clicked
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RemoveFctyRle(int id)
        {
            var ftyRle = _context.Set<FundingRole>().Find(id);
            if (ftyRle != null)
            {
                ftyRle.IsDeleted = true;
                _context.SaveChanges();
            }
            return Json(id);
        }

        /// <summary>
        /// MVN - this mentod used return IsDeleted to true back to the UI when the Remove button is clicked
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RemoveTctyRle(int id)
        {
            var tftyRle = _context.Set<FundingRole>().Find(id);
            if (tftyRle != null){
                tftyRle.IsDeleted = true;
                _context.SaveChanges();
            }
            return Json(id);
        }

        /// <summary>
        /// MVN - this mentod used return IsDeleted to true back to the UI when the Remove button is clicked
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RemoveOrganization(int id)
        {
            var organization = _context.Set<Organization>().Find(id);
            if (organization != null){
                organization.IsDeleted = true;
                _context.SaveChanges();
            }
            return Json(id);
        }
        /// <summary>
        /// MVN - this mentod used return IsDeleted to true back to the UI when the Remove button is clicked
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RemoveOrganizationType(int id)
        {
            var orgType = _context.Set<OrganizationType>().Find(id);
            if (orgType != null){
                orgType.IsDeleted = true;
                _context.SaveChanges();
            }
            return Json(id);
        }
    }
}

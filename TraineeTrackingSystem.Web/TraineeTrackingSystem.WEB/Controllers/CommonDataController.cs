﻿using System.Security.Claims;
using System;
using System.Linq;
using System.Web.Mvc;
using TraineeTrackingSystem.Data;
using TraineeTrackingSystem.Repository;
using TraineeTrackingSystem.Web.PostModels;
using TraineeTrackingSystem.Web.ViewModels;
using System.Xml;                               //this namespace so the XmlTextReader class does not need to be qualified
using System.IO;
using System.Xml.XPath;
using System.Net;
using System.Text;
using System.Collections;
using System.Xml.Linq;
using System.Collections.Generic;
using TraineeTrackingSystem.Web.Common;

namespace TraineeTrackingSystem.Web.Controllers
{
    public class CommonDataController : Controller
    {
        private readonly CommonDataRepository _CommonDataRepository;
        private readonly TTSEntities _context;

        // Retrieve the Citation based on Title => this return JSON format from 
        private const string HTTP_REQ = "http://entrezajax.appspot.com/esearch+esummary?&db=pubmed&term=";
        private const string API_KEY = "191d24f81e61c107bca103f7d6a9ca10"; 
           // Retroviral+RNA+elements+integrate+components+of+post-transcriptional+gene+expression&apikey=191d24f81e61c107bca103f7d6a9ca10
        // Retrieve the XML stream by the URL
        // Remote XML feed returned from an HTTPURL - provide the ability for user to key in with prefix of pmcid
        private const string HTTP_URL = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pmc&id=";
        public CommonDataController()
        {
             _context = new TTSEntities();
             _CommonDataRepository = new CommonDataRepository(_context);
        }

        protected override void Dispose(bool disposing)
        {
            _CommonDataRepository.Dispose();
            base.Dispose(disposing);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult SearchEmployee(string partialName)
        {

            var employees = _CommonDataRepository.SearchEmployees(partialName).OrderBy(p => p.FirstName);
            return Json(employees, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult SearchFaculty(PersonPostModel searchModel)
        {

            var faculties = _CommonDataRepository.SearchFaculty(searchModel.FirstName, searchModel.LastName, searchModel.MiddleName, searchModel.EmployeeId);
            return Json(faculties, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// MVN - created 4/27/2016 
        /// This will be used in the UI searchPerson function  
        /// </summary>
        /// <param name="searchModel"></param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult SearchUser(UserPostModel searchModel)
        {

            var user = _CommonDataRepository.SearchUser(searchModel.UserName, searchModel.FirstName, searchModel.LastName, searchModel.MiddleName, searchModel.RoleName);
            return Json(user, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult SearchForFundings(GeneralFundingPostModel searchModel)
        {
            var fundings = _CommonDataRepository.SearchForFundings(searchModel.FacultyId, searchModel.SponsorName, searchModel.Title, searchModel.GrtNumber, searchModel.FundingTypeId);
            return Json(fundings, JsonRequestBehavior.AllowGet);
        }
        
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult PostFCTYDegree(PersonPostModel searchModel)
        {
            var faculties = _CommonDataRepository.GetFCTYDegree(searchModel.DegreeYear, searchModel.AcadmicDegreeName, searchModel.OtherDegree);
            return Json(faculties, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult Search(PersonPostModel searchModel)
        {

            var comps = _CommonDataRepository.SearchPerson(searchModel.FirstName, searchModel.LastName, searchModel.MiddleName);
            return Json(comps, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult SearchMentee(MenteePostModel searchModel)
        {

            var mentees = _CommonDataRepository.SearchMentee(searchModel.FirstName, searchModel.LastName, searchModel.MiddleName, searchModel.StudentId);
            return Json(mentees, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult SearchTrainingGrants(TrainingGrantPostModel searchModel)
        {
            var trainingGrants = _CommonDataRepository.SearchTrainingGrants(searchModel.PdFacultyId, searchModel.SponsorName, searchModel.Title);
            return Json(trainingGrants, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult SearchSupport(SupportPostModel searchModel)
        {
            var support = _CommonDataRepository.SearchSupport(searchModel.Title, searchModel.SupportTypeId, searchModel.SupportNumber, searchModel.SupportOrganizationId, searchModel.DateStarted, searchModel.DateEnded);
            return Json(support, JsonRequestBehavior.AllowGet);
        }

        
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetMenteeList(Guid personId)
        {
            var result =
                _CommonDataRepository.GetMentee(personId).ToList().Select(mentee => new Search_VwMenteeMentors
                {
                    MenteeId = mentee.MenteeId,
                    MenteeFullName = mentee.MenteeFullName

                });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetTrainingGrantApplicantList(Guid tgrantId)
        {
            var result =
                _CommonDataRepository.GetTrainingGrantApplicantsByTGId(tgrantId).ToList().Select(app => new Search_VwTrainingGrantApplicants
                {
                    ApplicantGuid = app.ApplicantGuid,
                    ApplicantId = app.ApplicantId,
                    ApplicantType = app.ApplicantType

                });
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// MVN - loading the Grid DataSource
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetFundingInstTrainingGrantList(Guid personId)
        {
            var result =
                _CommonDataRepository.GetInstTrainingGrant(personId).ToList().Select(funding => new Search_VwTrainingGrantFaculty
                {
                    TrainingGrantId = funding.TrainingGrantId,
                    Title = funding.Title
                });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// MVN - This method will return a list of Mentee with FullName
        /// </summary>
        /// <returns></returns>
        public IQueryable<Search_VwMenteeDemographics> GetMenteeFullNameList(Guid personId)
        {
            return _context.Search_VwMenteeDemographics.Where(f=>f.Id == personId).OrderBy(m => m.FullName);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetOtherFundingList(Guid personId)
        {
            var result =
                _CommonDataRepository.GetOtherFunding(personId).ToList().Select(otherFunding => new Search_VwFundingFaculty
                {
                    Id = otherFunding.Id,
                    Title = otherFunding.Title
                });
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetFundingList(Guid personId)
        {
            var result =
                _CommonDataRepository.GetFund(personId).ToList().Select(funding => new Search_VwFundingDemographics
                {
                    //Id = funding.Id,
                    PiFacultyId = funding.PiFacultyId,
                    PiFullName = funding.PiFullName,
                    //TODO:  Add Role 
                    
                });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetResidencyInstitutionList(string name)
        {
            var result =
                _CommonDataRepository.GetInstitutionList(name).ToList().Select(residencyInstitution => new ResidencyInstitutionViewModel
                {
                    Id = residencyInstitution.Id,
                    Name = residencyInstitution.Name,
                    InstitutionIdentifier = residencyInstitution.InstitutionIdentifier
                });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddNewDegree(NewDegreePostModel model)
        {
            //new degree
            var degree = new AcademicDegree
            {
                SortOrder = _context.AcademicDegrees.Max(ac=>ac.SortOrder)+1,
                Name = model.Name,
                IsDeleted = false
            };
            _context.Set<AcademicDegree>().Add(degree);
            _context.SaveChanges();
            return Json(true);
        }

        [HttpPost]
        public JsonResult AddNewInstitution(InstitutionPostModel model)
        {
            var stateId = (model.StateId == 0) ? null : model.StateId;
            var countryId = (model.CountryId == 0) ? null : model.CountryId;
            
            //insert instition
            var institution = new Institution
            {
                Name = model.Name,
                City = model.City,
                StateId = stateId,
                CountryId = countryId,
                InstitutionTypeId=1,
                IsDeleted = false
            };
            _context.Set<Institution>().Add(institution);
            _context.SaveChanges();
            return Json(true);
        }

        [HttpPost]
        public JsonResult AddNewResidencyInstitution(ResidencyInstitutionPostModel model)
        {
            var institution = new Institution
            {
                Name = model.Name,
                City = model.City,
                StateId = model.StateId,
                CountryId = model.CountryId,
                InstitutionTypeId = 1,
                IsDeleted = false
            };
            _context.Set<Institution>().Add(institution);
            _context.SaveChanges();
            return Json(true);
        }

        [HttpPost]
        public JsonResult UpdateSupportSource(SupportSourcePostModel model)
        {
            //check if it's a new or existing Support Source
            bool isNew = model.Id.Equals(null) || !_context.Set<SupportSource>()
                .Any(ss => ss.Id == model.Id);

            var supportSource = isNew
                ? new SupportSource()
                : _context.Set<SupportSource>()
                .Single(ss => ss.Id == model.Id);

            supportSource.SupportTitle = model.SupportTitle;

            // Track last updated date and user
            supportSource.DateLastUpdated = DateTime.Now;
            supportSource.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);

            _context.Set<SupportSource>().Add(supportSource);
            _context.SaveChanges();
            return Json(true);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetAreaStudyList(string name)
        {
            var result = _CommonDataRepository.GetAreaOfStudy(name).ToList().Select(areaofstudy => new AreaOfStudyViewModel
            {
                Id = areaofstudy.Id,
                Name = areaofstudy.AreaOfStudy
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetInstitutionList(string name)
        {
            var result =
                _CommonDataRepository.GetInstitutionList(name).ToList().Select(institution => new InstitutionViewModel
                {
                    Id = institution.Id,
                    Name = institution.Name,
                    City = institution.City,
                    InstitutionIdentifier = institution.InstitutionIdentifier
                });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetUserRoleList(string name)
        {
            var result =
                _CommonDataRepository.GetUserRoleList(name).ToList().Select(role => new UsersViewModel
                {
                    //LoginId = role.LoginId
                    RoleId = role.Id,
                    Rolname = role.RoleName
                });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetAcademicDegreeInfoList(string name)
        {
            var result =
                _CommonDataRepository.GetAcademicDegreeInfoList(name).ToList().Select(academicDgrInfo => new AcademicInfoViewModel
                {
                    AcademicDegreeId = academicDgrInfo.Id,
                    AcademicDegree = academicDgrInfo.Name
                });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetCityList(string city)
        {
            var result =
                _CommonDataRepository.GetInstitutionList(city).ToList().Select(cty => new InstitutionViewModel
                {
                    Id = cty.Id,
                    City = cty.City
                });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetDegreeList(string name)
        {
            var result = _CommonDataRepository.GetDegreeList(name).ToList().Where(s => s.IsDeleted == false).Select(dg => new DegreeViewModel
            {
                Id = dg.Id,
                Name = dg.Name
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetTypeOfOrgList(string name)
        {
            var result = _CommonDataRepository.GetTypeOfOrgList(name).ToList().Where(orgType => orgType.IsDeleted == false).Select(orgType => new TypeOfOrganization
            {
                Id = orgType.Id,
                OrganizationType = orgType.OrganizationType_
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetProgramsList(string name)
        {
            var result = _CommonDataRepository.GetProgramsList(name).ToList().Select(prog => new AdminProgram
            {
                Id = prog.Id,
                ProgramTitle = prog.Title
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetCountryList(string name)
        {
            var result = _CommonDataRepository.GetCountryList(name).ToList().Where(s => s.IsDeleted == false).Select(country => new CountryViewModel{
                Id = country.Id,
                Name = country.Name
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetStateAbbreviationList(string name)
        {
            var result = _CommonDataRepository.GetStateAbbreviationList(name).ToList().Where(s => s.IsDeleted == false).Select(state => new AdminState
            {
                Id = state.Id,
                Abbreviation = state.Abbreviation
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }
         
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetStateProvinceList(string name)
        {
            var result = _CommonDataRepository.GetStateProvinceList(name).ToList().Where(s => s.IsDeleted == false).Select(state => new AdminState
            {
                Id = state.Id,
                FullName = state.FullName
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetFacultyRankList(string name)
        {
            var result = _CommonDataRepository.GetFacultyRankList(name).ToList().Where(f => f.IsDeleted == false).Select(ftyrnk => new AdminFacultyRank
            {
                Id = ftyrnk.Id,
                Rank = ftyrnk.Rank
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetFacultyGenderList(string name)
        {
            var result = _CommonDataRepository.GetFacultyGenderList(name).ToList().Where(f => f.IsDeleted == false).Select(ftygndr => new AdminFacultyGender
            {
                Id = ftygndr.Id,
                Gender = ftygndr.Name
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetFacultyTrackList(string name)
        {
            var result = _CommonDataRepository.GetFacultyTrackList(name).ToList().Where(f => f.IsDeleted == false).Select(ftytrk => new AdminFacultyTrack
            {
                Id = ftytrk.Id,
                FacultyTrack = ftytrk.Name
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetClinicalFtyPathwayList(string name)
        {
            var result = _CommonDataRepository.GetFacultyPathwayList(name).ToList().Where(f => f.IsDeleted == false).Select(ftypthwy => new AdminFacultyPathway
            {
                Id = ftypthwy.Id,
                ClinicalFacultyPathway = ftypthwy.Name
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetFacultyEthnicityList(string name)
        {
            var result = _CommonDataRepository.GetFacultyEthnicityList(name).ToList().Where(e => e.IsDeleted == false).Select(ftyethcity => new AdminFacultyEthnicity
            {
                Id = ftyethcity.Id,
                Ethnicity = ftyethcity.Name
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetFtyFundingRoleList(string name)
        {
            var result = _CommonDataRepository.GetFtyFundingRole(name).ToList().Where(f => f.IsDeleted == false && f.IsTrainingGrantRole == false).Select(ftyrnk => new AdminFundingRole
            {
                Id = ftyrnk.Id,
                Name = ftyrnk.Name
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetFtyTrainingGrantRoleList(string name)
        {
            var result = _CommonDataRepository.GetFtyFundingRole(name).ToList().Where(f => f.IsDeleted == false && f.IsTrainingGrantRole == true).Select(ftyrnk => new AdminFundingRole{
                Id = ftyrnk.Id,
                Name = ftyrnk.Name
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        ////////[OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        ////////public JsonResult GetOrganizationList(string name)
        ////////{
        ////////    var result = _CommonDataRepository.GetOrganization(name).ToList().Where(org => org.IsDeleted == false).Select(org => new AdminOrganization
        ////////    {
        ////////        Id = org.Id,
        ////////        HrName = org.HrName
        ////////        //OrganizationId = org.OrganizationId
        ////////        //OrganizationType = org.OrganizationType.OrganizationType_
        ////////    });
        ////////    return Json(result, JsonRequestBehavior.AllowGet);
        ////////}

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetOrganizationHRName(string name)
        {
            var result = _CommonDataRepository.GetOrganizationType(name).ToList().Where(org => org.IsDeleted == false).Select(org => new AdminOrganization
            {
                Id = org.Id,
                HrName = org.HrName
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //GetOrganizationType
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetOrganizationTypeList(string name)
        {
            var result = _CommonDataRepository.GetOrganizationType(name).ToList().Where(org => org.IsDeleted == false).Select(org => new AdminOrganization{
                Id = org.Id,
                DisplayName = org.DisplayName
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //GetOrganizationHRName
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetOrganizationHRNameList(string name)
        {
            var result = _CommonDataRepository.GetOrganizationType(name).ToList().Where(org => org.IsDeleted == false).Select(org => new AdminOrganization
            {
                Id = org.Id,
                HrName = org.HrName
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetSponsorNameList(string name)
        {
            var result = _CommonDataRepository.GetSponsorNameList(name).ToList().Select(funding => new FundingViewModel{
                //Id = funding.Institution.Id,
                //IntitutionId = funding.Institution.Id,
                SponsorId = funding.Institution_SponsorId.Id,
                Name = funding.Institution_SponsorId.Name
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }      
        [HttpPost]
        public JsonResult AddNewSponsor(NewFundingPostModel model)
        {
            var funding = new Funding 
            {
                Title = model.Name,
                IsDeleted = false
            };
            _context.Set<Funding>().Add(funding);
            _context.SaveChanges();
            return Json(true);
        }


        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetOrganizationList()
        {
            var result = _CommonDataRepository.GetOrganizationList();
            var vm = result.Select(s => new { OrganizationID = s.Id, Organization = s.DisplayName })
                .OrderBy(p => p.Organization).ToList();

            return Json(vm, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetDepartmentList()
        {
            var result = _CommonDataRepository.GetDepartmentList();
            var vm = result.Select(s => new { DepartmentID = s.Id, Department = s.DisplayName })
                .OrderBy(p => p.Department).ToList();
        
            return Json(vm, JsonRequestBehavior.AllowGet);
        }

        
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetDeptList()
        {

            var result = _CommonDataRepository.GetDepartmentList();
            var vm = result.Select(n => (new { Value = n.Id, Name = n.DisplayName })).OrderBy(p => p.Name).ToList();
            return Json(vm, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetFacultyMentorList()
        {
            return Json(_CommonDataRepository.GetFacultyList()
                .Select(s => new { MentorID = s.Id, MentorName = s.FullName })
                    .OrderBy(p => p.MentorName), JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetMenteeTypeList()
        {
            return Json(_CommonDataRepository.GetMenteeTypeList()
                .Select(s => new { MenteeTypeId = s.Id, MenteeType = s.LevelName })
                    .OrderBy(p => p.MenteeType), JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetTrainingGrantList()
        {
            return Json(_CommonDataRepository.GetTrainingGrantList()
                .Select(s => new { TrainingGrantID = s.Id, TrainingGrant = s.Funding.Title })
                    .OrderBy(p => p.TrainingGrant), JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetProgram(string name)
        {
            var result =
                _CommonDataRepository.GetProgram(name).ToList().Select(program => new ProgramViewModel
                {
                    Id = program.Id,
                    Title = program.Title
                });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetProgramList()
        {
            var result =
                _CommonDataRepository.GetProgramList().ToList().Select(program => new ProgramViewModel
                {
                    Id = program.Id,
                    Title = program.Title
                });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetFacultyResearchInterestsByFacultyId(Guid facultyId)
        {           
            var facPersonId = _CommonDataRepository.GetPersonIdForFacultyId(facultyId);

            var result = (facPersonId != Guid.Empty) 
                ? _CommonDataRepository.getFacultyResearchInterestsByFacultyId(facultyId)
                    .Select(ri => new InterestInfo { ResearchId = ri.Id, Name = ri.ResearchInterest }).OrderBy(ii => ii.Name)
                    : null;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public JsonResult GetLastGeneratedID(string idType)
        {
            return Json(_CommonDataRepository.GetLastGeneratedID(idType));
            //var idRecord = _CommonDataRepository.GetLastGeneratedID(idType);
            //return Json(idRecord.GeneratedId);
        }

        //Used for PubMED retrievals (need to use pmid to retrieve publications, but most users will only have pmcid)
        //public string GetPMIdByPMCId(string pmcid)
        //{
        //    string pmid = "";
        //    string inputURL = "http://www.ncbi.nlm.nih.gov/pmc/utils/idconv/v1.0/?tool=OSUTraineeTracking&&ids=" + pmcid;
        //    //string inputURL = WebConfigurationManager.AppSettings["PubMED_IDConverterAPIURL"] + pmcid;

        //    XmlReader xmlReader = XmlReader.Create(inputURL);
        //    while (xmlReader.Read())
        //    {
        //        if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name.ToUpper() == "RECORD"))
        //        {
        //            if (xmlReader.HasAttributes)
        //                pmid = (xmlReader.GetAttribute("pmid"));
        //        }
        //    }
            
        //    return pmid;
        //}
        /// <summary>
        /// MVN - 2-19-2015
        /// This method creates a secure/non secure PubMed API request based on the PMCID parameter passed. 
        /// NOTE:  this XML Document has XSI Referencing a Schema location =>this provide the physical location of schema  
        /// </summary>
        /// <param name="pmcid"></param>
        /// <returns></returns>
        public ActionResult GetPubDataByPMCId(string pmcid)
        {
            string inputURL = HTTP_URL + pmcid;
            /* ++++++++++++++++++++++++ TESTING ITEMS ++++++++++++++++++++++++++
                // Testing PMCId = pmc254288
                // Testing with prefix =>pmc2862870
                // Shorter list PMCId = 1169494
            // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
            string pmid = "";
            string title = "";
            string authorList = "";

            try{
                WebRequest request = HttpWebRequest.Create(inputURL);
                using (WebResponse response = request.GetResponse())
                // create a stream to hold the contents of the response->xml
                using (Stream xlmString = response.GetResponseStream()){
                    XmlDocument xmlDoc = new XmlDocument();                             // Create the XmDocument
                    xmlDoc.Load(xlmString);                                             // load URL from the Stream
                    XmlElement source = xmlDoc.DocumentElement;

                    XmlNodeList articaleNodeList = source.GetElementsByTagName("article-id");
                    XmlNodeList articleTitleNode = source.GetElementsByTagName("article-title");
                    XmlNodeList contribNode = source.GetElementsByTagName("contrib");

                    string strSurName = "";
                    string strGivenName = "";

                    //string [,] authorNameconcatenation;
                    // GOOD CODE *******
                    
                    //XmlNodeList testMe = source.GetElementsByTagName("subject");
                    //xmlDataContent = testMe[0].InnerXml;
                    // GOOD CODE END *********************************

                    foreach (XmlElement elem in articaleNodeList){
                        if (elem.HasAttributes){
                            var strFirstAttribute = elem.GetAttribute("pub-id-type");
                            if (strFirstAttribute.ToUpper() == "PMID"){
                                pmid = elem.InnerText;
                            }                        
                        }
                    }                 
                    title = articleTitleNode[0].InnerText;
                    foreach (XmlElement authorSurNameElm in contribNode){
                        if (authorSurNameElm.HasAttributes){
                            var strContrib = authorSurNameElm.GetAttribute("contrib-type");
                            if (strContrib.ToUpper() == "AUTHOR"){
                                XmlNodeList surName = authorSurNameElm.GetElementsByTagName("surname");
                                for (int i = 0; i < surName.Count; i++){
                                    string sName = surName[i].InnerText;
                                    strSurName = sName;
                                }

                                XmlNodeList givenName = authorSurNameElm.GetElementsByTagName("given-names");
                                for (int i = 0; i < givenName.Count; i++){
                                    string gName = givenName[i].InnerText;
                                    strGivenName = gName;
                                }
                            }                        
                        }
                        authorList += strSurName +", " + strGivenName +"<br>";
                    }
                    
                }
            }
            catch (XmlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            //var menteePublications = new MenteePublicationsViewModel
            //{
            //    PersonId = currMentee.PersonId,
            //    MenteePublications = currMentee.Person.Publications_AuthorId.Where(p => p.IsDeleted == false)
            //    .Select(p => new MenteePublicationViewModel
            //    {
            //        Id = p.Id,
            //        PMCId = p.Pmcid,
            //        PMId = p.Pmid,
            //        IsMenteeFirstAuthor = p.IsMenteeFirstAuthor,
            //        IsAdvisorCoAuthor = p.IsAdvisorCoAuthor,
            //        AuthorList = p.AuthorList,
            //        Title = p.Title,
            //        Journal = p.Journal,
            //        DateEpub = p.DateEpub,
            //        Doi = p.Doi,
            //        //DatePublicationLastUpdated = p.DatePublicationLastUpdated,
            //        YearPublished = p.YearPublished,
            //        Volume = p.Volume,
            //        Issue = p.Issue,
            //        Pagination = p.Pagination,
            //        Citation = p.Citation,
            //        Abstract = p.Abstract
            //    })
            //};
            var pubMedSearch = new MenteePublicationViewModel
            {
                PMCId = pmcid,
                PMId = pmid,
                Title = title,
                AuthorList = authorList
            };
            return new JsonCamelCaseResult(pubMedSearch, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult GetPubDataByPMCId(string title)
        //{
        //    var args = {

        //               };

        //    var pubMedSearch = new MenteePublicationViewModel
        //    {
        //        //PMCId = pmcid,
        //        //PMId = pmid,
        //        Title = title,
        //        //AuthorList = authorList
        //    };
        //    return new JsonCamelCaseResult(pubMedSearch, JsonRequestBehavior.AllowGet);
        //}


        /// <summary>
        /// MVN - 2/20/2015 
        /// THIS snippet function - uses XmlTexWriter and essentially formate the xmlDataContent string 
        /// Keep this for future debugging ref 
        /// </summary>
        /// <param name="XML"></param>
        /// <returns></returns>
        //public static String PrintXML(String XML)
        //{
        //    String Result = "";

        //    MemoryStream mStream = new MemoryStream();
        //    XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode);
        //    XmlDocument document = new XmlDocument();
        //    try{
        //        // Load the XmlDocument with the XML.
        //        document.LoadXml(XML);

        //        writer.Formatting = Formatting.Indented;

        //        // Write the XML into a formatting XmlTextWriter
        //        document.WriteContentTo(writer);
        //        writer.Flush();
        //        mStream.Flush();

        //        // Have to rewind the MemoryStream in order to read
        //        // its contents.
        //        mStream.Position = 0;

        //        // Read MemoryStream contents into a StreamReader.
        //        StreamReader sReader = new StreamReader(mStream);

        //        // Extract the text from the StreamReader.
        //        String FormattedXML = sReader.ReadToEnd();

        //        Result = FormattedXML;
        //    }
        //    catch (XmlException){
        //    }
        //    mStream.Close();
        //    writer.Close();
        //    return Result;
        //}
    }
}
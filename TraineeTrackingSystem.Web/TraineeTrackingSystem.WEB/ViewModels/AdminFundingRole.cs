﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AdminFundingRole
    {
        public int Id { get; set; } // FacultyRankId
        public string Name { get; set; } // Rank
        public int? TotalFundingRoleUsage { get; set; } // TotalFundingRoleUsage      

        public int TotalTrainingGrantRoleUsage { get; set; }
    }
}
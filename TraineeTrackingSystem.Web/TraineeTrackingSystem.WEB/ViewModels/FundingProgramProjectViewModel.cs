﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class FundingProgramProjectViewModel
    {
        public Guid ProjectId { get; set; }        

        public string PIFullName { get; set; }
        public string Title { get; set; }
        public DateTime? DateStarted { get; set; }
        public string DateStartedText { get; set; }
        public DateTime? DateEnded { get; set; }
        public string DateEndedText { get; set; }


        public DateTime? DateLastUpdated { get; set; }

        public string LastUpdatedBy { get; set; }

        public string StrDateLastUpdated { get; set; }

        public string StrLastUpdatedBy { get; set; }
    }
}
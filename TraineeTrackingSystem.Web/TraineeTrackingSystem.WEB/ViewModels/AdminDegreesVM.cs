﻿using System;
using System.Collections.Generic;


namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AdminDegreesVM
    {
        public int? SelectedId { get; set; }
        public string selectedDegree { get; set; }
        public IEnumerable<AdminDegreeVM> DegreeInformation { get; set; }
    }
}
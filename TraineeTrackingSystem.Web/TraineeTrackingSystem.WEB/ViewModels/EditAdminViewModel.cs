﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class EditAdminViewModel
    {
        public InstitutionsViewModel InstitutionSponsorInfo { get; set; }
        public AdminDegreesVM AcademicDegrees { get; set; }
        public AdminCountries CountryiesInfo { get; set; }
        public AdminStates StateProvineInfo { get; set; }
        public TypeOfOrganizations TypeOfOrgInfo { get; set; }
        public AdminPrograms ProgramInfo { get; set; }
        public AdminFacultyRanks FacultyRankInfo { get; set; }
        public AdminFacultyEthnicitys FacultyEthnicityInfo { get; set; }
        public AdminFundingRoles FundingRoleInfo { get; set; }

        public AdminFundingRoles TrainingGrantRoleInfo { get; set; }

        public AdminOrganizations OrganizationInfo { get; set; }

        public AdminFacultyGenders FacultyGenderInfo { get; set; }
        public AdminFacultyTracks FacultyTrackInfo { get; set; }
        public AdminFacultyPathways FacultyPathwayInfo { get; set; }
    }
}
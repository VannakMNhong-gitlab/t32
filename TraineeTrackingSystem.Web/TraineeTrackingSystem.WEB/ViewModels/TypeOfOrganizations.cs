﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class TypeOfOrganizations
    {
        public int? SelectedId { get; set; }
        public string selectedOrganizationType { get; set; }
        public IEnumerable<KeyValuePair<int, String>> OrganizationTypes { get; set; }
        public IEnumerable<TypeOfOrganization> TypeOfOrganizationInformation { get; set; }
    }
}
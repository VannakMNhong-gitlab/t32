﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class FacultySupportViewModel
    {
        public Guid Id { get; set; }         //FundingFaculty Id
        public Guid FundingId { get; set; } // FundingId 
        public Guid FacultyId { get; set; } // PIPersonId       
        public string FacultyFullName { get; set; } // FullName
       
        public int? FacultyRoleId { get; set; }
        public string FacultyRole { get; set; }
        //MVN - commented these out, as these properties are not used any where -> sending null to the UI
        //public virtual ICollection<Guid> Role { get; set; } // Many to many mapping
        //public IEnumerable<KeyValuePair<Guid?, string>> GetFullName { get; set; }
        // For Training Grants only
        public int? FacultySecondaryRoleId { get; set; }
        public string FacultySecondaryRole { get; set; }

        public int FacultyPrimaryOrgId { get; set; }
        public string FacultyPrimaryOrg { get; set; }

        public virtual IEnumerable<int> SelectedResearchInterests { get; set; }
        public virtual IEnumerable<InterestInfo> SelectedFacultyAllInterests { get; set; }


        //MVN - added these for the grid selection and include back to the dropdown
        public IEnumerable<KeyValuePair<Guid?, string>> FundingFacultiesList { get; set; }                  // Faculty Dropdown
        public IEnumerable<KeyValuePair<int, string>> RoleList { get; set; }                                // Roles DropdownList

        // MVN - added to track user last updated info
        public DateTime? DateLastUpdated { get; set; }
        public string StrDateLastUpdated { get; set; }  //the date will be formatted
        public string LastUpdatedBy { get; set; }
        //public string StrDateLastUpdated { get; set; }  //the date will be formatted
        public string StrLastUpdatedBy { get; set; } // LastUpdatedBy
    }

    public class InterestInfo
    {
        public int ResearchId { get; set; }
        public string Name { get; set; }
    }
}

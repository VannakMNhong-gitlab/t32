﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AwardInformationViewModel
    {
        public Guid Id { get; set; }        // This will be used as reference to the TrainingGrant Id
        public Guid FundingId { get; set; } // FundingId =>used in self.fundingId data-bind 
        public string SponsorAwardNumber { get; set; } // SponsorAwardNumber
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded
        public DateTime? DateLastUpdated { get; set; }
        public string StrDateLastUpdated { get; set; }  //the date will be formatted
        public string LastUpdatedBy { get; set; }

        public string StrLastUpdatedBy { get; set; }
    }
}
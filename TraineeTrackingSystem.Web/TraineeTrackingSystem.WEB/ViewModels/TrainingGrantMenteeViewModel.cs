﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class TrainingGrantMenteeViewModel
    {

        public Guid TrainingGrantId { get; set; }
        public Guid MenteeId { get; set; }
        public Guid MenteePersonId { get; set; }
        public string StudentId { get; set; }
        public string MenteeFullName { get; set; }
        public int DoctoralLevelId { get; set; }
        public string DoctoralLevel { get; set; }
        public int? YearStarted { get; set; }
        public int? YearEnded { get; set; }
        public string MentorFullName { get; set; }

        //public Guid? ProgramId { get; set; } // ProgramId
        //public string ProgramListFlat { get; set; }

        //public int? DepartmentId { get; set; }
        //public string DepartmentName { get; set; }

    }
}
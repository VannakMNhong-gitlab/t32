﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    /// <summary>
    /// MVN - data binding model - to be used for loading UI grid elements
    /// </summary>
    public class AdminFacultyTracks
    {
        public int selectedId { get; set; } // Id (Primary key)
        public string selectedTrack { get; set; } // Name
        public IEnumerable<AdminFacultyTrack> AdminFacultyTrackInformation { get; set; }
    }
}
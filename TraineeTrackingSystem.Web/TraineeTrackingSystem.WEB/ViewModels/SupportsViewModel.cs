﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class SupportsViewModel
    {
        public Guid Id { get; set; } // Id (Primary key)

        public Guid? SelectedId { get; set; } // Id (Primary key)

        //public IEnumerable<KeyValuePair<Guid, string>> FacultyList { get; set; }
        ////public IEnumerable<KeyValuePair<Guid, string>> Mentors { get; set; }
        //public IEnumerable<KeyValuePair<int, String>> Countries { get; set; }
        //public IEnumerable<KeyValuePair<int, String>> States { get; set; }
        //public IEnumerable<TrainingGrantViewModel> PreviousTrainingGrants { get; set; }   

        public string SelectedTitle { get; set; }
        public string SelectedSupportType { get; set; }
        public string SelectedSupportStatus { get; set; }
        public string SelectedSupportNumber { get; set; }
        public string SelectedSupportOrganization { get; set; }
        public DateTime? SelectedDateStarted { get; set; }
        public DateTime? SelectedDateEnded { get; set; }

    }
}
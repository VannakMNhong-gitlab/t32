﻿using TraineeTrackingSystem.Web.PostModels;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class CountryViewModel : NewCountryPostModel
    {
        public int Id { get; set; }
    }
}
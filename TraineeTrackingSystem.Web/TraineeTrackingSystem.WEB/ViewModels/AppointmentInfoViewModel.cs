﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.ViewModels
{
    /// <summary>
    /// MVN - this will be used in the controller grid setting 
    /// </summary>
    public class AppointmentInfoViewModel
    {
        public int Id { get; set; } // Id (Primary key)       
        public int InstitutionId { get; set; } // InstitutionId
        public string InstName { get; set; }
        public int? FacultyTrackId { get; set; } // FacultyTrackId
        public string FacultyTrack { get; set; }
        public IEnumerable<KeyValuePair<int, String>> FtyTrksDrpdwnDatasource { get; set; }
        public int? FacultyRankId { get; set; } // FacultyRankId
        public string FacultyRank { get; set; }
        public IEnumerable<KeyValuePair<int, String>> RankDrpdwnDatasource { get; set; }
        public Guid PersonId { get; set; } // PersonId
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded
        public string DateStartedText { get; set; }
        public string DateEndedText { get; set; }
        public int? ClinicalFacultyPathwayId { get; set; } // ClinicalFacultyPathwayId
        public string ClinicalFacultyPathway { get; set; } // ClinicalFacultyPathway
        public IEnumerable<KeyValuePair<int, String>> ClnlFtyPthwyDrpdwnDatasource { get; set; }
        public string OtherAffiliations { get; set; } // OtherAffiliations
        public string OtherTitles { get; set; } // OtherTitles
        public Decimal? PercentFte { get; set; } // PercentFTE
        //public DateTime? DateStarted { get; set; } // DateStarted
        //public DateTime? DateEnded { get; set; } // DateEnded
        public string Comment { get; set; } // Comment
        public string StrDateLastUpdated { get; set; }
        public string StrLastUpdatedBy { get; set; }
    }
}
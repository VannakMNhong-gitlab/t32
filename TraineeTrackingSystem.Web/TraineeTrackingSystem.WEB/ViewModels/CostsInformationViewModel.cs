﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class CostsInformationViewModel
    {
        public Guid Id { get; set; }        // This will be used as reference to the TrainingGrant Id
        public Guid FundingId { get; set; } // PK FundingId 
        public IEnumerable<CostInformationViewModel> CostInformationGridView { get; set; } //TO be used in Grid List
        public string selectedId { get; set; }   //current grid selection                
        public int? selectedCostYear { get; set; } // CostYear Funding.Id = FundingDirectCost.FundingId
        public DateTime SelectedDateStarted { get; set; }
        public DateTime SelectedDateEnded { get; set; }
        public Decimal? selectedCurrentYearDirectCosts { get; set; } // CurrentYearDirectCosts Funding.Id = FundingDirectCost.FundingId
        public Decimal? selectedTotalDirectCosts { get; set; } // TotalDirectCosts Funding.Id = FundingDirectCost.FundingId
        public DateTime? DateLastUpdated { get; set; }
        public string StrDateLastUpdated { get; set; }  //the date will be formatted
        public string LastUpdatedBy { get; set; }
        //public IEnumerable<KeyValuePair<int, string>> IsCurrentDataSource { get; set; }
        //public IEnumerable<KeyValuePair<Guid, string>> Statuses { get; set; }
        //public virtual IEnumerable<Guid> Statuses { get; set; }
        public IEnumerable<KeyValuePair<int, string>> BudgetPeriodStatuses { get; set; }
        //public int? BudgetPeriodStatusId { get; set; } // FundingStatusId
        //public bool IsCurrent { get; set; } // IsCurrent => default is false =>0
        public int fundingDirectCostId { get; set; }
    }
}
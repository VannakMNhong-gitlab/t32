﻿using System;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class ContactEmailViewModel:PersonVm
    {

        public string EmailAddress { get; set; } // EmailAddress
        public int ContactEntityTypeId { get; set; } // ContactEntityTypeId
        public bool IsPrimary { get; set; } // IsPrimary
    }
}
﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class CountryAdminPostModel
    {
        public int? Id { get; set; } // CountryId
        public string Country { get; set; } // Name 
        public int? TotalCountryUsage { get; set; }
        public IEnumerable<KeyValuePair<int, String>> Countries { get; set; }
    }
}
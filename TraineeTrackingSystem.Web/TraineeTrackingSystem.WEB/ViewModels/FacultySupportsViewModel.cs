﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class FacultySupportsViewModel
    {
        public Guid Id { get; set; }        /// currently uses as TrainingGrants Id
        public Guid FacultySupportId { get; set; }
        public string selectedId { get; set; }   //current grid selection 
        public Guid FundingId { get; set; } // FundingId       
        public IEnumerable<KeyValuePair<Guid?, string>> FundingFacultiesList { get; set; }                  // Faculty Dropdown
        public IEnumerable<KeyValuePair<int, string>> RoleList { get; set; }                                // Roles DropdownList
        public IEnumerable<FacultySupportViewModel> FacultySupportGridView { get; set; }            // Faculty Support GRID List 
        //public Guid? selectedFacultyId { get; set; }
        public Guid selectedFacultyId { get; set; }
        public string selectedFacultyFullName { get; set; } // FullName
        public int selectedFacultyRoleId { get; set; }
        //public int selectedFacultyRoleId { get; set; }
        public string selectedFacultyRole { get; set; }


        // For Training Grants only
        public IEnumerable<KeyValuePair<int, string>> FacultyInterests { get; set; }
        
        public int selectedFacultySecondaryRoleId { get; set; }
        public string selectedSecondaryFacultyRole { get; set; }
        public int selectedFacultyPrimaryOrgId { get; set; }
        public string selectedFacultyPrimaryOrg { get; set; }
        //TODO: >>>>>>>
        //public DateTime? DateLastUpdated { get; set; }
        //public string StrDateLastUpdated { get; set; }  //the date will be formatted
        //public string LastUpdatedBy { get; set; }
    }
}
﻿using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class EditFacultyViewModel
    {
        public FacultyGeneralViewMoel PersonGeneralViewMoel { get; set; }
        public ICollection<FacultyAcademicData> AcademicData { get; set; }
        public FacultyResearchInterestsViewModel FacultyResearchInterests { get; set; }
        public ICollection<Faculty> FacultyGeneralViewModel { get; set; }
        public AcademicDegreesViewModel DegreeInformations { get; set; }
        public MenteeMentorsViewModel MenteesGridView { get; set; }  //For Grid in MVVM pattern
        public InstitutionTrainingGrants FundingGridView { get; set; }
        public OtherFundingsInfo OtherGridView { get; set; }
        public AppointmentsInfoViewModel ApointmentInformations { get; set; }
        public AcademicsInfoViewModel AcademicInformations { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class InstitutionTrainingGrant
    {
        public Guid TrainingGrantId { get; set; } // TrainingGrantId
        public Guid? FacultyPersonId { get; set; } // FacultyPersonId
        public string FacultyPrimaryRole { get; set; } // FacultyPrimaryRole
        public string Title { get; set; } // Title

        public int SponsorId { get; set; } // InstitutionId
        public string SponsorName { get; set; }
     
        public string SponsorAwardNumber { get; set; } // SponsorAwardNumber
        public Guid FundingId { get; set; } // FundingId
        public DateTime? DateTimeFundingStarted { get; set; } // DateTimeFundingStarted
        public string StrDateTimeFundingStarted { get; set; }  //the date will be formatted
        public DateTime? DateTimeFundingEnded { get; set; } // DateTimeFundingEnded
        public string StrDateTimeFundingEnded { get; set; }  // the date will be formatted
        public string PDFullName { get; set; }

        public string FundingStatus { get; set; }

        public string TrainingGrantStatus { get; set; }
    }
}
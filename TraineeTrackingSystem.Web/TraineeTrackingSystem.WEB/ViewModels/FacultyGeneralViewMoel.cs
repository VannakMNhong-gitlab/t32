﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class FacultyGeneralViewMoel:PersonVm
    {
        public string EmployeeId { get; set; }
        /// MVN - 2-19-2016 these properties are no longer needed
        ///public int RankId { get; set; }
        ///public IEnumerable<KeyValuePair<int, String>> Ranks { get; set; }
        ///
        public Guid PersonId { get; set; } // PersonId
        public int PositionDepartmentId { get; set; }
        public IEnumerable<KeyValuePair<int, String>> PositionDepartments { get; set; }
        public int PositionOrganizationId { get; set; }
        public IEnumerable<KeyValuePair<int, String>> PositionOrganizations { get; set; }
        public Guid? MentorId { get; set; } // MentorId
        public IEnumerable<KeyValuePair<Guid, String>> PositionMentors { get; set; }
        public int? GenderId { get; set; } // PersonId
        //public int? GenderId { get; set; }
        public IEnumerable<KeyValuePair<int?, String>> PositionGenders { get; set; }
        //public string PositionGenders { get; set; }
        //public string BudgetPeriodStatus { get; set; }
        public int? GenderAtBirthId { get; set; }
        public IEnumerable<KeyValuePair<int?, String>> PositionGenderAtBirthes { get; set; }
        public int? EthnicityId { get; set; }
        public IEnumerable<KeyValuePair<int?, String>> PositionEthnicities { get; set; }
        public int OrganizationId { get; set; }
        public IEnumerable<KeyValuePair<int, String>> Organization { get; set; }
        public DateTime? DateLastUpdated { get; set; }
        public string StrDateLastUpdated { get; set; }  //the date will be formatted
        public string LastUpdatedBy { get; set; }      
        // FROM APPLICANT
        public int DepartmentId { get; set; }
        public IEnumerable<KeyValuePair<int, String>> Department { get; set; }

        public IEnumerable<KeyValuePair<Guid, string>> Programs { get; set; }
        public virtual IEnumerable<Guid> SelectedPrograms { get; set; }
        public Guid EnteredYearId { get; set; }
        public IEnumerable<KeyValuePair<int, string>> EnteredYear { get; set; }
        public int? DegreeEnteredYear { get; set; }
        public int SecondOrgId { get; set; }
        public IEnumerable<KeyValuePair<int, String>> SecondOrg { get; set; }
        
        public int TIUId { get; set; }

        public IEnumerable<KeyValuePair<int, String>> TIU { get; set; }
        public string OtherAffiliations { get; set; }
        public string OtherTitles { get; set; }

        // added for Applicants
        public int? SelectedId { get; set; }
        public int? doctoralLevel { get; set; } 
        //public string LevelName { get; set; } // LevelName

        //public bool IsTrainingGrantEligible { get; set; }
        //public int? IsUnderrepresentedMinority { get; set; }
        //public int? IsIndividualWithDisabilities { get; set; }
        //public int? IsFromDisadvantagedBkgd { get; set; }
    }
}
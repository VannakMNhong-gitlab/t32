﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AdminFacultyRanks
    {
        public int? SelectedId { get; set; }
        public string selectedFtyRank { get; set; }
        public IEnumerable<AdminFacultyRank> AdminFacultyRankInformation { get; set; }
    }
}
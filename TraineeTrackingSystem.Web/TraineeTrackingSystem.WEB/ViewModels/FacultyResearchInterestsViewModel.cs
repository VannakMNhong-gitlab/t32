﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class FacultyResearchInterestsViewModel
    {
        public Guid PersonId { get; set; }
        public int? SelectedId { get; set; }

        public string SelectedResearchInterest { get; set; }

        public IEnumerable<FacultyResearchInterestViewModel> FacultyResearchInterests { get; set; }
    }
}
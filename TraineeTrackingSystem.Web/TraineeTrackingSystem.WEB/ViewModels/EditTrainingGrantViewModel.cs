﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class EditTrainingGrantViewModel
    {
        public TrainingGrantViewModel TrainingGrantGeneralInfo { get; set; }

        public FacultySupportsViewModel ParticipatingFaculty { get; set; }
        public TrainingGrantApplicantsViewModel ApplicantPool { get; set; }
        public TrainingGrantMenteesViewModel FacultyMentees { get; set; }
        public InstitutionTrainingGrants TrainingGrantSupportGrants { get; set; }
        public OtherFundingsInfo TrainingGrantSupportOther { get; set; }
        public TrainingGrantsViewModel PreviousTrainingGrant { get; set; }


        public ICollection<TrainingGrant> TrainingGrantViewModel { get; set; }
        public AwardInformationViewModel AwardInfo { get; set; }
        public CostsInformationViewModel CostInfo { get; set; }
        public TraineesSummaryInformationViewModel PreDocTraineeSummaryInfo { get; set; }
        public TraineesSummaryInformationViewModel PostDocTraineeSummaryInfo { get; set; }
        public TraineesDetailedInformationViewModel TraineeDetailedInfo { get; set; }
    }
}
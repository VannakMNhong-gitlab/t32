﻿namespace TraineeTrackingSystem.Web.ViewModels
{
    public class EditFundingViewModel
    {
        public FundingsViewModel FundingInformations { get; set; }
        public FundingViewModel FundingGeneralViewModel { get; set; }
        public FacultySupportsViewModel FacultySupportGridView { get; set; }
        public FundingProgramProjectsViewModel ProgramProjectsGridView { get; set; }
        public FundingSubAwardViewModel SubAwardGridView { get; set; }
        public AwardInformationViewModel AwardInformationView { get; set; }
        public CostsInformationViewModel CostInformationGridView { get; set; }
    }
}

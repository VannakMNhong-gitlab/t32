﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    /// <summary>
    /// MVN - data binding model - used for the loading UI grid 
    /// </summary>
    public class AdminFacultyGenders
    {
        public int? selectedId { get; set; } // GenderId
        public string selectedGender { get; set; } // Gender
        //public IEnumerable<AdminFacultyGender>

        public IEnumerable<AdminFacultyGender> AdminFacultyGenderInformation { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    /// <summary>
    /// MVN - this will be used for the Grid UI selection View Model
    /// </summary>
    public class AppointmentsInfoViewModel
    {
        public int? selectedId { get; set; } // Id (Primary key)
        public Guid PersonId { get; set; } // PersonId
        public int InstitutionId { get; set; } // InstitutionId
        public int selectedInstName { get; set; }
        public IEnumerable<KeyValuePair<int, String>> Institutions { get; set; }

        public int? FacultyTrackId { get; set; } // FacultyTrackId
        public int selectedFtyTrack { get; set; }
        public IEnumerable<KeyValuePair<int, String>> FtyTrksDrpdwnDatasource { get; set; }
        public int? ClinicalFacultyPathwayId { get; set; } // ClinicalFacultyPathwayId
        public int selectedClinicalFacultyPathway { get; set; }
        public IEnumerable<KeyValuePair<int, String>> ClnlFtyPthwyDrpdwnDatasource { get; set; }
        public int? FacultyRankId { get; set; } // FacultyRankId
        public int selectedRank { get; set; }
        public IEnumerable<KeyValuePair<int, String>> RankDrpdwnDatasource { get; set; }
        public string selectedOtherAffiliations { get; set; } // OtherAffiliations
        public string selectedOtherTitles { get; set; } // OtherTitles
        //public Decimal? PercentFte { get; set; } // PercentFTE
        public string selectedPercentFte { get; set; }
        public DateTime selectedDateStarted { get; set; }
        public DateTime selectedDateEnded { get; set; }
        //public DateTime? DateStarted { get; set; } // DateStarted
        //public DateTime? DateEnded { get; set; } // DateEnded
        public string selectedComment { get; set; } // Comment
        //public IEnumerable<AcademicDegreeViewModel> DegreeInformations { get; set; }
        public IEnumerable<AppointmentInfoViewModel> AppointmentInformations { get; set; }


        
    }
}
﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class MenteeSupportsViewModel
    {
        public Guid PersonId { get; set; }  // PersonId of the Mentee

        //public IEnumerable<KeyValuePair<int, String>> SupportSources { get; set; }
        public IEnumerable<MenteeSupportViewModel> MenteeSupports { get; set; }
        public IEnumerable<KeyValuePair<int, String>> Countries { get; set; }
        public IEnumerable<KeyValuePair<int, String>> States { get; set; }

        public int SelectedId { get; set; }
        
        public string SelectedInstitution { get; set; }

        public DateTime? SelectedDateStarted { get; set; } // DateStarted
        public DateTime? SelectedDateEnded { get; set; } // DateEnded
        public string SelectedSupportSourceText { get; set; } // SupportSourceText

        //public int SelectedSupportSourceId { get; set; }
        public DateTime? DateLastUpdated { get; set; }
        public string StrDateLastUpdated { get; set; }  //the date will be formatted
        public string LastUpdatedBy { get; set; }
        public int Id { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class WorkHistoriesViewModel
    {
        public Guid PersonId { get; set; }  // PersonId of the Mentee

        public IEnumerable<WorkHistoryViewModel> TraineePositions { get; set; }
        public IEnumerable<KeyValuePair<int, String>> Countries { get; set; }
        public IEnumerable<KeyValuePair<int, String>> States { get; set; }

        public int SelectedId { get; set; }
        
        public string SelectedInstitution { get; set; }
        public string SelectedPositionTitle { get; set; }
        public DateTime SelectedDateStarted { get; set; }
        public DateTime SelectedDateEnded { get; set; }
        public string SelectedSupportSourceText { get; set; } // SupportSourceText

        public DateTime? DateLastUpdated { get; set; }
        public string StrDateLastUpdated { get; set; }  //the date will be formatted
        public string LastUpdatedBy { get; set; }
    }
}
﻿using TraineeTrackingSystem.Web.PostModels;
using System;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AreaOfStudyViewModel : NewAreaOfStudyPostModel
    {
        public Guid Id { get; set; } // Id (Primary key)
    }
}
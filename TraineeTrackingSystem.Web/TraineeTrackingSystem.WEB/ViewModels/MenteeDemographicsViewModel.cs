﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class MenteeDemographicsViewModel:PersonVm
    {
        public System.Guid PersonId { get; set; }
        public System.Guid MenteeId { get; set; }
        public string MenteeStudentId { get; set; }
        
        public bool IsTrainingGrantEligible { get; set; }
        public int? IsUnderrepresentedMinority { get; set; }
        public int? IsIndividualWithDisabilities { get; set; }
        public int? IsFromDisadvantagedBkgd { get; set; }

    }
}
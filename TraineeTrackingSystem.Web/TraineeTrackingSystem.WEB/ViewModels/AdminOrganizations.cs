﻿using System;
using System.Collections.Generic;
namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AdminOrganizations
    {
        public int? SelectedId { get; set; }
        public string selectedOrganization { get; set; }
        public string selectedOrganizationType { get; set; }
        public string selectedDisplayName { get; set; }
        public string selectedOrganizationHRName { get; set; }
        public IEnumerable<AdminOrganization> AdminOrganizationInformation { get; set; }
        /// <summary>
        ///  public IEnumerable<KeyValuePair<int, String>> Countries { get; set; }
        /// </summary>

        public IEnumerable<KeyValuePair<int, String>> OrganizationTypes { get; set; }
    }
}
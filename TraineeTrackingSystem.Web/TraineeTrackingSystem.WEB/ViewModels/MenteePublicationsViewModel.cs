﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class MenteePublicationsViewModel
    {
        public Guid PersonId { get; set; }
        public Guid SelectedId { get; set; }
        public string SelectedAuthorid { get; set; }
        public DateTime? SelectedDateEntered { get; set; }
        // FROM Publication table - these use in the UI binding to the data-bind value
        public string SelectedPmcid { get; set; } // PMCID
        public string SelectedPMId { get; set; }
        //public bool SelectedIsMenteeFirstAuthor { get; set; } // UI RADIO button IsMenteeFirstAuthor
        //public bool SelectedIsAdvisorCoAuthor { get; set; } // UI RADIO Button IsAdvisorCoAuthor
        //public int IsMenteeFirstAuthor { get; set; } // IsMenteeFirstAuthor
        //public int IsAdvisorCoAuthor { get; set; } // IsAdvisorCoAuthor
        public string SelectedIsMenteeFirstAuthor { get; set; } // UI RADIO button IsMenteeFirstAuthor
        public string SelectedIsAdvisorCoAuthor { get; set; } // UI RADIO Button IsAdvisorCoAuthor
        public string SelectedAuthorList { get; set; } // RTF AuthorList
        public string SelectedTitle { get; set; } // Title
        public string SelectedJournal { get; set; } // Journal
        public DateTime? SelectedDateEpub { get; set; } // DateEpub
        public string SelectedDoi { get; set; } // DOI
        public DateTime? SelectedDatePublicationLastUpdated { get; set; } // DatePublicationLastUpdated TOBEREPLACE by yearPublish
        public int? SelectedYearPublished { get; set; } // YearPublished
        public string SelectedVolume { get; set; } // Volume
        public string SelectedIssue { get; set; } // Issue
        public string SelectedPagination { get; set; } // Pagination
        public string SelectedCitation { get; set; }
        public string SelectedAbstract { get; set; } // Abstract
        public IEnumerable<MenteePublicationViewModel> MenteePublications { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class ApplicantOSUTimeLineViewModel
    {
        public Guid Id { get; set; }
        public Guid PersonId { get; set; } // PersonId
        public bool WasInterviewed { get; set; } // WasInterviewed
        public bool AcceptedOffer { get; set; } // AcceptedOffer
        public bool WasEnrolled { get; set; } // WasEnrolled
    }
}

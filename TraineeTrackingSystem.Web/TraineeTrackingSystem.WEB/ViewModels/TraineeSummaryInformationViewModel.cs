﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class TraineeSummaryInformationViewModel
    {
        // Grid data **************************************************************************************
        public Guid TraineeSummaryId { get; set; } // This replace Id bellow to reduce some confusion 
        //public Guid Id { get; set; } // Id (Primary key) MVN 6/17/2015 => no longer needed 
        public Guid TrainingGrantId { get; set; } // TrainingGrantId       
        public DateTime? DateStarted { get; set; } // DateStarted
        public string DateStartedText { get; set; }  // Formatted DateStarted for grid
        public DateTime? DateEnded { get; set; } // DateEnded
        public string DateEndedText { get; set; }  // Formatted DateEnded for grid
        public int? PositionsAwarded { get; set; } // PositionsAwarded
        public int? TraineesAppointed { get; set; } // TraineesAppointed
        public int? SupportMonthsUsed { get; set; } // SupportMonthsUsed
        public int? UrmTraineesAppointed { get; set; } // URMTraineesAppointed
        public int? DisabilitiesTraineesAppointed { get; set; } // DisabilitiesTraineesAppointed
        public int? DisadvantagedTraineesAppointed { get; set; } // DisadvantagedTraineesAppointed
        // Grid data *************************************************************************************
        public int DoctoralLevelId { get; set; } // DoctoralLevelId
        public int? SupportMonthsAwarded { get; set; } // SupportMonthsAwarded
        public int? UrmSupportMonthsUsed { get; set; } // URMSupportMonthsUsed
        public int? DisabilitiesSupportMonthsUsed { get; set; } // DisabilitiesSupportMonthsUsed
        public int? DisadvantagedSupportMonthsUsed { get; set; } // DisadvantagedSupportMonthsUsed
        public int? NumMdAppointed { get; set; } // NumMDAppointed
        public int? NumMdPhDAppointed { get; set; } // NumMDPhDAppointed
        public int? NumPhDAppointed { get; set; } // NumPhDAppointed
        public int? NumOtherDegreeAppointed { get; set; } // NumOtherDegreeAppointed
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public bool IsDeleted { get; set; } // IsDeleted




        public string StrDateLastUpdated { get; set; }

        public string StrLastUpdatedBy { get; set; }
    }
}
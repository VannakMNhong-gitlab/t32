﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AdminDegreeVM
    {
        public int Id { get; set; }
        public string AcademicDegreeName { get; set; }
        public int? TotalDegreeUsage { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;


namespace TraineeTrackingSystem.Web.ViewModels
{
    /// <summary>
    /// MVN - this will be used for the Grid UI selection View Model
    /// </summary>
    public class AcademicsInfoViewModel
    {
        public int? selectedId { get; set; } // Id (Primary key)
        public Guid FacultyId { get; set; }
        public Guid PersonId { get; set; } // PersonId
        //public int InstitutionId { get; set; } // InstitutionId
        public int selectedInstName { get; set; }
        //public IEnumerable<KeyValuePair<int, String>> Institutions { get; set; }
        public int selectedAcademicDegree { get; set; }
        public DateTime? selectedDateStarted { get; set; } // DateStarted
        public DateTime? selectedDateEnded { get; set; } // DateEnded
        //public string selectedAcademicDegree { get; set; } // Degree
        public DateTime? selectedDateDegreeCompleted { get; set; } // DateDegreeCompleted
        public string selectedAreaOfStudy { get; set; } // AreaOfStudy
        public string selectedResearchProjectTitle { get; set; } // ResearchProjectTitle
        public string selectedComments { get; set; } // Comments
        public IEnumerable<AcademicInfoViewModel> AcademicInformations { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AppSPreDocViewModel
    {
        public Guid Id { get; set; }
        public Guid PersonId { get; set; }
        public Decimal? Gpa { get; set; } // GPA => this maps to GradRecord
        public Decimal? GpaScale { get; set; } // GPAScale       
        public bool WasInterviewed { get; set; } // WasInterviewed
        public bool AcceptedOffer { get; set; } // AcceptedOffer
        public bool WasEnrolled { get; set; } // WasEnrolled
    }
}
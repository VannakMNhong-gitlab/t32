﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class ReportViewModel
    {
        //public ReportGeneralSectionViewModel GeneralSection { get; set; }

        public IEnumerable<KeyValuePair<int, string>> Reports { get; set; }
        public IEnumerable<KeyValuePair<Guid, string>> GrantProposals { get; set; }
        //public IEnumerable<KeyValuePair<Guid, string>> Mentees { get; set; }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public int SelectedReport { get; set; }
        public Guid SelectedGrantProposal { get; set; }
        public int SelectedYear { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class MenteeMentorViewModel
    {
        public Guid? MenteeId { get; set; } // MenteeId
        public Guid? MenteePersonId { get; set; } // MenteePersonId
        public string MenteeFullName { get; set; } // MenteeFullName
        public string MenteeType { get; set; } // MenteeType        
        public int? YearStarted { get; set; } // YearStarted
        public int? YearEnded { get; set; } // YearEnded
        public string CompletedDegree { get; set; } // CompletedDegree
        //public Guid TrainingPeriodId { get; set; } // TrainingPeriodId
        //public Guid? MentorPersonId { get; set; } // MentorPersonId
        //public string InstitutionName { get; set; } // InstitutionName
    }
}
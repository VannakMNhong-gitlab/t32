﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class ReportFundingViewModel
    {

        //public IEnumerable<KeyValuePair<int, string>> Reports { get; set; }
        public IEnumerable<KeyValuePair<Guid, string>> Fundings { get; set; }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public Guid SelectedFunding { get; set; }
        public int SelectedYear { get; set; }
        public bool IsFundingReportFound { get; set; }
    }
}

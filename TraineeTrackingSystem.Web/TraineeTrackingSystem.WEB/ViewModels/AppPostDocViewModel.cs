﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AppPostDocViewModel
    {
        public Guid Id { get; set; }
        public Guid PersonId { get; set; }
        /******************** these default to data schema ***************************/
        public bool? WasOfferedPosition { get; set; } // WasOfferedPosition
        public bool? AcceptedOffer { get; set; } // AcceptedOffer
        public bool? EnteredProgram { get; set; } // EnteredProgram
        /******************* these will force user selection *************************/
        //public bool? WasOfferedPosition { get; set; } // WasOfferedPosition
        //public bool? AcceptedOffer { get; set; } // AcceptedOffer
        //public bool? EnteredProgram { get; set; } // EnteredProgram
    }
}
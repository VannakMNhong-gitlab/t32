﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    /// <summary>
    /// MVN - data binding model to be used in the UI grid
    /// </summary>
    public class AdminFacultyTrack
    {
        public int Id { get; set; } // FacultyTrackId
        public string FacultyTrack { get; set; } // FacultyTrack
        public int? TotalFacultyTrackUsage { get; set; } // TotalFacultyTrackUsage
    }
}
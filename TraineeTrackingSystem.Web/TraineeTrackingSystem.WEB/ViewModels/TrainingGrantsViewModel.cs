﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class TrainingGrantsViewModel
    {
        public Guid Id { get; set; } // Id (Primary key)

        public Guid? SelectedId { get; set; } // Id (Primary key)

        public IEnumerable<KeyValuePair<Guid, string>> FacultyList { get; set; }
        //public IEnumerable<KeyValuePair<Guid, string>> Mentors { get; set; }
        public IEnumerable<KeyValuePair<int, String>> Countries { get; set; }
        public IEnumerable<KeyValuePair<int, String>> States { get; set; }
        public IEnumerable<TrainingGrantViewModel> PreviousTrainingGrants { get; set; }   

        public Guid SelectedFundingId { get; set; } // FundingId

        public string SelectedSponsor { get; set; }
        public string SelectedTitle { get; set; }

        public Guid SelectedPdFacultyId { get; set; }
        public string SelectedPdFullName { get; set; }

        //public int? SelectedDoctoralLevelId { get; set; } // DoctoralLevelId
        public string SelectedDoctoralLevel { get; set; }
        public int? SelectedNumPredocPositionsRequested { get; set; } // NumPredocPositionsRequested
        public int? SelectedNumPostdocPositionsRequested { get; set; } // NumPostdocPositionsRequested
        public int? SelectedNumPredocPositionsAwarded { get; set; } // NumPredocPositionsAwarded
        public int? SelectedNumPostdocPositionsAwarded { get; set; } // NumPostdocPositionsAwarded
        public int? SelectedPredocSupportMonthsAwarded { get; set; } // PredocSupportMonthsAwarded
        public int? SelectedPostdocSupportMonthsAwarded { get; set; } // PostdocSupportMonthsAwarded
        public Guid? SelectedPreviousTrainingGrantId { get; set; } // PreviousTrainingGrantId

        //public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        //public string LastUpdatedBy { get; set; } // LastUpdatedBy


        //public virtual ICollection<FacultyResearchInterest> FacultyResearchInterests { get; set; } // Many to many mapping
        //public virtual ICollection<Organization> Organizations { get; set; } // Many to many mapping
        //public virtual ICollection<Program> Programs { get; set; } // Many to many mapping
        //public virtual ICollection<TrainingGrant> TrainingGrants { get; set; } // TrainingGrant.FK_TrainingGrant_PreviousTrainingGrant
        //public virtual ICollection<TrainingGrantApplicant> TrainingGrantApplicants { get; set; } // TrainingGrantApplicant.FK_TrainingGrantApplicant_TrainingGrant
        //public virtual ICollection<TrainingGrantFunding> TrainingGrantFundings { get; set; } // Many to many mapping
        //public virtual ICollection<TrainingGrantTrainee> TrainingGrantTrainees { get; set; } // TrainingGrantTrainee.FK_TrainingGrantTrainee_TrainingGrant   
        
        
        //public Guid PersonId { get; set; }  // PersonId of the Mentee

        ////public IEnumerable<KeyValuePair<int, String>> SupportSources { get; set; }
        //public IEnumerable<MenteeSupportViewModel> MenteeSupports { get; set; }
        //public IEnumerable<KeyValuePair<int, String>> Countries { get; set; }
        //public IEnumerable<KeyValuePair<int, String>> States { get; set; }

        //public int SelectedId { get; set; }
        
        //public string SelectedInstitution { get; set; }

        //public DateTime? SelectedDateStarted { get; set; } // DateStarted
        //public DateTime? SelectedDateEnded { get; set; } // DateEnded
        //public string SelectedSupportSourceText { get; set; } // SupportSourceText

        ////public int SelectedSupportSourceId { get; set; }
       

    }
}
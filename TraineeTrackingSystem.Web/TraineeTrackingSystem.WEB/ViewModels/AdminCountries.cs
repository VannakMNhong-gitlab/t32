﻿using System;
using System.Collections.Generic;
namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AdminCountries
    {
        public int? SelectedId { get; set; }
        public string selectedCountry { get; set; }
        public IEnumerable<AdminCountry> CountryInformation { get; set; }

        public IEnumerable<KeyValuePair<int, string>> Countries { get; set; }
    }
}
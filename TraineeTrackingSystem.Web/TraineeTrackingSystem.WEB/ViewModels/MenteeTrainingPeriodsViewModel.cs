﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class MenteeTrainingPeriodsViewModel
    {
        public Guid PersonId { get; set; }  // PersonId of the Mentee

        public IEnumerable<KeyValuePair<int, String>> Countries { get; set; }
        public IEnumerable<KeyValuePair<int, String>> States { get; set; }
        public IEnumerable<KeyValuePair<Guid, string>> Programs { get; set; }
        public IEnumerable<KeyValuePair<Guid, string>> Mentors { get; set; }
        public IEnumerable<MenteeTrainingPeriodViewModel> MenteeTrainingPeriods { get; set; }

        public Guid SelectedId { get; set; }
        
        public int? SelectedInstitutionId { get; set; } // InstitutionId
        public int SelectedDoctoralLevelId { get; set; } // DoctoralLevelId
        public int? SelectedYearStarted { get; set; } // YearStarted
        public int? SelectedYearEnded { get; set; } // YearEnded
        public string SelectedDegree { get; set; }
        public Guid? SelectedProgram { get; set; } // ProgramId
        public Guid? SelectedMentor { get; set; }
        public string SelectedResearchProjectTitle { get; set; } // ResearchProjectTitle

        //MVN added to be used in the Associated Training Period, where it accessed the Mentee table
        public Guid Id { get; set; }  // MenteeId
        public Guid? SelectedTrainingPeriodId { get; set; }     //This will be used to validate the trainingPeriod info
        public bool IsTrainingGrantEligible { get; set; }
        public DateTime? DateLastUpdated { get; set; }
        public string StrDateLastUpdated { get; set; }  //the date will be formatted
        public string LastUpdatedBy { get; set; }

        public int DepartmentId { get; set; }

        public IEnumerable<KeyValuePair<int, string>> Departments { get; set; }

        public Guid TrainingPeriodId { get; set; }
    }
}
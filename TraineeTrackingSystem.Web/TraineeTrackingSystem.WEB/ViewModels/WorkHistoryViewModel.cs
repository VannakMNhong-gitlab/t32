﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class WorkHistoryViewModel
    {

        public int Id { get; set; }
        public Guid PersonId { get; set; }  // PersonId of the Mentee
        public int InstitutionId { get; set; } // InstitutionId
        public string InstitutionName { get; set; }
        public int? CityId { get; set; }
        public string CityName { get; set; }
        public int? StateId { get; set; }
        public string StateName { get; set; }
        public int? CountryId { get; set; }
        public string CountryName { get; set; }
        public DateTime? DateStarted { get; set; } // DateStartedPosition
        public string DateStartedText { get; set; }  // Formatted DateStarted for grid
        public DateTime? DateEnded { get; set; } // DateEndedPosition
        public string DateEndedText { get; set; }  // Formatted DateEnded for grid

        public string PositionTitle { get; set; } // PositionTitle
        public string PositionDescription { get; set; } // PositionDescription
        public string PositionLocation { get; set; } // PositionLocation

        public string SupportSourceText { get; set; } // SupportSourceText

        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy

        //public SupportSourcesViewModel PositionSupports { get; set; }



        public string StrDateLastUpdated { get; set; }

        public string StrLastUpdatedBy { get; set; }
    }
}
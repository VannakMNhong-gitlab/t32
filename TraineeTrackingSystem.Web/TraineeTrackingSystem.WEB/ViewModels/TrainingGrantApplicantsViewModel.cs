﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class TrainingGrantApplicantsViewModel
    {
        public Guid TrainingGrantId { get; set; }
        public int? ApplicantPoolAcademicYear { get; set; }

        public int selectedApplicantPoolAcademicYear { get; set; }
        public IEnumerable<TrainingGrantApplicantViewModel> TrainingGrantApplicants { get; set; }

        // FOR PREVIOUS APPLICANTS GRID
        public Guid SelectedApplicantGuid { get; set; }
        public IEnumerable<TrainingGrantApplicantViewModel> SelectedTrainingGrantApplicants { get; set; }

    }
}
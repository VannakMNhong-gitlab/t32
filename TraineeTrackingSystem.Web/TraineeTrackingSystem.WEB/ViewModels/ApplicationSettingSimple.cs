﻿namespace TraineeTrackingSystem.Web.ViewModels
{
    public class ApplicationSettingSimple
    {
        public string ApplicationName { get; set; }
        public string AdminUrl { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
namespace TraineeTrackingSystem.Web.ViewModels
{
    public class TypeOfOrganization
    {
        public int Id { get; set; } // OrganizationTypeId
        public string OrganizationType { get; set; } // OrganizationType
        public IEnumerable<KeyValuePair<int, String>> OrganizationTypes { get; set; }
        public int? TotalOrganization { get; set; } // TotalOrganization
    }
}
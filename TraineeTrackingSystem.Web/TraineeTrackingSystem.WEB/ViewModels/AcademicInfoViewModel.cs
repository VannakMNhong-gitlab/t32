﻿using System;
using System.Collections.Generic;


namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AcademicInfoViewModel
    {
        /// <summary>
        /// MVN - this will be used in the controller grid setting 
        /// </summary>
        public Guid Id { get; set; } // Id (Primary key) - to be used for facultyId
        public Guid PersonId { get; set; }
        public int InstitutionId { get; set; } // InstitutionId
        public string InstName { get; set; }
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded
        public string DateStartedText { get; set; }
        public string DateEndedText { get; set; }
        public int? AcademicDegreeId { get; set; } // AcademicDegreeId
        public string AcademicDegree { get; set; } // Degree
        public DateTime? DateDegreeCompleted { get; set; } // DateDegreeCompleted
        public string AreaOfStudy { get; set; } // AreaOfStudy
        public string ResearchProjectTitle { get; set; } // ResearchProjectTitle
        public string Comments { get; set; } // Comments
        public string StrDateLastUpdated { get; set; }
        public string StrLastUpdatedBy { get; set; }

    }
}
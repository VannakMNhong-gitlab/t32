﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AdminFundingRoles
    {
        public int? SelectedId { get; set; }
        public string selectedFfdRole { get; set; }
        public string selectedTgRole { get; set; }
        public IEnumerable<AdminFundingRole> AdminFundingRoleInformation { get; set; }
        public IEnumerable<AdminFundingRole> AdminTrainingGrantRoleInformation { get; set; }
    }
}
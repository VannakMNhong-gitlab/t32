﻿using System;
using System.Collections.Generic;
namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AdminOrganization
    {
        public int Id { get; set; } // Id (Primary key)
        public string OrganizationId { get; set; } // OrganizationId
        public int OrganizationTypeId { get; set; } // OrganizationTypeId
        public string OrganizationType { get; set; }
        public string DisplayName { get; set; } // DisplayName
        public string HrName { get; set; } // HRName
        public int? ParentOrganizationId { get; set; } // ParentOrganizationId
        public int? TotalOrganizationUsage { get; set; } // TotalOrganizationUsage
        public IEnumerable<KeyValuePair<int, string>> OrganizationTypes { get; set; }
    }
}
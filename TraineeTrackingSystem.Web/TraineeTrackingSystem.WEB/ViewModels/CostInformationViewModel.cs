﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class CostInformationViewModel
    {
        public int? Id { get; set; }            //FundingDirectCost PK Id => to be used in the costInformationGrid        
        public Guid FundingId { get; set; } // PK FundingId 
        public int? CostYear { get; set; } // CostYear Funding.Id = FundingDirectCost.FundingId

        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded

        public Decimal? CurrentYearDirectCosts { get; set; } // CurrentYearDirectCosts Funding.Id = FundingDirectCost.FundingId
        public Decimal? TotalDirectCosts { get; set; } // TotalDirectCosts Funding.Id = FundingDirectCost.FundingId
       
        
        public bool IsCurrent { get; set; } // IsCurrent => default is false =>0
        public int? BudgetPeriodStatusId { get; set; } // FundingStatusId
        //public IEnumerable<KeyValuePair<int, string>> BudgetPeriodStatuses { get; set; }
        public string BudgetPeriodStatus { get; set; }
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string StrDateLastUpdated { get; set; }  //the date will be formatted
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public string StrLastUpdatedBy { get; set; } // LastUpdatedBy
        public string DateStartedText { get; set; }

        public string DateEndedText { get; set; }
    }
}
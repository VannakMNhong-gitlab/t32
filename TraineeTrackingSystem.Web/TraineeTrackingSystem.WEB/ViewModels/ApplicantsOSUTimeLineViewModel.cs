﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class ApplicantsOSUTimeLineViewModel
    {
        public Guid PersonId { get; set; }
        public bool WasInterviewed { get; set; } // WasInterviewed
        public bool AcceptedOffer { get; set; } // AcceptedOffer
        public bool WasEnrolled { get; set; } // WasEnrolled

        public IEnumerable<ApplicantOSUTimeLineViewModel> ApplicantOSUTimeLineInformations { get; set; }
    }
}

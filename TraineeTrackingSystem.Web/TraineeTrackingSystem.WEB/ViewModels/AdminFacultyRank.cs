﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AdminFacultyRank
    {
        public int Id { get; set; } // FacultyRankId
        public string Rank { get; set; } // Rank
        public int? TotalFacultyRankUsage { get; set; } // TotalFacultyRankUsage
    }
}
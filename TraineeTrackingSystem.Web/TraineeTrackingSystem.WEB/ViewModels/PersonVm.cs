﻿using System;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class PersonVm
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

    }
}
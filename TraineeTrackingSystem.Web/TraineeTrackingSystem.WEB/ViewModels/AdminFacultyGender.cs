﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    /// <summary>
    /// MVN - data binding model to be used in the grid 
    /// </summary>
    public class AdminFacultyGender
    {
        public int Id { get; set; } // GenderId
        public string Gender { get; set; } // Gender
        public int? TotalGenderUsage { get; set; } // TotalGenderUsage
    }
}
﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;


namespace TraineeTrackingSystem.Web.ViewModels
{
    public class ApplicantsGeneralViewModel
    {
        public Guid Id { get; set; } // Id
        public Guid ProgramId { get; set; }
        public IEnumerable<KeyValuePair<Guid, string>> Programs { get; set; }
        //public virtual IEnumerable<KeyValuePair<Guid, string>> SelectedPrograms { get; set; }
        public virtual IEnumerable<Guid> SelectedPrograms { get; set; }
        public int DepartmentId { get; set; }
        public IEnumerable<KeyValuePair<int, String>> Departments { get; set; }
        public Guid PersonId { get; set; }
        public int? SelectedId { get; set; }
        public int? DoctoralLevelId { get; set; }
        public bool? IsTrainingGrantEligible { get; set; } // define this bool? will allow user to select YES or NO
                                                            // as the database would not accept NULL value
        public int IsUnderrepresentedMinority { get; set; }
        public int IsIndividualWithDisabilities { get; set; }
        public int IsFromDisadvantagedBkgd { get; set; }
        public int? YearEntered { get; set; }

        // FOR Training Grant Applicant Pool
        public IEnumerable<ApplicantGeneralViewModel> ApplicantPool { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AppPreDocViewModel
    {
        public Guid Id { get; set; }
        public Guid PersonId { get; set; }
        public Decimal? Gpa { get; set; } // GPA => this maps to GradRecord
        public Decimal? GpaScale { get; set; } // GPAScale     

        public bool HasGREScores { get; set; }  // NOT IN DB -- Set by Controller for reading in UI
        public bool HasMCATScores { get; set; }  // NOT IN DB -- Set by Controller for reading in UI
        public Decimal? GreScoreVerbal { get; set; } // GREScoreVerbal - GreScoreVerbal
        public Decimal? GrePercentileVerbal { get; set; } // GREPercentileVerbal - GrePercentileVerbal
        public Decimal? GreScoreQuantitative { get; set; } // GREScoreQuantitative (score) - GreScoreQuantitative
        public Decimal? GrePercentileQuantitative { get; set; } // GREPercentileQuantitative (Percentile) - GrePercentileQuantitative
        public Decimal? GreScoreAnalytical { get; set; } // GREScoreAnalytical - GreScoreAnalytical       
        public Decimal? GrePercentileAnalytical { get; set; } // GREPercentileAnalytical - GrePercentileAnalytical
        public Decimal? GreScoreSubject { get; set; } // GREScoreSubject - GreScoreSubject
        public Decimal? GrePercentileSubject { get; set; } // GREPercentileSubject (Percentile) - GrePercentileSubject
        public Decimal? McatScoreVerbalReasoning { get; set; } // MCATScoreVerbalReasoning - McatScoreVerbalReasoning
        public Decimal? McatScorePhysicalSciences { get; set; } // MCATScorePhysicalSciences - McatScorePhysicalSciences
        public Decimal? McatScoreBiologicalSciences { get; set; } // MCATScoreBiologicalSciences - McatScoreBiologicalSciences
        public string McatScoreWriting { get; set; } // MCATScoreWriting - McatScoreWriting
        public Decimal? McatPercentile { get; set; } // MCATPercentile - McatPercentile  

        public bool? WasInterviewed { get; set; } // WasInterviewed
        public bool? WasEnrolled { get; set; } // WasEnrolled
        public bool? AcceptedOffer { get; set; } // AcceptedOffer Post-doc
        public bool? WasAccepted { get; set; } // WasAccepted - Pre-doc
        public bool? WasOfferedPosition { get; set; } // WasOfferedPosition
        public bool? EnteredProgram { get; set; } // EnteredProgram
    }
}
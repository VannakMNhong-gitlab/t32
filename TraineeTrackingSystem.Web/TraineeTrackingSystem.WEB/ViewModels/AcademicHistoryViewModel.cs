﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AcademicHistoryViewModel
    {
        // FROM AcademicHistory table
        public Guid Id { get; set; } // Id (Primary key) =>AcademicHistory.Id = Institution.Id
        //public Guid PersonId { get; set; } // PersonId =>AcademicHistory.PersonId = Institution.PersonId
        //public int Id { get; set; }
        //public int? DegreeYear { get; set; }
        // FROM AcademicHistory
        public int? YearStarted { get; set; } // YearStarted - Year Entered 
        public int? YearEnded { get; set; } // YearEnded
        public int? YearDegreeCompleted { get; set; } // YearDegreeCompleted
        public int? InstitutionId { get; set; }
        public string InstitutionName { get; set; }
        public bool IsResidency { get; set; } // IsResidency
        public string AreaOfStudy { get; set; } // AreaOfStudy
        public int? CityId { get; set; }
        public string CityName { get; set; }
        public int? StateId { get; set; }
        public string StateName { get; set; }
        public int? CountryId { get; set; }
        public string CountryName { get; set; }
        public string AcademicDegreeName { get; set; }
        public int? AcademicDegreeId { get; set; }
        public string ResearchProjectTitle { get; set; } // ResearchProjectTitle
        public string DoctoralThesis { get; set; } // DoctoralThesis
        public string ResearchAdvisor { get; set; } // ResearchAdvisor
        public string ResidencyTrainingInstName { get; set; }
        public int? ResidencyInstitutionId { get; set; } // ResidencyInstitutionId 
        public string ResidencyAdvisor { get; set; } // ResidencyAdvisor
        public int? ResidencyYearStarted { get; set; } // ResidencyYearStarted
        public int? ResidencyYearEnded { get; set; } // ResidencyYearEnded
        public string Comments { get; set; } // Comments
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy

        public string StrDateLastUpdated { get; set; }

        public string StrLastUpdatedBy { get; set; }
    }
}
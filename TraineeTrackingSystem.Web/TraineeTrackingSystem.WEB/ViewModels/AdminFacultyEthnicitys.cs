﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    /// <summary>
    /// MVN - data binding model - to be used for loading UI grid elements
    /// </summary>
    public class AdminFacultyEthnicitys
    {
        public int selectedId { get; set; } // Id (Primary key)
        public string selectedEthnicity { get; set; } // Name
        public IEnumerable<AdminFacultyEthnicity> AdminFacultyEthnicityInformation { get; set; }
    }
}
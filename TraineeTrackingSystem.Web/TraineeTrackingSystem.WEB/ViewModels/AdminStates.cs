﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AdminStates
    {
        public int? SelectedId { get; set; }
        public string selectedAbbreviation { get; set; }
        public string selectedFullName { get; set; }
        public string selectedCountry { get; set; }
        //public IEnumerable<KeyValuePair<int, string>> Countries { get; set; }
        public IEnumerable<KeyValuePair<int, String>> Countries { get; set; }
        public IEnumerable<AdminState> StateProvinceInformation { get; set; }
        
    }
}
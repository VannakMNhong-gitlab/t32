﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class EditReportViewModel
    {
        public ReportViewModel ReportGeneralInfo { get; set; }
        public ReportTrainingGrantViewModel TrainingGrantDetails { get; set; }
        public ReportFundingViewModel FundingDetails { get; set; }
        public ReportApplicantViewModel ApplicantDetails { get; set; }
        public ReportFacultyViewModel FacultyDetails { get; set; }
        public ReportMenteeViewModel MenteeDetails { get; set; }


    }
}
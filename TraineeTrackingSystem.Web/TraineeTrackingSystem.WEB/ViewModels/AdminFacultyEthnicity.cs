﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    /// <summary>
    /// MVN - data binding model to be used in the UI grid
    /// </summary>
    public class AdminFacultyEthnicity
    {
        public int Id { get; set; } // Id (Primary key)
        public string Ethnicity { get; set; } // Name
        public int? TotalEthnicityUsage { get; set; } // TotalEthnicityUsage
    }
}
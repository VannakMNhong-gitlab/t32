﻿using System;
namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AdminCountry
    {
        public int Id { get; set; } // Id (Primary key)
        public string Country { get; set; } // Name 
        public int? TotalCountryUsage { get; set; }
    }
}
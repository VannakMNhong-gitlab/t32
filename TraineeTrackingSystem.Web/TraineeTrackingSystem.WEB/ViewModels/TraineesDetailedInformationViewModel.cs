﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class TraineesDetailedInformationViewModel
    {
        public Guid TrainingGrantId { get; set; }
        public IEnumerable<TraineeDetailedInformationViewModel> TraineeDetailedInformationView { get; set; }    // Grid dataSource
        public Guid? SelectedTrainingPeriodId { get; set; }

        //public TrainingGrantTrainee 
      
        /* ********************************** define dropdown properties *****************************
         *                                  and fields selections
        **********************************************************************************************/
        public IEnumerable<KeyValuePair<Guid?, string>> MenteeListDropdown { get; set; }                    // Faculty Dropdown
        public IEnumerable<KeyValuePair<Guid?, string>> FacultyMenteesDropdown { get; set; }            //TO Replace MenteeListDropdown 
        public string selectedId { get; set; } // to be removed => replace bellow
        public string selectedTraineeId { get; set; }
     
        public string SelectedMenteeId { get; set; }                                                      // UI selection
        public string SelectedDateStarted { get; set; } // DateStarted
        public string SelectedDateEnded { get; set; } // DateEnded

        // Second Grid Source 
        public string SelectedIdP { get; set; }
        public int SelectedDoctoralLevelId { get; set; } // DoctoralLevelId


        public int SelectedTraineeStatusId { get; set; } // TraineeStatus
        public string SelectedInstitution { get; set; }
        public string SelectedDegree { get; set; }
        public int? SelectedYearStarted { get; set; } // YearStarted
        public int? SelectedYearEnded { get; set; } // YearEnded
  



       
        //public IEnumerable<KeyValuePair<Guid?, string>> MenteeStartDate { get; set; }
        //public IEnumerable<KeyValuePair<Guid?, string>> MenteeStartEnd { get; set; }

        /* **************************** define pre/Post Doc properties *************************
         *                                  and fields selections
         ***************************************************************************************/
        public int? SelectedPredocTraineeStatusId { get; set; }
        public int? SelectedPostdocTraineeStatusId { get; set; }
        public string SelectedPredocReasonForLeaving { get; set; } // PredocReasonForLeaving
        public string SelectedPostdocReasonForLeaving { get; set; } // PostdocReasonForLeaving
        //Testing logic =>second grid validation 
        public string setMenteeUIContents { get; set; }
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public string StrDateLastUpdated { get; set; }  //the date will be formatted
    }
}
﻿using System.Web.Mvc;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    // Mappers
    //public class CreateFacultyViewModel : IMappableViewModel<CreateFacultyViewModel, Faculty>
    public class CreateFacultyViewModel
    {
        public Faculty Faculty { get; set; }
        public System.Guid Id { get; set; }
        public System.Guid PersonId { get; set; }
        public string EmployeeId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string Name { get; set; }
        public SelectList Faculties { get; set; }
    }
}
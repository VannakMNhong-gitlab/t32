﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AcademicsHistoryViewModel
    {
        public Guid PersonId { get; set; }      
        public int? SelectedId { get; set; }
        public string selectededInstitution { get; set; }
        public bool IsResidency { get; set; } // IsResidency
        public string selectedResidencyInstitution { get; set; }
        public string selectedAreaOfStudy { get; set; }
        public IEnumerable<KeyValuePair<int, String>> Countries { get; set; }
        public IEnumerable<KeyValuePair<int, String>> States { get; set; }
        public string selectedDegree { get; set; }
        //public string selectedEntryYear { get; set; } change this to started to match the new requirement
        public string selectedYearStarted { get; set; }
        //public string selectedCompletionYear { get; set; } change this to reflect the change on mock-up
        public string selectedYearEnded { get; set;}
        public string selectedYearDegreeCompleted { get; set; }
        public string selectedResearchProjectTitle { get; set; } // ResearchProjectTitle
        public string selectedDoctoralThesis { get; set; } // DoctoralThesis
        public string selectedResearchAdvisor { get; set; } // ResearchAdvisor
        //public string selectedResidencyAdvisor { get; set; } // ResearchAdvisor

        public string selectedResidencyAdvisor { get; set; } // ResidencyAdvisor
        public int? selectedResidencyYearStarted { get; set; } // ResidencyYearStarted
        public int? selectedResidencyYearEnded { get; set; } // ResidencyYearEnded
        public string selectedComments { get; set; } // Comments

        public IEnumerable<AcademicHistoryViewModel> AcademicHistoryInformations { get; set; }
        public DateTime? DateLastUpdated { get; set; }
        public string StrDateLastUpdated { get; set; }  //the date will be formatted
        public string LastUpdatedBy { get; set; }
    }
}
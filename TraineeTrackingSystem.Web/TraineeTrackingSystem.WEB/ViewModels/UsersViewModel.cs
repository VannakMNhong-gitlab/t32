﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class UsersViewModel: PersonVm
    {
        public string selectedId { get; set; }   //current grid selection 
        public Guid? PersonId { get; set; } // PersonId
        public string Username { get; set; }
        public int LoginId { get; set; } // LoginId (Primary key)
        public int? RoleId { get; set; } // RoleId (Primary key)
        //public IEnumerable<KeyValuePair<int, String>> Roles { get; set; }
        public IEnumerable<KeyValuePair<int, String>> Roles { get; set; }
        //public IEnumerable<UserViewModel> UserGridView { get; set; }

        public string Rolname { get; set; }

        public bool? IsActive { get; set; }
        public string StrDateLastUpdated { get; set; }  //the date will be formatted
        public string StrLastUpdatedBy { get; set; }
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
    }
}
﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;

//namespace TraineeTrackingSystem.Web.ViewModels
//{
//    public class ApplicantsPredDocInformationVVM
//    {
//        // TO BE USED in the UI
//        public Guid PersonId { get; set; } // PersonId
//        public bool WasInterviewed { get; set; } // WasInterviewed
//        public bool WasOfferedPosition { get; set; } // WasOfferedPosition
//        public bool AcceptedOffer { get; set; } // AcceptedOffer
//        public bool WasEnrolled { get; set; } // WasEnrolled
//        public Decimal? Gpa { get; set; } // GPA => this maps to GradRecord
//        public string GpaScale { get; set; } // GPAScale
//        public int? TestScoreTypeId { get; set; } // TestScoreTypeId
//        public IEnumerable<KeyValuePair<int, String>> TestScoreType { get; set; }
//        public int? GreScoreVerbal { get; set; } // GREScoreVerbal
//        public Decimal? GrePercentileVerbal { get; set; } // GREPercentileVerbal
//        public int? GreScoreQuantitative { get; set; } // GREScoreQuantitative (score)
//        public Decimal? GrePercentileQuantitative { get; set; } // GREPercentileQuantitative (Percentile)
//        public int? GreScoreAnalytical { get; set; } // GREScoreAnalytical        
//        public Decimal? GrePercentileAnalytical { get; set; } // GREPercentileAnalytical
//        public int? GreScoreSubject { get; set; } // GREScoreSubject
//        public Decimal? GrePercentileSubject { get; set; } // GREPercentileSubject (Percentile)
//        public int? McatScoreVerbalReasoning { get; set; } // MCATScoreVerbalReasoning
//        public int? McatScorePhysicalSciences { get; set; } // MCATScorePhysicalSciences
//        public int? McatScoreBiologicalSciences { get; set; } // MCATScoreBiologicalSciences
//        public int? McatScoreWriting { get; set; } // MCATScoreWriting
//        public Decimal? McatPercentile { get; set; } // MCATPercentile
//        public IEnumerable<ApplicantPredDocInformationCVM> ApplicantPredDocInformations { get; set; }


//        public IEnumerable<ApplicantPredDocInformationCVM> OSUTimeLines { get; set; }
//    }
//}
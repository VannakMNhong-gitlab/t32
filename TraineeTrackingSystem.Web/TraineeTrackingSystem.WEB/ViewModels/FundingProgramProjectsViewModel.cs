﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class FundingProgramProjectsViewModel
    {
        public Guid FundingId { get; set; }

        public IEnumerable<FundingProgramProjectViewModel> ProgramProjects { get; set; }
        public IEnumerable<KeyValuePair<Guid, String>> ProgramDirectors { get; set; }
        public Guid? ProgramDirectorId { get; set; }
    }
}
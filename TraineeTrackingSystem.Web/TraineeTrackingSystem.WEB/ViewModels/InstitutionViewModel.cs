﻿using System.Collections.Generic;
using System;
namespace TraineeTrackingSystem.Web.ViewModels
{

    public class InstitutionPostModel
    {
        public string Name { get; set; } // Name
        public string City { get; set; } // City
        public int? StateId { get; set; } // StateId
        public string StateName { get; set; }
        //public IEnumerable<KeyValuePair<int, String>> StateDropdownList { get; set; }                    // Faculty Dropdown
        public IEnumerable<KeyValuePair<int, String>> States { get; set; }
        public IEnumerable<KeyValuePair<int, String>> Countries { get; set; }
        public int? CountryId { get; set; } // CountryId
        public string CountryName { get; set; }
        public int? TotalInstitutionUsage { get; set; } // TotalInstitutionUsage
        //public IEnumerable<KeyValuePair<int?, String>> TotalInstitutionUsage { get; set; }
        //public IEnumerable<KeyValuePair<int, String>> TotalInstUsage { get; set; }
        //public string Institution { get; set; }
    }

    public class InstitutionViewModel : InstitutionPostModel
    {
        public int Id { get; set; } // Id (Primary key)     
        public string InstitutionIdentifier { get; set; } // InstitutionIdentifier
    }
}
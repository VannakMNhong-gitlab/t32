﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;

//namespace TraineeTrackingSystem.Web.ViewModels
//{
//    public class OSUTimesLineModel
//    {
//        public Guid Id { get; set; } // Id
//        public Guid PersonId { get; set; } // PersonId

//        // FROM OsuTimeline  - these USED IN THE UI
//        public int DegreeEnteredYearId { get; set; } //AcademicHistory
//        public int? DegreeEnteredYear { get; set; } // YearStarted - this maps OsuTimeLine
//        public bool WasInterviewed { get; set; } // WasInterviewed
//        public bool AcceptedOffer { get; set; } // AcceptedOffer
//        public bool WasEnrolled { get; set; } // WasEnrolled
//        // FROM GradeRecord
//        public string GpaScale { get; set; } // GPAScale
//        public int? TestScoreTypeId { get; set; } // TestScoreTypeId

//        public IEnumerable<OSUTimeLineModel> OSUTimeLineInformations { get; set; }
//    }
//}
﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class ReportApplicantViewModel
    {

        public IEnumerable<KeyValuePair<Guid, int>> Applicants { get; set; }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public int SelectedApplicantYearEntered { get; set; }
        public Guid SelectedApplicant { get; set; }
        public bool IsApplicantReportFound { get; set; }
    }
}

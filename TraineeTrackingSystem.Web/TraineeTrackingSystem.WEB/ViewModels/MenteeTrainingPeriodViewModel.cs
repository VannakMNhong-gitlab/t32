﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class MenteeTrainingPeriodViewModel
    {

        public Guid Id { get; set; }
        public Guid PersonId { get; set; }  // PersonId of the Mentee
        public int? InstitutionId { get; set; } // InstitutionId
        public string InstitutionName { get; set; }
        public int? CityId { get; set; }
        public string CityName { get; set; }
        public int? StateId { get; set; }
        public string StateName { get; set; }
        public int? CountryId { get; set; }
        public string CountryName { get; set; }
        public int DoctoralLevelId { get; set; } // DoctoralLevelId
        public string DoctoralLevel { get; set; }
        public int? YearStarted { get; set; } // YearStarted
        public int? YearEnded { get; set; } // YearEnded
        public int? AcademicDegreeId { get; set; } // AcademicDegreeId
        public string AcademicDegreeName { get; set; }
        public int? AcademicDegreeSoughtId { get; set; } // AcademicDegreeId
        public string AcademicDegreeSoughtName { get; set; }
        public string ResearchProjectTitle { get; set; } // ResearchProjectTitle
        public bool IsDeleted { get; set; }
        public virtual IEnumerable<Guid> SelectedPrograms { get; set; }
        //MVN - added this for dropdownList selection 
        ///public Guid ProgramId { get; set; } // ProgramId
        //public string Programs { get; set; }
        public Guid? ProgramId { get; set; } // ProgramId
        public IEnumerable<KeyValuePair<Guid, String>> Programs { get; set; }   // Collection of ALL Programs in db
        //public string ProgramName { get; set; }
        public virtual IEnumerable<Guid> SelectedMentors { get; set; }

        // MVN -testing adding here 
        ///public string Programs { get; set; } // Many to many mapping
        ///public IEnumerable<KeyValuePair<int, String>> SelectPrograms { get; set; }
        ///public IEnumerable<KeyValuePair<Guid, String>> SelectTPrograms { get; set; }
        /* ***************************************************************/
        // MVN - These data elements map to TraineeStatu table
        /* ***************************************************************/
        public Guid? TrainingPeriodId { get; set; } // TrainingPeriodId
        public int TraineeStatusId { get; set; } // Id (Primary key)
        public string Name { get; set; } // Name
        public int? InstitutionAssociationId { get; set; } // InstitutionAssociationId
        public string InstitutionAssociation { get; set; }
        public string PreDocProgramStatus { get; set; }
        public string PostDocProgramStatus { get; set; }
        public int? PredocTraineeStatusId { get; set; }
        public int? PostdocTraineeStatusId { get; set; }
        public string PredocReasonForLeaving { get; set; } // PredocReasonForLeaving
        public string PostdocReasonForLeaving { get; set; } // PostdocReasonForLeaving
        public string TraineeStatus { get; set; }
        public Guid? SelectedTrainingPeriodId { get; set; }
        public Guid MenteeId { get; set; }
        //public DateTime? DateLastUpdated { get; set; }
        //public string StrDateLastUpdated { get; set; }  //the date will be formatted
        //public string LastUpdatedBy { get; set; }
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        //public int? DegreeSoughtId { get; set; } // DegreeSoughtId
        public string StrDateLastUpdated { get; set; }  //the date will be formatted
        public string StrLastUpdatedBy { get; set; } // LastUpdatedBy

        public int DepartmentId { get; set; }

        public IEnumerable<KeyValuePair<int, string>> Departments { get; set; }
    }
}
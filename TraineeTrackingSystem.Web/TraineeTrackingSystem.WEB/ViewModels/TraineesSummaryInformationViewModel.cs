﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class TraineesSummaryInformationViewModel
    {
        public Guid Id { get; set; } // Id (Primary key)
        public Guid FundingId { get; set; } // PK FundingId 
        public string selectedTraineeId { get; set; }
        //public int? NumPredocPositionsAwarded { get; set; } // Number of Pre-doc Positions Awarded
        //public int? PredocSupportMonthsAwarded { get; set; } // Number of Months of Support per Pre-doc Position Awarded
        //public int? NumPostdocPositionsAwarded { get; set; } // Number of Post-doc Positions Awarded
        //public int? PostdocSupportMonthsAwarded { get; set; } // Number of Months of Support per Post-doc Position Awarded

        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public string StrDateLastUpdated { get; set; }  //the date will be formatted

        // MVN - New table is added
        public IEnumerable<TraineeSummaryInformationViewModel> TraineeSummaryInformationGridView { get; set; }
        //public IEnumerable<TraineeSummaryInformationViewModel> PostDocTraineeSummaryInformationGridView { get; set; }
        // Ref to TraineeInformationViewModel
        //public IEnumerable<TraineeInformationViewModel> TraineeInformationView { get; set; }

        public string selectedId { get; set; }   //current grid selection     
        //public int? SelectedNumPredocPositionsAwarded { get; set; } // Number of Pre-doc Positions Awarded
        //public int? SelectedPredocSupportMonthsAwarded { get; set; } // Number of Months of Support per Pre-doc Position Awarded
        //public int? SelectedNumPostdocPositionsAwarded { get; set; } // Number of Post-doc Positions Awarded
        //public int? SelectedPostdocSupportMonthsAwarded { get; set; } // Number of Months of Support per Post-doc Position Awarded

        //public IEnumerable<TraineeInformationViewModel> TraineeDetailInformationView { get; set; }
        public DateTime SelectedDateStarted { get; set; }
        public DateTime SelectedDateEnded { get; set; }

        public Guid TrainingGrantId { get; set; }
        public int? SupportMonthsAwarded { get; set; } // SupportMonthsAwarded
        public int? SupportMonthsUsed { get; set; } // SupportMonthsUsed
        public int? UrmSupportMonthsUsed { get; set; } // URMSupportMonthsUsed
        public int? DisabilitiesSupportMonthsUsed { get; set; } // DisabilitiesSupportMonthsUsed
        public int? DisadvantagedSupportMonthsUsed { get; set; } // DisadvantagedSupportMonthsUsed

        public int? NumMdAppointed { get; set; } // NumMDAppointed
        public int? NumMdPhDAppointed { get; set; } // NumMDPhDAppointed
        public int? NumPhDAppointed { get; set; } // NumPhDAppointed
        public int? NumOtherDegreeAppointed { get; set; } // NumOtherDegreeAppointed
    }
}
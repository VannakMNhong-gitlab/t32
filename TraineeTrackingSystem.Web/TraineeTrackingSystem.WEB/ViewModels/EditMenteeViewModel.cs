﻿using System.Collections.Generic;                                 
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class EditMenteeViewModel
    {
        public MenteeGeneralViewModel MenteeGeneralInfo { get; set; }

        public AppPreDocViewModel ApplicantInformation { get; set; }

        public MenteeTrainingPeriodsViewModel MenteeTrainingPeriods { get; set; }

        public AcademicsHistoryViewModel MenteeAcademicHistories { get; set; }

        public MenteeSupportsViewModel MenteeSupports { get; set; }

        public WorkHistoriesViewModel TraineePositions { get; set; }

        public MenteePublicationsViewModel MenteePublications { get; set; }

        public ICollection<Mentee> MenteeGeneralViewModel { get; set; }

        //public ICollection<Mentee> MenteeGeneralViewModel { get; set; }
    }
}
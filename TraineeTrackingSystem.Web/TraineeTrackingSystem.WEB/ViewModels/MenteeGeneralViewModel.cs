﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class MenteeGeneralViewModel:PersonVm
    {

        public System.Guid MenteeId { get; set; }
        public Guid PersonId { get; set; } // PersonId

        public DateTime? DateLastUpdated { get; set; }
        public string StrDateLastUpdated { get; set; }  //the date will be formatted
        public string StrLastUpdatedBy { get; set; }
        public string LastUpdatedBy { get; set; }
        public int? SelectedId { get; set; }

        public string StudentId { get; set; }
        //public System.Guid MentorId { get; set; }
        public int? InstitutionAssociationId { get; set; }
        //public int? DoctoralLevelId { get; set; }

        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public IEnumerable<KeyValuePair<int, String>> Departments { get; set; }
        //public DateTime DateStarted { get; set; }
        //public DateTime DateEnded { get; set; }
        //public string EmailAddress1 { get; set; }
        //public string EmailAddress2 { get; set; }
        //public string PhoneNumber { get; set; }
        //public string MailingAddressLine1 { get; set; }
        //public string MailingAddressLine2 { get; set; }
        //public string MailingAddressLine3 { get; set; }
        //public string MailingAddressCity { get; set; }
        //public int? MailingAddressStateId { get; set; }
        public IEnumerable<KeyValuePair<int, String>> AddressStates { get; set; }        
        //public string MailingAddressPostalCode { get; set; }
        //public int? MailingAddressCountryId { get; set; }
        public IEnumerable<KeyValuePair<int, String>> AddressCountries { get; set; }

        //public int OrganizationId { get; set; }
        //public IEnumerable<KeyValuePair<int, String>> Organization { get; set; }

        public bool IsTrainingGrantEligible { get; set; }
        public int? IsUnderrepresentedMinority { get; set; }
        public int? IsIndividualWithDisabilities { get; set; }
        public int? IsFromDisadvantagedBkgd { get; set; }


    }
}
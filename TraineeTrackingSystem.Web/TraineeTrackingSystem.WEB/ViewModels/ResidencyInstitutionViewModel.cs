﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
using TraineeTrackingSystem.Web.PostModels;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class ResidencyInstitutionViewModel : ResidencyInstitutionPostModel
    {
        public int Id { get; set; }
        public string InstitutionIdentifier { get; set; } // InstitutionIdentifier
    }
}
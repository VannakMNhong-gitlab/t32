﻿using System;
namespace TraineeTrackingSystem.Web.ViewModels
{

    public class ProgramPostModel
    {
        public string Title { get; set; } // Name

    }

    public class ProgramViewModel : ProgramPostModel
    {
        public Guid Id { get; set; } // Id (Primary key)
        
    }
}
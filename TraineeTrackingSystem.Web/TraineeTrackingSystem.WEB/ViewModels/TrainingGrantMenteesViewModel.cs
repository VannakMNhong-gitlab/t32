﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class TrainingGrantMenteesViewModel
    {
        public Guid TrainingGrantId { get; set; }
        //public int? ApplicantPoolAcademicYear { get; set; }

        //public int selectedApplicantPoolAcademicYear { get; set; }
        public IEnumerable<TrainingGrantMenteeViewModel> FacultyMentees { get; set; }   

    }
}
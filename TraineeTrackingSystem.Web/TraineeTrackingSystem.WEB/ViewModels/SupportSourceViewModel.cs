﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class SupportSourceViewModel
    {

        public int Id { get; set; }
        public string SupportTitle { get; set; }
        public bool IsNew { get; set; }

    }
}
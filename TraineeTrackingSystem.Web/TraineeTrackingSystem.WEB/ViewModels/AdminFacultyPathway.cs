﻿using System;
using System.Collections.Generic;


namespace TraineeTrackingSystem.Web.ViewModels
{
    /// <summary>
    /// MVN - data binding model to be used in the UI grid
    /// </summary>
    public class AdminFacultyPathway
    {
        public int Id { get; set; } // ClinicalFacultyPathwayId
        public string ClinicalFacultyPathway { get; set; } // ClinicalFacultyPathway
        public int? TotalClinicalFacultyPathwayUsage { get; set; } // TotalClinicalFacultyPathwayUsage
    }
}
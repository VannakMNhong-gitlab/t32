﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class SupportSourcesViewModel
    {

        public int SelectedId { get; set; }
        public string SelectedSupportTitle { get; set; }

        public IEnumerable<SupportSourceViewModel> CurrentSupportSources { get; set; }

    }
}
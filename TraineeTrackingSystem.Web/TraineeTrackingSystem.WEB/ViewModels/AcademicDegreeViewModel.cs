﻿namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AcademicDegreeViewModel
    {
        public int Id { get; set; }
        public int? InstitutionId { get; set; }
        public int? DegreeYear { get; set; }
        public string InstitutionName { get; set; }
        public int? AcademicDegreeId { get; set; }
        public string AcademicDegreeName { get; set; }
        public int? CityId { get; set; }
        public string CityName { get; set; }
        public int? StateId { get; set; }
        public string StateName { get; set; }
        public int? CountryId { get; set; }
        public string CountryName { get; set; }

        public string StrDateLastUpdated { get; set; }

        public string StrLastUpdatedBy { get; set; }

        public int? TotalDegreeUsage { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class OtherFundingsInfo
    {
        public Guid? FacultyPersonId { get; set; } // FacultyPersonId
        public int? SelectedId { get; set; }
        public Guid TrainingGrantId { get; set; }
        public IEnumerable<OtherFundingInfo> OtherGridView { get; set; }
        //public IEnumerable<OtherFundingInfo> OtherFundingGridView { get; set; }
        //public IEnumerable<MenteeMentorViewModel> MenteesGridView { get; set; }
    }
}
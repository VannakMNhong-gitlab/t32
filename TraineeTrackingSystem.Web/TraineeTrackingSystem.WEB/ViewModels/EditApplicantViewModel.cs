﻿//using TraineeTrackingSystem.Web.Controllers;
namespace TraineeTrackingSystem.Web.ViewModels
{
    public class EditApplicantViewModel
    {
        public ApplicantsGeneralViewModel ApplicantInformations { get; set; }    // Uses in the Applicant Create view
        public ApplicantGeneralViewModel ApplicantGeneralViewModel { get; set; } // Uses in the Applicant Edit view       
        public ApplicantsPredDocViewModel ApplicantPredDocInformations { get; set; } // uses in the Applicant Edit (create new pred-doc) -> partial view
        public ApplicantPredDocViewModel ApplicantPredDocViewModel { get; set; }
        public ApplicantsOSUTimeLineViewModel ApplicantOSUTimeLineInformations { get; set; }
        public AppSPreDocViewModel AppPreDocInformations { get; set; }
        public AppPreDocViewModel AppPreViewModel { get; set; }
        public AppPostDocViewModel AppPostDocViewModel { get; set; }
        public AcademicsHistoryViewModel AcademicHistoryInformations { get; set; }
    }
}
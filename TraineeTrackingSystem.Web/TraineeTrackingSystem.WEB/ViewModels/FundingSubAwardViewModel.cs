﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class FundingSubAwardViewModel
    {
        public Guid Id { get; set; }

        public string PrimeAwardNumber { get; set; }
        public IEnumerable<KeyValuePair<Guid, String>> PrimaryInvestigators { get; set; }
        public Guid? PrimeAwardPIId { get; set; }

        //public IEnumerable<KeyValuePair<int, String>> PrimeSponsors { get; set; }
        public int? PrimeSponsorId { get; set; }
        public string PrimeSponsorName { get; set; }

        public string StrDateLastUpdated { get; set; }

        public string StrLastUpdatedBy { get; set; }
    }
}
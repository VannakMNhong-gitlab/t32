﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class InstitutionTrainingGrants
    {
        public Guid? FacultyPersonId { get; set; } // FacultyPersonId
        public Guid TrainingGrantId { get; set; }
        public IEnumerable<InstitutionTrainingGrant> FundingGridView { get; set; }
    }
}
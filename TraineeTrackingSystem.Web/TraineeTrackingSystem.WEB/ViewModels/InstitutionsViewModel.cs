﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class InstitutionsViewModel
    {
        public string selectedId { get; set; }   //current grid selection 
        //public IEnumerable<TrainingGrantViewModel> PreviousTrainingGrants { get; set; }   
        public IEnumerable<InstitutionViewModel> AdminInstSponsorGridView { get; set; }            // Faculty Support GRID List 

        //The following view model will be back to the UI dropdown datasource
        public IEnumerable<KeyValuePair<int, String>> States { get; set; }
        public IEnumerable<KeyValuePair<int, String>> Countries { get; set; }
        public int selectedStateProvince { get; set; }
        public int selectedCountry { get; set; }
        public int selectedInstName { get; set; }
        public int selectedCityName { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class ApplicantsPredDocViewModel
    {
        public Guid PersonId { get; set; } // PersonId
        public int? SelectedId { get; set; }
        public Decimal? Gpa { get; set; } // GPA => this maps to GradRecord
        public string GpaScale { get; set; } // GPAScale       
        
        public IEnumerable<ApplicantPredDocViewModel> ApplicantPredDocInformations { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;


namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AwardsInformationViewModel
    {
        public Guid Id { get; set; } // Funding PK Id 
         
        public string selectedSponsorAwardNumber { get; set; } // SponsorAwardNumber
        public DateTime? selectedDateStarted { get; set; } // DateStarted
        public DateTime? selectedDateEnded { get; set; } // DateEnded

    }
}
﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class TrainingGrantViewModel
    {

        public Guid Id { get; set; } // Id (Primary key)

        public IEnumerable<KeyValuePair<Guid, string>> FacultyList { get; set; }
        public IEnumerable<KeyValuePair<int, String>> Countries { get; set; }
        public IEnumerable<KeyValuePair<int, String>> States { get; set; }
        public IEnumerable<KeyValuePair<int, String>> Departments { get; set; } // Collection of ALL Depts in db
        public IEnumerable<KeyValuePair<Guid, String>> Programs { get; set; }   // Collection of ALL Programs in db

        public virtual IEnumerable<Guid> SelectedPrograms { get; set; }
        //public Guid? SelectedProgram { get; set; }
        public virtual IEnumerable<int> SelectedDepartments { get; set; }
        //public virtual Funding Funding { get; set; }

        public Guid FundingId { get; set; } // FundingId

        public string Title { get; set; }

        public Guid PdFacultyId { get; set; }
        public string PdFullName { get; set; }

        public int TrainingGrantStatusId { get; set; } // TrainingGrantStatusId
        public IEnumerable<KeyValuePair<int, string>> TrainingGrantStatusName { get; set; }
        public string GrtNumber { get; set; } // GRTNumber
        public string SponsorReferenceNumber { get; set; } // SponsorReferenceNumber
        public string SponsorAwardNumber { get; set; }
        public DateTime? DateProjectedStart { get; set; }
        public DateTime? DateProjectedEnd { get; set; } // DateProjectedEnd
        public bool IsRenewal { get; set; } // IsRenewal
        
        public int? DoctoralLevelId { get; set; } // DoctoralLevelId
        public string DoctoralLevel { get; set; }
        public int? NumPredocPositionsRequested { get; set; } // NumPredocPositionsRequested
        public int? NumPostdocPositionsRequested { get; set; } // NumPostdocPositionsRequested
        public int? NumPredocPositionsAwarded { get; set; } // NumPredocPositionsAwarded
        public int? NumPostdocPositionsAwarded { get; set; } // NumPostdocPositionsAwarded
        public int? PredocSupportMonthsAwarded { get; set; } // PredocSupportMonthsAwarded
        public int? PostdocSupportMonthsAwarded { get; set; } // PostdocSupportMonthsAwarded
        public Guid? PreviousTrainingGrantId { get; set; } // PreviousTrainingGrantId
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public string StrDateLastUpdated { get; set; }  //the date will be formatted
        public int? SponsorId { get; set; } // InstitutionId
        public string SponsorName { get; set; }
        public int? CityId { get; set; }
        public string CityName { get; set; }
        public int? StateId { get; set; }
        public string StateName { get; set; }
        public int? CountryId { get; set; }
        public string CountryName { get; set; }
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded

        // MVN - added 
        public string selectedInstitution { get; set; } //USES in UI data-bind value sponsor=institution
        // .Key bind to sponsorId and selectedInstitution bind to sponsorName   


        public string StrLastUpdatedBy { get; set; }
    }
}
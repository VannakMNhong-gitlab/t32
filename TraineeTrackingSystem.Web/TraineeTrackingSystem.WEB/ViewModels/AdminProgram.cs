﻿using System;
using System.Collections.Generic;


namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AdminProgram
    {
        public Guid Id { get; set; } // Id (Primary key)
        public string ProgramTitle { get; set; } // Title
        public int DisplayId { get; set; } // DisplayId
        public int? TotalProgramUsage { get; set; } // TotalProgramUsage
    }
}
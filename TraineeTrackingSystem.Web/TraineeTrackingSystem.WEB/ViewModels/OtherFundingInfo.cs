﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class OtherFundingInfo
    {
        public Guid Id { get; set; } // Funding Id
        public string FacultyRole { get; set; } // FacultyPrimaryRole
        public string Title { get; set; } // Title

        public int SponsorId { get; set; } // InstitutionId
        public string SponsorName { get; set; }

        public string SponsorAwardNumber { get; set; } // SponsorAwardNumber

       
        public DateTime? DateTimeFundingStarted { get; set; } // DateTimeFundingStarted
        public string StrDateTimeFundingStarted { get; set; }  //the date will be formatted
        public DateTime? DateTimeFundingEnded { get; set; } // DateTimeFundingEnded
        public string StrDateTimeFundingEnded { get; set; }  // the date will be formatted
        //public Guid TrainingGrantId { get; set; }

        public string PIFullName { get; set; }
        public string FundingStatus { get; set; }
    }
}
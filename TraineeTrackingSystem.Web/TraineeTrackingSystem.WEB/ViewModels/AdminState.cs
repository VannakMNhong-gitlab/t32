﻿using System;
using System.Collections.Generic;
namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AdminState
    {
        public int Id { get; set; } // Id (Primary key)
        public string Abbreviation { get; set; } // Abbreviation
        public string FullName { get; set; } // FullName
        public int? CountryId { get; set; } // CountryId
        public string Country { get; set; } // Name 
        //public IEnumerable<KeyValuePair<int, string>> Country { get; set; }
    
        public int? TotalStateProvinceUsage { get; set; } // TotalStateProvinceUsage

        public IEnumerable<KeyValuePair<int, string>> Countries { get; set; }
    }
}
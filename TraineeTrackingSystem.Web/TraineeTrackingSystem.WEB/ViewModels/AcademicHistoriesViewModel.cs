﻿namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AcademicHistoriesViewModel
    {
        public int SelectedId { get; set; }
        public int SelectedInstitutionId { get; set; } // InstitutionId
        public int? SelectedYearStarted { get; set; } // YearStarted
        public int? SelectedYearEnded { get; set; } // YearEnded
        public int? SelectedAcademicDegreeId { get; set; } // AcademicDegreeId
        public string SelectedAreaOfStudy { get; set; } // AreaOfStudy
        public int? SelectedYearDegreeCompleted { get; set; } // YearDegreeCompleted
        public bool SelectedIsUndergraduate { get; set; } // IsUndergraduate
        public string SelectedResearchProjectTitle { get; set; } // ResearchProjectTitle
        public string SelectedDoctoralThesis { get; set; } // DoctoralThesis
        public string SelectedResearchAdvisor { get; set; } // ResearchAdvisor
        public bool SelectedIsResidency { get; set; } // IsResidency
        public int? SelectedResidencyInstitutionId { get; set; } // ResidencyInstitutionId
        public string SelectedResidencySpecialization { get; set; } // ResidencySpecialization
        public string SelectedResidencyPgy { get; set; } // ResidencyPGY
        public string SelectedResidencyAdvisor { get; set; } // ResidencyAdvisor
        public int? SelectedResidencyYearStarted { get; set; } // ResidencyYearStarted
        public int? SelectedResidencyYearEnded { get; set; } // ResidencyYearEnded
        public bool SelectedIsNonMedicalTransfer { get; set; } // IsNonMedicalTransfer
        public bool SelectedIsPhDTransfer { get; set; } // IsPhDTransfer
        public string SelectedLabResearch { get; set; } // LabResearch
        public int? SelectedTrainingDoctoralLevelId { get; set; } // TrainingDoctoralLevelId
        public string SelectedComments { get; set; } // Comments
    }
}
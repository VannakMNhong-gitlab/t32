﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class MenteeSupportViewModel
    {

        public int Id { get; set; }
        public Guid PersonId { get; set; }  // PersonId of the Mentee
        public int InstitutionId { get; set; } // InstitutionId
        public string InstitutionName { get; set; }
        public int? CityId { get; set; }
        public string CityName { get; set; }
        public int? StateId { get; set; }
        public string StateName { get; set; }
        public int? CountryId { get; set; }
        public string CountryName { get; set; }
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded
        public string SupportSourceText { get; set; } // SupportSourceText

        //public virtual IEnumerable<int> SelectedSupports { get; set; }
        //public SupportSourceViewModel MenteeCurrentSupports { get; set; }
        //public IEnumerable<SupportSourcesViewModel> MenteeCurrentSupports { get; set; }
        //public SupportSourcesViewModel MenteeCurrentSupports { get; set; }



        public string StrDateLastUpdated { get; set; }

        public string StrLastUpdatedBy { get; set; }
    }
}
﻿namespace TraineeTrackingSystem.Web.ViewModels
{
    public class FacultyResearchInterestViewModel
    {
        
        public int Id { get; set; }
        public string ResearchInterest { get; set; }
    }
}
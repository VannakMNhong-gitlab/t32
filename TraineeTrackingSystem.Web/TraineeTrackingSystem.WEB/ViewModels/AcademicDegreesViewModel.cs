﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AcademicDegreesViewModel
    {
        public Guid PersonId { get; set; }

        public IEnumerable<KeyValuePair<int, String>> Countries { get; set; }
        public IEnumerable<KeyValuePair<int, String>> States { get; set; }
        public IEnumerable<KeyValuePair<int, string>> Years { get; set; }
        public IEnumerable<AcademicDegreeViewModel> DegreeInformations { get; set; }
        public int? SelectedId { get; set; }
        public string selectedDegree { get; set; }       
        public string SelectedInstitution { get; set; }
 
    }
}
﻿using System;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class MenteePublicationViewModel
    {
        
        public Guid Id { get; set; }
        public Guid PersonId { get; set; }  // PersonId of the Mentee

        public Guid AuthorId { get; set; } // AuthorId
        public string DateEntered { get; set; }

        // FROM Publication table - These use in the UI data-bind value
        public string PMCId { get; set; } // PMCID

        public string PMId { get; set; }
        //public bool IsMenteeFirstAuthor { get; set; } // IsMenteeFirstAuthor
        public int IsMenteeFirstAuthor { get; set; } // IsMenteeFirstAuthor
        //public bool IsAdvisorCoAuthor { get; set; } // IsAdvisorCoAuthor
        public int IsAdvisorCoAuthor { get; set; } // IsAdvisorCoAuthor
        public string AuthorList { get; set; } // AuthorList
        public string Title { get; set; } // Title
        public string Journal { get; set; } // Journal
        public DateTime? DateEpub { get; set; } // DateEpub
        public string Doi { get; set; } // DOI
        public DateTime? DatePublicationLastUpdated { get; set; } // DatePublicationLastUpdated
        public int? YearPublished { get; set; } // YearPublished
        public string Volume { get; set; } // Volume
        public string Issue { get; set; } // Issue
        public string Pagination { get; set; } // Pagination
        public string Citation { get; set; }
        //public string Abstract { get; set; } // Abstract

        public string StrDateLastUpdated { get; set; }

        public string StrLastUpdatedBy { get; set; }
    }
}
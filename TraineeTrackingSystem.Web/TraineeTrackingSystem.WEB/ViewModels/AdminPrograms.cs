﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class AdminPrograms
    {
        public int? SelectedId { get; set; }
        public string selectedProgram { get; set; }
        public IEnumerable<AdminProgram> AdminProgramsInformation { get; set; }
    }
}
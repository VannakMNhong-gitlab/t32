﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class TrainingGrantApplicantViewModel
    {

        public Guid TrainingGrantId { get; set; }
        public Guid ApplicantGuid { get; set; }
        public int ApplicantId { get; set; }
        public Guid PersonId { get; set; }
        public int DoctoralLevelId { get; set; }
        public string DoctoralLevel { get; set; }
        public int? YearEntered { get; set; }

        public Guid? ProgramId { get; set; } // ProgramId
        public string ProgramListFlat { get; set; }

        public int? DepartmentId { get; set; }
        public string DepartmentName { get; set; }

    }
}
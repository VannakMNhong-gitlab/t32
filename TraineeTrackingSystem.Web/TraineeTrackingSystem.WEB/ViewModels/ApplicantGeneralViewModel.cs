﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class ApplicantGeneralViewModel : PersonVm
    {
        public Guid PersonId { get; set; } //Id in PersonVm is personId
        public int? ApplicantId { get; set; } // ApplicantId
        public int? SelectedId { get; set; }
        public int? DoctoralLevelId { get; set; }
        public IEnumerable<KeyValuePair<Guid, string>> Programs { get; set; }
        public virtual IEnumerable<Guid> SelectedPrograms { get; set; }
        public int DepartmentId { get; set; }
        public IEnumerable<KeyValuePair<int, String>> Departments { get; set; }
        public bool IsTrainingGrantEligible { get; set; }
        public int? IsUnderrepresentedMinority { get; set; }
        public int? IsIndividualWithDisabilities { get; set; }
        public int? IsFromDisadvantagedBkgd { get; set; }
        public int? YearEntered { get; set; }

        //Tracking date and user feature
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public string StrDateLastUpdated { get; set; }  //the date will be formatted

        public string StrLastUpdatedBy { get; set; }
    }
}
﻿//using System;
using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

using TraineeTrackingSystem.Web.PostModels;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class FundingViewModel : NewFundingPostModel
    {
        public Guid Id { get; set; }
        public string selectedId { get; set; }
        public Guid FundingId { get; set; } // FundingId 
        public string Title { get; set; } // Title
        public int? SponsorId { get; set; } // SponsorId        
        public string SponsorName { get; set; } // SponsorName       
        public string SponsorReferenceNumber { get; set; } // SponsorReferenceNumber        
        public int FundingStatusId { get; set; } // FundingStatusId
        public IEnumerable<KeyValuePair<int, string>> FundingStatusName { get; set; }
        public string GrtNumber { get; set; } // GRTNumber   
        //public bool IsRenewal { get; set; } // IsRenewal
        public string PrimeAward { get; set; } // PrimeAward
        public Guid PiFacultyId { get; set; }
        public IEnumerable<KeyValuePair<Guid?, string>> FundingFacultiesList { get; set; }

        public IEnumerable<KeyValuePair<int, String>> Countries { get; set; } //UI data source-dropdownlist 
        public IEnumerable<KeyValuePair<int, String>> States { get; set; }     //UI data source-dropdownlist
        // Add date and user tracking when the form is updated
        public DateTime? DateLastUpdated { get; set; }
        public string StrDateLastUpdated { get; set; }  //the date will be formatted
        public string LastUpdatedBy { get; set; }

        // MVN - Updated 5/14/2015 ->added Date Project start/end
        public DateTime? DateProjectedStart { get; set; } // DateProjectedStart
        public DateTime? DateProjectedEnd { get; set; } // DateProjectedEnd
        //public string SelectedInstitution { get; set; }
        // MVN - added 
        public string selectedInstitution { get; set; } //USES in UI data-bind value sponsor=institution

        // Molley - used by FundingType Fields
        public IEnumerable<KeyValuePair<int, String>> FundingTypes { get; set; }
        public int? FundingTypeId { get; set; } // FundingTypeId
        public Guid? ProgramProjectPdId { get; set; } // ProgramProjectPDId
        public int? PrimeSponsorId { get; set; } // PrimeSponsorId
        public string PrimeSponsorName { get; set; }
        public Guid? PrimeAwardPiId { get; set; } // PrimeAwardPIId













        // .Key bind to sponsorId and selectedInstitution bind to sponsorName  

        //public Guid FundingId { get; set; }
        /*
            public int RankId { get; set; }

            public IEnumerable<KeyValuePair<int, String>> Ranks { get; set; }
         */

        //public Guid PersonId { get; set; }

        //public IEnumerable<KeyValuePair<int, String>> SponsorName { get; set; } // SponsorName      
        /*
            public int? InstitutionId { get; set; }
            public int? DegreeYear { get; set; }
            public string InstitutionName { get; set; }
         */


        //public Guid PiFacultyId { get; set; } // PIPersonId

        //public int? SponsorId { get; set; } // SponsorId
        //public string SponsorName { get; set; } // SponsorName         
       
        //public string FullName { get; set; }
        //public IEnumerable<KeyValuePair<Guid, string>> FullName { get; set; }
      
        
        //public virtual IEnumerable<Guid?> selectedFullName { get; set; }  //USES IN UI DATA-BIND VALUE bind to piFacultyId
        //public virtual IEnumerable<KeyValuePair<Guid, string>> selectedFullName { get; set; } 
        //public virtual IEnumerable<Guid> PiFacultyId { get; set; }
        //public bool IsRenewal { get; set; } // IsRenewal
        
        /* working to resolve PI dropdownlist issue
         */
        //public IEnumerable<FundingsViewModel> Faculties { get; set; }
        //public int? SponsorId { get; set; } // SponsorId

        public string StrLastUpdatedBy { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class TraineeDetailedInformationViewModel
    {
        public Guid Id { get; set; } // Id   
        public Guid TrainingGrantId { get; set; } // TrainingGrantId
        //public Guid FundingId { get; set; }
        public Guid TrainingGrantTraineeId { get; set; }
        public Guid MenteeId { get; set; } // MenteeId
        public Guid? MenteePersonId { get; set; }
        public string MenteeFullName { get; set; }
        public string InstitutionAssociation { get; set; }
       
        //public IEnumerable<KeyValuePair<int, String>> InstitutionAssociation { get; set; }
        public string ResearchProjectTitle { get; set; }
        public string PreDocProgramStatus { get; set; }
        public string PostDocProgramStatus { get; set; }
        /*  ***** TOBE USED collecting ProgramStatus => based on the current DoctoralLevel then display the TraineeStatus **** */
        //public IEnumerable<KeyValuePair<int, String>> TraineeStatus { get; set; }
        public string TraineeStatus { get; set; }
        public DateTime? DateStarted { get; set; } // DateStarted
        //public string StrDateStarted { get; set; }      // the date will be formatted
        public DateTime? DateEnded { get; set; } // DateEnded
        //public string StrDateEnded { get; set; }        // the date will be formated
        //public int? MenteeTypeId { get; set; } // MenteeTypeId
        //public string MenteeType { get; set; } // MenteeType
        //public int? YearStarted { get; set; } // YearStarted
        //public int? YearEnded { get; set; } // YearEnded

       // public IEnumerable<TrainingGrantMenteeViewModel> TrainingPeriodGridDataSource { get; set; }
        public IEnumerable<MenteeTrainingPeriodViewModel> MenteeTrainingPeriods { get; set; }
        //public TrainingGrantMenteesViewModel TrainingPeriodGridDataSource { get; set; }

        public Guid? SelectedTrainingPeriodId { get; set; }
        public bool IsTrainingGrantEligible { get; set; } // IsTrainingGrantEligible
        //public Guid? SelectedIdP { get; set; }
        public int? InstitutionAssociationId { get; set; } // InstitutionAssociationId
        /* ***************************************************************/
        // These data elements map to TrainingGrantTrainee table
        /* ***************************************************************/
        public int? DoctoralLevelId { get; set; } // DoctoralLevelId
        public int? PredocTraineeStatusId { get; set; }
        public int? PostdocTraineeStatusId { get; set; }      
        public string PredocReasonForLeaving { get; set; } // PredocReasonForLeaving
        public string PostdocReasonForLeaving { get; set; } // PostdocReasonForLeaving
        //public virtual IEnumerable<InstitutionAssociation> SelectInstitutions { get; set; }
        //public class InstitutionInfo
        //{
        //    public int InstitutionAssociationId { get; set; }
        //    public string Name { get; set; }
        //}

        //Test
        //public virtual TraineeStatu TraineeStatu_PredocTraineeStatusId { get; set; } // FK_TrainingGrantTrainee_TraineeStatusPredoc
        //public virtual TraineeStatu TraineeStatu_PostdocTraineeStatusId { get; set; } // FK_TrainingGrantTrainee_TraineeStatusPostdoc

        // Track Trainee Detailed Information user update activity
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy
        public string StrDateLastUpdated { get; set; }  //the date will be formatted
        public string StrLastUpdatedBy { get; set; } // LastUpdatedBy

        public string DoctoralLevel { get; set; }
    }
}
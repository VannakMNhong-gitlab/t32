﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class SupportViewModel
    {

        public Guid Id { get; set; } // Id (Primary key)

        public IEnumerable<KeyValuePair<int, String>> SupportTypes { get; set; }
        public IEnumerable<KeyValuePair<int, String>> SupportOrganizations { get; set; }

        //public IEnumerable<KeyValuePair<Guid, string>> FacultyList { get; set; }
        //public IEnumerable<KeyValuePair<int, String>> Countries { get; set; }
        //public IEnumerable<KeyValuePair<int, String>> States { get; set; }
        //public IEnumerable<KeyValuePair<int, String>> Departments { get; set; } // Collection of ALL Depts in db
        //public IEnumerable<KeyValuePair<Guid, String>> Programs { get; set; }   // Collection of ALL Programs in db

        //public virtual IEnumerable<Guid> SelectedPrograms { get; set; }
        ////public Guid? SelectedProgram { get; set; }
        //public virtual IEnumerable<int> SelectedDepartments { get; set; }
        //public virtual Funding Funding { get; set; }


        public string Title { get; set; }
        public int? SupportTypeId { get; set; } // SupportTypeId
        //public IEnumerable<KeyValuePair<int, String>> SupportType { get; set; }
        public int SupportStatusId { get; set; } // TrainingGrantStatusId
        //public IEnumerable<KeyValuePair<int, String>> SupportStatusName { get; set; }
        public string SupportNumber { get; set; } // SupportNumber
        public int? SupportOrganizationId { get; set; } // SupportOrganizationId
        //public IEnumerable<KeyValuePair<int, String>> SupportOrganization { get; set; }
        public DateTime? DateStarted { get; set; } // DateStarted
        public DateTime? DateEnded { get; set; } // DateEnded
        public DateTime? DateCreated { get; set; } // DateCreated
        public string CreatedBy { get; set; } // CreatedBy
        public string CreatedByName { get; set; }
        public DateTime? DateLastUpdated { get; set; } // DateLastUpdated
        public string LastUpdatedBy { get; set; } // LastUpdatedBy

        public string StrDateLastUpdated { get; set; }
        public string StrLastUpdatedBy { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class MenteeMentorsViewModel
    {     
        public Guid? MentorPersonId { get; set; } // MentorPersonId
        public IEnumerable<MenteeMentorViewModel> MenteesGridView { get; set; }
        //public Guid TrainingPeriodId { get; set; } // TrainingPeriodId
        //public Guid MenteePersonId { get; set; } // MenteePersonId
        //public string SelectedMenteeFullName { get; set; } // MenteeFullName
        //public string SelectedMenteeType { get; set; } // MenteeType
        //public string SelectedInstitutionName { get; set; } // InstitutionName
        //public int? SelectedYearStarted { get; set; } // YearStarted
        //public int? SelectedYearEnded { get; set; } // YearEnded
        //public string SelectedCompletedDegree { get; set; } // CompletedDegree
        
    }
}
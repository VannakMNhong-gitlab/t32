﻿using System;
using System.Collections.Generic;

namespace TraineeTrackingSystem.Web.ViewModels
{
    /// <summary>
    /// MVN - data binding model - to be used for loading UI grid elements
    /// </summary>
    public class AdminFacultyPathways
    {
        public int selectedId { get; set; } // ClinicalFacultyPathwayId
        public string selectedClinicalFacultyPathway { get; set; } // ClinicalFacultyPathway      
        public IEnumerable<AdminFacultyPathway> AdminFacultyPathwayInformation { get; set; }
    }
}
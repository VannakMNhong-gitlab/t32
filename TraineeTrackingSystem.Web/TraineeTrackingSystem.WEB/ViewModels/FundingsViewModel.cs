﻿using System;
using System.Collections.Generic;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Web.ViewModels
{
    public class FundingsViewModel
    {
        public Guid Id { get; set; }
        public string selectedId { get; set; }
        public virtual IEnumerable<Guid?> selectedFullName { get; set; }  //USES IN UI DATA-BIND VALUE bind to piFacultyId
        public IEnumerable<KeyValuePair<Guid?, string>> FundingFacultiesList { get; set; }
        public IEnumerable<KeyValuePair<int, String>> Countries { get; set; }
        public IEnumerable<KeyValuePair<int, String>> States { get; set; }
        public IEnumerable<KeyValuePair<int, String>> FundingTypes { get; set; }
        public string selectedInstitution { get; set; } //USES in UI data-bind value sponsor=institution
                                             // .Key bind to sponsorId and selectedInstitution bind to sponsorName   
        public string selectedTitle { get; set; }
        public string selectedGRTNum { get; set; }
        public int? selectedFundingTypeId { get; set; }
        public IEnumerable<KeyValuePair<int, String>> SponsorName { get; set; } // SponsorName    
        public string Title { get; set; } // Title
        public bool IsRenewal { get; set; } // IsRenewal

        /******************************** THESE Might not needed ***********************/     
        public IEnumerable<FundingViewModel> FundingInformations { get; set; }       
        public int IntitutionId { get; set; }  // this replaced from Id 
        public Guid PiFacultyId { get; set; } // PIPersonId
        public int? SponsorId { get; set; } // SponsorId        
        public string GrtNumber { get; set; } // GRTNumber            
        public Guid? selectedPiFacultyId { get; set; } // PIPersonId
        public Guid FundingId { get; set; } // FundingId

        //public Guid Id { get; set; } // Id
        //public Guid PiFacultyId { get; set; } // PIPersonId
        //public int? SponsorId { get; set; } // SponsorId
        //public string SponsorName { get; set; } // SponsorName
        //public bool IsRenewal { get; set; } // IsRenewal
        //public IEnumerable<FundingFaculty> FundingInformations { get; set; }
        //public IEnumerable<FundingViewModel> FundingInformations { get; set; }
        //public string SelectedPi { get; set; }
        //public string SponsorName { get; set; } // SponsorName      
        //public IEnumerable<KeyValuePair<Guid?, String>> FacultiesList { get; set; }
        //public IEnumerable<KeyValuePair<Guid?, String>> FacultyFullName
        //{
        //    get { return PiFirstName + "," + PiLastName; }
        //}
        //public int? PrimaryRoleId { get; set; } // PrimaryRoleId
        //public string LastName { get; set; } // PILastName
        //public string FirstName { get; set; } // PIFirstName
        //public string FullName { get; set; } // PIFullName  
        //public string PiFullName { get; set; } // SponsorName
        //public string FullName { get; set; } // FullName
        // Sponsor      
        //public string selectedSponsorName { get; set; }  //USES IN UI DATA-BIND VALUE
        
        //public string SponsorReferenceNumber { get; set; } // SponsorReferenceNumber
        // Title       
        // GRT Number       
        // Renewal
        //public IEnumerable<KeyValuePair<int, String>> Countries { get; set; }
        //public IEnumerable<KeyValuePair<int, String>> States { get; set; }        
        //public string GrtNumber { get; set; } // GRTNumber

        //public virtual IEnumerable<Guid?> PiFacultyId { get; set; }
        //public bool IsRenewal { get; set; } // IsRenewal
        //public Guid PersonId { get; set; } // PersonId
        //public Guid? PiPersonId { get; set; } // PIPersonId
        //public IEnumerable<KeyValuePair<Guid?, String>> FundingFacultiesList { get; set; }
        //public IEnumerable<KeyValuePair<Guid?, string>> FundingFacultiesList { get; set; }
        //public virtual IEnumerable<Guid?> selectedFullName { get; set; }  //USES IN UI DATA-BIND VALUE bind to piFacultyId
        //public bool IsRenewal { get; set; } // IsRenewal


        //public string Title { get; set; } // Title

    }
}
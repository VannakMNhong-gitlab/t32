﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.ViewModels.Mentee
{
    public class MenteeEditViewModel
    {
        public System.Guid Id { get; set; }
        public string MenteeId { get; set; }
        public string StudentName { get; set; }
        public int InstitutionId { get; set; }
        public int MedicalRankId { get; set; }
        public int DoctoralLevelId { get; set; }
        public bool IsCurrentStudent { get; set; }
        public int DepartmentId { get; set; }
    }
}
﻿using System;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.WebPages;

namespace TraineeTrackingSystem.Web.Common
{
    public static class Utility
    {

        // this is for future expansion
        public static string MailFooter()
        {
            StringBuilder sb = new StringBuilder();

            string osuWMCHelpDesk = ConfigurationManager.AppSettings["OSUWMCHelpDesk"];
            string respondURL = ConfigurationManager.AppSettings["rspURL"];

            sb.Append(@"<a href='");
            sb.Append(respondURL);
            sb.AppendLine(@"'>Click this link to view all Active Requests.</a><br><br> ");

            sb.AppendLine();

            sb.Append(@"Do not reply to this e-mail address - it is not monitored. If you have any questions, please call the <a href='");
            sb.Append(@osuWMCHelpDesk);
            sb.AppendLine(@"'>OSUWMC Help Desk at 293-3861.</a><br>");

            return sb.ToString();
        }
        public static IHtmlString JavaEscape(this String str)
        {
            if (String.IsNullOrEmpty(str))
                return new HtmlString("");
            var html = ((System.Web.Mvc.WebViewPage)
                           WebPageContext.Current.Page).Html;

            var replaced = str.Replace("\\", "\\\\")
                                .Replace("\"", "\\\"")
                                .Replace("'", "\\\'")
                                .Replace("<", "\\<")
                                .Replace(">", "\\>")
                                .Replace(Environment.NewLine, "\\r\\n")
                                .Replace("\n", "\\n");



            return html.Raw(replaced);
        }

    }
}
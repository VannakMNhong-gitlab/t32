﻿using System.Configuration;

namespace TraineeTrackingSystem.Web.Configuration
{
    public static class SettingsManager
    {
        public static string BaseServiceUrl
        {
            get { return ConfigurationManager.AppSettings["BaseServiceUrl"]; }
        }
    }
}
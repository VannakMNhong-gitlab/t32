﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraineeTrackingSystem.Web.Entity
{
    //public class ASOEnum
    //{
    //}
    public enum CustomRoles
    {
        Reader = 1,
        Editor = 2,
        Administrator = 3,
        SuperUser = 4

    }
    public enum ErrorCode
    {
        Success = 0,
        InsertFailure,
        UpdateFailure,
        DeleteFailure,
        InvalidParameters,
        AccessFailure,
        DeniedAccess,
        NotAllowed,
        NotFound,
        NotUnique,
        MultipleRecordsFound,
        RecordExisits,
        RecordApproved,
        NetworkFailure,
        NotKnownFailure,

    }
    public class ErrorMessage
    {
        public string ErrorCode { get; set; }

        public string ReturnId { get; set; }

        public string Message { get; set; }


        public ErrorMessage(string errorCode, string pMessage, string pId)
        {
            this.ErrorCode = errorCode;
            this.Message = pMessage;
            this.ReturnId = pId;
        }
    }
}
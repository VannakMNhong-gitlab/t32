//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TraineeTrackingSystem.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class FacultyResearchInterest_TrainingProgram
    {
        public System.Guid Id { get; set; }
        public System.Guid TrainingProgramId { get; set; }
        public int FacultyResearchInterestId { get; set; }
    
        public virtual FacultyResearchInterest FacultyResearchInterest { get; set; }
        public virtual TrainingProgram TrainingProgram { get; set; }
    }
}

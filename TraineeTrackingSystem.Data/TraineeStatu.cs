//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TraineeTrackingSystem.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class TraineeStatu
    {
        public TraineeStatu()
        {
            this.Trainee_TrainingProgram = new HashSet<Trainee_TrainingProgram>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public int DoctoralLevelId { get; set; }
    
        public virtual ICollection<Trainee_TrainingProgram> Trainee_TrainingProgram { get; set; }
    }
}

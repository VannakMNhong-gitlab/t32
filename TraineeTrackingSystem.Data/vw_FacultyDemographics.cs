//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TraineeTrackingSystem.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_FacultyDemographics
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int DisplayId { get; set; }
        public string EmployeeId { get; set; }
        public string Rank { get; set; }
        public string PrimaryOrganization { get; set; }
        public string SecondaryOrganization { get; set; }
        public string OtherAffiliations { get; set; }
        public string OtherTitles { get; set; }
        public bool IsActive { get; set; }
        public string FullName { get; set; }
        public System.Guid PersonId { get; set; }
        public System.Guid Id { get; set; }
    }
}

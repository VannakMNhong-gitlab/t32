﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using TraineeTrackingSystem.Data;


namespace TraineeTrackingSystem.Repository
{
    public class UserRepository : BaseRepository
    {
        public UserRepository(TTSEntities context) : base(context) { }
        //private readonly CommonDataRepository commonRepository;
        /// <summary>
        /// MVN - this will be called by User Controller to access the Logins View table 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Search_VwLogins> GetUserFromViewTbSearch_VwLogins()
        {
            return context.Search_VwLogins;
        }
        public IEnumerable<Search_VwPersonRoles> GetPersonRoleFromViewTbSearch_VwPersonRoles()
        {
            return context.Search_VwPersonRoles.Where(f => f.IsActive == true);
        }
        /// <summary>
        /// MVN - inserting login information
        /// </summary>
        /// <param name="user"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        //public Login AddUser(Person user, string username)
        public Login AddNewLoginAcct(string lastName, string firstName, string middleName, string username, ClaimsPrincipal User)
        {
            ///FIRST => CREATE a Person Record
            var newPersonRec = new Person{
                PersonId = Guid.NewGuid(),//newLogin.PersonId.HasValue ? newLogin.PersonId.Value : Guid.Empty,
                LastName = lastName,       //Pass in from the controller
                FirstName = firstName,     //pass in from the controller
                MiddleName = middleName,   //Pass in from the controller
                DateLastUpdated = DateTime.Now,
                LastUpdatedBy = (((ClaimsPrincipal)User)
                    .Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value)
            };
            context.Set<Person>().Add(newPersonRec);
            context.SaveChanges();
            ///SECOND =>CREATE a Login Record
            /// NOTE:  in the Login table, loginId a primary key and it is automatically generated, isActive is default to true 
            var newLogin = new Login{                                   //NOTE: PK LoginId will automatically generated 
                PersonId = newPersonRec.PersonId,
                ExternalId = username,                                  //Pass in my the Controller
                IsActive = true,
                Username = (username != null) ? username : "NON",       //Pass in my the Controller
                DateLastUpdated = DateTime.Now,
                LastUpdatedBy = (((ClaimsPrincipal)User)
                    .Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value)
            };
            context.Set<Login>().Add(newLogin);
            context.SaveChanges();

            return newLogin;
        }
        ///////// <summary>
        ///////// MVN - In UI Edit page, ADD NEW ROLE => when the Role item is selected
        ///////// </summary>
        ///////// <param name="User"></param>
        ///////// <param name="loginId"></param>
        ///////// <param name="role"></param>
        ///////// <param name="isActive"></param>
        ///////// <returns></returns>
        //////public PersonRole UpdatePersonalRoleInfommm(ClaimsPrincipal User, PersonRole currenPrsalRole, Role role)
        ////////public PersonRole UpdatePersonalRoleInfo(ClaimsPrincipal User, PersonRole prson, Role role)
        //////{
        //////    PersonRole newRole = null;
        //////    if (currenPrsalRole.RoleId != 0 || role.Id != 0){
        //////        var addPersonRoleRecord = new PersonRole{
        //////            LoginId = currenPrsalRole.LoginId,              //Pass in from the Controller
        //////            //LoginId = loginId.HasValue ? loginId.Value : 0,              //Pass in from the Controller
        //////            RoleId = role.Id,               //pass in from the Controller
        //////            //IsActive = role.IsActive,            //Pass in from the Controller
        //////            //IsDeleted = role.IsDeleted,              //Set Un-Allow Nulls property   
        //////            IsActive = currenPrsalRole.IsActive,
        //////            IsDeleted = currenPrsalRole.IsDeleted,
        //////            DateAssigned = DateTime.Now,
        //////            DateLastUpdated = DateTime.Now,
        //////            LastUpdatedBy = (((ClaimsPrincipal)User)
        //////                .Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value),
        //////            AssignedBy = (((ClaimsPrincipal)User)
        //////                .Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value),

        //////        };
        //////        context.Set<PersonRole>().Add(addPersonRoleRecord);
        //////        context.SaveChanges();
        //////        newRole = addPersonRoleRecord;
        //////    }else{
        //////        newRole.LoginId = currenPrsalRole.LoginId;
        //////        newRole.IsActive = false;
        //////        newRole.IsDeleted = true;
        //////        newRole.DateRemoved = DateTime.Now;
        //////        newRole.RemovedBy = (((ClaimsPrincipal)User)
        //////                .Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
        //////    }

        //////    return newRole;
        //////}

        //////public PersonRole UpdatePersonalRoleInfo(ClaimsPrincipal User, /*string userName,*/ int loginId, Role role, bool isActive, bool isDelete)
        ////////public PersonRole UpdatePersonalRoleInfo(ClaimsPrincipal User, PersonRole prson, Role role)
        //////{

        //////    var addPersonRoleRecord = new PersonRole
        //////    {
        //////        LoginId = loginId,              //Pass in from the Controller
        //////        //LoginId = loginId.HasValue ? loginId.Value : 0,              //Pass in from the Controller
        //////        RoleId = role.Id,               //pass in from the Controller
        //////        //IsActive = role.IsActive,            //Pass in from the Controller
        //////        //IsDeleted = role.IsDeleted,              //Set Un-Allow Nulls property   
        //////        IsActive = isActive,
        //////        IsDeleted = isDelete,
        //////        DateAssigned = DateTime.Now,
        //////        DateLastUpdated = DateTime.Now,
        //////        LastUpdatedBy = (((ClaimsPrincipal)User)
        //////            .Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value),
        //////        AssignedBy = (((ClaimsPrincipal)User)
        //////            .Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value),
        //////    };

        //////    context.Set<PersonRole>().Add(addPersonRoleRecord);
        //////    context.SaveChanges();
        //////    return addPersonRoleRecord;
        //////}

        ///////// <summary>
        ///////// MVN - in UI Edit page, when the role item is selected 
        ///////// </summary>
        ///////// <param name="User"></param>
        ///////// <param name="loginId"></param>
        ///////// <param name="remPersonalRole"></param>
        ///////// <param name="isActive"></param>
        ///////// <returns></returns>
        //////public PersonRole RemovePersonRoleInfo(ClaimsPrincipal User, /*string userName,*/ int loginId, PersonRole remPersonalRole, bool isActive, bool isDelete)
        ////////public PersonRole UpdatePersonalRoleInfo(ClaimsPrincipal User, PersonRole prson, Role role)
        //////{

        //////    var addPersonRoleRecord = new PersonRole
        //////    {
        //////        //LoginId = remPersonalRole.PersonRoles.Where(p =>p.LoginId == loginId).FirstOrDefault().LoginId,              //Pass in from the Controller
        //////        //RoleId = remPersonalRole.PersonRoles.Where(r => r.LoginId == loginId).FirstOrDefault().RoleId,               //pass in from the Controller
        //////        //IsActive = role.IsActive,            //Pass in from the Controller
        //////        //IsDeleted = role.IsDeleted,              //Set Un-Allow Nulls property   
        //////        LoginId = loginId,
        //////        RoleId = remPersonalRole.RoleId,
        //////        IsActive = isActive,
        //////        IsDeleted = isDelete,
        //////        DateAssigned = DateTime.Now,
        //////        DateLastUpdated = DateTime.Now,
        //////        LastUpdatedBy = (((ClaimsPrincipal)User)
        //////            .Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value),
        //////        AssignedBy = (((ClaimsPrincipal)User)
        //////            .Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value),
        //////    };

        //////    //context.Set<PersonRole>().Remove(addPersonRoleRecord);
        //////    context.SaveChanges();
        //////    return addPersonRoleRecord;
        //////}
        //////public PersonRole RemovePersonRoleInfo(ClaimsPrincipal User, /*string userName,*/ int loginId, Role role, bool isActive)
        //////{
        //////    var removedPersonRoleRecord = new PersonRole{
        //////        LoginId = loginId,              //Pass in from the Controller
        //////        RoleId = role.Id,               //pass in from the Controller
        //////        IsActive = false,            //Pass in from the Controller
        //////        IsDeleted = true,              //Set Un-Allow Nulls property   
        //////        DateRemoved = DateTime.Now,
        //////        LastUpdatedBy = (((ClaimsPrincipal)User)
        //////            .Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value),
        //////        RemovedBy = (((ClaimsPrincipal)User)
        //////            .Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value),
        //////    };
        //////    //var removePrsnalRole = context.Set<PersonRole>().Find(loginId);
        //////    //if (removePrsnalRole != null){
        //////    //    removePrsnalRole.IsDeleted = true;
        //////    //    removePrsnalRole.IsActive = false;
        //////    //    removePrsnalRole.DateRemoved = DateTime.Now;
        //////    //    removePrsnalRole.RemovedBy = (((ClaimsPrincipal)User)
        //////    //        .Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
        //////    //    removePrsnalRole.LastUpdatedBy = (((ClaimsPrincipal)User)
        //////    //        .Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
        //////    //    context.SaveChanges();
        //////    //}
        //////    //return removePrsnalRole;
        //////    //context.Set<PersonRole>().Remove(removedPersonRoleRecord);
        //////    context.SaveChanges();
        //////    return removedPersonRoleRecord;

        //////}
        
       
        public Login GetLoginInfoFROMLoginTbl(int loginId)
        {
            Login personLoginRec = context.Logins.Find(loginId);
            return personLoginRec;
        }
        /// <summary>
        /// MVN - 4/29/2016 - to be used in the Edit method 
        /// </summary>
        /// <returns></returns>
        public PersonRole GetPersonRoleFROMPersonRoleDrpDwnTbl(int loginId)
        {
            PersonRole personRole = context.PersonRoles.Where(p => p.LoginId == loginId).FirstOrDefault();//.Find(loginId);
            return personRole;
        }

        public IEnumerable<Role> GetRoleFromRoleTbl()
        {
            return context.Roles;
        }
        /// <summary>
        /// MVN - 4/29/2016 - this will be used to get the person role Info, passing in by loginId
        /// </summary>
        /// <param name="loginId"></param>
        /// <returns></returns>
        //public PersonRole GetPersonRoleFROMPersonRoleTbl(int loginId)
        public IEnumerable<PersonRole> GetPersonRoleFROMPersonRoleTbl(int loginId,int roleId)
        {
            
            return context.PersonRoles.Where(u => u.LoginId == loginId);
        }

        public Search_VwPersonRoles GetPersonRoleFromViewTbSearch_VwPersonRolesById(int loginId) 
        {
            Search_VwPersonRoles personRoleRecord = context.Search_VwPersonRoles.Where(p => p.LoginId == loginId).FirstOrDefault();
            return personRoleRecord;
        }
        
    }
}
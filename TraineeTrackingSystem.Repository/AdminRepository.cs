﻿using System;
using System.Collections.Generic;
using System.Linq;
using TraineeTrackingSystem.Data;
using TraineeTrackingSystem.Common;

namespace TraineeTrackingSystem.Repository
{
    public class AdminRepository : BaseRepository
    {
        public AdminRepository(TTSEntities context)
            :base(context)
        {
           
        }

        public Search_VwInstitutions GetSearch_VwInstitutions(int? id)
        {
            //Search_VwInstitutions inst = context.Search_VwInstitutions.Where(i => i.Id == id);//.Find(id);
            return context.Search_VwInstitutions.Find(id);
        }
        public int GetInstUsage(int? id)
        {
            var instUsage = context.Search_VwInstitutions.FirstOrDefault(i => i.Id == id);
            return instUsage.TotalInstitutionUsage.HasValue ? instUsage.TotalInstitutionUsage.Value : 0;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using TraineeTrackingSystem.Common;
using TraineeTrackingSystem.Data;

using System.Security.Claims;
namespace TraineeTrackingSystem.Repository
{
    public class FacultyRepository: BaseRepository
    {
        public FacultyRepository(TTSEntities context)
            :base(context)
        {
           
        }
        
        

        public IQueryable<Faculty> GetFaculty()
        {
            return context.Faculties;
        }
        //public IQueryable<FacultyRank> GetFacultyRank()
        //{
        //    return context.FacultyRanks;
        //}
        
        public IQueryable<FacultyResearchInterest> getFacultyResearchInterest()
        {
            return context.FacultyResearchInterests;
        }
       //MVN TODO:  Clean this up and use GetOrganization instead
        public IEnumerable<Organization> GetFRInterestOrg()
        {
            return context.Organizations;
        }
        public IEnumerable<Organization> GetOrganization()
        {
            return context.Organizations;
        }
        /// <summary>
        /// MVN - this will allow me to get all data attribute in the Faculty Object
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Faculty> GetAllFtyAttr()
        {
            return context.Faculties;
        }

        /// <summary>
        /// MVN - I use this method for fullname 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Search_VwPerson> GetPerson()
        {
            return context.Search_VwPerson;
        }
        /// <summary>
        /// MVN - I use this method for fullname 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Person> GetOtherPersonAttr()
        {
            return context.People;
        }
        /*
         public IEnumerable<Organization> GetOrganization()
        {
            return context.Organizations;
        }
         */
        public Person getPersonById(Guid personId)
        {
            Person personObj = context.People.Find(personId);
            return personObj;
        }
        /*
         public Program GetProgById(Guid Id)
        {
            Program prog = context.Programs.Find(Id);
            return prog;
        } 
         */
        /// <summary>
        /// MVN - to get to gender list
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Gender> GetGender()
        {
            return context.Genders;
        }
        public IEnumerable<FacultyTrack> GetFtyTrks()
        {
            return context.FacultyTracks;
        }
        public IEnumerable<FacultyAppointment> GetFtyAptmnts()
        {
            return context.FacultyAppointments;
        }
        public IEnumerable<FacultyTrack> GetFtyTrack()
        {
            return context.FacultyTracks;
        }
        public IEnumerable<ClinicalFacultyPathway> GetClinicalFtyPathway()
        {
            return context.ClinicalFacultyPathways;
        }
        public IEnumerable<FacultyRank> GetFtyRnk()
        {
            return context.FacultyRanks;
        }
        /// <summary>
        /// MVN - to get the Ethnicity list
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Ethnicity> GetEthnicity()
        {
            return context.Ethnicities;
        }
        public IEnumerable<OsuTimeline> GetOSUTimeLine()
        {
            return context.OsuTimelines;
        }
        public IQueryable<OsuTimeline> GetOSUtimeLineInfo()
        {
            return context.OsuTimelines;
        }
        public IEnumerable<Applicant> GetApplicantList()
        {
            return context.Applicants;
        }

        public IEnumerable<Program> GetProgram()
        {
            return context.Programs;
        }
        public IEnumerable<AcademicHistory> GetAcademicDegree()
        {
            return context.AcademicHistories;
        }
        public Organization GetOrgById(int? Id)
        {
            Organization org = context.Organizations.Find(Id);
            return org;
        }
        public Program GetProgById(Guid Id)
        {
            Program prog = context.Programs.Find(Id);
            return prog;
        }
        public IEnumerable<DoctoralLevel> GetDoctoralLevelList()
        {
            return context.DoctoralLevels;
        }
        public IEnumerable<Institution> GetInstitution()
        {
            return context.Institutions;
        }

        public IQueryable<Institution> GetIQueryInstitution()
        {
            return context.Institutions;
        }

        public IEnumerable<FacultyAcademicData> GetFtyDegreeInfo()
        {
            return context.FacultyAcademicDatas;
        }
     
        public IQueryable<FacultyAcademicData> getDegreeDegreeInfo()
        {
            return context.FacultyAcademicDatas;
        }
        
        public IEnumerable<AcademicHistory> GetAcademicDegreeHistory()
        {
            return context.AcademicHistories;
        }
        public IQueryable<AcademicDegree> getDegree()
        {
            return context.AcademicDegrees;
        }


        // TODO: this may need to delete - correct way code below using Guid
        public Faculty getFacultyByID(int? Id)
        {
            return context.Faculties.Find(Id);
        }

        public Faculty getFacultyById(Guid Id)
        {
            Faculty faculty = context.Faculties.Find(Id);
            return faculty;
        }

        public Faculty AddFaculty(Person person, string employeeId)
        {
            var newPerson= context.Set<Person>().Add(person);
            context.SaveChanges();
            var newFaculty = new Faculty
            {
                Id=Guid.NewGuid(),
                PersonId = newPerson.PersonId,
                EmployeeId = (employeeId != null) ? employeeId : "NON" + newPerson.DisplayId.ToString("00000"),
                DateLastUpdated = newPerson.DateLastUpdated,
                LastUpdatedBy = newPerson.LastUpdatedBy
            };
            context.Set<Faculty>().Add(newFaculty);
            context.SaveChanges();
            return newFaculty;
        }
        /// <summary>
        /// MVN - Used this to update the person gender info
        /// </summary>
        /// <param name="person"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public Person UpdatePersonRec(Faculty fty, string employee)
        {
            bool isUpdate = fty.Id.Equals(Guid.Empty) || !context.Set<Person>()
                       .Any(f => f.PersonId == fty.PersonId);
            var personUpdRec = isUpdate
                        ? new Person()
                        : context.Set<Person>()
                        .Single(f => f.PersonId == fty.PersonId);
            if (fty.Person.Gender != null){
                personUpdRec.GenderId = fty.Person.GenderId;//.HasValue ? fty.Person.GenderId.Value : 0;
            }
            if (fty.Person.Gender != null){
                personUpdRec.GenderAtBirthId = fty.Person.GenderAtBirthId;//.HasValue ? fty.Person.GenderAtBirthId.Value : 0;
            }
            if (fty.Person.Gender != null){
                personUpdRec.EthnicityId = fty.Person.EthnicityId;//.HasValue ? fty.Person.EthnicityId.Value : 0;
            }
            ////////personUpdRec.GenderId = fty.Person.GenderId.HasValue ? fty.Person.GenderId.Value : 0;
            ////////personUpdRec.GenderAtBirthId = fty.Person.GenderAtBirthId.HasValue ? fty.Person.GenderAtBirthId.Value : 0;
            ////////personUpdRec.EthnicityId = fty.Person.EthnicityId.HasValue ? fty.Person.EthnicityId.Value : 0;
            context.Set<Person>().Add(personUpdRec);
            context.SaveChanges();
            return personUpdRec;

        }

        public ErrorCode AddUpdateFaculty(Faculty newFaculty)
        {
            bool recordFound = false;
            if (newFaculty.EmployeeId  == null)
            {
                context.Faculties.Add(newFaculty);
            }

            else
            {
                // RECORD FOUND        
                Faculty oldFaculty = context.Faculties.Where(f => f.EmployeeId == newFaculty.EmployeeId).FirstOrDefault();
                if (oldFaculty != null)
                {
                    // RECORD FOUND
                    newFaculty.EmployeeId = oldFaculty.EmployeeId;
                    newFaculty.Person.FirstName = oldFaculty.Person.FirstName;
                    newFaculty.Person.LastName = oldFaculty.Person.LastName;
                    newFaculty.Person.MiddleName = oldFaculty.Person.MiddleName;
                 
                    recordFound = true;
                }
            }


            //NO RECORD FOUND
            if (!recordFound)
            {
                //NO RECORD FOUND
                context.Faculties.Add(newFaculty);
            }

            context.SaveChanges();
            ErrorCode result = ErrorCode.Success;
            return result;
        }


        public ErrorCode AddUpdateDegree(FacultyAcademicData newDegree)
        {
            ErrorCode result = ErrorCode.NotFound;
            bool recordFound = false;
            if (newDegree.Id == null)
            {
                context.FacultyAcademicDatas.Add(newDegree);
            }

            else
            {
                // RECORD FOUND        
                FacultyAcademicData oldDegree = context.FacultyAcademicDatas.Where(f => f.Id == newDegree.Id).FirstOrDefault();
                if (oldDegree != null)
                {
                    // RECORD FOUND
                    oldDegree.Id = oldDegree.Id;
                   // oldDegree.DegreeTitle = oldDegree.DegreeTitle;
                    oldDegree.DegreeYear = oldDegree.DegreeYear;
                    

                    recordFound = true;
                }
            }


            //NO RECORD FOUND
            if (!recordFound)
            {
                //NO RECORD FOUND
                context.FacultyAcademicDatas.Add(newDegree);
            }

            context.SaveChanges();
            result = ErrorCode.Success;
            return result;
        }

        /* ------------------------------ DROPDOWN ----------------------------------- */
        //public List<Search_VwFacultyAcademicData>GetAcademicDataByID(Guid personId)
        //{
           
        //    return context.FacultyAcademicDatas.Where(o => o.PersonId == personId);
        //    //PersonId
        //}

        /* ------------------------------ FOR the UI ----------------------------------- */
        public IEnumerable<Search_VwFacultyDemographics> GetSearch_VwFacultyDemographics()
        {
            return context.Search_VwFacultyDemographics;
        }

        public IEnumerable<Search_VwMenteeMentors> GetSearch_VwMenteeMentors(Guid personId)
        {
            return context.Search_VwMenteeMentors.Where(p => p.MentorPersonId == personId);
        }
        /// <summary>
        /// MVN - to be used for the Faculty Funding Information Grid section ->Institutional Training Grants
        /// </summary>
        /// <param name="facultyPersonId"></param>
        /// <returns></returns>
        public IEnumerable<Search_VwTrainingGrantFaculty> GetSearch_VwTrainingGrantFaculty(Guid facultyPersonId)
        {
            return context.Search_VwTrainingGrantFaculty.Where(p => p.FacultyPersonId == facultyPersonId);
        }
        /// <summary>
        /// MVN - to be used for the Faculty Funding Information Grid section -> Other
        /// </summary>
        /// <param name="facultyPersonId"></param>
        /// <returns></returns>
        public IEnumerable<Search_VwFundingFaculty> GetSearch_VwFundingFaculty(Guid Id)
        {
            return context.Search_VwFundingFaculty.Where(p => p.FacultyPersonId == Id);
        }
        


        //Used for Sorting
        public IEnumerable<Search_VwMenteeMentors> GetSearch_VwMenteeMentorsSort()
        {
            return context.Search_VwMenteeMentors;
        }
        public IEnumerable<Search_VwFacultyAcademicData> GetSearch_VwFacultyAcademicData()
        {
            return context.Search_VwFacultyAcademicData;
        }
        
        public IEnumerable<Search_VwFacultyResearchInterests> GetSearch_VwFacultyResearchInterest()
        {
            return context.Search_VwFacultyResearchInterests;
        }


       
        //public IEnumerable<Search_VwFacultyDemographics> Read()
        //{
        //    return context.Search_VwFacultyDemographics;
        //}
    }
}
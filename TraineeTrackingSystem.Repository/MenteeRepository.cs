﻿using System;
using System.Collections.Generic;
using System.Linq;
using TraineeTrackingSystem.Data;
using TraineeTrackingSystem.Common;
using System.Security.Claims;
using System.Security.Principal;
namespace TraineeTrackingSystem.Repository
{
    public class MenteeRepository : BaseRepository
    {
        public MenteeRepository(TTSEntities context)
            :base(context)
        {
           
        }


        public IQueryable<Mentee> GetMentee()
        {
            // MOLLEY WAS HERE TESTING BRANCHING AND MERGING
            return context.Mentees;
        }
        public string getProgramName(Guid programId)
        {
           
            var rtResult = "";
            if (programId != null)
            {
                var result = context.Set<Program>().Find(programId).Title;
                rtResult = result;
            }
            return rtResult;
        }

        public string GetMenteeType(int id)
        {
            var docLvl = context.DoctoralLevels.FirstOrDefault(i => i.Id == id);
            //if(docLvl != null)
            return docLvl != null ? docLvl.LevelName : "";
        }

        ////public IQueryable<Search_VwMenteeDemographics> GetMenteeById(Guid Id)
        ////{
        ////    return context.Search_VwMenteeDemographics.Where(m => m.Id == Id);
            
        ////}
        public Mentee getMenteeById(int? id)
        {
            //throw new NotImplementedException();
            return context.Mentees.Find(id);
        }
        public Mentee GetMenteeById(Guid Id)
        {
            Mentee mentee = context.Mentees.Find(Id);
            return mentee;
        }
        public IEnumerable<Mentee> GetMenteeByPersonId(Guid personId)
        {
            //Mentee menteeDepartment = context.Mentees.Find(personId);
            //return menteeDepartment;
            return context.Mentees.Where(t => t.PersonId == personId);
        }
        public IEnumerable<Mentee> GetMentees()
        {
            //return context.Mentees.Where(x => x.CustomerId == customerId).ToList();
            return context.Mentees;
        }
        public IEnumerable<Organization> GetOrganization()
        {
            return context.Organizations;
        }
        public IQueryable<Publication> getMenteePublication()
        {
            return context.Publications;
        }

        public Mentee AddMentee(Person person, string studentId, bool isTGE)//, IPrincipal User)
        {
              
            var newPerson = context.Set<Person>().Add(person);
            context.SaveChanges();
            var newMentee = new Mentee
            {
                Id = Guid.NewGuid(),
                PersonId = newPerson.PersonId,
                StudentId = (studentId != null) ? studentId : "NON" + newPerson.DisplayId.ToString("00000"),
                IsTrainingGrantEligible = isTGE,
                /// MVN 
                /// Track last updated date and user
                //DateLastUpdated = DateTime.Now,
                //LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value)
                //DateCreated = DateTime.Now
                DateLastUpdated = newPerson.DateLastUpdated,
                LastUpdatedBy = newPerson.LastUpdatedBy
            };
            context.Set<Mentee>().Add(newMentee);
            context.SaveChanges();
            return newMentee;
        }


        public ErrorCode AddUpdateMentee(Mentee newMentee)
        {
            ErrorCode result = ErrorCode.NotFound;
            bool recordFound = false;
            if (newMentee.StudentId  == null)
            {
                context.Mentees.Add(newMentee);
            }

            else
            {
                // RECORD FOUND        
                Mentee oldMentee = context.Mentees.Where(m => m.StudentId == newMentee.StudentId).FirstOrDefault();
                if (oldMentee != null)
                {
                    // RECORD FOUND
                    newMentee.StudentId = oldMentee.StudentId;
                    newMentee.Person.FirstName = oldMentee.Person.FirstName;
                    newMentee.Person.LastName = oldMentee.Person.LastName;
                    newMentee.Person.MiddleName = oldMentee.Person.MiddleName;
                 
                    recordFound = true;
                }
            }


            //NO RECORD FOUND
            if (!recordFound)
            {
                //NO RECORD FOUND
                context.Mentees.Add(newMentee);
            }

            context.SaveChanges();
            result = ErrorCode.Success;
            return result;
        }

        public IEnumerable<Search_VwMenteeDemographics> GetSearch_VwMenteeDemographics()
        {
            return context.Search_VwMenteeDemographics;
        }

        public IEnumerable<Search_VwMenteeData> GetSearch_VwMenteeData()
        {
            return context.Search_VwMenteeData;
        }

    }
}
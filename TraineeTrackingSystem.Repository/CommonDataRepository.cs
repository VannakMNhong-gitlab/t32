﻿using System;
using System.Collections.Generic;
using System.Linq;
using TraineeTrackingSystem.Data;
using System.Security.Claims;
using System.Text.RegularExpressions;





namespace TraineeTrackingSystem.Repository
{
    public class CommonDataRepository : BaseRepository
    {

        public CommonDataRepository(TTSEntities context)
            :base(context)
        {
        }

        public IEnumerable<Person> GetPersonList()
        {
            return context.People.OrderBy(p => p.LastName);
        }
        //public IEnumerable<Search_VwLogins> GetLoginList()
        //{
        //    return context.Search_VwLogins.OrderBy(l => l.LastName);
        //}
       
        public string GetLoginInfo(string extLoginId) {
        //public string GetLoginInfo(string lastUpdatedBy)
        //{
            string loginInfo = "";
            // there is a potential chance that, external Id is null
            if (extLoginId != null){

                //var login = GetLoginList().First(l => l.ExternalId == extLoginId);

                /// MVN - this query returns the default value for the type
                /// also prevent the exception to be thrown, no matter what the result set contain
                /// 
                /// 10/08/15: MPC -- NEED TO CHECK FOR EMPTY LOGIN LIST BEFORE TRYING TO RETURN FIRSTORDEFAULT!!!
                //var login = GetLoginList().Where(l => l.ExternalId == extLoginId).FirstOrDefault();

                //// 12/1/2015: MPC -- There's no need to pull the entire list of logins here -- and the following statement was doing it TWICE!
                //var login = (GetLoginList() != null) ? GetLoginList().Where(l => l.ExternalId == extLoginId).FirstOrDefault() : null;

                //if (login != null){
                //    loginInfo = login.FirstName + " " + login.LastName;
                //}else{
                //    loginInfo = extLoginId;
                //}

                // 12/1/2015: MPC -- the following 5 lines are far more efficient than the above lines
                Search_VwLogins login = null;
                if (context.Search_VwLogins.Count() > 0) {
                    //login = context.Search_VwLogins.Where(l => l.ExternalId == extLoginId).FirstOrDefault();
                    login = context.Search_VwLogins.Where(l => l.Username == extLoginId).FirstOrDefault();
                }

                loginInfo = (login != null) ? login.FirstName + " " + login.LastName : extLoginId;
                
            }
            return loginInfo;
        }
        /// <summary>
        /// MVN - I create this because of the potential data property change =>userName
        /// Also I was using to debug issue with in the lastUpdateBy collum that tie to names, which provide incorrect result
        /// </summary>
        /// <param name="lastLoginBy"></param>
        /// <returns></returns>
        public string GetMedCenterLoginInfo(string lastUpdatedBy)
        {
            string medCenterLoginInfo = "";
            if (lastUpdatedBy != null){                             
                //Search_VwLogins login = null;
                Search_VwPersonRoles login = null;
                if (context.Search_VwLogins.Count() > 0){
                    login = context.Search_VwPersonRoles.Where(l => l.LastUpdatedBy == lastUpdatedBy).FirstOrDefault();//.LastUpdatedBy.ToString();
                    //var login = (GetLoginList() != null) ? GetLoginList().Where(l => l.ExternalId == extLoginId).FirstOrDefault() : null;
                }
                //medCenterLoginInfo = login.LastUpdatedBy != null ? login.LastUpdatedBy : lastUpdatedBy;
                medCenterLoginInfo = (login != null) ? login.FirstName + " " + login.LastName : lastUpdatedBy;

            }
            return medCenterLoginInfo;
        }

        /// <summary>
        /// 05/20/2016 - Created by Molley Collins
        /// Pulls all HTML tags off passed in string and returns scrubbed string  
        /// </summary>
        /// <param name="value">string</param>
        /// <returns>string</returns>
        public string ScrubHtml(string value)
        {
            var step1 = Regex.Replace(value, @"<[^>]+>|&nbsp;", "").Trim();
            var step2 = Regex.Replace(step1, @"\s{2,}", " ");
            return step2;
        }


        public Mentee GetMenteeById(Guid Id)
        {
            Mentee mentee = context.Mentees.Find(Id);
            return mentee;
        }

        public Faculty GetFacultyById(Guid Id)
        {
            Faculty faculty = context.Faculties.Find(Id);
            return faculty;
        }

        public TrainingGrant GetTrainingGrantById(Guid id)
        {
            //throw new NotImplementedException();
            return context.TrainingGrants.Find(id);
        }

        public Funding GetFundingById(Guid id)
        {
            return context.Fundings.Find(id);
        }

        public string GetMenteeFullName(Guid MenteeId)
        {
            Mentee mentee = GetMenteeById(MenteeId);
            if (mentee != null) { 
                var person = GetPersonList().First(p => p.PersonId == mentee.PersonId);
                return person.LastName + ", " + person.FirstName + " " + person.MiddleName;
            } else {
                return string.Empty;
            }
        }

        public string GetFacultyMemberFullName(Guid FacultyId)
        {
            Faculty faculty = GetFacultyById(FacultyId);
            if (faculty != null)
            {
                var person = GetPersonList().First(p => p.PersonId == faculty.PersonId);
                return person.LastName + ", " + person.FirstName + " " + person.MiddleName;
            }
            else
            {
                return string.Empty;
            }
        }

        public string GetTrainingGrantTitle(Guid tgId)
        {
            TrainingGrant tg = GetTrainingGrantById(tgId);

            if (tg != null) {
                return tg.Funding.Title;
            } else {
                return string.Empty;
            }
        }

        public string GetFundingTitle(Guid fundingId)
        {
            Funding funding = GetFundingById(fundingId);

            if (funding != null)
            {
                return funding.Title;
            } else {
                return string.Empty;
            }
        }
       
        public string GetPersonFullName(Guid Id)
        {
            var person = GetPersonList().First(p => p.PersonId == Id);
            return person.LastName + ", " + person.FirstName + " " + person.MiddleName;
        }

        public Guid GetPersonIdForFacultyId(Guid facultyId)
        {
            var faculty = ((facultyId != null) && (facultyId != Guid.Empty)) ? context.Faculties.First(f => f.Id == facultyId) : null;
            return (faculty != null) ? faculty.PersonId : Guid.Empty;
        }

        public Guid GetLoginPersonId(string userName)
        {
            Search_VwLogins loginInfo = null;
            if (context.Search_VwLogins.Count() > 0){
                loginInfo = context.Search_VwLogins.Where(l => l.Username == userName).FirstOrDefault();
            }
            return (loginInfo.PersonId.HasValue != null) ? loginInfo.PersonId.Value : Guid.Empty; 
        }

        public Person GetPerson(Guid personId)
        {
            var person = context.People.First(p => p.PersonId == personId);
            return person;
        }
        
        public IEnumerable<Organization> GetOrganizationList()
        {
            return context.Organizations.OrderBy(o => o.DisplayName);
        }

        public IEnumerable<Institution> GetSponsorList()
        {
            return context.Institutions.OrderBy(i => i.Name);
        }

        public Organization GetOrganizationById(int? id)
        {
            var org = context.Organizations.First(o => o.Id == id);
            return org;
        }
        public IQueryable<Organization> GetDepartmentList()
        {
            return context.Organizations.Where(c => c.OrganizationTypeId == 2);
        }

        public IEnumerable<FundingType> GetFundingTypeList()
        {
            return context.FundingTypes.OrderBy(o => o.Id);
        }

        public IEnumerable<Search_VwSupportTypes> GetSupportTypeList()
        {
            return context.Search_VwSupportTypes.OrderBy(st => st.FullName);
        }
        public IEnumerable<Search_VwSupportOrganizations> GetSupportOrganizationList()
        {
            return context.Search_VwSupportOrganizations.OrderBy(st => st.FullName);
        }
        /// <summary>
        /// MVN - added this mehtod for certain requirement, see TFS task=>9921 
        /// </summary>
        /// <returns></returns>
        public IQueryable<Search_VwFacultyDemographics> GetAllFacultyList()
        {
            return context.Search_VwFacultyDemographics.OrderBy(m => m.FullName);
        }
        public IQueryable<Search_VwFacultyDemographics> GetFacultyList()
        {
            return context.Search_VwFacultyDemographics.Where(f => f.IsActive == true).OrderBy(m => m.FullName);
            /// MVN - return sort name
            /// MPC - The previous statement was already sorting this in ascending order
            //return context.Search_VwFacultyDemographics.Where(f => f.IsActive == true).OrderByDescending(m => m.FullName);        
        }

        public IQueryable<Search_VwApplicantDemographics> GetApplicantNonNullFullNameList()
        {
            return context.Search_VwApplicantDemographics.Where(a => a.FullName != null).OrderBy(a => a.FullName);
        }

        public IQueryable<Search_VwApplicantDemographics> GetApplicantIdList(int yearEntered)
        {
            if (yearEntered > 0) {
                return context.Search_VwApplicantDemographics.Where(a => a.ApplicantId != null && a.YearEntered == yearEntered).OrderBy(a => a.ApplicantId);
            } else {
                return context.Search_VwApplicantDemographics.Where(a => a.ApplicantId != null).OrderBy(a => a.ApplicantId);
            }
        }

        public string GetApplicantIdByGuid(Guid applicantGuid)
        {
            if (applicantGuid != Guid.Empty)
            {
                Applicant applicant = context.Applicants.Find(applicantGuid);//.ApplicantId.ToString();
                if (applicant != null)
                {
                    return applicant.ApplicantId.ToString();
                } else { 
                    return string.Empty; 
                }
            }
            else
            {
                return string.Empty;
            }
        }

        //public IQueryable<FundingDirectCost> GetCurrentStatus() { return context.FundingDirectCosts.OrderBy(c => c.IsCurrent); }
        /// <summary>
        /// MVN - This method will return a list of Mentee with FullName
        /// </summary>
        /// <returns></returns>
        public IQueryable<Search_VwMenteeDemographics> GetMenteeFullNameList()
        {
            return context.Search_VwMenteeDemographics.OrderBy(m => m.FullName);
        }

        public IQueryable<Search_VwMenteeDemographics> GetMenteeNonNullFullNameList()
        {
            return context.Search_VwMenteeDemographics.Where(m => m.FullName != null).OrderBy(m => m.FullName);
        }

        public IQueryable<Search_VwFacultyDemographics> GetFacultyNonNullFullNameList()
        {
            return context.Search_VwFacultyDemographics.Where(f => f.FullName != null).OrderBy(f => f.FullName);
        }
       

        /// <summary>
        /// MVN - Use this list in the Trainee detailed Information grid mentee dropdown
        /// </summary>
        /// <returns></returns>
        public IQueryable<Search_VwTrainingGrantMentees> GetFacultyMenteesList(TrainingGrant trainingGrant)
        {
            //DateTime endDate = (Convert.ToDateTime(trainingGrant.ProjectedStartDate));
            DateTime endDate = (Convert.ToDateTime(trainingGrant.Funding.DateProjectedStart));
            return context.Search_VwTrainingGrantMentees
                .Where(vw => vw.TrainingGrantId == trainingGrant.Id
                        && (((trainingGrant.DoctoralLevelId == 3) || (trainingGrant.DoctoralLevelId.HasValue == false))
                            ? ((vw.MenteeTypeId == 1) || (vw.MenteeTypeId == 2))
                            : (vw.MenteeTypeId == trainingGrant.DoctoralLevelId))
                        && ((vw.TrainingPeriodYearEnded == null)
                            || (vw.TrainingPeriodYearEnded >= (endDate.Year - 10))
                                && (vw.TrainingPeriodYearEnded <= endDate.Year)
                            )
                 )
                .OrderBy(m => m.MenteeFullName);
            //return context.Search_VwTrainingGrantMentees.OrderBy(m => m.MenteeFullName);
        }
        ///////////// <summary>
        ///////////// MVN - 5/18/2015 -> This will be used to filter the faculty list based on the whether they appear in the Participating Grid 
        ///////////// </summary>
        ///////////// <param name="trainingGrant"></param>
        ///////////// <returns></returns>
        //////////public IQueryable<Search_VwFacultyDemographics> GetFacultyFilterBased()
        //////////{          
        //////////    return context.Search_VwFacultyDemographics
        //////////        //.Where(f => f.Id == fty.FundingId)
        //////////        //.Select(f =>f.)
        //////////        .OrderBy(m => m.FullName);
        //////////}

        //////////public Guid GetFtyId(Guid facultyId)
        //////////{
        //////////    var faculty = ((facultyId != null) && (facultyId != Guid.Empty)) ? context.Faculties.First(f => f.Id == facultyId) : null;
        //////////    return (faculty != null) ? faculty.PersonId : Guid.Empty;
        //////////}

        public IQueryable<Search_VwTrainingGrantMentees> GetMenteeList()
        {
            return context.Search_VwTrainingGrantMentees.OrderBy(m => m.FacultyFullName);
        }

        public IEnumerable<Search_VwFundingFaculty> GetFacultyFullNameList()
        {
            return context.Search_VwFundingFaculty.OrderBy(f => f.FacultyFullName);
        }

        public string GetFacultyFullName(Guid fundingId)
        {
            var faculty = GetFacultyFullNameList().Where(f => f.Id == fundingId ).OrderBy(f => f.FacultyFullName);
            //var faculty = GetFacultyFullNameList().First(f => f.FacultyId == fundingId);
            //return faculty.FacultyFullName;
            return faculty.ToString();
        }
        //public IQueryable<Funding> GetFundingList()
        //{
        //    return context.Fundings.OrderBy(m => m.FundingStatu);
        //}
        // MVN - testing
        public IQueryable<Search_VwFacultyDemographics> GetFaculty(string piFullName)
        {
            return context.Search_VwFacultyDemographics.Where(n =>n.FullName.Contains(piFullName)).OrderBy(n => n.FullName);
        }

        public IQueryable<Search_VwMenteeMentors> GetMentee(Guid personId)
        {
            return context.Search_VwMenteeMentors.Where(n => n.MentorPersonId == personId);
        }

        /// <summary>
        /// MVN - 3/2/2015
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        public IQueryable<Search_VwTrainingGrantFaculty> GetInstTrainingGrant(Guid personId)
        {
            return context.Search_VwTrainingGrantFaculty.Where(n => n.FacultyPersonId == personId);
        }
        /// <summary>
        /// MVN - 3/3/2015
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        public IQueryable<Search_VwFundingFaculty> GetOtherFunding(Guid personId)
        {
            return context.Search_VwFundingFaculty.Where(n => n.FacultyPersonId == personId);
        }


        /// <summary>
        /// MVN - 3/13/2015
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        public IQueryable<Search_VwTrainingGrantTrainees> GetTrainingGrantTrainee(Guid id)
        {
            return context.Search_VwTrainingGrantTrainees.Where(n => n.TrainingGrantId == id);
        }
        /// <summary>
        /// MVN - 3/20/2015
        /// List of all TrainingGrantTrainee Detailed 
        /// this method pass in TrainingGrant Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public IEnumerable<Search_VwTrainingGrantTrainees> GetSearch_VwTrainingGrantTrainees(Guid Id)
        {
            return context.Search_VwTrainingGrantTrainees.Where(t => t.TrainingGrantId == Id).OrderBy(m => m.MenteeFullName);
        }

        public IEnumerable<TrainingGrantTrainee> Get_TrainingGrantTrainee(Guid Id)
        {
            /*
                public Mentee GetMenteeById(Guid Id)
                {
                    Mentee mentee = context.Mentees.Find(Id);
                    return mentee;
                }
            */
            return context.TrainingGrantTrainees.Where(t => t.TrainingGrantId == Id);//.OrderBy(.ToList();//.OrderBy(m => m.Mentee.TrainingGrantTrainees);
        }

        public IEnumerable<TrainingPeriod> Get_TrainingPeriods(Guid trainingPeriodId)
        {
            return context.TrainingPeriods.Where(t => t.Id == trainingPeriodId);
        }
        
        //public TrainingPeriod GetTPLastUpdateInfo(Guid personId)
        //{
        //    var trainingPeriod = context.TrainingPeriods.Where(t => t.PersonId == personId && t.IsDeleted == false)
        //        //OrderByDescending methd is to sort the date list from most recent date entry to last date entry
        //        //.OrderBy(t => t.DateLastUpdated != null).ThenByDescending(t => t.DateLastUpdated)
        //        .OrderByDescending(t => t.DateLastUpdated)
        //        .FirstOrDefault();

        //    return trainingPeriod;
        //}
        //public Mentee GetMTLastUpdateInfo(Guid personId)
        //{
        //    var mentee = context.Mentees.Where(t => t.PersonId == personId && t.IsDeleted == false)
        //        //OrderByDescending methd is to sort the date list from most recent date entry to last date entry
        //        .OrderBy(t => t.DateLastUpdated != null).ThenByDescending(t => t.DateLastUpdated)
        //        .FirstOrDefault();

        //    return mentee;
        //}
        //public AcademicHistory GetAHLastUpdateInfo(Guid personId)
        //{
        //    var academic = context.AcademicHistories.Where(t => t.PersonId == personId && t.IsDeleted == false)
        //        //OrderByDescending methd is to sort the date list from most recent date entry to last date entry
        //        .OrderBy(t => t.DateLastUpdated != null).ThenByDescending(t => t.DateLastUpdated)
        //        //.OrderByDescending(t => t.DateLastUpdated)
        //        .FirstOrDefault();

        //    return academic;
        //}
        //public MenteeSupport GetMSLastUpdateInfo(Guid personId)
        //{
        //    var menteeSupport = context.MenteeSupports.Where(t => t.PersonId == personId && t.IsDeleted == false)
        //        //OrderByDescending methd is to sort the date list from most recent date entry to last date entry
        //        .OrderBy(t => t.DateLastUpdated != null).ThenByDescending(t => t.DateLastUpdated)
        //        .FirstOrDefault();

        //    return menteeSupport;
        //}
        //public WorkHistory GetWHLastUpdateInfo(Guid personId)
        //{
        //    var workHistory = context.WorkHistories.Where(t => t.PersonId == personId && t.IsDeleted == false)
        //        //OrderByDescending methd is to sort the date list from most recent date entry to last date entry
        //        .OrderBy(t => t.DateLastUpdated != null).ThenByDescending(t => t.DateLastUpdated)
        //        .FirstOrDefault();

        //    return workHistory;
        //}
        //public Applicant GetAPLastUpdateInfo(Guid personId)
        //{
        //    var applicant = context.Applicants.Where(t => t.PersonId == personId && t.IsDeleted == false)
        //        //OrderByDescending methd is to sort the date list from most recent date entry to last date entry
        //        .OrderBy(t => t.DateLastUpdated != null).ThenByDescending(t => t.DateLastUpdated)
        //        .FirstOrDefault();

        //    return applicant;
        //}
        public Funding GetFDLastUpdateInfo(Guid fundingId)
        {
            var funding = context.Fundings.Where(t => t.Id == fundingId && t.IsDeleted == false)
                //OrderByDescending methd is to sort the date list from most recent date entry to last date entry
                .OrderBy(t => t.DateLastUpdated != null).ThenByDescending(t => t.DateLastUpdated)
                .FirstOrDefault();

            return funding;
        }
        
        /////// <summary>
        /////// Test - getting coutry 
        /////// </summary>
        /////// <param name="fundingId"></param>
        /////// <returns></returns>
        /////// 
        ////public IQueryable<Country> GetCountry(int Id)
        ////{
        ////    //var country = context.Countries.Where(t => t.Id == Id && t.IsDeleted == false);
        ////    //    .OrderBy(t => t.Name);//.DateLastUpdated != null).ThenByDescending(t => t.DateLastUpdated)
        //// // return context.Search_VwFundingDemographics.Where(n => n.PiFacultyId == personId);

        ////    return context.Countries.Where(c =>c.Id == Id);
        ////}

        public FundingDirectCost GetFDDCostLastUpdateInfo(Guid fundingId)
        {
            var fundingDirectCost = context.FundingDirectCosts//.Where(t => t != null && (t.FundingId == fundingId) && (t.IsDeleted == false))
                .Where(t => t.FundingId == fundingId && t.IsDeleted == false)
                //OrderByDescending methd is to sort the date list from most recent date entry to last date entry            
                .OrderBy(t => t.DateLastUpdated != null).ThenByDescending(t => t.DateLastUpdated)
                .FirstOrDefault();
      
            return fundingDirectCost;
            
            
        }
        public TrainingGrant GetTGLastUpdateInfo(Guid fundingId)
        {
            var trainingGrant = context.TrainingGrants.Where(t => t.FundingId == fundingId && t.IsDeleted == false)
                //OrderByDescending methd is to sort the date list from most recent date entry to last date entry
                .OrderBy(t => t.DateLastUpdated != null).ThenByDescending(t => t.DateLastUpdated)
                .FirstOrDefault();

            return trainingGrant;
        }
        public TrainingGrantTrainee GetTGTLastUpdateInfo(Guid trainingGrantId)
        {
            var trainingGtrainee = context.TrainingGrantTrainees.Where(t => t.TrainingGrantId == trainingGrantId && t.IsDeleted == false)
                //OrderByDescending methd is to sort the date list from most recent date entry to last date entry
                //.OrderBy(t => t.DateLastUpdated != null).ThenByDescending(t => t.DateLastUpdated)
                .OrderByDescending(t => t.DateLastUpdated)
                .FirstOrDefault();
            return trainingGtrainee;
        }
        public TraineeSummaryInfo GetTSLastUpdateInfo(Guid trainingGrantId)
        {
            var traineeSummary = context.TraineeSummaryInfoes.Where(t => t.TrainingGrantId == trainingGrantId && t.IsDeleted == false)
                //OrderByDescending methd is to sort the date list from most recent date entry to last date entry
                //.OrderBy(t => t.DateLastUpdated != null).ThenByDescending(t => t.DateLastUpdated)
                .OrderByDescending(t => t.DateLastUpdated != null).ThenByDescending(t => t.DateLastUpdated)
                .FirstOrDefault(t => (t.DateLastUpdated != null));
            
            // Return the TraineeSummaryInfo object ......
            return traineeSummary;
        }
        public IQueryable<Search_VwTrainingGrantTrainees> GetTraineesList()
        {
            return context.Search_VwTrainingGrantTrainees.OrderBy(t => t.MenteeFullName);
        }

        public IQueryable<Search_VwFundingDemographics> GetFund(Guid personId)
        {
            return context.Search_VwFundingDemographics.Where(n => n.PiFacultyId == personId);
        }
        public IEnumerable<Institution> GetInstitutionSponsorList()
        { 
            // return the elements of a sequence in ascending order according to a key
            return context.Institutions.OrderBy(inst => inst.Id); 
        }
       

        ///public IEnumerable<Search_VwTrainingPeriodPrograms> GetTPFromViewTable(){return context.Search_VwTrainingPeriodPrograms;}
        ///public IEnumerable<Search_VwPrograms> GetProgFromViewTable() { return context.Search_VwPrograms; }
        
        public Institution GetDefaultInstitution()
        {
            // Customers would like Institution to default to The Ohio State University (id = 11 in db)
            int id = 11;
            return context.Institutions.First(i => i.Id == id);
        }

        //public Country getCountry(int? id)
        //{
        //    return context.Countries.First(c => c.Id == id);
        //}
        public IQueryable<Country> getCountry()
        {
            //return context.Countries.First(c => c.Name == name);
            //Country cntry = context.Countries.Find(id);
            return context.Countries.Where(c => c.IsDeleted == false).OrderBy(c => c.Name);
        }
        public IQueryable<OrganizationType> getOrgType()
        {
            //return context.Countries.First(c => c.Name == name);
            //Country cntry = context.Countries.Find(id);
            return context.OrganizationTypes.Where(c => c.IsDeleted == false).OrderBy(c => c.OrganizationType_);
        }
       
        //public AcademicDegree GetVariousDegreeName(int? degreeId) 
        //{
        //    //var joinTPDegree = from c in context.Set<TrainingPeriod>() from o in context.Set<AcademicDegree>() 
        //    //    .Include(t =>t.)

        //    var resultJoin = from t in context.TrainingPeriods
        //                 join v in context.AcademicDegrees on t.AcademicDegree.Name  equals v.Name
        //                 select new { where v.Id ==};
 
        //    return context.AcademicDegrees.First(a => a.Id == degreeId); 
        //}
        /// <summary>
        /// This method will be used to get trainee status based on the selected radio button id from the ui
        /// </summary>
        /// <param name="traineeSelectedId"></param>
        /// <returns></returns>
        //public TraineeStatu GetTraineeStatus(int? traineeSelectedId)
        //{
        //    return context.TraineeStatus.FirstOrDefault(t => t.Id == traineeSelectedId);
        //}
        public string GetTraineeStatus(int? traineeSelectedId)
        {
            var traineeStatus = context.TraineeStatus.FirstOrDefault(t => t.Id == traineeSelectedId);
            return traineeStatus != null ? traineeStatus.Name : "";
        }
       
        /*
         public string GetMenteeType(int id)
        {
            var docLvl = context.DoctoralLevels.FirstOrDefault(i => i.Id == id);
            //if(docLvl != null)
            return docLvl != null ? docLvl.LevelName : "";
        }
         */
        public IEnumerable<Search_VwInstitutions> getInstFromViewTable() { return context.Search_VwInstitutions; }
        public IEnumerable<Search_VwFacultyAppointments> getFtyApptmntFromViewTable() { return context.Search_VwFacultyAppointments; }
        public IEnumerable<Search_VwFacultyAcademicHistory> getFtyAcdmicFromViewTable() { return context.Search_VwFacultyAcademicHistory; }
        /// <summary>
        /// MVN - I create this method because I cannot get to other data attribute in the view table =>Search_VwFacultyAcademicHistory
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AcademicHistory> GetFtyAcademicHistoryObject() { return context.AcademicHistories; }
        public IEnumerable<Search_VwAcademicDegrees> getDegreeFromViewTable() { return context.Search_VwAcademicDegrees; }
        public IEnumerable<Search_VwCountries> getCountryFromViewTable() { return context.Search_VwCountries; }
        public IEnumerable<Search_VwStateProvinces> getStateProvinceFromViewTable() { return context.Search_VwStateProvinces; }
        public IEnumerable<Search_VwOrganizationTypes> getTypeOfOrgFromViewTable() { return context.Search_VwOrganizationTypes; }
        public IEnumerable<Search_VwPrograms> getProgramsFromViewTable() { return context.Search_VwPrograms; }
        public IEnumerable<Search_VwFacultyRanks> getFacultyRankFromViewTable() { return context.Search_VwFacultyRanks; }
        public IEnumerable<Search_VwGenders> getFacultyGenderFromViewTable() { return context.Search_VwGenders; }
        public IEnumerable<Search_VwEthnicities> getFacultyEthnicityFromViewTable() { return context.Search_VwEthnicities; }
        public IEnumerable<Search_VwFacultyTracks> getFacultyTrackFromViewTable() { return context.Search_VwFacultyTracks; }
        public IEnumerable<Search_VwClinicalFacultyPathways> getFacultyPathwayFromViewTable() { return context.Search_VwClinicalFacultyPathways; }
        public IEnumerable<Search_VwFundingRoles> getFundingRoleFromViewTable() { return context.Search_VwFundingRoles; }
        public IEnumerable<Search_VwOrganizations> getOrganizationFromViewTable() { return context.Search_VwOrganizations; }
        public IQueryable<Institution> GetInstitutionList(string name)
        {
            //return context.Institutions.Where(i=>i.Name.Contains(name)).OrderBy(i=>i.Name);
            /// MVN - added this 11/12/2015 
            /// where clause to it will not show up in the sponsor list once it removed from the admin page
            return context.Institutions.Where(i => i.Name.Contains(name) && i.IsDeleted == false).OrderBy(i => i.Name);
        }
        public IQueryable<Role> GetUserRoleList(string name)
        {
            return context.Roles.Where(i => i.RoleName.Contains(name) && i.IsDeleted == false).OrderBy(i => i.RoleName);
        }
        public IQueryable<AcademicDegree> GetAcademicDegreeInfoList(string name)
        {
            return context.AcademicDegrees.Where(i => i.Name.Contains(name) && i.IsDeleted == false).OrderBy(i => i.Name);
        }
        //public IEnumerable<Institution> GetCityList(string city)
        //{
        //    return context.Institutions.Where(i => i.City.Contains(city)).OrderBy(ad => ad.City);
        //}
        public IEnumerable<AcademicDegree> GetDegreeList(string name)
        {
            return context.AcademicDegrees.Where(i => i.Name.Contains(name)).OrderBy(ad=>ad.Name);
        }
        public IEnumerable<OrganizationType> GetTypeOfOrgList(string name)
        {
            return context.OrganizationTypes.Where(o => o.OrganizationType_.Contains(name)).OrderBy(org => org.OrganizationType_);
        }
        public IEnumerable<Program> GetProgramsList(string name)
        {
            return context.Programs.Where(p => p.Title.Contains(name)&& p.IsDeleted == false).OrderBy(prog => prog.Title);
        }
        public IEnumerable<Country> GetCountryList(string name)
        {
            return context.Countries.Where(i => i.Name.Contains(name)).OrderBy(ad => ad.Name);
        }
        public IEnumerable<StateProvince> GetStateAbbreviationList(string name)
        {
            return context.StateProvinces.Where(i => i.Abbreviation.Contains(name)).OrderBy(ad => ad.Abbreviation);
        }
        public IEnumerable<StateProvince> GetStateProvinceList(string name)
        {
            return context.StateProvinces.Where(i => i.FullName.Contains(name)).OrderBy(ad => ad.FullName);
        }
        public IEnumerable<FacultyRank> GetFacultyRankList(string name)
        {
            return context.FacultyRanks.Where(f => f.Rank.Contains(name)).OrderBy(f => f.Rank);
        }
        public IEnumerable<Gender> GetFacultyGenderList(string name)
        {
            return context.Genders.Where(f => f.Name.Contains(name)).OrderBy(f => f.Name);
        }
        public IEnumerable<FacultyTrack> GetFacultyTrackList(string name)
        {
            return context.FacultyTracks.Where(f => f.Name.Contains(name)).OrderBy(f => f.Name);
        }
        public IEnumerable<ClinicalFacultyPathway> GetFacultyPathwayList(string name)
        {
            return context.ClinicalFacultyPathways.Where(f => f.Name.Contains(name)).OrderBy(f => f.Name);
        }
        public IEnumerable<Ethnicity> GetFacultyEthnicityList(string name)
        {
            return context.Ethnicities.Where(e => e.Name.Contains(name)).OrderBy(f => f.Name);
        }
        public IEnumerable<FundingRole> GetFtyFundingRole(string name)
        {
            return context.FundingRoles.Where(f => f.Name.Contains(name)).OrderBy(f => f.Name);
        }
        public IEnumerable<Organization> GetOrganization(string name)
        {
            return context.Organizations.Where(o => o.OrganizationId.Contains(name)).OrderBy(o => o.OrganizationId);
        }
        public IEnumerable<Organization> GetOrganizationType(string name)
        {
            return context.Organizations.Where(o => o.DisplayName.Contains(name)).OrderBy(o => o.DisplayName);
        }
        public IEnumerable<Organization> GetOrganizationHRName(string name)
        {
            return context.Organizations.Where(o => o.HrName.Contains(name)).OrderBy(o => o.HrName);
        }
        public IEnumerable<Funding> GetSponsorNameList(string name)
        {
            return context.Fundings.Where(i => i.Title.Contains(name));
        }

        public IEnumerable<AcademicHistory> GetAreaOfStudy(string name)
        {
            return context.AcademicHistories.Where(i => i.AreaOfStudy.Contains(name));
        }

        //public IEnumerable<Search_VwFundingDemographics> GetFundingList()
        //{
        //    return context.Search_VwFundingDemographics;
        //}

        public IQueryable<DoctoralLevel> GetMenteeTypeList()
        {
            return context.DoctoralLevels;
        }
        public string GetMenteeType(int id)
        {
            var docLvl = context.DoctoralLevels.FirstOrDefault(i => i.Id == id);
            //if(docLvl != null)
            return docLvl != null ? docLvl.LevelName : "";
        }
        public IQueryable<TrainingGrant> GetTrainingGrantList()
        {
            return context.TrainingGrants;
        }

        public IQueryable<Program> GetProgram(string name)
        {
            return context.Programs.Where(i => i.Title.Contains(name));
        }
        public IQueryable<Program> PrgLst() { return context.Programs; }
        public IEnumerable<Program> GetProgramList()
        {
            return context.Programs.OrderBy(p => p.Title);
        }
        /// <summary>
        /// MVN - this method is a set of object to be used in Foreach loop
        /// </summary>
        /// <returns></returns>
        public IEnumerable<FundingDirectCost> GetFDDCostsList(Guid fundingId)
        {
            return context.FundingDirectCosts.Where(f => f.FundingId == fundingId);
        }
        public IEnumerable<FacultyResearchInterest> getFacultyResearchInterestList()
        {
            return context.FacultyResearchInterests;
        }

        public IEnumerable<TrainingGrant> GetTrainingGrants()
        {
            return context.TrainingGrants.Where(tg => tg.IsDeleted == false);
        }

        public IEnumerable<FacultyResearchInterest> getFacultyResearchInterestsByFacultyId(Guid facultyId)
        {
            var facPersonId = GetPersonIdForFacultyId(facultyId);
            return (facPersonId != Guid.Empty) ? context.FacultyResearchInterests.Where(ri => ri.PersonId == facPersonId) : null;
        }

        public IEnumerable<InstitutionAssociation> GetInstitutionAssociation()
        {
            return context.InstitutionAssociations.OrderBy(inst =>inst.Name);
        }
        
        private object GetInstitutionId(int Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<FacultyResearchInterest> getFacultyResearchInterestsByPersonId(Guid personId)
        {
            return context.FacultyResearchInterests.Where(ri => ri.PersonId == personId);
        }

        public IEnumerable<Search_VwApplicantPrograms> GetApplicantProgramList(Guid id)
        {
            if (id == null)
            {
                return Enumerable.Empty<Search_VwApplicantPrograms>().ToList();
            }

            IQueryable<Search_VwApplicantPrograms> searchSets = context.Set<Search_VwApplicantPrograms>();
            
            searchSets = searchSets.Where(app => app.PersonId == id);

            return searchSets.ToList();

        }
        /// <summary>
        /// MVN - added this 9/22/2015
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Search_VwPrograms> GetProgFromViewTable() 
        { 
            return context.Search_VwPrograms; 
        }
        public IQueryable<Search_VwApplicantData> GetAllApplicants()
        {
            return context.Search_VwApplicantData.OrderBy(a => a.ApplicantId);
        }

        public IEnumerable<Search_VwTrainingGrantApplicants> GetAllTrainingGrantApplicants()
        {
            return context.Search_VwTrainingGrantApplicants.OrderBy(a => a.ApplicantId);
        }

        public IEnumerable<Search_VwTrainingGrantApplicants> GetTrainingGrantApplicantsByTGId(Guid tgrantId)
        {
            return context.Search_VwTrainingGrantApplicants.Where(tga => tga.TrainingGrantId == tgrantId).OrderBy(a => a.ApplicantId);
        }

        public IEnumerable<Search_VwFundingProgramProjectData> GetSearch_VwFundingProgramProjectData(Funding parentFund)
        {
            return context.Search_VwFundingProgramProjectData
                //.Where(vw => vw.Id == parentFund.Id)
                .Where(vw => vw.GrtNumber == parentFund.GrtNumber
                    && (vw.Id != parentFund.Id))
                .OrderBy(vw => vw.DateProjectedStart);
        }

        public IEnumerable<Search_VwTrainingGrantApplicants> GetSearch_VwTrainingGrantApplicants(TrainingGrant tgrant)
        {
            return context.Search_VwTrainingGrantApplicants
                .Where(vw => vw.TrainingGrantId == tgrant.Id
                        && (((tgrant.DoctoralLevelId == 3) || (tgrant.DoctoralLevelId == null))
                            ? ((vw.ApplicantTypeId == 1) || (vw.ApplicantTypeId == 2))
                            : (vw.ApplicantTypeId == tgrant.DoctoralLevelId))
                )
                .OrderBy(app => app.ApplicantId);
        }

        public FundingDirectCost FundingDirectCostList(Guid fundingId)
        {
            var fundingCost = context.FundingDirectCosts.First(fdCost => fdCost.FundingId == fundingId);
            return fundingCost;
        }
        /*
          public Person GetPerson(Guid personId)
        {
            var person = context.People.First(p => p.PersonId == personId);
            return person;
        }
         */ 
        //[vw_FundingCostInformation]

        //  Get all Faculty:Mentee records that are associated with the passed in TrainingGrant
        //  Filter mentees by MenteeType = TrainingGrantType (unless TrainingGrantType = Both or NULL), 
        //  and Mentee Training Period End Year is NULL (still attending)  
        //  OR Training Period Start OR End date fall within last 10 years from the TG Projected Start Date
        public IEnumerable<Search_VwTrainingGrantMentees> GetSearch_VwTrainingGrantMentees(TrainingGrant tgrant)
        {

            DateTime endDate = (Convert.ToDateTime(tgrant.Funding.DateProjectedStart));

            return context.Search_VwTrainingGrantMentees
                .Where(vw => vw.TrainingGrantId == tgrant.Id
                        && ( ((tgrant.DoctoralLevelId == 3) || (tgrant.DoctoralLevelId.HasValue == false))
                            ? ((vw.MenteeTypeId == 1) || (vw.MenteeTypeId == 2))
                            : (vw.MenteeTypeId == tgrant.DoctoralLevelId) )
                        && ((vw.TrainingPeriodYearEnded == null)
                            || ( ((vw.TrainingPeriodYearStarted >= (endDate.Year - 10))
                                && (vw.TrainingPeriodYearStarted <= endDate.Year)) 
                                ||   (vw.TrainingPeriodYearEnded >= (endDate.Year - 10))
                                        && (vw.TrainingPeriodYearEnded <= endDate.Year) )                        
                            )                        
                 )
                .OrderBy(m => m.MenteeFullName);
        }

        //  Get all Faculty:Funding records
        public IEnumerable<Search_VwFundingFaculty> GetSearch_VwFundingFaculty()
        {
            return context.Search_VwFundingFaculty.OrderBy(fac => fac.FacultyFullName);
        }
        
        //  Get all Faculty:Funding records that are associated with the passed in Funding
        public IEnumerable<Search_VwFundingFaculty> GetSearch_VwFundingFacultyByFundingId(Guid fundingId)
        {
            return context.Search_VwFundingFaculty.Where(ff => ff.Id == fundingId).OrderBy(fac => fac.FacultyFullName);
        }
        
        public List<Guid> GetFundingFacultyIdsByFundingId(Guid fundingId)
        {
            List<Guid> facultyIdList = new List<Guid>();
            var faculty = context.FundingFaculties.Where(ff => ff.FundingId == fundingId && ff.IsDeleted == false);

            foreach (var fac in faculty) {
                facultyIdList.Add(fac.FacultyId);
            }

            return facultyIdList;
        }

        //  Get all Faculty:TrainingGrant records
        public IEnumerable<Search_VwTrainingGrantFaculty> GetSearch_VwTrainingGrantFaculty()
        {
            return context.Search_VwTrainingGrantFaculty.OrderBy(fac => fac.FacultyFullName);
        }

        public List<Guid> GetOtherGrantsAssociatedToFundingId(Guid fundingId, bool openGrantsOnly, bool completedGrantsOnly)
        {
            List<Guid> tgrantIdList = new List<Guid>();

            // ** Get all Faculty associated with this TrainingGrant/Funding
            var associatedFaculty = GetFundingFacultyIdsByFundingId(fundingId);

            // ** Get all Funding associated with each associated Faculty member
            var tgrants = GetSearch_VwTrainingGrantFaculty()
                    .Where(ff => associatedFaculty.Contains(ff.FacultyId));

            foreach (var tgrant in tgrants)
            {

                // Make sure current grant is not included in it's own list of associated grants
                if (tgrant.FundingId != fundingId) {

                    // if openGrantsOnly = true, only return grants in "open" (pending (2) or active (3)) status
                    if (openGrantsOnly) {
                        if ((tgrant.TrainingGrantStatusId == 2) || (tgrant.TrainingGrantStatusId == 3))
                        {
                            tgrantIdList.Add(tgrant.TrainingGrantId);
                        }                        
                    }

                    // if completedGrantsOnly = true, only return grants in "completed" (4) status
                    else if (completedGrantsOnly) {
                        if (tgrant.TrainingGrantStatusId == 4)
                        {
                            tgrantIdList.Add(tgrant.TrainingGrantId);
                        }

                    } else {
                        // otherwise, return all grants, no matter what the status
                        tgrantIdList.Add(tgrant.TrainingGrantId);
                    }
                }
                
            }

            // Return list of Funding Ids
            return tgrantIdList;
        }

        public List<Guid> GetOtherFundingAssociatedToFundingId(Guid fundingId, bool openFundsOnly, bool completedFundsOnly)
        {
            List<Guid> fundingIdList = new List<Guid>();

            // ** Get all Faculty associated with this TrainingGrant/Funding
            var associatedFaculty = GetFundingFacultyIdsByFundingId(fundingId);

            // ** Get all Funding associated with each associated Faculty member
            var funds = GetSearch_VwFundingFaculty()
                    .Where(ff => associatedFaculty.Contains(ff.FacultyId));

            foreach (var fund in funds)
            {
                // if openFundsOnly = true, only return funds in "open" (pending (2) or active (4)) status
                if (openFundsOnly) {
                    if ((fund.FundingStatusId == 2) || (fund.FundingStatusId == 4))
                    {
                        fundingIdList.Add(fund.Id);
                    }

                // if completedFundsOnly = true, only return funds in "completed" (5) status
                } else if (completedFundsOnly) {
                    if (fund.FundingStatusId == 5)
                    {
                        fundingIdList.Add(fund.Id);
                    }

                } else {
                    // otherwise, return all funds, no matter what the status
                    fundingIdList.Add(fund.Id);
                }
            }

            // Return list of Funding Ids
            return fundingIdList;
        }

        //  Get all Faculty:Funding records that are associated with the passed in FundingFacultys
        //public IEnumerable<Search_VwFundingFaculty> GetSearch_VwFundingFacultyByFundingId(IQueryable<FundingFaculty> fundingFacs)
        //{
        //    return context.Search_VwFundingFaculty.Where(ff => fundingFacs.Contains(ff.FacultyId ).OrderBy(fac => fac.FacultyFullName);
        //}

        //public IEnumerable<Search_VwTrainingGrantApplicants> GetTGApplicantsByDeptOrProgram(TrainingGrant tgrant)
        //{
        //    if ((tgrant == null))
        //    {
        //        return Enumerable.Empty<Search_VwTrainingGrantApplicants>().ToList();
        //    }

        //    IQueryable<Search_VwTrainingGrantApplicants> searchSets = context.Set<Search_VwTrainingGrantApplicants>();

        //    //if (searchSets != null)
        //    //{
        //    //    foreach (var applicant in searchSets)
        //    //    {
        //    //        foreach (var appProgId in applicant.Program)
        //    //        {
        //    //            if (tgrant.Programs.All(tgp => tgp.Id != appProgId))
        //    //            {
        //    //                var addProgram = _context.Set<Program>().Find(applicant);
        //    //                tgrant.Programs.Add(addProgram);
        //    //            }
        //    //        }
        //    //    }
        //    //}

        //    //searchSets = searchSets.Where(app => (app.ProgramId == programId) || (app.DepartmentId == deptId));
        //    //searchSets = searchSets.Where(app => (tgrant.Programs.Any(tgp => tgp.Id == app.ProgramId)) || (tgrant.Organizations.Any(tgo => tgo.Id == app.DepartmentId)) );
        //    searchSets = searchSets.Where(app => app.TrainingGrantId == tgrant.Id);

        //    return searchSets.ToList();

        //}


        public IEnumerable<Search_VwFacultyPrograms> GetFacultyProgramList(Guid id)
        {
            if (id == null)
            {
                return Enumerable.Empty<Search_VwFacultyPrograms>().ToList();
            }

            IQueryable<Search_VwFacultyPrograms> searchSets = context.Set<Search_VwFacultyPrograms>();

            searchSets = searchSets.Where(fp => fp.FacultyId == id);

            return searchSets.ToList();

        }

        public IEnumerable<Search_VwFundingDemographics> GetFundingsList(Guid id)
        {
            if (id == null)
            {
                return Enumerable.Empty<Search_VwFundingDemographics>().ToList();
            }

            IQueryable<Search_VwFundingDemographics> searchSets = context.Set<Search_VwFundingDemographics>();

            searchSets = searchSets.Where(app => app.PiFacultyId == id);

            return searchSets.ToList();

        }

        public IEnumerable<Search_VwTrainingGrantFacultyWithResearchInterests> GetTrainingGrantResearchInterestList(Guid GrantId, Guid FacultyId)
        {
            if ((GrantId == null) || (FacultyId == null))
            {
                return Enumerable.Empty<Search_VwTrainingGrantFacultyWithResearchInterests>().ToList();
            }

            IQueryable<Search_VwTrainingGrantFacultyWithResearchInterests> searchSets = context.Set<Search_VwTrainingGrantFacultyWithResearchInterests>();

            searchSets = searchSets.Where( tgri => ((tgri.TrainingGrantId == GrantId) && (tgri.FacultyId == FacultyId)) );

            return searchSets.ToList();

        }

        public IEnumerable<Search_VwTrainingPeriodPrograms> GetTrainingPeriodProgramList(Guid id)
        {
            if (id == null)
            {
                return Enumerable.Empty<Search_VwTrainingPeriodPrograms>().ToList();
            }

            IQueryable<Search_VwTrainingPeriodPrograms> searchSets = context.Set<Search_VwTrainingPeriodPrograms>();

            searchSets = searchSets.Where(tpp => tpp.TrainingPeriodId == id);

            return searchSets.ToList();

        }

        public Guid GetTrainingPeriodProgram(Guid id)
        {
            if (id == null)
            {
                //return Enumerable.Empty<Search_VwTrainingPeriodPrograms>().ToList();
                return Guid.Empty;
            }

            IQueryable<Search_VwTrainingPeriodPrograms> searchSets = context.Set<Search_VwTrainingPeriodPrograms>();

            //searchSets = searchSets.Where(tpp => tpp.TrainingPeriodId == id);
            var firstProg = searchSets.FirstOrDefault(tpp => tpp.TrainingPeriodId == id);

            return firstProg != null ? firstProg.ProgramId : Guid.Empty;

        }
        public int GetUserRoles(int roleId)
        {
            //if (loginId == null){
            //    return 0;
            //}

            IQueryable<PersonRole> searchRoleSets = context.Set<PersonRole>();
            var firstPrsRole = searchRoleSets.FirstOrDefault(r => r.RoleId == roleId);
            var findPrsRole = searchRoleSets.Where(l => l.RoleId == roleId).Select(r => r.RoleId);

            //context.SaveChanges();
            return firstPrsRole != null ? firstPrsRole.RoleId : 0;
        }

        ///////////// <summary>
        ///////////// ------------------------------------ TO REMOVE ---------------------------------------
        ///////////// </summary>
        ///////////// <param name="id"></param>
        ///////////// <returns></returns>
        //////////public int GetInstState(int id)
        //////////{
        //////////    //IQueryable<Search_VwInstitutions> searchSets = context.Set<Search_VwInstitutions>();
        //////////    IQueryable<Institution> searchSets = context.Set<Institution>();
        //////////    var firstInst = searchSets.FirstOrDefault(tpp => tpp.Id == id);

        //////////    return firstInst.StateId.HasValue ? firstInst.StateId.Value : 0; 
        //////////}
        //////////public int GetInstCountry(int id)
        //////////{
        //////////    IQueryable<Search_VwInstitutions> searchSets = context.Set<Search_VwInstitutions>();
        //////////    var firstInst = searchSets.FirstOrDefault(tpp => tpp.Id == id);

        //////////    return firstInst.CountryId.HasValue ? firstInst.CountryId.Value : 0;
        //////////}
        //////////public int GetInstUsage(int id)
        //////////{
        //////////    IQueryable<Search_VwInstitutions> searchSets = context.Set<Search_VwInstitutions>();
        //////////    var firstInst = searchSets.FirstOrDefault(tpp => tpp.Id == id);

        //////////    return firstInst.TotalInstitutionUsage.HasValue ? firstInst.TotalInstitutionUsage.Value : 0;
        //////////}
        /// ------------------------------------------------------------------------------------------------------
        //public Guid GetDeptment(Guid persionId)
        //{
        //    if (persionId == null){
        //        return Guid.Empty;
        //    }
        //    IQueryable<Mentee> searchSets = context.Set<Mentee>();
        //    var frstDept = searchSets.FirstOrDefault(dpt => dpt.PersonId == persionId);
        //    return frstDept != null ? frstDept.DepartmentId.HasValue ? frstDept.DepartmentId.Value : 0;
        //}
        //public string Program(Guid programId)
        //{
        //    if (id == null)
        //    {
        //        //return Enumerable.Empty<Search_VwTrainingPeriodPrograms>().ToList();
        //        return Guid.Empty;
        //    }

        //    IQueryable<Search_VwTrainingPeriodPrograms> searchSets = context.Set<Search_VwTrainingPeriodPrograms>();

        //    //searchSets = searchSets.Where(tpp => tpp.TrainingPeriodId == id);
        //    var firstProg = searchSets.FirstOrDefault(tpp => tpp.TrainingPeriodId == id);

        //    return firstProg != null ? firstProg.ProgramId : Guid.Empty;

        //}

        public IEnumerable<Search_VwTrainingPeriodMentors> GetTrainingPeriodMentorList(Guid id)
        {
            if (id == null)
            {
                return Enumerable.Empty<Search_VwTrainingPeriodMentors>().ToList();
            }

            IQueryable<Search_VwTrainingPeriodMentors> searchSets = context.Set<Search_VwTrainingPeriodMentors>();

            searchSets = searchSets.Where(tpp => tpp.TrainingPeriodId == id);

            return searchSets.ToList();

        }

        public IEnumerable<Search_VwTrainingGrantPrograms> GetTrainingGrantProgramList(Guid id)
        {
            if (id == null)
            {
                return Enumerable.Empty<Search_VwTrainingGrantPrograms>().ToList();
            }

            IQueryable<Search_VwTrainingGrantPrograms> searchSets = context.Set<Search_VwTrainingGrantPrograms>();

            searchSets = searchSets.Where(tgp => tgp.TrainingGrantId == id);

            return searchSets.ToList();

        }

        public IEnumerable<Search_VwTrainingGrantDepartments> GetTrainingGrantDepartmentList(Guid id)
        {
            if (id == null)
            {
                return Enumerable.Empty<Search_VwTrainingGrantDepartments>().ToList();
            }

            IQueryable<Search_VwTrainingGrantDepartments> searchSets = context.Set<Search_VwTrainingGrantDepartments>();

            searchSets = searchSets.Where(tgd => tgd.TrainingGrantId == id);

            return searchSets.ToList();

        }

        public FundingFaculty getFundingFacultybyId(Guid Id, int roleId)
        {
            FundingFaculty fundingFaculty = context.FundingFaculties.Find(Id);

            //return context.FundingFaculties.First(f => f.FundingId == Id && f.PrimaryRoleId == 1);

            return context.FundingFaculties.First(f => f.FundingId == Id && f.FundingRole_PrimaryRoleId.Id == roleId);
            /****************************************************************/
        }
        public IQueryable<FundingStatu> GetFundingStatus()
        {
            return context.FundingStatus;
        }

        public IQueryable<TrainingGrantStatu> GetTrainingGrantStatus()
        {
            return context.TrainingGrantStatus;
        }
        public IQueryable<Country> GetCountry()
        {
            return context.Countries;
        }
        /// <summary>
        /// MVN - paassing will be the DoctoralId 
        /// </summary>
        /// <param name="doctoralLevelId"></param>
        /// <returns></returns>
        public IEnumerable<TraineeStatu> GetTraineeStatus(int doctoralLevelId)
        {
            TraineeStatu traineeStatus = context.TraineeStatus.Find(doctoralLevelId);

            IQueryable<TraineeStatu> traineeStatusDataSet = context.Set<TraineeStatu>();
            if (traineeStatus.DoctoralLevelId == 1){
                traineeStatusDataSet = traineeStatusDataSet.Where(t => t.Name.Contains(traineeStatus.Name));
            }
            if (traineeStatus.DoctoralLevelId == 2){
                traineeStatusDataSet = traineeStatusDataSet.Where(t => t.Name.Contains(traineeStatus.Name));
            }
            return traineeStatusDataSet.ToList();
        }
        
        public IEnumerable<Search_VwFacultyAcademicData> GetFCTYDegree(int degreeYear, string degree, string otherDegree)
        {
            if (string.IsNullOrEmpty(degreeYear.ToString())
                && string.IsNullOrEmpty(degree)
                && string.IsNullOrEmpty(otherDegree))

                return Enumerable.Empty<Search_VwFacultyAcademicData>().ToList();

            IQueryable<Search_VwFacultyAcademicData> searchSets = context.Set<Search_VwFacultyAcademicData>();
            if (!string.IsNullOrEmpty(degreeYear.ToString()))
            {
                searchSets = searchSets.Where(o => o.FirstName.Contains(degreeYear.ToString()));
            }

            if (!string.IsNullOrEmpty(degree))
            {
                searchSets = searchSets.Where(o => o.LastName.Contains(degree));
            }

            if (!string.IsNullOrEmpty(otherDegree))
            {
                searchSets = searchSets.Where(o => o.MiddleName.Contains(otherDegree));
            }

            return searchSets.ToList();

        }

        public IEnumerable<Search_VwFacultyResearchInterests> GetFCTYResearchInterest(string titleName, string orgDisplayName)
        {
            if (string.IsNullOrEmpty(titleName)
                && string.IsNullOrEmpty(orgDisplayName))
                
                return Enumerable.Empty<Search_VwFacultyResearchInterests>().ToList();

            IQueryable<Search_VwFacultyResearchInterests> researchInterestSet = context.Set<Search_VwFacultyResearchInterests>();
            if (!string.IsNullOrEmpty(titleName))
            {
                researchInterestSet = researchInterestSet.Where(o => o.ResearchInterest.Contains(titleName));
            }

            //if (!string.IsNullOrEmpty(orgDisplayName))
            //{
            //    researchInterestSet = researchInterestSet.Where(o => o.ResearchInterestDeptDisplayName.Contains(orgDisplayName));
            //}
            return researchInterestSet.ToList();

        }

        public IEnumerable<Search_VwFacultyDemographics> SearchFaculty(string firstName, string lastName, string middleName, string employeeId)
        {
            if (string.IsNullOrEmpty(firstName)
                && string.IsNullOrEmpty(lastName)
                && string.IsNullOrEmpty(middleName)
                && string.IsNullOrEmpty(employeeId))
                return Enumerable.Empty<Search_VwFacultyDemographics>().ToList();

            IQueryable<Search_VwFacultyDemographics> searchSets = context.Set<Search_VwFacultyDemographics>();
            if (!string.IsNullOrEmpty(firstName))
            {
                searchSets = searchSets.Where(o => o.FirstName.Contains(firstName));
            }

            if (!string.IsNullOrEmpty(lastName))
            {
                searchSets = searchSets.Where(o => o.LastName.Contains(lastName));
            }

            if (!string.IsNullOrEmpty(middleName))
            {
                searchSets = searchSets.Where(o => o.MiddleName.Contains(middleName));
            }

            if (!string.IsNullOrEmpty(employeeId))
            {
                searchSets = searchSets.Where(o => o.EmployeeId.Contains(employeeId));
            }
           
            return searchSets.ToList();
            
        }
        /// <summary>
        /// MVN - created 4/27/2016 
        /// This will be used to search the Person Role VIEW table and return match set
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="middleName"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public IEnumerable<Search_VwPersonRoles> SearchUser(string userName, string firstName, string lastName, string middleName, string role)
        {
            if (string.IsNullOrEmpty(firstName)
                && string.IsNullOrEmpty(lastName)
                && string.IsNullOrEmpty(middleName)
                && string.IsNullOrEmpty(userName))
                return Enumerable.Empty<Search_VwPersonRoles>().ToList();

            IQueryable<Search_VwPersonRoles> searchSets = context.Set<Search_VwPersonRoles>();
            if (!string.IsNullOrEmpty(userName)){
                searchSets = searchSets.Where(o => o.Username.Contains(userName));
            }

            if (!string.IsNullOrEmpty(firstName)){
                searchSets = searchSets.Where(o => o.FirstName.Contains(firstName));
            }

            if (!string.IsNullOrEmpty(lastName)){
                searchSets = searchSets.Where(o => o.LastName.Contains(lastName));
            }

            if (!string.IsNullOrEmpty(middleName)){
                searchSets = searchSets.Where(o => o.MiddleName.Contains(middleName));
            }
            if (!string.IsNullOrEmpty(role))
            {
                searchSets = searchSets.Where(o => o.RoleName.Contains(role));
            }           

            return searchSets.ToList();

        }

        // +++++++++++++++++++++++ MVN is WORKING Here +++++++++++++++++++++++++++++++++++++++++++ //
        public IEnumerable<Search_VwFundingDemographics> SearchForFundings(Guid piFacultyId, string SponsorName, string Title, string grtNumber, int? FundingTypeId)
        {
            if ((piFacultyId == Guid.Empty)
                && string.IsNullOrEmpty(SponsorName)
                && string.IsNullOrEmpty(Title)
                && string.IsNullOrEmpty(grtNumber)
                && (FundingTypeId == 0))
                return Enumerable.Empty<Search_VwFundingDemographics>().ToList();

            IQueryable<Search_VwFundingDemographics> searchSets = context.Set<Search_VwFundingDemographics>();
            
            if (piFacultyId != null)
            {
                // add PI Role here 
                searchSets = searchSets.Where(f => f.PiFacultyId == piFacultyId);
            }
            if (!string.IsNullOrEmpty(SponsorName))
            {
                searchSets = searchSets.Where(s => s.SponsorName.Contains(SponsorName));
            }
            if (!string.IsNullOrEmpty(Title))
            {
                searchSets = searchSets.Where(s => s.Title.Contains(Title));
            }
            if (!string.IsNullOrEmpty(grtNumber))
            {
                searchSets = searchSets.Where(g => g.GrtNumber.Contains(grtNumber));
            }
            if (FundingTypeId != 0)
            {
                searchSets = searchSets.Where(f => f.FundingTypeId == FundingTypeId);
            }
            return searchSets.ToList();
        }      

        public List<Search_VwPerson> SearchPerson(string firstName, string lastName, string middleName)
        {
            if (string.IsNullOrEmpty(firstName)
                && string.IsNullOrEmpty(lastName)
                && string.IsNullOrEmpty(middleName))
                return Enumerable.Empty<Search_VwPerson>().ToList();

            IQueryable<Search_VwPerson> searchSets=context.Set<Search_VwPerson>() ;
            if (!string.IsNullOrEmpty(firstName))
            {
                searchSets = searchSets.Where(o => o.FirstName.Contains(firstName));
            }

            if (!string.IsNullOrEmpty(lastName))
            {
                searchSets = searchSets.Where(o => o.LastName.Contains(lastName));
            }

            if (!string.IsNullOrEmpty(middleName))
            {
                searchSets = searchSets.Where(o => o.MiddleName.Contains(middleName));
            }

            return searchSets.ToList();

        }

        public List<Search_VwPerson> SearchEmployees(string partialName)
        {
            List<Search_VwPerson> mPartials = new List<Search_VwPerson>();

            try
            {
                OSU.Medical.Entity.Employee[] employees = OSU.Medical.BusFacade.Enterprise.EmpFacade.FindEmployee(partialName, true);

                if (employees != null)
                {
                    foreach (OSU.Medical.Entity.Employee emp in employees)
                    {
                        Search_VwPerson mPartial = new Search_VwPerson();

                        //mPartial.UserName = emp.LanId;
                        //mPartial.Email = emp.EmailAddress;
                        //mPartial.Name = emp.FirstName + " " + emp.LastName;
                        //mPartial.DepartmentName = emp.Dept;
                        //mPartial.DepartmentID = emp.DeptId;
                        //mPartial.Title = emp.JobTitle;
                        //mPartial.Phone = emp.Phone;
                        //mPartial.PhonePreferred = emp.Phone;

                        mPartials.Add(mPartial);
                    }
                }

            }
            catch (Exception)
            { }

            return mPartials;
        }

        public Search_VwPerson GetEmployeeDetails(string userName)
        {
            Search_VwPerson mPartial = null;
            if (!String.IsNullOrEmpty(userName))
            {
                try
                {
                    OSU.Medical.Entity.Employee searchEmp = new OSU.Medical.Entity.Employee();
                    searchEmp.LanId = userName;
                    OSU.Medical.Entity.Employee emp = OSU.Medical.BusFacade.Enterprise.EmpFacade.FindEmployeeDetail(searchEmp, false);

                    if (emp != null)
                    {
                        mPartial = new Search_VwPerson();
                        //mPartial.UserName = emp.LanId;
                        //mPartial.Email = emp.EmailAddress;
                        //mPartial.Name = emp.FirstName + " " + emp.LastName;
                        //mPartial.DepartmentName = emp.Dept;
                        //mPartial.DepartmentID = emp.DeptId;
                        //mPartial.Title = emp.JobTitle;
                        //mPartial.Phone = emp.Phone;
                        //mPartial.PhonePreferred = emp.Phone;
                    }

                }
                catch (Exception)
                { }
            }

            return mPartial;
        }

        public IEnumerable<Search_VwTrainingGrantGeneralData> SearchTrainingGrants(Guid pdFacultyId, string sponsorName, string title)
        {
            if (pdFacultyId == null 
                && string.IsNullOrEmpty(sponsorName)
                && string.IsNullOrEmpty(title))
                return Enumerable.Empty<Search_VwTrainingGrantGeneralData>().ToList();

            IQueryable<Search_VwTrainingGrantGeneralData> searchSets = context.Set<Search_VwTrainingGrantGeneralData>();
            if (!pdFacultyId.Equals(null))
            {
                searchSets = searchSets.Where(tg => tg.PdFacultyId == pdFacultyId);
            }

            if (!string.IsNullOrEmpty(sponsorName))
            {
                searchSets = searchSets.Where(o => o.SponsorName.Contains(sponsorName));
            }

            if (!string.IsNullOrEmpty(title))
            {
                searchSets = searchSets.Where(o => o.Title.Contains(title));
            }

            return searchSets.ToList();

        }

        public IEnumerable<Search_VwSupportGeneralData> SearchSupport(string title, int? supportTypeId, string supportNumber, int? supportOrganizationId, 
            DateTime? dateStarted, DateTime? dateEnded)
        {
            // If all search fields are empty, return empty string
            if (string.IsNullOrEmpty(title)
                && supportTypeId.Equals(null)
                && string.IsNullOrEmpty(supportNumber)
                && supportOrganizationId.Equals(null)
                && dateStarted.Equals(null)
                && dateEnded.Equals(null))                   
                return Enumerable.Empty<Search_VwSupportGeneralData>().ToList();

            IQueryable<Search_VwSupportGeneralData> searchSets = context.Set<Search_VwSupportGeneralData>();

            if (!string.IsNullOrEmpty(title))
            {
                searchSets = searchSets.Where(spt => spt.SupportTitle.Contains(title));
            }

            if (!supportTypeId.Equals(null) && supportTypeId != 0)
            {
                searchSets = searchSets.Where(spt => spt.SupportTypeId == supportTypeId);
            }

            if (!string.IsNullOrEmpty(supportNumber))
            {
                searchSets = searchSets.Where(spt => spt.SupportNumber.Contains(supportNumber));
            }

            if (!supportOrganizationId.Equals(null) && supportOrganizationId != 0)
            {
                searchSets = searchSets.Where(spt => spt.SupportOrganizationId == supportOrganizationId);
            }

            if (!dateStarted.Equals(null))
            {
                searchSets = searchSets.Where(spt => spt.DateStarted == dateStarted);
            }

            if (!dateEnded.Equals(null))
            {
                searchSets = searchSets.Where(spt => spt.DateEnded == dateEnded);
            }

            return searchSets.ToList();

        }

        public IEnumerable<Search_VwMenteeDemographics> SearchMentee(string firstName, string lastName, string middleName, string studentId)
        {
            if (string.IsNullOrEmpty(firstName)
                && string.IsNullOrEmpty(lastName)
                && string.IsNullOrEmpty(middleName)
                && string.IsNullOrEmpty(studentId))
                return Enumerable.Empty<Search_VwMenteeDemographics>().ToList();

            IQueryable<Search_VwMenteeDemographics> searchSets = context.Set<Search_VwMenteeDemographics>();
            if (!string.IsNullOrEmpty(firstName))
            {
                searchSets = searchSets.Where(o => o.FirstName.Contains(firstName));
            }

            if (!string.IsNullOrEmpty(lastName))
            {
                searchSets = searchSets.Where(o => o.LastName.Contains(lastName));
            }

            if (!string.IsNullOrEmpty(middleName))
            {
                searchSets = searchSets.Where(o => o.MiddleName.Contains(middleName));
            }

            if (!string.IsNullOrEmpty(studentId))
            {
                searchSets = searchSets.Where(o => o.StudentId.Contains(studentId));
            }
            return searchSets.ToList();

        }

        // PURPOSE: Create a new or Update an existing GradeRecord record
        public GradeRecord AddUpdateGradeRecord(GradeRecord updateRecord)
        {
            bool isNew = context.GradeRecords.Count(e => e.PersonId == updateRecord.PersonId) > 0 ? false : true;
            var gradeRecord = isNew
                ? new GradeRecord()
                : context.Set<GradeRecord>()
                    .FirstOrDefault(e => e.PersonId == updateRecord.PersonId);

            gradeRecord.PersonId = updateRecord.PersonId;

            if (updateRecord.Gpa != null) { gradeRecord.Gpa = updateRecord.Gpa; }
            if (updateRecord.GpaScale != null) { gradeRecord.GpaScale = updateRecord.GpaScale; }

            if (updateRecord.GreScoreVerbal != null) { gradeRecord.GreScoreVerbal = updateRecord.GreScoreVerbal; }
            if (updateRecord.GrePercentileVerbal != null) { gradeRecord.GrePercentileVerbal = updateRecord.GrePercentileVerbal; }
            if (updateRecord.GreScoreQuantitative != null) { gradeRecord.GreScoreQuantitative = updateRecord.GreScoreQuantitative; }
            if (updateRecord.GrePercentileQuantitative != null) { gradeRecord.GrePercentileQuantitative = updateRecord.GrePercentileQuantitative; }
            if (updateRecord.GreScoreAnalytical != null) { gradeRecord.GreScoreAnalytical = updateRecord.GreScoreAnalytical; }
            if (updateRecord.GrePercentileAnalytical != null) { gradeRecord.GrePercentileAnalytical = updateRecord.GrePercentileAnalytical; }
            if (updateRecord.GreScoreSubject != null) { gradeRecord.GreScoreSubject = updateRecord.GreScoreSubject; }
            if (updateRecord.GrePercentileSubject != null) { gradeRecord.GrePercentileSubject = updateRecord.GrePercentileSubject; }

            if (updateRecord.McatScoreVerbalReasoning != null) { gradeRecord.McatScoreVerbalReasoning = updateRecord.McatScoreVerbalReasoning; }
            if (updateRecord.McatScorePhysicalSciences != null) { gradeRecord.McatScorePhysicalSciences = updateRecord.McatScorePhysicalSciences; }
            if (updateRecord.McatScoreBiologicalSciences != null) { gradeRecord.McatScoreBiologicalSciences = updateRecord.McatScoreBiologicalSciences; }
            if (updateRecord.McatScoreWriting != null) { gradeRecord.McatScoreWriting = updateRecord.McatScoreWriting; }
            if (updateRecord.McatPercentile != null) { gradeRecord.McatPercentile = updateRecord.McatPercentile; }

            if (isNew)
            {
                context.Set<GradeRecord>().Add(gradeRecord);
            }

            context.SaveChanges();
            return gradeRecord;

        }


        // PURPOSE: Create a new or Update an existing ContactEmail record
        public ContactEmail AddUpdateEmailAddress(Guid personId, string email, int emailType, bool isPrimary)
        {
            bool isNewEmail = context.ContactEmails.Count(e => e.PersonId == personId && e.IsPrimary == isPrimary) > 0 ? false : true;
            var contactEmail = isNewEmail
                ? new ContactEmail()
                : context.Set<ContactEmail>()
                    .FirstOrDefault(e => e.PersonId == personId && e.IsPrimary == isPrimary);

            contactEmail.PersonId = personId;
            contactEmail.EmailAddress = email;
            contactEmail.ContactEntityTypeId = emailType;
            contactEmail.IsPrimary = isPrimary;

            if (isNewEmail)
            {
                context.Set<ContactEmail>().Add(contactEmail);
            }

            context.SaveChanges();
            return contactEmail;
        }

        // PURPOSE: Create a new or Update an existing ContactPhone record
        public ContactPhone AddUpdatePhoneNumber(Guid personId, string phone, int phoneType, bool isPrimary)
        {
            bool isNewPhone = context.ContactPhones.Count(e => e.PersonId == personId && e.IsPrimary == true) > 0 ? false : true;
            var contactPhone = isNewPhone
                ? new ContactPhone()
                : context.Set<ContactPhone>()
                    .FirstOrDefault(e => e.PersonId == personId && e.IsPrimary == true);

            contactPhone.PersonId = personId;
            contactPhone.PhoneNumber = phone;
            contactPhone.ContactEntityTypeId = phoneType;
            contactPhone.IsPrimary = isPrimary;

            if (isNewPhone)
            {
                context.Set<ContactPhone>().Add(contactPhone);
            }

            context.SaveChanges();
            return contactPhone;

        }

        // PURPOSE: Create a new or Update an existing ContactAddress record
        public ContactAddress AddUpdateAddress(Guid personId, string addressLine1, string addressLine2,
            string addressLine3, string city, int? stateId, string postalCode, int? countryId, int addressType, bool isPrimary)
        {

            bool isNewAddress = context.ContactAddresses.Count(e => e.PersonId == personId && e.IsPrimary == true) > 0 ? false : true;
            var contactAddress = isNewAddress
                ? new ContactAddress()
                : context.Set<ContactAddress>()
                        .FirstOrDefault(e => e.PersonId == personId && e.IsPrimary == true);

            contactAddress.PersonId = personId;
            contactAddress.AddressLine1 = addressLine1;
            contactAddress.AddressLine2 = addressLine2;
            contactAddress.AddressLine3 = addressLine3;
            contactAddress.City = city;

            if (stateId > 0)
            {
                contactAddress.StateId = stateId;
            }
            contactAddress.PostalCode = postalCode;

            if (countryId > 0)
            {
                contactAddress.CountryId = countryId;
            }
            contactAddress.ContactEntityTypeId = addressType;
            contactAddress.IsPrimary = isPrimary;

            if (isNewAddress)
            {
                context.Set<ContactAddress>().Add(contactAddress);
            }

            context.SaveChanges();
            return contactAddress;
        
        }


        public FundingFaculty AddUpdateFundingFaculty(Guid facultyId, Guid fundingId, int primaryRole, int secondaryRole)
        {
            bool isNew = context.FundingFaculties.Count(f => f.FacultyId == facultyId 
                && f.PrimaryRoleId == primaryRole 
                && f.FundingId == fundingId) > 0 ? false : true;

            var fundingFaculty = isNew
                ? new FundingFaculty()
                : context.Set<FundingFaculty>()
                    .FirstOrDefault(f => f.FacultyId == facultyId 
                && f.PrimaryRoleId == primaryRole 
                && f.FundingId == fundingId);

            fundingFaculty.FacultyId = facultyId;
            fundingFaculty.FundingId = fundingId;
            if (primaryRole != 0)
            {
                fundingFaculty.PrimaryRoleId = primaryRole;
            }          
            if (secondaryRole != 0)
            {
                fundingFaculty.SecondaryRoleId = secondaryRole;
            }           
            if (isNew)
            {
                context.Set<FundingFaculty>().Add(fundingFaculty);
            }
            context.SaveChanges();
            return fundingFaculty;

        }
        // MVN - I am still working on this method
        /// <summary>
        ///  Ideally - I like to check FundingFaculty List and make sure that at lease 1 PI =>Role Id 1 
        /// </summary>
        /// <param name="facultyId"></param>
        /// <param name="fundingId"></param>
        /// <param name="primaryRole"></param>
        /// <returns></returns>
        public FundingFaculty RemoveFtySupport(Guid fundingFacultyId, int primaryRole)
        {
            bool isRemove = context.FundingFaculties.Count(f => f.Id == fundingFacultyId 
                && f.PrimaryRoleId == primaryRole) > 0 ? false : true;

            var fundingFaculty = isRemove
               ? new FundingFaculty()
                : context.Set<FundingFaculty>()
                    .FirstOrDefault(f => f.Id == fundingFacultyId
                && f.PrimaryRoleId == primaryRole);
            
            if (primaryRole > 1)
            {
                fundingFaculty.IsDeleted = true;
            }
            if (isRemove)
            {
                //context.Set<FundingFaculty>().Add(fundingFaculty);
                //fundingFaculty.IsDeleted = true;
                context.Set<FundingFaculty>().Add(fundingFaculty);
            }
            //context.SaveChanges();
            return fundingFaculty;

        }

        // TODO -- NOT WORKING YET -- trying to get last generated id used so we can add 1 to create the next generated id
        public IEnumerable<Search_VwGeneratedIDs> GetLastGeneratedID(string idType)
        {
            //return context.Search_VwGeneratedIDs.Where(i => i.IdType.Contains(idType.ToLower())).Take(1);
            var result = context.Search_VwGeneratedIDs.Where(i => i.IdType.Contains(idType.ToLower())).Take(1);
            return result;
        }

        public IEnumerable<Search_VwReports> GetSearch_VwReports()
        {
            return context.Search_VwReports;
        }

        public IEnumerable<Search_VwReports> GetSearch_VwReports(string searchTerm)
        {
            return context.Search_VwReports.Where(r => r.Title.Contains(searchTerm));
        }

        public IEnumerable<Report> GetReportList()
        {
            return context.Reports.Where(rpt => rpt.IsDeleted == false).OrderBy(rpt => rpt.Title);
        }
              

        public Report GetReportById(int Id)
        {
            Report rpt = context.Reports.Find(Id);
            return rpt;
        }

        public IEnumerable<Search_VwTrainingGrantGeneralData> GetSearch_VwTrainingGrantGeneralData()
        {
            return context.Search_VwTrainingGrantGeneralData.OrderBy(tg => tg.Title);
        }

        public IEnumerable<Search_VwFundingDemographics> GetSearch_VwFundingDemographics()
        {
            return context.Search_VwFundingDemographics.OrderBy(f => f.Title);
        }

        public FundingFaculty UpdateFFty(FundingFaculty updateffty)
        {
            bool isUpdate = context.FundingFaculties.Count(f => f.FacultyId == updateffty.FacultyId) > 0 ? false : true;
            var ffty = isUpdate
                ? new FundingFaculty()
                : context.Set<FundingFaculty>()
                .FirstOrDefault(f => f.FacultyId == updateffty.FacultyId);
            updateffty.FacultyId = updateffty.FacultyId;
            if (updateffty.FacultyId != null)
            {
                ffty.FacultyId = updateffty.FacultyId;
            }
            context.SaveChanges();
            return ffty;
        }
       

        public Funding AddOrUpdateFundingRd(Funding updateFunding, ClaimsPrincipal User)
        {
            bool isNew = context.Fundings.Count(f => f.Id == updateFunding.Id) > 0 ? false : true;
            var funding = isNew
                ? new Funding()
                : context.Set<Funding>()
                .FirstOrDefault(f => f.Id == updateFunding.Id);
            
            updateFunding.Id = updateFunding.Id;
            if (updateFunding.Title != null) { 
                funding.Title = updateFunding.Title; 
            }
            if (updateFunding.SponsorId != null)
            {
                funding.SponsorId = updateFunding.SponsorId;
            }
            if (updateFunding.SponsorReferenceNumber != null)
            {
                funding.SponsorReferenceNumber = updateFunding.SponsorReferenceNumber;
            }
            if (updateFunding.FundingStatusId != null)
            {
                funding.FundingStatusId = updateFunding.FundingStatusId;
            }
            if (updateFunding.GrtNumber != null)
            {
                funding.GrtNumber = updateFunding.GrtNumber;
            }
            if (updateFunding.FundingTypeId != null)
            {
                funding.FundingTypeId = updateFunding.FundingTypeId;
            }
            //if (updateFunding.IsRenewal != null)
            //{
                funding.IsRenewal = updateFunding.IsRenewal;
            //}
            if (updateFunding.PrimeAward != null)
            {
                funding.PrimeAward = updateFunding.PrimeAward;
            }
            if (updateFunding.DateProjectedStart != null)
            {
                funding.DateProjectedStart = updateFunding.DateProjectedStart;
            }
            if (updateFunding.DateProjectedEnd != null)
            {
                funding.DateProjectedEnd = updateFunding.DateProjectedEnd;
            }
            if (isNew)
            {
                context.Set<Funding>().Add(funding);
            }
            funding.DateLastUpdated = DateTime.Now;
            funding.LastUpdatedBy = (((ClaimsPrincipal)User).Claims.Where(p => p.Type == ClaimTypes.Name).FirstOrDefault().Value);
            context.SaveChanges();
            return funding;
        }

        
    }
}
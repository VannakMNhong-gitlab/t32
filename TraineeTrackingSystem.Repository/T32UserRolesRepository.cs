﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

using TraineeTrackingSystem.Data;
using TraineeTrackingSystem.Common;
using OSU.Medical.Entity;

namespace TraineeTrackingSystem.Repository
{
    public class T32UserRolesRepository : UserRolesRepository
    {
        private readonly TTSEntities dbContext;
        
        #region Constructors
        public T32UserRolesRepository()
        {
            this.dbContext = new TTSEntities();
        }

        //public T32UserRolesRepository(TTSEntities dbContext)
        //{
        //    this.dbContext = dbContext;
        //}
        #endregion

        #region Dispose Context
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    // dbContext.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        public Login GetUserByMedCenterId(string medCenterId)
        {
            var data = dbContext.Logins
               .Include(p => p.PersonRoles)
               .Where(p => p.Username.ToLower().Trim() == medCenterId.ToLower().Trim())
               .FirstOrDefault();

            return data;
        }
        //public List<Employee> GetUsersListByPartialNameFromASOUser(string partialName, List<string> roles)
        //{
        //    var userList = dbContext
        //}
        public List<Login> GetUserList()
        {
            var userList = dbContext.Logins;
            return userList.ToList();
        }
    }
}
﻿using System;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Repository
{
    public abstract class BaseRepository : IDisposable
    {
        protected readonly TTSEntities context;


        protected BaseRepository(TTSEntities context)            
        {
            this.context = context;
            this.context.Configuration.ValidateOnSaveEnabled = false;

        }

        #region Dispose Context
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        #endregion

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using TraineeTrackingSystem.Data;
using TraineeTrackingSystem.Common;

namespace TraineeTrackingSystem.Repository
{
    public class TrainingGrantRepository : BaseRepository
    {
        public TrainingGrantRepository(TTSEntities context)
            :base(context)
        {
           
        }


        public IQueryable<TrainingGrant> GetTrainingGrant()
        {
            return context.TrainingGrants;
        }

        public TrainingGrant getTrainingGrantById(Guid id)
        {
            //throw new NotImplementedException();
            return context.TrainingGrants.Find(id);
        }
        // MVN - Add THIS => to debugging issue
        public Funding GetFundingbyId(Guid Id)
        {
            Funding funding = context.Fundings.Find(Id);
            return funding;
        }
        public IEnumerable<TrainingGrant> GetTrainingGrants()
        {
            //return context.Mentees.Where(x => x.CustomerId == customerId).ToList();
            return context.TrainingGrants;
        }
        public IQueryable<BudgetPeriodStatu> GetBudgetPeriodStatus()
        {
            return context.BudgetPeriodStatus;
        }

        public TrainingGrant AddTrainingGrant(Funding funding)
        {

            var newFunding = context.Set<Funding>().Add(funding);
            context.SaveChanges();
            var newTrainingGrant = new TrainingGrant
            {
                Id = Guid.NewGuid(),
                FundingId = newFunding.Id
            };
            context.Set<TrainingGrant>().Add(newTrainingGrant);
            context.SaveChanges();
            return newTrainingGrant;
        }

        ///////// <summary>
        ///////// MVN - THIS => Get all TrainingPeriod that are associated with the passed-in TrainingGrantTrainee
        ///////// </summary>
        ///////// <param name="funding"></param>
        ///////// <returns></returns>
        ////////public IEnumerable<TrainingPeriod> GetMenteeList(TrainingGrantTrainee trainee)
        ////////{
        ////////    var newMentee = context.TrainingPeriods.Where(m => m.Id == trainee.MenteeId);
        ////////    return newMentee;
        ////////}
        ////////public TrainingPeriod GetMenteeList(TrainingGrantTrainee trainee)
        //////public IEnumerable<Mentee> GetMenteeList(TrainingGrantTrainee trainee)
        //////{
        //////    var newMentee = context.Mentees.Where(m => m.Id == trainee.MenteeId);
        //////    return newMentee;
        //////    //return context.TrainingPeriods.FirstOrDefault(m => m.Id == trainee.MenteeId);
        //////}
        public ErrorCode AddUpdateMentee(Mentee newMentee)
        {
            ErrorCode result = ErrorCode.NotFound;
            bool recordFound = false;
            if (newMentee.StudentId  == null)
            {
                context.Mentees.Add(newMentee);
            }

            else
            {
                // RECORD FOUND        
                Mentee oldMentee = context.Mentees.Where(m => m.StudentId == newMentee.StudentId).FirstOrDefault();
                if (oldMentee != null)
                {
                    // RECORD FOUND
                    newMentee.StudentId = oldMentee.StudentId;
                    newMentee.Person.FirstName = oldMentee.Person.FirstName;
                    newMentee.Person.LastName = oldMentee.Person.LastName;
                    newMentee.Person.MiddleName = oldMentee.Person.MiddleName;
                 
                    recordFound = true;
                }
            }


            //NO RECORD FOUND
            if (!recordFound)
            {
                //NO RECORD FOUND
                context.Mentees.Add(newMentee);
            }

            context.SaveChanges();
            result = ErrorCode.Success;
            return result;
        }


        public IEnumerable<Search_VwTrainingGrantGeneralData> GetSearch_VwTrainingGrantGeneralData()
        {
            return context.Search_VwTrainingGrantGeneralData;
        }

        ///// <summary>
        ///// MVN - to be used for the TG Trainee Detail Information Grid section 
        ///// </summary>
        ///// <param name="menteePersonId"></param>
        ///// <returns></returns>
        //public IEnumerable<Search_VwTrainingGrantMentees> GetSearch_VwTrainingGrantMentees(Guid menteePersonId)
        //{
        //    return context.Search_VwTrainingGrantMentees.Where(p => p.MenteePersonId == menteePersonId);
        //}


        ///// <summary>
        ///// MVN - to be used for the TG trainee detail grid information section
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        ///// 
        //public IEnumerable<Search_VwTrainingGrantTrainees> GetSearch_VwTrainingGrantTrainees(Guid id)
        //{
        //    return context.Search_VwTrainingGrantTrainees.Where(p => p.TrainingGrantId == id);
        //}
        /// <summary>
        /// MVN - Leaving these code for future ref joining the two tables - 
        /// I have not test the codes yet.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<Search_VwTrainingGrantTrainees> getJoinTGTraineeInfo(Guid id)
        {
            //return context.TrainingGrantTrainees.Where(t => t.Id == id);
            //IEnumerable<TraineeDetailedInformationViewModel> traineeInfo = from TrainingGrantTrainee in t join Search_VwTrainingGrantTrainees v where t.TrainingGrantId == v.TrainingGrantId
            var traineeInfoQuery = from t in context.TrainingGrantTrainees
                                   join v in context.Search_VwTrainingGrantTrainees on t.TrainingGrantId equals v.TrainingGrantId
                                   select new
                                   {
                                       Id = t.Id,
                                       TrainingGrantId = t.TrainingGrantId,
                                       MenteeId = t.MenteeId,
                                       DateStarted = t.DateStarted,
                                       DateEnded = t.DateEnded,
                                       DoctoralLevelId = t.DoctoralLevelId,
                                       PredocTraineeStatusId = t.PredocTraineeStatusId,
                                       PredocReasonForLeaving = t.PredocReasonForLeaving,
                                       PostdocTraineeStatusId = t.PostdocTraineeStatusId,
                                       PostdocReasonForLeaving = t.PostdocReasonForLeaving,
                                       DateLastUpdated = t.DateLastUpdated,
                                       LastUpdatedBy = t.LastUpdatedBy,
                                       IsDeleted = t.IsDeleted,
                                       TrainingPeriodId = t.TrainingPeriodId,
                                       Column1 = v.TrainingGrantId,
                                       FundingId = v.FundingId,
                                       TrainingGrantTraineeId = v.TrainingGrantTraineeId,
                                       AppointmentStartDate = v.AppointmentStartDate,
                                       AppointmentEndDate = v.AppointmentEndDate,
                                       Title = v.Title,
                                       TraineeId = v.TraineeId,
                                       Column2 = v.MenteeId,
                                       MenteePersonId = v.MenteePersonId,
                                       MenteeLastName = v.MenteeLastName,
                                       MenteeFirstName = v.MenteeFirstName,
                                       MenteeFullName = v.MenteeFullName,
                                       MenteeTypeId = v.MenteeTypeId,
                                       MenteeType = v.MenteeType,
                                       YearStarted = v.YearStarted,
                                       YearEnded = v.YearEnded,
                                       DepartmentId = v.DepartmentId,
                                       DepartmentName = v.DepartmentName,
                                       InstitutionAssociation = v.InstitutionAssociation,
                                       ResearchExperience = v.ResearchExperience,
                                       ProgramStatus = v.ProgramStatus
                                   };
            return context.Search_VwTrainingGrantTrainees.Where(p => p.TrainingGrantId == id); 
        }

        ///// <summary>
        ///// MVN - To be used for Add Trainee Expand 
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //public TrainingGrantTrainee GetTraineeInfo(Guid id)
        //{
        //    return context.TrainingGrantTrainees.Find(id);
        //}

        /*
         public IEnumerable<Search_VwTrainingGrantFacultyWithResearchInterests> GetTrainingGrantResearchInterestList(Guid GrantId, Guid FacultyId)
        {
            if ((GrantId == null) || (FacultyId == null))
            {
                return Enumerable.Empty<Search_VwTrainingGrantFacultyWithResearchInterests>().ToList();
            }

            IQueryable<Search_VwTrainingGrantFacultyWithResearchInterests> searchSets = context.Set<Search_VwTrainingGrantFacultyWithResearchInterests>();

            searchSets = searchSets.Where( tgri => ((tgri.TrainingGrantId == GrantId) && (tgri.FacultyId == FacultyId)) );

            return searchSets.ToList();

         }
         */


    }
}
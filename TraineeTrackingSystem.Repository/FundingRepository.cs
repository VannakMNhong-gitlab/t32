﻿using System;
using System.Collections.Generic;
using System.Linq;
using TraineeTrackingSystem.Common;
using TraineeTrackingSystem.Data;

namespace TraineeTrackingSystem.Repository
{
    public class FundingRepository : BaseRepository
    {
        public FundingRepository(TTSEntities context)
            : base(context) { }

        public IEnumerable<Search_VwFundingDemographics> GET_Search_VwFundingDemographics()
        {
            return context.Search_VwFundingDemographics;
        }
        public IEnumerable<FundingFaculty> GetFundingFacultyList()
        {
            return context.FundingFaculties;
        }
        // use this for sorting last names
        public IEnumerable<Faculty> GetPersonFullName(string name)
        {
            return context.Faculties.Where(i => i.Person.LastName.Contains(name)).OrderBy(i => i.Person.LastName);
        }
        // Replace this to bellow method
        //public IEnumerable<Search_VwFacultyDemographics> GetSearch_VwFacultyDemographics(Guid Id)
        //{
        //    // funding Id
        //    return context.Search_VwFacultyDemographics.Where(p => p.Id == Id);
        //}
        public IEnumerable<Search_VwFundingDemographics> GetSearch_VwFundingDemographics(Guid personId)
        {
            return context.Search_VwFundingDemographics.Where(p => p.PiFacultyId == personId);
        }
        /// <summary>
        /// 1/27/2015
        /// This will be used for faculty support grid section in Funding 
        /// </summary>
        /// <param name="fundingId"></param>
        /// <returns></returns>
        public IEnumerable<Search_VwFundingFaculty> GetSearch_VwFundingFaculty(Guid fundingId)
        {
            return context.Search_VwFundingFaculty.Where(fd => fd.Id == fundingId);
        }


        public IEnumerable<Faculty> GetName()
        {
            return context.Faculties;
        }
        
        public Funding AddFunding(Funding newfunding)
        {
            var funding = context.Set<Funding>().Add(newfunding);
            context.SaveChanges();
            return funding;
        }
        public Funding GetFundingbyId(Guid Id)
        {
            Funding funding = context.Fundings.Find(Id);
            return funding;
        }
        //public IQueryable<Search_VwFacultyDemographics> getFundingFacultyList(Funding funding)
        //{
        //    return context.Search_VwFacultyDemographics
        //        .Where(f => f.Id == funding.Id)
        //        .OrderBy(f => f.FullName);
        //    //return context.FundingFaculties.First(f => f.FundingId == Id && f.PrimaryRoleId == 1);
            
        //    //return context.FundingFaculties.First(f => f.FundingId == Id && f.FundingRole_PrimaryRoleId.Id == roleId);
        //    /****************************************************************/
        //}
        //public IQueryable<FundingFaculty> getFundingFacultyIdBased(Funding fund)
        //{
        //    return context.FundingFaculties.Where(f => f.FundingId == fund.Id);
        //}
        public IQueryable<FundingStatu> GetFundingStatus()
        {
            return context.FundingStatus;
        }
        public IQueryable<BudgetPeriodStatu> GetBudgetPeriodStatus()
        {
            return context.BudgetPeriodStatus;
        }
        //////public int getFdList(int fdCost, Guid fdId, int bdStatusId)
        //////{
        //////    // looking for certain record....
        //////    var newFdCstInfo = context.Set<FundingDirectCost>()
        //////                              .Find(fdCost);
                                     
        //////    int bPsTobeResturned = 0;


        //////    if (bdStatusId == 0 && fdId != null)
        //////    {
        //////        IEnumerable<FundingDirectCost> newFDDCost = context.Set<FundingDirectCost>();

        //////        //////////// check to see at least 1 "Current" status for a specific grant =>COUNT all 
        //////        var changeStatus = context.FundingDirectCosts.Where(s => bdStatusId == 0).Count() < 0 ? true : false;
        //////        if (changeStatus)
        //////        {
        //////            foreach (var changeCurrentStatus in newFDDCost.Where(f => f.FundingId == fdId))
        //////            {
        //////                if (changeCurrentStatus.FundingId == fdId)
        //////                {
        //////                    newFdCstInfo.BudgetPeriodStatusId = changeCurrentStatus.BudgetPeriodStatusId;
        //////                }
        //////            }

        //////        }
        //////        bPsTobeResturned = newFdCstInfo.BudgetPeriodStatusId;
        //////    }

        //////    return bPsTobeResturned;
        //////}

        public int GetVariousBPStatusId(int budgetPeriodStatusId, int? Id, Guid fundingId)
        {
            var newFdCstInfo = context.Set<FundingDirectCost>().Find(Id);
            int bPsTobeResturned = 0;
 
            
            if (budgetPeriodStatusId == 1){
                IEnumerable<FundingDirectCost> newFDDCost = context.Set<FundingDirectCost>();
   
                //////////// check to see at least 1 "Current" status for a specific grant =>COUNT all 
                var changeStatus = context.FundingDirectCosts.Where(s => s.BudgetPeriodStatusId == 1).Count() > 0? true:false;
                if (changeStatus) {
                    foreach (var changeCurrentStatus in newFDDCost.Where(f => f.FundingId == fundingId)) { 
                        if (changeCurrentStatus.FundingId == fundingId){
                            changeCurrentStatus.BudgetPeriodStatusId = 2;
                        }
                    }

                }
                bPsTobeResturned = budgetPeriodStatusId;
                newFdCstInfo.BudgetPeriodStatusId = bPsTobeResturned;
                context.SaveChanges();
            }

            //if (budgetPeriodStatusId == 0)
            //{
            //    bPsTobeResturned = newFdCstInfo.BudgetPeriodStatusId;
            //    context.SaveChanges();
            //    //bPsTobeResturned = budgetPeriodStatusId;
            //}
            if (budgetPeriodStatusId == 2){
                bPsTobeResturned = budgetPeriodStatusId;
                newFdCstInfo.BudgetPeriodStatusId = bPsTobeResturned;
                context.SaveChanges();
            }
            return bPsTobeResturned;
        }         
    }
}
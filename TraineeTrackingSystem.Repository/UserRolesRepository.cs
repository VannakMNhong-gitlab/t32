﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TraineeTrackingSystem.Data;
using TraineeTrackingSystem.Common;

namespace TraineeTrackingSystem.Repository
{
    public interface UserRolesRepository : IDisposable
    {
        Login GetUserByMedCenterId(string medCenterId);
        //Login GetMedCenterUser(string FName, string LName, string MedCenterId);
        ////List<Login> GetUserByMedCenterId(string medCenterId);
        //List<OSU.Medical.Entity.Employee> GetUsersListByPartialNameFromASOUser(string partialName, List<string> roles);

        //public void Dispose()
        //{
        //    throw new NotImplementedException();
        //}
    }
}
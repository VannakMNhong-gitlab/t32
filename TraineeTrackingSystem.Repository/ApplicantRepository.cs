﻿using System;
using System.Collections.Generic;
using System.Linq;
using TraineeTrackingSystem.Common;
using TraineeTrackingSystem.Data;


namespace TraineeTrackingSystem.Repository
{
    public class ApplicantRepository : BaseRepository
    {
        public ApplicantRepository(TTSEntities context)
            : base(context) { }

        public Applicant GetApplicantById(int Id)
        {
            Applicant applicant = context.Applicants.Find(Id);
            return applicant;
        }
        public Applicant GetApplicantByPersonId(Guid Id)
        {
            Applicant applicant = context.Applicants.Find(Id);
            return applicant;
        }
        public OsuTimeline GetOsuTimeLineById(Guid Id)
        {
            OsuTimeline osu = context.OsuTimelines.Find(Id);
            return osu;
        }
        public IEnumerable<OsuTimeline> GetosuTimeLineList()
        {
            return context.OsuTimelines;
        }
        public IEnumerable<GradeRecord> GetGradeList()
        {
            return context.GradeRecords;
        }


        public Organization GetOrgById(int Id)
        {
            Organization org = context.Organizations.Find(Id);
            return org;
        }

        public Person GetPersonById(Guid Id)
        {
            Person per = context.People.Find(Id);
            return per;
        }
        public IEnumerable<Person> GetPersonList()
        {
            return context.People;
        }
        public Applicant AddNewApplicant(Person person, Applicant app, GradeRecord grade, OsuTimeline osutimeLine)
        {
            var newPerson = context.Set<Person>().Add(person);
            var newApplicant = new Applicant
            {
                Id = Guid.NewGuid(),
                PersonId = newPerson.PersonId,
                ApplicantId = app.ApplicantId,
                DoctoralLevelId = app.DoctoralLevelId,
                DepartmentId = app.DepartmentId,

                
                IsTrainingGrantEligible = app.IsTrainingGrantEligible,
            };          
            var newGrade = new GradeRecord
            {
                PersonId = newPerson.PersonId,
            };

            var newOsu = context.Set<OsuTimeline>().Add(osutimeLine);
            var newOsuTimeLine = new OsuTimeline
            {
                PersonId = newPerson.PersonId,
                YearEntered = newOsu.YearEntered,
            };
            context.Set<Applicant>().Add(newApplicant);
            context.SaveChanges();

            context.Set<GradeRecord>().Add(newGrade);
            context.SaveChanges();

            context.Set<OsuTimeline>().Add(newOsuTimeLine);
            context.SaveChanges();

            return newApplicant;
        }
        public Applicant AddNewPredDocApplicant(Person person, Applicant app, GradeRecord grade, OsuTimeline osutimeLine)
        
        {
            var newApplicant = new Applicant
            {
                PersonId = person.PersonId,
                ApplicantId = app.ApplicantId,
            };
                 
            var newGrade = new GradeRecord
            {
                PersonId = newApplicant.PersonId,
                Gpa = grade.Gpa,
                GpaScale = grade.GpaScale,
                GreScoreVerbal = grade.GreScoreVerbal,
                GrePercentileVerbal = grade.GrePercentileVerbal,
                GreScoreQuantitative = grade.GreScoreQuantitative,
                GrePercentileQuantitative = grade.GrePercentileQuantitative,
                GreScoreAnalytical = grade.GreScoreAnalytical,
                GrePercentileAnalytical = grade.GreScoreAnalytical,
                GreScoreSubject = grade.GreScoreSubject,
                GrePercentileSubject = grade.GrePercentileSubject,
                McatScoreVerbalReasoning = grade.McatScoreVerbalReasoning,
                McatScorePhysicalSciences = grade.McatScorePhysicalSciences,
                McatScoreBiologicalSciences = grade.McatScoreBiologicalSciences,
                McatScoreWriting = grade.McatScoreWriting,
                McatPercentile = grade.McatPercentile
            };
            var newOsuTimeLine = new OsuTimeline
            {
                PersonId = newApplicant.PersonId,
                WasInterviewed = osutimeLine.WasInterviewed,
                //AcceptedOffer = osutimeLine.AcceptedOffer,
                WasAccepted = osutimeLine.WasAccepted,
                WasEnrolled = osutimeLine.WasEnrolled
            };
            context.Set<Applicant>().Add(newApplicant);
            context.SaveChanges();

            context.Set<GradeRecord>().Add(newGrade);
            context.SaveChanges();

            context.Set<OsuTimeline>().Add(newOsuTimeLine);
            context.SaveChanges();

            return newApplicant;
        }

        public IEnumerable<Search_VwApplicantDemographics> Get_Search_VwApplicantDemographics()
        {
            return context.Search_VwApplicantDemographics;
        }
        public IQueryable<Applicant> GetApplicant()
        {
            return context.Applicants;
        }
        
        public IEnumerable<DoctoralLevel> GetDoctoralLevelList()
        {
            return context.DoctoralLevels;
        }

        public IEnumerable<TestScoreType> GetTestScoreTypeList()
        {
            return context.TestScoreTypes;
        }
        public IEnumerable<Program> GetProgramList()
        {
            return context.Programs;
        }        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using TraineeTrackingSystem.Common;
using TraineeTrackingSystem.Data;

using System.Security.Claims;
namespace TraineeTrackingSystem.Repository
{
    public class SupportRepository: BaseRepository
    {
        public SupportRepository(TTSEntities context)
            :base(context)
        {
           
        }



        public IQueryable<Support> GetSupport()
        {
            return context.Supports;
        }
        //public IQueryable<FacultyRank> GetFacultyRank()
        //{
        //    return context.FacultyRanks;
        //}
        
        //public IQueryable<FacultyResearchInterest> getFacultyResearchInterest()
        //{
        //    return context.FacultyResearchInterests;
        //}

        //public IEnumerable<Organization> GetOrganization()
        //{
        //    return context.Organizations;
        //}
        
        //public IEnumerable<Program> GetProgram()
        //{
        //    return context.Programs;
        //}
       
        //public Program GetProgById(Guid Id)
        //{
        //    Program prog = context.Programs.Find(Id);
        //    return prog;
        //}
        //public IEnumerable<DoctoralLevel> GetDoctoralLevelList()
        //{
        //    return context.DoctoralLevels;
        //}
        //public IEnumerable<Institution> GetInstitution()
        //{
        //    return context.Institutions;
        //}

        //public IQueryable<Institution> GetIQueryInstitution()
        //{
        //    return context.Institutions;
        //}
        public Support GetSupportById(Guid Id)
        {
            Support spt = context.Supports.Find(Id);
            return spt;
        }

        public Support AddSupport(string title, int? supportTypeId, string supportNumber, int? supportOrganizationId,
            DateTime? dateStarted, DateTime? dateEnded, string createdBy)
        {

            var newSupport = new Support
            {
                Id = Guid.NewGuid(),
                Title = title,
                SupportTypeId = (supportTypeId == 0) ? null : supportTypeId,
                SupportNumber = supportNumber,
                SupportOrganizationId = (supportOrganizationId == 0) ? null : supportOrganizationId,
                DateStarted = dateStarted,
                DateEnded = dateEnded,
                CreatedBy = createdBy,
                DateLastUpdated = DateTime.Now,
                LastUpdatedBy = createdBy
            };
            context.Set<Support>().Add(newSupport);
            context.SaveChanges();
            return newSupport;
        }

        //public ErrorCode AddUpdateFaculty(Faculty newFaculty)
        //{
        //    bool recordFound = false;
        //    if (newFaculty.EmployeeId  == null)
        //    {
        //        context.Faculties.Add(newFaculty);
        //    }

        //    else
        //    {
        //        // RECORD FOUND        
        //        Faculty oldFaculty = context.Faculties.Where(f => f.EmployeeId == newFaculty.EmployeeId).FirstOrDefault();
        //        if (oldFaculty != null)
        //        {
        //            // RECORD FOUND
        //            newFaculty.EmployeeId = oldFaculty.EmployeeId;
        //            newFaculty.Person.FirstName = oldFaculty.Person.FirstName;
        //            newFaculty.Person.LastName = oldFaculty.Person.LastName;
        //            newFaculty.Person.MiddleName = oldFaculty.Person.MiddleName;
                 
        //            recordFound = true;
        //        }
        //    }


        //    //NO RECORD FOUND
        //    if (!recordFound)
        //    {
        //        //NO RECORD FOUND
        //        context.Faculties.Add(newFaculty);
        //    }

        //    context.SaveChanges();
        //    ErrorCode result = ErrorCode.Success;
        //    return result;
        //}


        //public ErrorCode AddUpdateDegree(FacultyAcademicData newDegree)
        //{
        //    ErrorCode result = ErrorCode.NotFound;
        //    bool recordFound = false;
        //    if (newDegree.Id == null)
        //    {
        //        context.FacultyAcademicDatas.Add(newDegree);
        //    }

        //    else
        //    {
        //        // RECORD FOUND        
        //        FacultyAcademicData oldDegree = context.FacultyAcademicDatas.Where(f => f.Id == newDegree.Id).FirstOrDefault();
        //        if (oldDegree != null)
        //        {
        //            // RECORD FOUND
        //            oldDegree.Id = oldDegree.Id;
        //           // oldDegree.DegreeTitle = oldDegree.DegreeTitle;
        //            oldDegree.DegreeYear = oldDegree.DegreeYear;
                    

        //            recordFound = true;
        //        }
        //    }


        //    //NO RECORD FOUND
        //    if (!recordFound)
        //    {
        //        //NO RECORD FOUND
        //        context.FacultyAcademicDatas.Add(newDegree);
        //    }

        //    context.SaveChanges();
        //    result = ErrorCode.Success;
        //    return result;
        //}

        public IEnumerable<Search_VwSupportGeneralData> GetSearch_VwSupportGeneralData()
        {
            return context.Search_VwSupportGeneralData;
        }

        //public IEnumerable<Search_VwSupportGeneralData> GetSearch_VwSupportGeneralDataById(Guid Id)
        //{
        //    return context.Search_VwSupportGeneralData.Find(Id);
        //}

       

        ////Used for Sorting
        //public IEnumerable<Search_VwMenteeMentors> GetSearch_VwMenteeMentorsSort()
        //{
        //    return context.Search_VwMenteeMentors;
        //}
        //public IEnumerable<Search_VwFacultyAcademicData> GetSearch_VwFacultyAcademicData()
        //{
        //    return context.Search_VwFacultyAcademicData;
        //}
        
        //public IEnumerable<Search_VwFacultyResearchInterests> GetSearch_VwFacultyResearchInterest()
        //{
        //    return context.Search_VwFacultyResearchInterests;
        //}

    }
}
﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using TraineeTrackingSystem.Common;
//using TraineeTrackingSystem.Data;

//namespace TraineeTrackingSystem.Repository
//{
//    public class ApplicantRespository : BaseRepository
//    {
//        public ApplicantRespository(TTSEntities context)
//            : base(context) { }

//        public Applicant GetApplicantById(int Id)
//        {
//            Applicant applicant = context.Applicants.Find(Id);
//            return applicant;
//        }
//        public Applicant GetApplicantByPersonId(Guid Id)
//        {
//            Applicant applicant = context.Applicants.Find(Id);
//            return applicant;
//        }
//        public OsuTimeline GetOsuTimeLineById(Guid Id)
//        {
//            OsuTimeline osu = context.OsuTimelines.Find(Id);
//            return osu;
//        }
       

//        public Organization GetOrgById(int Id)
//        {
//            Organization org = context.Organizations.Find(Id);
//            return org;
//        }

//        public Person GetPersonById(Guid Id)
//        {
//            Person per = context.People.Find(Id);
//            return per;
//        }

//        public Applicant AddNewApplicant(Person person, Applicant app, OsuTimeline osutimeLine)
//        {
//            var newPerson = context.Set<Person>().Add(person);
//            var newApplicant = new Applicant
//            {
//                Id = Guid.NewGuid(),
//                PersonId = newPerson.PersonId,
//                ApplicantId = app.ApplicantId,
//                DoctoralLevelId = app.DoctoralLevelId,
//                DepartmentId = app.DepartmentId,
//                IsTrainingGrantEligible = app.IsTrainingGrantEligible,
//            };
//            var newOsu = context.Set<OsuTimeline>().Add(osutimeLine);
//            var newOsuTimeLine = new OsuTimeline
//            {
//                PersonId = newPerson.PersonId,            
//                YearEntered = newOsu.YearEntered
//            };
//                context.Set<Applicant>().Add(newApplicant);
//                context.SaveChanges();

//                context.Set<OsuTimeline>().Add(newOsuTimeLine);
//                context.SaveChanges();

//                return newApplicant;
//        }
//        public Applicant AddNewPredDocApplicant(Person Person, GradeRecord grade, OsuTimeline osutimeLine, Guid PersonId, int applicantId)
//        {
//            var getPerson = GetPersonById(PersonId);
//            var getApplicant = GetApplicantById(applicantId);
//            var newPerson = new Person
//            {
//                PersonId = getPerson.PersonId,

//            };
//            var newApplicant = new Applicant
//            {
//                PersonId = newPerson.PersonId,
//                ApplicantId = getApplicant.ApplicantId,
//            };
//            var newGrade = new GradeRecord
//            {
//                PersonId = newPerson.PersonId,
//                Gpa = grade.Gpa,
//                GpaScale = grade.GpaScale
//            };
//            var newOsuTimeLine = new OsuTimeline
//            {
//                PersonId = newPerson.PersonId,
//                WasInterviewed = osutimeLine.WasInterviewed,
//                AcceptedOffer = osutimeLine.AcceptedOffer,
//                WasEnrolled = osutimeLine.WasEnrolled
//            };
//            context.Set<Applicant>().Add(newApplicant);
//            context.SaveChanges();

//            context.Set<GradeRecord>().Add(newGrade);
//            context.SaveChanges();

//            context.Set<OsuTimeline>().Add(newOsuTimeLine);
//            context.SaveChanges();

//            return newApplicant;
//        }
       
       
//        public IEnumerable<Search_VwApplicantDemographics> Get_Search_VwApplicantDemographics()
//        {
//            return context.Search_VwApplicantDemographics;
//            //return context.Search_VwApplicantDemographics.OrderByDescending(x => x.ApplicantId);
//        }
//        public IQueryable<Applicant> GetApplicant()
//        {
//            return context.Applicants;
//        }
        
//        public IEnumerable<DoctoralLevel> GetDoctoralLevelList()
//        {
//            return context.DoctoralLevels;
//        }

//        public IEnumerable<TestScoreType> GetTestScoreTypeList()
//        {
//            return context.TestScoreTypes;
//        }
//        public IEnumerable<Program> GetProgramList()
//        {
//            return context.Programs;
//        }
//        /* ================================== Bk 9/22/2014 ========================================
//          //public Applicant AddNewApplicant(Person person, Applicant app, OsuTimeline osutimeLine, GradeRecord grade, int applicantId)
//        //{
//        //    var newPerson = context.Set<Person>().Add(person);           
//        //    var newApplicant = new Applicant
//        //    {
//        //        Id = Guid.NewGuid(),
//        //        PersonId = newPerson.PersonId,
//        //        ApplicantId = applicantId,
//        //        DoctoralLevelId = app.DoctoralLevelId,
//        //        DepartmentId = app.DepartmentId,
//        //        IsTrainingGrantEligible = app.IsTrainingGrantEligible,
//        //    };
//        //    var newOsuTimeLine = new OsuTimeline
//        //    {
//        //        PersonId = newPerson.PersonId,
//        //        YearEntered = osutimeLine.YearEntered,
//        //        WasInterviewed = osutimeLine.WasInterviewed,
//        //        AcceptedOffer = osutimeLine.AcceptedOffer,
//        //        WasEnrolled = osutimeLine.WasEnrolled
//        //    };
            
//        //    var newGrade = new GradeRecord
//        //    {
//        //        PersonId = newPerson.PersonId,
//        //        Gpa = grade.Gpa,
//        //        GpaScale = grade.GpaScale,
//        //        TestScoreTypeId = grade.TestScoreTypeId,
//        //        GreScoreVerbal = grade.GreScoreVerbal,
//        //        GrePercentileVerbal = grade.GrePercentileVerbal,
//        //        GreScoreQuantitative = grade.GreScoreQuantitative,
//        //        GrePercentileQuantitative = grade.GrePercentileQuantitative,
//        //        GreScoreAnalytical = grade.GreScoreAnalytical,
//        //        GrePercentileAnalytical = grade.GreScoreAnalytical,
//        //        GreScoreSubject = grade.GreScoreSubject,
//        //        GrePercentileSubject = grade.GrePercentileSubject,
//        //        McatScoreVerbalReasoning = grade.McatScoreVerbalReasoning,
//        //        McatScorePhysicalSciences = grade.McatScorePhysicalSciences,
//        //        McatScoreBiologicalSciences = grade.McatScoreBiologicalSciences,
//        //        McatScoreWriting = grade.McatScoreWriting,
//        //        McatPercentile = grade.McatPercentile
//        //    };
            
            
//        //    context.Set<Applicant>().Add(newApplicant);
//        //    context.SaveChanges();

//        //    context.Set<OsuTimeline>().Add(newOsuTimeLine);
//        //    context.SaveChanges();

//        //    context.Set<GradeRecord>().Add(newGrade);
//        //    context.SaveChanges();

//        //    return newApplicant;
//        //}
         
         
          
         
//        //public Applicant AddNewApplicant(Person person, Applicant app, OsuTimeline osutimeLine, int applicantId)
//        public Applicant AddNewApplicant(Person person, Applicant app, OsuTimeline osutimeLine)
//        {
//            var newPerson = context.Set<Person>().Add(person);
//            var newApplicant = new Applicant
//            {
//                Id = Guid.NewGuid(),
//                PersonId = newPerson.PersonId,
//                //ApplicantId = applicantId,
//                ApplicantId = app.ApplicantId,
//                DoctoralLevelId = app.DoctoralLevelId,
//                DepartmentId = app.DepartmentId,
//                IsTrainingGrantEligible = app.IsTrainingGrantEligible,
//            };
//            var newOsu = context.Set<OsuTimeline>().Add(osutimeLine);
//            var newOsuTimeLine = new OsuTimeline
//            {
//                PersonId = newPerson.PersonId,            
//                //YearEntered = osutimeLine.YearEntered     
//                YearEntered = newOsu.YearEntered
//            };
//                context.Set<Applicant>().Add(newApplicant);
//                context.SaveChanges();

//                context.Set<OsuTimeline>().Add(newOsuTimeLine);
//                context.SaveChanges();

//                return newApplicant;
//        }
         
//           ================================= end 9/22/2014 ======================================= */
//    }
//}
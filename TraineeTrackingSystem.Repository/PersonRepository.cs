﻿using System;
using System.Linq;
using System.Data.Entity;
using TraineeTrackingSystem.Common;
using TraineeTrackingSystem.Data;


namespace TraineeTrackingSystem.Repository
{
    public class PersonRepository :  BaseRepository
    {
        public PersonRepository(TTSEntities context)
            :base(context)
        {
        }

        public IQueryable<Person> getPerson()
        {
            return context.People;
        }

        
        public Faculty getFacultyByID(int? Id)
        {
            return context.Faculties.Find(Id);
        }

        
        public ErrorCode AddUpdateFaculty(Faculty newFaculty, string firstName, string middleName, string lastName)
        {
            ErrorCode result = ErrorCode.NotFound;
            bool recordFound = false;

            // adding new person record
            Person personRecord = context.People.Where(o => o.PersonId == newFaculty.PersonId).Include(o => o.Faculties).FirstOrDefault();

            if (personRecord == null)
            {
                personRecord = CreateNewPerson(newFaculty.PersonId, firstName, middleName, lastName);
                context.People.Add(personRecord);
            }
            if (!recordFound)
            {
                context.Faculties.Add(newFaculty);
            }
            context.SaveChanges();
            result = ErrorCode.Success;
            return result;

            
        }

        private static Person CreateNewPerson(Guid personId, string FirstName, string MiddleName, string LastName)
        {
            Person personRecord = new Person();
            personRecord.FirstName = FirstName;
            personRecord.MiddleName = MiddleName;
            personRecord.LastName = LastName;
            personRecord.FirstName = FirstName;

            return personRecord;
        }
        
    }
}
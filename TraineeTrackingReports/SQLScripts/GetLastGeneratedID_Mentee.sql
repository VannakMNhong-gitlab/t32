/****** Object:  UserDefinedFunction [dbo].[GetLastGeneratedID_Mentee]    Script Date: 5/13/2015 3:49:11 PM ******/
DROP FUNCTION [dbo].[GetLastGeneratedID_Mentee]
GO

/****** Object:  UserDefinedFunction [dbo].[GetLastGeneratedID_Mentee]    Script Date: 5/13/2015 3:49:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[GetLastGeneratedID_Mentee]
	()
RETURNS VARCHAR(10) 
AS
Begin
	DECLARE @lastId VARCHAR(10)
	
	SELECT @lastId = LastId.StudentId
	FROM 
		(
			SELECT TOP 1 1 StudentId
			FROM dbo.Mentee
			WHERE StudentId LIKE 'NON%'
			ORDER BY StudentId DESC
		) AS LastId

	RETURN  @lastId
end
GO


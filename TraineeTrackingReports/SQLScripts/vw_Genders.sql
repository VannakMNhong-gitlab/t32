/****** Object:  View [Search].[vw_Genders]    Script Date: 5/2/2016 3:19:21 PM ******/
DROP VIEW [Search].[vw_Genders]
GO

/****** Object:  View [Search].[vw_Genders]    Script Date: 5/2/2016 3:19:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [Search].[vw_Genders] 
AS 

	SELECT
		g.Id AS GenderId
		, g.Name AS Gender
		, (vw.TotalGenders) AS TotalGenderUsage

	FROM 
		dbo.Gender g 
		LEFT OUTER JOIN Search.vw_UsageOfGenders vw ON vw.GenderId = g.Id

	WHERE
		g.IsDeleted = 0


GO


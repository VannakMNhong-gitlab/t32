/****** Object:  View [Search].[vw_FacultyResearchInterests]    Script Date: 5/2/2016 4:40:16 PM ******/
DROP VIEW [Search].[vw_FacultyResearchInterests]
GO

/****** Object:  View [Search].[vw_FacultyResearchInterests]    Script Date: 5/2/2016 4:40:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [Search].[vw_FacultyResearchInterests]
AS
	SELECT        
		fac.Id AS FacultyId
		, p.PersonId
		, p.LastName AS FacultyLastName
		, p.FirstName AS FacultyFirstName
		, p.MiddleName AS FacultyMiddleName
		, CASE 
			WHEN p.LastName IS NULL AND p.FirstName IS NULL THEN NULL
			WHEN p.LastName IS NOT NULL THEN p.LastName
				ELSE ''
			END +
			CASE WHEN p.FirstName IS NOT NULL THEN ', ' + p.FirstName
				ELSE ''
			END +
			CASE WHEN p.MiddleName IS NOT NULL THEN ' ' + p.MiddleName
				ELSE ''
			END AS FullName
		, facRInt.Id AS ResearchInterestId
		, facRInt.ResearchInterest
		, facRInt.DateLastUpdated AS ResearchInterestDateLastUpdated
		, facRInt.LastUpdatedBy AS ResearchInterestLastUpdatedBy
		, updLogins.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLogins.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLogins.MiddleName)), 1) + '. '
			ELSE '' END + updLogins.LastName AS ResearchInterestLastUpdatedByName

	FROM            
		dbo.Faculty fac 
		INNER JOIN dbo.Person p ON p.PersonId = fac.PersonId
			AND p.IsDeleted = 0
		LEFT OUTER JOIN dbo.FacultyResearchInterest facRInt ON facRInt.PersonId = p.PersonId
			AND facRInt.IsDeleted = 0
		LEFT OUTER JOIN Search.vw_Logins updLogins ON updLogins.Username = facRInt.LastUpdatedBy

	WHERE
		fac.IsDeleted = 0


GO


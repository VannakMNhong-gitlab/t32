/****** Object:  View [Search].[vw_OrganizationTypes]    Script Date: 1/29/2016 2:57:27 PM ******/
DROP VIEW [Search].[vw_OrganizationTypes]
GO

/****** Object:  View [Search].[vw_OrganizationTypes]    Script Date: 1/29/2016 2:57:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [Search].[vw_OrganizationTypes] 
AS 

	SELECT
		orgType.Id AS OrganizationTypeId
		, orgType.OrganizationType
		, (vw.TotalOrganization) AS TotalOrganizationTypeUsage

	FROM 
		dbo.OrganizationType orgType 
		LEFT OUTER JOIN Search.vw_UsageOfOrganizationTypes vw ON vw.OrganizationTypeId = orgType.Id
	
	WHERE 
		orgType.IsDeleted = 0



GO


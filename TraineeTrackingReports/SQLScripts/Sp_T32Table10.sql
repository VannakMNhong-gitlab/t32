/****** Object:  StoredProcedure [Reports].[Sp_T32Table10]    Script Date: 1/26/2016 3:49:54 PM ******/
DROP PROCEDURE [Reports].[Sp_T32Table10]
GO

/****** Object:  StoredProcedure [Reports].[Sp_T32Table10]    Script Date: 1/26/2016 3:49:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 09/15/2015
-- Description:	Used by T32 TABLE 10 Report -- ADMISSIONS FOR DIVERSITY CANDIDATES



--			NOTE (TODO): PER CUSTOMER: UNTIL TRAINEETRACKING APP IS CONNECTED TO PEOPLESOFT,
--			THIS REPORT CAN ONLY DISPLAY THE DEPTS AND PROGS RELEVANT TO THE SELECTED TRAINING
--			GRANT.


-- =============================================
CREATE PROCEDURE [Reports].[Sp_T32Table10]
	@TrainingGrantId uniqueidentifier
AS
BEGIN

	SET NOCOUNT ON;

	--DECLARE @CurrentYear AS INT = YEAR(GETDATE())

	-- == MOLLEY: THIS IS A HACK IN INTEREST OF TIME. :( ==
	CREATE TABLE #Temp (Id INT, Seq INT, DiversityGroup VARCHAR(MAX))

	INSERT INTO #Temp(Id, Seq, DiversityGroup)
		VALUES(1, 1, 'URM Trainees'),
			(2, 1, 'Trainees With Disabilities'),
			(3, 1, 'Trainees From Disadvantaged Backgrounds'),
			(4, 2, 'URM Trainees'),
			(5, 2, 'Trainees With Disabilities'),
			(6, 2, 'Trainees From Disadvantaged Backgrounds')



	SELECT 
		TrainingGrantId
		, IsRenewal
		, DoctoralLevelId
		, DoctoralLevel
		, Column4ItemTypeId
		, Column4ItemType
		, Column4ItemId
		, Column4ItemName
		, t.Id AS DiversityGroupId
		, t.Seq AS DiversityGroupSequenceId
		, DiversityGroup

	FROM
		Reports.vw_T32Table10 vw
			JOIN #Temp t ON t.Seq = vw.Column4ItemTypeId

	WHERE
		((@TrainingGrantId IS NULL) OR (TrainingGrantId = @TrainingGrantId))

	ORDER BY
		t.Id
		, t.Seq
		, Column4ItemTypeId
		, Column4ItemName


	SET NOCOUNT OFF
END

GO


/****** Object:  View [Search].[vw_Institutions]    Script Date: 6/2/2015 10:30:47 AM ******/
DROP VIEW [Search].[vw_Institutions]
GO

/****** Object:  View [Search].[vw_Institutions]    Script Date: 6/2/2015 10:30:47 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [Search].[vw_Institutions] 
AS 

	SELECT
		inst.Id
		, inst.Name
		, inst.InstitutionIdentifier
		, inst.City
		, inst.StateId AS StateProvinceId
		, st.Abbreviation AS StateProvinceAbbreviation
		, st.FullName AS StateProvinceFullName
		, inst.CountryId AS CountryId
		, inst.InstitutionTypeId
		, instType.Name AS InstitutionType
		, c.Name AS Country
		, (vw.TotalAcademicHistory + vw.TotalFacultyAcademics + vw.TotalFunding
			+ TotalGradeRecord + vw.TotalMenteeSupport + vw.TotalTrainingPeriods
			+ vw.TotalWorkHistory) AS TotalInstitutionUsage


	FROM 
		dbo.Institution inst
		LEFT OUTER JOIN dbo.StateProvince st ON st.Id = inst.StateId
			AND st.IsDeleted = 0
		LEFT OUTER JOIN dbo.Country c ON c.Id = inst.CountryId
			AND c.IsDeleted = 0
		LEFT OUTER JOIN dbo.InstitutionType instType ON instType.Id = inst.InstitutionTypeId
		LEFT OUTER JOIN Search.vw_UsageOfInstitutions vw ON vw.InstitutionId = inst.Id

	WHERE 
		inst.IsDeleted = 0






GO


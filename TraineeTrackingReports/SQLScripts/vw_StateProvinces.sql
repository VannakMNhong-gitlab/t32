/****** Object:  View [Search].[vw_StateProvinces]    Script Date: 12/1/2015 4:12:29 PM ******/
DROP VIEW [Search].[vw_StateProvinces]
GO

/****** Object:  View [Search].[vw_StateProvinces]    Script Date: 12/1/2015 4:12:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_StateProvinces] 
AS 

	SELECT
		stp.Id AS StateProvinceId
		, stp.Abbreviation AS Abbreviation
		, stp.FullName AS FullName
		, stp.CountryId
		, country.Name AS Country
		, (vw.TotalContactAddress + vw.TotalInstitutions) AS TotalStateProvinceUsage

	FROM 
		dbo.StateProvince stp 
		LEFT OUTER JOIN Search.vw_UsageOfStateProvinces vw ON vw.StateProvinceId = stp.Id
		LEFT OUTER JOIN dbo.Country country ON country.Id = stp.CountryId
			AND country.IsDeleted = 0

	WHERE 
		stp.IsDeleted = 0
		




GO


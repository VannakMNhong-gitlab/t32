/****** Object:  View [Search].[vw_TraineeSummaryInfo]    Script Date: 5/2/2016 4:50:37 PM ******/
DROP VIEW [Search].[vw_TraineeSummaryInfo]
GO

/****** Object:  View [Search].[vw_TraineeSummaryInfo]    Script Date: 5/2/2016 4:50:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [Search].[vw_TraineeSummaryInfo] 
AS 
	SELECT DISTINCT
		tsumm.Id AS TraineeSummaryInfoId
		, tsumm.TrainingGrantId
		, fund.Title AS TrainingGrantTitle
		, tsumm.DoctoralLevelId
		, docLvl.LevelName AS DoctoralLevel
		, tsumm.DateStarted AS DateSummaryStarted
		, tsumm.DateEnded AS DateSummaryEnded
		, tsumm.PositionsAwarded
		, tsumm.SupportMonthsAwarded
		, tsumm.TraineesAppointed
		, tsumm.SupportMonthsUsed
		, tsumm.URMTraineesAppointed
		, tsumm.DisabilitiesTraineesAppointed
		, tsumm.DisadvantagedTraineesAppointed
		, tsumm.URMSupportMonthsUsed
		, tsumm.DisabilitiesSupportMonthsUsed
		, tsumm.DisadvantagedSupportMonthsUsed
		, tsumm.NumMDAppointed
		, tsumm.NumMDPhDAppointed
		, tsumm.NumPhDAppointed
		, tsumm.NumOtherDegreeAppointed
		, tsumm.DateLastUpdated
		, tsumm.LastUpdatedBy
		, updLogins.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLogins.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLogins.MiddleName)), 1) + '. '
			ELSE '' END + updLogins.LastName AS LastUpdatedByName

	  FROM 
		dbo.TraineeSummaryInfo tsumm
		INNER JOIN dbo.TrainingGrant tgrant ON tgrant.Id = tsumm.TrainingGrantId
			AND tgrant.IsDeleted = 0
		INNER JOIN dbo.Funding fund ON fund.Id = tgrant.FundingId
			AND fund.IsDeleted = 0
		INNER JOIN dbo.DoctoralLevel docLvl ON docLvl.Id = tsumm.DoctoralLevelId
		LEFT OUTER JOIN Search.vw_Logins updLogins ON updLogins.Username = tsumm.LastUpdatedBy

	WHERE 
		tsumm.IsDeleted = 0
		




GO


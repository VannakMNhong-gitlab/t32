/****** Object:  View [Search].[vw_FacultyMentees]    Script Date: 6/12/2015 5:25:00 PM ******/
DROP VIEW [Search].[vw_FacultyMentees]
GO

/****** Object:  View [Search].[vw_FacultyMentees]    Script Date: 6/12/2015 5:25:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [Search].[vw_FacultyMentees] 
AS 
	SELECT DISTINCT TOP 100 PERCENT
		tp.Id AS TrainingPeriodId
		, mentors.FacultyId AS FacultyId
		, fac.PersonId AS MentorPersonId
		, pMentor.FirstName AS MentorFirstName
		, pMentor.MiddleName AS MentorMiddleName
		, pMentor.LastName AS MentorLastName
		, CASE 
			WHEN pMentor.LastName IS NULL AND pMentor.FirstName IS NULL THEN NULL
			WHEN pMentor.LastName IS NOT NULL THEN pMentor.LastName
				ELSE ''
			END +
			CASE WHEN pMentor.FirstName IS NOT NULL THEN ', ' + pMentor.FirstName
				ELSE ''
			END +
			CASE WHEN pMentor.MiddleName IS NOT NULL THEN ' ' + pMentor.MiddleName
				ELSE ''
			END AS MentorFullName
		, fac.PrimaryOrganizationId
		, pOrg.DisplayName AS MentorPrimaryOrganization
		, tp.YearStarted
		, tp.YearEnded
		, tp.DoctoralLevelId
		, mentee.PersonId AS MenteePersonId
		, pMentee.FirstName AS MenteeFirstName
		, pMentee.MiddleName AS MenteeMiddleName
		, pMentee.LastName AS MenteeLastName
		, CASE 
			WHEN pMentee.LastName IS NULL AND pMentee.FirstName IS NULL THEN NULL
			WHEN pMentee.LastName IS NOT NULL THEN pMentee.LastName
				ELSE ''
			END +
			CASE WHEN pMentee.FirstName IS NOT NULL THEN ', ' + pMentee.FirstName
				ELSE ''
			END +
			CASE WHEN pMentee.MiddleName IS NOT NULL THEN ' ' + pMentee.MiddleName
				ELSE ''
			END AS MenteeFullName


	FROM dbo.TrainingPeriodFaculty mentors
		INNER JOIN dbo.TrainingPeriod tp ON tp.Id = mentors.TrainingPeriodId
			AND tp.IsDeleted = 0
		INNER JOIN dbo.Faculty fac ON fac.Id = mentors.FacultyId
			AND fac.IsDeleted = 0
		INNER JOIN dbo.Person pMentor ON pMentor.PersonId = fac.PersonId
			AND pMentor.IsDeleted = 0
		INNER JOIN dbo.Mentee mentee ON mentee.PersonId = tp.PersonId
			AND mentee.IsDeleted = 0
		INNER JOIN dbo.Person pMentee ON pMentee.PersonId = mentee.PersonId
			AND pMentee.IsDeleted = 0
		LEFT OUTER JOIN dbo.Organization pOrg ON pOrg.Id = fac.PrimaryOrganizationId
			AND pOrg.IsDeleted = 0

	ORDER BY 
		MentorFullName, MenteeFullName



GO


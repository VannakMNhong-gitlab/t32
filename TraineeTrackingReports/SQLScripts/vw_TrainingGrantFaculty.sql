/****** Object:  View [Search].[vw_TrainingGrantFaculty]    Script Date: 5/2/2016 4:52:34 PM ******/
DROP VIEW [Search].[vw_TrainingGrantFaculty]
GO

/****** Object:  View [Search].[vw_TrainingGrantFaculty]    Script Date: 5/2/2016 4:52:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [Search].[vw_TrainingGrantFaculty] 
AS 
	SELECT DISTINCT
		tgrant.Id AS TrainingGrantId
		, fund.Id AS FundingId
		, dbo.StripHTML_Loop(fund.Title) AS Title
		, tgrant.TrainingGrantStatusId
		, tgStatus.Name AS TrainingGrantStatus
		, fund.DateProjectedStart
		, fund.DateProjectedEnd
		, fund.DateStarted AS DateTimeFundingStarted
		, fund.DateEnded AS DateTimeFundingEnded
		, fund.FundingTypeId
		, fundType.Name AS FundingType
		, fund.DateLastUpdated
		, fund.LastUpdatedBy
		, updLoginsFund.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLoginsFund.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLoginsFund.MiddleName)), 1) + '. '
			ELSE '' END + updLoginsFund.LastName AS LastUpdatedByName
		, fac.Id AS FacultyId
		, facPerson.PersonId AS FacultyPersonId
		, facPerson.LastName AS FacultyLastName
		, facPerson.FirstName AS FacultyFirstName
		, CASE 
				WHEN facPerson.LastName IS NULL AND facPerson.FirstName IS NULL THEN NULL
				WHEN facPerson.LastName IS NOT NULL THEN facPerson.LastName
				ELSE ''
			END +
			CASE WHEN facPerson.FirstName IS NOT NULL THEN ', ' + facPerson.FirstName
				ELSE ''
			END +
			CASE WHEN facPerson.MiddleName IS NOT NULL THEN ' ' + facPerson.MiddleName
				ELSE ''
			END AS FacultyFullName
		, org.DisplayName AS FacultyPrimaryAppt
		, fundPrimaryRole.Id AS FacultyPrimaryRoleId
		, fundPrimaryRole.Name AS FacultyPrimaryRole
		, fundSecondRole.Id AS FacultySecondaryRoleId
		, fundSecondRole.Name AS FacultySecondaryRole
		, fund.SponsorId
		, inst.Name AS SponsorName
		, fund.SponsorAwardNumber
		, fundFac.DateLastUpdated AS FundingFacultyDateLastUpdated
		, fundFac.LastUpdatedBy AS FundingFacultyLastUpdatedBy
		, updLoginsFac.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLoginsFac.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLoginsFac.MiddleName)), 1) + '. '
			ELSE '' END + updLoginsFac.LastName AS FundingFacultyLastUpdatedByName

	FROM 
		dbo.Funding fund
		INNER JOIN dbo.TrainingGrant tgrant ON tgrant.FundingId = fund.Id
			AND tgrant.IsDeleted = 0
		LEFT OUTER JOIN dbo.TrainingGrantStatus tgStatus ON tgStatus.Id = tgrant.TrainingGrantStatusId
		LEFT OUTER JOIN dbo.FundingType fundType ON fundType.Id = fund.FundingTypeId
		INNER JOIN dbo.FundingFaculty fundFac ON fundFac.FundingId = fund.Id
			AND fundFac.IsDeleted = 0
		INNER JOIN dbo.Faculty fac ON fac.Id = fundFac.FacultyId
			AND fac.IsDeleted = 0
		INNER JOIN dbo.Person facPerson ON facPerson.PersonId = fac.PersonId
			AND facPerson.IsDeleted = 0
		LEFT OUTER JOIN dbo.FundingRole fundPrimaryRole ON fundPrimaryRole.Id = fundFac.PrimaryRoleId
		LEFT OUTER JOIN dbo.FundingRole fundSecondRole ON fundSecondRole.Id = fundFac.SecondaryRoleId
		LEFT OUTER JOIN dbo.Institution inst ON inst.Id = fund.SponsorId
			AND inst.IsDeleted = 0
		LEFT OUTER JOIN dbo.Organization org ON org.Id = fac.PrimaryOrganizationId
			AND org.IsDeleted = 0
		LEFT OUTER JOIN Search.vw_Logins updLoginsFund ON updLoginsFund.Username = fund.LastUpdatedBy
		LEFT OUTER JOIN Search.vw_Logins updLoginsFac ON updLoginsFac.Username = fundFac.LastUpdatedBy
		
	WHERE 
		fund.IsDeleted = 0
		AND tgrant.Id IS NOT NULL
		




GO


/****** Object:  StoredProcedure [Reports].[Sp_T32Table8A]    Script Date: 1/26/2016 3:49:16 PM ******/
DROP PROCEDURE [Reports].[Sp_T32Table8A]
GO

/****** Object:  StoredProcedure [Reports].[Sp_T32Table8A]    Script Date: 1/26/2016 3:49:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 04/17/2015
-- Description:	Used by T32 TABLE 8A Report -- PREDOC APPLICANTS



--			NOTE (TODO): PER CUSTOMER: UNTIL TRAINEETRACKING APP IS CONNECTED TO PEOPLESOFT,
--			THIS REPORT CAN ONLY DISPLAY THE DEPTS AND PROGS RELEVANT TO THE SELECTED TRAINING
--			GRANT.


-- =============================================
CREATE PROCEDURE [Reports].[Sp_T32Table8A]
	@TrainingGrantId uniqueidentifier
AS
BEGIN

	SET NOCOUNT ON;

	--DECLARE @CurrentYear AS INT = YEAR(GETDATE())


	SELECT DISTINCT
		TrainingGrantId
		, IsRenewal
		, DoctoralLevelId
		, DoctoralLevel
		, Column2ItemTypeId
		, Column2ItemType
		, Column2ItemId
		, Column2ItemName

	FROM
		Reports.vw_T32Table8A

	WHERE
		((@TrainingGrantId IS NULL) OR (TrainingGrantId = @TrainingGrantId))

	ORDER BY
		Column2ItemTypeId
		, Column2ItemName


	-- == OLD QUERY ==
	--SELECT DISTINCT
	--	--ROW_NUMBER() OVER(ORDER BY vw.ApplicantId) AS [Row]
	--	--, 
	--	vw.ApplicantId
	--	, vw.YearEntered
	--	, vw.ApplicantDept   --How to get Dept/Program(s) -- both are multiples???
	--	, vw.ApplicantPrograms
	--	, academics.IsTrainingGrantEligible
	--	, academics.GPA
	--	, dbo.GetDegreeInstitutionsForPersonId(vw.ApplicantPersonId) AS DegreeInstitutions
	--	, dbo.GetDegreeInfoForPersonId(vw.ApplicantPersonId) AS DegreesAndYears
	--	, CAST(academics.GREScoreVerbal AS INT) AS GREScoreVerbal
	--	, CAST(academics.GREScoreQuantitative AS INT) AS GREScoreQuantitative
	--	, CAST(academics.GREScoreAnalytical AS INT) AS GREScoreAnalytical
	--	, CAST(academics.GREScoreSubject AS INT) AS GREScoreSubject
	--	, CASE 
	--		WHEN (academics.GREScoreVerbal IS NULL AND academics.GREScoreQuantitative IS NULL 
	--			AND academics.GREScoreAnalytical IS NULL AND academics.GREScoreSubject IS NULL) THEN 0
	--		ELSE 1
	--	END AS HasGREScores
	--	, CAST(academics.GREPercentileVerbal AS INT) AS GREPercentileVerbal
	--	, CAST(academics.GREPercentileQuantitative AS INT) AS GREPercentileQuantitative
	--	, CAST(academics.GREPercentileAnalytical AS INT) AS GREPercentileAnalytical
	--	, CAST(academics.GREPercentileSubject AS INT) AS GREPercentileSubject
	--	, CASE 
	--		WHEN (academics.GREPercentileVerbal IS NULL AND academics.GREPercentileQuantitative IS NULL 
	--			AND academics.GREPercentileAnalytical IS NULL AND academics.GREPercentileSubject IS NULL) THEN 0
	--		ELSE 1
	--	END AS HasGREPercentiles
	--	, CAST(academics.MCATScoreVerbalReasoning AS INT) AS MCATScoreVerbalReasoning
	--	, CAST(academics.MCATScoreBiologicalSciences AS INT) AS MCATScoreBiologicalSciences
	--	, CAST(academics.MCATScorePhysicalSciences AS INT) AS MCATScorePhysicalSciences
	--	, academics.MCATScoreWriting
	--	, CASE 
	--		WHEN (academics.MCATScoreVerbalReasoning IS NULL AND academics.MCATScoreBiologicalSciences IS NULL 
	--			AND academics.MCATScorePhysicalSciences IS NULL AND academics.MCATScoreWriting IS NULL) THEN 0
	--		ELSE 1
	--	END AS HasMCATScores
	--	, academics.WasInterviewed
	--	, academics.AcceptedOffer
	--	, academics.EnteredProgram
	--	, CASE WHEN (SELECT COUNT(*) FROM Search.vw_TrainingGrantApplicants
	--				WHERE TrainingGrantId = @T32Id) > 0 THEN 1
	--			ELSE 0
	--		END AS IsSupportedByThisGrant
		

	--FROM 
	--	Search.vw_TrainingGrantApplicants vw
	--	INNER JOIN Search.vw_ApplicantAcademicHistory academics ON academics.ApplicantId = vw.ApplicantId

	--WHERE 
	--	vw.ApplicantTypeId = 1
	--	AND ((@CurrentYear IS NULL) OR (vw.YearEntered = @CurrentYear))

	--	--AND @TrainingGrantId = 'D159F84F-5D07-4462-BBC1-F9A8A3714E45'

	SET NOCOUNT OFF
END

GO


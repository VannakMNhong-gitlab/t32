/****** Object:  View [Search].[vw_UsageOfFaculty]    Script Date: 6/12/2015 5:07:29 PM ******/
DROP VIEW [Search].[vw_UsageOfFaculty]
GO

/****** Object:  View [Search].[vw_UsageOfFaculty]    Script Date: 6/12/2015 5:07:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_UsageOfFaculty] 
AS 
	SELECT TOP 100 PERCENT
		fac.Id AS FacultyId
		, ppl.PersonId
		, ppl.LastName
		, ppl.FirstName
		, ppl.MiddleName
		, (SELECT Count(Id) FROM dbo.FacultyAcademicData facac 
			WHERE facac.PersonId = ppl.PersonId
				AND facac.IsDeleted = 0) AS TotalFacultyAcademics
		, (SELECT Count(ProgramId) FROM dbo.FacultyProgram facprog
			WHERE facprog.FacultyId = fac.Id) AS TotalFacultyPrograms
		, (SELECT Count(Id) FROM dbo.FacultyResearchInterest facresint 
			WHERE facresint.PersonId = ppl.PersonId
				AND facresint.IsDeleted = 0) AS TotalFacultyResearchInterests
		, (SELECT Count(ffac.Id) 
			FROM dbo.FundingFaculty ffac 
			LEFT OUTER JOIN dbo.TrainingGrant tg ON tg.FundingId = ffac.FundingId
			WHERE ffac.FacultyId = fac.Id
				AND tg.Id IS NULL
				AND ffac.IsDeleted = 0) AS TotalFundings
		, (SELECT Count(ffac.Id) 
			FROM dbo.FundingFaculty ffac
			INNER JOIN dbo.TrainingGrant tg ON tg.FundingId = ffac.FundingId
				AND tg.IsDeleted = 0		 
			WHERE ffac.FacultyId = fac.Id
				AND ffac.IsDeleted = 0) AS TotalTrainingGrants		
		, (SELECT Count(TrainingPeriodId) FROM dbo.TrainingPeriodFaculty tpfac
			WHERE tpfac.FacultyId = fac.Id) AS TotalTrainingPeriods

	FROM 
		dbo.Faculty fac
		INNER JOIN dbo.Person ppl ON ppl.PersonId = fac.PersonId
			AND ppl.IsDeleted = 0

	WHERE 
		fac.IsDeleted = 0

	GROUP BY
		fac.Id
		, ppl.PersonId
		, ppl.LastName
		, ppl.FirstName
		, ppl.MiddleName

	ORDER BY ppl.LastName, ppl.FirstName, ppl.MiddleName





GO


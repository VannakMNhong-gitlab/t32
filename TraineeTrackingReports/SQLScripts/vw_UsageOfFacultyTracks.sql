/****** Object:  View [Search].[vw_UsageOfFacultyTracks]    Script Date: 5/2/2016 3:18:02 PM ******/
DROP VIEW [Search].[vw_UsageOfFacultyTracks]
GO

/****** Object:  View [Search].[vw_UsageOfFacultyTracks]    Script Date: 5/2/2016 3:18:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_UsageOfFacultyTracks] 
AS 
	SELECT 
		facTrack.Id AS FacultyTrackId
		, facTrack.Name AS FacultyTrack
		, (SELECT Count(FacultyTrackId) 
			FROM dbo.FacultyAppointment facAppt				
			WHERE facAppt.FacultyTrackId =  facTrack.Id
				AND facAppt.IsDeleted = 0) AS TotalFacultyTracks

	FROM 
		dbo.FacultyTrack facTrack

	WHERE
		facTrack.IsDeleted = 0

	GROUP BY
		facTrack.Id
		, facTrack.Name



GO


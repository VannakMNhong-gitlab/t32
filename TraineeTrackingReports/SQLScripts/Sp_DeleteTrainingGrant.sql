/****** Object:  StoredProcedure [Search].[Sp_DeleteTrainingGrant]    Script Date: 7/28/2015 2:59:02 PM ******/
DROP PROCEDURE [Search].[Sp_DeleteTrainingGrant]
GO

/****** Object:  StoredProcedure [Search].[Sp_DeleteTrainingGrant]    Script Date: 7/28/2015 2:59:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 11/12/2014
-- Description:	Perform all of the updates and deletes necessary to 
--				DELETE a Training Grant record
--		NOTE:	Set @SoftDelete to TRUE to soft delete the Training Grant record
--				** HARD DELETES CANNOT BE UNDONE **
-- =============================================
CREATE PROCEDURE [Search].[Sp_DeleteTrainingGrant]
	@TrainingGrantId uniqueidentifier,
	@FundingId uniqueidentifier,
	@DisplayId INT,
	@SoftDelete BIT
AS
BEGIN

	SET NOCOUNT ON;


	-- If @TrainingGrantId & @FundingId were not passed in, go find them to use for deletes
	IF @FundingId IS NULL
		IF @TrainingGrantId IS NOT NULL
			SET @FundingId = (SELECT FundingId FROM dbo.TrainingGrant
							WHERE Id = @TrainingGrantId)
		ELSE IF @DisplayId IS NOT NULL
			SET @FundingId = (SELECT Id FROM dbo.Funding
							WHERE DisplayId = @DisplayId)
		ELSE
			PRINT 'Cannot find the associated parent Funding Record. Training Grant cannot
				be fully deleted without also deleting the parent Funding Record.
				Delete cancelled.'


	-- If @TrainingGrantId was not passed in, go find it to use for deletes
	IF @TrainingGrantId IS NULL AND @FundingId IS NOT NULL
		SET @TrainingGrantId = (SELECT Id FROM dbo.TrainingGrant
								WHERE FundingId = @FundingId)

	-- Make sure passed in TrainingGrant is not a Previous Training Grant for another TG
	DECLARE @LinkedAsPrevious uniqueidentifier
	SET @LinkedAsPrevious = (SELECT TOP 1 Id FROM dbo.TrainingGrant WHERE PreviousTrainingGrantId = @TrainingGrantId)

	IF @LinkedAsPrevious IS NOT NULL
	BEGIN
		PRINT 'Training Grant Id ' + Convert(varchar(64), @TrainingGrantId, 1) + ' is listed as a Previous Training Grant for TGId: ' 
				+ Convert(varchar(64), @LinkedAsPrevious, 1) + '. 
				Training Grant cannot be deleted without also deleting the dependent Training Grant Record.
				Delete cancelled.'
		SET @TrainingGrantId = NULL
	END
	
	-- Set to SoftDelete by default if NULL is passed in
	SET @SoftDelete = ISNULL(@SoftDelete, 1)


	IF @TrainingGrantId IS NOT NULL AND @FundingId IS NOT NULL
	BEGIN

		IF @SoftDelete = 1
		BEGIN

			-- Soft delete all associated Previous Training Grant Applicant records
			--UPDATE dbo.PreviousTrainingGrantApplicant
			--SET	IsDeleted = 1
			--WHERE TrainingGrantId = @TrainingGrantId

			-- Soft delete associated TrainingGrantTrainee records
			UPDATE dbo.TrainingGrantTrainee
			SET	
				IsDeleted = 1,
				DateLastUpdated = GETDATE(),
				LastUpdatedBy = RIGHT(SYSTEM_USER, 6)
			WHERE TrainingGrantId = @TrainingGrantId

			-- Soft delete associated TraineeSummaryInfo records
			UPDATE dbo.TraineeSummaryInfo
			SET	
				IsDeleted = 1,
				DateLastUpdated = GETDATE(),
				LastUpdatedBy = RIGHT(SYSTEM_USER, 6)
			WHERE TrainingGrantId = @TrainingGrantId

			-- Soft delete associated Training Grant record
			UPDATE dbo.TrainingGrant
			SET	
				IsDeleted = 1,
				DateLastUpdated = GETDATE(),
				LastUpdatedBy = RIGHT(SYSTEM_USER, 6)
			WHERE Id = @TrainingGrantId


			-- Soft delete associated Funding record(s)
			EXEC Search.Sp_DeleteFunding @FundingId, NULL, True, True

		END
		ELSE
		BEGIN

			-- HARD delete all associated Previous Training Grant Applicant records
			DELETE 
			FROM dbo.PreviousTrainingGrantApplicant 
			WHERE TrainingGrantId = @TrainingGrantId

			-- HARD delete all associated TrainingGrantDepartment records
			DELETE 
			FROM dbo.TrainingGrantDepartment 
			WHERE TrainingGrantId = @TrainingGrantId

			-- HARD delete all associated TrainingGrantFacultyResearchInterest records
			DELETE 
			FROM dbo.TrainingGrantFacultyResearchInterest 
			WHERE TrainingGrantId = @TrainingGrantId

			---- HARD delete all associated TrainingGrantFunding records
			--DELETE 
			--FROM dbo.TrainingGrantFunding 
			--WHERE TrainingGrantId = @TrainingGrantId

			-- HARD delete all associated TrainingGrantProgram records
			DELETE 
			FROM dbo.TrainingGrantProgram 
			WHERE TrainingGrantId = @TrainingGrantId

			-- HARD delete all associated TraineeSummaryInfo records
			DELETE 
			FROM dbo.TraineeSummaryInfo 
			WHERE TrainingGrantId = @TrainingGrantId

			-- HARD delete all associated TrainingGrantTrainee records
			DELETE 
			FROM dbo.TrainingGrantTrainee 
			WHERE TrainingGrantId = @TrainingGrantId

			-- HARD delete all associated TrainingGrant records
			DELETE 
			FROM dbo.TrainingGrant
			WHERE Id = @TrainingGrantId


			-- HARD delete associated Funding record(s)
			EXEC Search.Sp_DeleteFunding @FundingId, NULL, False, True

		END

	END


	SET NOCOUNT OFF
END

GO


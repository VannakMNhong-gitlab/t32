/****** Object:  View [Search].[vw_ApplicantData]    Script Date: 5/2/2016 4:33:47 PM ******/
DROP VIEW [Search].[vw_ApplicantData]
GO

/****** Object:  View [Search].[vw_ApplicantData]    Script Date: 5/2/2016 4:33:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [Search].[vw_ApplicantData]
AS
	SELECT DISTINCT TOP 100 PERCENT 
		app.Id AS Id
		, app.PersonId
		, p.FirstName
		, p.MiddleName
		, p.LastName
		, CASE 
			WHEN p.LastName IS NULL AND p.FirstName IS NULL THEN NULL
			WHEN p.LastName IS NOT NULL THEN p.LastName
				ELSE ''
			END +
			CASE WHEN p.FirstName IS NOT NULL THEN ', ' + p.FirstName
				ELSE ''
			END +
			CASE WHEN p.MiddleName IS NOT NULL THEN ' ' + p.MiddleName
				ELSE ''
			END AS FullName
		, app.ApplicantId
		, app.IsTrainingGrantEligible
		, p.IsFromDisadvantagedBkgd
		, p.IsIndividualWithDisabilities
		, p.IsUnderrepresentedMinority
		, dept.Id AS DepartmentId
		, dept.DisplayName AS Department
		, docLvl.Id AS ApplicantTypeId
		, docLvl.LevelName AS ApplicantType
		, prog.Id AS ProgramId
		, prog.Title AS Program
		, timeline.YearEntered
		, app.DateLastUpdated AS ApplicantDateLastUpdated
		, app.LastUpdatedBy AS ApplicantLastUpdatedBy
		, updLogins.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLogins.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLogins.MiddleName)), 1) + '. '
			ELSE '' END + updLogins.LastName AS ApplicantLastUpdatedByName

	FROM dbo.Applicant app
		INNER JOIN dbo.Person p ON p.PersonId = app.PersonId
			AND p.IsDeleted = 0
		INNER JOIN dbo.ApplicantProgram appProgs ON appProgs.PersonId = app.PersonId
		INNER JOIN dbo.Program prog ON prog.Id = appProgs.ProgramId
			AND prog.IsDeleted = 0
		LEFT OUTER JOIN dbo.DoctoralLevel docLvl ON docLvl.Id = app.DoctoralLevelId
		LEFT OUTER JOIN dbo.Organization dept ON dept.Id = app.DepartmentId
			AND dept.IsDeleted = 0
		LEFT OUTER JOIN dbo.OSUTimeline timeline ON timeline.PersonId = app.PersonId
			AND timeline.IsDeleted = 0
		LEFT OUTER JOIN Search.vw_Logins updLogins ON updLogins.Username = app.LastUpdatedBy
	
	WHERE
		app.IsDeleted = 0

	ORDER BY ApplicantId



GO


/****** Object:  View [Search].[vw_TrainingGrantDistinctTrainees]    Script Date: 9/15/2015 1:06:03 PM ******/
DROP VIEW [Search].[vw_TrainingGrantDistinctTrainees]
GO

/****** Object:  View [Search].[vw_TrainingGrantDistinctTrainees]    Script Date: 9/15/2015 1:06:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_TrainingGrantDistinctTrainees] 
AS 
	SELECT DISTINCT TOP 100 PERCENT
		vw.TrainingGrantId
		, vw.FundingId
		, vw.Title
		, vw.MenteeId
		, vw.MenteePersonId
		, vw.MenteeFullName AS TraineeFullName
		, vw.DepartmentId
		, vw.DepartmentName
		, vw.ProgramId
		, vw.ProgramTitle
		, vw.MenteeTypeId
		, mentee.IsTrainingGrantEligible

	FROM 
		Search.vw_TrainingGrantTrainees vw
		INNER JOIN dbo.Mentee mentee ON mentee.Id = vw.MenteeId	
			AND mentee.IsDeleted = 0

	ORDER BY
		TrainingGrantId
		, MenteeFullName
		



GO


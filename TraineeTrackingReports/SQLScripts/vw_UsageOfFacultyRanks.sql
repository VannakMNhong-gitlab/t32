/****** Object:  View [Search].[vw_UsageOfFacultyRanks]    Script Date: 1/29/2016 2:58:01 PM ******/
DROP VIEW [Search].[vw_UsageOfFacultyRanks]
GO

/****** Object:  View [Search].[vw_UsageOfFacultyRanks]    Script Date: 1/29/2016 2:58:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [Search].[vw_UsageOfFacultyRanks] 
AS 
	SELECT 
		facrank.Id AS FacultyRankId
		, facrank.[Rank]
		, (SELECT Count(FacultyRankId) FROM dbo.Faculty fac
			WHERE fac.FacultyRankId =  facrank.Id
				AND fac.IsDeleted = 0) AS TotalFaculty

	FROM 
		dbo.FacultyRank facrank

	WHERE
		facrank.IsDeleted = 0

	GROUP BY
		facrank.Id
		, facrank.[Rank]


GO


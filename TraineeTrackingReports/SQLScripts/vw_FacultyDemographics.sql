/****** Object:  View [Search].[vw_FacultyDemographics]    Script Date: 5/2/2016 4:39:24 PM ******/
DROP VIEW [Search].[vw_FacultyDemographics]
GO

/****** Object:  View [Search].[vw_FacultyDemographics]    Script Date: 5/2/2016 4:39:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_FacultyDemographics]
AS
	SELECT 
		fac.Id
		, fac.PersonId
		, p.FirstName
		, p.MiddleName
		, p.LastName
		, CASE 
			WHEN p.LastName IS NULL AND p.FirstName IS NULL THEN NULL
			WHEN p.LastName IS NOT NULL THEN p.LastName
				ELSE ''
			END +
			CASE WHEN p.FirstName IS NOT NULL THEN ', ' + p.FirstName
				ELSE ''
			END +
			CASE WHEN p.MiddleName IS NOT NULL THEN ' ' + p.MiddleName
				ELSE ''
			END AS FullName
		, p.DisplayId
		, fac.EmployeeId
		, fr.[Rank]
		, fac.PositionDepartmentId
		, pDept.DisplayName AS PositionDepartment
		, fac.PrimaryOrganizationId
		, pOrg.DisplayName AS PrimaryOrganization
		, fac.PrimaryOrganizationId AS PositionOrganizationId
		, pOrg.DisplayName AS PositionOrganization
		, sOrg.DisplayName AS SecondaryOrganization
		, fac.MentorId
		, mentor.FirstName AS MentorFirstName
		, mentor.MiddleName AS MentorMiddleName
		, mentor.LastName AS MentorLastName
		, CASE 
			WHEN mentor.LastName IS NULL AND mentor.FirstName IS NULL THEN NULL
			WHEN mentor.LastName IS NOT NULL THEN mentor.LastName
				ELSE ''
			END +
			CASE WHEN mentor.FirstName IS NOT NULL THEN ', ' + mentor.FirstName
				ELSE ''
			END +
			CASE WHEN mentor.MiddleName IS NOT NULL THEN ' ' + mentor.MiddleName
				ELSE ''
			END AS MentorFullName
		, fac.OtherAffiliations
		, fac.OtherTitles
		, fac.IsActive
		, p.GenderId
		, gend.Name AS Gender
		, p.GenderAtBirthId
		, gendBirth.Name AS GenderAtBirth
		, p.EthnicityId
		, eth.Name AS Ethnicity
		, fac.DateLastUpdated AS FacultyDateLastUpdated
		, fac.LastUpdatedBy AS FacultyLastUpdatedBy
		, updLogins.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLogins.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLogins.MiddleName)), 1) + '. '
			ELSE '' END + updLogins.LastName AS FacultyLastUpdatedByName

	FROM dbo.Faculty fac
		INNER JOIN dbo.Person p ON p.PersonId = fac.PersonId
			AND p.IsDeleted = 0
		LEFT OUTER JOIN dbo.FacultyRank fr ON fr.Id = fac.FacultyRankId
		LEFT OUTER JOIN dbo.Organization pOrg ON pOrg.Id = fac.PrimaryOrganizationId
			AND pOrg.IsDeleted = 0
		LEFT OUTER JOIN dbo.Organization pDept ON pDept.Id = fac.PositionDepartmentId
			AND pDept.IsDeleted = 0
		LEFT OUTER JOIN dbo.Person mentor ON mentor.PersonId = fac.MentorId
			AND mentor.IsDeleted = 0
		LEFT OUTER JOIN dbo.Gender gend ON gend.Id = p.GenderId
		LEFT OUTER JOIN dbo.Gender gendBirth ON gendBirth.Id = p.GenderAtBirthId
		LEFT OUTER JOIN dbo.Ethnicity eth ON eth.Id = p.EthnicityId
		LEFT OUTER JOIN Search.vw_Logins updLogins ON updLogins.Username = fac.LastUpdatedBy
		-- MOLLEY (2/29/16: To be removed once the Faculty form eliminates this field)
		LEFT OUTER JOIN dbo.Organization sOrg ON sOrg.Id = fac.SecondaryOrganizationId
			AND sOrg.IsDeleted = 0

	WHERE
		fac.IsDeleted = 0





GO


/****** Object:  StoredProcedure [Search].[Sp_DeletePerson]    Script Date: 7/28/2015 2:50:22 PM ******/
DROP PROCEDURE [Search].[Sp_DeletePerson]
GO

/****** Object:  StoredProcedure [Search].[Sp_DeletePerson]    Script Date: 7/28/2015 2:50:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 11/05/2014
-- Description:	Delete a Person record and all associated records (contain PersonId)
--		NOTE:	Set @SoftDelete to TRUE/1 to soft delete the Person
--				** HARD DELETES CANNOT BE UNDONE **
-- =============================================
CREATE PROCEDURE [Search].[Sp_DeletePerson]
	@PersonId uniqueidentifier,
	@DisplayId INT,
	@SoftDelete BIT
AS
BEGIN

	SET NOCOUNT ON;

	-- If @PersonId was not passed in, go find it to use for deletes
	IF @PersonId IS NULL AND @DisplayId IS NOT NULL
		SET @PersonId = (SELECT PersonId FROM dbo.Person
						WHERE DisplayId = @DisplayId)
	
	-- Set to SoftDelete by default if NULL is passed in
	SET @SoftDelete = ISNULL(@SoftDelete, 1)


	IF @PersonId IS NOT NULL
	BEGIN

		-- Make sure person is not connected to any foreign key tables

		IF @SoftDelete = 1
		BEGIN

			-- Soft delete associated Applicant record(s)
			EXEC Search.Sp_DeleteApplicant @PersonId, NULL, True, False

			-- Soft delete associated Faculty record
			EXEC Search.Sp_DeleteFaculty @PersonId, NULL, NULL, True, False

			-- Soft delete associated Mentee record
			EXEC Search.Sp_DeleteMentee @PersonId, NULL, True, False

			-- Soft delete associated Person record
			UPDATE dbo.Person
			SET	IsDeleted = 1
			WHERE PersonId = @PersonId

		END
		ELSE
		BEGIN

			-- HARD delete associated Applicant record(s)
			EXEC Search.Sp_DeleteApplicant @PersonId, NULL, False, False

			-- Soft delete associated Faculty record
			EXEC Search.Sp_DeleteFaculty @PersonId, NULL, NULL, False, False

			-- Soft delete associated Mentee record
			EXEC Search.Sp_DeleteMentee @PersonId, NULL, False, False

			-- HARD delete Person record
			DELETE 
			FROM dbo.Person 
			WHERE PersonId = @PersonId

		END
	END

	SET NOCOUNT OFF
END

GO


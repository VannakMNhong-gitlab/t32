/****** Object:  View [Search].[vw_UsageOfFundingRoles]    Script Date: 1/29/2016 2:58:10 PM ******/
DROP VIEW [Search].[vw_UsageOfFundingRoles]
GO

/****** Object:  View [Search].[vw_UsageOfFundingRoles]    Script Date: 1/29/2016 2:58:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [Search].[vw_UsageOfFundingRoles] 
AS 
	SELECT 
		fundrole.Id AS FundingRoleId
		, fundrole.Name
		, fundrole.IsTrainingGrantRole
		, (SELECT (Count(PrimaryRoleId) + Count(SecondaryRoleId)) FROM dbo.FundingFaculty ffac
			WHERE fundrole.IsTrainingGrantRole = 0 
				AND ((ffac.PrimaryRoleId = fundrole.Id)
				OR (ffac.SecondaryRoleId = fundrole.Id))
				AND ffac.IsDeleted = 0) AS TotalFundingFaculty
		, (SELECT (Count(PrimaryRoleId) + Count(SecondaryRoleId)) FROM dbo.FundingFaculty ffac
			WHERE fundrole.IsTrainingGrantRole = 1 
				AND ((ffac.PrimaryRoleId = fundrole.Id)
				OR (ffac.SecondaryRoleId = fundrole.Id))
				AND ffac.IsDeleted = 0) AS TotalTrainingGrantFaculty

	FROM 
		dbo.FundingRole fundrole

	WHERE
		fundrole.IsDeleted = 0

	GROUP BY
		fundrole.Id
		, fundrole.Name
		, fundrole.IsTrainingGrantRole


GO


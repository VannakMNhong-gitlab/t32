/****** Object:  StoredProcedure [Reports].[Sp_T32Table3]    Script Date: 1/26/2016 3:44:01 PM ******/
DROP PROCEDURE [Reports].[Sp_T32Table3]
GO

/****** Object:  StoredProcedure [Reports].[Sp_T32Table3]    Script Date: 1/26/2016 3:44:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 04/16/2015
-- Description:	Used by T32 TABLE 3 Report
--				GRANTS for Participating Faculty
-- =============================================
CREATE PROCEDURE [Reports].[Sp_T32Table3]
	@TrainingGrantId uniqueidentifier,
	@CurrentYear INT
AS
BEGIN

	SET NOCOUNT ON;

	SET @CurrentYear = ISNULL(@CurrentYear, YEAR(GETDATE()))

	SELECT 
		CASE WHEN SupportedTGStatusId = 2 THEN NumPredocPositionsRequested
			ELSE NumPreDocTraineesAppointed END AS Predoc
		, CASE WHEN SupportedTGStatusId = 2 THEN NumPostdocPositionsRequested
			ELSE NumPostDocTraineesAppointed END AS Postdoc
	INTO #tblTotalTrainees
	FROM Reports.vw_T32Table3 
	WHERE ((@TrainingGrantId IS NULL) OR (SelectedTrainingGrantId = @TrainingGrantId))
		AND (SupportedTrainingGrantId != @TrainingGrantId)
	GROUP BY 
		SupportedTrainingGrantId
		, SupportedTGStatusId
		, NumPredocPositionsRequested
		, NumPreDocTraineesAppointed
		, NumPostdocPositionsRequested
		, NumPostDocTraineesAppointed




	SELECT DISTINCT
		FacultyId
		, FacultyPersonId
		, FacultyLastName
		, FacultyFirstName
		, FacultyFullName
		, FacultyFullNameAbbrev
		, SelectedTrainingGrantId
		, SupportedTrainingGrantId
		, SupportedTGFundingId
		, SupportedTGTitle
		, SupportedTGFacultyId
		, SupportedTGFacultyPersonId
		, SupportedTGFacultyLastName
		, SupportedTGFacultyFirstName
		, SupportedTGFacultyFullName
		, SupportedTGFacultyFullNameAbbrev
		, SupportedTGStatusId
		, SupportedTGStatusName
		, SupportedTGSponsorId
		, SupportedTGSponsorName
		, SupportedTGSponsorAwardNumber
		, SupportedDateProjectedStart
		, RIGHT('00' + CAST(MONTH(SupportedDateProjectedStart) AS VARCHAR),2) + '/' + RIGHT(YEAR(SupportedDateProjectedStart), 2) AS SupportedDateProjectedStartShort
		, SupportedDateProjectedEnd
		, RIGHT('00' + CAST(MONTH(SupportedDateProjectedEnd) AS VARCHAR),2) + '/' + RIGHT(YEAR(SupportedDateProjectedEnd), 2) AS SupportedDateProjectedEndShort
		, SupportedTGDateStarted
		, RIGHT('00' + CAST(MONTH(SupportedTGDateStarted) AS VARCHAR),2) + '/' + RIGHT(YEAR(SupportedTGDateStarted), 2) AS SupportedTGDateStartedShort
		, SupportedTGDateEnded
		, RIGHT('00' + CAST(MONTH(SupportedTGDateEnded) AS VARCHAR),2) + '/' + RIGHT(YEAR(SupportedTGDateEnded), 2) AS SupportedTGDateEndedShort
		, SupportedTGGRTNumber
		, SupportedTGPrimeAward
		, PdFacultyId
		, PdPersonId
		, PdLastName
		, PdFirstName
		, PdFullName
		, PDNameAbbrev
		, PDPrimaryOrg
		, TotalFaculty
		, NumPredocPositionsRequested		
		, NumPostdocPositionsRequested
		, NumPreDocTraineesAppointed
		, NumPostDocTraineesAppointed
		, (SELECT SUM(predoc) FROM #tblTotalTrainees) as TotalPredocTrainees 
		, (SELECT SUM(postdoc) FROM #tblTotalTrainees) as TotalPostdocTrainees
		--, NumPredocPositionsAwarded
		--, NumPostdocPositionsAwarded
		--, (SELECT COUNT(*) FROM Search.vw_TrainingGrantTrainees mentPre
		--	WHERE mentPre.TrainingGrantId = vw.SupportedTrainingGrantId 
		--	AND mentPre.MenteeTypeId = 1
		--	AND @CurrentYear BETWEEN YEAR(mentPre.AppointmentStartDate) AND YEAR(mentPre.AppointmentEndDate)) AS TotalYrPredocTrainees
		--, (SELECT COUNT(*) FROM Search.vw_TrainingGrantTrainees mentPost
		--	WHERE mentPost.TrainingGrantId = vw.SupportedTrainingGrantId 
		--	AND mentPost.MenteeTypeId = 2
		--	AND @CurrentYear BETWEEN YEAR(mentPost.AppointmentStartDate) AND YEAR(mentPost.AppointmentEndDate)) AS TotalYrPostdocTrainees

	FROM 
		Reports.vw_T32Table3 vw

	WHERE 
		((@TrainingGrantId IS NULL) OR (vw.SelectedTrainingGrantId = @TrainingGrantId))
		AND (vw.SupportedTrainingGrantId != @TrainingGrantId)


	DROP TABLE #tblTotalTrainees

	SET NOCOUNT OFF
END

GO


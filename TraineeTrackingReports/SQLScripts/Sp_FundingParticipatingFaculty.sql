/****** Object:  StoredProcedure [Reports].[Sp_FundingParticipatingFaculty]    Script Date: 5/2/2016 2:54:32 PM ******/
DROP PROCEDURE [Reports].[Sp_FundingParticipatingFaculty]
GO

/****** Object:  StoredProcedure [Reports].[Sp_FundingParticipatingFaculty]    Script Date: 5/2/2016 2:54:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 04/28/2016
-- Description:	Used by Report: Funding
-- =============================================
CREATE PROCEDURE [Reports].[Sp_FundingParticipatingFaculty]
	@FundingId UNIQUEIDENTIFIER

AS
BEGIN

	SET NOCOUNT ON;

	IF @FundingId IS NOT NULL
	BEGIN

		SELECT DISTINCT
			fund.Id
			, dbo.StripHTML_Loop(Title) AS Title
			, fund.FundingTypeId
			, fac.Id AS FacultyId
			, facPerson.PersonId AS FacultyPersonId
			, facPerson.LastName AS FacultyLastName
			, facPerson.FirstName AS FacultyFirstName
			, CASE 
					WHEN facPerson.LastName IS NULL AND facPerson.FirstName IS NULL THEN NULL
					WHEN facPerson.LastName IS NOT NULL THEN facPerson.LastName
					ELSE ''
				END +
				CASE WHEN facPerson.FirstName IS NOT NULL THEN ', ' + facPerson.FirstName
					ELSE ''
				END +
				CASE WHEN facPerson.MiddleName IS NOT NULL THEN ' ' + facPerson.MiddleName
					ELSE ''
				END AS FacultyFullName
			, fundRole.Id AS FacultyRoleId
			, fundRole.Name AS FacultyRole

		FROM 
			dbo.Funding fund
			LEFT OUTER JOIN dbo.TrainingGrant tgrant ON tgrant.FundingId = fund.Id
				AND tgrant.IsDeleted = 0
			INNER JOIN dbo.FundingFaculty fundFac ON fundFac.FundingId = fund.Id
				AND fundFac.IsDeleted = 0
			INNER JOIN dbo.Faculty fac ON fac.Id = fundFac.FacultyId
				AND fac.IsDeleted = 0
			INNER JOIN dbo.Person facPerson ON facPerson.PersonId = fac.PersonId
				AND facPerson.IsDeleted = 0
			INNER JOIN dbo.FundingRole fundRole ON fundRole.Id = fundFac.PrimaryRoleId
		
		WHERE 
			fund.IsDeleted = 0
			AND tgrant.Id IS NULL
			AND fund.Id = @FundingId

	
	END

	SET NOCOUNT OFF
END

GO


/****** Object:  StoredProcedure [Search].[Sp_DeleteMentee]    Script Date: 5/13/2015 3:48:13 PM ******/
DROP PROCEDURE [Search].[Sp_DeleteMentee]
GO

/****** Object:  StoredProcedure [Search].[Sp_DeleteMentee]    Script Date: 5/13/2015 3:48:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 09/15/2014
-- Description:	Perform all of the updates and deletes necessary to 
--				DELETE a Mentee
--		NOTE:	Set @SoftDelete to TRUE to soft delete the Mentee
--				** HARD DELETES CANNOT BE UNDONE **
-- =============================================
CREATE PROCEDURE [Search].[Sp_DeleteMentee]
	@PersonId uniqueidentifier,
	@StudentId INT,
	@SoftDelete BIT,
	@DeletePersonRecord BIT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @MenteeId uniqueidentifier

	-- If @PersonId was not passed in, go find it to use for deletes
	IF @PersonId IS NULL AND @StudentId IS NOT NULL
		SET @PersonId = (SELECT PersonId FROM dbo.Mentee
						WHERE StudentId = @StudentId)
	
	-- Set to SoftDelete by default if NULL is passed in
	SET @SoftDelete = ISNULL(@SoftDelete, 1)

	SET @DeletePersonRecord = ISNULL(@DeletePersonRecord, 1)


	IF @PersonId IS NOT NULL
	BEGIN

		SET @MenteeId = (SELECT Id FROM dbo.Mentee
							WHERE PersonId = @PersonId)

		IF @SoftDelete = 1
		BEGIN

			-- Soft delete all associated Academic History records
			UPDATE dbo.AcademicHistory 
			SET	
				IsDeleted = 1,
				DateLastUpdated = GETDATE(),
				LastUpdatedBy = RIGHT(SYSTEM_USER, 6)
			WHERE PersonId = @PersonId

			-- Soft delete all associated Contact Address records
			UPDATE dbo.ContactAddress 
			SET	IsDeleted = 1
			WHERE PersonId = @PersonId

			-- Soft delete all associated Contact Email records
			UPDATE dbo.ContactEmail
			SET	IsDeleted = 1
			WHERE PersonId = @PersonId

			-- Soft delete all associated Contact Phone records
			UPDATE dbo.ContactPhone
			SET	IsDeleted = 1
			WHERE PersonId = @PersonId

			-- Soft delete all associated Grade Record records
			UPDATE dbo.GradeRecord
			SET	IsDeleted = 1
			WHERE PersonId = @PersonId

			-- Soft delete all associated Leave of Absence records
			UPDATE dbo.LeaveOfAbsence 
			SET	IsDeleted = 1
			WHERE PersonId = @PersonId

			-- Soft delete all associated Mentee Support records
			UPDATE dbo.MenteeSupport 
			SET	
				IsDeleted = 1,
				DateLastUpdated = GETDATE(),
				LastUpdatedBy = RIGHT(SYSTEM_USER, 6)
			WHERE PersonId = @PersonId

			-- Soft delete all associated OSUTimeline records
			UPDATE dbo.OSUTimeline 
			SET	IsDeleted = 1
			WHERE PersonId = @PersonId

			-- Soft delete all associated Publication records
			UPDATE dbo.Publication 
			SET	
				IsDeleted = 1,
				DateLastUpdated = GETDATE(),
				LastUpdatedBy = RIGHT(SYSTEM_USER, 6)
			WHERE AuthorId = @PersonId
				AND CoAuthorId IS NULL

			-- Soft delete all associated Training Grant Trainee records
			UPDATE dbo.TrainingGrantTrainee
			SET	
				IsDeleted = 1,
				DateLastUpdated = GETDATE(),
				LastUpdatedBy = RIGHT(SYSTEM_USER, 6)
			WHERE MenteeId = @MenteeId

			-- Soft delete all associated Training Period records
			UPDATE dbo.TrainingPeriod
			SET	
				IsDeleted = 1,
				DateLastUpdated = GETDATE(),
				LastUpdatedBy = RIGHT(SYSTEM_USER, 6)
			WHERE PersonId = @PersonId

			-- Soft delete all associated Work History records
			UPDATE dbo.WorkHistory
			SET	
				IsDeleted = 1,
				DateLastUpdated = GETDATE(),
				LastUpdatedBy = RIGHT(SYSTEM_USER, 6)
			WHERE PersonId = @PersonId

			-- Soft delete associated Mentee record
			UPDATE dbo.Mentee
			SET	
				IsDeleted = 1,
				DateLastUpdated = GETDATE(),
				LastUpdatedBy = RIGHT(SYSTEM_USER, 6)
			WHERE PersonId = @PersonId

			-- Soft delete associated Person record
			UPDATE dbo.Person
			SET	
				IsDeleted = 1,
				DateLastUpdated = GETDATE(),
				LastUpdatedBy = RIGHT(SYSTEM_USER, 6)
			WHERE PersonId = @PersonId

		END
		ELSE
		BEGIN

			-- HARD delete all associated Academic History records
			DELETE 
			FROM dbo.AcademicHistory 
			WHERE PersonId = @PersonId

			-- HARD delete all associated Contact Address records
			DELETE 
			FROM dbo.ContactAddress 
			WHERE PersonId = @PersonId

			-- HARD delete all associated Contact Email records
			DELETE 
			FROM dbo.ContactEmail
			WHERE PersonId = @PersonId

			-- HARD delete all associated Contact Phone records
			DELETE 
			FROM dbo.ContactPhone
			WHERE PersonId = @PersonId

			-- HARD delete all associated Grade Record records
			DELETE
			FROM dbo.GradeRecord
			WHERE PersonId = @PersonId

			-- HARD delete all associated Leave Of Absence records
			DELETE 
			FROM dbo.LeaveOfAbsence 
			WHERE PersonId = @PersonId


			-- Since a Mentee Support may have Support Source(s) associated, 
			--	HARD delete the JOIN recs 1st
			SELECT Id 
			INTO #Temp1
			FROM dbo.MenteeSupport
			WHERE PersonId = @PersonId

			DELETE
			FROM dbo.MenteeSupportSupportSource
			WHERE MenteeSupportId IN (SELECT Id FROM #Temp1)

			DROP TABLE #Temp1

			-- HARD delete all associated Mentee Support records
			DELETE 
			FROM dbo.MenteeSupport 
			WHERE PersonId = @PersonId

			-- HARD delete all associated OSUTimeline records
			DELETE
			FROM dbo.OSUTimeline 
			WHERE PersonId = @PersonId

			-- HARD delete all associated Publication records
			DELETE
			FROM dbo.Publication 
			WHERE AuthorId = @PersonId

			-- HARD delete all associated Training Grant Trainee records
			DELETE
			FROM dbo.TrainingGrantTrainee 
			WHERE MenteeId = @MenteeId


			-- Since a Training Period may have Faculty/Program(s) associated, 
			--	HARD delete the JOIN recs 1st
			SELECT Id 
			INTO #Temp2
			FROM dbo.TrainingPeriod
			WHERE PersonId = @PersonId

			DELETE
			FROM dbo.TrainingPeriodFaculty
			WHERE TrainingPeriodId IN (SELECT Id FROM #Temp2)

			DELETE
			FROM dbo.TrainingPeriodProgram
			WHERE TrainingPeriodId IN (SELECT Id FROM #Temp2)

			DROP TABLE #Temp2

			-- HARD delete all associated Training Period records
			DELETE
			FROM dbo.TrainingPeriod 
			WHERE PersonId = @PersonId


			-- Since a Work History may have Support Source(s) associated, 
			--	HARD delete the JOIN recs 1st
			SELECT Id 
			INTO #Temp3
			FROM dbo.WorkHistory
			WHERE PersonId = @PersonId

			DELETE
			FROM dbo.WorkHistorySupportSource
			WHERE WorkHistoryId IN (SELECT Id FROM #Temp3)

			DROP TABLE #Temp3

			-- HARD delete all associated Work History records
			DELETE
			FROM dbo.WorkHistory 
			WHERE PersonId = @PersonId


			-- HARD delete associated Mentee record
			DELETE
			FROM dbo.Mentee
			WHERE PersonId = @PersonId

			IF @DeletePersonRecord = 1
			BEGIN
				-- HARD delete associated Person record
				DELETE
				FROM dbo.Person
				WHERE PersonId = @PersonId
			END

		END

	END

	SET NOCOUNT OFF
END

GO


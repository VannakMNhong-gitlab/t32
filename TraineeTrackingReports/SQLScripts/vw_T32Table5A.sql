/****** Object:  View [Reports].[vw_T32Table5A]    Script Date: 5/26/2015 4:27:20 PM ******/
DROP VIEW [Reports].[vw_T32Table5A]
GO

/****** Object:  View [Reports].[vw_T32Table5A]    Script Date: 5/26/2015 4:27:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [Reports].[vw_T32Table5A] 
AS 
	SELECT DISTINCT TOP 100 PERCENT
	
			tgfac.FacultyId
			, fac.PersonId AS FacultyPersonId
			, facPerson.LastName AS FacultyLastName
			, facPerson.FirstName AS FacultyFirstName
			, CASE 
					WHEN facPerson.LastName IS NULL AND facPerson.FirstName IS NULL THEN NULL
					WHEN facPerson.LastName IS NOT NULL THEN facPerson.LastName
					ELSE ''
				END +
				CASE WHEN facPerson.FirstName IS NOT NULL THEN ', ' + facPerson.FirstName
					ELSE ''
				END +
				CASE WHEN facPerson.MiddleName IS NOT NULL THEN ' ' + facPerson.MiddleName
					ELSE ''
				END AS FacultyFullName
			, facPerson.LastName + ', ' + LEFT(facPerson.FirstName,1) + '.' AS FacultyNameAbbrev
			, tgrant.Id AS TrainingGrantId
			, fund.Title
			, fund.IsRenewal
			, mentee.Id AS MenteeId
			, mentee.StudentId
			, mentPerson.PersonId AS MenteePersonId
			, mentPerson.LastName AS MenteeLastName
			, mentPerson.FirstName AS MenteeFirstName
			, CASE 
					WHEN mentPerson.LastName IS NULL AND mentPerson.FirstName IS NULL THEN NULL
					WHEN mentPerson.LastName IS NOT NULL THEN mentPerson.LastName
					ELSE ''
				END +
				CASE WHEN mentPerson.FirstName IS NOT NULL THEN ', ' + mentPerson.FirstName
					ELSE ''
				END +
				CASE WHEN mentPerson.MiddleName IS NOT NULL THEN ' ' + mentPerson.MiddleName
					ELSE ''
				END AS MenteeFullName
			, mentPerson.LastName + ', ' + LEFT(mentPerson.FirstName,1) + '.' AS MenteeNameAbbrev
			, docLvlMent.Id AS MenteeTypeId
			, docLvlMent.LevelName AS MenteeType
			, mentee.IsTrainingGrantEligible
			, tp.YearStarted AS TrainingPeriodYearStarted
			, tp.YearEnded AS TrainingPeriodYearEnded
			, tpInst.Id AS TrainingPeriodInstitutionId
			, tpInst.Name AS TrainingPeriodInstitution
			, deg.Id AS TrainingPeriodDegreeId
			, deg.Name AS TrainingPeriodDegree
			, tp.ResearchProjectTitle
			, menteeSpt.SupportSourceText
			, workHist.PositionTitle
			, workHist.DateStartedPosition
			, workHist.DateEndedPosition

	FROM 
		dbo.FundingFaculty tgfac
		INNER JOIN dbo.Faculty fac ON fac.Id = tgfac.FacultyId
			AND fac.IsDeleted = 0
		INNER JOIN dbo.Person facPerson ON facPerson.PersonId = fac.PersonId
			AND facPerson.IsDeleted = 0
		LEFT OUTER JOIN dbo.TrainingPeriodFaculty lnkFac2TP ON lnkFac2TP.FacultyId = fac.Id
		LEFT OUTER JOIN dbo.TrainingPeriod tp ON tp.Id = lnkFac2TP.TrainingPeriodId
			--AND ((tp.DoctoralLevelId IS NULL) OR (tp.DoctoralLevelId = 1))
			AND tp.IsDeleted = 0
		LEFT OUTER JOIN dbo.Mentee mentee ON mentee.PersonId = tp.PersonId
			AND mentee.IsDeleted = 0
		LEFT OUTER JOIN dbo.Person mentPerson ON mentPerson.PersonId = mentee.PersonId
			AND mentPerson.IsDeleted = 0		
		INNER JOIN dbo.TrainingGrant tgrant ON tgrant.FundingId = tgfac.FundingId
			AND tgrant.IsDeleted = 0
		INNER JOIN dbo.Funding fund ON fund.Id = tgrant.FundingId
			AND fund.IsDeleted = 0
		LEFT OUTER JOIN dbo.DoctoralLevel docLvlTG ON docLvlTG.Id = tgrant.DoctoralLevelId
		LEFT OUTER JOIN dbo.DoctoralLevel docLvlMent ON docLvlMent.Id = tp.DoctoralLevelId
		LEFT OUTER JOIN dbo.Institution tpInst ON tpInst.Id = tp.InstitutionId
			AND tpInst.IsDeleted = 0
		LEFT OUTER JOIN dbo.AcademicDegree deg ON deg.Id = tp.AcademicDegreeId
			AND deg.IsDeleted = 0
		LEFT OUTER JOIN dbo.MenteeSupport menteeSpt ON menteeSpt.PersonId = mentee.PersonId
			AND menteeSpt.IsDeleted = 0
		LEFT OUTER JOIN dbo.WorkHistory workHist ON workHist.PersonId = mentee.PersonId
			AND (SELECT MAX(DateStartedPosition) FROM dbo.WorkHistory workHist2 WHERE workHist2.PersonId = mentee.PersonId) = workHist.DateStartedPosition
			AND workHist.IsDeleted = 0


	WHERE 
		tgfac.IsDeleted = 0
		AND ((tp.DoctoralLevelId IS NULL) OR (tp.DoctoralLevelId = 1))
		--AND (fund.Id IS NOT NULL AND tgrant.Id IS NULL)

		--AND tgrant.Id = 'D159F84F-5D07-4462-BBC1-F9A8A3714E45'		
		--AND docLvlMent.Id = 1
		
	ORDER BY fund.Title, FacultyLastName






GO


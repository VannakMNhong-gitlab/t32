/****** Object:  View [Search].[vw_TrainingPeriodPrograms]    Script Date: 5/2/2016 4:55:38 PM ******/
DROP VIEW [Search].[vw_TrainingPeriodPrograms]
GO

/****** Object:  View [Search].[vw_TrainingPeriodPrograms]    Script Date: 5/2/2016 4:55:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [Search].[vw_TrainingPeriodPrograms] 
AS 
	SELECT DISTINCT
		tpprog.TrainingPeriodId
		, tp.PersonId
		, tp.InstitutionId
		, inst.InstitutionIdentifier
		, inst.Name AS InstitutionName
		, inst.City AS InstitutionCity
		, inst.StateId AS InstitutionStateId
		, stp.Abbreviation AS InstitutionStateAbbreviation
		, stp.FullName AS InstitutionStateFullName
		, inst.CountryId AS InstitutionCountryId
		, c.Name AS InstitutionCountry
		, tp.DoctoralLevelId
		, docLvl.LevelName AS DoctoralLevel
		, tp.YearStarted
		, tp.YearEnded
		, tp.AcademicDegreeId
		, deg.Name AS AcademicDegree
		, tp.DegreeSoughtId
		, degSought.Name AS DegreeSought
		, tp.ResearchProjectTitle
		, tp.DateLastUpdated
		, tp.LastUpdatedBy
		, updLogins.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLogins.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLogins.MiddleName)), 1) + '. '
			ELSE '' END + updLogins.LastName AS LastUpdatedByName
		, tpprog.ProgramId
		, prog.DisplayId AS ProgramDisplayId
		, prog.Title AS ProgramTitle

	FROM dbo.TrainingPeriodProgram tpprog
		INNER JOIN dbo.TrainingPeriod tp ON tp.Id = tpprog.TrainingPeriodId
			AND tp.IsDeleted = 0
		INNER JOIN dbo.Program prog ON prog.Id = tpprog.ProgramId
			AND prog.IsDeleted = 0
		LEFT OUTER JOIN dbo.Institution inst ON inst.Id = tp.InstitutionId
			AND inst.IsDeleted = 0
		LEFT OUTER JOIN dbo.DoctoralLevel docLvl ON docLvl.Id = tp.DoctoralLevelId
		LEFT OUTER JOIN dbo.AcademicDegree deg ON deg.Id = tp.AcademicDegreeId
			AND deg.IsDeleted = 0
		LEFT OUTER JOIN dbo.AcademicDegree degSought ON degSought.Id = tp.DegreeSoughtId
			AND degSought.IsDeleted = 0
		LEFT OUTER JOIN dbo.StateProvince stp ON stp.Id = inst.StateId
			AND stp.IsDeleted = 0
		LEFT OUTER JOIN dbo.Country c ON (c.Id = inst.CountryId OR c.Id = stp.CountryId)
			AND c.IsDeleted = 0
		LEFT OUTER JOIN Search.vw_Logins updLogins ON updLogins.Username = tp.LastUpdatedBy




GO


/****** Object:  UserDefinedFunction [dbo].[GetDegreeInstitutionsForPersonId]    Script Date: 5/13/2015 3:49:04 PM ******/
DROP FUNCTION [dbo].[GetDegreeInstitutionsForPersonId]
GO

/****** Object:  UserDefinedFunction [dbo].[GetDegreeInstitutionsForPersonId]    Script Date: 5/13/2015 3:49:04 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE FUNCTION [dbo].[GetDegreeInstitutionsForPersonId]
(@personId UNIQUEIDENTIFIER)
RETURNS VARCHAR(max) 
AS
Begin
	DECLARE @institutionlist VARCHAR(MAX)		
	
	SELECT @institutionlist = COALESCE(@institutionlist + '; ' ,'') + institutionlist.Name
	FROM 
		(
		SELECT DISTINCT TOP 100 PERCENT inst.Name, hist.YearDegreeCompleted
		FROM dbo.AcademicHistory hist
			JOIN dbo.Institution inst ON inst.Id = hist.InstitutionId
		WHERE hist.PersonId = @personId
			AND hist.IsDeleted = 0
		ORDER BY hist.YearDegreeCompleted
		) AS institutionlist

	RETURN  @institutionlist
end 


GO


/****** Object:  StoredProcedure [Reports].[Sp_GetMenteeList]    Script Date: 5/2/2016 2:54:56 PM ******/
DROP PROCEDURE [Reports].[Sp_GetMenteeList]
GO

/****** Object:  StoredProcedure [Reports].[Sp_GetMenteeList]    Script Date: 5/2/2016 2:54:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 04/14/2016
-- Description:	Used by multiple reports
-- =============================================
CREATE PROCEDURE [Reports].[Sp_GetMenteeList]
	@IncludeExtraOption VARCHAR(25),
	@Status VARCHAR(25),
	@Types VARCHAR(50),
	@Departments VARCHAR(MAX),
	@Institutions VARCHAR(MAX),
	@DoctoralLevel INT

AS
BEGIN

	SET NOCOUNT ON;

	-- == SANITIZE PARAMETERS ==
	IF @IncludeExtraOption = '0' SET @IncludeExtraOption = NULL
	IF @Status = '0' SET @Status = NULL
	IF @Types = '0' SET @Types = NULL
	IF @Departments = '0' SET @Departments = NULL
	IF @Institutions = '0' SET @Institutions = NULL

	SET @Departments = REPLACE(@Departments, '0,', '')	
	SET @Institutions = REPLACE(@Institutions, '0,', '')
	
	-- == Pull apart multi-select parameters ==
	DECLARE @splitTypes TABLE ( id INT IDENTITY(1,1), val NVARCHAR(MAX) )
	DECLARE @splitDepartments TABLE ( id INT IDENTITY(1,1), val NVARCHAR(MAX) )
	DECLARE @splitInstitutions TABLE ( id INT IDENTITY(1,1), val NVARCHAR(MAX) )

	INSERT INTO @splitTypes(val)
	(SELECT val FROM dbo.splitString(@Types, ','))

	INSERT INTO @splitDepartments(val)
	(SELECT val FROM dbo.splitString(@Departments, ','))

	INSERT INTO @splitInstitutions(val)
	(SELECT val FROM dbo.splitString(@Institutions, ','))


	-- 03/22/2016: Per Beth, the DisplayId and Seq#s are not useful, so they are being removed
	-- from this list
	IF @IncludeExtraOption IS NOT NULL
	BEGIN
		-- The SSRS Reports that use this SP need an extra row for an extra option
		SELECT 
			CAST(CAST(0 AS BINARY) AS uniqueidentifier) AS Id
			, CAST(CAST(0 AS BINARY) AS uniqueidentifier) AS PersonId
			, '[' + @IncludeExtraOption + ']' AS Name
			--, '(' + CONVERT(CHAR(10),GETDATE(),101) + ') [' + @IncludeExtraOption + ']' AS PrefixedName
			--, '[' + @IncludeExtraOption + ']' AS SuffixedName
					
		UNION

		SELECT 
			mentee.Id AS Id
			, mentee.PersonId
			--, p.FirstName
			--, p.MiddleName
			--, p.LastName
			, CASE 
				WHEN p.LastName IS NULL AND p.FirstName IS NULL THEN NULL
				WHEN p.LastName IS NOT NULL THEN p.LastName
					ELSE ''
				END +
				CASE WHEN p.FirstName IS NOT NULL THEN ', ' + p.FirstName
					ELSE ''
				END +
				CASE WHEN p.MiddleName IS NOT NULL THEN ' ' + p.MiddleName
					ELSE ''
				END AS [Name]
		
		FROM
			dbo.Mentee mentee
			INNER JOIN dbo.Person p ON p.PersonId = mentee.PersonId
				AND p.IsDeleted = 0

		WHERE
			mentee.IsDeleted = 0

		ORDER BY
			[Name]

	END
	ELSE
	BEGIN

		SELECT 
			mentee.Id
			, mentee.PersonId
			--, p.FirstName
			--, p.MiddleName
			--, p.LastName
			, CASE 
				WHEN p.LastName IS NULL AND p.FirstName IS NULL THEN NULL
				WHEN p.LastName IS NOT NULL THEN p.LastName
					ELSE ''
				END +
				CASE WHEN p.FirstName IS NOT NULL THEN ', ' + p.FirstName
					ELSE ''
				END +
				CASE WHEN p.MiddleName IS NOT NULL THEN ' ' + p.MiddleName
					ELSE ''
				END AS [Name]
		
		FROM
			dbo.Mentee mentee
			INNER JOIN dbo.Person p ON p.PersonId = mentee.PersonId
				AND p.IsDeleted = 0

		WHERE
			mentee.IsDeleted = 0

		ORDER BY
			[Name]

	END		

	SET NOCOUNT OFF
END

GO


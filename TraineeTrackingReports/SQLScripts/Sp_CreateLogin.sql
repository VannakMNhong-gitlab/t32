/****** Object:  StoredProcedure [dbo].[Sp_CreateLogin]    Script Date: 5/2/2016 4:27:14 PM ******/
DROP PROCEDURE [dbo].[Sp_CreateLogin]
GO

/****** Object:  StoredProcedure [dbo].[Sp_CreateLogin]    Script Date: 5/2/2016 4:27:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 09/17/2015
-- Description:	Create a Person record and a Login record (contain PersonId)
-- =============================================
CREATE PROCEDURE [dbo].[Sp_CreateLogin]
	@FirstName VARCHAR(50)
	, @MiddleName VARCHAR(50)
	, @LastName VARCHAR(75)
	, @IsUnderrepresentedMinority INT
    , @IsIndividualWithDisabilities INT
    , @IsFromDisadvantagedBkgd INT
    , @LastUpdatedBy VARCHAR(50)
	, @Username NVARCHAR(100)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE	@DateLastUpdated DATETIME = GETDATE()

	DECLARE @tempID TABLE (
		ColGuid uniqueidentifier
	)


	INSERT INTO dbo.Person(FirstName, MiddleName, LastName, IsUnderrepresentedMinority, IsIndividualWithDisabilities, IsFromDisadvantagedBkgd, DateLastUpdated, LastUpdatedBy)
		OUTPUT inserted.PersonId
		INTO @tempID
	VALUES (@FirstName, @MiddleName, @LastName, @IsUnderrepresentedMinority, @IsIndividualWithDisabilities, @IsFromDisadvantagedBkgd, @DateLastUpdated, @LastUpdatedBy)


	IF (SELECT COUNT(*) FROM @tempID) > 0 
	BEGIN
		INSERT INTO dbo.Logins(Username, PersonId, DateLastUpdated, LastUpdatedBy, CreatedBy)
		VALUES (@Username, (SELECT TOP 1 ColGuid FROM @tempId), @DateLastUpdated, @LastUpdatedBy, @LastUpdatedBy)
	END

	
	SET NOCOUNT OFF
END

GO

GRANT EXECUTE ON [dbo].[Sp_CreateLogin] TO [OSUMC\Right.Web.SQLUser] AS [dbo]
GO


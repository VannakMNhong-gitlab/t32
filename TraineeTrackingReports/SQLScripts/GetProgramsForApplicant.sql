/****** Object:  UserDefinedFunction [dbo].[GetProgramsForApplicant]    Script Date: 5/13/2015 3:49:18 PM ******/
DROP FUNCTION [dbo].[GetProgramsForApplicant]
GO

/****** Object:  UserDefinedFunction [dbo].[GetProgramsForApplicant]    Script Date: 5/13/2015 3:49:18 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE FUNCTION [dbo].[GetProgramsForApplicant]
(@personId UNIQUEIDENTIFIER)
RETURNS VARCHAR(max) 
AS
Begin
	DECLARE @programs VARCHAR(MAX)		
	
	SELECT @programs = COALESCE(@programs + ', ' ,'') + programs.Name
	FROM 
		(
		SELECT DISTINCT prog.Title AS Name
		FROM dbo.ApplicantProgram appprog
			JOIN dbo.Program prog ON prog.Id = appprog.ProgramId
			JOIN dbo.Person ppl ON ppl.PersonId = @personId
		WHERE appprog.PersonId = @personId
			AND prog.IsDeleted = 0
			AND ppl.IsDeleted = 0
		) AS programs

	RETURN  @programs
end 


GO


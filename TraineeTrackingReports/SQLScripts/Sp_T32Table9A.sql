/****** Object:  StoredProcedure [Reports].[Sp_T32Table9A]    Script Date: 1/26/2016 3:49:33 PM ******/
DROP PROCEDURE [Reports].[Sp_T32Table9A]
GO

/****** Object:  StoredProcedure [Reports].[Sp_T32Table9A]    Script Date: 1/26/2016 3:49:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 09/15/2015
-- Description:	Used by T32 TABLE 9A Report -- PREDOC TRAINEES ON TG



--			NOTE (TODO): PER CUSTOMER: UNTIL TRAINEETRACKING APP IS CONNECTED TO PEOPLESOFT,
--			THIS REPORT CAN ONLY DISPLAY THE DEPTS AND PROGS RELEVANT TO THE SELECTED TRAINING
--			GRANT.


-- =============================================
CREATE PROCEDURE [Reports].[Sp_T32Table9A]
	@TrainingGrantId uniqueidentifier
AS
BEGIN

	SET NOCOUNT ON;

	--DECLARE @CurrentYear AS INT = YEAR(GETDATE())


	SELECT DISTINCT
		TrainingGrantId
		, IsRenewal
		, DoctoralLevelId
		, DoctoralLevel
		, Column1ItemTypeId
		, Column1ItemType
		, Column1ItemId
		, Column1ItemName

	FROM
		Reports.vw_T32Table9A

	WHERE
		((@TrainingGrantId IS NULL) OR (TrainingGrantId = @TrainingGrantId))

	ORDER BY
		Column1ItemTypeId
		, Column1ItemName


	SET NOCOUNT OFF
END

GO


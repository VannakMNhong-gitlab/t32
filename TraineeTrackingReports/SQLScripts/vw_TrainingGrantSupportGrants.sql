/****** Object:  View [Search].[vw_TrainingGrantSupportGrants]    Script Date: 5/13/2015 3:45:58 PM ******/
DROP VIEW [Search].[vw_TrainingGrantSupportGrants]
GO

/****** Object:  View [Search].[vw_TrainingGrantSupportGrants]    Script Date: 5/13/2015 3:45:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [Search].[vw_TrainingGrantSupportGrants] 
AS 

	SELECT 
		tgrant.*

	FROM 
		dbo.TrainingGrant tgrant
		INNER JOIN dbo.TrainingGrantSupportGrant lnkGrants ON lnkGrants.TrainingGrantId = tgrant.Id
		INNER JOIN dbo.TrainingGrant supportGrant ON supportGrant.Id = lnkGrants.SupportTrainingGrantId
			AND supportGrant.IsDeleted = 0

	WHERE
		tgrant.IsDeleted = 0



	--SELECT DISTINCT
	--	tgrant.Id		
	--	, tgrant.PreviousTrainingGrantId
	--	, fund.Id AS FundingId
	--	, fund.SponsorAwardNumber
	--	, fund.Title
	--	, fund.FundingStatusId AS GrantStatusId
	--	, fundStatus.Name AS GrantStatus
	--	, fund.SponsorId
	--	, sponsor.Name AS SponsorName
	--	, fund.SponsorReferenceNumber
	--	, fund.GRTNumber
	--	, fund.DateStarted AS DateTimeGrantStarted
	--	, fund.DateEnded AS DateTimeGrantEnded
	--	, fund.FundingTypeId
	--	, fundType.Name AS FundingType
	--	, fund.IsRenewal
	--	, tgrant.DoctoralLevelId
	--	, docLvl.LevelName AS DoctoralLevel
	--	, tgrant.NumPredocPositionsRequested
	--	, tgrant.NumPostdocPositionsRequested
	--	, tgrant.NumPredocPositionsAwarded
	--	, tgrant.NumPostdocPositionsAwarded
	--	, tgrant.PredocSupportMonthsAwarded
	--	, tgrant.PostdocSupportMonthsAwarded
	--	, fund.DateLastUpdated
	--	, fund.LastUpdatedBy
	--	, fac.Id AS PdFacultyId
	--	, facPerson.PersonId AS PdPersonId
	--	, facPerson.LastName AS PdLastName
	--	, facPerson.FirstName AS PdFirstName
	--	, CASE 
	--			WHEN facPerson.LastName IS NULL AND facPerson.FirstName IS NULL THEN NULL
	--			WHEN facPerson.LastName IS NOT NULL THEN facPerson.LastName
	--			ELSE ''
	--		END +
	--		CASE WHEN facPerson.FirstName IS NOT NULL THEN ', ' + facPerson.FirstName
	--			ELSE ''
	--		END +
	--		CASE WHEN facPerson.MiddleName IS NOT NULL THEN ' ' + facPerson.MiddleName
	--			ELSE ''
	--	END AS PdFullName

	--FROM 
	--	dbo.TrainingGrant tgrant
	--	INNER JOIN dbo.Funding fund ON fund.Id = tgrant.FundingId
	--		AND fund.IsDeleted = 0
	--	INNER JOIN dbo.FundingFaculty fundFac ON fundFac.FundingId = fund.Id
	--		AND fundFac.IsDeleted = 0
	--	INNER JOIN dbo.Faculty fac ON fac.Id = fundFac.FacultyId
	--		AND fac.IsDeleted = 0
	--	INNER JOIN dbo.Person facPerson ON facPerson.PersonId = fac.PersonId
	--		AND facPerson.IsDeleted = 0
	--	LEFT OUTER JOIN dbo.FundingStatus fundStatus ON fundStatus.Id = fund.FundingStatusId
	--	LEFT OUTER JOIN dbo.FundingType fundType ON fundType.Id = fund.FundingTypeId
	--	LEFT OUTER JOIN dbo.Institution sponsor ON sponsor.Id = fund.SponsorId
	--		AND sponsor.IsDeleted = 0		
	--	LEFT OUTER JOIN dbo.FundingRole fundRole ON fundRole.Id = fundFac.PrimaryRoleId
	--	LEFT OUTER JOIN dbo.DoctoralLevel docLvl ON docLvl.Id = tgrant.DoctoralLevelId
		
	--WHERE 
	--	tgrant.IsDeleted = 0
	--	AND ((fundRole.Id = 6) OR (fundRole.Id IS NULL))




GO


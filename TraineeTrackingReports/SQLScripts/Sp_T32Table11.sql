/****** Object:  StoredProcedure [Reports].[Sp_T32Table11]    Script Date: 1/26/2016 3:43:27 PM ******/
DROP PROCEDURE [Reports].[Sp_T32Table11]
GO

/****** Object:  StoredProcedure [Reports].[Sp_T32Table11]    Script Date: 1/26/2016 3:43:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 10/19/2015
-- Description:	Used by T32 TABLE 11 Report -- APPOINTMENTS TO TG
-- =============================================
CREATE PROCEDURE [Reports].[Sp_T32Table11]
	@TrainingGrantId uniqueidentifier			-- 'd159f84f-5d07-4462-bbc1-f9a8a3714e45'
AS
BEGIN

	SET NOCOUNT ON;

	SELECT 
		*
	FROM 
		Search.vw_TraineeSummaryInfo

	WHERE 
		TrainingGrantId = @TrainingGrantId
		AND ((YEAR(DateSummaryStarted) <= YEAR(GETDATE())) AND (YEAR(DateSummaryStarted) >= YEAR(GETDATE())-4))
		AND ((YEAR(DateSummaryEnded) <= YEAR(GETDATE())) AND (YEAR(DateSummaryEnded) >= YEAR(GETDATE())-4))

	ORDER BY 
		DateSummaryStarted, DateSummaryEnded


	SET NOCOUNT OFF
END

GO


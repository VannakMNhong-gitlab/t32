/****** Object:  View [Search].[vw_AcademicDegrees]    Script Date: 6/2/2015 10:31:19 AM ******/
DROP VIEW [Search].[vw_AcademicDegrees]
GO

/****** Object:  View [Search].[vw_AcademicDegrees]    Script Date: 6/2/2015 10:31:19 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [Search].[vw_AcademicDegrees] 
AS 
	SELECT 
		deg.Id AS AcademicDegreeId
		, deg.Name
		, deg.[Description]
		, deg.SortOrder
		, (vw.TotalAcademicHistory + vw.TotalFacultyAcademics + vw.TotalTrainingPeriods) AS TotalDegreeUsage

	FROM 
		dbo.AcademicDegree deg
		LEFT OUTER JOIN Search.vw_UsageOfAcademicDegrees vw ON vw.DegreeId = deg.Id

	WHERE 
		deg.IsDeleted = 0


GO


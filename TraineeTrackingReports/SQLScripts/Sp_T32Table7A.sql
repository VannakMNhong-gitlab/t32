/****** Object:  StoredProcedure [Reports].[Sp_T32Table7A]    Script Date: 1/26/2016 3:47:38 PM ******/
DROP PROCEDURE [Reports].[Sp_T32Table7A]
GO

/****** Object:  StoredProcedure [Reports].[Sp_T32Table7A]    Script Date: 1/26/2016 3:47:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 10/09/2015
-- Description:	Used by T32 TABLE 7A Report -- PREDOC APPLICANTS ON TG



--			NOTE (TODO): PER CUSTOMER: UNTIL TRAINEETRACKING APP IS CONNECTED TO PEOPLESOFT,
--			THIS REPORT CAN ONLY DISPLAY THE DEPTS AND PROGS RELEVANT TO THE SELECTED TRAINING
--			GRANT.


-- =============================================
CREATE PROCEDURE [Reports].[Sp_T32Table7A]
	@TrainingGrantId uniqueidentifier
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @TempYears TABLE(id INT IDENTITY(1,1), Yr int)
	DECLARE @CurrentYear INT = YEAR(GETDATE())

	INSERT INTO @TempYears(Yr)
	VALUES(@CurrentYear-4),
		(@CurrentYear-3),
		(@CurrentYear-2),
		(@CurrentYear-1),
		(@CurrentYear)


	DECLARE @TempSummary TABLE(TrainingGrantId uniqueidentifier
		, Column1ItemTypeId INT
		, Column1ItemType VARCHAR(25)
		, Column1ItemId INT
		, Column1ItemName VARCHAR(50))	

	INSERT INTO @TempSummary(TrainingGrantId, Column1ItemTypeId, Column1ItemType, Column1ItemName)
	VALUES (@TrainingGrantId, 4, 'Other', 'Total All Programs')


	SELECT DISTINCT
		TrainingGrantId
		, Column1ItemTypeId
		, Column1ItemType
		, Column1ItemId
		, Column1ItemName
		, y.Yr
		

	FROM
		Reports.vw_T32Table7A t
		CROSS JOIN @TempYears y

	WHERE
		((@TrainingGrantId IS NULL) OR (TrainingGrantId = @TrainingGrantId))


	UNION

	SELECT
		t.*
		, y.Yr

	FROM @TempSummary t
		CROSS JOIN @TempYears y


	ORDER BY
		Column1ItemTypeId
		, Column1ItemName
		, Yr


	SET NOCOUNT OFF
END

GO


/****** Object:  View [Reports].[vw_T32Table3]    Script Date: 9/15/2015 1:08:02 PM ******/
DROP VIEW [Reports].[vw_T32Table3]
GO

/****** Object:  View [Reports].[vw_T32Table3]    Script Date: 9/15/2015 1:08:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Reports].[vw_T32Table3] 
AS 
	SELECT DISTINCT
	
			tgfac.FacultyId
			, participatingfac.PersonId AS FacultyPersonId
			, facPerson.LastName AS FacultyLastName
			, facPerson.FirstName AS FacultyFirstName
			, CASE 
					WHEN facPerson.LastName IS NULL AND facPerson.FirstName IS NULL THEN NULL
					WHEN facPerson.LastName IS NOT NULL THEN facPerson.LastName
					ELSE ''
				END +
				CASE WHEN facPerson.FirstName IS NOT NULL THEN ', ' + facPerson.FirstName
					ELSE ''
				END +
				CASE WHEN facPerson.MiddleName IS NOT NULL THEN ' ' + facPerson.MiddleName
					ELSE ''
				END AS FacultyFullName
			, facPerson.LastName + ', ' + LEFT(facPerson.FirstName,1) + '.' AS FacultyFullNameAbbrev			
			, tgrant.Id AS SelectedTrainingGrantId
			, tggen.Id AS SupportedTrainingGrantId
			, tggen.FundingId AS SupportedTGFundingId
			, dbo.StripHTML_Loop(tggen.Title) AS SupportedTGTitle
			, facspt.Id AS SupportedTGFacultyId
			, facspt.PersonId AS SupportedTGFacultyPersonId
			, facPerson.LastName AS SupportedTGFacultyLastName
			, facPerson.FirstName AS SupportedTGFacultyFirstName
			, CASE 
					WHEN facPerson.LastName IS NULL AND facPerson.FirstName IS NULL THEN NULL
					WHEN facPerson.LastName IS NOT NULL THEN facPerson.LastName
					ELSE ''
				END +
				CASE WHEN facPerson.FirstName IS NOT NULL THEN ', ' + facPerson.FirstName
					ELSE ''
				END +
				CASE WHEN facPerson.MiddleName IS NOT NULL THEN ' ' + facPerson.MiddleName
					ELSE ''
				END AS SupportedTGFacultyFullName
			, facPerson.LastName + ', ' + LEFT(facPerson.FirstName,1) + '.' AS SupportedTGFacultyFullNameAbbrev
			, tggen.TrainingGrantStatusId AS SupportedTGStatusId
			, tggen.TrainingGrantStatus AS SupportedTGStatusName
			, tggen.SponsorId AS SupportedTGSponsorId
			, tggen.SponsorName AS SupportedTGSponsorName
			, tggen.SponsorAwardNumber AS SupportedTGSponsorAwardNumber
			, tggen.DateProjectedStart AS SupportedDateProjectedStart
			, tggen.DateProjectedEnd AS SupportedDateProjectedEnd
			, tggen.DateTimeGrantStarted AS SupportedTGDateStarted
			, tggen.DateTimeGrantEnded AS SupportedTGDateEnded
			, tggen.GRTNumber AS SupportedTGGRTNumber
			, tggen.PrimeAward AS SupportedTGPrimeAward
			, tggen.PdFacultyId
			, tggen.PdPersonId
			, tggen.PdLastName
			, tggen.PdFirstName
			, tggen.PdFullName
			, tggen.PdLastName + ', ' + LEFT(tggen.PdFirstName,1) + '.' AS PDNameAbbrev
			, facPD.PrimaryOrganizationId AS PDPrimaryOrgId
			, orgPD.DisplayName AS PDPrimaryOrg
			, totals.TotalFaculty
			, tggen.NumPredocPositionsRequested
			, tggen.NumPostdocPositionsRequested
			--, tggen.NumPredocPositionsAwarded
			--, tggen.NumPostdocPositionsAwarded
			, tpresumm.TraineesAppointed AS NumPreDocTraineesAppointed
			, tpostsumm.TraineesAppointed AS NumPostDocTraineesAppointed

	FROM 
		dbo.FundingFaculty tgfac
		INNER JOIN dbo.Faculty participatingfac ON participatingfac.Id = tgfac.FacultyId
			AND participatingfac.IsDeleted = 0
		INNER JOIN dbo.TrainingGrant tgrant ON tgrant.FundingId = tgfac.FundingId
			AND tgrant.IsDeleted = 0
		LEFT OUTER JOIN Search.vw_TrainingGrantFaculty tgfacspt ON tgfacspt.FacultyId = participatingfac.Id
		LEFT OUTER JOIN Search.vw_TrainingGrantGeneralData tggen ON tggen.Id = tgfacspt.TrainingGrantId
		INNER JOIN dbo.Faculty facspt ON facspt.Id = tgfacspt.FacultyId
			AND facspt.IsDeleted = 0
		INNER JOIN dbo.Person facPerson ON facPerson.PersonId = tgfacspt.FacultyPersonId
			AND facPerson.IsDeleted = 0
		LEFT OUTER JOIN dbo.Faculty facPD ON facPD.Id = tggen.PdFacultyId
			AND facPD.IsDeleted = 0
		LEFT OUTER JOIN dbo.Organization orgPD ON orgPD.Id = facPD.PrimaryOrganizationId
			AND orgPD.IsDeleted = 0
		INNER JOIN Reports.vw_TrainingGrantTotals totals ON totals.TrainingGrantId = tggen.Id
		LEFT OUTER JOIN Search.vw_TraineeCurrentSummaryInfo tcurrsumm ON tcurrsumm.TrainingGrantId = tggen.Id
		LEFT OUTER JOIN Search.vw_TraineeSummaryInfo tpresumm ON tpresumm.TraineeSummaryInfoId = tcurrsumm.CurrentPredocTraineeSummaryInfoId
		LEFT OUTER JOIN Search.vw_TraineeSummaryInfo tpostsumm ON tpostsumm.TraineeSummaryInfoId = tcurrsumm.CurrentPostdocTraineeSummaryInfoId

	WHERE 
		tgfac.IsDeleted = 0

		--AND tgrant.Id = 'D159F84F-5D07-4462-BBC1-F9A8A3714E45'--'ea3d21b8-5768-498d-87ea-a953728e05b5'






GO


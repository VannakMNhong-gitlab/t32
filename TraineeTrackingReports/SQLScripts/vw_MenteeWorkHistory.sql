/****** Object:  View [Search].[vw_MenteeWorkHistory]    Script Date: 5/2/2016 4:47:23 PM ******/
DROP VIEW [Search].[vw_MenteeWorkHistory]
GO

/****** Object:  View [Search].[vw_MenteeWorkHistory]    Script Date: 5/2/2016 4:47:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [Search].[vw_MenteeWorkHistory]
AS
	SELECT 
		mentee.Id AS MenteeId
		, mentee.PersonId AS MenteePersonId
		, p.FirstName
		, p.MiddleName
		, p.LastName
		, CASE 
			WHEN p.LastName IS NULL AND p.FirstName IS NULL THEN NULL
			WHEN p.LastName IS NOT NULL THEN p.LastName
				ELSE ''
			END +
			CASE WHEN p.FirstName IS NOT NULL THEN ', ' + p.FirstName
				ELSE ''
			END +
			CASE WHEN p.MiddleName IS NOT NULL THEN ' ' + p.MiddleName
				ELSE ''
			END AS FullName
		, p.DisplayId
		, mentee.StudentId
		, mentee.IsTrainingGrantEligible
		, mentee.WasRecruitedToLab
		, dept.DisplayName AS Department
		, instAssoc.Name AS InstitutionAssociation
		, mentee.DateLastUpdated AS MenteeDateLastUpdated
		, mentee.LastUpdatedBy AS MenteeLastUpdatedBy
		, updLoginsMentee.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLoginsMentee.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLoginsMentee.MiddleName)), 1) + '. '
			ELSE '' END + updLoginsMentee.LastName AS MenteeLastUpdatedByName
		--, prog.Title AS Program
		--, docLvl.LevelName AS MenteeType
		--, medRank.[Rank] AS MedicalRank
		, workHist.Id AS WorkHistoryId
		, workHist.InstitutionId
		, workInst.Name AS Institution
		, workHist.DateStartedPosition
		, workHist.DateEndedPosition
		, workHist.PositionTitle
		, workHist.PositionLocation
		, workHist.DateLastUpdated AS WorkHistDateLastUpdated
		, workHist.LastUpdatedBy AS WorkHistLastUpdatedBy
		, updLoginsWorkHist.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLoginsWorkHist.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLoginsWorkHist.MiddleName)), 1) + '. '
			ELSE '' END + updLoginsWorkHist.LastName AS WorkHistLastUpdatedByName

	FROM dbo.Mentee mentee
		INNER JOIN dbo.Person p ON p.PersonId = mentee.PersonId
			AND p.IsDeleted = 0
		--LEFT OUTER JOIN dbo.DoctoralLevel docLvl ON docLvl.Id = mentee.DoctoralLevelId
		--LEFT OUTER JOIN dbo.MedicalRank medRank ON medRank.Id = mentee.MedicalRankId
		LEFT OUTER JOIN dbo.Organization dept ON dept.Id = mentee.DepartmentId
			AND dept.IsDeleted = 0
		LEFT OUTER JOIN dbo.InstitutionAssociation instAssoc ON instAssoc.Id = mentee.InstitutionAssociationId
		--LEFT OUTER JOIN dbo.TrainingPeriod per ON per.PersonId = mentee.PersonId
		--	AND per.IsDeleted = 0
		--LEFT OUTER JOIN dbo.TrainingPeriodProgram perprog ON perprog.TrainingPeriodId = per.Id
		--LEFT OUTER JOIN dbo.Program prog ON prog.Id = perprog.ProgramId
		--LEFT OUTER JOIN dbo.Trainee_TrainingProgram lnkMentee2Prog ON lnkMentee2Prog.PersonId = mentee.PersonId
		--	AND lnkMentee2Prog.IsDeleted = 0
		--	AND lnkMentee2Prog.DateEnded IS NULL OR DATEDIFF(day, lnkMentee2Prog.DateEnded, GETDATE()) > 0
		LEFT OUTER JOIN dbo.WorkHistory workHist ON workHist.PersonId = mentee.PersonId
			AND workHist.IsDeleted = 0
		LEFT OUTER JOIN dbo.Institution workInst ON workInst.Id = workHist.InstitutionId
			AND workInst.IsDeleted = 0
		LEFT OUTER JOIN Search.vw_Logins updLoginsMentee ON updLoginsMentee.Username = mentee.LastUpdatedBy
		LEFT OUTER JOIN Search.vw_Logins updLoginsWorkHist ON updLoginsWorkHist.Username = workHist.LastUpdatedBy
	
	WHERE
		mentee.IsDeleted = 0





GO


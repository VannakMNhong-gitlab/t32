/****** Object:  View [Search].[vw_FacultyData]    Script Date: 5/2/2016 4:38:36 PM ******/
DROP VIEW [Search].[vw_FacultyData]
GO

/****** Object:  View [Search].[vw_FacultyData]    Script Date: 5/2/2016 4:38:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [Search].[vw_FacultyData]
AS
	SELECT 
		fac.Id
		, fac.PersonId
		, p.FirstName
		, p.MiddleName
		, p.LastName
		, CASE 
			WHEN p.LastName IS NULL AND p.FirstName IS NULL THEN NULL
			WHEN p.LastName IS NOT NULL THEN p.LastName
				ELSE ''
			END +
			CASE WHEN p.FirstName IS NOT NULL THEN ', ' + p.FirstName
				ELSE ''
			END +
			CASE WHEN p.MiddleName IS NOT NULL THEN ' ' + p.MiddleName
				ELSE ''
			END AS FullName
		, p.DisplayId
		, fac.EmployeeId
		, fr.[Rank]
		, pOrg.DisplayName AS PrimaryOrganization
		, sOrg.DisplayName AS SecondaryOrganization
		, facRInt.ResearchInterest
		, degs.Name AS Degree
		, fad.AreaOfStudy
		, fad.DegreeYear
		, inst.Id AS DegreeInstitutionId
		, inst.Name AS DegreeInstitution
		, stp.Abbreviation AS DegreeInstitutionStateProvince
		, c.Name AS DegreeInstitutionCountry
		, fac.OtherAffiliations
		, fac.OtherTitles
		, fac.IsActive
		, fac.DateLastUpdated AS FacultyDateLastUpdated
		, fac.LastUpdatedBy AS FacultyLastUpdatedBy
		, updLogins.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLogins.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLogins.MiddleName)), 1) + '. '
			ELSE '' END + updLogins.LastName AS FacultyLastUpdatedByName

	FROM dbo.Faculty fac
		INNER JOIN dbo.Person p ON p.PersonId = fac.PersonId
			AND p.IsDeleted = 0
		LEFT OUTER JOIN dbo.FacultyRank fr ON fr.Id = fac.FacultyRankId
		LEFT OUTER JOIN dbo.Organization pOrg ON pOrg.Id = fac.PrimaryOrganizationId
			AND pOrg.IsDeleted = 0
		LEFT OUTER JOIN dbo.Organization sOrg ON sOrg.Id = fac.SecondaryOrganizationId
			AND sOrg.IsDeleted = 0
		LEFT OUTER JOIN dbo.FacultyResearchInterest facRInt ON facRInt.PersonId = p.PersonId
			AND facRInt.IsDeleted = 0
		LEFT OUTER JOIN dbo.FacultyAcademicData fad ON fad.PersonId = fac.PersonId
			AND fad.IsDeleted = 0
		LEFT OUTER JOIN dbo.AcademicDegree degs ON degs.Id = fad.AcademicDegreeId
			AND degs.IsDeleted = 0
		LEFT OUTER JOIN dbo.Institution inst ON inst.Id = fad.InstitutionId
			AND inst.IsDeleted = 0
		LEFT OUTER JOIN dbo.StateProvince stp ON stp.Id = inst.StateId
			AND stp.IsDeleted = 0
		LEFT OUTER JOIN dbo.Country c ON c.Id = inst.CountryId
			AND c.IsDeleted = 0
		LEFT OUTER JOIN Search.vw_Logins updLogins ON updLogins.Username = fac.LastUpdatedBy

	WHERE
		fac.IsDeleted = 0



GO


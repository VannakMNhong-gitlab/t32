/****** Object:  View [Reports].[vw_TrainingGrantTotals]    Script Date: 5/13/2015 3:41:25 PM ******/
DROP VIEW [Reports].[vw_TrainingGrantTotals]
GO

/****** Object:  View [Reports].[vw_TrainingGrantTotals]    Script Date: 5/13/2015 3:41:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [Reports].[vw_TrainingGrantTotals] 
AS 

	SELECT DISTINCT TOP 100 PERCENT
		tgrant.Id AS TrainingGrantId
		, dbo.StripHTML_Loop(fund.Title) AS TrainingGrantTitle
		, (SELECT COUNT(*) FROM dbo.FundingFaculty 
			WHERE FundingId = tgrant.FundingId AND IsDeleted = 0) AS TotalFaculty
		, (SELECT COUNT(*) FROM Search.vw_TrainingGrantMentees vwM
			WHERE TrainingGrantId = tgrant.Id
			AND ((tgrant.DoctoralLevelId=3) OR vwM.MenteeTypeId = tgrant.DoctoralLevelId)) AS TotalMentees
		, (SELECT COUNT(*) FROM Search.vw_TrainingGrantApplicants vwA
			WHERE TrainingGrantId = tgrant.Id
			AND ((tgrant.DoctoralLevelId=3) OR vwA.ApplicantTypeId = tgrant.DoctoralLevelId)
			AND tgrant.ApplicantPoolAcademicYear = vwA.YearEntered) AS TotalApplicants
		, (SELECT COUNT(*) FROM dbo.TrainingGrantTrainee 
			WHERE TrainingGrantId = tgrant.Id AND IsDeleted = 0) AS TotalTrainees
		, (SELECT COUNT(*) FROM Search.vw_TrainingGrantTrainees 
			WHERE TrainingGrantId = tgrant.Id 
			AND MenteeTypeId=1) AS TotalPredocTrainees
		, (SELECT COUNT(*) FROM Search.vw_TrainingGrantTrainees 
			WHERE TrainingGrantId = tgrant.Id 
			AND MenteeTypeId=2) AS TotalPostdocTrainees

	FROM dbo.TrainingGrant tgrant
		INNER JOIN dbo.Funding fund ON fund.Id = tgrant.FundingId
			AND fund.IsDeleted = 0
	WHERE
		tgrant.IsDeleted = 0

	ORDER BY
		TrainingGrantTitle



GO


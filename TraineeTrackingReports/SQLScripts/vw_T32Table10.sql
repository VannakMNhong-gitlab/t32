/****** Object:  View [Reports].[vw_T32Table10]    Script Date: 9/15/2015 4:36:58 PM ******/
DROP VIEW [Reports].[vw_T32Table10]
GO

/****** Object:  View [Reports].[vw_T32Table10]    Script Date: 9/15/2015 4:36:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [Reports].[vw_T32Table10] 
AS 

	SELECT DISTINCT 
		tgrant.Id AS TrainingGrantId
		, fund.IsRenewal
		, tgrant.DoctoralLevelId
		, docLvl.LevelName AS DoctoralLevel
		, 1 AS Column4ItemTypeId
		, 'Program' AS Column4ItemType
		, prog.DisplayId AS Column4ItemId
		, prog.Title AS Column4ItemName

	FROM dbo.TrainingGrant tgrant
		INNER JOIN dbo.Funding fund ON fund.Id = tgrant.FundingId
			AND fund.IsDeleted = 0
		INNER JOIN dbo.DoctoralLevel docLvl ON docLvl.Id = tgrant.DoctoralLevelId
		INNER JOIN dbo.TrainingGrantProgram tgprog ON tgprog.TrainingGrantId = tgrant.Id
		INNER JOIN dbo.Program prog ON prog.Id = tgprog.ProgramId
			AND prog.IsDeleted = 0

	WHERE 
		tgrant.IsDeleted = 0


	UNION

	SELECT DISTINCT
		tgrant.Id AS TrainingGrantId
		, fund.IsRenewal
		, tgrant.DoctoralLevelId
		, docLvl.LevelName AS DoctoralLevel
		, 2 AS Column4ItemTypeId
		, 'Department' AS Column4ItemType
		, tgdept.OrganizationId AS Column4ItemId
		, dept.DisplayName AS Column4ItemName

	FROM dbo.TrainingGrant tgrant
		INNER JOIN dbo.Funding fund ON fund.Id = tgrant.FundingId
			AND fund.IsDeleted = 0
		LEFT OUTER JOIN dbo.DoctoralLevel docLvl ON docLvl.Id = tgrant.DoctoralLevelId
		LEFT OUTER JOIN dbo.TrainingGrantDepartment tgdept ON tgdept.TrainingGrantId = tgrant.Id
		LEFT OUTER JOIN dbo.Organization dept ON dept.Id = tgdept.OrganizationId
			AND dept.IsDeleted = 0

	WHERE 
		tgrant.IsDeleted = 0

		--AND tgrant.Id = 'D159F84F-5D07-4462-BBC1-F9A8A3714E45'

GO


/****** Object:  View [Search].[vw_TraineeCurrentSummaryInfo]    Script Date: 9/15/2015 1:08:23 PM ******/
DROP VIEW [Search].[vw_TraineeCurrentSummaryInfo]
GO

/****** Object:  View [Search].[vw_TraineeCurrentSummaryInfo]    Script Date: 9/15/2015 1:08:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [Search].[vw_TraineeCurrentSummaryInfo] 
AS 
	SELECT DISTINCT
		tgrant.Id AS TrainingGrantId
		, 1 AS DoctoralLevelId
		, (SELECT TOP 1 tsumm.Id 
			FROM dbo.TraineeSummaryInfo tsumm
			INNER JOIN dbo.TrainingGrant tgrant2 ON tgrant.Id = tgrant2.Id AND tgrant2.Id = tsumm.TrainingGrantId
				AND tgrant.IsDeleted = 0
			WHERE tsumm.IsDeleted = 0
				AND tsumm.DoctoralLevelId = 1
			ORDER BY tsumm.DateStarted DESC, tsumm.DateEnded DESC) AS CurrentPredocTraineeSummaryInfoId
		, (SELECT TOP 1 tsumm.Id 
			FROM dbo.TraineeSummaryInfo tsumm
			INNER JOIN dbo.TrainingGrant tgrant2 ON tgrant.Id = tgrant2.Id AND tgrant2.Id = tsumm.TrainingGrantId
				AND tgrant.IsDeleted = 0
			WHERE tsumm.IsDeleted = 0
				AND tsumm.DoctoralLevelId = 2
			ORDER BY tsumm.DateStarted DESC, tsumm.DateEnded DESC) AS CurrentPostdocTraineeSummaryInfoId

	FROM 
		dbo.TrainingGrant tgrant

	WHERE 
		tgrant.IsDeleted = 0
		
	

GO


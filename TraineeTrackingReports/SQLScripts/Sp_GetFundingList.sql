/****** Object:  StoredProcedure [Reports].[Sp_GetFundingList]    Script Date: 5/2/2016 2:54:47 PM ******/
DROP PROCEDURE [Reports].[Sp_GetFundingList]
GO

/****** Object:  StoredProcedure [Reports].[Sp_GetFundingList]    Script Date: 5/2/2016 2:54:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 04/28/2016
-- Description:	Used by multiple reports
-- =============================================
CREATE PROCEDURE [Reports].[Sp_GetFundingList]
	@IncludeExtraOption VARCHAR(25),
	@Status VARCHAR(25),
	@Types VARCHAR(50),
	@Sponsors VARCHAR(MAX)--,
	--@DateProjectedStart DATETIME,
	--@DateProjectedEnd DATETIME,
	--@DateStarted DATETIME,
	--@DateEnded DATETIME

AS
BEGIN

	SET NOCOUNT ON;

	-- == SANITIZE PARAMETERS ==
	IF @IncludeExtraOption = '0' SET @IncludeExtraOption = NULL
	IF @Status = '0' SET @Status = NULL
	IF @Types = '0' SET @Types = NULL
	IF @Sponsors = '0' SET @Sponsors = NULL
	
	SET @Sponsors = REPLACE(@Sponsors, '0,', '')
	
	-- == Pull apart multi-select parameters ==
	--DECLARE @splitMSCUs TABLE ( id INT IDENTITY(1,1), val NVARCHAR(MAX) )
	DECLARE @splitStatus TABLE ( id INT IDENTITY(1,1), val NVARCHAR(MAX) )
	DECLARE @splitTypes TABLE ( id INT IDENTITY(1,1), val NVARCHAR(MAX) )
	DECLARE @splitSponsors TABLE ( id INT IDENTITY(1,1), val NVARCHAR(MAX) )

	INSERT INTO @splitStatus(val)
	(SELECT val FROM dbo.splitString(@Status, ','))

	INSERT INTO @splitTypes(val)
	(SELECT val FROM dbo.splitString(@Types, ','))

	INSERT INTO @splitSponsors(val)
	(SELECT val FROM dbo.splitString(@Sponsors, ','))


	-- 03/22/2016: Per Beth, the DisplayId and Seq#s are not useful, so they are being removed
	-- from this list
	IF @IncludeExtraOption IS NOT NULL
	BEGIN
		-- The SSRS Reports that use this SP need an extra row for an extra option
		SELECT 
			CAST(CAST(0 AS BINARY) AS uniqueidentifier) AS Id
			, '[' + @IncludeExtraOption + ']' AS Title
			--, '(' + CONVERT(CHAR(10),GETDATE(),101) + ') [' + @IncludeExtraOption + ']' AS PrefixedName
			--, '[' + @IncludeExtraOption + ']' AS SuffixedName
					
		UNION

		SELECT DISTINCT
			fund.Id
			, fund.Title

		FROM 
			dbo.Funding fund
			LEFT OUTER JOIN dbo.FundingStatus fundStatus ON fundStatus.Id = fund.FundingStatusId
			LEFT OUTER JOIN dbo.FundingType fundType ON fundType.Id = fund.FundingTypeId
			LEFT OUTER JOIN dbo.Institution sponsor ON sponsor.Id = fund.SponsorId
				AND sponsor.IsDeleted = 0
			LEFT OUTER JOIN dbo.TrainingGrant tgrant ON tgrant.FundingId = fund.Id
		
		WHERE 
			fund.IsDeleted = 0
			AND tgrant.Id IS NULL
			AND ((@Status IS NULL)
				OR (fundStatus.Id IN (SELECT val FROM @splitStatus))
			)
			AND ((@Types IS NULL)
				OR (fundType.Id IN (SELECT val FROM @splitTypes))
			)
			AND ((@Sponsors IS NULL)
				OR (sponsor.Id IN (SELECT val FROM @splitSponsors))
			)

		ORDER BY
			Title

	END
	ELSE
	BEGIN

		SELECT DISTINCT
			fund.Id
			, fund.Title

		FROM 
			dbo.Funding fund
			LEFT OUTER JOIN dbo.FundingStatus fundStatus ON fundStatus.Id = fund.FundingStatusId
			LEFT OUTER JOIN dbo.FundingType fundType ON fundType.Id = fund.FundingTypeId
			LEFT OUTER JOIN dbo.Institution sponsor ON sponsor.Id = fund.SponsorId
				AND sponsor.IsDeleted = 0
			LEFT OUTER JOIN dbo.TrainingGrant tgrant ON tgrant.FundingId = fund.Id
		
		WHERE 
			fund.IsDeleted = 0
			AND tgrant.Id IS NULL
			AND ((@Status IS NULL)
				OR (fundStatus.Id IN (SELECT val FROM @splitStatus))
			)
			AND ((@Types IS NULL)
				OR (fundType.Id IN (SELECT val FROM @splitTypes))
			)
			AND ((@Sponsors IS NULL)
				OR (sponsor.Id IN (SELECT val FROM @splitSponsors))
			)

		ORDER BY
			Title

	END		

	SET NOCOUNT OFF
END

GO


/****** Object:  View [Search].[vw_FacultyGrantsAndFunding]    Script Date: 5/13/2015 3:42:51 PM ******/
DROP VIEW [Search].[vw_FacultyGrantsAndFunding]
GO

/****** Object:  View [Search].[vw_FacultyGrantsAndFunding]    Script Date: 5/13/2015 3:42:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_FacultyGrantsAndFunding] 
AS 
	SELECT DISTINCT
		fac.Id AS FacultyId
		, facPerson.PersonId AS FacultyPersonId
		, facPerson.LastName AS FacultyLastName
		, facPerson.FirstName AS FacultyFirstName
		, CASE 
				WHEN facPerson.LastName IS NULL AND facPerson.FirstName IS NULL THEN NULL
				WHEN facPerson.LastName IS NOT NULL THEN facPerson.LastName
				ELSE ''
			END +
			CASE WHEN facPerson.FirstName IS NOT NULL THEN ', ' + facPerson.FirstName
				ELSE ''
			END +
			CASE WHEN facPerson.MiddleName IS NOT NULL THEN ' ' + facPerson.MiddleName
				ELSE ''
			END AS FacultyFullName
		, fundRole.Id AS FacultyRoleId
		, fundRole.Name AS FacultyRole
		--, CASE WHEN tgrant.Id IS NULL THEN fund.Id
		--	ELSE tgrant.Id END AS FundGrantId
		, fund.Id AS FundingId
		, tgrant.Id AS TrainingGrantId
		, dbo.StripHTML_Loop(Title) AS Title
		, CASE WHEN tgrant.Id IS NULL THEN fund.FundingStatusId
			ELSE tgrant.TrainingGrantStatusId 
		  END AS StatusId
		, CASE WHEN tgrant.Id IS NULL THEN fundStatus.Name
			ELSE tgStatus.Name
		  END AS StatusName
		, CASE WHEN tgrant.Id IS NULL THEN 'Funding'
			ELSE 'Training Grant' END AS FundingType
		, fund.DateProjectedStart
		, fund.DateProjectedEnd
		, fund.SponsorId
		, sponsor.Name AS SponsorName
		, fund.SponsorAwardNumber
		, fund.DateStarted
		, fund.DateEnded
		, fund.GRTNumber AS GRTNumber
		, fund.PrimeAward
		, costs.CostYear
		, costs.CurrentYearDirectCosts
		, costs.TotalDirectCosts
		

	FROM 
		dbo.Faculty fac
		INNER JOIN dbo.FundingFaculty fundFac ON fundFac.FacultyId = fac.Id
			AND fundFac.IsDeleted = 0
		INNER JOIN dbo.Funding fund ON fund.Id = fundFac.FundingId
			AND fund.IsDeleted = 0
		LEFT OUTER JOIN dbo.TrainingGrant tgrant ON tgrant.FundingId = fund.Id
			AND tgrant.IsDeleted = 0
		INNER JOIN dbo.Person facPerson ON facPerson.PersonId = fac.PersonId
			AND facPerson.IsDeleted = 0
		LEFT OUTER JOIN dbo.FundingStatus fundStatus ON fundStatus.Id = fund.FundingStatusId
		LEFT OUTER JOIN dbo.TrainingGrantStatus tgStatus ON tgStatus.Id = tgrant.TrainingGrantStatusId
		LEFT OUTER JOIN dbo.FundingType fundType ON fundType.Id = fund.FundingTypeId
		LEFT OUTER JOIN dbo.FundingRole fundRole ON fundRole.Id = fundFac.PrimaryRoleId
		LEFT OUTER JOIN dbo.Institution sponsor ON sponsor.Id = fund.SponsorId
			AND sponsor.IsDeleted = 0
		LEFT OUTER JOIN dbo.FundingDirectCost costs ON costs.FundingId = fund.Id
			AND costs.IsDeleted = 0
		
	WHERE 
		fac.IsDeleted = 0



GO


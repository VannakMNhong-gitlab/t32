/****** Object:  StoredProcedure [Reports].[Sp_FundingGeneralInfo]    Script Date: 5/2/2016 2:54:23 PM ******/
DROP PROCEDURE [Reports].[Sp_FundingGeneralInfo]
GO

/****** Object:  StoredProcedure [Reports].[Sp_FundingGeneralInfo]    Script Date: 5/2/2016 2:54:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Molley Collins
-- Create date: 04/28/2016
-- Description:	Used by Report: Funding
-- =============================================
CREATE PROCEDURE [Reports].[Sp_FundingGeneralInfo]
	@FundingId UNIQUEIDENTIFIER

AS
BEGIN

	SET NOCOUNT ON;

	IF @FundingId IS NOT NULL
	BEGIN

		SELECT DISTINCT
			fund.Id			
			, fund.Title
			, fund.FundingStatusId
			, fundStatus.Name AS FundingStatus
			, fund.DateProjectedStart
			, fund.DateProjectedEnd
			, fund.SponsorId
			, sponsor.Name AS SponsorName
			, fund.SponsorReferenceNumber
			, fund.GRTNumber			
			, fund.FundingTypeId
			, fundType.Name AS FundingType
			, fund.SponsorAwardNumber
			, fund.DateStarted AS DateTimeFundingStarted
			, fund.DateEnded AS DateTimeFundingEnded
			, fund.PrimeAward AS PrimeAwardNumber
			, fund.PrimeSponsorId
			, primesponsor.Name AS PrimeSponsorName
			, PrimeAwardPIId
			, facPrimePerson.LastName AS PrimeAwardPILastName
			, facPrimePerson.FirstName AS PrimeAwardPIFirstName
			, CASE 
					WHEN facPrimePerson.LastName IS NULL AND facPrimePerson.FirstName IS NULL THEN NULL
					WHEN facPrimePerson.LastName IS NOT NULL THEN facPrimePerson.LastName
					ELSE ''
				END +
				CASE WHEN facPrimePerson.FirstName IS NOT NULL THEN ', ' + facPrimePerson.FirstName
					ELSE ''
				END +
				CASE WHEN facPrimePerson.MiddleName IS NOT NULL THEN ' ' + facPrimePerson.MiddleName
					ELSE ''
				END AS PrimeAwardPIFullName

		FROM 
			dbo.Funding fund
			LEFT OUTER JOIN dbo.FundingStatus fundStatus ON fundStatus.Id = fund.FundingStatusId
			LEFT OUTER JOIN dbo.FundingType fundType ON fundType.Id = fund.FundingTypeId
			LEFT OUTER JOIN dbo.Institution sponsor ON sponsor.Id = fund.SponsorId
				AND sponsor.IsDeleted = 0
			LEFT OUTER JOIN dbo.TrainingGrant tgrant ON tgrant.FundingId = fund.Id
			LEFT OUTER JOIN dbo.Institution primesponsor ON primesponsor.Id = fund.PrimeSponsorId
			AND primesponsor.IsDeleted = 0
			LEFT OUTER JOIN dbo.Faculty facPrime ON facPrime.Id = fund.PrimeAwardPIId
				AND facPrime.IsDeleted = 0
			LEFT OUTER JOIN dbo.Person facPrimePerson ON facPrimePerson.PersonId = facPrime.PersonId
				AND facPrimePerson.IsDeleted = 0
		
		WHERE 
			fund.IsDeleted = 0
			AND tgrant.Id IS NULL
			AND fund.Id = @FundingId

	
	END

	SET NOCOUNT OFF
END


GO


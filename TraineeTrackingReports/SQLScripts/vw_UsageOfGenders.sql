/****** Object:  View [Search].[vw_UsageOfGenders]    Script Date: 5/2/2016 3:18:26 PM ******/
DROP VIEW [Search].[vw_UsageOfGenders]
GO

/****** Object:  View [Search].[vw_UsageOfGenders]    Script Date: 5/2/2016 3:18:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [Search].[vw_UsageOfGenders] 
AS 
	SELECT 
		g.Id AS GenderId
		, g.Name AS Gender
		, (SELECT Count(GenderId) 
				FROM dbo.Person p		
				WHERE p.GenderId = g.Id
					AND p.IsDeleted = 0) 
		   +
			(SELECT Count(GenderAtBirthId) 
				FROM dbo.Person p		
				WHERE p.GenderAtBirthId = g.Id
					AND p.IsDeleted = 0)
		AS TotalGenders

	FROM 
		dbo.Gender g

	WHERE
		g.IsDeleted = 0

	GROUP BY
		g.Id
		, g.Name
		

GO


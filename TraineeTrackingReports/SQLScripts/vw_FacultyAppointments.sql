/****** Object:  View [Search].[vw_FacultyAppointments]    Script Date: 5/2/2016 4:37:54 PM ******/
DROP VIEW [Search].[vw_FacultyAppointments]
GO

/****** Object:  View [Search].[vw_FacultyAppointments]    Script Date: 5/2/2016 4:37:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_FacultyAppointments]
AS
	SELECT
		facAppt.Id AS FacultyAppointmentId
		, facAppt.PersonId AS FacultyPersonId
		, p.FirstName
		, p.MiddleName
		, p.LastName
		, CASE 
			WHEN p.LastName IS NULL AND p.FirstName IS NULL THEN NULL
			WHEN p.LastName IS NOT NULL THEN p.LastName
				ELSE ''
			END +
			CASE WHEN p.FirstName IS NOT NULL THEN ', ' + p.FirstName
				ELSE ''
			END +
			CASE WHEN p.MiddleName IS NOT NULL THEN ' ' + p.MiddleName
				ELSE ''
			END AS FullName
		, facAppt.InstitutionId
		, inst.Name AS InstitutionName
		, facAppt.FacultyTrackId
		, facTrack.Name AS FacultyTrack
		, facAppt.ClinicalFacultyPathwayId
		, facPath.Name AS ClinicalFacultyPathway
		, facAppt.FacultyRankId
		, facRank.[Rank] AS FacultyRank
		, facAppt.OtherAffiliations
		, facAppt.OtherTitles
		, facAppt.PercentFTE
		, facAppt.DateStarted
		, facAppt.DateEnded
		, facAppt.Comment
		, facAppt.DateLastUpdated AS AppointmentDateLastUpdated
		, facAppt.LastUpdatedBy AS AppointmentLastUpdatedBy
		, updLogins.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLogins.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLogins.MiddleName)), 1) + '. '
			ELSE '' END + updLogins.LastName AS AppointmentLastUpdatedByName

	FROM 
		dbo.FacultyAppointment facAppt
		INNER JOIN dbo.Person p ON p.PersonId = facAppt.PersonId
			AND p.IsDeleted = 0
		INNER JOIN dbo.Institution inst ON inst.Id = facAppt.InstitutionId
			AND inst.IsDeleted = 0
		LEFT OUTER JOIN dbo.FacultyTrack facTrack ON facTrack.Id = facAppt.FacultyTrackId
		LEFT OUTER JOIN dbo.ClinicalFacultyPathway facPath ON facPath.Id = facAppt.ClinicalFacultyPathwayId
		LEFT OUTER JOIN dbo.FacultyRank facRank ON facRank.Id = facAppt.FacultyRankId
		LEFT OUTER JOIN Search.vw_Logins updLogins ON updLogins.Username = facAppt.LastUpdatedBy

	WHERE facAppt.IsDeleted = 0




GO


/****** Object:  View [Search].[vw_Person]    Script Date: 5/2/2016 4:47:51 PM ******/
DROP VIEW [Search].[vw_Person]
GO

/****** Object:  View [Search].[vw_Person]    Script Date: 5/2/2016 4:47:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [Search].[vw_Person] 
AS 
	SELECT DISTINCT
		p.PersonId
		, p.DisplayId
		, p.FirstName
		, p.MiddleName
		, p.LastName
		, CASE 
			WHEN p.LastName IS NULL AND p.FirstName IS NULL THEN NULL			
			WHEN p.LastName IS NOT NULL THEN p.LastName
				ELSE ''
			END +
			CASE WHEN p.FirstName IS NOT NULL THEN ', ' + p.FirstName
				ELSE ''
			END +
			CASE WHEN p.MiddleName IS NOT NULL THEN ' ' + p.MiddleName
				ELSE ''
			END AS FullName
		, p.IsUnderrepresentedMinority
		, p.IsIndividualWithDisabilities
		, p.IsFromDisadvantagedBkgd
		, p.GenderId
		, gend.Name AS Gender
		, p.GenderAtBirthId
		, gendBirth.Name AS GenderAtBirth
		, p.EthnicityId
		, eth.Name AS Ethnicity
		, p.DateCreated
		, fac.EmployeeId
		, mentee.StudentId
		, app.ApplicantId
		, CASE 
			WHEN fac.EmployeeId IS NOT NULL THEN 1
			ELSE 0
		END AS IsFaculty
		, CASE 
			WHEN mentee.StudentId IS NOT NULL THEN 1
			ELSE 0
		END AS IsMentee
		, CASE 
			WHEN app.ApplicantId IS NOT NULL THEN 1
			ELSE 0
		END AS IsApplicant
		, p.DateLastUpdated
		, p.LastUpdatedBy
		, updLogins.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLogins.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLogins.MiddleName)), 1) + '. '
			ELSE '' END + updLogins.LastName AS LastUpdatedByName

	FROM dbo.Person p
		LEFT OUTER JOIN dbo.Gender gend ON gend.Id = p.GenderId
		LEFT OUTER JOIN dbo.Gender gendBirth ON gendBirth.Id = p.GenderAtBirthId
		LEFT OUTER JOIN dbo.Ethnicity eth ON eth.Id = p.EthnicityId
		LEFT JOIN dbo.Faculty fac ON fac.PersonId = p.PersonId
			AND fac.IsDeleted = 0
		LEFT JOIN dbo.Mentee mentee ON mentee.PersonId = p.PersonId
			AND mentee.IsDeleted = 0
		LEFT JOIN dbo.Applicant app ON app.PersonId = p.PersonId
			AND app.IsDeleted = 0
		LEFT OUTER JOIN Search.vw_Logins updLogins ON updLogins.Username = p.LastUpdatedBy

	WHERE 
		p.IsDeleted = 0





GO


/****** Object:  View [Reports].[vw_T32Table4]    Script Date: 5/13/2015 3:41:05 PM ******/
DROP VIEW [Reports].[vw_T32Table4]
GO

/****** Object:  View [Reports].[vw_T32Table4]    Script Date: 5/13/2015 3:41:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [Reports].[vw_T32Table4] 
AS 
	SELECT DISTINCT
	
			tgfac.FacultyId
			, fac.PersonId AS FacultyPersonId
			, facPerson.LastName AS FacultyLastName
			, facPerson.FirstName AS FacultyFirstName
			, CASE 
					WHEN facPerson.LastName IS NULL AND facPerson.FirstName IS NULL THEN NULL
					WHEN facPerson.LastName IS NOT NULL THEN facPerson.LastName
					ELSE ''
				END +
				CASE WHEN facPerson.FirstName IS NOT NULL THEN ', ' + facPerson.FirstName
					ELSE ''
				END +
				CASE WHEN facPerson.MiddleName IS NOT NULL THEN ' ' + facPerson.MiddleName
					ELSE ''
				END AS FacultyFullName
			, facPerson.LastName + ', ' + LEFT(facPerson.FirstName,1) AS FacultyFullNameAbbrev
			, fundRole.Id AS FacultyRoleId
			, fundRole.Name AS FacultyRole
			, fund.Id AS FundingId
			, tgrant.Id AS TrainingGrantId
			, dbo.StripHTML_Loop(fund.Title) AS Title
			, fundStatus.Id AS StatusId
			, fundStatus.Name AS StatusName
			, fund.SponsorId
			, sponsor.Name AS SponsorName
			, fund.SponsorAwardNumber
			, fund.DateStarted
			, fund.DateEnded
			, fund.GRTNumber
			, fund.PrimeAward
			--, costs.CostYear
			--, costs.CurrentYearDirectCosts
			--, costs.TotalDirectCosts

	FROM 
		dbo.FundingFaculty tgfac
		INNER JOIN dbo.Faculty fac ON fac.Id = tgfac.FacultyId
			AND fac.IsDeleted = 0
		INNER JOIN dbo.Person facPerson ON facPerson.PersonId = fac.PersonId
			AND facPerson.IsDeleted = 0
		INNER JOIN dbo.TrainingGrant tgrant ON tgrant.FundingId = tgfac.FundingId
			AND tgrant.IsDeleted = 0
		LEFT OUTER JOIN Search.vw_FundingFaculty fundfac ON fundfac.FacultyId = fac.Id
		LEFT OUTER JOIN dbo.Funding fund ON fund.Id = fundfac.Id
			AND fund.FundingStatusId IN (2,4)
			AND fund.IsDeleted = 0
		LEFT OUTER JOIN dbo.FundingStatus fundStatus ON fundStatus.Id = fund.FundingStatusId			
		LEFT OUTER JOIN dbo.FundingRole fundRole ON fundRole.Id = fundFac.FacultyRoleId
		LEFT OUTER JOIN dbo.Institution sponsor ON sponsor.Id = fund.SponsorId
			AND sponsor.IsDeleted = 0
		--LEFT OUTER JOIN dbo.FundingDirectCost costs ON costs.FundingId = fund.Id
		--	AND costs.IsDeleted = 0
		--	AND ((costs.CostYear IS NULL) OR (costs.CostYear = @CurrentYear))

	WHERE 
		tgfac.IsDeleted = 0
		--AND (fund.Id IS NOT NULL AND tgrant.Id IS NULL)

		--AND tgrant.Id = 'D159F84F-5D07-4462-BBC1-F9A8A3714E45'
		


GO


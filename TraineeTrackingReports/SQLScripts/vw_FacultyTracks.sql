/****** Object:  View [Search].[vw_FacultyTracks]    Script Date: 5/2/2016 3:18:51 PM ******/
DROP VIEW [Search].[vw_FacultyTracks]
GO

/****** Object:  View [Search].[vw_FacultyTracks]    Script Date: 5/2/2016 3:18:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [Search].[vw_FacultyTracks] 
AS 

	SELECT
		facTrack.Id AS FacultyTrackId
		, facTrack.Name AS FacultyTrack
		, (vw.TotalFacultyTracks) AS TotalFacultyTrackUsage

	FROM 
		dbo.FacultyTrack facTrack 
		LEFT OUTER JOIN Search.vw_UsageOfFacultyTracks vw ON vw.FacultyTrackId = facTrack.Id

	WHERE
		facTrack.IsDeleted = 0


GO


/****** Object:  View [Search].[vw_FundingProgramProjects]    Script Date: 5/2/2016 4:43:08 PM ******/
DROP VIEW [Search].[vw_FundingProgramProjects]
GO

/****** Object:  View [Search].[vw_FundingProgramProjects]    Script Date: 5/2/2016 4:43:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_FundingProgramProjects] 
AS 
	SELECT DISTINCT TOP 100 PERCENT
		fund.Id AS ParentFundId
		, fund.SponsorAwardNumber AS ParentFundSponsorAwardNumber
		, fund.Title AS ParentFundTitle
		, fund.FundingStatusId AS ParentFundStatusId
		, fundstat.Name AS ParentFundStatus
		, sponsor.Id AS ParentFundSponsorId
		, sponsor.Name AS ParentFundSponsor
		, fund.SponsorReferenceNumber AS ParentFundSponsorReferenceNumber
		, fund.GRTNumber AS ParentFundGRTNumber
		, fund.DateStarted AS ParentFundDateStarted
		, fund.DateEnded AS ParentFundDateEnded
		, fund.FundingTypeId AS ParentFundTypeId
		, fundtype.Name AS ParentFundType
		, facPersonPI.LastName AS ParentFundPILastName
		, facPersonPI.FirstName AS ParentFundPIFirstName
		, CASE 
				WHEN facPersonPI.LastName IS NULL AND facPersonPI.FirstName IS NULL THEN NULL
				WHEN facPersonPI.LastName IS NOT NULL THEN facPersonPI.LastName
				ELSE ''
			END +
			CASE WHEN facPersonPI.FirstName IS NOT NULL THEN ', ' + facPersonPI.FirstName
				ELSE ''
			END +
			CASE WHEN facPersonPI.MiddleName IS NOT NULL THEN ' ' + facPersonPI.MiddleName
				ELSE ''
			END AS ParentFundPIFullName
		, fund.DateLastUpdated AS ParentFundDateLastUpdated
		, fund.LastUpdatedBy AS ParentFundLastUpdatedBy
		, updLoginsParent.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLoginsParent.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLoginsParent.MiddleName)), 1) + '. '
			ELSE '' END + updLoginsParent.LastName AS ParentFundLastUpdatedByName
		, fund.DisplayId AS ParentFundDisplayId
		, fund.DateProjectedStart AS ParentFundDateProjectedStart
		, fund.DateProjectedEnd AS ParentFundDateProjectedEnd
		, fund.ProgramProjectPDId AS ParentFundProgramProjectPDId
		, facProjectPerson.LastName AS ParentFundProgramProjectPDLastName
		, facProjectPerson.FirstName AS ParentFundProgramProjectPDFirstName
		, CASE 
				WHEN facProjectPerson.LastName IS NULL AND facProjectPerson.FirstName IS NULL THEN NULL
				WHEN facProjectPerson.LastName IS NOT NULL THEN facProjectPerson.LastName
				ELSE ''
			END +
			CASE WHEN facProjectPerson.FirstName IS NOT NULL THEN ', ' + facProjectPerson.FirstName
				ELSE ''
			END +
			CASE WHEN facProjectPerson.MiddleName IS NOT NULL THEN ' ' + facProjectPerson.MiddleName
				ELSE ''
			END AS ParentFundProgramProjectPDFullName
		, progproj.Id AS ProjectFundId
		, progproj.SponsorAwardNumber AS ProjectFundSponsorAwardNumber
		, progproj.Title AS ProjectFundTitle
		, progproj.FundingStatusId AS ProjectFundStatusId
		, projstat.Name AS ProjectFundStatus
		, projsponsor.Id AS ProjectFundSponsorId
		, projsponsor.Name AS ProjectFundSponsor
		, progproj.SponsorReferenceNumber AS ProjectFundSponsorReferenceNumber
		, progproj.GRTNumber AS ProjectFundGRTNumber
		, progproj.DateStarted AS ProjectFundDateStarted
		, progproj.DateEnded AS ProjectFundDateEnded
		, progproj.FundingTypeId AS ProjectFundTypeId
		, projtype.Name AS ProjectFundType
		, progproj.DateLastUpdated AS ProjectFundDateLastUpdated
		, progproj.LastUpdatedBy AS ProjectFundLastUpdatedBy
		, updLoginsProject.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLoginsProject.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLoginsProject.MiddleName)), 1) + '. '
			ELSE '' END + updLoginsProject.LastName AS ProjectFundLastUpdatedByName
		, progproj.DisplayId AS ProjectFundDisplayId
		, progproj.DateProjectedStart AS ProjectFundDateProjectedStart
		, progproj.DateProjectedEnd AS ProjectFundDateProjectedEnd

	FROM 
		dbo.Funding AS fund
		INNER JOIN dbo.FundingStatus AS fundstat ON fundstat.Id = fund.FundingStatusId
		INNER JOIN dbo.FundingType AS fundtype ON fundtype.Id = fund.FundingTypeId
		INNER JOIN dbo.FundingFaculty fundFacPI ON fundFacPI.FundingId = fund.Id
			AND fundFacPI.IsDeleted = 0
		INNER JOIN dbo.Faculty facPI ON facPI.Id = fundFacPI.FacultyId
			AND facPI.IsDeleted = 0
		INNER JOIN dbo.Person facPersonPI ON facPersonPI.PersonId = facPI.PersonId
			AND facPersonPI.IsDeleted = 0
		LEFT OUTER JOIN dbo.Institution AS sponsor ON sponsor.Id = fund.SponsorId
			AND sponsor.IsDeleted = 0
		--INNER JOIN dbo.FundingProgramProjects AS fpp ON fpp.FundingId = fund.Id
		--INNER JOIN dbo.Funding AS progproj ON progproj.Id = fpp.ProgramProjectId
		--	AND progproj.IsDeleted = 0
		INNER JOIN dbo.Funding AS progproj ON progproj.GRTNumber = fund.GRTNumber
			AND progproj.Id != fund.Id
			AND progproj.IsDeleted = 0
		INNER JOIN dbo.FundingStatus AS projstat ON projstat.Id = progproj.FundingStatusId
		INNER JOIN dbo.FundingType AS projtype ON projtype.Id = progproj.FundingTypeId
		LEFT OUTER JOIN dbo.Faculty facProject ON facProject.Id = fund.ProgramProjectPDId
			AND facProject.IsDeleted = 0
		LEFT OUTER JOIN dbo.Person facProjectPerson ON facProjectPerson.PersonId = facProject.PersonId
			AND facProjectPerson.IsDeleted = 0
		LEFT OUTER JOIN dbo.Institution AS projsponsor ON projsponsor.Id = progproj.SponsorId
			AND projsponsor.IsDeleted = 0
		LEFT OUTER JOIN Search.vw_Logins updLoginsParent ON updLoginsParent.Username = fund.LastUpdatedBy
		LEFT OUTER JOIN Search.vw_Logins updLoginsProject ON updLoginsProject.Username = progproj.LastUpdatedBy

	WHERE
		fund.IsDeleted = 0
		--AND fund.FundingTypeId = 1

	--ORDER BY
	--	ParentFu




GO


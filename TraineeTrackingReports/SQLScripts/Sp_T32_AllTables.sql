/****** Object:  StoredProcedure [Reports].[Sp_T32_AllTables]    Script Date: 1/26/2016 3:49:08 PM ******/
DROP PROCEDURE [Reports].[Sp_T32_AllTables]
GO

/****** Object:  StoredProcedure [Reports].[Sp_T32_AllTables]    Script Date: 1/26/2016 3:49:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 04/17/2015
-- Description:	Used by T32 ALL TABLES Report
-- =============================================
CREATE PROCEDURE [Reports].[Sp_T32_AllTables]
	@TrainingGrantId uniqueidentifier
AS
BEGIN

	SET NOCOUNT ON;

	SELECT DISTINCT
		vw.Id AS TrainingGrantId
		, vw.GRTNumber
		, vw.Title
		, vw.TrainingGrantStatus
		, vw.SponsorName
		, vw.SponsorReferenceNumber
		, vw.SponsorAwardNumber		
		, vw.DateTimeGrantStarted
		, vw.DateTimeGrantEnded
		, vw.IsRenewal
		, vw.DoctoralLevelId
		, vw.DoctoralLevel
		, vw.NumPredocPositionsRequested
		, vw.NumPostdocPositionsRequested

	FROM 
		Search.vw_TrainingGrantGeneralData vw

	WHERE 
		((@TrainingGrantId IS NULL) OR (vw.Id = @TrainingGrantId))

		--AND @TrainingGrantId = 'D159F84F-5D07-4462-BBC1-F9A8A3714E45'

	SET NOCOUNT OFF
END

GO


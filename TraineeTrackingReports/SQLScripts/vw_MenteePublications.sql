/****** Object:  View [Search].[vw_MenteePublications]    Script Date: 5/2/2016 4:46:29 PM ******/
DROP VIEW [Search].[vw_MenteePublications]
GO

/****** Object:  View [Search].[vw_MenteePublications]    Script Date: 5/2/2016 4:46:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [Search].[vw_MenteePublications]
AS
	SELECT 
		mentee.Id AS MenteeId
		, mentee.PersonId AS MenteePersonId
		, p.FirstName
		, p.MiddleName
		, p.LastName
		, CASE 
			WHEN p.LastName IS NULL AND p.FirstName IS NULL THEN NULL
			WHEN p.LastName IS NOT NULL THEN p.LastName
				ELSE ''
			END +
			CASE WHEN p.FirstName IS NOT NULL THEN ', ' + p.FirstName
				ELSE ''
			END +
			CASE WHEN p.MiddleName IS NOT NULL THEN ' ' + p.MiddleName
				ELSE ''
			END AS FullName
		, p.DisplayId
		, mentee.StudentId
		, mentee.IsTrainingGrantEligible
		, mentee.DateLastUpdated AS MenteeDateLastUpdated
		, mentee.LastUpdatedBy AS MenteeLastUpdatedBy
		, updLoginsMentee.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLoginsMentee.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLoginsMentee.MiddleName)), 1) + '. '
			ELSE '' END + updLoginsMentee.LastName AS MenteeLastUpdatedByName
		, dept.DisplayName AS Department
		--, docLvl.LevelName AS MenteeType
		, pub.Id AS Pub_GUID
		, pub.Title AS PubTitle
		, pub.YearPublished AS PubYearPublished
		, pub.Abstract AS PubAbstract
		, pub.PMCID AS PubPMCID
		, pub.PMID AS PubPMID
		, pub.ManuscriptId AS PubManuscriptId
		, pub.Journal AS PubJournal
		, pub.Volume AS PubVolume
		, pub.Issue AS PubIssue
		, pub.Pagination AS PubPagination
		, pub.Citation AS PubCitation
		, pub.IsPublicAccess AS PubIsPublicAccess
		, pub.IsMenteeFirstAuthor AS PubIsMenteeFirstAuthor
		, pub.IsAdvisorCoAuthor AS PubIsAdvisorCoAuthor
		, pub.AuthorList AS PubAuthorList
		, pub.DatePublicationLastUpdated AS PubDatePublicationLastUpdated
		, pub.DateLastUpdated AS PubDateLastUpdated
		, pub.LastUpdatedBy AS PubLastUpdatedBy
		, updLoginsPub.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLoginsPub.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLoginsPub.MiddleName)), 1) + '. '
			ELSE '' END + updLoginsPub.LastName AS PubLastUpdatedByName

	FROM dbo.Mentee mentee
		INNER JOIN dbo.Person p ON p.PersonId = mentee.PersonId
			AND p.IsDeleted = 0
		--LEFT OUTER JOIN dbo.DoctoralLevel docLvl ON docLvl.Id = mentee.DoctoralLevelId
		LEFT OUTER JOIN dbo.Organization dept ON dept.Id = mentee.DepartmentId
			AND dept.IsDeleted = 0
		LEFT OUTER JOIN dbo.Publication pub ON ((pub.AuthorId = mentee.PersonId) OR (pub.CoAuthorId = mentee.PersonId))
			AND pub.IsDeleted = 0
		LEFT OUTER JOIN Search.vw_Logins updLoginsMentee ON updLoginsMentee.Username = mentee.LastUpdatedBy
		LEFT OUTER JOIN Search.vw_Logins updLoginsPub ON updLoginsPub.Username = pub.LastUpdatedBy
	
	WHERE
		mentee.IsDeleted = 0




GO


/****** Object:  View [Search].[vw_ApplicantPrograms]    Script Date: 10/6/2015 11:59:04 AM ******/
DROP VIEW [Search].[vw_ApplicantPrograms]
GO

/****** Object:  View [Search].[vw_ApplicantPrograms]    Script Date: 10/6/2015 11:59:04 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Fixed FullName to show NULL instead of '' if there's no First or Last Name
CREATE VIEW [Search].[vw_ApplicantPrograms] 
AS 
	SELECT DISTINCT
		app.Id
		, appProg.PersonId
		, p.FirstName
		, p.MiddleName
		, p.LastName
		, CASE 
			WHEN p.LastName IS NULL AND p.FirstName IS NULL THEN NULL
			WHEN p.LastName IS NOT NULL THEN p.LastName
				ELSE ''
			END +
			CASE WHEN p.FirstName IS NOT NULL THEN ', ' + p.FirstName
				ELSE ''
			END +
			CASE WHEN p.MiddleName IS NOT NULL THEN ' ' + p.MiddleName
				ELSE ''
			END AS FullName
		, app.ApplicantId
		, appProg.ProgramId
		, prog.DisplayId AS ProgramDisplayId
		, prog.Title AS ProgramTitle

	FROM dbo.ApplicantProgram appProg
		INNER JOIN dbo.Applicant app ON app.PersonId = appProg.PersonId
			AND app.IsDeleted = 0
		INNER JOIN dbo.Person p ON p.PersonId = app.PersonId
			AND p.IsDeleted = 0
		INNER JOIN dbo.Program prog ON prog.Id = appProg.ProgramId
			AND prog.IsDeleted = 0



GO


/****** Object:  View [Search].[vw_TrainingGrantGeneralData]    Script Date: 5/2/2016 4:53:54 PM ******/
DROP VIEW [Search].[vw_TrainingGrantGeneralData]
GO

/****** Object:  View [Search].[vw_TrainingGrantGeneralData]    Script Date: 5/2/2016 4:53:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [Search].[vw_TrainingGrantGeneralData] 
AS 
	SELECT DISTINCT
		tgrant.Id		
		, tgrant.PreviousTrainingGrantId
		, fund.Id AS FundingId
		, fund.SponsorAwardNumber
		, dbo.StripHTML_Loop(fund.Title) AS Title
		, tgrant.TrainingGrantStatusId
		, tgStatus.Name AS TrainingGrantStatus
		, fund.DateProjectedStart
		, fund.DateProjectedEnd
		, fund.SponsorId
		, sponsor.Name AS SponsorName
		, fund.SponsorReferenceNumber
		, fund.GRTNumber
		, fund.PrimeAward
		, fund.DateStarted AS DateTimeGrantStarted
		, fund.DateEnded AS DateTimeGrantEnded
		, fund.FundingTypeId
		, fundType.Name AS FundingType
		, fund.IsRenewal
		, tgrant.DoctoralLevelId
		, docLvl.LevelName AS DoctoralLevel
		, tgrant.NumPredocPositionsRequested
		, tgrant.NumPostdocPositionsRequested
		, tgrant.NumPredocPositionsAwarded
		, tgrant.NumPostdocPositionsAwarded
		, tgrant.PredocSupportMonthsAwarded
		, tgrant.PostdocSupportMonthsAwarded
		, fund.DateLastUpdated
		, fund.LastUpdatedBy
		, updLogins.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLogins.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLogins.MiddleName)), 1) + '. '
			ELSE '' END + updLogins.LastName AS LastUpdatedByName
		, fac.Id AS PdFacultyId
		, facPerson.PersonId AS PdPersonId
		, facPerson.LastName AS PdLastName
		, facPerson.FirstName AS PdFirstName
		, CASE 
				WHEN facPerson.LastName IS NULL AND facPerson.FirstName IS NULL THEN NULL
				WHEN facPerson.LastName IS NOT NULL THEN facPerson.LastName
				ELSE ''
			END +
			CASE WHEN facPerson.FirstName IS NOT NULL THEN ', ' + facPerson.FirstName
				ELSE ''
			END +
			CASE WHEN facPerson.MiddleName IS NOT NULL THEN ' ' + facPerson.MiddleName
				ELSE ''
			END AS PdFullName
		, CASE WHEN fund.FundingStatusId in ( 4, 5)  THEN fund.DateStarted                   
				ELSE fund.DateProjectedStart
			END AS  DisplayStartDate                  
		, CASE WHEN fund.FundingStatusId in ( 4, 5)  THEN fund.DateEnded               
				ELSE fund.DateProjectedEnd
			END AS  DisplayEndDate

	FROM 
		dbo.TrainingGrant tgrant
		INNER JOIN dbo.Funding fund ON fund.Id = tgrant.FundingId
			AND fund.IsDeleted = 0
		INNER JOIN dbo.FundingFaculty fundFac ON fundFac.FundingId = fund.Id
			AND fundFac.IsDeleted = 0
		INNER JOIN dbo.Faculty fac ON fac.Id = fundFac.FacultyId
			AND fac.IsDeleted = 0
		INNER JOIN dbo.Person facPerson ON facPerson.PersonId = fac.PersonId
			AND facPerson.IsDeleted = 0
		LEFT OUTER JOIN dbo.TrainingGrantStatus tgStatus ON tgStatus.Id = tgrant.TrainingGrantStatusId
		LEFT OUTER JOIN dbo.FundingType fundType ON fundType.Id = fund.FundingTypeId
		LEFT OUTER JOIN dbo.Institution sponsor ON sponsor.Id = fund.SponsorId
			AND sponsor.IsDeleted = 0		
		LEFT OUTER JOIN dbo.FundingRole fundRole ON fundRole.Id = fundFac.PrimaryRoleId
		LEFT OUTER JOIN dbo.DoctoralLevel docLvl ON docLvl.Id = tgrant.DoctoralLevelId
		LEFT OUTER JOIN Search.vw_Logins updLogins ON updLogins.Username = fund.LastUpdatedBy
		
	WHERE 
		tgrant.IsDeleted = 0
		AND ((fundRole.Id = 6) OR (fundRole.Id IS NULL))



GO


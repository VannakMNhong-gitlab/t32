/****** Object:  UserDefinedFunction [dbo].[splitString]    Script Date: 5/13/2015 3:48:39 PM ******/
DROP FUNCTION [dbo].[splitString]
GO

/****** Object:  UserDefinedFunction [dbo].[splitString]    Script Date: 5/13/2015 3:48:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 11/13/2014
-- Description:	Splits a delimited string into a temporary table
-- Returns: Temporary table of split out values
-- =============================================
CREATE FUNCTION [dbo].[splitString](
    @delimited NVARCHAR(MAX),
    @delimiter NVARCHAR(100)
) RETURNS @t TABLE (id INT IDENTITY(1,1), val NVARCHAR(MAX))
AS
BEGIN
    DECLARE @xml XML
    SET @xml = N'<t>' + REPLACE(@delimited,@delimiter,'</t><t>') + '</t>'
    INSERT INTO @t(val)
    SELECT  r.value('.','varchar(MAX)') as item
    FROM  @xml.nodes('/t') as records(r)
    RETURN
END
GO


/****** Object:  View [Search].[vw_ClinicalFacultyPathways]    Script Date: 5/2/2016 3:19:06 PM ******/
DROP VIEW [Search].[vw_ClinicalFacultyPathways]
GO

/****** Object:  View [Search].[vw_ClinicalFacultyPathways]    Script Date: 5/2/2016 3:19:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [Search].[vw_ClinicalFacultyPathways] 
AS 

	SELECT
		facPath.Id AS ClinicalFacultyPathwayId
		, facPath.Name AS ClinicalFacultyPathway
		, (vw.TotalClinicalFacultyPathways) AS TotalClinicalFacultyPathwayUsage

	FROM 
		dbo.ClinicalFacultyPathway facPath 
		LEFT OUTER JOIN Search.vw_UsageOfClinicalFacultyPathways vw ON vw.ClinicalFacultyPathwayId = facPath.Id

	WHERE
		facPath.IsDeleted = 0


GO


/****** Object:  View [Search].[vw_FundingProgramProjectData]    Script Date: 5/2/2016 4:42:30 PM ******/
DROP VIEW [Search].[vw_FundingProgramProjectData]
GO

/****** Object:  View [Search].[vw_FundingProgramProjectData]    Script Date: 5/2/2016 4:42:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [Search].[vw_FundingProgramProjectData] 
AS 
	SELECT DISTINCT
		fund.Id
		, fac.Id AS PIFacultyId
		, facPerson.PersonId AS PIPersonId
		, fund.SponsorAwardNumber
		, fund.Title
		, fund.FundingStatusId
		, fundStatus.Name AS FundingStatus
		, fund.DateProjectedStart
		, fund.DateProjectedEnd
		, fund.SponsorId
		, sponsor.Name AS SponsorName
		, fund.SponsorReferenceNumber
		, fund.GRTNumber
		, fund.DateStarted AS DateTimeFundingStarted
		, fund.DateEnded AS DateTimeFundingEnded
		, fund.FundingTypeId
		, fundType.Name AS FundingType
		, fund.IsRenewal
		, fund.DateLastUpdated
		, fund.LastUpdatedBy
		, updLogins.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLogins.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLogins.MiddleName)), 1) + '. '
			ELSE '' END + updLogins.LastName AS LastUpdatedByName
		, facPerson.LastName AS PILastName
		, facPerson.FirstName AS PIFirstName
		, CASE 
				WHEN facPerson.LastName IS NULL AND facPerson.FirstName IS NULL THEN NULL
				WHEN facPerson.LastName IS NOT NULL THEN facPerson.LastName
				ELSE ''
			END +
			CASE WHEN facPerson.FirstName IS NOT NULL THEN ', ' + facPerson.FirstName
				ELSE ''
			END +
			CASE WHEN facPerson.MiddleName IS NOT NULL THEN ' ' + facPerson.MiddleName
				ELSE ''
			END AS PIFullName
		, ProgramProjectPDId
		, facProjectPerson.LastName AS ProgramProjectPDLastName
		, facProjectPerson.FirstName AS ProgramProjectPDFirstName
		, CASE 
				WHEN facProjectPerson.LastName IS NULL AND facProjectPerson.FirstName IS NULL THEN NULL
				WHEN facProjectPerson.LastName IS NOT NULL THEN facProjectPerson.LastName
				ELSE ''
			END +
			CASE WHEN facProjectPerson.FirstName IS NOT NULL THEN ', ' + facProjectPerson.FirstName
				ELSE ''
			END +
			CASE WHEN facProjectPerson.MiddleName IS NOT NULL THEN ' ' + facProjectPerson.MiddleName
				ELSE ''
			END AS ProgramProjectPDFullName

	FROM 
		dbo.Funding fund
		INNER JOIN dbo.FundingFaculty fundFac ON fundFac.FundingId = fund.Id
			AND fundFac.IsDeleted = 0
		INNER JOIN dbo.Faculty fac ON fac.Id = fundFac.FacultyId
			AND fac.IsDeleted = 0
		INNER JOIN dbo.Person facPerson ON facPerson.PersonId = fac.PersonId
			AND facPerson.IsDeleted = 0
		LEFT OUTER JOIN dbo.FundingStatus fundStatus ON fundStatus.Id = fund.FundingStatusId
		LEFT OUTER JOIN dbo.FundingType fundType ON fundType.Id = fund.FundingTypeId
		LEFT OUTER JOIN dbo.Institution sponsor ON sponsor.Id = fund.SponsorId
			AND sponsor.IsDeleted = 0
		LEFT OUTER JOIN dbo.FundingRole fundRole ON fundRole.Id = fundFac.PrimaryRoleId
		LEFT OUTER JOIN dbo.TrainingGrant tgrant ON tgrant.FundingId = fund.Id
		LEFT OUTER JOIN dbo.Faculty facProject ON facProject.Id = fund.ProgramProjectPDId
			AND facProject.IsDeleted = 0
		LEFT OUTER JOIN dbo.Person facProjectPerson ON facProjectPerson.PersonId = facProject.PersonId
			AND facProjectPerson.IsDeleted = 0
		LEFT OUTER JOIN Search.vw_Logins updLogins ON updLogins.Username = fund.LastUpdatedBy
		
	WHERE 
		fund.IsDeleted = 0
		AND ((fundRole.Id = 1) OR (fundRole.Id IS NULL))
		AND tgrant.Id IS NULL
		--AND fund.FundingTypeId = 1





GO


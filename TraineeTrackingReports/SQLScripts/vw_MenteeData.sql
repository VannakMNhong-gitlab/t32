/****** Object:  View [Search].[vw_MenteeData]    Script Date: 5/2/2016 4:44:58 PM ******/
DROP VIEW [Search].[vw_MenteeData]
GO

/****** Object:  View [Search].[vw_MenteeData]    Script Date: 5/2/2016 4:44:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_MenteeData]
AS
	SELECT 
		mentee.Id AS Id
		, mentee.PersonId
		, p.FirstName
		, p.MiddleName
		, p.LastName
		, CASE 
			WHEN p.LastName IS NULL AND p.FirstName IS NULL THEN NULL
			WHEN p.LastName IS NOT NULL THEN p.LastName
				ELSE ''
			END +
			CASE WHEN p.FirstName IS NOT NULL THEN ', ' + p.FirstName
				ELSE ''
			END +
			CASE WHEN p.MiddleName IS NOT NULL THEN ' ' + p.MiddleName
				ELSE ''
			END AS FullName
		, p.DisplayId
		, mentee.StudentId
		, mentee.IsTrainingGrantEligible
		, mentee.WasRecruitedToLab
		, p.IsFromDisadvantagedBkgd
		, p.IsIndividualWithDisabilities
		, p.IsUnderrepresentedMinority
		, dept.DisplayName AS Department
		, instAssoc.Name AS InstitutionAssociation
		, mentee.DateLastUpdated AS MenteeDateLastUpdated
		, mentee.LastUpdatedBy AS MenteeLastUpdatedBy
		, updLoginsMentee.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLoginsMentee.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLoginsMentee.MiddleName)), 1) + '. '
			ELSE '' END + updLoginsMentee.LastName AS MenteeLastUpdatedByName
		, prog.Title AS Program
		--, fac.Id AS FacultyId
		--, mentor.PersonId AS MentorPersonId
		--, mentor.LastName AS MentorLName
		--, mentor.FirstName AS MentorFName
		--, mentor.MiddleName AS MentorMName
		--, CASE 
		--		WHEN mentor.LastName IS NULL AND mentor.FirstName IS NULL THEN NULL
		--		WHEN mentor.LastName IS NOT NULL THEN mentor.LastName
		--			ELSE ''
		--	END +
		--	CASE WHEN mentor.FirstName IS NOT NULL THEN ', ' + mentor.FirstName
		--		ELSE ''
		--	END +
		--	CASE WHEN mentor.MiddleName IS NOT NULL THEN ' ' + mentor.MiddleName
		--		ELSE ''
		--	END AS MentorFullName
		--, mentorRole.[Role] AS MentorRole
		--, docLvl.LevelName AS MenteeType
		--, medRank.[Rank] AS MedicalRank
		, grades.GPA
		, grades.GPAScale
		, grades.InstitutionId AS GradingInstitutionId
		, instGrades.Name AS GradingInstitution
		, grades.TestScoreTypeId
		, tst.Name AS TestScoreType
		, grades.YearTested
		, grades.GREScoreVerbal	
		, grades.GREScoreQuantitative
		, grades.GREScoreAnalytical
		, grades.GREScoreSubject
		, grades.GREPercentileVerbal	
		, grades.GREPercentileQuantitative
		, grades.GREPercentileAnalytical
		, grades.GREPercentileSubject
		, grades.MCATScoreVerbalReasoning
		, grades.MCATScorePhysicalSciences
		, grades.MCATScoreBiologicalSciences
		, grades.MCATScoreWriting
		, grades.MCATPercentile
		, degs.Name AS Degree
		, instDegree.Id AS DegreeInstitutionId
		, instDegree.Name AS DegreeInstitution
		, stp.Abbreviation AS DegreeInstitutionStateProvince
		, c.Name AS DegreeInstitutionCountry
		, acadHist.AreaOfStudy AS DegreeAreaOfStudy
		--, acadHist.DateStarted AS DegreeDateStarted
		--, DATEPART(yy, acadHist.DateStarted) AS DegreeYearStarted
		--, acadHist.DateEnded AS DegreeDateEnded
		--, DATEPART(yy, acadHist.DateEnded) AS DegreeYearEnded
		, acadHist.YearStarted AS DegreeYearStarted
		, acadHist.YearEnded AS DegreeYearEnded
		, acadHist.YearDegreeCompleted
		, acadHist.IsUndergraduate AS DegreeIsUndergrad
		, acadHist.ResearchProjectTitle AS DegreeResearchProjTitle
		, acadHist.ResearchAdvisor AS DegreeResearchAdvisor
		, acadHist.DoctoralThesis AS DegreeDoctoralThesis
		, acadHist.IsResidency AS DegreeIsResidency
		, instResidency.Id AS ResidencyInstitutionId
		, instResidency.Name AS ResidencyInstitution
		, acadHist.ResidencySpecialization
		, acadHist.ResidencyPGY
		, acadHist.ResidencyAdvisor
		, acadHist.ResidencyYearStarted
		, acadHist.ResidencyYearEnded
		, acadHist.IsNonMedicalTransfer
		, acadHist.IsPhDTransfer
		, acadHist.DateLastUpdated AS HistoryDateLastUpdated
		, acadHist.LastUpdatedBy AS HistoryLastUpdatedBy
		, updLoginsAcadHist.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLoginsAcadHist.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLoginsAcadHist.MiddleName)), 1) + '. '
			ELSE '' END + updLoginsAcadHist.LastName AS HistoryLastUpdatedByName
		, trgDocLvl.LevelName AS DegreeTrainingDoctoralLevel
		--, lnkMentee2Prog.DateStarted
		--, lnkMentee2Prog.DateEnded
		, CASE WHEN email.IsPrimary = 1
				THEN email.EmailAddress
				ELSE ''
			END AS PrimaryEmailAddress
		, CASE WHEN email.IsPrimary = 0
			THEN email.EmailAddress
			ELSE ''
		END AS EmailAddress
		, CASE WHEN addy.IsPrimary = 1
				THEN addy.AddressLine1 + 
					CASE WHEN addy.AddressLine2 IS NOT NULL
						THEN ' ' + addy.AddressLine2
						ELSE ''
					END + 
					CASE WHEN addy.AddressLine3 IS NOT NULL
						THEN ' ' + addy.AddressLine3
						ELSE ''
					END
					+ ', ' + addy.City
					+ ', ' + addystate.Abbreviation
					+ '  ' + addy.PostalCode
				ELSE ''
			END AS MailingAddress
		, CASE WHEN addy.IsPrimary = 1 THEN addy.AddressLine1 END AS AddressLine1
		, CASE WHEN addy.IsPrimary = 1 THEN addy.AddressLine2 END AS AddressLine2
		, CASE WHEN addy.IsPrimary = 1 THEN addy.AddressLine3 END AS AddressLine3
		, CASE WHEN addy.IsPrimary = 1 THEN addy.City END AS AddressCity
		, CASE WHEN addy.IsPrimary = 1 THEN addy.StateId END AS AddressStateProvinceId
		, CASE WHEN addy.IsPrimary = 1 THEN addystate.Abbreviation END AS AddressStateProvince
		, CASE WHEN addy.IsPrimary = 1 THEN addy.PostalCode END AS AddressPostalCode
		, CASE WHEN phone.IsPrimary = 0
			THEN phone.PhoneNumber
			ELSE ''
		END AS PhoneNumber
	FROM dbo.Mentee mentee
		INNER JOIN dbo.Person p ON p.PersonId = mentee.PersonId
			AND p.IsDeleted = 0
		--LEFT OUTER JOIN dbo.DoctoralLevel docLvl ON docLvl.Id = mentee.DoctoralLevelId
		--LEFT OUTER JOIN dbo.MedicalRank medRank ON medRank.Id = mentee.MedicalRankId
		LEFT OUTER JOIN dbo.Organization dept ON dept.Id = mentee.DepartmentId
			AND dept.IsDeleted = 0
		LEFT OUTER JOIN dbo.InstitutionAssociation instAssoc ON instAssoc.Id = mentee.InstitutionAssociationId
		LEFT OUTER JOIN dbo.TrainingPeriod per ON per.PersonId = mentee.PersonId
			AND per.IsDeleted = 0
		LEFT OUTER JOIN dbo.TrainingPeriodProgram perprog ON perprog.TrainingPeriodId = per.Id
		LEFT OUTER JOIN dbo.Program prog ON prog.Id = perprog.ProgramId
		--LEFT OUTER JOIN dbo.Trainee_TrainingProgram lnkMentee2Prog ON lnkMentee2Prog.PersonId = mentee.PersonId
		--	AND lnkMentee2Prog.IsDeleted = 0
		--	AND lnkMentee2Prog.DateEnded IS NULL OR DATEDIFF(day, lnkMentee2Prog.DateEnded, GETDATE()) > 0
		--LEFT OUTER JOIN dbo.TrainingProgram prog ON prog.Id = lnkMentee2Prog.TrainingProgramId
		--	AND prog.IsDeleted = 0
		--LEFT OUTER JOIN dbo.Faculty_Mentee lnkFac2Mentee ON lnkFac2Mentee.MenteeId = mentee.Id
		--	AND lnkFac2Mentee.IsDeleted = 0
		--	AND lnkFac2Mentee.DateEnded IS NULL OR DATEDIFF(day, lnkFac2Mentee.DateEnded, GETDATE()) > 0
		--LEFT OUTER JOIN dbo.Faculty fac ON fac.Id = lnkFac2Mentee.FacultyId
		--	AND fac.IsDeleted = 0
		--LEFT OUTER JOIN dbo.Person mentor ON mentor.PersonId = fac.PersonId
		--	AND mentor.IsDeleted = 0
		--LEFT OUTER JOIN dbo.FacultyRole mentorRole ON mentorRole.Id = lnkFac2Mentee.FacultyRoleId
		LEFT OUTER JOIN dbo.GradeRecord grades ON grades.PersonId = mentee.PersonId
			AND grades.IsDeleted = 0
		LEFT OUTER JOIN dbo.Institution instGrades ON instGrades.Id = grades.InstitutionId
			AND instGrades.IsDeleted = 0
		LEFT OUTER JOIN dbo.TestScoreType tst ON tst.Id = grades.TestScoreTypeId
		LEFT OUTER JOIN dbo.AcademicHistory acadHist ON acadHist.PersonId = mentee.PersonId
			AND acadHist.IsDeleted = 0
		LEFT OUTER JOIN dbo.AcademicDegree degs ON degs.Id = acadHist.AcademicDegreeId
			AND degs.IsDeleted = 0
		LEFT OUTER JOIN dbo.DoctoralLevel trgDocLvl ON trgDocLvl.Id = acadHist.TrainingDoctoralLevelId
		LEFT OUTER JOIN dbo.Institution instDegree ON instDegree.Id = acadHist.InstitutionId
			AND instDegree.IsDeleted = 0
		LEFT OUTER JOIN dbo.StateProvince stp ON stp.Id = instDegree.StateId
			AND stp.IsDeleted = 0
		LEFT OUTER JOIN dbo.Country c ON c.Id = instDegree.CountryId
			AND c.IsDeleted = 0
		LEFT OUTER JOIN dbo.ContactEmail email ON email.PersonId = p.PersonId
			AND email.IsDeleted = 0
		LEFT OUTER JOIN dbo.ContactAddress addy ON addy.PersonId = p.PersonId
			AND addy.IsDeleted = 0
		LEFT OUTER JOIN dbo.StateProvince addystate ON addystate.Id = addy.StateId
			AND addystate.IsDeleted = 0
		LEFT OUTER JOIN dbo.ContactPhone phone ON phone.PersonId = p.PersonId
			AND phone.IsDeleted = 0
		LEFT OUTER JOIN dbo.Institution instResidency ON instResidency.Id = acadHist.ResidencyInstitutionId
			AND instResidency.IsDeleted = 0
		LEFT OUTER JOIN Search.vw_Logins updLoginsMentee ON updLoginsMentee.Username = mentee.LastUpdatedBy
		LEFT OUTER JOIN Search.vw_Logins updLoginsAcadHist ON updLoginsAcadHist.Username = acadHist.LastUpdatedBy
	
	WHERE
		mentee.IsDeleted = 0



GO


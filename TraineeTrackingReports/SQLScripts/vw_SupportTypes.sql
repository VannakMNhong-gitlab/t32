/****** Object:  View [Search].[vw_SupportTypes]    Script Date: 12/1/2015 4:12:19 PM ******/
DROP VIEW [Search].[vw_SupportTypes]
GO

/****** Object:  View [Search].[vw_SupportTypes]    Script Date: 12/1/2015 4:12:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [Search].[vw_SupportTypes] 
AS 

	SELECT
		sptType.Id
		, sptType.Name
		, sptType.Abbreviation
		, sptType.Name + ' (' + sptType.Abbreviation + ')' AS FullName
		--, (vw.TotalOrganization) AS TotalOrganizationTypeUsage

	FROM 
		dbo.SupportType sptType 
		--LEFT OUTER JOIN Search.vw_UsageOfSupportTypes vw ON vw.OrganizationTypeId = orgType.Id


GO


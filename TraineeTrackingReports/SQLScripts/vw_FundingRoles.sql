/****** Object:  View [Search].[vw_FundingRoles]    Script Date: 1/29/2016 3:01:29 PM ******/
DROP VIEW [Search].[vw_FundingRoles]
GO

/****** Object:  View [Search].[vw_FundingRoles]    Script Date: 1/29/2016 3:01:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_FundingRoles] 
AS 

	SELECT
		fundrole.Id AS FundingRoleId
		, fundrole.Name
		, fundrole.IsTrainingGrantRole
		, vw.TotalFundingFaculty AS TotalFundingRoleUsage
		, vw.TotalTrainingGrantFaculty AS TotalTrainingGrantRoleUsage

	FROM 
		dbo.FundingRole fundrole 
		LEFT OUTER JOIN Search.vw_UsageOfFundingRoles vw ON vw.FundingRoleId = fundrole.Id

	WHERE
		fundrole.IsDeleted = 0


GO


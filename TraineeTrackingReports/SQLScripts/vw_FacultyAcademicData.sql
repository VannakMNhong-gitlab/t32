/****** Object:  View [Search].[vw_FacultyAcademicData]    Script Date: 5/2/2016 4:35:56 PM ******/
DROP VIEW [Search].[vw_FacultyAcademicData]
GO

/****** Object:  View [Search].[vw_FacultyAcademicData]    Script Date: 5/2/2016 4:35:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_FacultyAcademicData]
AS
	SELECT 
		fac.Id AS FacultyId
		, fac.PersonId
		, p.FirstName
		, p.MiddleName
		, p.LastName
		, CASE 
			WHEN p.LastName IS NULL AND p.FirstName IS NULL THEN NULL
			WHEN p.LastName IS NOT NULL THEN p.LastName
				ELSE ''
			END +
			CASE WHEN p.FirstName IS NOT NULL THEN ', ' + p.FirstName
				ELSE ''
			END +
			CASE WHEN p.MiddleName IS NOT NULL THEN ' ' + p.MiddleName
				ELSE ''
			END AS FullName
		, p.DisplayId
		, fac.EmployeeId
		, fac.DateLastUpdated AS FacultyDateLastUpdated
		, fac.LastUpdatedBy AS FacultyLastUpdatedBy
		, updLoginsFac.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLoginsFac.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLoginsFac.MiddleName)), 1) + '. '
			ELSE '' END + updLoginsFac.LastName AS FacultyLastUpdatedByName
		, degs.Name AS Degree
		, fad.AreaOfStudy
		, fad.DegreeYear
		, inst.Id AS DegreeInstitutionId
		, inst.Name AS DegreeInstitution
		, inst.City AS DegreeInstitutionCity
		, stp.Abbreviation AS DegreeInstitutionState
		, c.Name AS DegreeInstitutionCountry
		, fac.IsActive
		, fad.DateLastUpdated AS AcademicDataDateLastUpdated
		, fad.LastUpdatedBy AS AcademicDataLastUpdatedBy
		, updLoginsFAD.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLoginsFAD.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLoginsFAD.MiddleName)), 1) + '. '
			ELSE '' END + updLoginsFAD.LastName AS AcademicDataLastUpdatedByName

	FROM dbo.Faculty fac
		INNER JOIN dbo.Person p ON p.PersonId = fac.PersonId
			AND p.IsDeleted = 0
		LEFT OUTER JOIN dbo.FacultyAcademicData fad ON fad.PersonId = fac.PersonId
			AND fad.IsDeleted = 0
		LEFT OUTER JOIN dbo.AcademicDegree degs ON degs.Id = fad.AcademicDegreeId
			AND degs.IsDeleted = 0
		LEFT OUTER JOIN dbo.Institution inst ON inst.Id = fad.InstitutionId
			AND inst.IsDeleted = 0
		LEFT OUTER JOIN dbo.StateProvince stp ON stp.Id = inst.StateId
			AND stp.IsDeleted = 0
		LEFT OUTER JOIN dbo.Country c ON c.Id = inst.CountryId
			AND c.IsDeleted = 0
		LEFT OUTER JOIN Search.vw_Logins updLoginsFac ON updLoginsFac.Username = fac.LastUpdatedBy
		LEFT OUTER JOIN Search.vw_Logins updLoginsFAD ON updLoginsFAD.Username = fad.LastUpdatedBy
	WHERE
		fac.IsDeleted = 0



GO


/****** Object:  View [Search].[vw_PersonRoles]    Script Date: 5/2/2016 4:49:34 PM ******/
DROP VIEW [Search].[vw_PersonRoles]
GO

/****** Object:  View [Search].[vw_PersonRoles]    Script Date: 5/2/2016 4:49:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [Search].[vw_PersonRoles]
AS
	SELECT 
		ppl.PersonId
		, ppl.FirstName
		, ppl.LastName
		, ppl.MiddleName
		, CASE 
			WHEN ppl.LastName IS NULL AND ppl.FirstName IS NULL THEN NULL
			WHEN ppl.LastName IS NOT NULL THEN ppl.LastName
				ELSE ''
			END +
			CASE WHEN ppl.FirstName IS NOT NULL THEN ', ' + ppl.FirstName
				ELSE ''
			END +
			CASE WHEN ppl.MiddleName IS NOT NULL THEN ' ' + ppl.MiddleName
				ELSE ''
			END AS FullName
		, logins.LoginId
		, logins.ExternalId
		, logins.Username
		, roles.Id AS RoleId
		, roles.RoleName
		, roles.IsActive
		, roles.[Description]
		, proles.DateLastUpdated
		, proles.LastUpdatedBy
		, updLogins.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLogins.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLogins.MiddleName)), 1) + '. '
			ELSE '' END + updLogins.LastName AS LastUpdatedByName
		
	FROM 
		dbo.Person ppl
		INNER JOIN dbo.Logins logins ON logins.PersonId = ppl.PersonId
		LEFT OUTER JOIN dbo.PersonRoles proles ON proles.LoginId = logins.LoginId
			AND proles.IsDeleted = 0
		LEFT OUTER JOIN dbo.Roles roles ON roles.Id = proles.RoleId
			AND roles.IsDeleted = 0
		LEFT OUTER JOIN Search.vw_Logins updLogins ON updLogins.Username = proles.LastUpdatedBy

	WHERE
		ppl.IsDeleted = 0






GO


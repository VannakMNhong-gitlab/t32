/****** Object:  View [Search].[vw_UsageOfCountries]    Script Date: 6/2/2015 10:26:43 AM ******/
DROP VIEW [Search].[vw_UsageOfCountries]
GO

/****** Object:  View [Search].[vw_UsageOfCountries]    Script Date: 6/2/2015 10:26:43 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [Search].[vw_UsageOfCountries] 
AS 
	SELECT 
		c.Id AS CountryId
		, c.Name AS CountryName
		, (SELECT Count(CountryId) FROM dbo.ContactAddress addy 
			WHERE addy.CountryId =  c.Id
				AND addy.IsDeleted = 0) AS TotalContactAddress
		, (SELECT Count(CountryId) FROM dbo.Institution inst 
			WHERE inst.CountryId = c.Id
				AND inst.IsDeleted = 0) AS TotalInstitutions
		, (SELECT Count(CountryId) FROM dbo.StateProvince stp 
			WHERE stp.CountryId =  c.Id
				AND stp.IsDeleted = 0) AS TotalStateProvince

	FROM 
		dbo.Country c

	WHERE 
		c.IsDeleted = 0

	GROUP BY
		c.Id
		, c.Name



GO


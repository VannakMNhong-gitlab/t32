/****** Object:  View [Search].[vw_UsageOfOrganizationTypes]    Script Date: 1/29/2016 2:57:40 PM ******/
DROP VIEW [Search].[vw_UsageOfOrganizationTypes]
GO

/****** Object:  View [Search].[vw_UsageOfOrganizationTypes]    Script Date: 1/29/2016 2:57:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [Search].[vw_UsageOfOrganizationTypes] 
AS 
	SELECT 
		orgType.Id AS OrganizationTypeId
		, orgType.OrganizationType
		, (SELECT Count(OrganizationTypeId) FROM dbo.Organization org 
			WHERE org.OrganizationTypeId =  orgType.Id
				AND org.IsDeleted = 0) AS TotalOrganization

	FROM 
		dbo.OrganizationType orgType

	WHERE
		orgType.IsDeleted = 0

	GROUP BY
		orgType.Id
		, orgType.OrganizationType


GO


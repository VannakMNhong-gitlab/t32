/****** Object:  View [Search].[vw_Reports]    Script Date: 1/29/2016 3:07:42 PM ******/
DROP VIEW [Search].[vw_Reports]
GO

/****** Object:  View [Search].[vw_Reports]    Script Date: 1/29/2016 3:07:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [Search].[vw_Reports] 
AS 

	SELECT
		rpt.Id
		, rpt.Name
		, rpt.Title
		, rpt.ModifiedDate
		, rpt.[Type_Id] AS ReportTypeId
		, rptType.Name AS ReportType
		, rpt.ReportPath
		, rpt.[Description]
		, rpt.StatusTypeId
		, statType.Name AS StatusType
		, rpt.IsPredoc
		, rpt.IsPostdoc
		, rpt.IsNewOnly
		, rpt.IsRenewalOnly

	FROM 
		dbo.Reports rpt
		INNER JOIN dbo.ReportTypes rptType ON rptType.Id = rpt.[Type_Id]
		INNER JOIN dbo.StatusTypes statType ON statType.Id = rpt.StatusTypeId

	WHERE 
		rpt.IsDeleted = 0


GO


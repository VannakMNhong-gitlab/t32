/****** Object:  View [Search].[vw_UsageOfStateProvinces]    Script Date: 6/2/2015 12:26:23 PM ******/
DROP VIEW [Search].[vw_UsageOfStateProvinces]
GO

/****** Object:  View [Search].[vw_UsageOfStateProvinces]    Script Date: 6/2/2015 12:26:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_UsageOfStateProvinces] 
AS 
	SELECT 
		stp.Id AS StateProvinceId
		, stp.Abbreviation AS Abbreviation
		, stp.FullName AS FullName
		, (SELECT Count(StateId) FROM dbo.ContactAddress addy 
			WHERE addy.StateId =  stp.Id
				AND addy.IsDeleted = 0) AS TotalContactAddress
		, (SELECT Count(StateId) FROM dbo.Institution inst 
			WHERE inst.StateId = stp.Id
				AND inst.IsDeleted = 0) AS TotalInstitutions

	FROM 
		dbo.StateProvince stp

	WHERE 
		stp.IsDeleted = 0

	GROUP BY
		stp.Id
		, stp.Abbreviation
		, stp.FullName




GO


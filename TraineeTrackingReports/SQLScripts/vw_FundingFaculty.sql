/****** Object:  View [Search].[vw_FundingFaculty]    Script Date: 5/2/2016 4:42:03 PM ******/
DROP VIEW [Search].[vw_FundingFaculty]
GO

/****** Object:  View [Search].[vw_FundingFaculty]    Script Date: 5/2/2016 4:42:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [Search].[vw_FundingFaculty] 
AS 
	SELECT DISTINCT
		fund.Id
		, dbo.StripHTML_Loop(Title) AS Title
		, fund.FundingStatusId
		, fundStatus.Name AS FundingStatus
		, fund.DateProjectedStart
		, fund.DateProjectedEnd
		, fund.DateStarted AS DateTimeFundingStarted
		, fund.DateEnded AS DateTimeFundingEnded
		, fund.FundingTypeId
		, fundType.Name AS FundingType
		, fund.DateLastUpdated
		, fund.LastUpdatedBy
		, fac.Id AS FacultyId
		, facPerson.PersonId AS FacultyPersonId
		, facPerson.LastName AS FacultyLastName
		, facPerson.FirstName AS FacultyFirstName
		, CASE 
				WHEN facPerson.LastName IS NULL AND facPerson.FirstName IS NULL THEN NULL
				WHEN facPerson.LastName IS NOT NULL THEN facPerson.LastName
				ELSE ''
			END +
			CASE WHEN facPerson.FirstName IS NOT NULL THEN ', ' + facPerson.FirstName
				ELSE ''
			END +
			CASE WHEN facPerson.MiddleName IS NOT NULL THEN ' ' + facPerson.MiddleName
				ELSE ''
			END AS FacultyFullName
		, fundRole.Id AS FacultyRoleId
		, fundRole.Name AS FacultyRole
		, fund.SponsorId
		, inst.Name AS SponsorName
		, fund.SponsorAwardNumber
		, fundFac.DateLastUpdated AS FundingFacultyDateLastUpdated
		, fundFac.LastUpdatedBy AS FundingFacultyLastUpdatedBy
		, updLogins.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLogins.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLogins.MiddleName)), 1) + '. '
			ELSE '' END + updLogins.LastName AS FundingFacultyLastUpdatedByName

	FROM 
		dbo.Funding fund
		LEFT OUTER JOIN dbo.TrainingGrant tgrant ON tgrant.FundingId = fund.Id
			AND tgrant.IsDeleted = 0
		LEFT OUTER JOIN dbo.FundingStatus fundStatus ON fundStatus.Id = fund.FundingStatusId
		LEFT OUTER JOIN dbo.FundingType fundType ON fundType.Id = fund.FundingTypeId
		INNER JOIN dbo.FundingFaculty fundFac ON fundFac.FundingId = fund.Id
			AND fundFac.IsDeleted = 0
		INNER JOIN dbo.Faculty fac ON fac.Id = fundFac.FacultyId
			AND fac.IsDeleted = 0
		INNER JOIN dbo.Person facPerson ON facPerson.PersonId = fac.PersonId
			AND facPerson.IsDeleted = 0
		INNER JOIN dbo.FundingRole fundRole ON fundRole.Id = fundFac.PrimaryRoleId
		LEFT OUTER JOIN dbo.Institution inst ON inst.Id = fund.SponsorId
			AND inst.IsDeleted = 0
		LEFT OUTER JOIN Search.vw_Logins updLogins ON updLogins.Username = fundFac.LastUpdatedBy
		
	WHERE 
		fund.IsDeleted = 0
		AND tgrant.Id IS NULL


GO


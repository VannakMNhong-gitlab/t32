/****** Object:  View [Search].[vw_ApplicantAcademicHistory]    Script Date: 5/2/2016 4:33:39 PM ******/
DROP VIEW [Search].[vw_ApplicantAcademicHistory]
GO

/****** Object:  View [Search].[vw_ApplicantAcademicHistory]    Script Date: 5/2/2016 4:33:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [Search].[vw_ApplicantAcademicHistory]
AS
	SELECT 
		app.Id AS Id
		, app.PersonId AS ApplicantPersonId
		, p.FirstName
		, p.MiddleName
		, p.LastName
		, CASE 
			WHEN p.LastName IS NULL AND p.FirstName IS NULL THEN NULL
			WHEN p.LastName IS NOT NULL THEN p.LastName
				ELSE ''
			END +
			CASE WHEN p.FirstName IS NOT NULL THEN ', ' + p.FirstName
				ELSE ''
			END +
			CASE WHEN p.MiddleName IS NOT NULL THEN ' ' + p.MiddleName
				ELSE ''
			END AS FullName
		, app.ApplicantId
		, app.IsTrainingGrantEligible
		, docLvl.Id AS ApplicantTypeId
		, docLvl.LevelName AS ApplicantType
		, app.DateLastUpdated AS ApplicantDateLastUpdated
		, app.LastUpdatedBy AS ApplicantLastUpdatedBy
		, updLoginsApp.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLoginsApp.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLoginsApp.MiddleName)), 1) + '. '
			ELSE '' END + updLoginsApp.LastName AS ApplicantLastUpdatedByName
		, grades.GPA
		, grades.GPAScale
		, grades.InstitutionId AS GradingInstitutionId
		, instGrades.Name AS GradingInstitution
		, grades.TestScoreTypeId
		, tst.Name AS TestScoreType
		, grades.YearTested
		, grades.GREScoreVerbal	
		, grades.GREScoreQuantitative
		, grades.GREScoreAnalytical
		, grades.GREScoreSubject
		, grades.GREPercentileVerbal	
		, grades.GREPercentileQuantitative
		, grades.GREPercentileAnalytical
		, grades.GREPercentileSubject
		, grades.MCATScoreVerbalReasoning
		, grades.MCATScorePhysicalSciences
		, grades.MCATScoreBiologicalSciences
		, grades.MCATScoreWriting
		, grades.MCATPercentile
		, timeline.WasInterviewed
		, timeline.WasAccepted
		, timeline.AcceptedOffer
		, timeline.WasEnrolled
		, timeline.WasOfferedPosition
		, timeline.EnteredProgram
		, degs.Name AS Degree
		, instDegree.Id AS DegreeInstitutionId
		, instDegree.Name AS DegreeInstitution
		, stp.Abbreviation AS DegreeInstitutionState
		, c.Name AS DegreeInstitutionCountry
		, acadHist.AreaOfStudy AS DegreeAreaOfStudy
		, acadHist.YearStarted AS DegreeYearStarted
		, acadHist.YearEnded AS DegreeYearEnded
		, acadHist.YearDegreeCompleted
		, acadHist.IsUndergraduate AS DegreeIsUndergrad
		, acadHist.ResearchProjectTitle AS DegreeResearchProjTitle
		, acadHist.ResearchAdvisor AS DegreeResearchAdvisor
		, acadHist.DoctoralThesis AS DegreeDoctoralThesis
		, acadHist.IsResidency AS DegreeIsResidency
		, instResidency.Id AS ResidencyInstitutionId
		, instResidency.Name AS ResidencyInstitution
		, acadHist.ResidencySpecialization
		, acadHist.ResidencyPGY
		, acadHist.ResidencyAdvisor
		, acadHist.ResidencyYearStarted
		, acadHist.ResidencyYearEnded
		, acadHist.IsNonMedicalTransfer
		, acadHist.IsPhDTransfer
		, acadHist.DateLastUpdated AS HistoryDateLastUpdated
		, acadHist.LastUpdatedBy AS HistoryLastUpdatedBy
		, updLoginsAcadHist.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLoginsAcadHist.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLoginsAcadHist.MiddleName)), 1) + '. '
			ELSE '' END + updLoginsAcadHist.LastName AS HistoryLastUpdatedByName
		, trgDocLvl.LevelName AS DegreeTrainingDoctoralLevel
	FROM dbo.Applicant app
		INNER JOIN dbo.Person p ON p.PersonId = app.PersonId
			AND p.IsDeleted = 0
		LEFT OUTER JOIN dbo.DoctoralLevel docLvl ON docLvl.Id = app.DoctoralLevelId
		LEFT OUTER JOIN dbo.OSUTimeline timeline ON timeline.PersonId = app.PersonId
			AND timeline.IsDeleted = 0
		LEFT OUTER JOIN dbo.Institution instAlt ON instAlt.Id = timeline.Declined_AltInstitutionId
			AND instAlt.IsDeleted = 0
		LEFT OUTER JOIN dbo.GradeRecord grades ON grades.PersonId = app.PersonId
			AND grades.IsDeleted = 0
		LEFT OUTER JOIN dbo.Institution instGrades ON instGrades.Id = grades.InstitutionId
			AND instGrades.IsDeleted = 0
		LEFT OUTER JOIN dbo.TestScoreType tst ON tst.Id = grades.TestScoreTypeId
		LEFT OUTER JOIN dbo.AcademicHistory acadHist ON acadHist.PersonId = app.PersonId
			AND acadHist.IsDeleted = 0
		LEFT OUTER JOIN dbo.AcademicDegree degs ON degs.Id = acadHist.AcademicDegreeId
			AND degs.IsDeleted = 0
		LEFT OUTER JOIN dbo.DoctoralLevel trgDocLvl ON trgDocLvl.Id = acadHist.TrainingDoctoralLevelId
		LEFT OUTER JOIN dbo.Institution instDegree ON instDegree.Id = acadHist.InstitutionId
			AND instDegree.IsDeleted = 0
		LEFT OUTER JOIN dbo.StateProvince stp ON stp.Id = instDegree.StateId
			AND stp.IsDeleted = 0
		LEFT OUTER JOIN dbo.Country c ON c.Id = instDegree.CountryId
			AND c.IsDeleted = 0
		LEFT OUTER JOIN dbo.Institution instResidency ON instResidency.Id = acadHist.ResidencyInstitutionId
			AND instResidency.IsDeleted = 0
		LEFT OUTER JOIN Search.vw_Logins updLoginsApp ON updLoginsApp.Username = app.LastUpdatedBy
		LEFT OUTER JOIN Search.vw_Logins updLoginsAcadHist ON updLoginsAcadHist.Username = acadHist.LastUpdatedBy



GO


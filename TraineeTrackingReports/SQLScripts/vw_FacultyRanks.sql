/****** Object:  View [Search].[vw_FacultyRanks]    Script Date: 1/29/2016 3:01:39 PM ******/
DROP VIEW [Search].[vw_FacultyRanks]
GO

/****** Object:  View [Search].[vw_FacultyRanks]    Script Date: 1/29/2016 3:01:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_FacultyRanks] 
AS 

	SELECT
		facrank.Id AS FacultyRankId
		, facrank.[Rank]
		, (vw.TotalFaculty) AS TotalFacultyRankUsage

	FROM 
		dbo.FacultyRank facrank 
		LEFT OUTER JOIN Search.vw_UsageOfFacultyRanks vw ON vw.FacultyRankId = facrank.Id

	WHERE
		facrank.IsDeleted = 0



GO


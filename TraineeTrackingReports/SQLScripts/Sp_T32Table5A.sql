/****** Object:  StoredProcedure [Reports].[Sp_T32Table5A]    Script Date: 1/26/2016 3:46:59 PM ******/
DROP PROCEDURE [Reports].[Sp_T32Table5A]
GO

/****** Object:  StoredProcedure [Reports].[Sp_T32Table5A]    Script Date: 1/26/2016 3:46:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 05/07/2015
-- Description:	Used by T32 TABLE 5A Report
--				Predoc Mentees of Participating Faculty
-- =============================================
CREATE PROCEDURE [Reports].[Sp_T32Table5A]
	@TrainingGrantId uniqueidentifier,
	@CurrentYear INT
AS
BEGIN

	SET NOCOUNT ON;

	SET @CurrentYear = ISNULL(@CurrentYear, YEAR(GETDATE()))

	SELECT DISTINCT
		vw.FacultyId
		, vw.FacultyPersonId
		, vw.FacultyLastName
		, vw.FacultyFirstName
		, vw.FacultyFullName
		, vw.FacultyNameAbbrev
		, vw.TrainingGrantId
		, vw.Title
		, vw.IsRenewal
		, vw.MenteeId
		, vw.StudentId
		, vw.MenteePersonId
		, vw.MenteeLastName
		, vw.MenteeFirstName
		, vw.MenteeFullName
		, vw.MenteeNameAbbrev
		, vw.MenteeTypeId
		, vw.MenteeType
		, vw.IsTrainingGrantEligible
		, CASE WHEN vw.TrainingPeriodYearEnded IS NULL OR vw.TrainingPeriodYearEnded >= @CurrentYear THEN 'Current'
			ELSE 'Past'
		END AS PastCurrentTrainee
		, vw.TrainingPeriodYearStarted
		, RIGHT(vw.TrainingPeriodYearStarted, 2) AS TrainingPeriodYearStartedShort
		, vw.TrainingPeriodYearEnded
		, RIGHT(vw.TrainingPeriodYearEnded, 2) AS TrainingPeriodYearEndedShort
		, vw.TrainingPeriodInstitutionId
		, vw.TrainingPeriodInstitution
		, vw.TrainingPeriodDegreeId
		, vw.TrainingPeriodDegree
		, vw.ResearchProjectTitle
		, vw.SupportSourceText
		, vw.PositionTitle
		, vw.DateStartedPosition
		, vw.DateEndedPosition
		, prevInst.Name AS PreviousDegreeInstitution
		, prevAH.YearDegreeCompleted AS PreviousDegreeYear
		, RIGHT(prevAH.YearDegreeCompleted, 2) AS PreviousDegreeYearShort
		, prevDeg.Name AS PreviousDegree
		--, CASE WHEN prevAH.YearDegreeCompleted IS NULL THEN '--' 
		--	ELSE RIGHT(prevAH.YearDegreeCompleted, 2) END AS PreviousDegreeYearShort
		--, CASE WHEN prevDeg.Name IS NULL THEN '--' 
		--	ELSE prevDeg.Name END AS PreviousDegree
		, CASE WHEN trainee.Id IS NOT NULL THEN 1
			ELSE 0 END AS IsTraineeOnT32
		, CASE WHEN vw.MenteePersonId IS NOT NULL 
			THEN DENSE_RANK() OVER(PARTITION BY vw.FacultyId, vw.TrainingGrantId, vw.MenteePersonId, vw.TrainingPeriodInstitutionId, vw.TrainingPeriodDegreeId ORDER BY prevAH.YearDegreeCompleted ASC) 
			ELSE 1 END AS DegreeNumber
		
	FROM 
		Reports.vw_T32Table5A vw
			LEFT OUTER JOIN dbo.AcademicHistory prevAH ON prevAH.PersonId = vw.MenteePersonId
				AND prevAH.YearDegreeCompleted <= vw.TrainingPeriodYearStarted
				AND prevAH.IsDeleted = 0
			LEFT OUTER JOIN dbo.Institution prevInst ON prevInst.Id = prevAH.InstitutionId
				AND prevInst.IsDeleted = 0
			LEFT OUTER JOIN dbo.AcademicDegree prevDeg ON prevDeg.Id = prevAH.AcademicDegreeId
				AND prevDeg.IsDeleted = 0
			LEFT OUTER JOIN dbo.TrainingGrantTrainee trainee ON trainee.MenteeId = vw.MenteeId
				AND trainee.TrainingGrantId = @TrainingGrantId
				AND trainee.IsDeleted = 0

			--LEFT OUTER JOIN dbo.TrainingPeriod prevTP ON prevTP.PersonId = vw.MenteePersonId
			--	AND prevTP.YearEnded <= vw.TrainingPeriodYearStarted
			--	AND prevTP.IsDeleted = 0
			--LEFT OUTER JOIN dbo.Institution prevInst ON prevInst.Id = prevTP.InstitutionId
			--	AND prevInst.IsDeleted = 0
			--LEFT OUTER JOIN dbo.AcademicDegree prevDeg ON prevDeg.Id = prevTP.AcademicDegreeId
			--	AND prevDeg.IsDeleted = 0
			--LEFT OUTER JOIN dbo.TrainingGrantTrainee trainee ON trainee.MenteeId = vw.MenteeId
			--	AND trainee.TrainingGrantId = @T32Id
			--	AND trainee.IsDeleted = 0

	WHERE 
		((@TrainingGrantId IS NULL) OR (vw.TrainingGrantId = @TrainingGrantId))
		AND ((vw.TrainingPeriodYearEnded IS NULL)
			OR (vw.TrainingPeriodYearStarted BETWEEN (@CurrentYear - 10) AND @CurrentYear)
			OR (vw.TrainingPeriodYearEnded BETWEEN (@CurrentYear - 10) AND @CurrentYear)
		)

		--AND @TrainingGrantId = 'D159F84F-5D07-4462-BBC1-F9A8A3714E45'

	SET NOCOUNT OFF
END

GO


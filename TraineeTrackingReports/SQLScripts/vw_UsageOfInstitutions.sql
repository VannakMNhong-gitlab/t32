/****** Object:  View [Search].[vw_UsageOfInstitutions]    Script Date: 6/2/2015 10:26:50 AM ******/
DROP VIEW [Search].[vw_UsageOfInstitutions]
GO

/****** Object:  View [Search].[vw_UsageOfInstitutions]    Script Date: 6/2/2015 10:26:50 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [Search].[vw_UsageOfInstitutions] 
AS 
	SELECT 
		inst.Id AS InstitutionId
		, inst.Name AS InstitutionName
		, (SELECT Count(InstitutionId) + Count(ResidencyInstitutionId) FROM dbo.AcademicHistory achist 
			WHERE ((achist.InstitutionId = inst.Id)
					OR (achist.ResidencyInstitutionId = inst.Id))
				AND achist.IsDeleted = 0) AS TotalAcademicHistory
		, (SELECT Count(InstitutionId) FROM dbo.FacultyAcademicData facac 
			WHERE facac.InstitutionId = inst.Id
				AND facac.IsDeleted = 0) AS TotalFacultyAcademics
		, (SELECT Count(SponsorId) FROM dbo.Funding fund 
			WHERE fund.SponsorId = inst.Id
				AND fund.IsDeleted = 0) AS TotalFunding
		, (SELECT Count(InstitutionId) FROM dbo.GradeRecord graderec 
			WHERE graderec.InstitutionId = inst.Id
				AND graderec.IsDeleted = 0) AS TotalGradeRecord
		, (SELECT Count(InstitutionId) FROM dbo.MenteeSupport msupp 
			WHERE msupp.InstitutionId = inst.Id
				AND msupp.IsDeleted = 0) AS TotalMenteeSupport
		, (SELECT Count(InstitutionId) FROM dbo.TrainingPeriod tp 
			WHERE tp.InstitutionId = inst.Id
				AND tp.IsDeleted = 0) AS TotalTrainingPeriods
		, (SELECT Count(InstitutionId) FROM dbo.WorkHistory workhist 
			WHERE workhist.InstitutionId = inst.Id
				AND workhist.IsDeleted = 0) AS TotalWorkHistory

	FROM 
		dbo.Institution inst

	WHERE 
		inst.IsDeleted = 0

	GROUP BY
		inst.Id
		, inst.Name


GO


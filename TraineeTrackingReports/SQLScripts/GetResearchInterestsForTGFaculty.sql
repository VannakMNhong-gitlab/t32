/****** Object:  UserDefinedFunction [dbo].[GetResearchInterestsForTGFaculty]    Script Date: 5/13/2015 3:49:26 PM ******/
DROP FUNCTION [dbo].[GetResearchInterestsForTGFaculty]
GO

/****** Object:  UserDefinedFunction [dbo].[GetResearchInterestsForTGFaculty]    Script Date: 5/13/2015 3:49:26 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE FUNCTION [dbo].[GetResearchInterestsForTGFaculty]
(@grantId UNIQUEIDENTIFIER, @facultyId UNIQUEIDENTIFIER)
RETURNS VARCHAR(max) 
AS
Begin
	DECLARE @researchInterests VARCHAR(MAX)		
	
	SELECT @researchInterests = COALESCE(@researchInterests + '; ' ,'') + researchInterests.Name
	FROM 
		(
		SELECT DISTINCT resInt.ResearchInterest AS Name
		FROM dbo.TrainingGrantFacultyResearchInterest tgResInt
			JOIN dbo.TrainingGrant tgrant ON tgrant.Id = tgResInt.TrainingGrantId
				AND tgrant.IsDeleted = 0
			JOIN dbo.FacultyResearchInterest resInt ON resInt.Id = tgResInt.FacultyResearchInterestId
			JOIN dbo.Faculty fac ON fac.PersonId = resInt.PersonId
				AND fac.IsDeleted = 0				
		WHERE tgrant.Id = @grantId
			AND fac.Id = @facultyId
		) AS researchInterests

	RETURN  @researchInterests
end 


GO


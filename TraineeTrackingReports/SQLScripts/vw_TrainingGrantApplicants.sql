/****** Object:  View [Search].[vw_TrainingGrantApplicants]    Script Date: 5/13/2015 3:45:00 PM ******/
DROP VIEW [Search].[vw_TrainingGrantApplicants]
GO

/****** Object:  View [Search].[vw_TrainingGrantApplicants]    Script Date: 5/13/2015 3:45:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_TrainingGrantApplicants] 
AS 

	SELECT DISTINCT TOP 100 PERCENT
		tgrant.Id AS TrainingGrantId
		, fund.Id AS FundingId
		, dbo.StripHTML_Loop(fund.Title) AS Title
		, app.Id AS ApplicantGUID
		, app.PersonId AS ApplicantPersonId
		, app.ApplicantId
		, app.DoctoralLevelId AS ApplicantTypeId
		, docLvl.LevelName AS ApplicantType
		, app.DepartmentId AS ApplicantDeptId
		, appOrg.DisplayName AS ApplicantDept
		, timeline.YearEntered
		, dbo.GetProgramsForApplicant(app.PersonId) AS ApplicantPrograms
		, tgrant.ApplicantPoolAcademicYear
		--, prog.Title AS TGProg
		--, tgOrg.DisplayName AS TGOrg

	FROM 
		dbo.Funding fund
		INNER JOIN dbo.TrainingGrant tgrant ON tgrant.FundingId = fund.Id
			AND tgrant.IsDeleted = 0
		LEFT OUTER JOIN dbo.TrainingGrantDepartment tgDept ON tgDept.TrainingGrantId = tgrant.Id
		LEFT OUTER JOIN dbo.Organization tgOrg ON tgOrg.Id = tgDept.OrganizationId
			AND tgOrg.IsDeleted = 0
		LEFT OUTER JOIN dbo.TrainingGrantProgram tgProg ON tgProg.TrainingGrantId = tgrant.Id	
		LEFT OUTER JOIN dbo.Program prog ON prog.Id = tgProg.ProgramId
			AND prog.IsDeleted = 0
		INNER JOIN dbo.Applicant app ON (app.DepartmentId = tgDept.OrganizationId
			OR tgProg.ProgramId IN 
				(SELECT ProgramId FROM dbo.ApplicantProgram appProg WHERE appProg.PersonId = app.PersonId))
			--AND app.DoctoralLevelId = tgrant.DoctoralLevelId
			AND app.IsDeleted = 0
		LEFT OUTER JOIN dbo.Organization appOrg ON appOrg.Id = app.DepartmentId
			AND appOrg.IsDeleted = 0
		LEFT OUTER JOIN dbo.DoctoralLevel docLvl ON docLvl.Id = app.DoctoralLevelId
		LEFT OUTER JOIN dbo.OSUTimeline timeline ON timeline.PersonId = app.PersonId
			AND timeline.IsDeleted = 0
		
	WHERE 
		fund.IsDeleted = 0
		AND tgrant.Id IS NOT NULL
		--AND tgrant.Id = 'D159F84F-5D07-4462-BBC1-F9A8A3714E45' --'6B0B9258-72C6-407F-B84F-421A0E6FDDCA'

	ORDER BY ApplicantId


-- === CODE FOR ASSOCIATING APPLICANTS WITH SELECTED FACULTY DEPTS ===

	--SELECT DISTINCT TOP 100 PERCENT
	--	tgrant.Id AS TrainingGrantId
	--	, fund.Id AS FundingId
	--	, fund.Title
	--	, fac.Id AS FacultyId
	--	, facPerson.PersonId AS FacultyPersonId
	--	, facPerson.LastName AS FacultyLastName
	--	, facPerson.FirstName AS FacultyFirstName
	--	, CASE 
	--			WHEN facPerson.LastName IS NULL AND facPerson.FirstName IS NULL THEN NULL
	--			WHEN facPerson.LastName IS NOT NULL THEN facPerson.LastName
	--			ELSE ''
	--		END +
	--		CASE WHEN facPerson.FirstName IS NOT NULL THEN ', ' + facPerson.FirstName
	--			ELSE ''
	--		END +
	--		CASE WHEN facPerson.MiddleName IS NOT NULL THEN ' ' + facPerson.MiddleName
	--			ELSE ''
	--		END AS FacultyFullName
	--	, facPrimaryOrg.DisplayName AS FacultyPrimaryOrg
	--	, facSecondaryOrg.DisplayName AS FacultySecondaryOrg
	--	, app.Id AS ApplicantGUID
	--	, app.ApplicantId
	--	, app.DoctoralLevelId AS ApplicantTypeId
	--	, docLvl.LevelName AS ApplicantType
	--	, app.DepartmentId AS ApplicantDeptId
	--	, appOrg.DisplayName AS ApplicantDept
	--	, timeline.YearEntered
	--	, dbo.GetProgramsForApplicant(app.PersonId) AS ApplicantPrograms

	--FROM 
	--	dbo.Funding fund
	--	INNER JOIN dbo.TrainingGrant tgrant ON tgrant.FundingId = fund.Id
	--		AND tgrant.IsDeleted = 0
	--	INNER JOIN dbo.FundingFaculty fundFac ON fundFac.FundingId = fund.Id
	--		AND fundFac.IsDeleted = 0
	--	LEFT OUTER JOIN dbo.Faculty fac ON fac.Id = fundFac.FacultyId
	--		AND fac.IsDeleted = 0
	--	LEFT OUTER JOIN dbo.Person facPerson ON facPerson.PersonId = fac.PersonId
	--		AND facPerson.IsDeleted = 0
	--	INNER JOIN dbo.Applicant app ON (app.DepartmentId = fac.PrimaryOrganizationId
	--		OR app.DepartmentId = fac.SecondaryOrganizationId)
	--		AND app.IsDeleted = 0
	--	LEFT OUTER JOIN dbo.Organization facPrimaryOrg ON facPrimaryOrg.Id = fac.PrimaryOrganizationId
	--		AND facPrimaryOrg.IsDeleted = 0
	--	LEFT OUTER JOIN dbo.Organization facSecondaryOrg ON facSecondaryOrg.Id = fac.SecondaryOrganizationId
	--		AND facSecondaryOrg.IsDeleted = 0
	--	LEFT OUTER JOIN dbo.Organization appOrg ON appOrg.Id = app.DepartmentId
	--		AND appOrg.IsDeleted = 0
	--	LEFT OUTER JOIN dbo.DoctoralLevel docLvl ON docLvl.Id = app.DoctoralLevelId
	--	LEFT OUTER JOIN dbo.OSUTimeline timeline ON timeline.PersonId = app.PersonId
	--		AND timeline.IsDeleted = 0
		
	--WHERE 
	--	fund.IsDeleted = 0
	--	AND tgrant.Id IS NOT NULL
	--	--AND tgrant.Id = '6B0B9258-72C6-407F-B84F-421A0E6FDDCA'

	--ORDER BY ApplicantId
	








GO


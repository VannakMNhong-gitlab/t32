/****** Object:  StoredProcedure [Reports].[Sp_T32Table8B]    Script Date: 1/26/2016 3:49:25 PM ******/
DROP PROCEDURE [Reports].[Sp_T32Table8B]
GO

/****** Object:  StoredProcedure [Reports].[Sp_T32Table8B]    Script Date: 1/26/2016 3:49:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 09/15/2015
-- Description:	Used by T32 TABLE 8B Report -- POSTDOC APPLICANTS



--			NOTE (TODO): PER CUSTOMER: UNTIL TRAINEETRACKING APP IS CONNECTED TO PEOPLESOFT,
--			THIS REPORT CAN ONLY DISPLAY THE DEPTS AND PROGS RELEVANT TO THE SELECTED TRAINING
--			GRANT.


-- =============================================
CREATE PROCEDURE [Reports].[Sp_T32Table8B]
	@TrainingGrantId uniqueidentifier
AS
BEGIN

	SET NOCOUNT ON;

	--DECLARE @CurrentYear AS INT = YEAR(GETDATE())


	SELECT DISTINCT
		TrainingGrantId
		, IsRenewal
		, DoctoralLevelId
		, DoctoralLevel
		, Column2ItemTypeId
		, Column2ItemType
		, Column2ItemId
		, Column2ItemName

	FROM
		Reports.vw_T32Table8A

	WHERE
		((@TrainingGrantId IS NULL) OR (TrainingGrantId = @TrainingGrantId))

	ORDER BY
		Column2ItemTypeId
		, Column2ItemName


	SET NOCOUNT OFF
END

GO


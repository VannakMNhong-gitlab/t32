/****** Object:  View [Search].[vw_SupportOrganizations]    Script Date: 12/1/2015 4:12:12 PM ******/
DROP VIEW [Search].[vw_SupportOrganizations]
GO

/****** Object:  View [Search].[vw_SupportOrganizations]    Script Date: 12/1/2015 4:12:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [Search].[vw_SupportOrganizations] 
AS 

	SELECT
		sptOrg.Id
		, sptOrg.Name
		, sptOrg.Abbreviation
		, sptOrg.Name + ' (' + sptOrg.Abbreviation + ')' AS FullName
		, sptOrg.SortOrder
		--, (vw.TotalOrganization) AS TotalOrganizationTypeUsage

	FROM 
		dbo.SupportOrganization sptOrg
		--LEFT OUTER JOIN Search.vw_UsageOfSupportTypes vw ON vw.OrganizationTypeId = orgType.Id


GO


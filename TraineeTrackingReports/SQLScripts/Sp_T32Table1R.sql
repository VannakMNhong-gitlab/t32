/****** Object:  StoredProcedure [Reports].[Sp_T32Table1R]    Script Date: 1/26/2016 3:43:45 PM ******/
DROP PROCEDURE [Reports].[Sp_T32Table1R]
GO

/****** Object:  StoredProcedure [Reports].[Sp_T32Table1R]    Script Date: 1/26/2016 3:43:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 06/12/2015
-- Description:	Used by T32 TABLE 1 (Renewal) Report
-- =============================================
CREATE PROCEDURE [Reports].[Sp_T32Table1R]
	@TrainingGrantId uniqueidentifier
AS
BEGIN

	SET NOCOUNT ON;

	SELECT DISTINCT
		vw.TrainingGrantId
		, vw.IsRenewal
		, vw.DoctoralLevelId
		, vw.DoctoralLevel
		, vw.Column1ItemTypeId
		, vw.Column1ItemType
		, vw.Column1ItemId
		, vw.Column1ItemName
		, vw.TotalParticipatingFaculty
		, vw.TotalPredocMentees
		, vw.TotalPredocMenteesOnAnyTG
		, vw.TotalPredocMenteesOnTG
		, vw.TotalPredocTGEMenteesOnTG
		, vw.TotalPredocURMMenteesOnTG
		, vw.TotalPredocDisabilitiesMenteesOnTG
		, vw.TotalPredocDisadvantagedMenteesOnTG
		, vw.TotalPostdocMentees
		, vw.TotalPostdocMenteesOnAnyTG
		, vw.TotalPostdocMenteesOnTG
		, vw.TotalPostdocTGEMenteesOnTG
		, vw.TotalPostdocURMMenteesOnTG
		, vw.TotalPostdocDisabilitiesMenteesOnTG
		, vw.TotalPostdocDisadvantagedMenteesOnTG
		, vw.TotalPredocTrainees
		, vw.TotalPredocTGETrainees
		, vw.TotalPredocURMTrainees
		, vw.TotalPredocDisabilitiesTrainees
		, vw.TotalPredocDisadvantagedTrainees
		, vw.TotalPostdocTrainees
		, vw.TotalPostdocTGETrainees
		, vw.TotalPostdocURMTrainees
		, vw.TotalPostdocDisabilitiesTrainees
		, vw.TotalPostdocDisadvantagedTrainees

	FROM 
		Reports.vw_T32Table1R vw

	WHERE 
		((@TrainingGrantId IS NULL) OR (vw.TrainingGrantId = @TrainingGrantId))

		--AND @TrainingGrantId = 'D159F84F-5D07-4462-BBC1-F9A8A3714E45'

	SET NOCOUNT OFF
END

GO


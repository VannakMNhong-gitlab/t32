/****** Object:  View [Search].[vw_TrainingGrantTrainees]    Script Date: 5/2/2016 4:56:36 PM ******/
DROP VIEW [Search].[vw_TrainingGrantTrainees]
GO

/****** Object:  View [Search].[vw_TrainingGrantTrainees]    Script Date: 5/2/2016 4:56:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_TrainingGrantTrainees] 
AS 
	SELECT DISTINCT
		tgrant.Id AS TrainingGrantId
		, fund.Id AS FundingId
		, trainee.Id AS TrainingGrantTraineeId
		, trainee.DateStarted AS AppointmentStartDate
		, trainee.DateEnded AS AppointmentEndDate
		, dbo.StripHTML_Loop(fund.Title) AS Title
		, trainee.Id AS TraineeId
		, mentee.Id AS MenteeId
		, mentPerson.PersonId AS MenteePersonId
		, mentPerson.LastName AS MenteeLastName
		, mentPerson.FirstName AS MenteeFirstName
		, CASE 
				WHEN mentPerson.LastName IS NULL AND mentPerson.FirstName IS NULL THEN NULL
				WHEN mentPerson.LastName IS NOT NULL THEN mentPerson.LastName
				ELSE ''
			END +
			CASE WHEN mentPerson.FirstName IS NOT NULL THEN ', ' + mentPerson.FirstName
				ELSE ''
			END +
			CASE WHEN mentPerson.MiddleName IS NOT NULL THEN ' ' + mentPerson.MiddleName
				ELSE ''
			END AS MenteeFullName
		, docLvl.Id AS MenteeTypeId
		, docLvl.LevelName AS MenteeType
		, tp.YearStarted
		, tp.YearEnded
		, org.Id AS DepartmentId
		, org.DisplayName AS DepartmentName
		, instAssoc.Name AS InstitutionAssociation
		, prog.Id AS ProgramId
		, prog.Title AS ProgramTitle
		, tp.ResearchProjectTitle AS ResearchExperience
		, tp.DegreeSoughtId
		, degSought.Name AS DegreeSought
		, CASE WHEN trainee.PredocTraineeStatusId IS NOT NULL THEN tStatusPre.Name
			WHEN trainee.PostdocTraineeStatusId IS NOT NULL THEN tStatusPost.Name
			ELSE 'Undefined' END ProgramStatus
		, trainee.DateLastUpdated AS TraineeDateLastUpdated
		, trainee.LastUpdatedBy AS TraineeLastUpdatedBy
		, updLogins.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLogins.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLogins.MiddleName)), 1) + '. '
			ELSE '' END + updLogins.LastName AS TraineeLastUpdatedByName

	FROM 
		dbo.Funding fund
		INNER JOIN dbo.TrainingGrant tgrant ON tgrant.FundingId = fund.Id
			AND tgrant.IsDeleted = 0
		INNER JOIN dbo.TrainingGrantTrainee trainee ON trainee.TrainingGrantId = tgrant.Id
			AND trainee.IsDeleted = 0
		INNER JOIN dbo.Mentee mentee ON mentee.Id = trainee.MenteeId
			AND mentee.IsDeleted = 0
		INNER JOIN dbo.Person mentPerson ON mentPerson.PersonId = mentee.PersonId
			AND mentPerson.IsDeleted = 0
		INNER JOIN dbo.TrainingPeriod tp ON tp.Id = trainee.TrainingPeriodId
			AND tp.IsDeleted = 0
		LEFT OUTER JOIN dbo.TrainingPeriodProgram tpprog ON tpprog.TrainingPeriodId = tp.Id
		LEFT OUTER JOIN dbo.Program prog ON prog.Id = tpprog.ProgramId
			AND prog.IsDeleted = 0
		LEFT OUTER JOIN dbo.AcademicDegree degSought ON degSought.Id = tp.DegreeSoughtId
			AND degSought.IsDeleted = 0
		LEFT OUTER JOIN dbo.DoctoralLevel docLvl ON docLvl.Id = tp.DoctoralLevelId
		LEFT OUTER JOIN dbo.Organization org ON org.Id = mentee.DepartmentId
			AND org.IsDeleted = 0
		LEFT OUTER JOIN dbo.TraineeStatus tStatusPre ON tStatusPre.Id = trainee.PredocTraineeStatusId
		LEFT OUTER JOIN dbo.TraineeStatus tStatusPost ON tStatusPost.Id = trainee.PostdocTraineeStatusId
		LEFT OUTER JOIN dbo.InstitutionAssociation instAssoc ON instAssoc.Id = mentee.InstitutionAssociationId
		LEFT OUTER JOIN Search.vw_Logins updLogins ON updLogins.Username = trainee.LastUpdatedBy
		
	WHERE 
		fund.IsDeleted = 0
		AND tgrant.Id IS NOT NULL




GO


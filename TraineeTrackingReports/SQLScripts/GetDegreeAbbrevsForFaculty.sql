/****** Object:  UserDefinedFunction [dbo].[GetDegreeAbbrevsForFaculty]    Script Date: 5/13/2015 3:48:49 PM ******/
DROP FUNCTION [dbo].[GetDegreeAbbrevsForFaculty]
GO

/****** Object:  UserDefinedFunction [dbo].[GetDegreeAbbrevsForFaculty]    Script Date: 5/13/2015 3:48:49 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE FUNCTION [dbo].[GetDegreeAbbrevsForFaculty]
(@facultyId UNIQUEIDENTIFIER)
RETURNS VARCHAR(max) 
AS
Begin
	DECLARE @degrees VARCHAR(MAX)		
	
	SELECT @degrees = COALESCE(@degrees + '; ' ,'') + facDegrees.Name
	FROM 
		(
		SELECT DISTINCT deg.Name AS Name
		FROM dbo.Faculty fac			
			JOIN dbo.FacultyAcademicData acad ON acad.PersonId = fac.PersonId
				AND acad.IsDeleted = 0
			JOIN dbo.AcademicDegree deg ON deg.Id = acad.AcademicDegreeId
				AND deg.IsDeleted = 0		 				
		WHERE fac.IsDeleted = 0
			AND fac.Id = @facultyId
		) AS facDegrees

	RETURN  @degrees
end 


GO


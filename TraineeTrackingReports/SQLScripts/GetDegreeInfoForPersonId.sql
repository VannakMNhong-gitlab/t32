/****** Object:  UserDefinedFunction [dbo].[GetDegreeInfoForPersonId]    Script Date: 5/13/2015 3:48:56 PM ******/
DROP FUNCTION [dbo].[GetDegreeInfoForPersonId]
GO

/****** Object:  UserDefinedFunction [dbo].[GetDegreeInfoForPersonId]    Script Date: 5/13/2015 3:48:56 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE FUNCTION [dbo].[GetDegreeInfoForPersonId]
(@personId UNIQUEIDENTIFIER)
RETURNS VARCHAR(max) 
AS
Begin
	DECLARE @degreelist VARCHAR(MAX)		
	
	SELECT @degreelist = COALESCE(@degreelist + '; ' ,'') + degreelist.DegreeAndYear
	FROM 
		(
		SELECT DISTINCT TOP 100 PERCENT deg.Name + ' ''' + RIGHT(hist.YearDegreeCompleted, 2) AS DegreeAndYear, hist.YearDegreeCompleted
		FROM dbo.AcademicHistory hist
			JOIN dbo.AcademicDegree deg ON deg.Id = hist.AcademicDegreeId
		WHERE hist.PersonId = @personId
			AND hist.IsDeleted = 0
		ORDER BY hist.YearDegreeCompleted
		) AS degreelist

	RETURN  @degreelist
end 


GO


/****** Object:  View [Search].[vw_UsageOfEthnicities]    Script Date: 5/2/2016 3:18:34 PM ******/
DROP VIEW [Search].[vw_UsageOfEthnicities]
GO

/****** Object:  View [Search].[vw_UsageOfEthnicities]    Script Date: 5/2/2016 3:18:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [Search].[vw_UsageOfEthnicities] 
AS 
	SELECT 
		eth.Id AS EthnicityId
		, eth.Name AS Ethnicity
		, (SELECT Count(EthnicityId) 
				FROM dbo.Person p		
				WHERE p.EthnicityId = eth.Id
					AND p.IsDeleted = 0) 
		AS TotalEthnicities

	FROM 
		dbo.Ethnicity eth

	WHERE
		eth.IsDeleted = 0

	GROUP BY
		eth.Id
		, eth.Name
		

GO


/****** Object:  UserDefinedFunction [dbo].[StripHTML_Loop]    Script Date: 5/13/2015 3:49:33 PM ******/
DROP FUNCTION [dbo].[StripHTML_Loop]
GO

/****** Object:  UserDefinedFunction [dbo].[StripHTML_Loop]    Script Date: 5/13/2015 3:49:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[StripHTML_Loop]( 
	@htmldesc varchar(max) 
) RETURNS varchar(max) AS
BEGIN
    DECLARE @first int, @last int,@len int 

	SET @first = CHARINDEX('<',@htmldesc) 
	SET @last = CHARINDEX('>',@htmldesc,CHARINDEX('<',@htmldesc)) 
	SET @len = (@last - @first) + 1 

	WHILE @first > 0 AND @last > 0 AND @len > 0 
	BEGIN 
		---Stuff function is used to insert string at given position and delete number of characters specified from original string
		SET @htmldesc = STUFF(@htmldesc,@first,@len,'')  
		SET @first = CHARINDEX('<',@htmldesc) 
		SET @last = CHARINDEX('>',@htmldesc,CHARINDEX('<',@htmldesc)) 
		SET @len = (@last - @first) + 1 
	END 

	SET @htmldesc = REPLACE(@htmldesc, '&gt;', '>')
	SET @htmldesc = REPLACE(@htmldesc, '&lt;', '<')
	SET @htmldesc = REPLACE(@htmldesc, '&nbsp;', ' ')
	SET @htmldesc = REPLACE(@htmldesc, '&copy;', '-')

	RETURN LTRIM(RTRIM(@htmldesc)) 
END

--SELECT dbo.StripHTML_Loop('MVN =&gt;Debugging&nbsp;')
--select dbo.StripHTML_Loop('<p style="margin: 0px 0px 20px; padding: 0px; color: #333333; ">If you are using an identity column on your SQL Server tables, you can set the next insert value to whatever value you want.</p>  <p style="margin: 20px 0px; ">It would be wise to first check </p>  ')
GO


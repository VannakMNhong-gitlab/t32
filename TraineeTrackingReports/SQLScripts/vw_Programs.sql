/****** Object:  View [Search].[vw_Programs]    Script Date: 6/2/2015 10:27:28 AM ******/
DROP VIEW [Search].[vw_Programs]
GO

/****** Object:  View [Search].[vw_Programs]    Script Date: 6/2/2015 10:27:28 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [Search].[vw_Programs] 
AS 
	SELECT 
		prog.Id AS ProgramId
		, prog.Title AS ProgramTitle
		, prog.DisplayId AS ProgramDisplayId
		, (vw.TotalApplicantPrograms + vw.TotalFacultyPrograms + vw.TotalTrainingGrantPrograms 
			+ vw.TotalTrainingPeriodPrograms) AS TotalProgramUsage

	FROM 
		dbo.Program prog
		LEFT OUTER JOIN Search.vw_UsageOfPrograms vw ON vw.ProgramId = prog.Id

	WHERE 
		prog.IsDeleted = 0


GO


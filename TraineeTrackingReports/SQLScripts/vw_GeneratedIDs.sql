/****** Object:  View [Search].[vw_GeneratedIDs]    Script Date: 5/13/2015 3:43:30 PM ******/
DROP VIEW [Search].[vw_GeneratedIDs]
GO

/****** Object:  View [Search].[vw_GeneratedIDs]    Script Date: 5/13/2015 3:43:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_GeneratedIDs] 
AS 
	SELECT DISTINCT TOP 100 PERCENT 
		StudentId AS GeneratedId,
		'Mentee' AS IDType
	FROM dbo.Mentee
	WHERE StudentId LIKE 'NON%'
		AND IsDeleted = 0

	UNION

	SELECT DISTINCT TOP 100 PERCENT  
		CAST(ApplicantId AS VARCHAR) AS GeneratedId,
		'Applicant' AS IDType
	FROM dbo.Applicant
	WHERE IsDeleted = 0

	UNION

	SELECT DISTINCT TOP 100 PERCENT 
		EmployeeId AS GeneratedId,
		'Faculty' AS IDType
	FROM dbo.Faculty
	WHERE EmployeeId LIKE 'NON%'
		AND IsDeleted = 0

	ORDER BY IDType DESC, GeneratedId ASC



GO


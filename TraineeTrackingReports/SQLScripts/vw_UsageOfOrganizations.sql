/****** Object:  View [Search].[vw_UsageOfOrganizations]    Script Date: 6/2/2015 10:26:57 AM ******/
DROP VIEW [Search].[vw_UsageOfOrganizations]
GO

/****** Object:  View [Search].[vw_UsageOfOrganizations]    Script Date: 6/2/2015 10:26:57 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_UsageOfOrganizations] 
AS 
	SELECT 
		org.Id AS OrganizationId
		, org.DisplayName AS OrganizationName
		, (SELECT Count(DepartmentId) FROM dbo.Applicant app 
			WHERE app.DepartmentId = org.Id
				AND app.IsDeleted = 0) AS TotalApplicantDepts
		, (SELECT Count(PrimaryOrganizationId) + Count(SecondaryOrganizationId) + Count(TIUId) FROM dbo.Faculty fac 
			WHERE ((fac.PrimaryOrganizationId = org.Id)
					OR (fac.SecondaryOrganizationId = org.Id)
					OR (fac.TIUId = org.Id))
				AND fac.IsDeleted = 0) AS TotalFacultyOrgs
		, (SELECT Count(DepartmentId) FROM dbo.Mentee mentee 
			WHERE mentee.DepartmentId = org.Id
				AND mentee.IsDeleted = 0) AS TotalMenteeDepts		
		, (SELECT Count(OrganizationId) FROM dbo.TrainingGrantDepartment tgdept 
			WHERE tgdept.OrganizationId = org.Id) AS TotalTrainingGrantDepts

	FROM 
		dbo.Organization org

	WHERE 
		org.IsDeleted = 0

	GROUP BY
		org.Id
		, org.DisplayName





GO


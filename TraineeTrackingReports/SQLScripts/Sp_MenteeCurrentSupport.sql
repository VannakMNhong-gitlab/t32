/****** Object:  StoredProcedure [Reports].[Sp_MenteeCurrentSupport]    Script Date: 5/2/2016 4:30:46 PM ******/
DROP PROCEDURE [Reports].[Sp_MenteeCurrentSupport]
GO

/****** Object:  StoredProcedure [Reports].[Sp_MenteeCurrentSupport]    Script Date: 5/2/2016 4:30:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 04/14/2016
-- Description:	Used by Report: Mentee
-- =============================================
CREATE PROCEDURE [Reports].[Sp_MenteeCurrentSupport]
	@MenteeId UNIQUEIDENTIFIER

AS
BEGIN

	SET NOCOUNT ON;

	IF @MenteeId IS NOT NULL
	BEGIN

		SELECT TOP 100 PERCENT 
			mentee.Id AS MenteeId
			, mentee.PersonId AS MenteePersonId
			, p.LastName
			, mentspt.Id AS CurrentSupportId
			, inst.Id AS InstitutionId
			, inst.Name AS Institution
			, mentspt.SupportSourceText
			, mentspt.DateLastUpdated
			, updLogins.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLogins.MiddleName))) > 0
					THEN LEFT(LTRIM(RTRIM(updLogins.MiddleName)), 1) + '. '
					ELSE '' END + updLogins.LastName 
				AS LastUpdatedByName

		FROM 
			dbo.Mentee mentee
			INNER JOIN dbo.Person p ON p.PersonId = mentee.PersonId
				AND p.IsDeleted = 0
			INNER JOIN dbo.MenteeSupport mentspt ON mentspt.PersonId = mentee.PersonId
				AND mentspt.IsDeleted = 0
			LEFT OUTER JOIN dbo.Institution inst ON inst.Id = mentspt.InstitutionId
				AND inst.IsDeleted = 0
			LEFT OUTER JOIN Search.vw_Logins updLogins ON updLogins.Username = mentspt.LastUpdatedBy
	
		WHERE
			mentee.IsDeleted = 0
			AND mentee.Id = @MenteeId

		ORDER BY
			mentspt.Id

	END

	SET NOCOUNT OFF
END
GO


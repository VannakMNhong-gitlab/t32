/****** Object:  StoredProcedure [Reports].[Sp_T32Table6B]    Script Date: 1/26/2016 3:47:30 PM ******/
DROP PROCEDURE [Reports].[Sp_T32Table6B]
GO

/****** Object:  StoredProcedure [Reports].[Sp_T32Table6B]    Script Date: 1/26/2016 3:47:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 09/16/2015
-- Description:	Used by T32 TABLE 6B (New) Report
--				Publications by Postdoc Mentees of Participating Faculty
-- =============================================
CREATE PROCEDURE [Reports].[Sp_T32Table6B]
	@TrainingGrantId uniqueidentifier,
	@CurrentYear INT
AS
BEGIN

	SET NOCOUNT ON;

	SET @CurrentYear = ISNULL(@CurrentYear, YEAR(GETDATE()))

	SELECT	
		mentee.TrainingGrantId
		, mentee.FacultyId
		, mentee.FacultyLastName + ', ' + LEFT(mentee.FacultyFirstName, 1) AS FacultyNameAbbrev
		, mentee.MenteeId
		, mentee.MenteeLastName + ', ' + LEFT(mentee.MenteeFirstName, 1) AS MenteeNameAbbrev
		, mentee.IsTrainingGrantEligible
		, mentee.TrainingPeriodYearStarted
		, mentee.TrainingPeriodYearEnded
		, CASE WHEN mentee.TrainingPeriodYearEnded IS NULL THEN 'Current'
			WHEN mentee.TrainingPeriodYearEnded < YEAR(GETDATE()) THEN 'Past'
			ELSE 'Current' END AS PastCurrent
		, pub.Pub_GUID
		, pub.PubYearPublished
		, dbo.StripHTML_Loop(pub.PubCitation) AS PubCitation
	FROM 
		Search.vw_TrainingGrantMentees mentee
		LEFT OUTER JOIN Search.vw_MenteePublications pub ON pub.MenteeId = mentee.MenteeId

	WHERE 
		((@TrainingGrantId IS NULL) OR (mentee.TrainingGrantId = @TrainingGrantId))
		AND ((mentee.TrainingPeriodYearEnded IS NULL)
			OR (mentee.TrainingPeriodYearStarted BETWEEN (@CurrentYear - 10) AND @CurrentYear)
			OR ((mentee.TrainingPeriodYearEnded IS NULL) 
				OR (mentee.TrainingPeriodYearEnded BETWEEN (@CurrentYear - 10) AND @CurrentYear))
		)
		AND mentee.MenteeTypeId = 2

	GROUP BY
		mentee.TrainingGrantId
		, mentee.FacultyId		
		, mentee.MenteeId
		, mentee.FacultyLastName
		, mentee.FacultyFirstName
		, mentee.MenteeLastName
		, mentee.MenteeFirstName
		, mentee.IsTrainingGrantEligible
		, pub.Pub_GUID
		, pub.PubYearPublished
		, pub.PubCitation
		, mentee.TrainingPeriodYearStarted
		, mentee.TrainingPeriodYearEnded


		--AND @TrainingGrantId = 'D159F84F-5D07-4462-BBC1-F9A8A3714E45'

	SET NOCOUNT OFF
END

GO


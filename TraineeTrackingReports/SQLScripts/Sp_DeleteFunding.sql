/****** Object:  StoredProcedure [Search].[Sp_DeleteFunding]    Script Date: 5/13/2015 3:48:06 PM ******/
DROP PROCEDURE [Search].[Sp_DeleteFunding]
GO

/****** Object:  StoredProcedure [Search].[Sp_DeleteFunding]    Script Date: 5/13/2015 3:48:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 11/12/2014
-- Description:	Perform all of the updates and deletes necessary to 
--				DELETE a Funding record
--		NOTE:	Set @SoftDelete to TRUE to soft delete the Funding record
--				** HARD DELETES CANNOT BE UNDONE **
-- =============================================
CREATE PROCEDURE [Search].[Sp_DeleteFunding]
	@FundingId uniqueidentifier,
	@DisplayId INT,
	@SoftDelete BIT,
	@CalledByDeleteTrainingGrant BIT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @OKTORUN INT = 1

	-- If @FundingId was not passed in, go find it to use for deletes
	IF @FundingId IS NULL AND @DisplayId IS NOT NULL
		SET @FundingId = (SELECT Id FROM dbo.Funding
						WHERE DisplayId = @DisplayId)
	
	-- Set to SoftDelete by default if NULL is passed in
	SET @SoftDelete = ISNULL(@SoftDelete, 1)
	SET @CalledByDeleteTrainingGrant = ISNULL(@CalledByDeleteTrainingGrant, 0)

	-- Check to make sure the Funding record is not associated with a
	-- Training Grant. If so, the delete has to be performed through the
	-- DeleteTrainingGrant SP
	IF @CalledByDeleteTrainingGrant = 0 AND ((SELECT COUNT(*)
		FROM dbo.TrainingGrant
		WHERE FundingId = @FundingId) > 0)
	BEGIN
		PRINT 'The entered Funding record is associated with a Training Grant.
				Please use the Sp_DeleteTrainingGrant Stored Procedure to perform this action.
				Delete cancelled.'
	END
	ELSE
	BEGIN

		IF @OKTORUN = 1
		BEGIN

		IF @FundingId IS NOT NULL
		BEGIN

			IF @SoftDelete = 1
			BEGIN

				-- Soft delete all associated Funding Faculty records
				UPDATE dbo.FundingFaculty
				SET	IsDeleted = 1
				WHERE FundingId = @FundingId

				-- Soft delete all associated FundingDirectCost records
				UPDATE dbo.FundingDirectCost
				SET	
					IsDeleted = 1,
					DateLastUpdated = GETDATE(),
					LastUpdatedBy = RIGHT(SYSTEM_USER, 6)
				WHERE FundingId = @FundingId

				-- Soft delete associated Funding record
				UPDATE dbo.Funding
				SET	
					IsDeleted = 1,
					DateLastUpdated = GETDATE(),
					LastUpdatedBy = RIGHT(SYSTEM_USER, 6)
				WHERE Id = @FundingId

			END
			ELSE
			BEGIN

				-- HARD delete all associated Funding Faculty records
				DELETE 
				FROM dbo.FundingFaculty 
				WHERE FundingId = @FundingId

				-- HARD delete all associated FundingDirectCost records
				DELETE 
				FROM dbo.FundingDirectCost 
				WHERE FundingId = @FundingId

				-- HARD delete all associated Funding records
				DELETE 
				FROM dbo.Funding
				WHERE Id = @FundingId

			END

		END
		END

	END

	SET NOCOUNT OFF
END

GO


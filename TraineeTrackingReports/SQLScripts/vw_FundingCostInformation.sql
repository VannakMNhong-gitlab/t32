/****** Object:  View [Search].[vw_FundingCostInformation]    Script Date: 5/2/2016 4:41:07 PM ******/
DROP VIEW [Search].[vw_FundingCostInformation]
GO

/****** Object:  View [Search].[vw_FundingCostInformation]    Script Date: 5/2/2016 4:41:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [Search].[vw_FundingCostInformation] 
AS 
	SELECT DISTINCT
		fund.Id AS FundingId
		, costs.Id AS FundingCostId
		, fund.SponsorAwardNumber
		, dbo.StripHTML_Loop(Title) AS Title
		, fund.DateProjectedStart
		, fund.DateProjectedEnd
		, fund.DateStarted AS DateTimeFundingStarted
		, fund.DateEnded AS DateTimeFundingEnded
		, costs.CostYear
		, costs.DateStarted AS DateCostsStarted
		, costs.DateEnded AS DateCostsEnded
		, costs.BudgetPeriodStatusId
		, budgstat.Name AS BudgetPeriodStatus
		, costs.CurrentYearDirectCosts
		, costs.TotalDirectCosts
		, costs.DateLastUpdated
		, costs.LastUpdatedBy
		, updLogins.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLogins.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLogins.MiddleName)), 1) + '. '
			ELSE '' END + updLogins.LastName AS LastUpdatedByName

	FROM 
		dbo.Funding fund
		LEFT OUTER JOIN dbo.FundingDirectCost costs ON costs.FundingId = fund.Id
		LEFT OUTER JOIN dbo.TrainingGrant tgrant ON tgrant.FundingId = fund.Id
		LEFT OUTER JOIN dbo.BudgetPeriodStatus budgstat ON budgstat.Id = costs.BudgetPeriodStatusId
		LEFT OUTER JOIN Search.vw_Logins updLogins ON updLogins.Username = costs.LastUpdatedBy		

	WHERE 
		fund.IsDeleted = 0
		AND costs.IsDeleted = 0
		AND tgrant.Id IS NULL




GO


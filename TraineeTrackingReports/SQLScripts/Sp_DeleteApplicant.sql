/****** Object:  StoredProcedure [Search].[Sp_DeleteApplicant]    Script Date: 6/2/2015 10:25:58 AM ******/
DROP PROCEDURE [Search].[Sp_DeleteApplicant]
GO

/****** Object:  StoredProcedure [Search].[Sp_DeleteApplicant]    Script Date: 6/2/2015 10:25:58 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 09/15/2014
-- Description:	Perform all of the updates and deletes necessary to 
--				DELETE an Applicant
--		NOTE:	Set @SoftDelete to TRUE to soft delete the Applicant
--				** HARD DELETES CANNOT BE UNDONE **
-- =============================================
CREATE PROCEDURE [Search].[Sp_DeleteApplicant]
	@PersonId uniqueidentifier,
	@ApplicantId INT,
	@SoftDelete BIT,
	@DeletePersonRecord BIT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ApplicantGUID uniqueidentifier

	-- If @PersonId was not passed in, go find it to use for deletes
	IF @PersonId IS NULL AND @ApplicantId IS NOT NULL
		SET @PersonId = (SELECT PersonId FROM dbo.Applicant
						WHERE ApplicantId = @ApplicantId)
	
	
	-- Set to SoftDelete by default if NULL is passed in
	SET @SoftDelete = ISNULL(@SoftDelete, 1)

	SET @DeletePersonRecord = ISNULL(@DeletePersonRecord, 1)


	IF @PersonId IS NOT NULL
	BEGIN

		SET @ApplicantGUID = (SELECT Id FROM dbo.Applicant
								WHERE PersonId = @PersonId)
		
		IF @SoftDelete = 1
		BEGIN
			-- Soft delete all associated Academic History records
			UPDATE dbo.AcademicHistory 
			SET	
				IsDeleted = 1,
				DateLastUpdated = GETDATE(),
				LastUpdatedBy = RIGHT(SYSTEM_USER, 6)
			WHERE PersonId = @PersonId

			-- Soft delete all associated Grade Record records
			UPDATE dbo.GradeRecord
			SET	IsDeleted = 1
			WHERE PersonId = @PersonId

			-- Soft delete all associated OSUTimeline records
			UPDATE dbo.OSUTimeline 
			SET	IsDeleted = 1
			WHERE PersonId = @PersonId

			-- Soft delete all associated Previous Training Grant Applicant records
			--UPDATE dbo.PreviousTrainingGrantApplicant
			--SET	IsDeleted = 1
			--WHERE ApplicantId = @ApplicantGUID

			-- Soft delete associated Applicant record
			UPDATE dbo.Applicant
			SET	
				IsDeleted = 1,
				DateLastUpdated = GETDATE(),
				LastUpdatedBy = RIGHT(SYSTEM_USER, 6)
			WHERE PersonId = @PersonId

			-- Soft delete associated Person record
			UPDATE dbo.Person
			SET	
				IsDeleted = 1,
				DateLastUpdated = GETDATE(),
				LastUpdatedBy = RIGHT(SYSTEM_USER, 6)
			WHERE PersonId = @PersonId

		END
		ELSE
		BEGIN
			-- HARD delete all associated Academic History records
			DELETE 
			FROM dbo.AcademicHistory 
			WHERE PersonId = @PersonId

			-- HARD delete all associated Applicant Program records
			DELETE 
			FROM dbo.ApplicantProgram 
			WHERE PersonId = @PersonId

			-- HARD delete all associated Grade Record records
			DELETE
			FROM dbo.GradeRecord
			WHERE PersonId = @PersonId

			-- HARD delete all associated OSUTimeline records
			DELETE
			FROM dbo.OSUTimeline 
			WHERE PersonId = @PersonId

			-- HARD delete associated Previous Training Grant Applicant records
			DELETE
			FROM dbo.PreviousTrainingGrantApplicant
			WHERE ApplicantId = @ApplicantGUID

			-- HARD delete associated Applicant record
			DELETE
			FROM dbo.Applicant
			WHERE PersonId = @PersonId

			IF @DeletePersonRecord = 1
			BEGIN
				-- HARD delete associated Person record
				DELETE
				FROM dbo.Person
				WHERE PersonId = @PersonId
			END

		END
	END

	SET NOCOUNT OFF
END

GO


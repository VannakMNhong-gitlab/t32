/****** Object:  StoredProcedure [Reports].[Sp_T32Table7B]    Script Date: 1/26/2016 3:47:47 PM ******/
DROP PROCEDURE [Reports].[Sp_T32Table7B]
GO

/****** Object:  StoredProcedure [Reports].[Sp_T32Table7B]    Script Date: 1/26/2016 3:47:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 10/09/2015
-- Description:	Used by T32 TABLE 7B Report -- POSTDOC APPLICANTS ON TG



--			NOTE (TODO): PER CUSTOMER: UNTIL TRAINEETRACKING APP IS CONNECTED TO PEOPLESOFT,
--			THIS REPORT CAN ONLY DISPLAY THE DEPTS AND PROGS RELEVANT TO THE SELECTED TRAINING
--			GRANT.


-- =============================================
CREATE PROCEDURE [Reports].[Sp_T32Table7B]
	@TrainingGrantId uniqueidentifier
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @CurrentYear INT = YEAR(GETDATE())

	DECLARE @TempYears TABLE(id INT IDENTITY(1,1), Yr int)	

	INSERT INTO @TempYears(Yr)
	VALUES(@CurrentYear-4),
		(@CurrentYear-3),
		(@CurrentYear-2),
		(@CurrentYear-1),
		(@CurrentYear)


	DECLARE @TempDegrees TABLE(id INT IDENTITY(1,1), Degree VARCHAR(10))	

	INSERT INTO @TempDegrees(Degree)
	VALUES('PhD'),
		('MD'),
		('MD/PhD'),
		('Other'),
		('Total')


	DECLARE @TempSummary TABLE(TrainingGrantId uniqueidentifier
		, Column1ItemTypeId INT
		, Column1ItemType VARCHAR(25)
		, Column1ItemId INT
		, Column1ItemName VARCHAR(50)
		, DegreeSortOrder INT
		, Degree VARCHAR(25))	

	INSERT INTO @TempSummary(TrainingGrantId, Column1ItemTypeId, Column1ItemType, Column1ItemName, DegreeSortOrder, Degree)
	VALUES (@TrainingGrantId, 4, 'Other', 'Total All Programs', 0, '')


	SELECT DISTINCT
		TrainingGrantId
		, Column1ItemTypeId
		, Column1ItemType
		, Column1ItemId
		, Column1ItemName
		, d.Id AS DegreeSortOrder
		, d.Degree
		, y.Yr
		

	FROM
		Reports.vw_T32Table7B t
		CROSS JOIN @TempYears y
		CROSS JOIN @TempDegrees d

	WHERE
		((@TrainingGrantId IS NULL) OR (TrainingGrantId = @TrainingGrantId))


	UNION

	SELECT
		t.*
		, y.Yr

	FROM @TempSummary t
		CROSS JOIN @TempYears y

	UNION

	SELECT
		@TrainingGrantId, 3, 'Other', 0, 'List similar data for other Department / Programs', 0, '', NULL

	ORDER BY
		Column1ItemTypeId
		, Column1ItemName
		, Yr
		, Degree

	SET NOCOUNT OFF
END

GO


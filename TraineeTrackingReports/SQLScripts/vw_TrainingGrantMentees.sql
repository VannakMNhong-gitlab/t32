/****** Object:  View [Search].[vw_TrainingGrantMentees]    Script Date: 7/7/2015 10:17:34 AM ******/
DROP VIEW [Search].[vw_TrainingGrantMentees]
GO

/****** Object:  View [Search].[vw_TrainingGrantMentees]    Script Date: 7/7/2015 10:17:34 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [Search].[vw_TrainingGrantMentees] 
AS 
	SELECT DISTINCT
		tgrant.Id AS TrainingGrantId
		, fund.Id AS FundingId
		, dbo.StripHTML_Loop(fund.Title) AS TrainingGrantTitle
		, fund.DateProjectedStart AS TrainingGrantProjectedStartDate
		, fund.DateProjectedEnd AS TrainingGrantProjectedEndDate
		, fund.DateStarted AS TrainingGrantDateStarted
		, fund.DateEnded AS TrainingGrantDateEnded
		, tgrant.DoctoralLevelId AS TrainingGrantTypeId
		, docLvlTG.LevelName AS TrainingGrantType
		, fac.Id AS FacultyId
		, facPerson.PersonId AS FacultyPersonId
		, facPerson.LastName AS FacultyLastName
		, facPerson.FirstName AS FacultyFirstName
		, CASE 
				WHEN facPerson.LastName IS NULL AND facPerson.FirstName IS NULL THEN NULL
				WHEN facPerson.LastName IS NOT NULL THEN facPerson.LastName
				ELSE ''
			END +
			CASE WHEN facPerson.FirstName IS NOT NULL THEN ', ' + facPerson.FirstName
				ELSE ''
			END +
			CASE WHEN facPerson.MiddleName IS NOT NULL THEN ' ' + facPerson.MiddleName
				ELSE ''
			END AS FacultyFullName
		, mentee.Id AS MenteeId
		, mentee.StudentId
		, mentPerson.PersonId AS MenteePersonId
		, mentPerson.LastName AS MenteeLastName
		, mentPerson.FirstName AS MenteeFirstName
		, CASE 
				WHEN mentPerson.LastName IS NULL AND mentPerson.FirstName IS NULL THEN NULL
				WHEN mentPerson.LastName IS NOT NULL THEN mentPerson.LastName
				ELSE ''
			END +
			CASE WHEN mentPerson.FirstName IS NOT NULL THEN ', ' + mentPerson.FirstName
				ELSE ''
			END +
			CASE WHEN mentPerson.MiddleName IS NOT NULL THEN ' ' + mentPerson.MiddleName
				ELSE ''
			END AS MenteeFullName
		, tp.YearStarted AS TrainingPeriodYearStarted
		, tp.YearEnded AS TrainingPeriodYearEnded
		, docLvlMent.Id AS MenteeTypeId
		, docLvlMent.LevelName AS MenteeType
		, org.Id AS MenteeDepartmentId
		, org.DisplayName AS MenteeDepartment
		, mentee.IsTrainingGrantEligible

	FROM 
		dbo.Funding fund
		INNER JOIN dbo.TrainingGrant tgrant ON tgrant.FundingId = fund.Id
			AND tgrant.IsDeleted = 0
		INNER JOIN dbo.FundingFaculty fundFac ON fundFac.FundingId = fund.Id
			AND fundFac.IsDeleted = 0
		INNER JOIN dbo.Faculty fac ON fac.Id = fundFac.FacultyId
			AND fac.IsDeleted = 0
		INNER JOIN dbo.Person facPerson ON facPerson.PersonId = fac.PersonId
			AND facPerson.IsDeleted = 0
		INNER JOIN dbo.TrainingPeriodFaculty lnkFac2TP ON lnkFac2TP.FacultyId = fac.Id
		INNER JOIN dbo.TrainingPeriod tp ON tp.Id = lnkFac2TP.TrainingPeriodId
			AND tp.IsDeleted = 0
		INNER JOIN dbo.Mentee mentee ON mentee.PersonId = tp.PersonId
			AND mentee.IsDeleted = 0
		INNER JOIN dbo.Person mentPerson ON mentPerson.PersonId = mentee.PersonId
			AND mentPerson.IsDeleted = 0
		LEFT OUTER JOIN dbo.DoctoralLevel docLvlTG ON docLvlTG.Id = tgrant.DoctoralLevelId
		LEFT OUTER JOIN dbo.DoctoralLevel docLvlMent ON docLvlMent.Id = tp.DoctoralLevelId
		LEFT OUTER JOIN dbo.Organization org ON org.Id = mentee.DepartmentId
			AND org.IsDeleted = 0

		
	WHERE 
		fund.IsDeleted = 0
		AND tgrant.Id IS NOT NULL
		


GO


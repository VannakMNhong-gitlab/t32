/****** Object:  StoredProcedure [Reports].[Sp_MenteeGeneralInfo]    Script Date: 4/28/2016 3:45:10 PM ******/
DROP PROCEDURE [Reports].[Sp_MenteeGeneralInfo]
GO

/****** Object:  StoredProcedure [Reports].[Sp_MenteeGeneralInfo]    Script Date: 4/28/2016 3:45:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 04/14/2016
-- Description:	Used by Report: Mentee
-- =============================================
CREATE PROCEDURE [Reports].[Sp_MenteeGeneralInfo]
	@MenteeId UNIQUEIDENTIFIER

AS
BEGIN

	SET NOCOUNT ON;

	IF @MenteeId IS NOT NULL
	BEGIN

		SELECT DISTINCT
			mentee.Id AS Id
			, mentee.PersonId
			, p.FirstName
			, p.MiddleName
			, p.LastName
			, mentee.StudentId
			, instAssoc.Name AS InstitutionAssociation
			, dept.DisplayName AS Department
			, mentee.IsTrainingGrantEligible
			, p.IsUnderrepresentedMinority
			, p.IsIndividualWithDisabilities
			, p.IsFromDisadvantagedBkgd

		FROM 
			dbo.Mentee mentee
			INNER JOIN dbo.Person p ON p.PersonId = mentee.PersonId
				AND p.IsDeleted = 0
			LEFT OUTER JOIN dbo.Organization dept ON dept.Id = mentee.DepartmentId
				AND dept.IsDeleted = 0
			LEFT OUTER JOIN dbo.InstitutionAssociation instAssoc ON instAssoc.Id = mentee.InstitutionAssociationId

		WHERE
			mentee.IsDeleted = 0
			AND mentee.Id = @MenteeId
	
	END

	SET NOCOUNT OFF
END

GO


/****** Object:  View [Search].[vw_Ethnicities]    Script Date: 5/2/2016 3:19:39 PM ******/
DROP VIEW [Search].[vw_Ethnicities]
GO

/****** Object:  View [Search].[vw_Ethnicities]    Script Date: 5/2/2016 3:19:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [Search].[vw_Ethnicities] 
AS 

	SELECT
		eth.Id AS EthnicityId
		, eth.Name AS Ethnicity
		, (vw.TotalEthnicities) AS TotalEthnicityUsage

	FROM 
		dbo.Ethnicity eth
		LEFT OUTER JOIN Search.vw_UsageOfEthnicities vw ON vw.EthnicityId = eth.Id

	WHERE
		eth.IsDeleted = 0


GO


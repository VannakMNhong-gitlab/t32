/****** Object:  StoredProcedure [Reports].[Sp_T32Table2]    Script Date: 1/26/2016 3:43:53 PM ******/
DROP PROCEDURE [Reports].[Sp_T32Table2]
GO

/****** Object:  StoredProcedure [Reports].[Sp_T32Table2]    Script Date: 1/26/2016 3:43:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 04/17/2015
-- Description:	Used by T32 TABLE 2 Report
-- =============================================
CREATE PROCEDURE [Reports].[Sp_T32Table2]
	@TrainingGrantId uniqueidentifier
AS
BEGIN

	SET NOCOUNT ON;

	SELECT DISTINCT
		vw.Id AS FacultyId
		, vw.PersonId
		, vw.FullName
		, dbo.GetDegreeAbbrevsForFaculty(vw.Id) AS DegreeList
		, vw.[Rank]
		, vw.PrimaryOrganization
		, vw.SecondaryOrganization
		, fac.PrimaryRoleId
		, rolesP.Name AS PrimaryRole
		, fac.SecondaryRoleId
		, rolesS.Name AS SecondaryRole
		, dbo.GetResearchInterestsForTGFaculty(tgrant.Id, vw.Id) AS ResearchInterests

	FROM 
		dbo.FundingFaculty fac
		INNER JOIN dbo.TrainingGrant tgrant ON tgrant.FundingId = fac.FundingId
			AND tgrant.IsDeleted = 0
		INNER JOIN Search.vw_FacultyDemographics vw ON vw.Id = fac.FacultyId
		LEFT OUTER JOIN dbo.FundingRole rolesP ON rolesP.Id = fac.PrimaryRoleId
		LEFT OUTER JOIN dbo.FundingRole rolesS ON rolesS.Id = fac.SecondaryRoleId

	WHERE 
		fac.IsDeleted = 0
		AND ((@TrainingGrantId IS NULL) OR (tgrant.Id = @TrainingGrantId))

		--AND @TrainingGrantId = 'D159F84F-5D07-4462-BBC1-F9A8A3714E45'

	SET NOCOUNT OFF
END

GO


/****** Object:  View [Search].[vw_Organizations]    Script Date: 6/2/2015 10:27:43 AM ******/
DROP VIEW [Search].[vw_Organizations]
GO

/****** Object:  View [Search].[vw_Organizations]    Script Date: 6/2/2015 10:27:43 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_Organizations] 
AS 
	SELECT DISTINCT
		org.Id
		, org.OrganizationId
		, org.DisplayName
		, org.HRName
		, org.OrganizationTypeId
		, orgType.OrganizationType
		, org.ParentOrganizationId
		, parentOrg.OrganizationId AS ParentOrgId
		, parentOrg.DisplayName AS ParentOrgDisplayName
		, parentOrg.HRName AS ParentOrgHRName
		, parentOrg.OrganizationTypeId AS ParentOrgTypeId
		, parentOrgType.OrganizationType AS ParentOrgType
		, (vw.TotalApplicantDepts + vw.TotalFacultyOrgs + vw.TotalMenteeDepts 
			+ vw.TotalTrainingGrantDepts) AS TotalOrganizationUsage

	FROM 
		dbo.Organization org
		LEFT OUTER JOIN dbo.OrganizationType orgType ON orgType.Id = org.OrganizationTypeId
		LEFT OUTER JOIN dbo.Organization parentOrg ON parentOrg.OrganizationId = org.ParentOrganizationId
			AND parentOrg.IsDeleted = 0
		LEFT OUTER JOIN dbo.OrganizationType parentOrgType ON parentOrgType.Id = parentOrg.OrganizationTypeId
		LEFT OUTER JOIN Search.vw_UsageOfOrganizations vw ON vw.OrganizationId = org.Id

	WHERE 
		org.IsDeleted = 0



GO


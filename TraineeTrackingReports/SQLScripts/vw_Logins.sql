/****** Object:  View [Search].[vw_Logins]    Script Date: 5/2/2016 3:03:27 PM ******/
DROP VIEW [Search].[vw_Logins]
GO

/****** Object:  View [Search].[vw_Logins]    Script Date: 5/2/2016 3:03:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [Search].[vw_Logins]
AS
	SELECT 
		logins.LoginId
		, logins.ExternalId
		, logins.Username
		, logins.PersonId
		, ppl.FirstName
		, ppl.LastName
		, ppl.MiddleName
		, logins.DateCreated
		, logins.DateLastUpdated
		, logins.LastUpdatedBy
	FROM dbo.Logins logins
		INNER JOIN dbo.Person ppl ON ppl.PersonId = logins.PersonId
			AND ppl.IsDeleted = 0
	WHERE
		logins.IsActive = 1





GO


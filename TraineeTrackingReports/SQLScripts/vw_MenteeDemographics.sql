/****** Object:  View [Search].[vw_MenteeDemographics]    Script Date: 5/2/2016 4:45:23 PM ******/
DROP VIEW [Search].[vw_MenteeDemographics]
GO

/****** Object:  View [Search].[vw_MenteeDemographics]    Script Date: 5/2/2016 4:45:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- Fixed FullName to show NULL instead of '' if there's no First or Last Name
CREATE VIEW [Search].[vw_MenteeDemographics]
AS
	SELECT DISTINCT TOP 100 PERCENT
		mentee.Id AS Id
		, mentee.PersonId
		, p.FirstName
		, p.MiddleName
		, p.LastName
		, CASE 
			WHEN p.LastName IS NULL AND p.FirstName IS NULL THEN NULL
			WHEN p.LastName IS NOT NULL THEN p.LastName
				ELSE ''
			END +
			CASE WHEN p.FirstName IS NOT NULL THEN ', ' + p.FirstName
				ELSE ''
			END +
			CASE WHEN p.MiddleName IS NOT NULL THEN ' ' + p.MiddleName
				ELSE ''
			END AS FullName
		, p.DisplayId
		, mentee.StudentId
		, mentee.IsTrainingGrantEligible
		, mentee.WasRecruitedToLab
		, p.IsFromDisadvantagedBkgd
		, p.IsIndividualWithDisabilities
		, p.IsUnderrepresentedMinority
		, dept.Id AS DepartmentId
		, dept.DisplayName AS Department
		, instAssoc.Name AS InstitutionAssociation
		, mentee.DateLastUpdated AS MenteeDateLastUpdated
		, mentee.LastUpdatedBy AS MenteeLastUpdatedBy
		, updLoginsMentee.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLoginsMentee.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLoginsMentee.MiddleName)), 1) + '. '
			ELSE '' END + updLoginsMentee.LastName AS MenteeLastUpdatedByName
		, (SELECT TOP 1 perprog.ProgramId 
			FROM dbo.TrainingPeriod per
				LEFT OUTER JOIN dbo.TrainingPeriodProgram perprog ON perprog.TrainingPeriodId = per.Id
			WHERE per.PersonId = mentee.PersonId
				AND per.IsDeleted = 0
			ORDER BY per.YearEnded DESC
			) AS ProgramId
		, (SELECT TOP 1 prog.Title 
			FROM dbo.TrainingPeriod per
				LEFT OUTER JOIN dbo.TrainingPeriodProgram perprog ON perprog.TrainingPeriodId = per.Id
				LEFT OUTER JOIN dbo.Program prog ON prog.Id = perprog.ProgramId
			WHERE per.PersonId = mentee.PersonId
				AND per.IsDeleted = 0
			ORDER BY per.YearEnded DESC
			) AS ProgramTitle
		--, prog.Title AS Program
		--, docLvl.LevelName AS MenteeType
		--, medRank.[Rank] AS MedicalRank
		--, fac.Id AS FacultyId
		--, mentor.PersonId AS MentorPersonId
		--, mentor.LastName AS MentorLName
		--, mentor.FirstName AS MentorFName
		--, mentor.MiddleName AS MentorMName
		--, CASE 
		--		WHEN mentor.LastName IS NULL AND mentor.FirstName IS NULL THEN NULL
		--		WHEN mentor.LastName IS NOT NULL THEN mentor.LastName
		--			ELSE ''
		--	END +
		--	CASE WHEN mentor.FirstName IS NOT NULL THEN ', ' + mentor.FirstName
		--		ELSE ''
		--	END +
		--	CASE WHEN mentor.MiddleName IS NOT NULL THEN ' ' + mentor.MiddleName
		--		ELSE ''
		--	END AS MentorFullName
		--, mentorRole.[Role] AS MentorRole
		--, lnkMentee2Prog.DateStarted
		--, lnkMentee2Prog.DateEnded
		--, email.IsPrimary
		, CASE WHEN email.IsPrimary IS NULL
				THEN 0
				ELSE email.IsPrimary
			END AS IsPrimaryEmail
		, email.EmailAddress
		--, CASE WHEN email.IsPrimary = 1
		--		THEN email.EmailAddress
		--		ELSE ''
		--	END AS PrimaryEmailAddress
		--, CASE WHEN email.IsPrimary = 0
		--	THEN email.EmailAddress
		--	ELSE ''
		--END AS EmailAddress
		, CASE WHEN addy.IsPrimary = 1
				THEN addy.AddressLine1 + 
					CASE WHEN addy.AddressLine2 IS NOT NULL
						THEN ' ' + addy.AddressLine2
						ELSE ''
					END + 
					CASE WHEN addy.AddressLine3 IS NOT NULL
						THEN ' ' + addy.AddressLine3
						ELSE ''
					END
					+ ', ' + addy.City
					+ ', ' + addystate.Abbreviation
					+ '  ' + addy.PostalCode
				ELSE ''
			END AS MailingAddress
		, CASE WHEN addy.IsPrimary = 1 THEN addy.AddressLine1 END AS AddressLine1
		, CASE WHEN addy.IsPrimary = 1 THEN addy.AddressLine2 END AS AddressLine2
		, CASE WHEN addy.IsPrimary = 1 THEN addy.AddressLine3 END AS AddressLine3
		, CASE WHEN addy.IsPrimary = 1 THEN addy.City END AS AddressCity
		, CASE WHEN addy.IsPrimary = 1 THEN addy.StateId END AS AddressStateId
		, CASE WHEN addy.IsPrimary = 1 THEN addystate.Abbreviation END AS AddressState
		, CASE WHEN addy.IsPrimary = 1 THEN addy.PostalCode END AS AddressPostalCode
		, CASE WHEN phone.IsPrimary = 0
			THEN phone.PhoneNumber
			ELSE ''
		END AS PhoneNumber
	FROM dbo.Mentee mentee
		INNER JOIN dbo.Person p ON p.PersonId = mentee.PersonId
			AND p.IsDeleted = 0
		--LEFT OUTER JOIN dbo.DoctoralLevel docLvl ON docLvl.Id = mentee.DoctoralLevelId
		--LEFT OUTER JOIN dbo.MedicalRank medRank ON medRank.Id = mentee.MedicalRankId
		LEFT OUTER JOIN dbo.Organization dept ON dept.Id = mentee.DepartmentId
			AND dept.IsDeleted = 0
		LEFT OUTER JOIN dbo.InstitutionAssociation instAssoc ON instAssoc.Id = mentee.InstitutionAssociationId
		--LEFT OUTER JOIN dbo.TrainingPeriod per ON per.PersonId = mentee.PersonId
		--	AND per.IsDeleted = 0
		--LEFT OUTER JOIN dbo.TrainingPeriodProgram perprog ON perprog.TrainingPeriodId = per.Id
		--LEFT OUTER JOIN dbo.Program prog ON prog.Id = perprog.ProgramId
		--LEFT OUTER JOIN dbo.Faculty fac ON fac.Id = lnkFac2Mentee.FacultyId
		--	AND fac.IsDeleted = 0
		--LEFT OUTER JOIN dbo.Person mentor ON mentor.PersonId = fac.PersonId
		--	AND mentor.IsDeleted = 0
		--LEFT OUTER JOIN dbo.FacultyRole mentorRole ON mentorRole.Id = lnkFac2Mentee.FacultyRoleId
		LEFT OUTER JOIN dbo.ContactEmail email ON email.PersonId = p.PersonId
			AND email.IsPrimary = 1
			AND email.IsDeleted = 0
		LEFT OUTER JOIN dbo.ContactAddress addy ON addy.PersonId = p.PersonId
			AND addy.IsDeleted = 0
		LEFT OUTER JOIN dbo.StateProvince addystate ON addystate.Id = addy.StateId
			AND addystate.IsDeleted = 0
		LEFT OUTER JOIN dbo.ContactPhone phone ON phone.PersonId = p.PersonId
			AND phone.IsDeleted = 0
		LEFT OUTER JOIN Search.vw_Logins updLoginsMentee ON updLoginsMentee.Username = mentee.LastUpdatedBy
	
	WHERE
		mentee.IsDeleted = 0

	ORDER BY
		FullName



GO


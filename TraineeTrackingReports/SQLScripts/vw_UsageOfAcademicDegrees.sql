/****** Object:  View [Search].[vw_UsageOfAcademicDegrees]    Script Date: 6/2/2015 10:26:33 AM ******/
DROP VIEW [Search].[vw_UsageOfAcademicDegrees]
GO

/****** Object:  View [Search].[vw_UsageOfAcademicDegrees]    Script Date: 6/2/2015 10:26:33 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_UsageOfAcademicDegrees] 
AS 
	SELECT 
		deg.Id AS DegreeId
		, deg.Name AS DegreeName
		, (SELECT Count(AcademicDegreeId) FROM dbo.AcademicHistory achist 
			WHERE achist.AcademicDegreeId = deg.Id
				AND achist.IsDeleted = 0) AS TotalAcademicHistory
		, (SELECT Count(AcademicDegreeId) FROM dbo.FacultyAcademicData facac 
			WHERE facac.AcademicDegreeId = deg.Id
				AND facac.IsDeleted = 0) AS TotalFacultyAcademics
		, (SELECT Count(AcademicDegreeId) FROM dbo.TrainingPeriod tp 
			WHERE tp.AcademicDegreeId = deg.Id
				AND tp.IsDeleted = 0) AS TotalTrainingPeriods

	FROM 
		dbo.AcademicDegree deg

	WHERE 
		deg.IsDeleted = 0

	GROUP BY
		deg.Id
		, deg.Name



GO


/****** Object:  StoredProcedure [Reports].[Sp_GetT32List]    Script Date: 6/10/2015 11:52:31 AM ******/
DROP PROCEDURE [Reports].[Sp_GetT32List]
GO

/****** Object:  StoredProcedure [Reports].[Sp_GetT32List]    Script Date: 6/10/2015 11:52:31 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 04/16/2015
-- Description:	Used by ALL T32 Table Reports
-- =============================================
CREATE PROCEDURE [Reports].[Sp_GetT32List]
	@IsRenewal INT,
	@DoctoralLevel INT

AS
BEGIN

	SET NOCOUNT ON;

	SET @IsRenewal = ISNULL(@IsRenewal, 2)

	SELECT DISTINCT
		tg.Id
		, tg.FundingId
		, fund.IsRenewal
		, fund.DisplayId
		, dbo.StripHTML_Loop(fund.Title) AS Title
		, fund.GRTNumber

	FROM 
		dbo.TrainingGrant tg
		INNER JOIN dbo.Funding fund ON fund.Id = tg.FundingId
			AND fund.IsDeleted = 0

	WHERE
		tg.IsDeleted = 0
		AND ((@IsRenewal IS NULL OR @IsRenewal = 2) OR (fund.IsRenewal = @IsRenewal))
		AND ((@DoctoralLevel IS NULL OR @DoctoralLevel = 0) 
			OR (@DoctoralLevel = 1 AND tg.DoctoralLevelId IN (1,3))
			OR (@DoctoralLevel = 2 AND tg.DoctoralLevelId IN (2,3))
			OR (tg.DoctoralLevelId = @DoctoralLevel))

		

	SET NOCOUNT OFF
END

GO


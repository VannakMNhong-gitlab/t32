/****** Object:  View [Search].[vw_Countries]    Script Date: 6/2/2015 10:31:54 AM ******/
DROP VIEW [Search].[vw_Countries]
GO

/****** Object:  View [Search].[vw_Countries]    Script Date: 6/2/2015 10:31:54 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_Countries] 
AS 

	SELECT
		c.Id AS CountryId
		, c.Name AS Country
		, (vw.TotalContactAddress + vw.TotalInstitutions
			+ vw.TotalStateProvince) AS TotalCountryUsage

	FROM 
		dbo.Country c 
		LEFT OUTER JOIN Search.vw_UsageOfCountries vw ON vw.CountryId = c.Id

	WHERE 
		c.IsDeleted = 0
		


GO


/****** Object:  StoredProcedure [Reports].[Sp_FundingCostInfo]    Script Date: 5/2/2016 2:54:14 PM ******/
DROP PROCEDURE [Reports].[Sp_FundingCostInfo]
GO

/****** Object:  StoredProcedure [Reports].[Sp_FundingCostInfo]    Script Date: 5/2/2016 2:54:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 04/28/2016
-- Description:	Used by Report: Funding
-- =============================================
CREATE PROCEDURE [Reports].[Sp_FundingCostInfo]
	@FundingId UNIQUEIDENTIFIER

AS
BEGIN

	SET NOCOUNT ON;

	IF @FundingId IS NOT NULL
	BEGIN

		SELECT DISTINCT
			fund.Id AS FundingId
			, fund.FundingStatusId
			, costs.Id AS FundingCostId
			, dbo.StripHTML_Loop(Title) AS Title
			, costs.CostYear
			, costs.DateStarted AS DateCostsStarted
			, costs.DateEnded AS DateCostsEnded
			, costs.BudgetPeriodStatusId
			, budgstat.Name AS BudgetPeriodStatus
			, costs.CurrentYearDirectCosts
			, costs.TotalDirectCosts

		FROM 
			dbo.Funding fund
			LEFT OUTER JOIN dbo.FundingDirectCost costs ON costs.FundingId = fund.Id
			LEFT OUTER JOIN dbo.TrainingGrant tgrant ON tgrant.FundingId = fund.Id
			LEFT OUTER JOIN dbo.BudgetPeriodStatus budgstat ON budgstat.Id = costs.BudgetPeriodStatusId	

		WHERE 
			fund.IsDeleted = 0
			AND costs.IsDeleted = 0
			AND tgrant.Id IS NULL
			AND fund.Id = @FundingId

	
	END

	SET NOCOUNT OFF
END

GO


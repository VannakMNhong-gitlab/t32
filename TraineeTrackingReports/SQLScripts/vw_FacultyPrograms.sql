/****** Object:  View [Search].[vw_FacultyPrograms]    Script Date: 6/2/2015 10:25:13 AM ******/
DROP VIEW [Search].[vw_FacultyPrograms]
GO

/****** Object:  View [Search].[vw_FacultyPrograms]    Script Date: 6/2/2015 10:25:13 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [Search].[vw_FacultyPrograms] 
AS 
	SELECT DISTINCT
		fac.Id AS FacultyId
		, fac.PersonId AS FacultyPersonId
		, p.FirstName
		, p.MiddleName
		, p.LastName
		, CASE 
			WHEN p.LastName IS NULL AND p.FirstName IS NULL THEN NULL
			WHEN p.LastName IS NOT NULL THEN p.LastName
				ELSE ''
			END +
			CASE WHEN p.FirstName IS NOT NULL THEN ', ' + p.FirstName
				ELSE ''
			END +
			CASE WHEN p.MiddleName IS NOT NULL THEN ' ' + p.MiddleName
				ELSE ''
			END AS FullName
		, fac.EmployeeId
		, facProg.ProgramId
		, prog.DisplayId AS ProgramDisplayId
		, prog.Title AS ProgramTitle

	FROM dbo.FacultyProgram facProg
		INNER JOIN dbo.Faculty fac ON fac.Id = facProg.FacultyId
			AND fac.IsDeleted = 0
		INNER JOIN dbo.Person p ON p.PersonId = fac.PersonId
			AND p.IsDeleted = 0
		INNER JOIN dbo.Program prog ON prog.Id = facProg.ProgramId
			AND prog.IsDeleted = 0



GO


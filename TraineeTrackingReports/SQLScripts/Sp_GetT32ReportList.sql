/****** Object:  StoredProcedure [Reports].[Sp_GetT32ReportList]    Script Date: 7/28/2015 3:47:32 PM ******/
DROP PROCEDURE [Reports].[Sp_GetT32ReportList]
GO

/****** Object:  StoredProcedure [Reports].[Sp_GetT32ReportList]    Script Date: 7/28/2015 3:47:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 05/13/2015
-- Description:	Used by T32 ALL TABLES Report
-- =============================================
CREATE PROCEDURE [Reports].[Sp_GetT32ReportList]
	@TrainingGrantId uniqueidentifier
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @DoctoralLevel INT, @IsRenewal BIT

	SET @DoctoralLevel = (SELECT DoctoralLevelId FROM dbo.TrainingGrant
							WHERE Id = @TrainingGrantId)

	SET @IsRenewal = (SELECT IsRenewal 
						FROM dbo.TrainingGrant tg
							INNER JOIN dbo.Funding fund ON fund.Id = tg.FundingId
						WHERE tg.Id = @TrainingGrantId)


	SELECT DISTINCT
		Id
		, Title
		, [Description]

	FROM 
		dbo.Reports rep

	WHERE
		rep.IsDeleted = 0
		AND rep.StatusTypeId = 2
		AND rep.Id != 1
		--AND ((@DoctoralLevel = 1 AND rep.IsPredoc = 1)
		--	OR (@DoctoralLevel = 2 AND rep.IsPostdoc = 1)
		--	OR (@DoctoralLevel = 3 AND (rep.IsPredoc = 1 AND rep.IsPostdoc = 1))
		--)
		--AND (@IsRenewal = 1 AND (rep.IsNewOnly = 0 AND rep.IsRenewalOnly = 1))

	SET NOCOUNT OFF
END

GO


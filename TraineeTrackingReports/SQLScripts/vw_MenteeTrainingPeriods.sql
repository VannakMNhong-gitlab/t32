/****** Object:  View [Search].[vw_MenteeTrainingPeriods]    Script Date: 5/2/2016 4:46:57 PM ******/
DROP VIEW [Search].[vw_MenteeTrainingPeriods]
GO

/****** Object:  View [Search].[vw_MenteeTrainingPeriods]    Script Date: 5/2/2016 4:46:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_MenteeTrainingPeriods] 
AS 
	SELECT DISTINCT TOP 100 PERCENT
		tp.Id AS TrainingPeriodId
		, tp.PersonId AS MenteePersonId
		, mentee.Id AS MenteeId
		, mentee.StudentId AS MenteeStudentId
		, pMentee.FirstName
		, pMentee.MiddleName
		, pMentee.LastName
		, CASE 
			WHEN pMentee.LastName IS NULL AND pMentee.FirstName IS NULL THEN NULL
			WHEN pMentee.LastName IS NOT NULL THEN pMentee.LastName
				ELSE ''
			END +
			CASE WHEN pMentee.FirstName IS NOT NULL THEN ', ' + pMentee.FirstName
				ELSE ''
			END +
			CASE WHEN pMentee.MiddleName IS NOT NULL THEN ' ' + pMentee.MiddleName
				ELSE ''
			END AS MenteeFullName
		, docLvl.Id AS DoctoralLevelId
		, docLvl.LevelName AS DoctoralLevel
		, tp.InstitutionId
		, inst.InstitutionIdentifier
		, inst.Name AS InstitutionName
		, inst.City AS InstitutionCity
		, inst.StateId AS InstitutionStateId
		, stp.Abbreviation AS InstitutionStateAbbreviation
		, stp.FullName AS InstitutionStateFullName
		, inst.CountryId AS InstitutionCountryId
		, c.Name AS InstitutionCountry
		, prog.Id AS ProgramId
		, prog.Title AS ProgramTitle
		, tp.YearStarted
		, tp.YearEnded
		, tp.AcademicDegreeId AS CompletedDegreeId
		, degree.Name AS CompletedDegree
		, tp.DegreeSoughtId
		, degSought.Name AS DegreeSought
		, tp.ResearchProjectTitle
		, tp.DateLastUpdated
		, tp.LastUpdatedBy
		, updLogins.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLogins.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLogins.MiddleName)), 1) + '. '
			ELSE '' END + updLogins.LastName AS LastUpdatedByName

	FROM dbo.TrainingPeriod tp
		LEFT OUTER JOIN dbo.Mentee mentee ON mentee.PersonId = tp.PersonId 
			AND mentee.IsDeleted = 0
		INNER JOIN dbo.Person pMentee ON pMentee.PersonId = mentee.PersonId
			AND pMentee.IsDeleted = 0
		LEFT OUTER JOIN dbo.DoctoralLevel docLvl ON docLvl.Id = tp.DoctoralLevelId
		LEFT OUTER JOIN dbo.TrainingPeriodProgram tpprog ON tpprog.TrainingPeriodId = tp.Id
		LEFT OUTER JOIN dbo.Program prog ON prog.Id = tpprog.ProgramId
			AND prog.IsDeleted = 0
		LEFT OUTER JOIN dbo.Institution inst ON inst.Id = tp.InstitutionId
			AND inst.IsDeleted = 0
		LEFT OUTER JOIN dbo.StateProvince stp ON stp.Id = inst.StateId
			AND stp.IsDeleted = 0
		LEFT OUTER JOIN dbo.Country c ON c.Id = inst.CountryId
			OR c.Id = stp.CountryId
		LEFT OUTER JOIN dbo.AcademicDegree degree ON degree.Id = tp.AcademicDegreeId
			AND degree.IsDeleted = 0
		LEFT OUTER JOIN dbo.AcademicDegree degSought ON degSought.Id = tp.DegreeSoughtId
			AND degSought.IsDeleted = 0
		LEFT OUTER JOIN Search.vw_Logins updLogins ON updLogins.Username = tp.LastUpdatedBy

	WHERE 
		tp.IsDeleted = 0

	ORDER BY MenteeStudentId
	






GO


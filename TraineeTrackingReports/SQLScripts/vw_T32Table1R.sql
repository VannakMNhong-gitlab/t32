/****** Object:  View [Reports].[vw_T32Table1R]    Script Date: 9/15/2015 1:09:29 PM ******/
DROP VIEW [Reports].[vw_T32Table1R]
GO

/****** Object:  View [Reports].[vw_T32Table1R]    Script Date: 9/15/2015 1:09:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [Reports].[vw_T32Table1R] 
AS 

	SELECT DISTINCT 
		tgrant.Id AS TrainingGrantId
		, fund.IsRenewal
		, tgrant.DoctoralLevelId
		, docLvl.LevelName AS DoctoralLevel
		, 1 AS Column1ItemTypeId
		, 'Program' AS Column1ItemType
		, prog.DisplayId AS Column1ItemId
		, prog.Title AS Column1ItemName
		, (SELECT Count(FacultyId) FROM Search.vw_TrainingGrantFacultyWithPrograms vw
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.FacultyProgramId = prog.Id) AS TotalParticipatingFaculty
		, (SELECT Count(DISTINCT vw.MenteePersonId) 
			FROM Search.vw_MenteeTrainingPeriods vw
			WHERE vw.ProgramId = prog.Id
				AND vw.DoctoralLevelId = 1) AS TotalPredocMentees
		, (SELECT Count(DISTINCT vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantMenteesWithPrograms vw
			WHERE vw.MenteeProgramId = prog.Id
				AND vw.MenteeTypeId = 1) AS TotalPredocMenteesOnAnyTG
		, (SELECT Count(DISTINCT vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantMenteesWithPrograms vw
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.MenteeProgramId = prog.Id
				AND vw.MenteeTypeId = 1) AS TotalPredocMenteesOnTG
		, (SELECT Count(DISTINCT vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantMenteesWithPrograms vw
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.MenteeProgramId = prog.Id
				AND vw.MenteeTypeId = 1
				AND vw.IsTrainingGrantEligible = 1) AS TotalPredocTGEMenteesOnTG
		, (SELECT Count(DISTINCT vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantMenteesWithPrograms vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.MenteeProgramId = prog.Id
				AND vw.MenteeTypeId = 1
				AND ppl.IsUnderrepresentedMinority = 1) AS TotalPredocURMMenteesOnTG
		, (SELECT Count(DISTINCT vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantMenteesWithPrograms vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.MenteeProgramId = prog.Id
				AND vw.MenteeTypeId = 1
				AND ppl.IsIndividualWithDisabilities = 1) AS TotalPredocDisabilitiesMenteesOnTG
		, (SELECT Count(DISTINCT ppl.PersonId) 
			FROM Search.vw_TrainingGrantMenteesWithPrograms vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.MenteeProgramId = prog.Id
				AND vw.MenteeTypeId = 1
				AND ppl.IsFromDisadvantagedBkgd = 1) AS TotalPredocDisadvantagedMenteesOnTG
		, (SELECT Count(DISTINCT vw.MenteePersonId) 
			FROM Search.vw_MenteeTrainingPeriods vw
			WHERE vw.ProgramId = prog.Id
				AND vw.DoctoralLevelId = 2) AS TotalPostdocMentees
		, (SELECT Count(DISTINCT vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantMenteesWithPrograms vw
			WHERE vw.MenteeProgramId = prog.Id
				AND vw.MenteeTypeId = 2) AS TotalPostdocMenteesOnAnyTG
		, (SELECT Count(DISTINCT vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantMenteesWithPrograms vw
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.MenteeProgramId = prog.Id
				AND vw.MenteeTypeId = 2) AS TotalPostdocMenteesOnTG
		, (SELECT Count(DISTINCT vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantMenteesWithPrograms vw
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.MenteeProgramId = prog.Id
				AND vw.MenteeTypeId = 2
				AND vw.IsTrainingGrantEligible = 1) AS TotalPostdocTGEMenteesOnTG
		, (SELECT Count(DISTINCT vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantMenteesWithPrograms vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.MenteeProgramId = prog.Id
				AND vw.MenteeTypeId = 2
				AND ppl.IsUnderrepresentedMinority = 1) AS TotalPostdocURMMenteesOnTG
		, (SELECT Count(DISTINCT vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantMenteesWithPrograms vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.MenteeProgramId = prog.Id
				AND vw.MenteeTypeId = 2
				AND ppl.IsIndividualWithDisabilities = 1) AS TotalPostdocDisabilitiesMenteesOnTG
		, (SELECT Count(DISTINCT ppl.PersonId) 
			FROM Search.vw_TrainingGrantMenteesWithPrograms vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.MenteeProgramId = prog.Id
				AND vw.MenteeTypeId = 2
				AND ppl.IsFromDisadvantagedBkgd = 1) AS TotalPostdocDisadvantagedMenteesOnTG

		, (SELECT DISTINCT Count(vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantDistinctTrainees vw
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.ProgramId = prog.Id
				AND vw.MenteeTypeId = 1) AS TotalPredocTrainees
		, (SELECT DISTINCT Count(vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantDistinctTrainees vw
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.ProgramId = prog.Id
				AND vw.MenteeTypeId = 1
				AND vw.IsTrainingGrantEligible = 1) AS TotalPredocTGETrainees
		, (SELECT DISTINCT Count(ppl.PersonId) 
			FROM Search.vw_TrainingGrantDistinctTrainees vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.ProgramId = prog.Id
				AND vw.MenteeTypeId = 1
				AND ppl.IsUnderrepresentedMinority = 1) AS TotalPredocURMTrainees
		, (SELECT DISTINCT Count(ppl.PersonId) 
			FROM Search.vw_TrainingGrantDistinctTrainees vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.ProgramId = prog.Id
				AND vw.MenteeTypeId = 1
				AND ppl.IsIndividualWithDisabilities = 1) AS TotalPredocDisabilitiesTrainees
		, (SELECT DISTINCT Count(ppl.PersonId) 
			FROM Search.vw_TrainingGrantDistinctTrainees vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.ProgramId = prog.Id
				AND vw.MenteeTypeId = 1
				AND ppl.IsFromDisadvantagedBkgd = 1) AS TotalPredocDisadvantagedTrainees
		, (SELECT DISTINCT Count(vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantDistinctTrainees vw
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.ProgramId = prog.Id
				AND vw.MenteeTypeId = 2) AS TotalPostdocTrainees
		, (SELECT DISTINCT Count(vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantDistinctTrainees vw
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.ProgramId = prog.Id
				AND vw.MenteeTypeId = 2
				AND vw.IsTrainingGrantEligible = 1) AS TotalPostdocTGETrainees
		, (SELECT DISTINCT Count(ppl.PersonId) 
			FROM Search.vw_TrainingGrantDistinctTrainees vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.ProgramId = prog.Id
				AND vw.MenteeTypeId = 2
				AND ppl.IsUnderrepresentedMinority = 1) AS TotalPostdocURMTrainees
		, (SELECT DISTINCT Count(ppl.PersonId) 
			FROM Search.vw_TrainingGrantDistinctTrainees vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.ProgramId = prog.Id
				AND vw.MenteeTypeId = 2
				AND ppl.IsIndividualWithDisabilities = 1) AS TotalPostdocDisabilitiesTrainees
		, (SELECT DISTINCT Count(ppl.PersonId) 
			FROM Search.vw_TrainingGrantDistinctTrainees vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.ProgramId = prog.Id
				AND vw.MenteeTypeId = 2
				AND ppl.IsFromDisadvantagedBkgd = 1) AS TotalPostdocDisadvantagedTrainees		

	FROM dbo.TrainingGrant tgrant
		INNER JOIN dbo.Funding fund ON fund.Id = tgrant.FundingId
			AND fund.IsDeleted = 0
		INNER JOIN dbo.DoctoralLevel docLvl ON docLvl.Id = tgrant.DoctoralLevelId
		INNER JOIN dbo.TrainingGrantProgram tgprog ON tgprog.TrainingGrantId = tgrant.Id
		INNER JOIN dbo.Program prog ON prog.Id = tgprog.ProgramId
			AND prog.IsDeleted = 0
		LEFT OUTER JOIN dbo.FundingFaculty ffac ON ffac.FundingId = tgrant.FundingId
			AND ffac.IsDeleted = 0

	WHERE 
		tgrant.IsDeleted = 0

		
	UNION

	SELECT DISTINCT
		tgrant.Id AS TrainingGrantId
		, fund.IsRenewal
		, tgrant.DoctoralLevelId
		, docLvl.LevelName AS DoctoralLevel
		, 2 AS Column1ItemTypeId
		, 'Department' AS Column1ItemType
		, tgdept.OrganizationId AS Column1ItemId
		, dept.DisplayName AS Column1ItemName
		, (SELECT Count(FacultyId) FROM Search.vw_TrainingGrantFacultyWithPrograms vw
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.FacultyPrimaryOrgId = dept.Id) AS TotalParticipatingFaculty
		-- === TODO: ISSUE! HOW CAN WE TELL WHAT DOCLVL MENTEE IS CURRENTLY? DATES ON TP?? ===
		, (SELECT Count(DISTINCT vw.PersonId) 
			FROM Search.vw_MenteeDemographics vw
			WHERE vw.DepartmentId = dept.Id
				--AND vw.DoctoralLevelId = 1
				) AS TotalPredocMentees
		, (SELECT Count(DISTINCT vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantMenteesWithPrograms vw
			WHERE vw.MenteeDeptId = dept.Id
				AND vw.MenteeTypeId = 1) AS TotalPredocMenteesOnAnyTG
		, (SELECT DISTINCT Count(vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantMentees vw
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.MenteeDepartmentId = dept.Id
				AND vw.MenteeTypeId = 1) AS TotalPredocMenteesOnTG
		, (SELECT DISTINCT Count(vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantMentees vw
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.MenteeDepartmentId = dept.Id
				AND vw.MenteeTypeId = 1
				AND vw.IsTrainingGrantEligible = 1) AS TotalPredocTGEMenteesOnTG
		, (SELECT DISTINCT Count(ppl.PersonId) 
			FROM Search.vw_TrainingGrantMentees vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.MenteeDepartmentId = dept.Id
				AND vw.MenteeTypeId = 1
				AND ppl.IsUnderrepresentedMinority = 1) AS TotalPredocURMMenteesOnTG
		, (SELECT DISTINCT Count(ppl.PersonId) 
			FROM Search.vw_TrainingGrantMentees vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.MenteeDepartmentId = dept.Id
				AND vw.MenteeTypeId = 1
				AND ppl.IsIndividualWithDisabilities = 1) AS TotalPredocDisabilitiesMenteesOnTG
		, (SELECT DISTINCT Count(ppl.PersonId) 
			FROM Search.vw_TrainingGrantMentees vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.MenteeDepartmentId = dept.Id
				AND vw.MenteeTypeId = 1
				AND ppl.IsFromDisadvantagedBkgd = 1) AS TotalPredocDisadvantagedMenteesOnTG
		-- === TODO: ISSUE! HOW CAN WE TELL WHAT DOCLVL MENTEE IS CURRENTLY? DATES ON TP?? ===
		, (SELECT Count(DISTINCT vw.PersonId) 
			FROM Search.vw_MenteeDemographics vw
			WHERE vw.DepartmentId = dept.Id
				--AND vw.DoctoralLevelId = 2
				) AS TotalPostdocMentees
		, (SELECT Count(DISTINCT vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantMenteesWithPrograms vw
			WHERE vw.MenteeDeptId = dept.Id
				AND vw.MenteeTypeId = 2) AS TotalPostdocMenteesOnAnyTG
		, (SELECT DISTINCT Count(vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantMentees vw
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.MenteeDepartmentId = dept.Id
				AND vw.MenteeTypeId = 2) AS TotalPostdocMentees
		, (SELECT DISTINCT Count(vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantMentees vw
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.MenteeDepartmentId = dept.Id
				AND vw.MenteeTypeId = 2
				AND vw.IsTrainingGrantEligible = 1) AS TotalPostdocTGEMenteesOnTG
		, (SELECT DISTINCT Count(ppl.PersonId) 
			FROM Search.vw_TrainingGrantMentees vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.MenteeDepartmentId = dept.Id
				AND vw.MenteeTypeId = 2
				AND ppl.IsUnderrepresentedMinority = 1) AS TotalPostdocURMMenteesOnTG
		, (SELECT DISTINCT Count(ppl.PersonId) 
			FROM Search.vw_TrainingGrantMentees vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.MenteeDepartmentId = dept.Id
				AND vw.MenteeTypeId = 2
				AND ppl.IsIndividualWithDisabilities = 1) AS TotalPostdocDisabilitiesMenteesOnTG
		, (SELECT DISTINCT Count(ppl.PersonId) 
			FROM Search.vw_TrainingGrantMentees vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.MenteeDepartmentId = dept.Id
				AND vw.MenteeTypeId = 2
				AND ppl.IsFromDisadvantagedBkgd = 1) AS TotalPostdocDisadvantagedMenteesOnTG


		, (SELECT DISTINCT Count(vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantDistinctTrainees vw
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.DepartmentId = dept.Id
				AND vw.MenteeTypeId = 1) AS TotalPredocTrainees
		, (SELECT DISTINCT Count(vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantDistinctTrainees vw
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.DepartmentId = dept.Id
				AND vw.MenteeTypeId = 1
				AND vw.IsTrainingGrantEligible = 1) AS TotalPredocTGETrainees
		, (SELECT DISTINCT Count(ppl.PersonId) 
			FROM Search.vw_TrainingGrantDistinctTrainees vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.DepartmentId = dept.Id
				AND vw.MenteeTypeId = 1
				AND ppl.IsUnderrepresentedMinority = 1) AS TotalPredocURMTrainees
		, (SELECT DISTINCT Count(ppl.PersonId) 
			FROM Search.vw_TrainingGrantDistinctTrainees vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.DepartmentId = dept.Id
				AND vw.MenteeTypeId = 1
				AND ppl.IsIndividualWithDisabilities = 1) AS TotalPredocDisabilitiesTrainees
		, (SELECT DISTINCT Count(ppl.PersonId) 
			FROM Search.vw_TrainingGrantDistinctTrainees vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.DepartmentId = dept.Id
				AND vw.MenteeTypeId = 1
				AND ppl.IsFromDisadvantagedBkgd = 1) AS TotalPredocDisadvantagedTrainees
		, (SELECT DISTINCT Count(vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantDistinctTrainees vw
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.DepartmentId = dept.Id
				AND vw.MenteeTypeId = 2) AS TotalPostdocTrainees
		, (SELECT DISTINCT Count(vw.MenteePersonId) 
			FROM Search.vw_TrainingGrantDistinctTrainees vw
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.DepartmentId = dept.Id
				AND vw.MenteeTypeId = 2
				AND vw.IsTrainingGrantEligible = 1) AS TotalPostdocTGETrainees
		, (SELECT DISTINCT Count(ppl.PersonId) 
			FROM Search.vw_TrainingGrantDistinctTrainees vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.DepartmentId = dept.Id
				AND vw.MenteeTypeId = 2
				AND ppl.IsUnderrepresentedMinority = 1) AS TotalPostdocURMTrainees
		, (SELECT DISTINCT Count(ppl.PersonId) 
			FROM Search.vw_TrainingGrantDistinctTrainees vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.DepartmentId = dept.Id
				AND vw.MenteeTypeId = 2
				AND ppl.IsIndividualWithDisabilities = 1) AS TotalPostdocDisabilitiesTrainees
		, (SELECT DISTINCT Count(ppl.PersonId) 
			FROM Search.vw_TrainingGrantDistinctTrainees vw
			INNER JOIN dbo.Person ppl ON ppl.PersonId = vw.MenteePersonId
				AND ppl.IsDeleted = 0
			WHERE vw.TrainingGrantId = tgrant.Id
				AND vw.DepartmentId = dept.Id
				AND vw.MenteeTypeId = 2
				AND ppl.IsFromDisadvantagedBkgd = 1) AS TotalPostdocDisadvantagedTrainees		

	FROM dbo.TrainingGrant tgrant
		INNER JOIN dbo.Funding fund ON fund.Id = tgrant.FundingId
			AND fund.IsDeleted = 0
		LEFT OUTER JOIN dbo.DoctoralLevel docLvl ON docLvl.Id = tgrant.DoctoralLevelId
		LEFT OUTER JOIN dbo.TrainingGrantDepartment tgdept ON tgdept.TrainingGrantId = tgrant.Id
		LEFT OUTER JOIN dbo.Organization dept ON dept.Id = tgdept.OrganizationId
			AND dept.IsDeleted = 0

	WHERE 
		tgrant.IsDeleted = 0





GO


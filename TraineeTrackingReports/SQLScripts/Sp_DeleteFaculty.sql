/****** Object:  StoredProcedure [Search].[Sp_DeleteFaculty]    Script Date: 7/7/2015 10:10:14 AM ******/
DROP PROCEDURE [Search].[Sp_DeleteFaculty]
GO

/****** Object:  StoredProcedure [Search].[Sp_DeleteFaculty]    Script Date: 7/7/2015 10:10:14 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 09/15/2014
-- Description:	Perform all of the updates and deletes necessary to 
--				DELETE a Faculty member
--		NOTE:	Set @SoftDelete to TRUE to soft delete the Faculty
--				** HARD DELETES CANNOT BE UNDONE **
-- =============================================
CREATE PROCEDURE [Search].[Sp_DeleteFaculty]
	@PersonId uniqueidentifier,
	@FacultyId uniqueidentifier,
	@EmployeeId INT,
	@SoftDelete BIT,
	@DeletePersonRecord BIT
AS
BEGIN

	SET NOCOUNT ON;

	--DECLARE @FacultyId uniqueidentifier

	-- If @PersonId was not passed in, go find it to use for deletes
	IF @PersonId IS NULL
	BEGIN	 
		IF @FacultyId IS NOT NULL
			SET @PersonId = (SELECT PersonId FROM dbo.Faculty
						WHERE Id = @FacultyId)
		ELSE IF @EmployeeId IS NOT NULL
			SET @PersonId = (SELECT PersonId FROM dbo.Faculty
						WHERE EmployeeId = @EmployeeId)
	END

	-- Set to SoftDelete by default if NULL is passed in
	SET @SoftDelete = ISNULL(@SoftDelete, 1)

	SET @DeletePersonRecord = ISNULL(@DeletePersonRecord, 1)


	IF @PersonId IS NOT NULL
	BEGIN

		IF @FacultyId IS NULL
			SET @FacultyId = (SELECT Id FROM dbo.Faculty
							WHERE PersonId = @PersonId)

		IF @SoftDelete = 1
		BEGIN

			-- Soft delete all associated Contact Address records
			UPDATE dbo.ContactAddress 
			SET	IsDeleted = 1
			WHERE PersonId = @PersonId

			-- Soft delete all associated Contact Email records
			UPDATE dbo.ContactEmail
			SET	IsDeleted = 1
			WHERE PersonId = @PersonId

			-- Soft delete all associated Contact Phone records
			UPDATE dbo.ContactPhone
			SET	IsDeleted = 1
			WHERE PersonId = @PersonId

			-- Soft delete all associated Academic Data records
			UPDATE dbo.FacultyAcademicData 
			SET	IsDeleted = 1
			WHERE PersonId = @PersonId

			-- Soft delete all associated Research Interest records
			UPDATE dbo.FacultyResearchInterest 
			SET	IsDeleted = 1
			WHERE PersonId = @PersonId

			-- Soft delete all associated Funding Faculty records
			UPDATE dbo.FundingFaculty
			SET	IsDeleted = 1
			WHERE FacultyId = @FacultyId

			-- Soft delete all associated Training Period records
			--UPDATE dbo.TrainingPeriodFaculty
			--SET	IsDeleted = 1
			--WHERE FacultyId = @FacultyId

			-- Soft delete associated Faculty record
			UPDATE dbo.Faculty
			SET	
				IsDeleted = 1,
				DateLastUpdated = GETDATE(),
				LastUpdatedBy = RIGHT(SYSTEM_USER, 6)
			WHERE PersonId = @PersonId

			-- Soft delete associated Person record
			UPDATE dbo.Person
			SET	
				IsDeleted = 1,
				DateLastUpdated = GETDATE(),
				LastUpdatedBy = RIGHT(SYSTEM_USER, 6)
			WHERE PersonId = @PersonId

		END
		ELSE
		BEGIN

			-- HARD delete all associated Contact Address records
			DELETE
			FROM dbo.ContactAddress 
			WHERE PersonId = @PersonId

			-- HARD delete all associated Contact Email records
			DELETE
			FROM dbo.ContactEmail
			WHERE PersonId = @PersonId

			-- HARD delete all associated Contact Phone records
			DELETE
			FROM dbo.ContactPhone
			WHERE PersonId = @PersonId

			-- HARD delete all associated Academic Data records
			DELETE
			FROM dbo.FacultyAcademicData 
			WHERE PersonId = @PersonId


			-- Since a Research Interest may have Training Grant(s) associated, 
			--	HARD delete the JOIN recs 1st
			SELECT Id 
			INTO #Temp1
			FROM dbo.FacultyResearchInterest
			WHERE PersonId = @PersonId

			DELETE
			FROM dbo.TrainingGrantFacultyResearchInterest
			WHERE FacultyResearchInterestId IN (SELECT Id FROM #Temp1)

			DROP TABLE #Temp1

			-- HARD delete all associated Research Interest records
			DELETE
			FROM dbo.FacultyResearchInterest 
			WHERE PersonId = @PersonId

			-- HARD delete all associated Training Grant records
			DELETE
			FROM dbo.FundingFaculty
			WHERE FacultyId = @FacultyId

			-- HARD delete all associated Training Period records
			DELETE
			FROM dbo.TrainingPeriodFaculty
			WHERE FacultyId = @FacultyId

			-- HARD delete associated Faculty record
			DELETE
			FROM dbo.Faculty
			WHERE PersonId = @PersonId

			IF @DeletePersonRecord = 1
			BEGIN
				-- HARD delete associated Person record
				DELETE
				FROM dbo.Person
				WHERE PersonId = @PersonId
			END

		END

	END

	SET NOCOUNT OFF
END

GO


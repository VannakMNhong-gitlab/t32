/****** Object:  View [Search].[vw_UsageOfClinicalFacultyPathways]    Script Date: 5/2/2016 3:18:14 PM ******/
DROP VIEW [Search].[vw_UsageOfClinicalFacultyPathways]
GO

/****** Object:  View [Search].[vw_UsageOfClinicalFacultyPathways]    Script Date: 5/2/2016 3:18:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [Search].[vw_UsageOfClinicalFacultyPathways] 
AS 
	SELECT 
		facPath.Id AS ClinicalFacultyPathwayId
		, facPath.Name AS ClinicalFacultyPathway
		, (SELECT Count(ClinicalFacultyPathwayId) 
			FROM dbo.FacultyAppointment facAppt				
			WHERE facAppt.ClinicalFacultyPathwayId = facPath.Id
				AND facAppt.IsDeleted = 0) AS TotalClinicalFacultyPathways

	FROM 
		dbo.ClinicalFacultyPathway facPath

	WHERE
		facPath.IsDeleted = 0

	GROUP BY
		facPath.Id
		, facPath.Name
		

GO


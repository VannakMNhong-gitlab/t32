/****** Object:  StoredProcedure [Reports].[Sp_FundingProgramProjects]    Script Date: 5/2/2016 4:29:02 PM ******/
DROP PROCEDURE [Reports].[Sp_FundingProgramProjects]
GO

/****** Object:  StoredProcedure [Reports].[Sp_FundingProgramProjects]    Script Date: 5/2/2016 4:29:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 05/02/2016
-- Description:	Used by Report: Funding
-- =============================================
CREATE PROCEDURE [Reports].[Sp_FundingProgramProjects]
	@FundingId UNIQUEIDENTIFIER

AS
BEGIN

	SET NOCOUNT ON;

	IF @FundingId IS NOT NULL
	BEGIN

		SELECT DISTINCT TOP 100 PERCENT
			fund.Id AS ParentFundId
			, fund.FundingTypeId AS ParentFundTypeId
			, CASE 
					WHEN facProjectPerson.LastName IS NULL AND facProjectPerson.FirstName IS NULL THEN NULL
					WHEN facProjectPerson.LastName IS NOT NULL THEN facProjectPerson.LastName
					ELSE ''
				END +
				CASE WHEN facProjectPerson.FirstName IS NOT NULL THEN ', ' + facProjectPerson.FirstName
					ELSE ''
				END +
				CASE WHEN facProjectPerson.MiddleName IS NOT NULL THEN ' ' + facProjectPerson.MiddleName
					ELSE ''
				END AS ParentFundProgramProjectPDFullName
			, progproj.Id AS ProjectFundId
			, progproj.Title AS ProjectFundTitle
			, progproj.FundingStatusId AS ProjectFundStatusId
			, projstat.Name AS ProjectFundStatus
			, projsponsor.Id AS ProjectFundSponsorId
			, projsponsor.Name AS ProjectFundSponsor
			, progproj.GRTNumber AS ProjectFundGRTNumber
			, CASE 
				WHEN projFacPersonPI.LastName IS NULL AND projFacPersonPI.FirstName IS NULL THEN NULL
				WHEN projFacPersonPI.LastName IS NOT NULL THEN projFacPersonPI.LastName
				ELSE ''
				END +
				CASE WHEN projFacPersonPI.FirstName IS NOT NULL THEN ', ' + projFacPersonPI.FirstName
					ELSE ''
				END +
				CASE WHEN projFacPersonPI.MiddleName IS NOT NULL THEN ' ' + projFacPersonPI.MiddleName
					ELSE ''
				END AS ProjectFundPDFullName
			, CASE WHEN progproj.FundingStatusId < 4 THEN progproj.DateProjectedStart
					ELSE progproj.DateStarted END
				AS ProjectFundStartDate
			, CASE WHEN progproj.FundingStatusId < 4 THEN progproj.DateProjectedEnd
					ELSE progproj.DateEnded END
				AS ProjectFundEndDate
			--, progproj.DateStarted AS ProjectFundDateStarted
			--, progproj.DateEnded AS ProjectFundDateEnded
			, progproj.DateLastUpdated AS ProjectFundDateLastUpdated
			, progproj.LastUpdatedBy AS ProjectFundLastUpdatedBy
			, updLoginsProject.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLoginsProject.MiddleName))) > 0
					THEN LEFT(LTRIM(RTRIM(updLoginsProject.MiddleName)), 1) + '. '
					ELSE '' END + updLoginsProject.LastName 
				AS ProjectFundLastUpdatedByName
			--, progproj.DateProjectedStart AS ProjectFundDateProjectedStart
			--, progproj.DateProjectedEnd AS ProjectFundDateProjectedEnd

		FROM 
			dbo.Funding AS fund
			LEFT OUTER JOIN dbo.Faculty facProject ON facProject.Id = fund.ProgramProjectPDId
				AND facProject.IsDeleted = 0
			LEFT OUTER JOIN dbo.Person facProjectPerson ON facProjectPerson.PersonId = facProject.PersonId
				AND facProjectPerson.IsDeleted = 0
			LEFT OUTER JOIN dbo.Funding AS progproj ON progproj.GRTNumber = fund.GRTNumber
				AND progproj.Id != fund.Id
				AND progproj.IsDeleted = 0
			LEFT OUTER JOIN dbo.FundingFaculty projFundFacPI ON projFundFacPI.FundingId = progproj.Id
				AND projFundFacPI.IsDeleted = 0
			LEFT OUTER JOIN dbo.Faculty projFacPI ON projFacPI.Id = projFundFacPI.FacultyId
				AND projFacPI.IsDeleted = 0
			LEFT OUTER JOIN dbo.Person projFacPersonPI ON projFacPersonPI.PersonId = projFacPI.PersonId
				AND projFacPersonPI.IsDeleted = 0
			LEFT OUTER JOIN dbo.FundingStatus AS projstat ON projstat.Id = progproj.FundingStatusId			
			LEFT OUTER JOIN dbo.Institution AS projsponsor ON projsponsor.Id = progproj.SponsorId
				AND projsponsor.IsDeleted = 0
			LEFT OUTER JOIN Search.vw_Logins updLoginsProject ON updLoginsProject.Username = progproj.LastUpdatedBy

		WHERE
			fund.IsDeleted = 0
			AND fund.Id = @FundingId

	
	END

	SET NOCOUNT OFF
END

GO


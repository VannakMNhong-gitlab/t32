/****** Object:  StoredProcedure [Reports].[Sp_MenteeCurrentPosition]    Script Date: 5/2/2016 4:30:20 PM ******/
DROP PROCEDURE [Reports].[Sp_MenteeCurrentPosition]
GO

/****** Object:  StoredProcedure [Reports].[Sp_MenteeCurrentPosition]    Script Date: 5/2/2016 4:30:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 04/14/2016
-- Description:	Used by Report: Mentee
-- =============================================
CREATE PROCEDURE [Reports].[Sp_MenteeCurrentPosition]
	@MenteeId UNIQUEIDENTIFIER

AS
BEGIN

	SET NOCOUNT ON;

	IF @MenteeId IS NOT NULL
	BEGIN

		SELECT TOP 100 PERCENT 
			mentee.Id AS MenteeId
			, mentee.PersonId AS MenteePersonId
			, p.LastName
			, workHist.Id AS CurrentPositionId
			, workhist.PositionTitle
			, inst.Id AS InstitutionId
			, inst.Name AS Institution
			, workhist.DateStartedPosition
			, workhist.DateEndedPosition
			, workHist.SupportSourceText
			, workhist.DateLastUpdated
			, updLogins.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLogins.MiddleName))) > 0
					THEN LEFT(LTRIM(RTRIM(updLogins.MiddleName)), 1) + '. '
					ELSE '' END + updLogins.LastName 
				AS LastUpdatedByName

		FROM 
			dbo.Mentee mentee
			INNER JOIN dbo.Person p ON p.PersonId = mentee.PersonId
				AND p.IsDeleted = 0
			INNER JOIN dbo.WorkHistory workhist ON workhist.PersonId = mentee.PersonId
				AND workhist.IsDeleted = 0
			LEFT OUTER JOIN dbo.Institution inst ON inst.Id = workhist.InstitutionId
				AND inst.IsDeleted = 0
			LEFT OUTER JOIN Search.vw_Logins updLogins ON updLogins.Username = workhist.LastUpdatedBy
	
		WHERE
			mentee.IsDeleted = 0
			AND mentee.Id = @MenteeId

		ORDER BY
			workhist.Id

	END

	SET NOCOUNT OFF
END
GO


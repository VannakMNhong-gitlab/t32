/****** Object:  StoredProcedure [Reports].[Sp_MenteeTrainingPeriods]    Script Date: 5/2/2016 4:32:01 PM ******/
DROP PROCEDURE [Reports].[Sp_MenteeTrainingPeriods]
GO

/****** Object:  StoredProcedure [Reports].[Sp_MenteeTrainingPeriods]    Script Date: 5/2/2016 4:32:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 04/14/2016
-- Description:	Used by Report: Mentee
-- =============================================
CREATE PROCEDURE [Reports].[Sp_MenteeTrainingPeriods]
	@MenteeId UNIQUEIDENTIFIER

AS
BEGIN

	SET NOCOUNT ON;

	IF @MenteeId IS NOT NULL
	BEGIN

		SELECT DISTINCT TOP 100 PERCENT
			tp.Id AS TrainingPeriodId
			, tp.PersonId AS MenteePersonId
			, mentee.Id AS MenteeId
			, mentee.StudentId AS MenteeStudentId
			, docLvl.Id AS DoctoralLevelId
			, docLvl.LevelName AS DoctoralLevel
			, tp.InstitutionId
			, inst.Name AS InstitutionName
			, prog.Id AS ProgramId
			, prog.Title AS ProgramTitle
			, tp.YearStarted
			, tp.YearEnded
			, tp.AcademicDegreeId AS CompletedDegreeId
			, degree.Name AS CompletedDegree
			, tp.DegreeSoughtId
			, degSought.Name AS DegreeSought
			, tp.ResearchProjectTitle
			, tp.DateLastUpdated
			, tp.LastUpdatedBy
			, updLogins.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLogins.MiddleName))) > 0
				THEN LEFT(LTRIM(RTRIM(updLogins.MiddleName)), 1) + '. '
				ELSE '' END + updLogins.LastName AS LastUpdatedByName

		FROM dbo.TrainingPeriod tp
			LEFT OUTER JOIN dbo.Mentee mentee ON mentee.PersonId = tp.PersonId 
				AND mentee.IsDeleted = 0
			INNER JOIN dbo.Person pMentee ON pMentee.PersonId = mentee.PersonId
				AND pMentee.IsDeleted = 0
			LEFT OUTER JOIN dbo.DoctoralLevel docLvl ON docLvl.Id = tp.DoctoralLevelId
			LEFT OUTER JOIN dbo.TrainingPeriodProgram tpprog ON tpprog.TrainingPeriodId = tp.Id
			LEFT OUTER JOIN dbo.Program prog ON prog.Id = tpprog.ProgramId
				AND prog.IsDeleted = 0
			LEFT OUTER JOIN dbo.Institution inst ON inst.Id = tp.InstitutionId
				AND inst.IsDeleted = 0
			LEFT OUTER JOIN dbo.AcademicDegree degree ON degree.Id = tp.AcademicDegreeId
				AND degree.IsDeleted = 0
			LEFT OUTER JOIN dbo.AcademicDegree degSought ON degSought.Id = tp.DegreeSoughtId
				AND degSought.IsDeleted = 0
			LEFT OUTER JOIN Search.vw_Logins updLogins ON updLogins.Username = tp.LastUpdatedBy

		WHERE 
			tp.IsDeleted = 0
			AND mentee.Id = @MenteeId

		ORDER BY
			TrainingPeriodId

	END

	SET NOCOUNT OFF
END
GO


/****** Object:  View [Search].[vw_UsageOfPrograms]    Script Date: 6/2/2015 10:27:11 AM ******/
DROP VIEW [Search].[vw_UsageOfPrograms]
GO

/****** Object:  View [Search].[vw_UsageOfPrograms]    Script Date: 6/2/2015 10:27:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_UsageOfPrograms] 
AS 
	SELECT 
		prog.Id AS ProgramId
		, prog.Title AS ProgramTitle
		, (SELECT Count(ProgramId) FROM dbo.ApplicantProgram appprog
			WHERE appprog.ProgramId = prog.Id) AS TotalApplicantPrograms
		, (SELECT Count(ProgramId) FROM dbo.FacultyProgram facprog
			WHERE facprog.ProgramId = prog.Id) AS TotalFacultyPrograms		
		, (SELECT Count(ProgramId) FROM dbo.TrainingGrantProgram tgprog 
			WHERE tgprog.ProgramId = prog.Id) AS TotalTrainingGrantPrograms
		, (SELECT Count(ProgramId) FROM dbo.TrainingPeriodProgram tpprog 
			WHERE tpprog.ProgramId = prog.Id) AS TotalTrainingPeriodPrograms

	FROM 
		dbo.Program prog

	WHERE 
		prog.IsDeleted = 0

	GROUP BY
		prog.Id
		, prog.Title





GO


/****** Object:  View [Search].[vw_PersonDemographics]    Script Date: 2/22/2016 11:47:02 AM ******/
DROP VIEW [Search].[vw_PersonDemographics]
GO

/****** Object:  View [Search].[vw_PersonDemographics]    Script Date: 2/22/2016 11:47:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- Fixed FullName to show NULL instead of '' if there's no First or Last Name
CREATE VIEW [Search].[vw_PersonDemographics] 
AS 
	SELECT DISTINCT
		p.PersonId
		, p.DisplayId
		, p.FirstName
		, p.MiddleName
		, p.LastName
		, CASE 
			WHEN p.LastName IS NULL AND p.FirstName IS NULL THEN NULL			
			WHEN p.LastName IS NOT NULL THEN p.LastName
				ELSE ''
			END +
			CASE WHEN p.FirstName IS NOT NULL THEN ', ' + p.FirstName
				ELSE ''
			END +
			CASE WHEN p.MiddleName IS NOT NULL THEN ' ' + p.MiddleName
				ELSE ''
			END AS FullName
		, p.IsUnderrepresentedMinority
		, p.IsIndividualWithDisabilities
		, p.IsFromDisadvantagedBkgd
		, p.GenderId
		, gend.Name AS Gender
		, p.GenderAtBirthId
		, gendBirth.Name AS GenderAtBirth
		, p.EthnicityId
		, eth.Name AS Ethnicity
		, p.DateCreated
		, fac.EmployeeId
		, mentee.StudentId
		, app.ApplicantId
		, CASE 
			WHEN fac.EmployeeId IS NOT NULL THEN 1
			ELSE 0
		END AS IsFaculty
		, CASE 
			WHEN mentee.StudentId IS NOT NULL THEN 1
			ELSE 0
		END AS IsMentee
		, CASE 
			WHEN app.ApplicantId IS NOT NULL THEN 1
			ELSE 0
		END AS IsApplicant
		, CASE WHEN email.IsPrimary = 1
			THEN email.EmailAddress
			ELSE ''
		END AS PrimaryEmailAddress
		, CASE WHEN email.IsPrimary = 0
			THEN email.EmailAddress
			ELSE ''
		END AS EmailAddress
		, CASE WHEN addy.IsPrimary = 1
				THEN addy.AddressLine1 + 
					CASE WHEN addy.AddressLine2 IS NOT NULL
						THEN ' ' + addy.AddressLine2
						ELSE ''
					END + 
					CASE WHEN addy.AddressLine3 IS NOT NULL
						THEN ' ' + addy.AddressLine3
						ELSE ''
					END
					+ ', ' + addy.City
					+ ', ' + stp.Abbreviation
					+ '  ' + addy.PostalCode
				ELSE ''
			END AS MailingAddress
		, CASE WHEN addy.IsPrimary = 1 THEN addy.AddressLine1 END AS AddressLine1
		, CASE WHEN addy.IsPrimary = 1 THEN addy.AddressLine2 END AS AddressLine2
		, CASE WHEN addy.IsPrimary = 1 THEN addy.AddressLine3 END AS AddressLine3
		, CASE WHEN addy.IsPrimary = 1 THEN addy.City END AS AddressCity
		, CASE WHEN addy.IsPrimary = 1 THEN addy.StateId END AS AddressStateId
		, CASE WHEN addy.IsPrimary = 1 THEN stp.Abbreviation END AS AddressStateProvince
		, CASE WHEN addy.IsPrimary = 1 THEN addy.PostalCode END AS AddressPostalCode
		, CASE WHEN phone.IsPrimary = 0
			THEN phone.PhoneNumber
			ELSE ''
		END AS PhoneNumber
	FROM dbo.Person p
		LEFT OUTER JOIN dbo.Gender gend ON gend.Id = p.GenderId
		LEFT OUTER JOIN dbo.Gender gendBirth ON gendBirth.Id = p.GenderAtBirthId
		LEFT OUTER JOIN dbo.Ethnicity eth ON eth.Id = p.EthnicityId
		LEFT JOIN dbo.Faculty fac ON fac.PersonId = p.PersonId
			AND fac.IsDeleted = 0
		LEFT JOIN dbo.Mentee mentee ON mentee.PersonId = p.PersonId
			AND mentee.IsDeleted = 0
		LEFT JOIN dbo.Applicant app ON app.PersonId = p.PersonId
			AND app.IsDeleted = 0
		LEFT OUTER JOIN dbo.ContactEmail email ON email.PersonId = p.PersonId
			AND email.IsDeleted = 0
		LEFT OUTER JOIN dbo.ContactAddress addy ON addy.PersonId = p.PersonId
			AND addy.IsDeleted = 0
		LEFT OUTER JOIN dbo.StateProvince stp ON stp.Id = addy.StateId
			AND stp.IsDeleted = 0
		LEFT OUTER JOIN dbo.ContactPhone phone ON phone.PersonId = p.PersonId
			AND phone.IsDeleted = 0
	WHERE 
		p.IsDeleted = 0




GO


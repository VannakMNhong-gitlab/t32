/****** Object:  View [Search].[vw_TrainingPeriodMentors]    Script Date: 5/2/2016 4:55:08 PM ******/
DROP VIEW [Search].[vw_TrainingPeriodMentors]
GO

/****** Object:  View [Search].[vw_TrainingPeriodMentors]    Script Date: 5/2/2016 4:55:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_TrainingPeriodMentors] 
AS 
	SELECT DISTINCT
		tp.Id AS TrainingPeriodId
		, mentors.FacultyId AS FacultyId
		, fac.PersonId AS MentorPersonId
		, pMentor.FirstName AS MentorFirstName
		, pMentor.MiddleName AS MentorMiddleName
		, pMentor.LastName AS MentorLastName
		, CASE 
			WHEN pMentor.LastName IS NULL AND pMentor.FirstName IS NULL THEN NULL
			WHEN pMentor.LastName IS NOT NULL THEN pMentor.LastName
				ELSE ''
			END +
			CASE WHEN pMentor.FirstName IS NOT NULL THEN ', ' + pMentor.FirstName
				ELSE ''
			END +
			CASE WHEN pMentor.MiddleName IS NOT NULL THEN ' ' + pMentor.MiddleName
				ELSE ''
			END AS MentorFullName
		, fac.EmployeeId
		, fac.PrimaryOrganizationId
		, pOrg.DisplayName AS MentorPrimaryOrganization
		, tp.InstitutionId
		, inst.InstitutionIdentifier
		, inst.Name AS InstitutionName
		, inst.City AS InstitutionCity
		, inst.StateId AS InstitutionStateId
		, stp.Abbreviation AS InstitutionStateAbbreviation
		, stp.FullName AS InstitutionStateFullName
		, inst.CountryId AS InstitutionCountryId
		, c.Name AS InstitutionCountry
		, tp.YearStarted
		, tp.YearEnded
		, tp.ResearchProjectTitle
		, tp.DateLastUpdated
		, tp.LastUpdatedBy
		, updLogins.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLogins.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLogins.MiddleName)), 1) + '. '
			ELSE '' END + updLogins.LastName AS LastUpdatedByName

	FROM dbo.TrainingPeriodFaculty mentors
		INNER JOIN dbo.TrainingPeriod tp ON tp.Id = mentors.TrainingPeriodId
			AND tp.IsDeleted = 0
		INNER JOIN dbo.Faculty fac ON fac.Id = mentors.FacultyId
			AND fac.IsDeleted = 0
		LEFT OUTER JOIN dbo.Person pMentor ON pMentor.PersonId = fac.PersonId
			AND pMentor.IsDeleted = 0
		LEFT OUTER JOIN dbo.Organization pOrg ON pOrg.Id = fac.PrimaryOrganizationId
			AND pOrg.IsDeleted = 0
		LEFT OUTER JOIN dbo.Institution inst ON inst.Id = tp.InstitutionId
			AND inst.IsDeleted = 0
		LEFT OUTER JOIN dbo.StateProvince stp ON stp.Id = inst.StateId
			AND stp.IsDeleted = 0
		LEFT OUTER JOIN dbo.Country c ON (c.Id = inst.CountryId OR c.Id = stp.CountryId)
			AND c.IsDeleted = 0
		LEFT OUTER JOIN Search.vw_Logins updLogins ON updLogins.Username = tp.LastUpdatedBy


GO


/****** Object:  StoredProcedure [Reports].[Sp_T32Table4]    Script Date: 1/26/2016 3:44:10 PM ******/
DROP PROCEDURE [Reports].[Sp_T32Table4]
GO

/****** Object:  StoredProcedure [Reports].[Sp_T32Table4]    Script Date: 1/26/2016 3:44:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 04/16/2015
-- Description:	Used by T32 TABLE 4 Report
--				FUNDING for Participating Faculty
-- =============================================
CREATE PROCEDURE [Reports].[Sp_T32Table4]
	@TrainingGrantId uniqueidentifier,
	@CurrentYear INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @UseCurrent INT = 0

	IF @CurrentYear IS NULL
		SET @UseCurrent = 1
	--SET @CurrentYear = ISNULL(@CurrentYear, YEAR(GETDATE()))

	SELECT DISTINCT
		vw.FacultyId
		, vw.FacultyPersonId
		, vw.FacultyLastName
		, vw.FacultyFirstName
		, vw.FacultyFullName
		, vw.FacultyFullNameAbbrev
		, vw.FacultyRoleId
		, vw.FacultyRole
		, vw.FundingId
		, vw.TrainingGrantId
		, vw.Title
		, vw.StatusId
		, vw.StatusName
		, vw.SponsorId
		, vw.SponsorName
		, vw.SponsorAwardNumber
		, vw.DateProjectedStart
		, RIGHT('00' + CAST(MONTH(vw.DateProjectedStart) AS VARCHAR),2) + '/' + RIGHT(YEAR(vw.DateProjectedStart), 2) AS DateProjectedStartShort
		, vw.DateProjectedEnd
		, RIGHT('00' + CAST(MONTH(vw.DateProjectedEnd) AS VARCHAR),2) + '/' + RIGHT(YEAR(vw.DateProjectedEnd), 2) AS DateProjectedEndShort
		, vw.DateStarted
		, RIGHT('00' + CAST(MONTH(vw.DateStarted) AS VARCHAR),2) + '/' + RIGHT(YEAR(vw.DateStarted), 2) AS DateStartedShort
		, vw.DateEnded
		, RIGHT('00' + CAST(MONTH(vw.DateEnded) AS VARCHAR),2) + '/' + RIGHT(YEAR(vw.DateEnded), 2) AS DateEndedShort
		, vw.GRTNumber
		, vw.PrimeAward
		, costs.CostYear
		, costs.CurrentYearDirectCosts
		, costs.TotalDirectCosts

	FROM 
		Reports.vw_T32Table4 vw
		LEFT OUTER JOIN dbo.FundingDirectCost costs ON costs.FundingId = vw.FundingId
			AND costs.IsDeleted = 0
			AND ((@UseCurrent = 1 AND costs.BudgetPeriodStatusId = 1)
				OR ((@UseCurrent = 0) AND (@CurrentYear BETWEEN YEAR(costs.DateStarted) AND YEAR(costs.DateEnded)))
			)
			--AND ((costs.CostYear IS NULL) OR (costs.CostYear = @CurrentYear))

	WHERE 
		((@TrainingGrantId IS NULL) OR (vw.TrainingGrantId = @TrainingGrantId))
		AND NOT ((vw.FundingId IS NULL) AND (FacultyRoleId IS NOT NULL))
		--AND @TrainingGrantId = 'D159F84F-5D07-4462-BBC1-F9A8A3714E45'

	SET NOCOUNT OFF
END

GO


/****** Object:  View [Reports].[vw_T32Table8B]    Script Date: 9/15/2015 3:42:15 PM ******/
DROP VIEW [Reports].[vw_T32Table8B]
GO

/****** Object:  View [Reports].[vw_T32Table8B]    Script Date: 9/15/2015 3:42:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [Reports].[vw_T32Table8B] 
AS 

	SELECT DISTINCT 
		tgrant.Id AS TrainingGrantId
		, fund.IsRenewal
		, tgrant.DoctoralLevelId
		, docLvl.LevelName AS DoctoralLevel
		, 1 AS Column2ItemTypeId
		, 'Program' AS Column2ItemType
		, prog.DisplayId AS Column2ItemId
		, prog.Title AS Column2ItemName

	FROM dbo.TrainingGrant tgrant
		INNER JOIN dbo.Funding fund ON fund.Id = tgrant.FundingId
			AND fund.IsDeleted = 0
		INNER JOIN dbo.DoctoralLevel docLvl ON docLvl.Id = tgrant.DoctoralLevelId
		INNER JOIN dbo.TrainingGrantProgram tgprog ON tgprog.TrainingGrantId = tgrant.Id
		INNER JOIN dbo.Program prog ON prog.Id = tgprog.ProgramId
			AND prog.IsDeleted = 0

	WHERE 
		tgrant.IsDeleted = 0
		AND tgrant.DoctoralLevelId = 2 OR tgrant.DoctoralLevelId = 3


	UNION

	SELECT DISTINCT
		tgrant.Id AS TrainingGrantId
		, fund.IsRenewal
		, tgrant.DoctoralLevelId
		, docLvl.LevelName AS DoctoralLevel
		, 2 AS Column2ItemTypeId
		, 'Department' AS Column2ItemType
		, tgdept.OrganizationId AS Column2ItemId
		, dept.DisplayName AS Column2ItemName

	FROM dbo.TrainingGrant tgrant
		INNER JOIN dbo.Funding fund ON fund.Id = tgrant.FundingId
			AND fund.IsDeleted = 0
		LEFT OUTER JOIN dbo.DoctoralLevel docLvl ON docLvl.Id = tgrant.DoctoralLevelId
		LEFT OUTER JOIN dbo.TrainingGrantDepartment tgdept ON tgdept.TrainingGrantId = tgrant.Id
		LEFT OUTER JOIN dbo.Organization dept ON dept.Id = tgdept.OrganizationId
			AND dept.IsDeleted = 0

	WHERE 
		tgrant.IsDeleted = 0
		AND tgrant.DoctoralLevelId = 2 OR tgrant.DoctoralLevelId = 3

		--AND tgrant.Id = 'D159F84F-5D07-4462-BBC1-F9A8A3714E45'

GO


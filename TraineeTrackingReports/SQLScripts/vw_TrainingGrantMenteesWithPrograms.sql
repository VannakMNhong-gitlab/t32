/****** Object:  View [Search].[vw_TrainingGrantMenteesWithPrograms]    Script Date: 7/28/2015 3:49:23 PM ******/
DROP VIEW [Search].[vw_TrainingGrantMenteesWithPrograms]
GO

/****** Object:  View [Search].[vw_TrainingGrantMenteesWithPrograms]    Script Date: 7/28/2015 3:49:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [Search].[vw_TrainingGrantMenteesWithPrograms] 
AS 
	SELECT DISTINCT
		tgrant.Id AS TrainingGrantId
		, fund.Id AS FundingId
		, dbo.StripHTML_Loop(fund.Title) AS TrainingGrantTitle
		, fund.DateProjectedStart AS TrainingGrantProjectedStartDate
		, fund.DateProjectedEnd AS TrainingGrantProjectedEndDate
		, fund.DateStarted AS TrainingGrantDateStarted
		, fund.DateEnded AS TrainingGrantDateEnded
		, tgrant.DoctoralLevelId AS TrainingGrantTypeId
		, docLvlTG.LevelName AS TrainingGrantType
		, fac.Id AS FacultyId
		, facPerson.PersonId AS FacultyPersonId
		, facPerson.LastName AS FacultyLastName
		, facPerson.FirstName AS FacultyFirstName
		, CASE 
				WHEN facPerson.LastName IS NULL AND facPerson.FirstName IS NULL THEN NULL
				WHEN facPerson.LastName IS NOT NULL THEN facPerson.LastName
				ELSE ''
			END +
			CASE WHEN facPerson.FirstName IS NOT NULL THEN ', ' + facPerson.FirstName
				ELSE ''
			END +
			CASE WHEN facPerson.MiddleName IS NOT NULL THEN ' ' + facPerson.MiddleName
				ELSE ''
			END AS FacultyFullName
		, mentee.Id AS MenteeId
		, mentee.StudentId
		, mentPerson.PersonId AS MenteePersonId
		, mentPerson.LastName AS MenteeLastName
		, mentPerson.FirstName AS MenteeFirstName
		, CASE 
				WHEN mentPerson.LastName IS NULL AND mentPerson.FirstName IS NULL THEN NULL
				WHEN mentPerson.LastName IS NOT NULL THEN mentPerson.LastName
				ELSE ''
			END +
			CASE WHEN mentPerson.FirstName IS NOT NULL THEN ', ' + mentPerson.FirstName
				ELSE ''
			END +
			CASE WHEN mentPerson.MiddleName IS NOT NULL THEN ' ' + mentPerson.MiddleName
				ELSE ''
			END AS MenteeFullName
		, mentPerson.IsUnderrepresentedMinority
		, mentPerson.IsIndividualWithDisabilities
		, mentPerson.IsFromDisadvantagedBkgd
		, mentee.DepartmentId AS MenteeDeptId
		, org.DisplayName AS MenteeDept		
		, docLvlMent.Id AS MenteeTypeId
		, docLvlMent.LevelName AS MenteeType
		, mentee.IsTrainingGrantEligible
		, prog.Id AS MenteeProgramId
		, prog.Title AS MenteeProgramTitle
		, tp.YearStarted AS TrainingPeriodYearStarted
		, tp.YearEnded AS TrainingPeriodYearEnded

	FROM 
		dbo.Funding fund
		INNER JOIN dbo.TrainingGrant tgrant ON tgrant.FundingId = fund.Id
			AND tgrant.IsDeleted = 0
		INNER JOIN dbo.FundingFaculty fundFac ON fundFac.FundingId = fund.Id
			AND fundFac.IsDeleted = 0
		INNER JOIN dbo.Faculty fac ON fac.Id = fundFac.FacultyId
			AND fac.IsDeleted = 0
		INNER JOIN dbo.Person facPerson ON facPerson.PersonId = fac.PersonId
			AND facPerson.IsDeleted = 0
		INNER JOIN dbo.TrainingPeriodFaculty lnkFac2TP ON lnkFac2TP.FacultyId = fac.Id
		INNER JOIN dbo.TrainingPeriod tp ON tp.Id = lnkFac2TP.TrainingPeriodId
			AND tp.IsDeleted = 0
		INNER JOIN dbo.Mentee mentee ON mentee.PersonId = tp.PersonId
			AND mentee.IsDeleted = 0
		INNER JOIN dbo.Person mentPerson ON mentPerson.PersonId = mentee.PersonId
			AND mentPerson.IsDeleted = 0
		LEFT OUTER JOIN dbo.DoctoralLevel docLvlTG ON docLvlTG.Id = tgrant.DoctoralLevelId
		LEFT OUTER JOIN dbo.DoctoralLevel docLvlMent ON docLvlMent.Id = tp.DoctoralLevelId
		LEFT OUTER JOIN dbo.Organization org ON org.Id = mentee.DepartmentId
			AND org.IsDeleted = 0
		LEFT OUTER JOIN dbo.TrainingPeriodProgram tpprog ON tpprog.TrainingPeriodId = tp.Id
		LEFT OUTER JOIN dbo.Program prog ON prog.Id = tpprog.ProgramId
			AND prog.IsDeleted = 0

		
	WHERE 
		fund.IsDeleted = 0
		AND tgrant.Id IS NOT NULL



GO


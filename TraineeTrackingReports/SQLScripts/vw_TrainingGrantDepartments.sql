/****** Object:  View [Search].[vw_TrainingGrantDepartments]    Script Date: 5/2/2016 4:52:09 PM ******/
DROP VIEW [Search].[vw_TrainingGrantDepartments]
GO

/****** Object:  View [Search].[vw_TrainingGrantDepartments]    Script Date: 5/2/2016 4:52:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [Search].[vw_TrainingGrantDepartments] 
AS 
	SELECT DISTINCT
		tgdept.TrainingGrantId
		, fund.SponsorId
		, inst.InstitutionIdentifier
		, inst.Name AS InstitutionName
		, inst.City AS InstitutionCity
		, inst.StateId AS InstitutionStateId
		, stp.Abbreviation AS InstitutionStateAbbreviation
		, stp.FullName AS InstitutionStateFullName
		, inst.CountryId AS InstitutionCountryId
		, c.Name AS InstitutionCountry
		, tg.DoctoralLevelId
		, docLvl.LevelName AS DoctoralLevel
		, tg.DateLastUpdated
		, tg.LastUpdatedBy
		, updLogins.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLogins.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLogins.MiddleName)), 1) + '. '
			ELSE '' END + updLogins.LastName AS LastUpdatedByName
		, tgdept.OrganizationId AS DepartmentId
		, dept.OrganizationId AS DeptIdentifier
		, dept.DisplayName AS DeptDisplayName
		, dept.HRName AS DeptHRName

	FROM dbo.TrainingGrantDepartment tgdept
		INNER JOIN dbo.TrainingGrant tg ON tg.Id = tgdept.TrainingGrantId
			AND tg.IsDeleted = 0
		INNER JOIN dbo.Funding fund ON fund.Id = tg.FundingId
			AND fund.IsDeleted = 0
		INNER JOIN dbo.Organization dept ON dept.Id = tgdept.OrganizationId
			AND dept.IsDeleted = 0
		LEFT OUTER JOIN dbo.Institution inst ON inst.Id = fund.SponsorId
			AND inst.IsDeleted = 0
		LEFT OUTER JOIN dbo.DoctoralLevel docLvl ON docLvl.Id = tg.DoctoralLevelId
		LEFT OUTER JOIN dbo.StateProvince stp ON stp.Id = inst.StateId
			AND stp.IsDeleted = 0
		LEFT OUTER JOIN dbo.Country c ON (c.Id = inst.CountryId	OR c.Id = stp.CountryId)
			AND c.IsDeleted = 0
		LEFT OUTER JOIN Search.vw_Logins updLogins ON updLogins.Username = tg.LastUpdatedBy



GO


/****** Object:  StoredProcedure [Reports].[Sp_MenteePublications]    Script Date: 5/2/2016 4:31:39 PM ******/
DROP PROCEDURE [Reports].[Sp_MenteePublications]
GO

/****** Object:  StoredProcedure [Reports].[Sp_MenteePublications]    Script Date: 5/2/2016 4:31:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 04/14/2016
-- Description:	Used by Report: Mentee
-- =============================================
CREATE PROCEDURE [Reports].[Sp_MenteePublications]
	@MenteeId UNIQUEIDENTIFIER

AS
BEGIN

	SET NOCOUNT ON;

	IF @MenteeId IS NOT NULL
	BEGIN

		SELECT TOP 100 PERCENT 
			mentee.Id AS MenteeId
			, mentee.PersonId AS MenteePersonId
			, p.LastName
			, pub.ID AS PublicationId
			, pub.YearPublished
			, pub.Citation
			, pub.DateLastUpdated
			, updLogins.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLogins.MiddleName))) > 0
					THEN LEFT(LTRIM(RTRIM(updLogins.MiddleName)), 1) + '. '
					ELSE '' END + updLogins.LastName 
				AS LastUpdatedByName

		FROM 
			dbo.Mentee mentee
			INNER JOIN dbo.Person p ON p.PersonId = mentee.PersonId
				AND p.IsDeleted = 0
			INNER JOIN dbo.Publication pub ON pub.AuthorId = mentee.PersonId
				AND pub.IsDeleted = 0
			LEFT OUTER JOIN Search.vw_Logins updLogins ON updLogins.Username = pub.LastUpdatedBy
	
		WHERE
			mentee.IsDeleted = 0
			AND mentee.Id = @MenteeId

		ORDER BY
			pub.Id

	END

	SET NOCOUNT OFF
END
GO


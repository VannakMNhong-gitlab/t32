/****** Object:  UserDefinedFunction [dbo].[StripHTML_XML]    Script Date: 5/13/2015 3:49:40 PM ******/
DROP FUNCTION [dbo].[StripHTML_XML]
GO

/****** Object:  UserDefinedFunction [dbo].[StripHTML_XML]    Script Date: 5/13/2015 3:49:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[StripHTML_XML]( 
	@text varchar(max) 
) RETURNS varchar(max) AS
BEGIN
    DECLARE @textXML xml
    DECLARE @result varchar(max)
    SET @textXML = REPLACE( @text, '&', '' );
    WITH doc(contents) AS
    (
        SELECT chunks.chunk.query('.') FROM @textXML.nodes('/') AS chunks(chunk)
    )
    SELECT @result = contents.value('.', 'varchar(max)') FROM doc
    RETURN @result
END

GO


/****** Object:  View [Search].[vw_SupportGeneralData]    Script Date: 5/2/2016 4:50:08 PM ******/
DROP VIEW [Search].[vw_SupportGeneralData]
GO

/****** Object:  View [Search].[vw_SupportGeneralData]    Script Date: 5/2/2016 4:50:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [Search].[vw_SupportGeneralData] 
AS 

	SELECT
		spt.Id AS SupportId
		, spt.Title AS SupportTitle
		, spt.DisplayId AS SupportDisplayId
		, spt.SupportTypeId
		, sptType.Name AS SupportType
		, sptType.Abbreviation AS SupportTypeAbbreviation
		, sptType.Name + ' (' + sptType.Abbreviation + ')' AS SupportTypeFullName
		, spt.SupportStatusId
		, sptStat.Name AS SupportStatus
		, spt.SupportNumber
		, spt.SupportOrganizationId
		, sptOrg.Name AS SupportOrganization
		, sptOrg.Abbreviation AS SupportOrganizationAbbreviation
		, sptOrg.Name + ' (' + sptOrg.Abbreviation + ')' AS SupportOrganizationFullName
		, spt.DateStarted
		, spt.DateEnded
		, spt.DateCreated
		, spt.CreatedBy
		, updLoginCrt.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLoginCrt.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLoginCrt.MiddleName)), 1) + '. '
			ELSE '' END + updLoginCrt.LastName AS CreatedByName
		, spt.DateLastUpdated
		, spt.LastUpdatedBy
		, updLoginUpd.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLoginUpd.MiddleName))) > 0
			THEN LEFT(LTRIM(RTRIM(updLoginUpd.MiddleName)), 1) + '. '
			ELSE '' END + updLoginUpd.LastName AS LastUpdatedByName

	FROM 
		dbo.Support spt
		LEFT OUTER JOIN dbo.SupportType sptType ON sptType.Id = spt.SupportTypeId
		LEFT OUTER JOIN dbo.SupportStatus sptStat ON sptStat.Id = spt.SupportStatusId
		LEFT OUTER JOIN dbo.SupportOrganization sptOrg ON sptOrg.Id = spt.SupportOrganizationId
		LEFT OUTER JOIN Search.vw_Logins updLoginCrt ON updLoginCrt.Username = spt.CreatedBy
		LEFT OUTER JOIN Search.vw_Logins updLoginUpd ON updLoginUpd.Username = spt.LastUpdatedBy

	WHERE 
		spt.IsDeleted = 0
		



GO


/****** Object:  StoredProcedure [Reports].[Sp_MenteeAcademicHistory]    Script Date: 5/2/2016 4:29:57 PM ******/
DROP PROCEDURE [Reports].[Sp_MenteeAcademicHistory]
GO

/****** Object:  StoredProcedure [Reports].[Sp_MenteeAcademicHistory]    Script Date: 5/2/2016 4:29:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molley Collins
-- Create date: 04/15/2016
-- Description:	Used by Report: Mentee
-- =============================================
CREATE PROCEDURE [Reports].[Sp_MenteeAcademicHistory]
	@MenteeId UNIQUEIDENTIFIER

AS
BEGIN

	SET NOCOUNT ON;

	IF @MenteeId IS NOT NULL
	BEGIN

		SELECT TOP 100 PERCENT 
			mentee.Id AS MenteeId
			, mentee.PersonId AS MenteePersonId
			, p.LastName
			, acadHist.Id AS AcademicHistoryId
			, degs.Name AS Degree
			, instDegree.Id AS DegreeInstitutionId
			, instDegree.Name AS DegreeInstitution
			, acadHist.YearStarted AS DegreeYearStarted
			, acadHist.YearEnded AS DegreeYearEnded
			, acadHist.AreaOfStudy AS DegreeAreaOfStudy
			, acadHist.YearDegreeCompleted
			, acadHist.ResearchProjectTitle
			, acadHist.DoctoralThesis
			, acadHist.ResearchAdvisor
			, acadHist.ResidencyInstitutionId
			, instResid.Name AS ResidencyInstitution
			, acadHist.ResidencyAdvisor
			, acadHist.ResidencyYearStarted
			, acadHist.ResidencyYearEnded
			, dbo.StripHTML_Loop(acadHist.Comments) AS Comments
			, acadHist.DateLastUpdated
			, updLoginsAcadHist.FirstName + ' ' + CASE WHEN len(LTRIM(RTRIM(updLoginsAcadHist.MiddleName))) > 0
					THEN LEFT(LTRIM(RTRIM(updLoginsAcadHist.MiddleName)), 1) + '. '
					ELSE '' END + updLoginsAcadHist.LastName 
				AS LastUpdatedByName

		FROM 
			dbo.Mentee mentee
			INNER JOIN dbo.Person p ON p.PersonId = mentee.PersonId
				AND p.IsDeleted = 0
			INNER JOIN dbo.AcademicHistory acadHist ON acadHist.PersonId = mentee.PersonId
				AND acadHist.IsDeleted = 0
			LEFT OUTER JOIN dbo.AcademicDegree degs ON degs.Id = acadHist.AcademicDegreeId
				AND degs.IsDeleted = 0
			LEFT OUTER JOIN dbo.Institution instDegree ON instDegree.Id = acadHist.InstitutionId
				AND instDegree.IsDeleted = 0
			LEFT OUTER JOIN dbo.Institution instResid ON instResid.Id = acadHist.ResidencyInstitutionId
				AND instResid.IsDeleted = 0
			LEFT OUTER JOIN Search.vw_Logins updLoginsAcadHist ON updLoginsAcadHist.Username = acadHist.LastUpdatedBy
	
		WHERE
			mentee.IsDeleted = 0
			AND mentee.Id = @MenteeId

		ORDER BY
			acadHist.Id

	END

	SET NOCOUNT OFF
END
GO


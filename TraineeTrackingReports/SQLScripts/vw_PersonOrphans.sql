/****** Object:  View [Search].[vw_PersonOrphans]    Script Date: 5/2/2016 4:48:54 PM ******/
DROP VIEW [Search].[vw_PersonOrphans]
GO

/****** Object:  View [Search].[vw_PersonOrphans]    Script Date: 5/2/2016 4:48:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [Search].[vw_PersonOrphans] 
AS 

	SELECT p.*, l.Username, l.IsActive
	FROM Search.vw_Person p
		LEFT OUTER JOIN dbo.Logins l ON l.PersonId = p.PersonId
	WHERE 
		p.EmployeeId IS NULL 
		AND p.StudentId IS NULL 
		AND p.ApplicantId IS NULL
		AND l.LoginId IS NULL

	--SELECT DISTINCT
	--	p.PersonId
	--	, p.DisplayId
	--	, p.FirstName
	--	, p.MiddleName
	--	, p.LastName
	--	, p.DateCreated
	--	, fac.EmployeeId
	--	, mentee.StudentId
	--	, app.ApplicantId
	--	, CASE 
	--		WHEN fac.EmployeeId IS NOT NULL THEN 1
	--		ELSE 0
	--	END AS IsFaculty
	--	, CASE 
	--		WHEN mentee.StudentId IS NOT NULL THEN 1
	--		ELSE 0
	--	END AS IsMentee
	--	, CASE 
	--		WHEN app.ApplicantId IS NOT NULL THEN 1
	--		ELSE 0
	--	END AS IsApplicant
	--FROM dbo.Person p
	--	LEFT JOIN dbo.Faculty fac ON fac.PersonId = p.PersonId
	--		AND fac.IsDeleted = 0
	--	LEFT JOIN dbo.Mentee mentee ON mentee.PersonId = p.PersonId
	--		AND mentee.IsDeleted = 0
	--	LEFT JOIN dbo.Applicant app ON app.PersonId = p.PersonId
	--		AND app.IsDeleted = 0
	--WHERE 
	--	p.IsDeleted = 0
	--	AND ((fac.EmployeeId IS NULL)
	--		AND (mentee.StudentId IS NULL)
	--		AND (app.ApplicantId IS NULL))

	



GO

